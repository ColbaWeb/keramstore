<?$APPLICATION->IncludeComponent(
    "nextype:alpha.shops",
    "main",
    array(
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "IBLOCK_ID" => "8",
        "IBLOCK_TYPE" => "nt_alpha_tools",
        "SEF_FOLDER" => "/shops/",
        "SEF_MODE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_TITLE" => "Y",
        "USER_CONSENT" => "N",
        "USER_CONSENT_ID" => "0",
        "USER_CONSENT_IS_CHECKED" => "Y",
        "USER_CONSENT_IS_LOADED" => "N",
        "COMPONENT_TEMPLATE" => "main",
        "LIST_SOURCE" => "LOCATION",
        "MAP_PROVIDER" => "YANDEX",
        "SEF_URL_TEMPLATES" => array(
            "location" => "#LOCATION_CODE#/",
        )
    ),
    false
);?>
<br>
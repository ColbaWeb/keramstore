<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Расписание работы и адрес салона магазина плитки, контактная информация, схема проезда");
$APPLICATION->SetPageProperty("keywords", "keramstore, керамстор, купить плитку, керамическая плитка, заказать плитку, керамогранит, купить столешницу, контакты, схема проезда");
$APPLICATION->SetPageProperty("title", "Контакты | Керамическая плитка в Москве | KERAMSTORE");
$APPLICATION->SetTitle("Контакты");
?>

<? $APPLICATION->IncludeFile($APPLICATION->GetCurDir() . "contacts_map_inc.php") ?>
    
<? $APPLICATION->IncludeFile($APPLICATION->GetCurDir() . "contacts_text_inc.php") ?>

<? $APPLICATION->IncludeFile($APPLICATION->GetCurDir() . "contacts_form_inc.php") ?>

    
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
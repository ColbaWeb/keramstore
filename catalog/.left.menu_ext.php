<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;


$aMenuLinksExt = $APPLICATION->IncludeComponent(
   "nextype:alpha.menu.sections",
   "",
   Array(
      "IS_SEF" => "Y",
      "DEPTH_LEVEL" => "3",
      "SEF_BASE_URL" => "",
      "SECTION_PAGE_URL" => "/catalog/#SECTION_CODE_PATH#/",
      "IBLOCK_TYPE" => "nt_alpha_catalog", 
      "IBLOCK_ID" => "5", 
      "CACHE_TIME" => "360000000" 
   )
);

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
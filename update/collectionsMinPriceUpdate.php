<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");
\Bitrix\Main\Loader::includeModule("currency");

$IBLOCK_ID = 5;

$arFilter = array('IBLOCK_ID' => $IBLOCK_ID, 'DEPTH_LEVEL' => 1,);
$rsSect = CIBlockSection::GetList(array(),$arFilter, false, array("ID", "NAME", "UF_MIN_PRICE"));
while ($arSect = $rsSect->GetNext())
{
    $rsProducts = CIBlockElement::GetList(
        Array('CATALOG_PRICE_SCALE_1' => 'ASC'),
        Array('IBLOCK_ID' => $IBLOCK_ID, 'SECTION_ID' => $arSect["ID"], "INCLUDE_SUBSECTIONS" => "N", "!CATALOG_PRICE_SCALE_1" => false, "!=CATALOG_PRICE_SCALE_1" => '0'),
        false,
        Array('nTopCount' => 1),
        Array('ID', 'NAME', 'CATALOG_PRICE_SCALE_1')
    );
    while ($arEl = $rsProducts->GetNext())
    {
        $ar_res = CPrice::GetBasePrice($arEl['ID']);
        if($ar_res["CURRENCY"] == 'EUR'){
            $ar_res['PRICE'] = CCurrencyRates::ConvertCurrency($ar_res['PRICE'], "EUR", "RUB");
        }
        $arFields = Array(
            "UF_MIN_PRICE" => $ar_res['PRICE'],
        );

        $bs = new CIBlockSection;
        $res = $bs->Update($arSect["ID"], $arFields);
        var_dump($res);
    }
}
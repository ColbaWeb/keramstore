<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");
\Bitrix\Main\Loader::includeModule("currency");
\Bitrix\Main\Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$IBLOCK_ID = 5;
$IBLOCK_ID_FACTORY = 16;
$hlbl = 7;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
    "select" => array("UF_NAME", "UF_XML_ID"),
    "order" => array("ID" => "ASC"),
    "filter" => array()
));

while($arData = $rsData->Fetch()){
    $arCountry[$arData["UF_NAME"]] = $arData["UF_XML_ID"];
}

$arFilter = array('IBLOCK_ID' => $IBLOCK_ID, 'DEPTH_LEVEL' => 1, "UF_BRANDS" => false);
$rsSect = CIBlockSection::GetList(array(),$arFilter, false, array("ID", "UF_BRANDS"));
while ($arSect = $rsSect->GetNext())
{
    $rsProducts = CIBlockElement::GetList(
        Array('SORT' => 'ASC'),
        Array('IBLOCK_ID' => $IBLOCK_ID, 'SECTION_ID' => $arSect["ID"], "INCLUDE_SUBSECTIONS" => "N", "!PROPERTY_COUNTRY" => false, "!PROPERTY_FACTORY" => false,),
        false,
        Array('nTopCount' => 1),
        Array('ID', 'PROPERTY_COUNTRY', 'PROPERTY_FACTORY')
    );
    while ($arEl = $rsProducts->GetNext())
    {
        $brand = trim($arEl["PROPERTY_FACTORY_VALUE"]);
        $country = $arEl["PROPERTY_COUNTRY_VALUE"];
    }

    $rsFactory = CIBlockElement::GetList(
        Array('SORT' => 'ASC'),
        Array('IBLOCK_ID' => $IBLOCK_ID_FACTORY, "NAME" => $brand, 'PROPERTY_COUNTRY' => $arCountry[$country]),
        false,
        Array('nTopCount' => 1),
        Array('ID',)
    );
    while ($arFactory = $rsFactory->GetNext())
    {
        $arFields = Array(
            "UF_BRANDS" => $arFactory['ID'],
        );
        $bs = new CIBlockSection;
        $res = $bs->Update($arSect["ID"], $arFields);
        var_dump($res);
    }
}
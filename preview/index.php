<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Application as BitrixApplication;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!Loader::includeModule("nextype.alpha") || !Loader::includeModule("iblock"))
{
    Tools\Messages::error('The nextype.alpha module could not be connected');
    return;
}

if (!$GLOBALS['USER']->IsAdmin() && !Application::getInstance()->isDemo())
{
    if (!defined("ERROR_404"))
	define("ERROR_404", "Y");

    \CHTTP::setStatus("404 Not Found");

    \Bitrix\Iblock\Component\Tools::process404('Not found', true, true, true, false);
    return;
}

$request = BitrixApplication::getInstance()->getContext()->getRequest();

if ($request->get('mobile') !== null)
{
    include(__DIR__ . "/include/mobile.php");
}
elseif ($request->get('tablet') !== null)
{
    include(__DIR__ . "/include/tablet.php");
}
else
{
    if (!defined("ERROR_404"))
	define("ERROR_404", "Y");

    \CHTTP::setStatus("404 Not Found");

    \Bitrix\Iblock\Component\Tools::process404('Not found', true, true, true, false);
    return;
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");

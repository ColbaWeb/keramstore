<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Application as BitrixApplication;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

Application::getInstance(SITE_ID)->prolog();

Layout\Assets::getInstance()->addLess(array (
    SITE_TEMPLATE_PATH . '/less/elements.less',
    SITE_TEMPLATE_PATH . '/less/icons.less',
    SITE_TEMPLATE_PATH . '/less/template_styles.less',
));

Layout\Assets::getInstance()->addCss(array (
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap.min.css',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap-select.min.css',
    SITE_TEMPLATE_PATH . '/vendor/perfect-scrollbar/perfect-scrollbar.css',
));

Layout\Assets::getInstance()->addJs(array (
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.min.js',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap.bundle.min.js',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap-select.min.js',
    SITE_TEMPLATE_PATH . '/vendor/maskedinput/jquery.maskedinput.min.js',
    SITE_TEMPLATE_PATH . '/vendor/perfect-scrollbar/perfect-scrollbar.min.js',
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.inview.js',
    SITE_TEMPLATE_PATH . '/vendor/formstyler/jquery.formstyler.min.js',
    SITE_TEMPLATE_PATH . '/vendor/swiper/swiper.min.js',
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.lazy.min.js',
    
    SITE_TEMPLATE_PATH . '/js/application.js',
    SITE_TEMPLATE_PATH . '/js/layout.js',
    SITE_TEMPLATE_PATH . '/js/popups.js',
));
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Mobile preview</title>
        <? $APPLICATION->ShowHead(); ?>
	<style type="text/css">
		body.tablet_preview {
			margin: 0;
		    padding: 0;
		    background-color: #fff;
		    position: relative;
		}
		.options {
			position: fixed;
			top: 169px;
			left: 0;
			display: flex;
			flex-direction: column;
		}
		.options a {
			cursor: pointer;
			width: 60px;
			height: 60px;
			border: 1px solid rgba(0, 0, 0, 0.07);
			box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.04), 0px 0px 2px rgba(0, 0, 0, 0.06), 0px 0px 1px rgba(0, 0, 0, 0.04);
			border-radius: 0px 8px 8px 0px;
			background: url(./../img-preview/settings.svg) center center no-repeat;
		}
		/*buttons*/
		.device-select {
			position: fixed;
			top: 245px;
			left: -120px;
			display: flex;
			cursor: pointer;
			border: 1px solid rgba(0, 0, 0, 0.07);
			box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.04), 0px 0px 2px rgba(0, 0, 0, 0.06), 0px 0px 1px rgba(0, 0, 0, 0.04);
			border-radius: 0px 8px 8px 0px;
			width: 180px;
			height: 60px;
		}
		.device-select:hover {
			left: 0;
			flex-direction: row-reverse;
		}
		.device-select .item {
			width: 60px;
			height: 60px;
			opacity: 0.5;
			box-sizing: border-box;
		}
		.device-select .item:hover {
			opacity: 1;
			border: 1px solid rgba(0, 0, 0, 0.2);
		}
		.device-select .item.desktop {
			background: url(./../img-preview/desktop-preview.svg) center center no-repeat;
		}
		.device-select .item.tablet {
			background: url(./../img-preview/tablet-preview.svg) center center no-repeat;
		}
		.device-select .item.mobile {
			background: url(./../img-preview/mobile-preview.svg) center center no-repeat;
		}
		.device-select .item.active {
			opacity: 1;
			order: 3;
		}
		/*end buttons*/

		.tablet_preview .device {
			margin: 64px auto;
			height: 1068px;
		    width: 812px;
			position: relative;
		}
		.tablet_preview .device .on, .tablet_preview .device .vol {
			position: absolute;
		}
		.tablet_preview .device .on {
			right: 59px;
			top: -3px;
			background: url(./img/on-off.svg) center 0 no-repeat;
			width: 23px;
			height: 13px;
		}
		.tablet_preview .device .vol {
			right: -2.5px;
			top: 100px;
			background: url(./img/volume.svg) center 0 no-repeat;
			width: 13px;
			height: 103px;
		}
		.tablet_preview .device .tablet {
		    background: url(./img/tablet.svg) center 0 no-repeat;
		    position: absolute;
		    top: 0;
		    height: 1068px;
		    width: 812px;
		    margin: auto;
		    display: flex;
		    justify-content: center;
		    align-items: center;
		    z-index: 5;
		}
		.tablet_preview .device .tablet .container-iframe {
			background-color: #fff;
			overflow-x: hidden;
			overflow-y: auto;
			width: 765px;
			height: 1020px;
			border-radius: 25px;
			z-index: 10;
		}
	</style>
</head>

<body class="tablet_preview">
	
	<div class="device">
		<div class="on"></div>
		<div class="vol"></div>
		<div class="tablet">
			<div class="container-iframe">
                            <iframe seamless="seamless" src="<?=SITE_DIR?>?preview_mode=tablet" style="width: 765px; height: 100%;border: 0"></iframe>
			</div>
		</div>
	</div>
    
        <?
        if (Options\Base::getInstance(SITE_ID)->getValue('publicOptions'))
        {
            Layout\Partial::getInstance()->render('ajax/public_options.php', array (), false, false);
        }
        
        Application::getInstance()->epilog();
        ?>
    
        <script>
        BX.ready(function(){
            new window.Alpha.Application({
                siteId: '<?= SITE_ID ?>',
                currentDir: '<?=BitrixApplication::getInstance()->getContext()->getRequest()->getRequestedPageDirectory()?>'
            });
        });
        </script>

</body>

</html>
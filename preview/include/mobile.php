<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Application as BitrixApplication;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

Application::getInstance(SITE_ID)->prolog();

Layout\Assets::getInstance()->addLess(array (
    SITE_TEMPLATE_PATH . '/less/elements.less',
    SITE_TEMPLATE_PATH . '/less/icons.less',
    SITE_TEMPLATE_PATH . '/less/template_styles.less',
));

Layout\Assets::getInstance()->addCss(array (
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap.min.css',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap-select.min.css',
    SITE_TEMPLATE_PATH . '/vendor/perfect-scrollbar/perfect-scrollbar.css',
));

Layout\Assets::getInstance()->addJs(array (
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.min.js',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap.bundle.min.js',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap-select.min.js',
    SITE_TEMPLATE_PATH . '/vendor/maskedinput/jquery.maskedinput.min.js',
    SITE_TEMPLATE_PATH . '/vendor/perfect-scrollbar/perfect-scrollbar.min.js',
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.inview.js',
    SITE_TEMPLATE_PATH . '/vendor/formstyler/jquery.formstyler.min.js',
    SITE_TEMPLATE_PATH . '/vendor/swiper/swiper.min.js',
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.lazy.min.js',
    
    SITE_TEMPLATE_PATH . '/js/application.js',
    SITE_TEMPLATE_PATH . '/js/layout.js',
    SITE_TEMPLATE_PATH . '/js/popups.js',
));
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Mobile preview</title>
        <? $APPLICATION->ShowHead(); ?>
	<style type="text/css">
		body.mobile_preview {
			margin: 0;
		    padding: 0;
		    background-color: #fff;
		    position: relative;
                    cursor: grab;
		}
		.mobile_preview .btns {
			position: fixed;
			top: 15%;
			left: 0;
			display: flex;
			flex-direction: column;
		}
		.mobile_preview .btns div {
			width: 60px;
			height: 60px;
			border: 1px solid rgba(0, 0, 0, 0.07);
			box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.04), 0px 0px 2px rgba(0, 0, 0, 0.06), 0px 0px 1px rgba(0, 0, 0, 0.04);
			border-radius: 0px 8px 8px 0px;
			box-sizing: border-box;
		}
		.mobile_preview .btns .options {
			margin-bottom: 16px;
			background: url(./img/settings.svg) center center no-repeat;
		}
		.mobile_preview .btns .mobile {
			background: url(./img/mobile-sett.svg) center center no-repeat;
		}
		.mobile_preview .device {
			margin: 64px auto;
			height: 852px;
		    width: 415px;
			position: relative;
		}
		.mobile_preview .device .on, .mobile_preview .device .vol {
			position: absolute;
		}
		.mobile_preview .device .on {
			left: -3px;
			top: 77px;
			background: url(./img/on-off.svg) center 0 no-repeat;
			width: 13px;
			height: 24px;
		}
		.mobile_preview .device .vol {
			left: -3px;
			top: 127px;
			background: url(./img/volume.svg) center 0 no-repeat;
			width: 13px;
			height: 103px;
		}
		.mobile_preview .device .mobile {
		    background: url(./img/phone.svg) center 0 no-repeat;
		    position: absolute;
		    top: 0;
		    height: 852px;
		    width: 415px;
		    margin: auto;
		    display: flex;
		    justify-content: center;
		    align-items: center;
		    z-index: 5;
		}
		.mobile_preview .device .mobile .container-iframe {
			background-color: #fff;
			overflow-x: hidden;
			overflow-y: auto;
			width: 375px;
			height: 812px;
			border-radius: 25px;
			z-index: 10;
		}
                .mobile_preview .device .mobile .container-iframe iframe {
                    border-radius: 25px;
                }
	</style>
</head>

<body class="mobile_preview">
	
	<div class="device">
		<div class="on"></div>
		<div class="vol"></div>
		<div class="mobile">
			<div class="container-iframe">
                            <iframe seamless="seamless" src="<?=SITE_DIR?>?preview_mode=mobile" style="width: 375px; height: 100%;border: 0;"></iframe>
			</div>
		</div>
	</div>
    
        <?
        if (Options\Base::getInstance(SITE_ID)->getValue('publicOptions'))
        {
            Layout\Partial::getInstance()->render('ajax/public_options.php', array (), false, false);
        }
        
        Application::getInstance()->epilog();
        ?>
    
        <script>
        BX.ready(function(){
            new window.Alpha.Application({
                siteId: '<?= SITE_ID ?>',
                currentDir: '<?=BitrixApplication::getInstance()->getContext()->getRequest()->getRequestedPageDirectory()?>'
            });
        });
        </script>

</body>

</html>
<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Интернет-магазин керамической плитки в Москве | KERAMSTORE");
$APPLICATION->SetPageProperty("keywords", "купить плитку, керамическая плитка, заказать плитку, керамогранит, купить столешницу");
$APPLICATION->SetPageProperty("description", "Предлагаем купить оригинальные коллекции европейских и российских производителей плитки и керамогранита, в которых собраны качественные, сертифицированные отделочные материалы, соответствующие актуальным тенденциям.");
$APPLICATION->SetTitle("Керамическая плитка в Москве");
?>
<? \Nextype\Alpha\Application::getInstance(SITE_ID)->showHomepage(); ?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
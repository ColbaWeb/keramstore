<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;

/**
* @global CMain $APPLICATION
* @var array $arParams
* @var array $arResult
* @var CatalogSectionComponent $component
* @var CBitrixComponentTemplate $this
* @var string $templateName
* @var string $componentPath
* @var string $templateFolder
*/

$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS'],
        'DETAIL_PAGE_URL' => $arResult['DETAIL_PAGE_URL']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel',
    'WISHLIST_ID' => $mainId.'_wishlist',
    'ITEM_CODE_ID' => $mainId.'_itemcode',
    'DISCOUNT_TIMER_ID' => $mainId.'_discount_timer',
    'STOCK_BLOCK_ID' => $mainId.'_stock_block'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	
}
else
{
	$actualItem = $arResult;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = strlen(strip_tags($arResult['DETAIL_TEXT'])) > 0;
$showProperties = !empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'];
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-primary' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-primary' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['PRODUCT']['SUBSCRIBE'] === 'Y' || $haveOffers);

if ($haveOffers && !empty($arResult['OFFERS_PROP']))
{
    foreach ($arResult['SKU_PROPS'] as $skuProperty)
    {
        if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
            continue;

        $propertyId = $skuProperty['ID'];
        $skuProps[] = array(
            'ID' => $propertyId,
            'SHOW_MODE' => $skuProperty['SHOW_MODE'],
            'VALUES' => $skuProperty['VALUES'],
            'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
        );
    }
}

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

Tools\Product::modifierStock($actualItem, $arParams);
Tools\Product::modifierStock($arResult, $arParams);

$arPartialData = array (
    'haveOffers' => $haveOffers,
    'itemIds' => $itemIds,
    'actualItem' => $actualItem,
    'arParams' => $arParams,
    'arResult' => $arResult,
    'component' => $component
);

if ($arParams['REVIEWS_MODE'] == 'Y')
{
    Layout\Partial::getInstance()->render('ajax/product_reviews.php', array (
            'arParams' => array (
                'ELEMENT_ID' => $arResult['ID'],
                'OPENNED' => 'Y'
            )
    ), false, false);
}

//@include(__DIR__ . "/fixed_header.php");
?>

<?
$page = $APPLICATION->GetCurPage();
$pos = strpos($page, "catalogprods");
if ($pos === false) {

} else {
    $page = str_replace("catalogprods", "catalog", $page);
    $page = "https://".$_SERVER["HTTP_HOST"].$page;
    LocalRedirect($page, false, "301 Moved permanently");
}
/*$APPLICATION->IncludeComponent(
	"coffeediz:schema.org.Product", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"SHOW" => "Y",
		"NAME" => $actualItem['NAME'],
		"DESCRIPTION" => $arResult['PREVIEW_TEXT'],
		"AGGREGATEOFFER" => "N",
		"PRICE" => $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']]['PRINT_RATIO_PRICE'],
		"PRICECURRENCY" => "RUB",
		"ITEMAVAILABILITY" => "InStock",
		"ITEMCONDITION" => "NewCondition",
		"PAYMENTMETHOD" => array(
			0 => "VISA",
			1 => "MasterCard",
			2 => "ByBankTransferInAdvance",
			3 => "ByInvoice",
			4 => "Cash",
			5 => "COD",
		),
		"PARAM_RATING_SHOW" => "N"
	),
	false
);*/

/*echo "<pre>";
print_r($arResult['PROPERTIES']);
echo "</pre>";*/

/*echo "<pre>";
print_r($arResult['DISPLAY_PROPERTIES']);
echo "</pre>";

echo "<pre>";
print_r($arParams['MAIN_BLOCK_PROPERTY_CODE']);
echo "</pre>";*/
/*
echo "<pre>";
print_r($arResult['DISPLAY_PROPERTIES']);
echo "</pre>";*/
?>

<? if ($arParams['REVIEWS_MODE'] == 'Y'):?><!--noindex--><noindex><?endif;?>

    <div class="item-detail-content" id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">
		<div class="img-block<?=(!is_array($actualItem['PICTURES']) || count($actualItem['PICTURES']) <= 1) ? ' disabled-slider' : ''?>" id="<?=$itemIds['BIG_SLIDER_ID']?>">
			<div class="thumbs">
				<!-- slider/thumbs.php -->
				<? Layout\Partial::getInstance(__DIR__)->render('slider/thumbs.php', $arPartialData, false, false) ?>
				<!-- end: slider/thumbs.php -->
			</div>
			<div class="image-wrap">
				<div class="image">
					<!-- slider/images.php -->
					<? Layout\Partial::getInstance(__DIR__)->render('slider/images.php', $arPartialData, false, false) ?>
					<!-- end: slider/images.php -->
				</div>
			</div>
			
			<!-- labels.php -->
            <? Layout\Partial::getInstance(__DIR__)->render('labels.php', $arPartialData, false, false) ?>
            <!-- end: labels.php -->

            <!-- stock.php -->
            <? Layout\Partial::getInstance(__DIR__)->render('stock.php', $arPartialData, false, false) ?>
            <!-- end: stock.php -->
		</div>

		<div class="sidebar-block">

			<!-- item_code.php -->
			<? Layout\Partial::getInstance(__DIR__)->render('item_code.php', $arPartialData, false, false) ?>
			<!-- end: item_code.php -->
                        
            <!-- timer.php -->
			<? Layout\Partial::getInstance(__DIR__)->render('timer.php', $arPartialData, false, false) ?>
			<!-- end: timer.php -->
			
			<!-- price.php -->
			<? Layout\Partial::getInstance(__DIR__)->render('price.php', $arPartialData, false, false) ?>
			<!-- end: price.php -->
			
			<?
			if ($actualItem['CAN_BUY'] && Options\Base::getInstance(SITE_ID)->getValue('buyoneclickActive'))
			{
				?><a href="javascript:void(0)" onclick="window.Alpha.Popups.openAjaxContent({url: '<?=SITE_DIR?>include/ajax/buy_one_click.php?id=<?=$actualItem['ID']?>'})" class="one-click-buy font-size-button-large"><?=Loc::getMessage('CT_BCE_CATALOG_BUY_ONE_CLICK')?></a><?
			}
			?>
				
			<!-- complect.php -->
			<?
			if (isset($arResult['SET_ITEMS']) && !empty($arResult['SET_ITEMS']))
			{
				Layout\Partial::getInstance(__DIR__)->render('complect.php', $arPartialData, false, false);
			}
			?>
			<!-- end: complect.php -->
			
			<!-- sku.php -->
			<?
			if ($haveOffers)
			{
				Layout\Partial::getInstance(__DIR__)->render('sku.php', $arPartialData, false, false);
			}
			?>
			<!-- end: sku.php -->
			
			<!-- buttons.php -->
			<? Layout\Partial::getInstance(__DIR__)->render('buttons.php', $arPartialData, false, false) ?>
			<!-- end: buttons.php -->

			<!-- delivery.php -->
			<?
            if (Options\Base::getInstance(SITE_ID)->getValue('productDeliveryActive'))
            {
                Layout\Ajax::getInstance()->renderLazyLoad('ajax/delivery.php', array (
                    'elementId' => $arResult['ID'],
                    'arParams' => $arResult['ORIGINAL_PARAMETERS']
                ), false, false);
            }
            ?>
			<!-- end: delivery.php -->
			
			<!-- gifts.php -->
			<?
			if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
			{
				CBitrixComponent::includeComponentClass('bitrix:sale.products.gift');
				Layout\Partial::getInstance(__DIR__)->render('gifts.php', $arPartialData, false, false);
			}
			?>
			<!-- end: gifts.php -->
			
			<!-- preview_text.php -->
			<?
			if (!empty($arResult['PREVIEW_TEXT']))
			{
			Layout\Partial::getInstance(__DIR__)->render('preview_text.php', $arPartialData, false, false);
			}
			?>
			<!-- end: preview_text.php -->

			<!-- full_properties.php -->
			<? Layout\Partial::getInstance(__DIR__)->render('full_properties.php', $arPartialData, false, false) ?>
			<!-- end: full_properties.php -->

            <!--  detail_text.php -->
            <?
//            Layout\Partial::getInstance(__DIR__)->render('detail_text.php', $arPartialData, false, false);
            ?>
            <!-- end:  detail_text.php -->

        <!--</div>-->
            <!-- sku_table.php -->
            <?
            if ($haveOffers)
            {
                //Layout\Partial::getInstance(__DIR__)->render('sku_table.php', $arPartialData, false, false);
            }
            ?>
            <!-- end: sku_table.php -->

        <!--<div class="sidebar-block">-->
			
			<div class="rating-brand-container">
			<!-- rating.php -->
			<?
			if (Options\Base::getInstance(SITE_ID)->getValue('catalogReviews'))
			{
				Layout\Partial::getInstance(__DIR__)->render('rating.php', $arPartialData, false, false);
			}
			?>
			<!-- end: rating.php -->

			<!-- brand.php -->
			<?
			if ($arParams['BRAND_USE'] === 'Y')
			{
				Layout\Partial::getInstance(__DIR__)->render('brand.php', $arPartialData, false, false);
			}
			?>
			<!-- end: brand.php -->
			</div>
                        
		</div>
		<div class="clear" style="clear: both"></div>
        <div>
            <!-- similar_items.php -->
            <? Layout\Ajax::getInstance()->renderLazyLoad('ajax/similar_products.php', array (
                'elementId' => $arResult['ID'],
                'arParams' => $arResult['ORIGINAL_PARAMETERS']
            ), false, false) ?>
            <!-- end: similar_items.php -->
        </div>
       
        <meta itemprop="name" content="<?=$name?>" />
	<meta itemprop="category" content="<?=$arResult['CATEGORY_PATH']?>" />
	<?
	if ($haveOffers)
	{
		foreach ($arResult['JS_OFFERS'] as $offer)
		{
			$currentOffersList = array();

			if (!empty($offer['TREE']) && is_array($offer['TREE']))
			{
				foreach ($offer['TREE'] as $propName => $skuId)
				{
					$propId = (int)substr($propName, 5);

					foreach ($skuProps as $prop)
					{
						if ($prop['ID'] == $propId)
						{
							foreach ($prop['VALUES'] as $propId => $propValue)
							{
								if ($propId == $skuId)
								{
									$currentOffersList[] = $propValue['NAME'];
									break;
								}
							}
						}
					}
				}
			}

			$offerPrice = $offer['ITEM_PRICES'][$offer['ITEM_PRICE_SELECTED']];
			?>
			<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="sku" content="<?=htmlspecialcharsbx(implode('/', $currentOffersList))?>" />
			<meta itemprop="price" content="<?=$offerPrice['RATIO_PRICE']?>" />
			<meta itemprop="priceCurrency" content="<?=$offerPrice['CURRENCY']?>" />
			<link itemprop="availability" href="http://schema.org/<?=($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
                        </span>
			<?
		}

		unset($offerPrice, $currentOffersList);
	}
	else
	{
		?>
		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<meta itemprop="price" content="<?=$price['RATIO_PRICE']?>" />
		<meta itemprop="priceCurrency" content="<?=$price['CURRENCY']?>" />
		<link itemprop="availability" href="http://schema.org/<?=($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
                </span>
		<?
	}
	
	if ($haveOffers)
	{
		$offerIds = array();
		$offerCodes = array();

		$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

		foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
		{
			$offerIds[] = (int)$jsOffer['ID'];
			$offerCodes[] = $jsOffer['CODE'];

			$fullOffer = $arResult['OFFERS'][$ind];
			$measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

			$strAllProps = '';
			$strMainProps = '';
			$strPriceRangesRatio = '';
			$strPriceRanges = '';

			if ($arResult['SHOW_OFFERS_PROPS'])
			{
				if (!empty($jsOffer['DISPLAY_PROPERTIES']))
				{
					foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
					{
                                            $current = '<div class="char">';
                                            $current .= '<div class="char-name"><span><span itemprop="name">'.$property['NAME'].'</span></span></div>';
                                            $current .= '<div class="char-value"><span itemprop="value">'.(is_array($property['VALUE']) ? implode(' / ', $property['VALUE']) : $property['VALUE']).'</span></div>';
                                            $current .= '</div>';
        
                                            $strAllProps .= $current;

                                            if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
                                            {
                                                $strMainProps .= $current;
                                            }
					}

					unset($current);
				}
			}

			if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
			{
				$strPriceRangesRatio = '('.Loc::getMessage(
						'CT_BCE_CATALOG_RATIO_PRICE',
						array('#RATIO#' => ($useRatio
								? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
								: '1'
							).' '.$measureName)
					).')';

				foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
				{
					if ($range['HASH'] !== 'ZERO-INF')
					{
						$itemPrice = false;

						foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
						{
							if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
							{
								break;
							}
						}

						if ($itemPrice)
						{
							$strPriceRanges .= '<dt>'.Loc::getMessage(
									'CT_BCE_CATALOG_RANGE_FROM',
									array('#FROM#' => $range['SORT_FROM'].' '.$measureName)
								).' ';

							if (is_infinite($range['SORT_TO']))
							{
								$strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
							}
							else
							{
								$strPriceRanges .= Loc::getMessage(
									'CT_BCE_CATALOG_RANGE_TO',
									array('#TO#' => $range['SORT_TO'].' '.$measureName)
								);
							}

							$strPriceRanges .= '</dt><dd>'.($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']).'</dd>';
						}
					}
				}

				unset($range, $itemPrice);
			}

			$jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
			$jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
			$jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
			$jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
		}

		$templateData['OFFER_IDS'] = $offerIds;
		$templateData['OFFER_CODES'] = $offerCodes;
		unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

		$jsParams = array(
			'CONFIG' => array(
				'USE_CATALOG' => $arResult['CATALOG'],
				'AJAX_MODE' => $arParams['AJAX_MODE'],
				'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
				'SHOW_PRICE' => true,
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
				'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
				'OFFER_GROUP' => $arResult['OFFER_GROUP'],
				'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
				'MAIN_PICTURE_MODE_ZOOM' => $arParams['DETAIL_PICTURE_MODE_ZOOM'],
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
				'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
				'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
				'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
				'USE_STICKERS' => true,
				'USE_SUBSCRIBE' => $showSubscribe,
				'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
				'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
				'ALT' => $alt,
				'TITLE' => $title,
				'MAGNIFIER_ZOOM_PERCENT' => 200,
				'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
				'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
				'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
					? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
					: null,
                'STORES' => $arParams["STORES"],
                'USE_STORE' => $arParams["USE_STORE"],
                'FIELDS' => $arParams["FIELDS"],
                'SHOW_EMPTY_STORE' => $arParams["SHOW_EMPTY_STORE"],
                'USE_MIN_AMOUNT' => $arParams["USE_MIN_AMOUNT"],
                'SHOW_GENERAL_STORE_INFORMATION' => $arParams["SHOW_GENERAL_STORE_INFORMATION"],
                'MIN_AMOUNT' => $arParams["MIN_AMOUNT"],
                'MAIN_TITLE' => $arParams['MAIN_TITLE']
			),
			'PRODUCT_TYPE' => $arResult['PRODUCT']['TYPE'],
			'VISUAL' => $itemIds,
			'DEFAULT_PICTURE' => array(
				'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
				'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
			),
			'PRODUCT' => array(
				'ID' => $arResult['ID'],
				'ACTIVE' => $arResult['ACTIVE'],
				'NAME' => $arResult['~NAME'],
				'CATEGORY' => $arResult['CATEGORY_PATH']
			),
			'BASKET' => array(
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'BASKET_URL' => $arParams['BASKET_URL'],
				'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
				'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
                'WISHLIST_PAGE_URL' => $arParams['WISHLIST_PAGE_URL'],
                'ADD_WISH_TO_DELAYED' => Options\Base::getInstance(SITE_ID)->getValue('wishListBasketEnabled') ? "Y" : "N",
                'WISHLIST_URL_TEMPLATE' => Tools\Wishlist::getChangeUrl()
			),
			'OFFERS' => $arResult['JS_OFFERS'],
			'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
			'TREE_PROPS' => $skuProps
		);
	}
	else
	{
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
		{
			?>
			<div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
				<?
				if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
				{
					foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
					{
						?>
						<input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
						<?
						unset($arResult['PRODUCT_PROPERTIES'][$propId]);
					}
				}

				$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
				if (!$emptyProductProperties)
				{
					?>
					<table>
						<?
						foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
						{
							?>
							<tr>
								<td><?=$arResult['PROPERTIES'][$propId]['NAME']?></td>
								<td>
									<?
									if (
										$arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
										&& $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
									)
									{
										foreach ($propInfo['VALUES'] as $valueId => $value)
										{
											?>
											<label>
												<input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]"
													value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"checked"' : '')?>>
												<?=$value?>
											</label>
											<br>
											<?
										}
									}
									else
									{
										?>
										<select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]">
											<?
											foreach ($propInfo['VALUES'] as $valueId => $value)
											{
												?>
												<option value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"selected"' : '')?>>
													<?=$value?>
												</option>
												<?
											}
											?>
										</select>
										<?
									}
									?>
								</td>
							</tr>
							<?
						}
						?>
					</table>
					<?
				}
				?>
			</div>
			<?
		}

		$jsParams = array(
			'CONFIG' => array(
				'USE_CATALOG' => $arResult['CATALOG'],
				'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
				'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
                'AJAX_MODE' => $arParams['AJAX_MODE'],
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
				'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
                'MAIN_PICTURE_MODE_ZOOM' => $arParams['DETAIL_PICTURE_MODE_ZOOM'],
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
				'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
				'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
				'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
				'USE_STICKERS' => true,
				'USE_SUBSCRIBE' => $showSubscribe,
				'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
				'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
				'ALT' => $alt,
				'TITLE' => $title,
				'MAGNIFIER_ZOOM_PERCENT' => 200,
				'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
				'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
				'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
					? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
					: null,
                'STORES' => $arParams["STORES"],
                'USE_STORE' => $arParams["USE_STORE"],
                'FIELDS' => $arParams["FIELDS"],
                'SHOW_EMPTY_STORE' => $arParams["SHOW_EMPTY_STORE"],
                'USE_MIN_AMOUNT' => $arParams["USE_MIN_AMOUNT"],
                'SHOW_GENERAL_STORE_INFORMATION' => $arParams["SHOW_GENERAL_STORE_INFORMATION"],
                'MIN_AMOUNT' => $arParams["MIN_AMOUNT"],
                'MAIN_TITLE' => $arParams['MAIN_TITLE']
			),
			'VISUAL' => $itemIds,
			'PRODUCT_TYPE' => $arResult['PRODUCT']['TYPE'],
			'PRODUCT' => array(
				'ID' => $arResult['ID'],
				'ACTIVE' => $arResult['ACTIVE'],
				'PICT' => reset($arResult['MORE_PHOTO']),
				'NAME' => $arResult['~NAME'],
				'SUBSCRIPTION' => true,
				'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
				'ITEM_PRICES' => $arResult['ITEM_PRICES'],
				'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
				'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
				'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
				'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
				'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
				'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
				'SLIDER' => $arResult['MORE_PHOTO'],
				'CAN_BUY' => $arResult['CAN_BUY'],
				'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
				'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
				'MAX_QUANTITY' => $arResult['PRODUCT']['QUANTITY'],
				'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
				'CATEGORY' => $arResult['CATEGORY_PATH'],
                                'CURRENT_DISCOUNT_ACTIVE_TO' => $arResult['CURRENT_DISCOUNT_ACTIVE_TO']
			),
			'BASKET' => array(
				'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'EMPTY_PROPS' => $emptyProductProperties,
				'BASKET_URL' => $arParams['BASKET_URL'],
				'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
                'WISHLIST_PAGE_URL' => $arParams['WISHLIST_PAGE_URL'],
                'ADD_WISH_TO_DELAYED' => Options\Base::getInstance(SITE_ID)->getValue('wishListBasketEnabled') ? "Y" : "N",
                'WISHLIST_URL_TEMPLATE' => Tools\Wishlist::getChangeUrl()
			)
		);
		unset($emptyProductProperties);
	}

	if ($arParams['DISPLAY_COMPARE'])
	{
		$jsParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	?>
        
    </div>
    
    <? if ($arParams['REVIEWS_MODE'] == 'Y'):?></noindex><!--/noindex--><?endif;?>

<script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>',
                
        CT_BCE_TIMER_DAYS_1: '<?=GetMessageJS('CT_BCE_TIMER_DAYS_1')?>',
        CT_BCE_TIMER_DAYS_2: '<?=GetMessageJS('CT_BCE_TIMER_DAYS_2')?>',
        CT_BCE_TIMER_DAYS_5: '<?=GetMessageJS('CT_BCE_TIMER_DAYS_5')?>',

        CT_BCE_TIMER_HOURS_1: '<?=GetMessageJS('CT_BCE_TIMER_HOURS_1')?>',
        CT_BCE_TIMER_HOURS_2: '<?=GetMessageJS('CT_BCE_TIMER_HOURS_2')?>',
        CT_BCE_TIMER_HOURS_5: '<?=GetMessageJS('CT_BCE_TIMER_HOURS_5')?>',

        CT_BCE_TIMER_MINUTES_1: '<?=GetMessageJS('CT_BCE_TIMER_MINUTES_1')?>',
        CT_BCE_TIMER_MINUTES_2: '<?=GetMessageJS('CT_BCE_TIMER_MINUTES_2')?>',
        CT_BCE_TIMER_MINUTES_5: '<?=GetMessageJS('CT_BCE_TIMER_MINUTES_5')?>',

        CT_BCE_TIMER_SECONDS_1: '<?=GetMessageJS('CT_BCE_TIMER_SECONDS_1')?>',
        CT_BCE_TIMER_SECONDS_2: '<?=GetMessageJS('CT_BCE_TIMER_SECONDS_2')?>',
        CT_BCE_TIMER_SECONDS_5: '<?=GetMessageJS('CT_BCE_TIMER_SECONDS_5')?>',

        IZOOMIFY_PLUGIN: "<?= SITE_TEMPLATE_PATH ?>/vendor/hover_touch/jquery.izoomify.js",
        LIGHTBOX_PLUGIN: "<?= SITE_TEMPLATE_PATH ?>/vendor/lightbox/lightbox.min.js",

	});

    BX.ready(function () {
        window.<?=$obName?> = new window.Alpha.CatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
    });
</script>

<?
unset($actualItem, $itemIds, $jsParams, $arPartialData);
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;

/*
<div data-entity="tab-content" data-value="custom-tab-2" class="block">
    <h5>Custom tab 2</h5>
    <p>Text custom tab</p>
</div>
 */
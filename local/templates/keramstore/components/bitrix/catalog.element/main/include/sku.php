<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;


if ($haveOffers && !empty($arResult['OFFERS_PROP']))
{
?>
<div id="<?=$itemIds['TREE_ID']?>" class="sku-container">
    <?
    foreach ($arResult['SKU_PROPS'] as $skuProperty)
    {
        if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
            continue;
        
        $propertyId = $skuProperty['ID'];
        $skuProps[] = array(
                'ID' => $propertyId,
                'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                'VALUES' => $skuProperty['VALUES'],
                'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
            );
        
        if ($skuProperty['SHOW_MODE'] === 'PICT')
        {
            ?>
            <div class="images-sku-block" data-entity="sku-line-block" data-entity-id="images-sku-block-<?=$propertyId?>">
                <div class="title font-title"><?=htmlspecialcharsEx($skuProperty['NAME'])?></div>
                <div class="images-sku-list">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?
                            foreach ($skuProperty['VALUES'] as &$value)
                            {
                                $value['NAME'] = htmlspecialcharsbx($value['NAME']);
                                ?>
                                <div class="swiper-slide">
                                    <a href="javascript:void(0)" title="<?=$value['NAME']?>" data-entity="sku-value" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>" class="item" style="background-image: url('<?=$value['PICT']['SRC']?>');"></a>
                                </div>
                                <?
                            }												
                            ?>
                        </div>
                    </div>
                    <div class="swiper-arrows">
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
            <script>
            BX.ready(function () {
                new Swiper('[data-entity-id="images-sku-block-<?=$propertyId?>"] .swiper-container', {
                    slidesPerView: "auto",
                    spaceBetween: 8,
                    freeMode: true,

                    breakpoints: {
                        1024: {
                            navigation: {
                                nextEl: '[data-entity-id="images-sku-block-<?=$propertyId?>"] .swiper-button-next',
                                prevEl: '[data-entity-id="images-sku-block-<?=$propertyId?>"] .swiper-button-prev',
                            },
                        }
                    }
                });
            });
            </script>
            <?
        }
        else
        {
            ?>
            <div class="sku-list" data-entity="sku-line-block">
                <div class="title font-title"><?=htmlspecialcharsEx($skuProperty['NAME'])?></div>
                <div class="list">
                    <?
                    foreach ($skuProperty['VALUES'] as &$value)
                    {
                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);
                        ?><a href="javascript:void()" data-entity="sku-value" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>" class="item sku-element selected"><?=$value['NAME']?></a><?
                    }
                    ?>
                </div>
            </div>
            <?
        }
    }
    ?>
</div>

<?
}
?>
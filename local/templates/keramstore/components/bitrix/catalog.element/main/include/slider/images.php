<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

if ((is_array($actualItem['PICTURES']) && count($actualItem['PICTURES']) == 1) || empty($actualItem['PICTURES']))
{
    $arPicture = !empty($actualItem['PICTURES']) ? end($actualItem['PICTURES']) : array('SRC' => '');
    ?>
    <div class="image-wrapper"><?=Layout\Assets::showImage($arPicture['SRC']);?><link href="<?=$arPicture['SRC']?>" itemprop="image"/></div>  
    <?
}
elseif (is_array($actualItem['PICTURES']) && count($actualItem['PICTURES']) > 1)
{
    ?>
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?
            foreach ($actualItem['PICTURES'] as $arPicture)
            {
                ?>
                <div class="swiper-slide">
                    <div class="item">
                        <div class="image-wrapper">
                            <? if ($arPicture['TYPE'] == "VIDEO"): ?>
                                <div class="video-wrapper js-video" onclick="window.Alpha.VideoControl($(this));">
                                    <div class="video-button"></div>
                                    <? $arVideoParams["attributes"] = array('preload="metadata"', 'loop="loop"'); ?>
                                    <?=Layout\Assets::showVideo($arPicture['SRC'], $arVideoParams);?>
                                </div>
                            <? else: ?>
                                <?=Layout\Assets::showImage($arPicture['SRC'], [], false, true);?>
                                <link href="<?=$arPicture['SRC']?>" itemprop="image"/>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <?
}
?>

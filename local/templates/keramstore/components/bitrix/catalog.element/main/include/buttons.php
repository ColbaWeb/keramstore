<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;

$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['PRODUCT']['SUBSCRIBE'] === 'Y' || $haveOffers || !$arResult['ITEM_PRICES'][0]['PRICE']);

?>
<div class="buttons">
    <?
    if ($arParams['USE_PRODUCT_QUANTITY'])
    {
        ?>
        <div class="counter" data-entity="quantity-block" <?= (!$actualItem['CAN_BUY'] ? ' style="display: none;"' : '') ?>>
            <a href="javascript:void(0)" id="<?=$itemIds['QUANTITY_DOWN_ID']?>" class="minus"><i class="icon icon-bx-minus"></i></a>
            <input type="number" id="<?=$itemIds['QUANTITY_ID']?>" value="<?=$price['MIN_QUANTITY']?>">
            <span class="measure" id="<?=$itemIds['QUANTITY_MEASURE']?>"><?=$actualItem['ITEM_MEASURE']['TITLE']?></span>
            <a href="javascript:void(0)" id="<?=$itemIds['QUANTITY_UP_ID']?>" class="plus"><i class="icon icon-bx-plus"></i></a>
        </div>
        <?
    }
    ?>
    <div class="buttons-wrap" data-entity="main-button-container">
        <div class="buttons-buy" id="<?=$itemIds['BASKET_ACTIONS_ID']?>" <?=($actualItem['CAN_BUY'] ? '' : ' style="display: none;"')?>>
            <?
            if ($showAddBtn)
            {
                ?><a href="javascript:void(0)" class="btn btn-primary btn-large btn-to-cart" id="<?=$itemIds['ADD_BASKET_LINK']?>"><?=$arParams['MESS_BTN_ADD_TO_BASKET']?> <i class="icon icon-basket-white"></i></a><?
            }
            
            if ($showBuyBtn)
            {
                ?><a href="javascript:void(0)" id="<?=$itemIds['BUY_LINK']?>" class="btn btn-primary btn-large btn-to-cart"><?=$arParams['MESS_BTN_BUY']?> <i class="icon icon-basket-white"></i></a><?
            }
            ?>
        </div>
        <?
        if ($showSubscribe)
        {
                $APPLICATION->IncludeComponent(
                        'bitrix:catalog.product.subscribe',
                        'main',
                        array(
                            'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                            'PRODUCT_ID' => $arResult['ID'],
                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                            'BUTTON_CLASS' => 'btn btn-primary btn-large btn-subscribe mt-3',
                            'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                );
        }

        if (Options\Base::getInstance(SITE_ID)->getValue('wishList') && ($actualItem['CAN_BUY'] || !Options\Base::getInstance(SITE_ID)->getValue('wishListBasketEnabled')))
        {
            ?><a href="javascript:void(0)" id="<?=$itemIds['WISHLIST_ID']?>" class="btn btn-to-favorites"><i class="icon icon-bx-heart"></i></a><?
        }
        ?>
        
        
    </div>
    
</div>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;


$APPLICATION->IncludeComponent(
        'nextype:alpha.catalog.brandblock',
        'main',
        array(
            'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ELEMENT_ID' => $arResult['ID'],
            'ELEMENT_CODE' => '',
            'PROP_CODE' => $arParams['BRAND_PROP_CODE'],
            'URL_TEMPLATE' => $arParams['BRAND_URL_TEMPLATE'],
            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
            'CACHE_TIME' => $arParams['CACHE_TIME'],
            'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
            'SHOW_DEACTIVATED' => 'Y',
            'SHOW_ELEMENTS_COUNT' => 'Y',
            'WIDTH' => '',
            'HEIGHT' => ''
        ),
        $component,
        array('HIDE_ICONS' => 'Y')
);

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

?>

<div class="timer-container" id="<?=$itemIds['DISCOUNT_TIMER_ID']?>">
    <div class="item">
        <span class="time-counter" data-entity="days-value"></span>
        <span class="measure-unit" data-entity="days-label"></span>
    </div>
    <div class="item">
        <span class="time-counter" data-entity="hours-value"></span>
        <span class="measure-unit" data-entity="hours-label"></span>
    </div>
    <div class="item">
        <span class="time-counter" data-entity="minutes-value"></span>
        <span class="measure-unit" data-entity="minutes-label"></span>
    </div>
    <div class="item">
        <span class="time-counter" data-entity="seconds-value"></span>
        <span class="measure-unit" data-entity="seconds-label"></span>
    </div>
</div>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

?>
<div class="complect">
    <div class="font-title complect-title"><?=Loc::getMessage('CT_BCE_CATALOG_COMPLECT_BLOCK_TITLE')?></div>
    <div class="list">
        <?
        foreach ($arResult['SET_ITEMS'] as $item)
        {
        ?>
        <div class="item">
            <?
                echo Layout\Assets::showBackgroundImage($item['PREVIEW_PICTURE']['SRC'], array(
                "attributes" => array(
                    "class=\"img\""
                    )
                ));
            ?>
            
            <div class="info">
                <?
                if (isset($item['MIN_PRICE']) && !empty($item['MIN_PRICE']['DISCOUNT_PRICE']))
                {
                    ?>
                    <div class="price-block">
                        <div class="price font-body-2 bold"><?=$item['MIN_PRICE']['DISCOUNT_PRICE_PRINT']?></div>
                        
                        <div class="price-wrap">
                            
                            <?
                            if ($item['MIN_PRICE']['DISCOUNT_PRICE'] != $item['MIN_PRICE']['PRICE'])
                            {
                                ?>
                                <div class="font-body-small price-old-block"><span class="price-old"><?=$item['MIN_PRICE']['PRICE_PRINT']?></span></div>
                                <div class="labels">
                                    <span class="label action">-<?=$item['DISCOUNT_PERCENT']?>%</span>
                                </div>
                                <?
                            }
                            ?>
                            <div class="count font-body-small"><?=$item['QUANTITY'] . " " . $item['MEASURE']?></div>
                        </div>
                    </div>
                    <?
                }
                ?>
                
                <a href="<?=$item['DETAIL_PAGE_URL']?>" target="_blank" class="font-title-small item-title">
                    <?=$item['NAME']?>
                </a>
            </div>
        </div>
        <?
        }
        ?>
        
    </div>
</div>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

/*echo "<pre>";
print_r($arResult['DISPLAY_PROPERTIES']);
echo "</pre>";

echo "<pre>";
print_r($arParams['MAIN_BLOCK_PROPERTY_CODE']);
echo "</pre>";*/


$showOffersBlock = $haveOffers && !empty($arResult['OFFERS_PROP']);
$mainBlockProperties = array_intersect_key($arResult['DISPLAY_PROPERTIES'], $arParams['MAIN_BLOCK_PROPERTY_CODE']);
$showPropsBlock = !empty($mainBlockProperties) || $arResult['SHOW_OFFERS_PROPS'];

if ($showPropsBlock)
{
    if (!empty($mainBlockProperties))
    {
        ?>
        <div class="chars">
            <div class="chars-title font-title"><?=Loc::getMessage('CT_BCE_CATALOG_PROPERTIES')?></div>
            <div class="props">
            <? foreach ($mainBlockProperties as $property)
            {
                ?>
                <div itemprop="additionalProperty" itemscope="" itemtype="http://schema.org/PropertyValue" class="char">
                    <div class="char-name"><span><span itemprop="name"><?=$property['NAME']?></span></span></div>
                    <div class="char-value"><span itemprop="value"><?=(is_array($property['DISPLAY_VALUE']) ? implode(' / ', $property['DISPLAY_VALUE']) : $property['DISPLAY_VALUE'])?></span></div>
                </div>
                <?
            }
            ?>
            </div>
            
            <?
            if (!empty($arResult['DISPLAY_PROPERTIES']))
            {
                ?><a href="javascript:void(0)" class="chars-link dotted-link font-large-button"><span data-entity="tab-link" data-value="properties"><?=Loc::getMessage('CT_BCE_CATALOG_ALL_PROPERTIES')?></span><i class="icon icon-arrow-right-text-button"></i></a><?
            }
            ?>
        </div>
        <?
    }
    
    if ($arResult['SHOW_OFFERS_PROPS'])
    {
        ?>
            <div class="chars" id="<?=$itemIds['DISPLAY_MAIN_PROP_DIV']?>">
                <div class="chars-title font-title"><?=Loc::getMessage('CT_BCE_CATALOG_PROPERTIES')?></div>
                <div class="props" data-entity="props-list"></div>
            </div>
        <?
    }
}
?>
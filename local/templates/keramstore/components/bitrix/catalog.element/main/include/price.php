<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$showDiscount = $price['PERCENT'] > 0;
?>

<div class="price-block">
    <div class="price font-price-large bold" id="<?=$itemIds['PRICE_ID']?>"><?=$price['PRINT_RATIO_PRICE']?></div>
    
    <?
    if ($arParams['SHOW_OLD_PRICE'] === 'Y')
    {
        ?><div class="font-body-small price-old-block"><span class="price-old" <?=($showDiscount ? '' : 'style="display: none;"')?> id="<?=$itemIds['OLD_PRICE_ID']?>"><?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?></span></div><?
    }
    
    if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
    {
        if ($haveOffers)
        {
            ?>
            <div class="labels">
                <span class="label action" style="display: none;" id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>"></span>
            </div>
            <?
        }
        else
        {
            if ($price['DISCOUNT'] > 0)
            {
                ?>
                <div class="labels">
                    <span class="label action" id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>"><?= -$price['PERCENT'] ?>%</span>
                </div>
                <?
            }
        }
    }
    ?>
    
</div>

<?
if ($arParams['USE_PRICE_COUNT'])
{
    $showRanges = !$haveOffers && is_array($actualItem['ITEM_QUANTITY_RANGES']) && count($actualItem['ITEM_QUANTITY_RANGES']) > 1;
    $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
    ?>
    										<div class="price-ranges-block"
    <?= $showRanges ? '' : 'style="display: none;"' ?>
    											data-entity="price-ranges-block">
    <?
    if ($arParams['MESS_PRICE_RANGES_TITLE'])
    {
        ?>
        												<div class="product-item-detail-info-container-title font-title">
        <?= $arParams['MESS_PRICE_RANGES_TITLE'] ?>
        													<span data-entity="price-ranges-ratio-header">
        												(<?=
        (Loc::getMessage(
                'CT_BCE_CATALOG_RATIO_PRICE',
                array('#RATIO#' => ($useRatio ? $measureRatio : '1') . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
        ))
        ?>)
        											</span>
        												</div>
        <?
    }
    ?>
    											<ul class="product-item-detail-properties font-body-2 reset-ul-list" data-entity="price-ranges-body">
    <?
    if ($showRanges)
    {
        foreach ($actualItem['ITEM_QUANTITY_RANGES'] as $range)
        {
            if ($range['HASH'] !== 'ZERO-INF')
            {
                $itemPrice = false;

                foreach ($arResult['ITEM_PRICES'] as $itemPrice)
                {
                    if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
                    {
                        break;
                    }
                }

                if ($itemPrice)
                {
                    ?>
                    																<li class="product-item-detail-properties-item">
                    																<span class="product-item-detail-properties-name">
                    <?
                    echo Loc::getMessage(
                            'CT_BCE_CATALOG_RANGE_FROM',
                            array('#FROM#' => $range['SORT_FROM'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
                    ) . ' ';

                    if (is_infinite($range['SORT_TO']))
                    {
                        echo Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                    }
                    else
                    {
                        echo Loc::getMessage(
                                'CT_BCE_CATALOG_RANGE_TO',
                                array('#TO#' => $range['SORT_TO'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'])
                        );
                    }
                    ?>
                    																</span>
                    																	<span class="product-item-detail-properties-dots"></span>
                    																	<span class="product-item-detail-properties-value bold"><?= ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']) ?></span>
                    																</li>
                    <?
                }
            }
        }
    }
    ?>
    											</ul>
    										</div>
    <?
    unset($showRanges, $useRatio, $itemPrice, $range);
}
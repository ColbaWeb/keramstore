<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

/*echo "<pre>";
print_r($arResult['DISPLAY_PROPERTIES']);
echo "</pre>";*/


if (!empty($arResult['DISPLAY_PROPERTIES']))
{
?>
    <div class="chars">
        <div class="props">
        <?
        foreach ($arResult['DISPLAY_PROPERTIES'] as $property)
        {
            if(isset($property['DISPLAY_VALUE'])) {
                $value = (is_array($property['DISPLAY_VALUE']) ? implode(' / ', $property['DISPLAY_VALUE']) : $property['DISPLAY_VALUE']);
            } else {
                $value = $property['VALUE'];
            }

        ?>
        <div itemprop="additionalProperty" itemscope="" itemtype="http://schema.org/PropertyValue" class="char">
            <div class="char-name"><span><span itemprop="name"><?= $property['NAME'] ?></span></span></div>
            <div class="char-value"><span itemprop="value"><?= $value ?></span></div>
        </div>
        <?
        }
        ?>
        </div>
        <?
        if ($arResult['SHOW_OFFERS_PROPS'])
        {
            ?>
            <div class="props">
                <div id="<?= $itemIds['DISPLAY_PROP_DIV'] ?>"></div>
            </div>
            <?
        }
        ?>
    </div>
    <?
}


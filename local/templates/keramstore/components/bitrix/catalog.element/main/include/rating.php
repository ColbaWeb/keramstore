<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid\Declension;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;

if (Options\Base::getInstance(SITE_ID)->getValue('catalogReviews'))
{
    $circleClass = "empty";
    $summary = Reviews\Base::getInstance()->getSummaryByProduct($arResult['ID']);
    
    if (isset($summary['avg']) && $summary['avg'] > 0)
    {
        if ($summary['avg'] <= 0.25)
            $circleClass = "small";
        elseif ($summary['avg'] > 0.25 && $summary['avg'] <= 3.5)
            $circleClass = "medium";
        elseif ($summary['avg'] > 3.5 && $summary['avg'] <= 4.5)
            $circleClass = "large";
        else
            $circleClass = "full";
    }
    
    ?>

    <div class="rating-circle-block">
        <div class="circle h3 <?=$circleClass?>">
            <?=(isset($summary['avg']) && $summary['avg'] > 0) ? number_format($summary['avg'], 1, ",", " ") : "0" ?>
        </div>
        <div class="title-block">
            <?
            if (isset($summary['votes']) && $summary['votes'] > 0)
            {
                $itemDeclension = new Declension(Loc::getMessage('CT_BCE_CATALOG_REVIEWS_COUNT_DECLENSION_1'), Loc::getMessage('CT_BCE_CATALOG_REVIEWS_COUNT_DECLENSION_2'), Loc::getMessage('CT_BCE_CATALOG_REVIEWS_COUNT_DECLENSION_3'));
                ?>
                <a href="<?=isset($arResult['REVIEWS_DETAIL_URL']) ? $arResult['REVIEWS_DETAIL_URL'] : '#'?>" onclick="if (!!window.obProductReviews<?=$arResult['ID']?>) { event.preventDefault(); window.obProductReviews<?=$arResult['ID']?>.doOpenList(); return false;}" class="chars-link dotted-link font-large-button"><span><?=$summary['votes'] . " " . $itemDeclension->get($summary['votes'])?></span><i class="icon icon-arrow-right-text-button"></i></a>
                
                <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                    <meta itemprop="ratingValue" content="<?=number_format($summary['avg'], 1, ",", "")?>">
                    <meta itemprop="reviewCount" content="<?=$summary['votes']?>">
                </span>
                
                
                <?
            }
            else
            {
                ?>
                <a href="javascript:void(0)" onclick="if (!!window.obProductReviews<?=$arResult['ID']?>) window.obProductReviews<?=$arResult['ID']?>.doOpenForm();" class="chars-link dotted-link font-large-button"><span><?=Loc::getMessage('CT_BCE_CATALOG_MAKE_REVIEW_BUTTON_TITLE')?></span><i class="icon icon-arrow-right-text-button"></i></a>
                <?
            }
            ?>
            
        </div>
    </div>
    <?
}


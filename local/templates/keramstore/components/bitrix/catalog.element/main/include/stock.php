<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;

$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];

if (Options\Base::getInstance(SITE_ID)->getValue('catalogReviews'))
{
    $summaryReviews = Reviews\Base::getInstance()->getSummaryByProduct($arResult['ITEM']['ID']);
}
?>

<div class="label badge-in-stock">
    
    <?
    
    if ($arParams['SHOW_MAX_QUANTITY'] !== 'N')
    {
        if ($haveOffers)
        {
            ?>
            <div class="in-stock<?=(isset($summaryReviews) && $summaryReviews['avg'] > 0) ? ' has-reviews' : ''?>" style="display: none;" id="<?=$itemIds['QUANTITY_LIMIT']?>">
                <?
                if ($arParams['SHOW_MAX_QUANTITY'] != "M")
                    echo $arParams['MESS_SHOW_MAX_QUANTITY'] . ":";
                ?>
                <span data-entity="quantity-limit-value"></span>
            </div>
            <div class="out-stock" id="<?=$itemIds['NOT_AVAILABLE_MESS']?>" <?=($actualItem['CAN_BUY'] ? 'style="display: none;"' : '')?>>
                <?=$arParams['MESS_NOT_AVAILABLE']?>
            </div>
            <?
        }
        else
        {
            if ($measureRatio && (float)$actualItem['PRODUCT']['QUANTITY'] > 0 && $actualItem['CHECK_QUANTITY'])
            {
                ?>
                <div class="in-stock" id="<?=$itemIds['QUANTITY_LIMIT']?>">
                    <?
                    if ($arParams['SHOW_MAX_QUANTITY'] != "M")
                        echo $arParams['MESS_SHOW_MAX_QUANTITY'] . ":";
                    ?>
                    <span data-entity="quantity-limit-value">
                                    <?
                                    if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
                                    {
                                        if ((float) $actualItem['PRODUCT']['QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
                                        {
                                            echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
                                        }
                                        else
                                        {
                                            echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
                                        }
                                    }
                                    else
                                    {
                                        echo $actualItem['PRODUCT']['QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'];
                                    }
                                    ?>
                    </span>
                </div>
                <?
            }
            elseif ($measureRatio && (float)$actualItem['PRODUCT']['QUANTITY'] == 0 && $actualItem['CAN_BUY'])
            {
                echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
            }
            else
            {
                ?><div class="out-stock"><?= $arParams['MESS_NOT_AVAILABLE'] ?></div><?
            }
        }
    }
    
    if (isset($summaryReviews) && $summaryReviews['avg'] > 0)
    {
            ?>
            <div class="rating">
                <i class="icon icon-star-12"></i>
                <div class="value"><?= number_format($summaryReviews['avg'], 1, ",", " ")?></div>
            </div>
            <?
    }
    ?>
    
</div>
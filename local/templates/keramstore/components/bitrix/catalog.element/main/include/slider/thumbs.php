<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

if (is_array($actualItem['PICTURES']) && count($actualItem['PICTURES']) > 1)
{
    ?>
    <div class="thumbs-wrapper">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <? 
                foreach ($actualItem['PICTURES'] as $arPicture)
                {
                    ?>
                    <div class="swiper-slide">
                        <div class="img-wrapper">
                            <? if ($arPicture['TYPE'] == "VIDEO"): ?>
                                <div class="video-thumb-icon"></div>
                            <? endif;
                            $videoClass = $arPicture['TYPE'] == "VIDEO" ? " video-thumb" : "";
                            echo Layout\Assets::showBackgroundImage($arPicture['THUMB'], array(
                                "attributes" => array(
                                    "class=\"img" . $videoClass . "\""
                                )
                            ));
                            ?>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
        <div class="swiper-arrows">
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>
    <?
}
?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
?>
<div class="code-wrapper">
    <? if (!$haveOffers && !empty($actualItem['ITEM_CODE']))
    {
        ?><div class="code font-body-2" id="<?= $itemIds['ITEM_CODE_ID'] ?>"><?= Loc::getMessage('CT_BCE_CATALOG_ARTICLE') ?> <span data-entity="item-code-value"><?= $actualItem['ITEM_CODE'] ?></span></div><?
    }
    elseif ($haveOffers)
    {
        ?><div class="code font-body-2" <? if (empty($actualItem['ITEM_CODE'])): ?>style="display: none"<? endif; ?> id="<?= $itemIds['ITEM_CODE_ID'] ?>"><?= Loc::getMessage('CT_BCE_CATALOG_ARTICLE') ?> <span data-entity="item-code-value"><?= $actualItem['ITEM_CODE'] ?></span></div><?
    }
    ?>
    <? if ($arParams["USE_STORE"] == "Y" && empty($arResult['SET_ITEMS'])): ?>
        <div class="in-store font-body-2" id="<?= $itemIds["STOCK_BLOCK_ID"] ?>">
        <?
        if ($arResult['PRODUCT']['TYPE'] != 3)
        {
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.store.amount",
                "main",
                Array(
                    "STORES" => $arParams["STORES"],
                    "ELEMENT_ID" => $arResult["ID"],
                    "OFFER_ID" => $actualItem["ID"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "MAIN_TITLE" => $arParams["MAIN_TITLE"],
                    "FIELDS" => $arParams["FIELDS"],
                    "SHOW_EMPTY_STORE" => $arParams["SHOW_EMPTY_STORE"],
                    "USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
                    "SHOW_GENERAL_STORE_INFORMATION" => $arParams["SHOW_GENERAL_STORE_INFORMATION"],
                    "MIN_AMOUNT" => $arParams["MIN_AMOUNT"]
                )
            );
        }
        ?>
        </div>
    <? endif; ?>
</div>

<?

if (!empty($actualItem['ITEM_CODE']))
{
    ?><meta itemprop="sku" content="<?=$actualItem['ITEM_CODE']?>"><?
}
?>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

?>

<div class="labels" id="<?=$itemIds['STICKER_ID']?>">
    
    <?
    if (!empty($actualItem['LABEL_ARRAY_VALUE']))
    {
        foreach ($actualItem['LABEL_ARRAY_VALUE'] as $code => $value)
        {
            if (isset($actualItem['PROPERTIES'][$code]['VALUE']) && !empty($actualItem['PROPERTIES'][$code]['VALUE']))
            {
                foreach ($actualItem['PROPERTIES'][$code]['VALUE'] as $labelKey => $labelValue)
                {
                    ?>
                    <div class="label-wrap">
                        <div class="label <?= $actualItem['PROPERTIES'][$code]['VALUE_XML_ID'][$labelKey] ?>"><?= $labelValue ?></div>
                    </div>
                    <?
                }
            }
        }
    }
    ?>
</div>
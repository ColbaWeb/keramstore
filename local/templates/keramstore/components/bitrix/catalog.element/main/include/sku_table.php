<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;


if ($haveOffers && !empty($arResult['OFFERS_PROP']))
{
?>
<div class="sku-table">
    <div class="sku-table-row sku-table-head">
        <div class="sku-table-left">
            <div class="sku-table-cell" data-cell="1">
                <a href="javascript:void(0);">
                    <span>��������</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
            <div class="sku-table-cell" data-cell="2">
                <a href="javascript:void(0);">
                    <span>��������</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
            <div class="sku-table-cell" data-cell="3">
                <a href="javascript:void(0);">
                    <span>�����������</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
            <div class="sku-table-cell" data-cell="4">
                <a href="javascript:void(0);">
                    <span>�������� �����</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
            <div class="sku-table-cell" data-cell="5">
                <a href="javascript:void(0);">
                    <span>�������� �����</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
            <div class="sku-table-cell" data-cell="6">
                <a href="javascript:void(0);">
                    <span>�������� �����</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
        </div>
        <div class="sku-table-right">
            <div class="sku-table-cell" data-cell="7">
                <a href="javascript:void(0);">
                    <span>�������</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
            <div class="sku-table-cell" data-cell="8">
                <a href="javascript:void(0);">
                    <span>����</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
            <div class="sku-table-cell" data-cell="9">
                <a href="javascript:void(0);">
                    <span>������</span>
                    <i class="icon icon-arrow-light-down"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="sku-table-row active">
        <div class="sku-table-left">
            <div class="sku-table-cell sku-table-name" data-cell="1">Gauss LED Elementary Globe 6W E27 2700K</div>
            <div class="sku-table-cell" data-cell="2">
                <div class="sku-table-title">��������</div>
                <div>6 ��</div>
            </div>
            <div class="sku-table-cell" data-cell="3">
                <div class="sku-table-title">�����������</div>
                <div>2700 K</div>
            </div>
            <div class="sku-table-cell" data-cell="4">
                <div class="sku-table-title">�������� �����</div>
                <div>420 ��</div>
            </div>
            <div class="sku-table-cell" data-cell="5">
                <div class="sku-table-title">�������� �����</div>
                <div>420 ��</div>
            </div>
            <div class="sku-table-cell" data-cell="6">
                <div class="sku-table-title">�������� �����</div>
                <div>420 ��</div>
            </div>
        </div>
        <div class="sku-table-right">
            <div class="sku-table-cell" data-cell="7">
                <div class="sku-table-stock">� �������</div>
            </div>
            <div class="sku-table-cell sku-table-price" data-cell="8">100 ?</div>
            <div class="sku-table-cell sku-table-btn" data-cell="9">
                <a href="javascript:void(0)" class="btn btn-primary btn-medium"><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></a>
            </div>
        </div>
    </div>
    <div class="sku-table-row">
        <div class="sku-table-left">
            <div class="sku-table-cell sku-table-name" data-cell="1">Gauss LED Elementary Globe 6W E27 4100K</div>
            <div class="sku-table-cell" data-cell="2">
                <div class="sku-table-title">��������</div>
                <div>Gauss LED Elementary Globe 6W E27 4100K</div>
            </div>
            <div class="sku-table-cell" data-cell="3">
                <div class="sku-table-title">�����������</div>
                <div>Gauss LED Elementary Globe 6W E27 4100K Gauss LED Elementary Globe 6W E27 4100K</div>
            </div>
            <div class="sku-table-cell" data-cell="4">
                <div class="sku-table-title">�������� �����</div>
                <div>Gauss LED Elementary Globe 6W E27 4100K</div>
            </div>
            <div class="sku-table-cell" data-cell="5">
                <div class="sku-table-title">�������� �����</div>
                <div>Gauss LED Elementary Globe 6W E27 4100K</div>
            </div>
            <div class="sku-table-cell" data-cell="6">
                <div class="sku-table-title">�������� �����</div>
                <div>Gauss LED Elementary Globe 6W E27 4100K</div>
            </div>
        </div>
        <div class="sku-table-right">
            <div class="sku-table-cell" data-cell="7">
                <div class="sku-table-stock">��� � �������</div>
            </div>
            <div class="sku-table-cell sku-table-price" data-cell="8">199 999,99 ?</div>
            <div class="sku-table-cell sku-table-btn" data-cell="9">
                <a href="javascript:void(0)" class="btn btn-primary btn-medium"><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></a>
            </div>
        </div>
    </div>
    <div class="sku-table-row">
        <div class="sku-table-left">
            <div class="sku-table-cell sku-table-name" data-cell="1">Gauss LED Elementary Globe 6W E27 2700K</div>
            <div class="sku-table-cell" data-cell="2">
                <div class="sku-table-title">��������</div>
                <div>120 ��</div>
            </div>
            <div class="sku-table-cell" data-cell="3">
                <div class="sku-table-title">�����������</div>
                <div>6500 K</div>
            </div>
            <div class="sku-table-cell" data-cell="4">
                <div class="sku-table-title">�������� �����</div>
                <div>540 ��</div>
            </div>
            <div class="sku-table-cell" data-cell="5">
                <div class="sku-table-title">�������� �����</div>
                <div>2000 ��</div>
            </div>
            <div class="sku-table-cell" data-cell="6">
                <div class="sku-table-title">�������� �����</div>
                <div>420 ��</div>
            </div>
        </div>
        <div class="sku-table-right">
            <div class="sku-table-cell" data-cell="7">
                <div class="sku-table-stock">� �������</div>
            </div>
            <div class="sku-table-cell sku-table-price" data-cell="8">1 999,99 ?</div>
            <div class="sku-table-cell sku-table-btn" data-cell="9">
                <a href="javascript:void(0)" class="btn btn-primary btn-medium"><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></a>
            </div>
        </div>
    </div>
    <div class="sku-table-row sku-table-row-scroll">
        <div class="sku-table-left sku-table-scroll">
            <div class="sku-table-scroll-inner"></div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        //scroll sync
        $('.sku-table-left').scroll(function () {
           $('.sku-table-left').scrollLeft($(this).scrollLeft());
        });

        $(window).on('load resize', function () {

            let row = $('.sku-table .sku-table-row');

            if ($(window).width() > 1024) {

                //cells width
                let rows = {};

                function initRows () {
                    for (let row of $('[data-cell]')) {
                        const code = row.getAttribute('data-cell');
                        getMaxSizes(code, true);
                    }
                }

                function getMaxSizes (code) {
                    rows[code] = getMaxOfArray(Array.from($(`[data-cell=${code}]`)).map((elem) => elem.offsetWidth));
                }

                function getMaxOfArray (numArray) {
                    return Math.max.apply(null, numArray);
                }

                function setSizes () {
                    for (let code in rows) {
                        for (let row of $(`[data-cell=${code}]`)) {
                            row.style.width = '';
                        }
                        getMaxSizes(code);
                        for (let row of $(`[data-cell=${code}]`)) {
                            row.style.width = rows[code] + 'px';
                        }
                    }
                }

                //row parts width
                function calcPartsWidth () {

                    row.each(function () {

                        let $this = $(this);
                        let rowLeft = $this.find('.sku-table-left');
                        let rowRight= $this.find('.sku-table-right');

                        rowRight.each(function () {
                            let rowRightCell = $(this).find('.sku-table-cell');
                            let rowRightWidth = 0;

                            rowRightCell.each(function() {
                                rowRightWidth += $(this).outerWidth();
                            });

                            $(this).width(rowRightWidth);
                            rowLeft.width($this.outerWidth() - rowRight.width());
                        });
                    });

                    let rowScroll = $('.sku-table-scroll');
                    let rowScrollInner = rowScroll.find('.sku-table-scroll-inner');

                    rowScroll.width($('.sku-table-row:first-of-type .sku-table-left').width());

                    let rowLeftCell = $('.sku-table-row:first-of-type .sku-table-left').find('.sku-table-cell');
                    let rowRightInnerWidth = 0;

                    rowLeftCell.each(function () {
                        rowRightInnerWidth += $(this).outerWidth();
                    });

                    rowScrollInner.width(rowRightInnerWidth);
                }

                initRows();
                setSizes();
                calcPartsWidth();
            }

            else {
                $('.sku-table-left').removeAttr('style');
                $('.sku-table-right').removeAttr('style');
                $('.sku-table-cell').removeAttr('style');
                $('.sku-table-scroll-inner').removeAttr('style');
            }
        });
    });
</script>
<?
}
?>

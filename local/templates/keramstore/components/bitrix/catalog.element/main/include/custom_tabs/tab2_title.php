<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;

/*
<a class="anchor" href="javascript:void(0)" data-entity="tab-link" data-value="custom-tab-2">
    <div class="icon icon-arrow-right"></div> Custom tab 2
</a>
 */
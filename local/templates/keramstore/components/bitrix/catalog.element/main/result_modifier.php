<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Tools;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

if (!Loader::includeModule('nextype.alpha'))
    return;

$haveOffers = !empty($arResult['OFFERS']);
$arResizeParams = array('width' => 1000, 'height' => 1000);
$arResizeThumbsParams = array('width' => 250, 'height' => 250);

$arResult['PICTURES'] = array();

if ($haveOffers)
{
    $actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]) ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']] : reset($arResult['OFFERS']);
    
    if (!empty($arParams['OFFER_CODE_PROP']) && isset($actualItem['PROPERTIES'][$arParams['OFFER_CODE_PROP']]))
    {
        $arResult['ITEM_CODE'] = is_array($actualItem['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE']) ? implode(", ", $actualItem['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE']) : $actualItem['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE'];
    }
}
else
{
    $actualItem = $arResult;
    
    if (!empty($arParams['PRODUCT_CODE_PROP']) && isset($actualItem['PROPERTIES'][$arParams['PRODUCT_CODE_PROP']]))
    {
        $arResult['ITEM_CODE'] = is_array($actualItem['PROPERTIES'][$arParams['PRODUCT_CODE_PROP']]['VALUE']) ? implode(", ", $actualItem['PROPERTIES'][$arParams['PRODUCT_CODE_PROP']]['VALUE']) : $actualItem['PROPERTIES'][$arParams['PRODUCT_CODE_PROP']]['VALUE'];
    }
    
    $price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];

    if ($price['BASE_PRICE'] > $price['RATIO_PRICE'])
    {
        $currentDiscount = false;
        $arDiscounts = \CCatalogDiscount::GetDiscountByPrice($price["ID"], $GLOBALS['USER']->GetUserGroupArray(), "N", SITE_ID);

        if (!empty($arDiscounts))
        {
            $currentDiscount = end(array_filter($arDiscounts, function ($item) {
                if (!empty($item['ACTIVE_TO']))
                    return true;
            }));
        }

        if (is_array($currentDiscount))
        {
            $arResult['CURRENT_DISCOUNT_ACTIVE_TO'] = date("Y-m-d H:i:s", strtotime($currentDiscount['ACTIVE_TO']));
        }
    }
}

if (!empty($actualItem['DETAIL_PICTURE']))
{
    if ($arImage = CFile::ResizeImageGet($actualItem['DETAIL_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
    {
        $arThumb = CFile::ResizeImageGet($actualItem['DETAIL_PICTURE']['ID'], $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
        
        $arResult['PICTURES'][$actualItem['DETAIL_PICTURE']['ID']] = array(
            'SRC' => $arImage['src'],
            'THUMB' => isset($arThumb['src']) ? $arThumb['src'] : $arImage['src'],
            'ALT' => $actualItem['NAME'],
            'TITLE' => $actualItem['NAME'],
        );
    }
}
elseif (!empty($actualItem['PREVIEW_PICTURE']))
{
    if ($arImage = CFile::ResizeImageGet($actualItem['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
    {
        $arThumb = CFile::ResizeImageGet($actualItem['PREVIEW_PICTURE']['ID'], $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
        
        $arResult['PICTURES'][$actualItem['PREVIEW_PICTURE']['ID']] = array(
            'SRC' => $arImage['src'],
            'THUMB' => isset($arThumb['src']) ? $arThumb['src'] : $arImage['src'],
            'ALT' => $actualItem['NAME'],
            'TITLE' => $actualItem['NAME'],
        );
    }
}

if (!empty($arParams['ADD_PICT_PROP']) && isset($actualItem['PROPERTIES'][$arParams['ADD_PICT_PROP']]) && !empty($actualItem['PROPERTIES'][$arParams['ADD_PICT_PROP']]['VALUE']))
{
    foreach ($actualItem['PROPERTIES'][$arParams['ADD_PICT_PROP']]['VALUE'] as $morePhotoId)
    {
        if (!empty($morePhotoId) && !isset($arResult['PICTURES']['PICTURES'][$morePhotoId]))
        {
            if ($arImage = CFile::ResizeImageGet($morePhotoId, $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
            {
                $arThumb = CFile::ResizeImageGet($morePhotoId, $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
                
                $arResult['PICTURES'][$morePhotoId] = array(
                    'SRC' => $arImage['src'],
                    'THUMB' => isset($arThumb['src']) ? $arThumb['src'] : $arImage['src'],
                    'ALT' => $actualItem['NAME'],
                    'TITLE' => $actualItem['NAME'],
                );
            }
        }
    }
}

if (isset($actualItem['PROPERTIES']['SLIDER_VIDEOS']) && !empty($actualItem['PROPERTIES']['SLIDER_VIDEOS']['VALUE']))
{
    foreach ($actualItem['PROPERTIES']['SLIDER_VIDEOS']['VALUE'] as $moreVideoId)
    {
        if (!empty($moreVideoId) && !isset($arResult['PICTURES']['PICTURES'][$moreVideoId]))
        {
            $arResult['PICTURES'][$moreVideoId] = array(
                'SRC' => CFile::GetPath($moreVideoId),
                'TYPE' => "VIDEO",
                'THUMB' => reset($arResult['PICTURES'])['SRC']
            );
        }
    }
}


if ($haveOffers && empty($arResult['PICTURES']))
{
    if (!empty($arResult['PREVIEW_PICTURE']))
    {
        if ($arImage = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arResult['PICTURES'][$arResult['PREVIEW_PICTURE']['ID']] = array(
                'SRC' => $arImage['src'],
                'ALT' => $actualItem['NAME'],
                'TITLE' => $actualItem['NAME'],
            );
        }
    }
    elseif (!empty($arResult['DETAIL_PICTURE']))
    {
        if ($arImage = CFile::ResizeImageGet($arResult['DETAIL_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arResult['PICTURES'][$arResult['DETAIL_PICTURE']['ID']] = array(
                'SRC' => $arImage['src'],
                'ALT' => $actualItem['NAME'],
                'TITLE' => $actualItem['NAME'],
            );
        }
    }

    if (!empty($arParams['ADD_PICT_PROP']) && isset($arResult['PROPERTIES'][$arParams['ADD_PICT_PROP']]) && !empty($arResult['PROPERTIES'][$arParams['ADD_PICT_PROP']]['VALUE']))
    {
        foreach ($arResult['PROPERTIES'][$arParams['ADD_PICT_PROP']]['VALUE'] as $morePhotoId)
        {
            if (!empty($morePhotoId) && !isset($arResult['PICTURES']['PICTURES'][$morePhotoId]))
            {
                if ($arImage = CFile::ResizeImageGet($morePhotoId, $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
                {
                    $arResult['PICTURES'][$morePhotoId] = array(
                        'SRC' => $arImage['src'],
                        'ALT' => $actualItem['NAME'],
                        'TITLE' => $actualItem['NAME'],
                    );
                }
            }
        }
    }

    if (isset($arResult['PROPERTIES']['SLIDER_VIDEOS']) && !empty($arResult['PROPERTIES']['SLIDER_VIDEOS']['VALUE']))
    {
        foreach ($arResult['PROPERTIES']['SLIDER_VIDEOS']['VALUE'] as $moreVideoId)
        {
            if (!empty($moreVideoId) && !isset($arResult['PICTURES']['PICTURES'][$moreVideoId]))
            {
                $arResult['PICTURES'][$moreVideoId] = array(
                    'SRC' => CFile::GetPath($moreVideoId),
                    'TYPE' => "VIDEO",
                    'THUMB' => reset($arResult['PICTURES'])['SRC']
                );
            }
        }
    }
}

if ($haveOffers)
{
    foreach ($arResult['JS_OFFERS'] as $key => $arOffer)
    {
        $arRawOffer = $arResult['OFFERS'][$key];
        
        $arOffer['MORE_PHOTO'] = array ();
        
        if (!empty($arRawOffer['DETAIL_PICTURE']) && $arImage = CFile::ResizeImageGet($arRawOffer['DETAIL_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arThumb = CFile::ResizeImageGet($arRawOffer['DETAIL_PICTURE']['ID'], $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
            $arOffer['PICTURE'] = array (
                'ID' => $arRawOffer['DETAIL_PICTURE']['ID'],
                'SRC' => $arImage['src'],
                'THUMB' => $arThumb['src']
            );
        }
        elseif (!empty($arRawOffer['PREVIEW_PICTURE']) && $arImage = CFile::ResizeImageGet($arRawOffer['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arThumb = CFile::ResizeImageGet($arRawOffer['PREVIEW_PICTURE']['ID'], $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
            $arOffer['PICTURE'] = array (
                'ID' => $arRawOffer['PREVIEW_PICTURE']['ID'],
                'SRC' => $arImage['src'],
                'THUMB' => $arThumb['src']
            );
        }
        
        if (empty($arOffer['PICTURE']) && !empty($arOffer['DETAIL_PICTURE']['ID']) && $arImage = CFile::ResizeImageGet($arOffer['DETAIL_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arThumb = CFile::ResizeImageGet($arOffer['DETAIL_PICTURE']['ID'], $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
            $arOffer['PICTURE'] = array (
                'ID' => $arOffer['DETAIL_PICTURE']['ID'],
                'SRC' => $arImage['src'],
                'THUMB' => $arThumb['src']
            );
        }
        elseif (empty($arOffer['PICTURE']) && !empty($arOffer['PREVIEW_PICTURE']['ID']) && $arImage = CFile::ResizeImageGet($arOffer['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arThumb = CFile::ResizeImageGet($arOffer['PREVIEW_PICTURE']['ID'], $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
            $arOffer['PICTURE'] = array (
                'ID' => $arOffer['PREVIEW_PICTURE']['ID'],
                'SRC' => $arImage['src'],
                'THUMB' => $arThumb['src']
            );
        }
        
        if (!empty($arParams['OFFER_ADD_PICT_PROP']) && isset($arRawOffer['PROPERTIES'][$arParams['OFFER_ADD_PICT_PROP']]))
        {
            $morePhotos = $arRawOffer['PROPERTIES'][$arParams['OFFER_ADD_PICT_PROP']];
            
            if (!empty($morePhotos['VALUE']))
            {
                foreach ($morePhotos['VALUE'] as $photoId)
                {
                    if (!empty($photoId) && $arImage = CFile::ResizeImageGet($photoId, $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
                    {
                        $arThumb = CFile::ResizeImageGet($photoId, $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
                        $arOffer['MORE_PHOTO'][] = array (
                            'SRC' => $arImage['src'],
                            'THUMB' => $arThumb['src']
                        );
                    }
                }
            }
        }

        if (isset($arRawOffer['PROPERTIES']['SLIDER_VIDEOS']))
        {
            $moreVideos = $arRawOffer['PROPERTIES']['SLIDER_VIDEOS'];

            if (!empty($moreVideos['VALUE']))
            {
                $mainThumb = CFile::ResizeImageGet($arOffer['PICTURE']['ID'], $arResizeThumbsParams, BX_RESIZE_IMAGE_EXACT);
                foreach ($moreVideos['VALUE'] as $videoId)
                {
                    if (!empty($videoId) && $arVideo = CFile::GetFileArray($videoId))
                    {
                        $arOffer['MORE_PHOTO'][] = array (
                            'SRC' => $arVideo['SRC'],
                            'THUMB' => $mainThumb['src'],
                            'TYPE' => "VIDEO"
                        );
                    }
                }
            }
        }

        if (!empty($arParams['OFFER_CODE_PROP']) && isset($arRawOffer['PROPERTIES'][$arParams['OFFER_CODE_PROP']]))
        {
            $arOffer['ITEM_CODE'] = is_array($arRawOffer['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE']) ? implode(", ", $arRawOffer['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE']) : $arRawOffer['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE'];
        }
        
        $price = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']];
        $arOffer['CURRENT_DISCOUNT_ACTIVE_TO'] = "";
        
        if (isset($price) && $price['BASE_PRICE'] > $price['RATIO_PRICE'])
        {
            $currentDiscount = false;
            $arDiscounts = \CCatalogDiscount::GetDiscountByPrice($price["ID"], $GLOBALS['USER']->GetUserGroupArray(), "N", SITE_ID);

            if (!empty($arDiscounts))
            {
                $currentDiscount = end(array_filter($arDiscounts, function ($item) {
                    if (!empty($item['ACTIVE_TO']))
                        return true;
                }));
            }

            if (is_array($currentDiscount))
            {
                $arOffer['CURRENT_DISCOUNT_ACTIVE_TO'] = date("Y-m-d H:i:s", strtotime($currentDiscount['ACTIVE_TO']));
            }
        }
        
        unset($arOffer['SLIDER'], $arOffer['PREVIEW_PICTURE'], $arOffer['DETAIL_PICTURE'], $arRawOffer);
        
        $arResult['JS_OFFERS'][$key] = $arOffer;
    }
            
    if (!empty($arResult['SKU_PROPS']))
    {
        foreach ($arResult['SKU_PROPS'] as $key => $skuProperty)
        {
            if ($skuProperty['SHOW_MODE'] === "PICT" && !empty($skuProperty['VALUES']))
            {
                foreach ($skuProperty['VALUES'] as $keyValue => $skuValue)
                {
                    foreach ($arResult['OFFERS'] as $arOffer)
                    {
                        if (!isset($arOffer['PREVIEW_PICTURE']['ID']) || empty($arOffer['PREVIEW_PICTURE']['ID']))
                            continue;

                        if (isset($arOffer['TREE']) && is_array($arOffer['TREE']) && isset($arOffer['TREE']['PROP_' . $skuProperty['ID']]) && $arOffer['TREE']['PROP_' . $skuProperty['ID']] == $skuValue['ID'])
                        {
                            $arResize = CFile::ResizeImageGet($arOffer['PREVIEW_PICTURE']['ID'], array ('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT);
                            if ($arResize)
                            {
                                $skuValue['PICT']['SRC'] = $arResize['src'];
                                break;
                            }
                        }
                    }

                    $arResult['SKU_PROPS'][$key]['VALUES'][$keyValue] = $skuValue;
                }
            }
        }
    }
}


if (!empty($arResult['PRODUCT']['TYPE']) && $arResult['PRODUCT']['TYPE'] == \Bitrix\Catalog\ProductTable::TYPE_SET)
{
    $arSets = \CCatalogProductSet::getAllSetsByProduct($arResult['ID'], \CCatalogProductSet::TYPE_SET);
    if (!empty($arSets))
    {
        $arSets = reset($arSets);
        if (!empty($arSets['ITEMS']))
        {
            $arMeasures = Array ();
            $rsMeasures = \CCatalogMeasure::getList();
            while ($arMeasure = $rsMeasures->GetNext())
            {
                $arMeasures[$arMeasure['ID']] = $arMeasure;
            }
            
            $arSetIDs = $arSetItems = Array ();
            foreach ($arSets['ITEMS'] as $arItem)
            {
                $arSetItems[$arItem['ITEM_ID']] = Array (
                    'DISCOUNT_PERCENT' => $arItem['DISCOUNT_PERCENT'],
                    'SORT' => $arItem['SORT'],
                    'QUANTITY' => $arItem['QUANTITY'],
                    'MEASURE' => isset($arMeasures[$arItem['MEASURE']]) ? $arMeasures[$arItem['MEASURE']]['SYMBOL'] : ""
                );
                
                $arSetIDs[] = $arItem['ITEM_ID'];
            }
            
            $rsSetItems = \CIBlockElement::GetList(Array(), Array ('=ID' => $arSetIDs), false, false);
            while ($arSetItem = $rsSetItems->GetNext())
            {
                $arSetItem = array_merge($arSetItem, $arSetItems[$arSetItem['ID']]);
                if (!empty($arSetItem['PREVIEW_PICTURE']))
                {
                    $arPreview = \CFile::ResizeImageGet($arSetItem['PREVIEW_PICTURE'], Array ('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
                    $arSetItem['PREVIEW_PICTURE'] = Array (
                        'SRC' => $arPreview['src'],
                        'TITLE' => $arSetItem['NAME'],
                        'ALT' => $arSetItem['NAME'],
                    );
                }
                
                
                if (!empty($arResult['CAT_PRICES']))
                {
                    $arSetPrices = array ();
                    foreach ($arResult['CAT_PRICES'] as $arGroup)
                    {
                        $arGroupPrice = \CPrice::GetList(array (), array ('PRODUCT_ID' => $arSetItem['ID'], 'CATALOG_GROUP_ID' => $arGroup['ID']))->fetch();
                        if (isset($arGroupPrice) && !empty($arGroupPrice['PRICE']))
                        {
                            $arGroupPrice['PRICE_PRINT'] = CurrencyFormat($arGroupPrice['PRICE'], $arGroupPrice['CURRENCY']);
                            
                            $arGroupPrice['DISCOUNT_PRICE'] = floatval($arSetItem['DISCOUNT_PERCENT']) > 0 ? $arGroupPrice['PRICE'] - ($arGroupPrice['PRICE'] * floatval($arSetItem['DISCOUNT_PERCENT']) / 100) : $arGroupPrice['PRICE'];
                            $arGroupPrice['DISCOUNT_PRICE_PRINT'] = CurrencyFormat($arGroupPrice['DISCOUNT_PRICE'], $arGroupPrice['CURRENCY']);
                            
                            $arSetPrices[] = $arGroupPrice;
                        }
                    }
                    
                    usort($arSetPrices, function ($a, $b) {
                        if ($a['DISCOUNT_PRICE'] == $b['DISCOUNT_PRICE'])
                            return 0;
                        
                        return $a['DISCOUNT_PRICE'] < $b['DISCOUNT_PRICE'] ? -1 : 1;
                    });
                    
                    if (!empty($arSetPrices))
                    {
                        $arSetItem['PRICES'] = $arSetPrices;
                        $arSetItem['MIN_PRICE'] = $arSetPrices[0];
                    }
                }

                $arSetItems[$arSetItem['ID']] = $arSetItem;
            }
            
            if (!empty($arSetItems))
            {
                usort($arSetItems, function($a, $b)
                {
                    if ($a['SORT'] == $b['SORT'])
                        return 0;
                    
                    return ($a['SORT'] < $b['SORT']) ? -1 : 1;
                });
                
                $arResult['SET_ITEMS'] = $arSetItems;
                unset($arSetItems);
            }
        }
    }
   
}

if (isset($arParams['REVIEWS_URL']))
{
    
    $rsElement = CIBlockElement::GetList(array(), array("ID" => $arResult['ID']), false, false, array("ID", "NAME", "DETAIL_PAGE_URL"));
    $rsElement->SetUrlTemplates($arParams['REVIEWS_URL']);
    if ($arElement = $rsElement->GetNext())
    {
        $arResult['REVIEWS_DETAIL_URL'] = $arElement['DETAIL_PAGE_URL'];
    }
    
}

$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>16, "NAME" =>$arResult['DISPLAY_PROPERTIES']["FACTORY"]["DISPLAY_VALUE"] ,"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $brand = $ob->GetFields();
    if($brand["NAME"])
        $arResult['DISPLAY_PROPERTIES']["FACTORY"]["DISPLAY_VALUE"] = '<a href="'.$brand["DETAIL_PAGE_URL"].'">'.$brand["NAME"].'</a>';
}

$arResult['DISPLAY_PROPERTIES']["COLLECTION"]["DISPLAY_VALUE"] = '<a href="'.$arResult["SECTION"]["SECTION_PAGE_URL"].'">'.$arResult["DISPLAY_PROPERTIES"]["COLLECTION"]["DISPLAY_VALUE"].'</a>';

if(!empty($arParams['DOPSEO'])) {
    $ipropElementValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arResult['IBLOCK_ID'],$arResult['ID']);
    $pageProperties = $ipropElementValues->getValues();

    $title = $pageProperties['ELEMENT_META_TITLE']." ".$arParams['DOPSEO'];
    $descript = $pageProperties['ELEMENT_META_DESCRIPTION']." ".$arParams['DOPSEO'];
    $APPLICATION->SetPageProperty('title',$title);
    $APPLICATION->SetPageProperty('description',$descript);
}

foreach($arResult['PROPERTIES'] as $key => $property) {
    foreach($arParams['MAIN_BLOCK_PROPERTY_CODE'] as $key2 => $property2) {
        if($key == $key2){
            if(!empty($property["VALUE"])) {
                $arResult['DISPLAY_PROPERTIES'][$key] = $property;
            }
        }
    }
}
(function(window){
	'use strict';

	if (window.Alpha.CatalogElement)
		return;

	var BasketButton = function(params)
	{
		BasketButton.superclass.constructor.apply(this, arguments);
		this.buttonNode = BX.create('SPAN', {
			props: {className: 'btn btn-primary btn-buy btn-sm', id: this.id},
			style: typeof params.style === 'object' ? params.style : {},
			text: params.text,
			events: this.contextEvents
		});

		if (BX.browser.IsIE())
		{
			this.buttonNode.setAttribute('hideFocus', 'hidefocus');
		}
	};
	BX.extend(BasketButton, BX.PopupWindowButton);

	window.Alpha.CatalogElement = function(arParams)
	{
		this.productType = 0;

		this.config = {
			useCatalog: true,
			useStore: false,
			showQuantity: true,
			showPrice: true,
			showAbsent: true,
			showOldPrice: false,
			showPercent: false,
			showSkuProps: false,
			showOfferGroup: false,
			useCompare: false,
			useStickers: false,
			useSubscribe: false,
			usePopup: false,
			useMagnifier: false,
			usePriceRanges: false,
			basketAction: ['BUY'],
			showClosePopup: false,
			templateTheme: '',
			showSlider: false,
			sliderInterval: 5000,
			useEnhancedEcommerce: false,
			dataLayerName: 'dataLayer',
			brandProperty: false,
			alt: '',
			title: '',
			magnifierZoomPercent: 2
		};

		this.checkQuantity = false;
		this.maxQuantity = 0;
		this.minQuantity = 0;
		this.stepQuantity = 1;
		this.isDblQuantity = false;
		this.canBuy = true;
		this.isGift = false;
		this.canSubscription = true;
		this.currentIsSet = false;
		this.updateViewedCount = false;
                this.giftSelectedId = false;

		this.currentPriceMode = '';
		this.currentPrices = [];
		this.currentPriceSelected = 0;
		this.currentQuantityRanges = [];
		this.currentQuantityRangeSelected = 0;

		this.precision = 6;
		this.precisionFactor = Math.pow(10, this.precision);

		this.visual = {};
		this.basketMode = '';
		this.product = {
			checkQuantity: false,
			maxQuantity: 0,
			stepQuantity: 1,
			startQuantity: 1,
			isDblQuantity: false,
			canBuy: true,
			canSubscription: true,
			name: '',
			pict: {},
			id: 0,
			addUrl: '',
			buyUrl: '',
			slider: {},
			sliderCount: 0,
			useSlider: false,
			sliderPict: []
		};
		this.mess = {};

		this.basketData = {
			useProps: false,
			emptyProps: false,
			quantity: 'quantity',
			props: 'prop',
			basketUrl: '',
			sku_props: '',
			sku_props_var: 'basket_props',
			add_url: '',
			buy_url: '',
			wishlist_url: '',
			wishlist_page_url: BX.message('SITE_DIR') + 'wishlist/',
			add_wish_to_delayed: false
		};
		this.compareData = {
			compareUrl: '',
			compareDeleteUrl: '',
			comparePath: ''
		};

		this.defaultPict = {
			preview: null,
			detail: null
		};

		this.offers = [];
		this.offerNum = 0;
		this.treeProps = [];
		this.selectedValues = {};

		this.mouseTimer = null;
		this.isTouchDevice = BX.hasClass(document.documentElement, 'bx-touch');
		this.touch = null;
		this.slider = {
			interval: null,
			progress: null,
			paused: null,
			controls: []
		};

		this.quantityDelay = null;
		this.quantityTimer = null;

		this.obProduct = null;
		this.obQuantity = null;
		this.obQuantityUp = null;
		this.obQuantityDown = null;
		this.obPrice = {
			price: null,
			full: null,
			discount: null,
			percent: null,
			total: null
		};
		this.obTree = null;
		this.obPriceRanges = null;
		this.obBuyBtn = null;
		this.obAddToBasketBtn = null;
		this.obBasketActions = null;
		this.obNotAvail = null;
		this.obSubscribe = null;
		this.obSkuProps = null;
		this.obMainSkuProps = null;
		this.obBigSlider = null;
		this.obMeasure = null;
		this.obQuantityLimit = {
			all: null,
			value: null
		};
		this.obCompare = null;
                this.obWishlist = null;
		this.obTabsPanel = null;
                this.obDiscountTimer = null;

		this.node = {};
		// top panel small card
		this.smallCardNodes = {};

		this.magnify = {
			enabled: false,
			obBigImg: null,
			obBigSlider: null,
			height: 0,
			width: 0,
			timer: 0
		};
		this.currentImg = {
			id: 0,
			src: '',
			width: 0,
			height: 0
		};
		this.viewedCounter = {
			path: '/bitrix/components/bitrix/catalog.element/ajax.php',
			params: {
				AJAX: 'Y',
				SITE_ID: '',
				PRODUCT_ID: 0,
				PARENT_ID: 0
			}
		};

		this.obPopupWin = null;
		this.basketUrl = '';
		this.basketParams = {};

		this.errorCode = 0;

		if (typeof arParams === 'object')
		{
			this.params = arParams;
			this.initConfig();

			if (this.params.MESS)
			{
				this.mess = this.params.MESS;
			}

			switch (this.productType)
			{
				case 0: // no catalog
				case 1: // product
				case 2: // set
					this.initProductData();
					break;
				case 3: // sku
					this.initOffersData();
					break;
				default:
					this.errorCode = -1;
			}

			this.initBasketData();
			this.initCompareData();
		}

		if (this.errorCode === 0)
		{
			BX.ready(BX.delegate(this.init, this));
		}

		this.params = {};

		BX.addCustomEvent('onSaleProductIsGift', BX.delegate(this.onSaleProductIsGift, this));
		BX.addCustomEvent('onSaleProductIsNotGift', BX.delegate(this.onSaleProductIsNotGift, this));
	};

	window.Alpha.CatalogElement.prototype = {
		getEntity: function(parent, entity, additionalFilter)
		{
			if (!parent || !entity)
				return null;

			additionalFilter = additionalFilter || '';

			return parent.querySelector(additionalFilter + '[data-entity="' + entity + '"]');
		},

		getEntities: function(parent, entity, additionalFilter)
		{
			if (!parent || !entity)
				return {length: 0};

			additionalFilter = additionalFilter || '';

			return parent.querySelectorAll(additionalFilter + '[data-entity="' + entity + '"]');
		},

		onSaleProductIsGift: function(productId, offerId)
		{
			if (offerId && this.offers && this.offers[this.offerNum].ID == offerId)
			{
				this.setGift();
			}
		},

		onSaleProductIsNotGift: function(productId, offerId)
		{
			if (offerId && this.offers && this.offers[this.offerNum].ID == offerId)
			{
				this.restoreSticker();
				this.isGift = false;
				this.setPrice();
			}
		},

		reloadGiftInfo: function()
		{
			if (this.productType === 3)
			{
				this.checkQuantity = true;
				this.maxQuantity = 1;

				this.setPrice();
				this.redrawSticker({text: BX.message('PRODUCT_GIFT_LABEL')});
			}
		},

		setGift: function()
		{
			if (this.productType === 3)
			{
				// sku
				this.isGift = true;
			}

			if (this.productType === 1 || this.productType === 2)
			{
				// simple
				this.isGift = true;
			}

			if (this.productType === 0)
			{
				this.isGift = false;
			}

			this.reloadGiftInfo();
		},

		setOffer: function(offerNum)
		{
			this.offerNum = parseInt(offerNum);
			this.setCurrent();
		},

		init: function()
		{
			var i = 0,
				j = 0,
				treeItems = null;

			this.obProduct = BX(this.visual.ID);
			if (!this.obProduct)
			{
				this.errorCode = -1;
			}

			this.obBigSlider = BX(this.visual.BIG_SLIDER_ID);
                        this.obDiscountTimer = BX(this.visual.DISCOUNT_TIMER_ID);

			if (!this.obBigSlider)
			{
				this.errorCode = -2;
			}

			if (this.config.showPrice)
			{
				this.obPrice.price = BX(this.visual.PRICE_ID);
				if (!this.obPrice.price && this.config.useCatalog)
				{
					this.errorCode = -16;
				}
				else
				{
					this.obPrice.total = BX(this.visual.PRICE_TOTAL);

					if (this.config.showOldPrice)
					{
						this.obPrice.full = BX(this.visual.OLD_PRICE_ID);
						this.obPrice.discount = BX(this.visual.DISCOUNT_PRICE_ID);

						if (!this.obPrice.full || this.obPrice.discount)
						{
							this.config.showOldPrice = false;
						}
					}

					if (this.config.showPercent)
					{
						this.obPrice.percent = BX(this.visual.DISCOUNT_PERCENT_ID);
						if (!this.obPrice.percent)
						{
							this.config.showPercent = false;
						}
					}
				}

				this.obBasketActions = BX(this.visual.BASKET_ACTIONS_ID);
				if (this.obBasketActions)
				{
					if (BX.util.in_array('BUY', this.config.basketAction))
					{
						this.obBuyBtn = BX(this.visual.BUY_LINK);
					}

					if (BX.util.in_array('ADD', this.config.basketAction))
					{
						this.obAddToBasketBtn = BX(this.visual.ADD_BASKET_LINK);
					}
				}
				this.obNotAvail = BX(this.visual.NOT_AVAILABLE_MESS);
			}
                        


			if (this.config.showQuantity)
			{
				this.obQuantity = BX(this.visual.QUANTITY_ID);
				this.node.quantity = this.getEntity(this.obProduct, 'quantity-block');
				if (this.visual.QUANTITY_UP_ID)
				{
					this.obQuantityUp = BX(this.visual.QUANTITY_UP_ID);
				}

				if (this.visual.QUANTITY_DOWN_ID)
				{
					this.obQuantityDown = BX(this.visual.QUANTITY_DOWN_ID);
				}
			}

			if (this.productType === 3)
			{
				if (this.visual.TREE_ID)
				{
					this.obTree = BX(this.visual.TREE_ID);
					if (!this.obTree)
					{
						this.errorCode = -256;
					}
				}

				if (this.visual.QUANTITY_MEASURE)
				{
					this.obMeasure = BX(this.visual.QUANTITY_MEASURE);
				}

				if (this.visual.QUANTITY_LIMIT && this.config.showMaxQuantity !== 'N')
				{
					this.obQuantityLimit.all = BX(this.visual.QUANTITY_LIMIT);
					if (this.obQuantityLimit.all)
					{
						this.obQuantityLimit.value = this.getEntity(this.obQuantityLimit.all, 'quantity-limit-value');
						if (!this.obQuantityLimit.value)
						{
							this.obQuantityLimit.all = null;
						}
					}
				}

				if (this.config.usePriceRanges)
				{
					this.obPriceRanges = this.getEntity(this.obProduct, 'price-ranges-block');
				}
			}

			if (this.config.showSkuProps)
			{
				this.obSkuProps = BX(this.visual.DISPLAY_PROP_DIV);
				this.obMainSkuProps = BX(this.visual.DISPLAY_MAIN_PROP_DIV);
			}

			if (this.config.useCompare)
			{
				this.obCompare = BX(this.visual.COMPARE_LINK);
			}

			if (this.config.useSubscribe)
			{
				this.obSubscribe = BX(this.visual.SUBSCRIBE_LINK);
			}
                        
                        if (!!this.visual.WISHLIST_ID && BX(this.visual.WISHLIST_ID))
                        {
                            this.obWishlist = BX(this.visual.WISHLIST_ID);
                        }
                        
                        if (this.obWishlist)
                        {
                            BX.bind(this.obWishlist, 'click', BX.delegate(this.wishlistChange, this));
                        }
                        
			
			//this.initPopup();

			
			if (this.errorCode === 0)
			{
				
				if (this.config.showQuantity)
				{
					var startEventName = this.isTouchDevice ? 'touchstart' : 'mousedown';
					var endEventName = this.isTouchDevice ? 'touchend' : 'mouseup';

					if (this.obQuantityUp)
					{
						BX.bind(this.obQuantityUp, startEventName, BX.proxy(this.startQuantityInterval, this));
						BX.bind(this.obQuantityUp, endEventName, BX.proxy(this.clearQuantityInterval, this));
						BX.bind(this.obQuantityUp, 'mouseout', BX.proxy(this.clearQuantityInterval, this));
						BX.bind(this.obQuantityUp, 'click', BX.delegate(this.quantityUp, this));
					}

					if (this.obQuantityDown)
					{
						BX.bind(this.obQuantityDown, startEventName, BX.proxy(this.startQuantityInterval, this));
						BX.bind(this.obQuantityDown, endEventName, BX.proxy(this.clearQuantityInterval, this));
						BX.bind(this.obQuantityDown, 'mouseout', BX.proxy(this.clearQuantityInterval, this));
						BX.bind(this.obQuantityDown, 'click', BX.delegate(this.quantityDown, this));
					}

					if (this.obQuantity)
					{
						BX.bind(this.obQuantity, 'change', BX.delegate(this.quantityChange, this));
					}
				}

				switch (this.productType)
				{
					case 0: // no catalog
					case 1: // product
					case 2: // set
						
                                                this.initSlider();
						this.checkQuantityControls();
						this.setAnalyticsDataLayer('showDetail');
                                                
                                                BX.onCustomEvent('showDetailProduct', {
                                                    id: this.params.PRODUCT.ID,
                                                    name: this.params.PRODUCT.NAME
                                                });
                                                
                                                if (!!this.params.PRODUCT.CURRENT_DISCOUNT_ACTIVE_TO && this.params.PRODUCT.CURRENT_DISCOUNT_ACTIVE_TO != "")
                                                {
                                                    this.setDiscountTimer(this.params.PRODUCT.CURRENT_DISCOUNT_ACTIVE_TO);
                                                }
                                                
						break;
					case 3: // sku
						treeItems = this.obTree.querySelectorAll('[data-entity="sku-value"]');
						for (i = 0; i < treeItems.length; i++)
						{
							BX.bind(treeItems[i], 'click', BX.delegate(this.selectOfferProp, this));
						}

						this.setCurrent();
						break;
				}

				this.obBuyBtn && BX.bind(this.obBuyBtn, 'click', BX.proxy(this.buyBasket, this));
				this.obAddToBasketBtn && BX.bind(this.obAddToBasketBtn, 'click', BX.proxy(this.add2Basket, this));

				if (this.obCompare)
				{
					BX.bind(this.obCompare, 'click', BX.proxy(this.compare, this));
					BX.addCustomEvent('onCatalogDeleteCompare', BX.proxy(this.checkDeletedCompare, this));
				}
                                
                                var tabLinks = this.obProduct.querySelectorAll('[data-entity="tab-link"]');
                                if (tabLinks.length > 0)
                                {
                                    for (i = 0; i < tabLinks.length; i++)
                                    {
                                        var link = tabLinks[i];
                                        if (!!link && link.getAttribute('data-value'))
                                        {
                                            BX.bind(link, 'click', BX.proxy(this.scrollToTab, this));
                                        }
                                    }
                                }
                                
                                BX.addCustomEvent('onSaleGiftChange', BX.delegate(function(productId){
                                    this.giftSelectedId = productId;
                                }, this));
			}
                        
		},

		initConfig: function()
		{
			if (this.params.PRODUCT_TYPE)
			{
				this.productType = parseInt(this.params.PRODUCT_TYPE, 10);
			}

			if (this.params.CONFIG.USE_CATALOG !== 'undefined' && BX.type.isBoolean(this.params.CONFIG.USE_CATALOG))
			{
				this.config.useCatalog = this.params.CONFIG.USE_CATALOG;
			}

			this.config.showQuantity = this.params.CONFIG.SHOW_QUANTITY;
			this.config.showPrice = this.params.CONFIG.SHOW_PRICE;
			this.config.showPercent = this.params.CONFIG.SHOW_DISCOUNT_PERCENT;
			this.config.showOldPrice = this.params.CONFIG.SHOW_OLD_PRICE;
			this.config.showSkuProps = this.params.CONFIG.SHOW_SKU_PROPS;
			this.config.showOfferGroup = this.params.CONFIG.OFFER_GROUP;
			this.config.useCompare = this.params.CONFIG.DISPLAY_COMPARE;
			this.config.useStickers = this.params.CONFIG.USE_STICKERS;
			this.config.useSubscribe = this.params.CONFIG.USE_SUBSCRIBE;
			this.config.showMaxQuantity = this.params.CONFIG.SHOW_MAX_QUANTITY;
			this.config.relativeQuantityFactor = parseInt(this.params.CONFIG.RELATIVE_QUANTITY_FACTOR);
			this.config.usePriceRanges = this.params.CONFIG.USE_PRICE_COUNT;
			this.config.useStore = this.params.CONFIG.USE_STORE == "Y" ? true : false;

			if (this.params.CONFIG.MAIN_PICTURE_MODE)
			{
				this.config.usePopup = BX.util.in_array('POPUP', this.params.CONFIG.MAIN_PICTURE_MODE);
				this.config.useMagnifier = BX.util.in_array('MAGNIFIER', this.params.CONFIG.MAIN_PICTURE_MODE);
			}

			if(parseFloat(this.params.CONFIG.MAIN_PICTURE_MODE_ZOOM) > 1 && parseFloat(this.params.CONFIG.MAIN_PICTURE_MODE_ZOOM) < 8)
			{
				this.config.magnifierZoomPercent = this.params.CONFIG.MAIN_PICTURE_MODE_ZOOM;
			}
			else if(parseFloat(this.params.CONFIG.MAIN_PICTURE_MODE_ZOOM) > 7)
			{
				this.config.magnifierZoomPercent = 7;
			}
			else
			{
				this.config.magnifierZoomPercent = 2;
			}

			if (this.params.CONFIG.ADD_TO_BASKET_ACTION)
			{
				this.config.basketAction = this.params.CONFIG.ADD_TO_BASKET_ACTION;
			}

			this.config.showClosePopup = this.params.CONFIG.SHOW_CLOSE_POPUP;
			this.config.templateTheme = this.params.CONFIG.TEMPLATE_THEME || '';
			this.config.showSlider = this.params.CONFIG.SHOW_SLIDER === 'Y';

			if (this.config.showSlider && !this.isTouchDevice)
			{
				this.config.sliderInterval = parseInt(this.params.CONFIG.SLIDER_INTERVAL) || 5000;
			}
			else
			{
				this.config.sliderInterval = false;
			}

			this.config.useEnhancedEcommerce = this.params.CONFIG.USE_ENHANCED_ECOMMERCE === 'Y';
			this.config.dataLayerName = this.params.CONFIG.DATA_LAYER_NAME;
			this.config.brandProperty = this.params.CONFIG.BRAND_PROPERTY;

			this.config.alt = this.params.CONFIG.ALT || '';
			this.config.title = this.params.CONFIG.TITLE || '';

			if (!this.params.VISUAL || typeof this.params.VISUAL !== 'object' || !this.params.VISUAL.ID)
			{
				this.errorCode = -1;
				return;
			}

			this.visual = this.params.VISUAL;
		},

		initProductData: function()
		{
			var j = 0;

			if (this.params.PRODUCT && typeof this.params.PRODUCT === 'object')
			{
				if (this.config.showPrice)
				{
					this.currentPriceMode = this.params.PRODUCT.ITEM_PRICE_MODE;
					this.currentPrices = this.params.PRODUCT.ITEM_PRICES;
					this.currentPriceSelected = this.params.PRODUCT.ITEM_PRICE_SELECTED;
					this.currentQuantityRanges = this.params.PRODUCT.ITEM_QUANTITY_RANGES;
					this.currentQuantityRangeSelected = this.params.PRODUCT.ITEM_QUANTITY_RANGE_SELECTED;
				}

				if (this.config.showQuantity)
				{
					this.product.checkQuantity = this.params.PRODUCT.CHECK_QUANTITY;
					this.product.isDblQuantity = this.params.PRODUCT.QUANTITY_FLOAT;

					if (this.product.checkQuantity)
					{
						this.product.maxQuantity = this.product.isDblQuantity
							? parseFloat(this.params.PRODUCT.MAX_QUANTITY)
							: parseInt(this.params.PRODUCT.MAX_QUANTITY, 10);
					}

					this.product.stepQuantity = this.product.isDblQuantity
						? parseFloat(this.params.PRODUCT.STEP_QUANTITY)
						: parseInt(this.params.PRODUCT.STEP_QUANTITY, 10);
					this.checkQuantity = this.product.checkQuantity;
					this.isDblQuantity = this.product.isDblQuantity;
					this.stepQuantity = this.product.stepQuantity;
					this.maxQuantity = this.product.maxQuantity;
					this.minQuantity = this.currentPriceMode === 'Q' ? parseFloat(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;

					if (this.isDblQuantity)
					{
						this.stepQuantity = Math.round(this.stepQuantity * this.precisionFactor) / this.precisionFactor;
					}
				}

				this.product.canBuy = this.params.PRODUCT.CAN_BUY;
				this.canSubscription = this.product.canSubscription = this.params.PRODUCT.SUBSCRIPTION;

				this.product.name = this.params.PRODUCT.NAME;
				this.product.pict = this.params.PRODUCT.PICT;
				this.product.id = this.params.PRODUCT.ID;
				this.product.category = this.params.PRODUCT.CATEGORY;

				if (this.params.PRODUCT.ADD_URL)
				{
					this.product.addUrl = this.params.PRODUCT.ADD_URL;
				}

				if (this.params.PRODUCT.BUY_URL)
				{
                                    this.product.buyUrl = this.params.PRODUCT.BUY_URL;
				}

				this.currentIsSet = true;
			}
			else
			{
				this.errorCode = -1;
			}
		},

		initOffersData: function()
		{
			if (this.params.OFFERS && BX.type.isArray(this.params.OFFERS))
			{
				this.offers = this.params.OFFERS;
				this.offerNum = 0;

				if (this.params.OFFER_SELECTED)
				{
					this.offerNum = parseInt(this.params.OFFER_SELECTED, 10) || 0;
				}

				if (this.params.TREE_PROPS)
				{
					this.treeProps = this.params.TREE_PROPS;
				}

				if (this.params.DEFAULT_PICTURE)
				{
					this.defaultPict.preview = this.params.DEFAULT_PICTURE.PREVIEW_PICTURE;
					this.defaultPict.detail = this.params.DEFAULT_PICTURE.DETAIL_PICTURE;
				}

				if (this.params.PRODUCT && typeof this.params.PRODUCT === 'object')
				{
					this.product.id = parseInt(this.params.PRODUCT.ID, 10);
					this.product.name = this.params.PRODUCT.NAME;
					this.product.category = this.params.PRODUCT.CATEGORY;
				}
			}
			else
			{
				this.errorCode = -1;
			}
			this.config.stores = {
				"STORES": this.params.CONFIG.STORES,
				"MAIN_TITLE": this.params.CONFIG.MAIN_TITLE,
				"FIELDS": this.params.CONFIG.FIELDS,
				"SHOW_EMPTY_STORE": this.params.CONFIG.SHOW_EMPTY_STORE,
				"USE_MIN_AMOUNT": this.params.CONFIG.USE_MIN_AMOUNT,
				"SHOW_GENERAL_STORE_INFORMATION": this.params.CONFIG.SHOW_GENERAL_STORE_INFORMATION,
				"MIN_AMOUNT": this.params.CONFIG.MIN_AMOUNT
			};
		},

		initBasketData: function()
		{
			if (this.params.BASKET && typeof this.params.BASKET === 'object')
			{
				if (this.productType === 1 || this.productType === 2)
				{
					this.basketData.useProps = this.params.BASKET.ADD_PROPS;
					this.basketData.emptyProps = this.params.BASKET.EMPTY_PROPS;
				}

				if (this.params.BASKET.QUANTITY)
				{
					this.basketData.quantity = this.params.BASKET.QUANTITY;
				}

				if (this.params.BASKET.PROPS)
				{
					this.basketData.props = this.params.BASKET.PROPS;
				}

				if (this.params.BASKET.BASKET_URL)
				{
					this.basketData.basketUrl = this.params.BASKET.BASKET_URL;
				}

				if (this.productType === 3)
				{
					if (this.params.BASKET.SKU_PROPS)
					{
						this.basketData.sku_props = this.params.BASKET.SKU_PROPS;
					}
				}

				if (this.params.BASKET.ADD_URL_TEMPLATE)
				{
					this.basketData.add_url = this.params.BASKET.ADD_URL_TEMPLATE;
				}

				if (this.params.BASKET.BUY_URL_TEMPLATE)
				{
					this.basketData.buy_url = this.params.BASKET.BUY_URL_TEMPLATE;
				}
                                
				if (this.params.BASKET.WISHLIST_URL_TEMPLATE)
				{
					this.basketData.wishlist_url = this.params.BASKET.WISHLIST_URL_TEMPLATE;
					if (this.params.BASKET.WISHLIST_PAGE_URL && this.params.BASKET.WISHLIST_PAGE_URL.length > 0)
						this.basketData.wishlist_page_url = this.params.BASKET.WISHLIST_PAGE_URL;
					if (this.params.BASKET.ADD_WISH_TO_DELAYED === "Y")
						this.basketData.add_wish_to_delayed = true;
				}

				if (this.basketData.add_url === '' && this.basketData.buy_url === '')
				{
					this.errorCode = -1024;
				}
			}
		},

		initCompareData: function()
		{
			if (this.config.useCompare)
			{
				if (this.params.COMPARE && typeof this.params.COMPARE === 'object')
				{
					if (this.params.COMPARE.COMPARE_PATH)
					{
						this.compareData.comparePath = this.params.COMPARE.COMPARE_PATH;
					}

					if (this.params.COMPARE.COMPARE_URL_TEMPLATE)
					{
						this.compareData.compareUrl = this.params.COMPARE.COMPARE_URL_TEMPLATE;
					}
					else
					{
						this.config.useCompare = false;
					}

					if (this.params.COMPARE.COMPARE_DELETE_URL_TEMPLATE)
					{
						this.compareData.compareDeleteUrl = this.params.COMPARE.COMPARE_DELETE_URL_TEMPLATE;
					}
					else
					{
						this.config.useCompare = false;
					}
				}
				else
				{
					this.config.useCompare = false;
				}
			}
		},

		initPopup: function()
		{

			if (this.config.usePopup)
			{

				$(this.obBigSlider).find('img').each(function () {
					let src = $(this).attr("data-src");

					$(this).parent().attr('data-lightbox', 'product');
					$(this).parent().attr('href', src);
				});

			}
		},

		initSlider: function()
		{
			if (!!this.obBigSlider)
			{
				var obBigContainer = this.obBigSlider.querySelector('.image .swiper-container'),
					obThumbsContainer = this.obBigSlider.querySelector('.thumbs .swiper-container');

				let this_ = this;

				if (!!obThumbsContainer && !!obBigContainer) {

					var thumbsSwiper = new Swiper(obThumbsContainer, {
						slidesPerView: 'auto',
						spaceBetween: 16,
						freeMode: true,
						watchSlidesVisibility: true,
						direction: 'vertical',
                        watchOverflow: true,
						breakpoints: {
							1024: {
								navigation: {
									nextEl: '.thumbs .swiper-button-next',
									prevEl: '.thumbs .swiper-button-prev',
								},
							},
						}

					});

					var imageSwiper = new Swiper(obBigContainer, {
						slidesPerView: 1,
						spaceBetween: 8,
						speed: 400,
						lazy: true,
						simulateTouch: false,
						// effect: 'fade',
                        pagination: {
                            el: '.swiper-pagination',
                            type: 'bullets',
                            clickable: true,
                        },
						thumbs: {
							swiper: thumbsSwiper
						},
						on: {
							slideChangeTransitionStart: function () {
								$(window).scroll();
							},
							sliderMove: function () {
								this_.stopVideo();
							},
						},
						breakpoints: {
							1025: {
								speed: 0,
								pagination: false,
							},
						}

					});

					let this_ = this;

					$(this.obBigSlider).find('.thumbs .swiper-slide').on('mouseover', function() {
						if (!BX.hasClass(this, 'swiper-slide-thumb-active'))
						{
							this_.stopVideo();
							imageSwiper.slideTo($(this).index());

							if (this_.config.useMagnifier && !this_.isTouchDevice)
								this_.detailPictureZoom(this_.obBigSlider);
						}
					});

					$(this.obBigSlider).find('.thumbs .swiper-button-next').on('click', function() {
						if (!$(this).closest('.thumbs').find('.swiper-slide:last-child').hasClass('swiper-slide-visible')){
							thumbsSwiper.setTranslate(thumbsSwiper.getTranslate() - 96);
						}
					});
				}
			}

			if (this.config.useMagnifier && !this.isTouchDevice)
				this.detailPictureZoom(this.obBigSlider);

			if (this.config.usePopup && !this.isTouchDevice)
				this.initPopup();
		},

		stopVideo: function()
		{
			let videoContainer = this.obBigSlider.querySelector('.swiper-slide-active .js-video');

			if (videoContainer)
			{
				let	playBtn = $(videoContainer).find('.video-button'),
					video = $(videoContainer).find('video').get(0);

				if (!video.paused) {
					video.pause();
					$(videoContainer).removeClass('playing');
					playBtn.removeClass('pause');
				}
			}
		},

		detailPictureZoom: function(slider)
		{

			let imgZoom;
			if ($('.image .swiper-container .swiper-slide-active').length == 0)
				imgZoom = $('.image');
			else
				imgZoom = $('.image .swiper-container .swiper-slide-active .item .image-wrapper');

			imgZoom.izoomify({
				duration: 500,
				magnify: this.config.magnifierZoomPercent
			});

		},

		setAnalyticsDataLayer: function(action)
		{
			if (!this.config.useEnhancedEcommerce || !this.config.dataLayerName)
				return;

			var item = {},
				info = {},
				variants = [],
				i, k, j, propId, skuId, propValues;

			switch (this.productType)
			{
				case 0: //no catalog
				case 1: //product
				case 2: //set
					item = {
						'id': this.product.id,
						'name': this.product.name,
						'price': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].PRICE,
						'category': this.product.category,
						'brand': BX.type.isArray(this.config.brandProperty) ? this.config.brandProperty.join('/') : this.config.brandProperty
					};
					break;
				case 3: //sku
					for (i in this.offers[this.offerNum].TREE)
					{
						if (this.offers[this.offerNum].TREE.hasOwnProperty(i))
						{
							propId = i.substring(5);
							skuId = this.offers[this.offerNum].TREE[i];

							for (k in this.treeProps)
							{
								if (this.treeProps.hasOwnProperty(k) && this.treeProps[k].ID == propId)
								{
									for (j in this.treeProps[k].VALUES)
									{
										propValues = this.treeProps[k].VALUES[j];
										if (propValues.ID == skuId)
										{
											variants.push(propValues.NAME);
											break;
										}
									}

								}
							}
						}
					}

					item = {
						'id': this.offers[this.offerNum].ID,
						'name': this.offers[this.offerNum].NAME,
						'price': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].PRICE,
						'category': this.product.category,
						'brand': BX.type.isArray(this.config.brandProperty) ? this.config.brandProperty.join('/') : this.config.brandProperty,
						'variant': variants.join('/')
					};
					break;
			}

			switch (action)
			{
				case 'showDetail':
					info = {
						'event': 'showDetail',
						'ecommerce': {
							'currencyCode': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].CURRENCY || '',
							'detail': {
								'products': [{
									'name': item.name || '',
									'id': item.id || '',
									'price': item.price || 0,
									'brand': item.brand || '',
									'category': item.category || '',
									'variant': item.variant || ''
								}]
							}
						}
					};
					break;
				case 'addToCart':
					info = {
						'event': 'addToCart',
						'ecommerce': {
							'currencyCode': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].CURRENCY || '',
							'add': {
								'products': [{
									'name': item.name || '',
									'id': item.id || '',
									'price': item.price || 0,
									'brand': item.brand || '',
									'category': item.category || '',
									'variant': item.variant || '',
									'quantity': this.config.showQuantity && this.obQuantity ? this.obQuantity.value : 1
								}]
							}
						}
					};
					break;
			}

			window[this.config.dataLayerName] = window[this.config.dataLayerName] || [];
			window[this.config.dataLayerName].push(info);
		},

		

		checkTouch: function(event)
		{
			if (!event || !event.changedTouches)
				return false;

			return event.changedTouches[0].identifier === this.touch.identifier;
		},

		touchStartEvent: function(event)
		{
			if (event.touches.length != 1)
				return;

			this.touch = event.changedTouches[0];
		},

		touchEndEvent: function(event)
		{
			if (!this.checkTouch(event))
				return;

			var deltaX = this.touch.pageX - event.changedTouches[0].pageX,
				deltaY = this.touch.pageY - event.changedTouches[0].pageY;

			if (Math.abs(deltaX) >= Math.abs(deltaY) + 10)
			{
				if (deltaX > 0)
				{
					this.slideNext();
				}

				if (deltaX < 0)
				{
					this.slidePrev();
				}
			}
		},

		

		getItemIndex: function(item)
		{
			return BX.util.array_values(this.product.slider.ITEMS).indexOf(item);
		},

		eq: function(obj, i)
		{
			var len = obj.length,
				j = +i + (i < 0 ? len : 0);

			return j >= 0 && j < len ? obj[j] : {};
		},

		scrollToTab: function(event)
		{
                    var target = BX.getEventTarget(event);
                    
                    if (!target)
                        return;
                    
                    var tab = this.obProduct.querySelector('[data-entity="tab-content"][data-value="' + target.getAttribute('data-value') + '"]');
                    if (!!tab)
                    {
                        var scrollTop = BX.GetWindowScrollPos().scrollTop,
                            containerTop = BX.pos(tab).top - 50;
                    
                    
                            
				new BX.easing({
					duration: 500,
					start: {scroll: scrollTop},
					finish: {scroll: containerTop},
					transition: BX.easing.makeEaseOut(BX.easing.transitions.quint),
					step: BX.delegate(function(state){
						window.scrollTo(0, state.scroll);
					}, this)
				}).animate();
			
                    }
		},

		
		startQuantityInterval: function()
		{
			var target = BX.proxy_context;
			var func = target.id === this.visual.QUANTITY_DOWN_ID
				? BX.proxy(this.quantityDown, this)
				: BX.proxy(this.quantityUp, this);

			this.quantityDelay = setTimeout(
				BX.delegate(function() {
					this.quantityTimer = setInterval(func, 150);
				}, this),
				300
			);
		},

		clearQuantityInterval: function()
		{
			clearTimeout(this.quantityDelay);
			clearInterval(this.quantityTimer);
		},

		quantityUp: function()
		{
			var curValue = 0,
				boolSet = true;

			if (this.errorCode === 0 && this.config.showQuantity && this.canBuy && !this.isGift)
			{
				curValue = this.isDblQuantity ? parseFloat(this.obQuantity.value) : parseInt(this.obQuantity.value, 10);
				if (!isNaN(curValue))
				{
					curValue += this.stepQuantity;

					curValue = this.checkQuantityRange(curValue, 'up');

					if (this.checkQuantity && curValue > this.maxQuantity)
					{
						boolSet = false;
					}

					if (boolSet)
					{
						if (this.isDblQuantity)
						{
							curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
						}

						this.obQuantity.value = curValue;

						this.setPrice();
					}
				}
			}
		},

		quantityDown: function()
		{
			var curValue = 0,
				boolSet = true;

			if (this.errorCode === 0 && this.config.showQuantity && this.canBuy && !this.isGift)
			{
				curValue = (this.isDblQuantity ? parseFloat(this.obQuantity.value) : parseInt(this.obQuantity.value, 10));
				if (!isNaN(curValue))
				{
					curValue -= this.stepQuantity;

					curValue = this.checkQuantityRange(curValue, 'down');

					if (curValue < this.minQuantity)
					{
						boolSet = false;
					}

					if (boolSet)
					{
						if (this.isDblQuantity)
						{
							curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
						}

						this.obQuantity.value = curValue;

						this.setPrice();
					}
				}
			}
		},

		quantityChange: function()
		{
			var curValue = 0,
				intCount;

			if (this.errorCode === 0 && this.config.showQuantity)
			{
				if (this.canBuy)
				{
					curValue = this.isDblQuantity ? parseFloat(this.obQuantity.value) : Math.round(this.obQuantity.value);
					if (!isNaN(curValue))
					{
						curValue = this.checkQuantityRange(curValue);

						if (this.checkQuantity)
						{
							if (curValue > this.maxQuantity)
							{
								curValue = this.maxQuantity;
							}
						}

						this.checkPriceRange(curValue);

						intCount = Math.floor(
							Math.round(curValue * this.precisionFactor / this.stepQuantity) / this.precisionFactor
						) || 1;
						curValue = (intCount <= 1 ? this.stepQuantity : intCount * this.stepQuantity);
						curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;

						if (curValue < this.minQuantity)
						{
							curValue = this.minQuantity;
						}

						this.obQuantity.value = curValue;
					}
					else
					{
						this.obQuantity.value = this.minQuantity;
					}
				}
				else
				{
					this.obQuantity.value = this.minQuantity;
				}

				this.setPrice();
			}
		},

		quantitySet: function(index)
		{
			var strLimit, resetQuantity;
			
			var newOffer = this.offers[index],
				oldOffer = this.offers[this.offerNum];

			if (this.errorCode === 0)
			{
				this.canBuy = newOffer.CAN_BUY;

				this.currentPriceMode = newOffer.ITEM_PRICE_MODE;
				this.currentPrices = newOffer.ITEM_PRICES;
				this.currentPriceSelected = newOffer.ITEM_PRICE_SELECTED;
				this.currentQuantityRanges = newOffer.ITEM_QUANTITY_RANGES;
				this.currentQuantityRangeSelected = newOffer.ITEM_QUANTITY_RANGE_SELECTED;

				if (this.canBuy)
				{
					this.node.quantity && BX.style(this.node.quantity, 'display', '');

					this.obBasketActions && BX.style(this.obBasketActions, 'display', '');
					this.smallCardNodes.buyButton && BX.style(this.smallCardNodes.buyButton, 'display', '');
					this.smallCardNodes.addButton && BX.style(this.smallCardNodes.addButton, 'display', '');

					this.obNotAvail && BX.style(this.obNotAvail, 'display', 'none');
					this.smallCardNodes.notAvailableButton && BX.style(this.smallCardNodes.notAvailableButton, 'display', 'none');

					this.obSubscribe && BX.style(this.obSubscribe, 'display', 'none');
				}
				else
				{
					this.node.quantity && BX.style(this.node.quantity, 'display', 'none');

					this.obBasketActions && BX.style(this.obBasketActions, 'display', 'none');
					this.smallCardNodes.buyButton && BX.style(this.smallCardNodes.buyButton, 'display', 'none');
					this.smallCardNodes.addButton && BX.style(this.smallCardNodes.addButton, 'display', 'none');

					this.obNotAvail && BX.style(this.obNotAvail, 'display', '');
					this.smallCardNodes.notAvailableButton && BX.style(this.smallCardNodes.notAvailableButton, 'display', '');

					if (this.obSubscribe)
					{
						if (newOffer.CATALOG_SUBSCRIBE === 'Y')
						{
							BX.style(this.obSubscribe, 'display', '');
							this.obSubscribe.setAttribute('data-item', newOffer.ID);
							BX(this.visual.SUBSCRIBE_LINK + '_hidden').click();
						}
						else
						{
							BX.style(this.obSubscribe, 'display', 'none');
						}
					}
				}

				this.isDblQuantity = newOffer.QUANTITY_FLOAT;
				this.checkQuantity = newOffer.CHECK_QUANTITY;

				if (this.isDblQuantity)
				{
					this.stepQuantity = Math.round(parseFloat(newOffer.STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor;
					this.maxQuantity = parseFloat(newOffer.MAX_QUANTITY);
					this.minQuantity = this.currentPriceMode === 'Q' ? parseFloat(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
				}
				else
				{
					this.stepQuantity = parseInt(newOffer.STEP_QUANTITY, 10);
					this.maxQuantity = parseInt(newOffer.MAX_QUANTITY, 10);
					this.minQuantity = this.currentPriceMode === 'Q' ? parseInt(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
				}

				if (this.config.showQuantity)
				{
					var isDifferentMinQuantity = oldOffer.ITEM_PRICES.length
						&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED]
						&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED].MIN_QUANTITY != this.minQuantity;

					if (this.isDblQuantity)
					{
						resetQuantity = Math.round(parseFloat(oldOffer.STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor !== this.stepQuantity
							|| isDifferentMinQuantity
							|| oldOffer.MEASURE !== newOffer.MEASURE
							|| (
								this.checkQuantity
								&& parseFloat(oldOffer.MAX_QUANTITY) > this.maxQuantity
								&& parseFloat(this.obQuantity.value) > this.maxQuantity
							);
					}
					else
					{
						resetQuantity = parseInt(oldOffer.STEP_QUANTITY, 10) !== this.stepQuantity
							|| isDifferentMinQuantity
							|| oldOffer.MEASURE !== newOffer.MEASURE
							|| (
								this.checkQuantity
								&& parseInt(oldOffer.MAX_QUANTITY, 10) > this.maxQuantity
								&& parseInt(this.obQuantity.value, 10) > this.maxQuantity
							);
					}

					this.obQuantity.disabled = !this.canBuy;

					if (resetQuantity)
					{
						this.obQuantity.value = this.minQuantity;
					}

					if (this.obMeasure)
					{
						if (newOffer.MEASURE)
						{
							BX.adjust(this.obMeasure, {html: newOffer.MEASURE});
						}
						else
						{
							BX.adjust(this.obMeasure, {html: ''});
						}
					}
				}

				if (this.obQuantityLimit.all)
				{
					if (!this.checkQuantity || this.maxQuantity == 0)
					{
                                            if (this.canBuy)
                                            {
                                                if (this.config.showMaxQuantity === 'M')
                                                {
                                                    BX.adjust(this.obQuantityLimit.value, {html: BX.message('RELATIVE_QUANTITY_MANY')});
                                                    BX.adjust(this.obQuantityLimit.all, {style: {display: ''}});
                                                }
                                                else
                                                {
                                                    if (BX.hasClass(this.obQuantityLimit.all, 'has-reviews'))
                                                    {
                                                        BX.adjust(this.obQuantityLimit.value, {html: ''});
                                                        BX.adjust(this.obQuantityLimit.all, {style: {display: 'none'}});
                                                    }
                                                    else
                                                    {
                                                        BX.adjust(BX.findParent(this.obQuantityLimit.all, {tagName: 'div'}), {style: {display: 'none'}});
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                BX.adjust(this.obQuantityLimit.value, {html: ''});
                                                BX.adjust(this.obQuantityLimit.all, {style: {display: 'none'}});
                                            }
					}
					else
					{
						if (this.config.showMaxQuantity === 'M')
						{
							strLimit = (this.maxQuantity / this.stepQuantity >= this.config.relativeQuantityFactor)
								? BX.message('RELATIVE_QUANTITY_MANY')
								: BX.message('RELATIVE_QUANTITY_FEW');
						}
						else
						{
							strLimit = this.maxQuantity;

							if (newOffer.MEASURE)
							{
								strLimit += (' ' + newOffer.MEASURE);
							}
						}
                                                
                                                BX.adjust(BX.findParent(this.obQuantityLimit.all, {tagName: 'div'}), {style: {display: ''}});
						BX.adjust(this.obQuantityLimit.value, {html: strLimit});
						BX.adjust(this.obQuantityLimit.all, {style: {display: ''}});
					}
				}

				if (this.config.usePriceRanges && this.obPriceRanges)
				{
					if (
						this.currentPriceMode === 'Q'
						&& newOffer.PRICE_RANGES_HTML
					)
					{
						var rangesBody = this.getEntity(this.obPriceRanges, 'price-ranges-body'),
							rangesRatioHeader = this.getEntity(this.obPriceRanges, 'price-ranges-ratio-header');

						if (rangesBody)
						{
							rangesBody.innerHTML = newOffer.PRICE_RANGES_HTML;
						}

						if (rangesRatioHeader)
						{
							rangesRatioHeader.innerHTML = newOffer.PRICE_RANGES_RATIO_HTML;
						}

						this.obPriceRanges.style.display = '';
					}
					else
					{
						this.obPriceRanges.style.display = 'none';
					}

				}
			}
		},

		selectOfferProp: function()
		{
			var i = 0,
				strTreeValue = '',
				arTreeItem = [],
				rowItems = null,
				target = BX.proxy_context;

			if (target && target.hasAttribute('data-treevalue'))
			{
				if (BX.hasClass(target, 'selected'))
					return;
				
				strTreeValue = target.getAttribute('data-treevalue');
				arTreeItem = strTreeValue.split('_');
				this.searchOfferPropIndex(arTreeItem[0], arTreeItem[1]);
                                
				rowItems = BX.findChildren(BX.findParent(target, {attribute: {'data-entity': "sku-line-block"}}), {attribute: {'data-entity': "sku-value"}}, true);
                                
				if (rowItems && rowItems.length)
				{
					for (i = 0; i < rowItems.length; i++)
					{
						BX.removeClass(rowItems[i], 'selected');
					}
				}

				BX.addClass(target, 'selected');

			}
		},

		searchOfferPropIndex: function(strPropID, strPropValue)
		{
			var strName = '',
				arShowValues = false,
				arCanBuyValues = [],
				allValues = [],
				index = -1,
				i, j,
				arFilter = {},
				tmpFilter = [];

			for (i = 0; i < this.treeProps.length; i++)
			{
				if (this.treeProps[i].ID === strPropID)
				{
					index = i;
					break;
				}
			}

			if (index > -1)
			{
				for (i = 0; i < index; i++)
				{
					strName = 'PROP_' + this.treeProps[i].ID;
					arFilter[strName] = this.selectedValues[strName];
				}

				strName = 'PROP_' + this.treeProps[index].ID;
				arFilter[strName] = strPropValue;

				for (i = index + 1; i < this.treeProps.length; i++)
				{
					strName = 'PROP_' + this.treeProps[i].ID;
					arShowValues = this.getRowValues(arFilter, strName);

					if (!arShowValues)
						break;

					allValues = [];

					if (this.config.showAbsent)
					{
						arCanBuyValues = [];
						tmpFilter = [];
						tmpFilter = BX.clone(arFilter, true);

						for (j = 0; j < arShowValues.length; j++)
						{
							tmpFilter[strName] = arShowValues[j];
							allValues[allValues.length] = arShowValues[j];
							if (this.getCanBuy(tmpFilter))
								arCanBuyValues[arCanBuyValues.length] = arShowValues[j];
						}
					}
					else
					{
						arCanBuyValues = arShowValues;
					}

					if (this.selectedValues[strName] && BX.util.in_array(this.selectedValues[strName], arCanBuyValues))
					{
						arFilter[strName] = this.selectedValues[strName];
					}
					else
					{
						if (this.config.showAbsent)
						{
							arFilter[strName] = (arCanBuyValues.length ? arCanBuyValues[0] : allValues[0]);
						}
						else
						{
							arFilter[strName] = arCanBuyValues[0];
						}
					}

					this.updateRow(i, arFilter[strName], arShowValues, arCanBuyValues);
				}

				this.selectedValues = arFilter;
				this.changeInfo();
			}
		},

		updateRow: function(intNumber, activeId, showId, canBuyId)
		{
			var i = 0,
				value = '',
				isCurrent = false,
				rowItems = null;

			var lineContainer = this.getEntities(this.obTree, 'sku-line-block');
                        
			if (intNumber > -1 && intNumber < lineContainer.length)
			{
				rowItems = lineContainer[intNumber].querySelectorAll('[data-entity="sku-value"]');
				for (i = 0; i < rowItems.length; i++)
				{
					value = rowItems[i].getAttribute('data-onevalue');
                                        
					isCurrent = value === activeId;

					if (isCurrent)
					   BX.addClass(rowItems[i], 'selected');
					else
					   BX.removeClass(rowItems[i], 'selected');

					if (BX.util.in_array(value, canBuyId))
							BX.removeClass(rowItems[i], 'unavailable');
					else
							BX.addClass(rowItems[i], 'unavailable');

					if (!BX.util.in_array(value, showId))
					{
						BX.addClass(rowItems[i], 'd-none');
					}

					//rowItems[i].style.display = BX.util.in_array(value, showId) ? '' : 'none';

					if (isCurrent && value == 0 && canBuyId.length == 1)
					{
						BX.addClass(lineContainer[intNumber], 'd-none');
					}
				}

				
			}
		},

		getRowValues: function(arFilter, index)
		{
			var arValues = [],
				i = 0,
				j = 0,
				boolSearch = false,
				boolOneSearch = true;

			if (arFilter.length === 0)
			{
				for (i = 0; i < this.offers.length; i++)
				{
					if (!BX.util.in_array(this.offers[i].TREE[index], arValues))
					{
						arValues[arValues.length] = this.offers[i].TREE[index];
					}
				}
				boolSearch = true;
			}
			else
			{
				for (i = 0; i < this.offers.length; i++)
				{
					boolOneSearch = true;

					for (j in arFilter)
					{
						if (arFilter[j] !== this.offers[i].TREE[j])
						{
							boolOneSearch = false;
							break;
						}
					}

					if (boolOneSearch)
					{
						if (!BX.util.in_array(this.offers[i].TREE[index], arValues))
						{
							arValues[arValues.length] = this.offers[i].TREE[index];
						}

						boolSearch = true;
					}
				}
			}

			return (boolSearch ? arValues : false);
		},

		getCanBuy: function(arFilter)
		{
			var i,
				j = 0,
				boolOneSearch = true,
				boolSearch = false;

			for (i = 0; i < this.offers.length; i++)
			{
				boolOneSearch = true;

				for (j in arFilter)
				{
					if (arFilter[j] !== this.offers[i].TREE[j])
					{
						boolOneSearch = false;
						break;
					}
				}

				if (boolOneSearch)
				{
					if (this.offers[i].CAN_BUY)
					{
						boolSearch = true;
						break;
					}
				}
			}

			return boolSearch;
		},

		setCurrent: function()
		{
                    
			var i,
				j = 0,
				strName = '',
				arShowValues = false,
				arCanBuyValues = [],
				arFilter = {},
				tmpFilter = [],
				current = this.offers[this.offerNum].TREE;

			for (i = 0; i < this.treeProps.length; i++)
			{
				strName = 'PROP_' + this.treeProps[i].ID;
				arShowValues = this.getRowValues(arFilter, strName);

				if (!arShowValues)
					break;

				if (BX.util.in_array(current[strName], arShowValues))
				{
					arFilter[strName] = current[strName];
				}
				else
				{
					arFilter[strName] = arShowValues[0];
					this.offerNum = 0;
				}

				if (this.config.showAbsent)
				{
					arCanBuyValues = [];
					tmpFilter = [];
					tmpFilter = BX.clone(arFilter, true);

					for (j = 0; j < arShowValues.length; j++)
					{
						tmpFilter[strName] = arShowValues[j];

						if (this.getCanBuy(tmpFilter))
						{
							arCanBuyValues[arCanBuyValues.length] = arShowValues[j];
						}
					}
				}
				else
				{
					arCanBuyValues = arShowValues;
				}
                                
				this.updateRow(i, arFilter[strName], arShowValues, arCanBuyValues);
			}

			this.selectedValues = arFilter;
			this.changeInfo();
		},

		changeInfo: function()
		{
			var index = -1,
				j = 0,
				boolOneSearch = true,
				eventData = {
					currentId: (this.offerNum > -1 ? this.offers[this.offerNum].ID : 0),
					newId: 0
				};

			var i, offerGroupNode;

			for (i = 0; i < this.offers.length; i++)
			{
				boolOneSearch = true;

				for (j in this.selectedValues)
				{
					if (this.selectedValues[j] !== this.offers[i].TREE[j])
					{
						boolOneSearch = false;
						break;
					}
				}

				if (boolOneSearch)
				{
					index = i;
					break;
				}
			}

			if (index > -1)
			{
				if (index != this.offerNum)
				{
					this.isGift = false;
				}
                                
                                if (!!this.offers[index].PICTURE && !!this.offers[index].PICTURE.SRC)
                                {
                                    if (this.offers[index].MORE_PHOTO.length > 0)
                                    {
                                        var arThumbs = [], arBigPictures = [];
                                        
                                        arThumbs.push(BX.create('div', {
                                            props: {className: 'swiper-slide'},
                                            children: new window.Alpha.LazyBackgroundThumb({src: this.offers[index].PICTURE.THUMB, className: 'img', type: this.offers[index].PICTURE.TYPE})

                                        }));

                                        arBigPictures.push(BX.create('div', {
                                            props: {className: 'swiper-slide'},
                                            children: [
                                                BX.create('div', {
                                                    props: {className: 'item'},
                                                    children: new window.Alpha.LazyImage({src: this.offers[index].PICTURE.SRC, className: 'image-wrapper', type: this.offers[index].PICTURE.TYPE})
                                                })
                                            ]
                                        }));
                                        
                                        for (i in this.offers[index].MORE_PHOTO)
										{
                                            arThumbs.push(BX.create('div', {
                                                props: {className: 'swiper-slide'},
                                                children: new window.Alpha.LazyBackgroundThumb({src: this.offers[index].MORE_PHOTO[i].THUMB, className: 'img', type: this.offers[index].MORE_PHOTO[i].TYPE})

                                            }));

                                            arBigPictures.push(BX.create('div', {
                                                props: {className: 'swiper-slide'},
                                                children: [
                                                    BX.create('div', {
                                                        props: {className: 'item'},
                                                        children: new window.Alpha.LazyImage({src: this.offers[index].MORE_PHOTO[i].SRC, className: 'image-wrapper', type: this.offers[index].MORE_PHOTO[i].TYPE})
                                                    })
                                                ]
                                            }));
                                        }
                                        
                                        BX.cleanNode(this.obBigSlider.querySelector('.image'));
                                        BX.cleanNode(this.obBigSlider.querySelector('.thumbs'));
                                        BX.removeClass(this.obBigSlider, 'disabled-slider');
                                        
                                        BX.adjust(this.obBigSlider.querySelector('.image'), {
                                            children: [
                                                BX.create('div', {
                                                    props: {className: 'swiper-container'},
                                                    children: [
                                                        BX.create('div', {
                                                            props: {className: 'swiper-wrapper'},
                                                            children: arBigPictures
                                                        }),
                                                        BX.create('div', {
                                                            props: {className: 'swiper-pagination'},
                                                        })
                                                    ]
                                                })
                                            ]
                                        });
                                        
                                        BX.adjust(this.obBigSlider.querySelector('.thumbs'), {
                                            children: [
                                                BX.create('div', {
                                                    props: {className: 'thumbs-wrapper'},
                                                    children: [
                                                        
                                                        BX.create('div', {
                                                            props: {className: 'swiper-container'},
                                                            children: [
                                                                BX.create('div', {
                                                                    props: {className: 'swiper-wrapper'},
                                                                    children: arThumbs
                                                                }),
                                                            ]
                                                        }),
                                                        
                                                        BX.create('div', {
                                                            props: {className: 'swiper-arrows'},
                                                            children: [
                                                                BX.create('div', {props: {className: 'swiper-button-next'}}),
                                                                BX.create('div', {props: {className: 'swiper-button-prev'}})
                                                            ]
                                                        }),
                                                    ]
                                                })
                                            ]
                                        });
                                    }
                                    else
                                    {
                                        
                                        BX.cleanNode(this.obBigSlider.querySelector('.image'));
                                        BX.cleanNode(this.obBigSlider.querySelector('.thumbs'));
                                        
                                        BX.addClass(this.obBigSlider, 'disabled-slider');
                                        BX.adjust(this.obBigSlider.querySelector('.image'), {
                                            children: new window.Alpha.LazyImage({src: this.offers[index].PICTURE.SRC, className: 'image-wrapper'})
                                        });
                                        
                                    }
                                }
                                
                                if (!!this.visual.ITEM_CODE_ID)
                                {
                                    var obItemCode = BX(this.visual.ITEM_CODE_ID),
                                        obItemCodeValue = obItemCode.querySelector('[data-entity="item-code-value"]');
                                    
                                    if (!!this.offers[index].ITEM_CODE && this.offers[index].ITEM_CODE.length > 0)
                                    {
                                        BX.adjust(obItemCodeValue, {
                                            html: this.offers[index].ITEM_CODE
                                        });
                                        
                                        BX.adjust(obItemCode, {
                                            style: {display: 'inline-flex'}
                                        });
                                    }
                                    else
                                    {
                                        BX.adjust(obItemCode, {
                                            style: {display: 'none'}
                                        });
                                    }
                                }

				for (i = 0; i < this.offers.length; i++)
				{
					if (this.config.showOfferGroup && this.offers[i].OFFER_GROUP)
					{
						if (offerGroupNode = BX(this.visual.OFFER_GROUP + this.offers[i].ID))
						{
							offerGroupNode.style.display = (i == index ? '' : 'none');
						}
					}

				}

				

				if (this.config.showSkuProps)
				{
					if (this.obSkuProps)
					{
						if (!this.offers[index].DISPLAY_PROPERTIES)
						{
							BX.adjust(this.obSkuProps, {style: {display: 'none'}, html: ''});
						}
						else
						{
							BX.adjust(this.obSkuProps, {style: {display: ''}, html: this.offers[index].DISPLAY_PROPERTIES});
						}
					}

					if (this.obMainSkuProps)
					{
						if (!this.offers[index].DISPLAY_PROPERTIES_MAIN_BLOCK)
						{
							BX.addClass(this.obMainSkuProps, 'd-none');

							if (!!this.obMainSkuProps.querySelector('[data-entity="props-list"]'))
								this.obMainSkuProps.querySelector('[data-entity="props-list"]').innerHTML = '';
						}
						else
						{
							BX.removeClass(this.obMainSkuProps, 'd-none');

							if (!!this.obMainSkuProps.querySelector('[data-entity="props-list"]'))
								this.obMainSkuProps.querySelector('[data-entity="props-list"]').innerHTML = this.offers[index].DISPLAY_PROPERTIES_MAIN_BLOCK;
						}
					}
				}

				this.quantitySet(index);
				this.setPrice();
				this.setCompared(this.offers[index].COMPARED);
                                
                                this.setDiscountTimer(this.offers[index].CURRENT_DISCOUNT_ACTIVE_TO);

				this.offerNum = index;

				this.setAnalyticsDataLayer('showDetail');
				this.incViewedCounter();
				this.initSlider();
                                
				eventData.newId = this.offers[this.offerNum].ID;
				// only for compatible catalog.store.amount custom templates
				BX.onCustomEvent('onCatalogStoreProductChange', [this.offers[this.offerNum].ID]);
				// new event
				BX.onCustomEvent('onCatalogElementChangeOffer', [eventData]);
                                
				BX.onCustomEvent('showDetailProduct', {
					id: this.offers[this.offerNum].ID,
					name: !!this.offers[this.offerNum].NAME ? this.offers[this.offerNum].NAME : this.product.name
				});
                                
				eventData = null;

				if (this.config.useStore)
				{
					this.changeStoresBlock();
				}
			}
		},

		changeStoresBlock: function()
		{
			let params = this.config.stores,
				this_ = this;

			params["ELEMENT_ID"] = this.product.id;
			params["OFFER_ID"] = this.offers[this.offerNum].ID;

			BX.ajax({
				url: BX.message('SITE_DIR')+'include/ajax/stores_amount.php',
				data: { params: params, charset: BX.message("LANG_CHARSET") },
				method: 'POST',
				dataType: 'html',
				onsuccess: function(data)
				{
					BX.adjust(BX(this_.visual.STOCK_BLOCK_ID), {html: data});
				},
				onfailure: function()
				{

				}
			});
		},

		restoreSticker: function()
		{
			if (this.previousStickerText)
			{
				this.redrawSticker({text: this.previousStickerText});
			}
			else
			{
				this.hideSticker();
			}
		},

		hideSticker: function()
		{
			BX.hide(BX(this.visual.STICKER_ID));
		},

		redrawSticker: function(stickerData)
		{
			stickerData = stickerData || {};
			var text = stickerData.text || '';

			var sticker = BX(this.visual.STICKER_ID);
			if (!sticker)
				return;

			BX.show(sticker);

			var previousStickerText = sticker.getAttribute('title');
			if (previousStickerText && previousStickerText != text)
			{
				this.previousStickerText = previousStickerText;
			}

			BX.adjust(sticker, {text: text, attrs: {title: text}});
		},

		checkQuantityRange: function(quantity, direction)
		{
			if (typeof quantity === 'undefined'|| this.currentPriceMode !== 'Q')
			{
				return quantity;
			}

			quantity = parseFloat(quantity);

			var nearestQuantity = quantity;
			var range, diffFrom, absDiffFrom, diffTo, absDiffTo, shortestDiff;

			for (var i in this.currentQuantityRanges)
			{
				if (this.currentQuantityRanges.hasOwnProperty(i))
				{
					range = this.currentQuantityRanges[i];

					if (
						parseFloat(quantity) >= parseFloat(range.SORT_FROM)
						&& (
							range.SORT_TO === 'INF'
							|| parseFloat(quantity) <= parseFloat(range.SORT_TO)
						)
					)
					{
						nearestQuantity = quantity;
						break;
					}
					else
					{
						diffFrom = parseFloat(range.SORT_FROM) - quantity;
						absDiffFrom = Math.abs(diffFrom);
						diffTo = parseFloat(range.SORT_TO) - quantity;
						absDiffTo = Math.abs(diffTo);

						if (shortestDiff === undefined || shortestDiff > absDiffFrom)
						{
							if (
								direction === undefined
								|| (direction === 'up' && diffFrom > 0)
								|| (direction === 'down' && diffFrom < 0)
							)
							{
								shortestDiff = absDiffFrom;
								nearestQuantity = parseFloat(range.SORT_FROM);
							}
						}

						if (shortestDiff === undefined || shortestDiff > absDiffTo)
						{
							if (
								direction === undefined
								|| (direction === 'up' && diffFrom > 0)
								|| (direction === 'down' && diffFrom < 0)
							)
							{
								shortestDiff = absDiffTo;
								nearestQuantity = parseFloat(range.SORT_TO);
							}
						}
					}
				}
			}

			return nearestQuantity;
		},

		checkPriceRange: function(quantity)
		{
			if (typeof quantity === 'undefined'|| this.currentPriceMode !== 'Q')
			{
				return;
			}

			var range, found = false;

			for (var i in this.currentQuantityRanges)
			{
				if (this.currentQuantityRanges.hasOwnProperty(i))
				{
					range = this.currentQuantityRanges[i];

					if (
						parseFloat(quantity) >= parseFloat(range.SORT_FROM)
						&& (
							range.SORT_TO === 'INF'
							|| parseFloat(quantity) <= parseFloat(range.SORT_TO)
						)
					)
					{
						found = true;
						this.currentQuantityRangeSelected = range.HASH;
						break;
					}
				}
			}

			if (!found && (range = this.getMinPriceRange()))
			{
				this.currentQuantityRangeSelected = range.HASH;
			}

			for (var k in this.currentPrices)
			{
				if (this.currentPrices.hasOwnProperty(k))
				{
					if (this.currentPrices[k].QUANTITY_HASH == this.currentQuantityRangeSelected)
					{
						this.currentPriceSelected = k;
						break;
					}
				}
			}
		},

		getMinPriceRange: function()
		{
			var range;

			for (var i in this.currentQuantityRanges)
			{
				if (this.currentQuantityRanges.hasOwnProperty(i))
				{
					if (
						!range
						|| parseInt(this.currentQuantityRanges[i].SORT_FROM) < parseInt(range.SORT_FROM)
					)
					{
						range = this.currentQuantityRanges[i];
					}
				}
			}

			return range;
		},

		checkQuantityControls: function()
		{
			if (!this.obQuantity)
				return;

			var reachedTopLimit = this.checkQuantity && parseFloat(this.obQuantity.value) + this.stepQuantity > this.maxQuantity,
				reachedBottomLimit = parseFloat(this.obQuantity.value) - this.stepQuantity < this.minQuantity;

			if (reachedTopLimit)
			{
				BX.addClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled');
			}
			else if (BX.hasClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled'))
			{
				BX.removeClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled');
			}

			if (reachedBottomLimit)
			{
				BX.addClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled');
			}
			else if (BX.hasClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled'))
			{
				BX.removeClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled');
			}

			if (reachedTopLimit && reachedBottomLimit)
			{
				this.obQuantity.setAttribute('disabled', 'disabled');
			}
			else
			{
				this.obQuantity.removeAttribute('disabled');
			}
		},

		setPrice: function()
		{
			var economyInfo = '', price;

			if (this.obQuantity)
			{
				this.checkPriceRange(this.obQuantity.value);
			}

			this.checkQuantityControls();

			price = this.currentPrices[this.currentPriceSelected];

			if (this.isGift)
			{
				price.PRICE = 0;
				price.DISCOUNT = price.BASE_PRICE;
				price.PERCENT = 100;
			}

			if (this.obPrice.price)
			{
				if (price)
				{
					BX.adjust(this.obPrice.price, {html: BX.Currency.currencyFormat(price.RATIO_PRICE, price.CURRENCY, true)});
					this.smallCardNodes.price && BX.adjust(this.smallCardNodes.price, {
						html: BX.Currency.currencyFormat(price.RATIO_PRICE, price.CURRENCY, true)
					});
				}
				else
				{
					BX.adjust(this.obPrice.price, {html: ''});
					this.smallCardNodes.price && BX.adjust(this.smallCardNodes.price, {html: ''});
				}

				if (price && price.RATIO_PRICE !== price.RATIO_BASE_PRICE)
				{
					if (this.config.showOldPrice)
					{
						this.obPrice.full && BX.adjust(this.obPrice.full, {
							style: {display: ''},
							html: BX.Currency.currencyFormat(price.RATIO_BASE_PRICE, price.CURRENCY, true)
						});
						this.smallCardNodes.oldPrice && BX.adjust(this.smallCardNodes.oldPrice, {
							style: {display: ''},
							html: BX.Currency.currencyFormat(price.RATIO_BASE_PRICE, price.CURRENCY, true)
						});

						if (this.obPrice.discount)
						{
							economyInfo = BX.message('ECONOMY_INFO_MESSAGE');
							economyInfo = economyInfo.replace('#ECONOMY#', BX.Currency.currencyFormat(price.RATIO_DISCOUNT, price.CURRENCY, true));
							BX.adjust(this.obPrice.discount, {style: {display: ''}, html: economyInfo});
						}
					}

					if (this.config.showPercent)
					{
						this.obPrice.percent && BX.adjust(this.obPrice.percent, {
							style: {display: ''},
							html: -price.PERCENT + '%'
						});
					}
				}
				else
				{
					if (this.config.showOldPrice)
					{
						this.obPrice.full && BX.adjust(this.obPrice.full, {style: {display: 'none'}, html: ''});
						this.smallCardNodes.oldPrice && BX.adjust(this.smallCardNodes.oldPrice, {style: {display: 'none'}, html: ''});
						this.obPrice.discount && BX.adjust(this.obPrice.discount, {style: {display: 'none'}, html: ''});
					}

					if (this.config.showPercent)
					{
						this.obPrice.percent && BX.adjust(this.obPrice.percent, {style: {display: 'none'}, html: ''});
					}
				}

				if (this.obPrice.total)
				{
					if (price && this.obQuantity && this.obQuantity.value != this.stepQuantity)
					{
						BX.adjust(this.obPrice.total, {
							html: BX.message('PRICE_TOTAL_PREFIX') + ' <strong>'
							+ BX.Currency.currencyFormat(price.PRICE * this.obQuantity.value, price.CURRENCY, true)
							+ '</strong>',
							style: {display: ''}
						});
					}
					else
					{
						BX.adjust(this.obPrice.total, {
							html: '',
							style: {display: 'none'}
						});
					}
				}
			}
		},
                
		wishlistChange: function (event) {

			var target = BX.getEventTarget(event),
				changeUrl = this.basketData.wishlist_url;

			if (changeUrl == '' || changeUrl.indexOf('#ID#') == -1)
				return false;

			if (this.basketData.add_wish_to_delayed)
			{
				switch (this.productType)
				{
					case 0: // no catalog
					case 1: // product
					case 2: // set
						changeUrl = changeUrl.replace('#ID#', this.product.id.toString());
						break;
					case 3: // sku
						changeUrl = changeUrl.replace('#ID#', this.offers[this.offerNum].ID);
						break;
				}
			}
			else
				changeUrl = changeUrl.replace('#ID#', this.product.id.toString());

			BX.ajax({
				method: 'POST',
				dataType: 'json',
				url: changeUrl,
				onsuccess: BX.proxy(function (response) {

					if (typeof(response) == 'object')
					{

						if (response.result == 'ok' && response.state == "added")
						{
							if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
							{
								new window.Alpha.Messages({
									type: 'default',
									title: BX.message('ADD_TO_WISHLIST_TITLE'),
									message: this.product.name.toString(),
									url: this.basketData.wishlist_page_url,
									urlText: 'WISHLIST'
								});
							}

							BX.addClass(this.obWishlist, 'active');
						}
						else if (response.result == 'ok' && response.state == "deleted")
						{
							if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
							{
								new window.Alpha.Messages({
									type: 'default',
									title: BX.message('DELETE_FROM_WISHLIST_TITLE'),
									message: this.product.name.toString()
								});
							}

							BX.removeClass(this.obWishlist, 'active');
						}

						BX.onCustomEvent('OnWishlistChange');

					}
					else
					{
						console.log("error change wishlist: ", response);
					}


				}, this)
			});
		},

		setWishlist: function (state) {

			state = (state) || '';

			if (state == "active")
			{
				BX.addClass(this.obWishlist, 'active');
			}
			else
			{
				BX.removeClass(this.obWishlist, 'active');
			}

		},

		compare: function(event)
		{
			var checkbox = this.obCompare.querySelector('[data-entity="compare-checkbox"]'),
				target = BX.getEventTarget(event),
				checked = true;

			if (checkbox)
			{
				checked = target === checkbox ? checkbox.checked : !checkbox.checked;
			}

			var url = checked ? this.compareData.compareUrl : this.compareData.compareDeleteUrl,
				compareLink;

			if (url)
			{
				if (target !== checkbox)
				{
					BX.PreventDefault(event);
					this.setCompared(checked);
				}

				switch (this.productType)
				{
					case 0: // no catalog
					case 1: // product
					case 2: // set
						compareLink = url.replace('#ID#', this.product.id.toString());
						break;
					case 3: // sku
						compareLink = url.replace('#ID#', this.offers[this.offerNum].ID);
						break;
				}

				BX.ajax({
					method: 'POST',
					dataType: checked ? 'json' : 'html',
					url: compareLink + (compareLink.indexOf('?') !== -1 ? '&' : '?') + 'ajax_action=Y',
					onsuccess: checked
						? BX.proxy(this.compareResult, this)
						: BX.proxy(this.compareDeleteResult, this)
				});
			}
		},

		compareResult: function(result)
		{
			

			if (!BX.type.isPlainObject(result))
				return;

			
			if (this.offers.length > 0)
			{
				this.offers[this.offerNum].COMPARED = result.STATUS === 'OK';
			}

			if (result.STATUS === 'OK')
			{
				BX.onCustomEvent('OnCompareChange');
                                
				if (!!window.Alpha.Messages && typeof(window.Alpha.Messages) == 'function')
				{
					new window.Alpha.Messages({
						type: 'addToCompare',
						title: BX.message('COMPARE_TITLE'),
						message: BX.message('COMPARE_MESSAGE_OK')
					});
				}
			}
			else
			{
				if (!!window.Alpha.Messages && typeof(window.Alpha.Messages) == 'function')
					{
						new window.Alpha.Messages({
							type: 'error',
							title: BX.message('COMPARE_TITLE'),
							message: (result.MESSAGE ? result.MESSAGE : BX.message('COMPARE_UNKNOWN_ERROR'))
						});
					}
                                
                                
			}

		},

		compareDeleteResult: function()
		{
			BX.onCustomEvent('OnCompareChange');

			if (this.offers && this.offers.length)
			{
				this.offers[this.offerNum].COMPARED = false;
			}
		},

		setCompared: function(state)
		{
			if (!this.obCompare)
				return;

			var checkbox = this.getEntity(this.obCompare, 'compare-checkbox');
			if (checkbox)
			{
				checkbox.checked = state;
			}
		},

		setCompareInfo: function(comparedIds)
		{
			if (!BX.type.isArray(comparedIds))
				return;

			for (var i in this.offers)
			{
				if (this.offers.hasOwnProperty(i))
				{
					this.offers[i].COMPARED = BX.util.in_array(this.offers[i].ID, comparedIds);
				}
			}
		},

		compareRedirect: function()
		{
			if (this.compareData.comparePath)
			{
				location.href = this.compareData.comparePath;
			}
			else
			{
				this.obPopupWin.close();
			}
		},

		checkDeletedCompare: function(id)
		{
			switch (this.productType)
			{
				case 0: // no catalog
				case 1: // product
				case 2: // set
					if (this.product.id == id)
					{
						this.setCompared(false);
					}

					break;
				case 3: // sku
					var i = this.offers.length;
					while (i--)
					{
						if (this.offers[i].ID == id)
						{
							this.offers[i].COMPARED = false;

							if (this.offerNum == i)
							{
								this.setCompared(false);
							}

							break;
						}
					}
			}
		},

		initBasketUrl: function()
		{
			this.basketUrl = (this.basketMode === 'ADD' ? this.basketData.add_url : this.basketData.buy_url);

			switch (this.productType)
			{
				case 1: // product
				case 2: // set
					this.basketUrl = this.basketUrl.replace('#ID#', this.product.id.toString());
					break;
				case 3: // sku
					this.basketUrl = this.basketUrl.replace('#ID#', this.offers[this.offerNum].ID);
					break;
			}

			this.basketParams = {
				'ajax_basket': 'Y'
			};

			if (this.config.showQuantity)
			{
				this.basketParams[this.basketData.quantity] = this.obQuantity.value;
			}

			if (this.basketData.sku_props)
			{
				this.basketParams[this.basketData.sku_props_var] = this.basketData.sku_props;
			}
		},

		fillBasketProps: function()
		{
			if (!this.visual.BASKET_PROP_DIV)
				return;

			var
				i = 0,
				propCollection = null,
				foundValues = false,
				obBasketProps = null;

			if (this.basketData.useProps && !this.basketData.emptyProps)
			{
				if (this.obPopupWin && this.obPopupWin.contentContainer)
				{
					obBasketProps = this.obPopupWin.contentContainer;
				}
			}
			else
			{
				obBasketProps = BX(this.visual.BASKET_PROP_DIV);
			}

			if (obBasketProps)
			{
				propCollection = obBasketProps.getElementsByTagName('select');
				if (propCollection && propCollection.length)
				{
					for (i = 0; i < propCollection.length; i++)
					{
						if (!propCollection[i].disabled)
						{
							switch (propCollection[i].type.toLowerCase())
							{
								case 'select-one':
									this.basketParams[propCollection[i].name] = propCollection[i].value;
									foundValues = true;
									break;
								default:
									break;
							}
						}
					}
				}

				propCollection = obBasketProps.getElementsByTagName('input');
				if (propCollection && propCollection.length)
				{
					for (i = 0; i < propCollection.length; i++)
					{
						if (!propCollection[i].disabled)
						{
							switch (propCollection[i].type.toLowerCase())
							{
								case 'hidden':
									this.basketParams[propCollection[i].name] = propCollection[i].value;
									foundValues = true;
									break;
								case 'radio':
									if (propCollection[i].checked)
									{
										this.basketParams[propCollection[i].name] = propCollection[i].value;
										foundValues = true;
									}
									break;
								default:
									break;
							}
						}
					}
				}
			}

			if (!foundValues)
			{
				this.basketParams[this.basketData.props] = [];
				this.basketParams[this.basketData.props][0] = 0;
			}
		},

		sendToBasket: function()
		{
			if (!this.canBuy)
				return;

			this.initBasketUrl();
			this.fillBasketProps();
			BX.ajax({
				method: 'POST',
				dataType: 'json',
				url: this.basketUrl,
				data: this.basketParams,
				onsuccess: BX.proxy(this.basketResult, this)
			});
		},
                
                sendGiftToBasket: function () {
                    
                    var basketUrl = this.basketData.add_url.replace('#ID#', this.giftSelectedId.toString()),
                        basketParams = {
                            'ajax_basket': 'Y'
                        }
                        
                        BX.ajax({
				method: 'POST',
				dataType: 'json',
				url: basketUrl,
				data: basketParams,
				onsuccess: BX.proxy(function () {
                                    BX.onCustomEvent('OnBasketChange');
                                }, this)
			});
                },

		add2Basket: function()
		{
			this.basketMode = 'ADD';
			this.basket();
		},

		buyBasket: function()
		{
			this.basketMode = 'BUY';
			this.basket();
		},

		basket: function()
		{
			var contentBasketProps = '';

			if (!this.canBuy)
				return;

			switch (this.productType)
			{
				case 1: // product
				case 2: // set
					if (this.basketData.useProps && !this.basketData.emptyProps)
					{
						this.initPopupWindow();
						this.obPopupWin.setTitleBar(BX.message('TITLE_BASKET_PROPS'));

						if (BX(this.visual.BASKET_PROP_DIV))
						{
							contentBasketProps = BX(this.visual.BASKET_PROP_DIV).innerHTML;
						}

						this.obPopupWin.setContent(contentBasketProps);
						this.obPopupWin.setButtons([
							new BasketButton({
								text: BX.message('BTN_SEND_PROPS'),
								events: {
									click: BX.delegate(this.sendToBasket, this)
								}
							})
						]);
						this.obPopupWin.show();
					}
					else
					{
						this.sendToBasket();
					}
					break;
				case 3: // sku
					this.sendToBasket();
					break;
			}
		},

		basketResult: function(arResult)
		{
			var productPict;

			
			if (!BX.type.isPlainObject(arResult))
				return;

			if (arResult.STATUS === 'OK')
			{
				this.setAnalyticsDataLayer('addToCart');
			}

			if (arResult.STATUS === 'OK' && this.basketMode === 'BUY')
			{
				this.basketRedirect();
			}
			else
			{
				
				if (arResult.STATUS === 'OK')
				{
					BX.onCustomEvent('OnBasketChange');
					switch (this.productType)
					{
						case 1: // product
						case 2: // set
							productPict = !!this.product.pict.SRC ? this.product.pict.SRC : null;
							break;
						case 3: // sku
							if (!!this.offers[this.offerNum].PICTURE)
								productPict = this.offers[this.offerNum].PICTURE.THUMB;
							else if (!!this.product.pict.SRC)
								productPict = this.product.pict.SRC;
                                                    
							break;
					}
                                        
					if (!!window.Alpha.Messages && typeof(window.Alpha.Messages) == 'function')
					{
						new window.Alpha.Messages({
							type: 'addToBasket',
							title: BX.message('TITLE_SUCCESSFUL'),
							message: this.product.name,
							image: productPict,
							url: this.basketData.basketUrl,
							urlText: 'BASKET'
						});
					}

					if (!!this.giftSelectedId && parseInt(this.giftSelectedId) > 0)
					{
						this.sendGiftToBasket();
					}

				}
				else
				{
					if (!!window.Alpha.Messages && typeof(window.Alpha.Messages) == 'function')
					{
						new window.Alpha.Messages({
							type: 'error',
							title: BX.message('TITLE_ERROR'),
							message: (arResult.MESSAGE ? arResult.MESSAGE : BX.message('BASKET_UNKNOWN_ERROR')),
						});
					}
 
				}

			}
		},

		basketRedirect: function()
		{
			location.href = (this.basketData.basketUrl ? this.basketData.basketUrl : BX.message('BASKET_URL'));
		},

		initPopupWindow: function()
		{
			if (this.obPopupWin)
				return;

			this.obPopupWin = BX.PopupWindowManager.create('CatalogElementBasket_' + this.visual.ID, null, {
				autoHide: false,
				offsetLeft: 0,
				offsetTop: 0,
				overlay: true,
				closeByEsc: true,
				titleBar: true,
				closeIcon: true,
				contentColor: 'white',
				className: this.config.templateTheme ? 'bx-' + this.config.templateTheme : ''
			});
		},

		incViewedCounter: function()
		{
			if (this.currentIsSet && !this.updateViewedCount)
			{
				switch (this.productType)
				{
					case 1:
					case 2:
						this.viewedCounter.params.PRODUCT_ID = this.product.id;
						this.viewedCounter.params.PARENT_ID = this.product.id;
						break;
					case 3:
						this.viewedCounter.params.PARENT_ID = this.product.id;
						this.viewedCounter.params.PRODUCT_ID = this.offers[this.offerNum].ID;
						break;
					default:
						return;
				}

				this.viewedCounter.params.SITE_ID = BX.message('SITE_ID');
				this.updateViewedCount = true;
				BX.ajax.post(
					this.viewedCounter.path,
					this.viewedCounter.params,
					BX.delegate(function()
					{
						this.updateViewedCount = false;
					}, this)
				);
			}
		},

		allowViewedCount: function(update)
		{
			this.currentIsSet = true;

			if (update)
			{
				this.incViewedCounter();
			}
		},
                
                setDiscountTimer: function(activeTo)
		{
                    activeTo = (activeTo) || null;
                    BX.removeClass(this.obDiscountTimer, 'active');
                    
                    if (!!activeTo) {
                        var dateActiveTo = new Date(activeTo);
                        if (!!dateActiveTo && dateActiveTo > new Date())
                        {
                            BX.adjust(this.obDiscountTimer.querySelector('[data-entity="days-label"]'), {html: BX.message('CT_BCE_TIMER_DAYS_1')});
                            BX.adjust(this.obDiscountTimer.querySelector('[data-entity="hours-label"]'), {html: BX.message('CT_BCE_TIMER_HOURS_1')});
                            BX.adjust(this.obDiscountTimer.querySelector('[data-entity="minutes-label"]'), {html: BX.message('CT_BCE_TIMER_MINUTES_1')});
                            BX.adjust(this.obDiscountTimer.querySelector('[data-entity="seconds-label"]'), {html: BX.message('CT_BCE_TIMER_SECONDS_1')});
                            
                            var timerFunction = BX.delegate(function () {
                                var timeDiff = new Date(Math.abs(dateActiveTo.getTime() - (new Date()).getTime())),
                                    days = timeDiff.getUTCDate(),
                                    hours = timeDiff.getUTCHours(),
                                    minutes = timeDiff.getUTCMinutes(),
                                    seconds = timeDiff.getUTCSeconds();
                                    
                                BX.adjust(this.obDiscountTimer.querySelector('[data-entity="days-value"]'), {html: days});
                                BX.adjust(this.obDiscountTimer.querySelector('[data-entity="hours-value"]'), {html: hours});
                                BX.adjust(this.obDiscountTimer.querySelector('[data-entity="minutes-value"]'), {html: minutes});
                                BX.adjust(this.obDiscountTimer.querySelector('[data-entity="seconds-value"]'), {html: seconds});
                                
                                if (days == 1)
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="days-label"]'), {html: BX.message('CT_BCE_TIMER_DAYS_1')});
                                else if (days >= 1 && days < 5)
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="days-label"]'), {html: BX.message('CT_BCE_TIMER_DAYS_2')});
                                else
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="days-label"]'), {html: BX.message('CT_BCE_TIMER_DAYS_5')});
                                
                                
                                if (hours == 1)
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="hours-label"]'), {html: BX.message('CT_BCE_TIMER_HOURS_1')});
                                else if (hours >= 1 && hours < 5)
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="hours-label"]'), {html: BX.message('CT_BCE_TIMER_HOURS_2')});
                                else
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="hours-label"]'), {html: BX.message('CT_BCE_TIMER_HOURS_5')});
                                
                                
                                if (minutes == 1)
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="minutes-label"]'), {html: BX.message('CT_BCE_TIMER_MINUTES_1')});
                                else if (minutes >= 1 && minutes < 5)
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="minutes-label"]'), {html: BX.message('CT_BCE_TIMER_MINUTES_2')});
                                else
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="minutes-label"]'), {html: BX.message('CT_BCE_TIMER_MINUTES_5')});
                                
                                
                                if (seconds == 1)
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="seconds-label"]'), {html: BX.message('CT_BCE_TIMER_SECONDS_1')});
                                else if (seconds >= 1 && seconds < 5)
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="seconds-label"]'), {html: BX.message('CT_BCE_TIMER_SECONDS_2')});
                                else
                                    BX.adjust(this.obDiscountTimer.querySelector('[data-entity="seconds-label"]'), {html: BX.message('CT_BCE_TIMER_SECONDS_5')});

                            }, this);
                            
                            BX.addClass(this.obDiscountTimer, 'active');
                            timerFunction();
                            
                            setInterval(timerFunction, 1000);
                            

                        }
                    }
		},
	}
})(window);
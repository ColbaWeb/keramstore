<?
use Nextype\Alpha\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if($arResult["PHONE_REGISTRATION"])
{
	CJSCore::Init('phone_auth');
}


if (isset($arParams["~AUTH_RESULT"]['TYPE']) && $arParams["~AUTH_RESULT"]['TYPE'] == "OK")
{
    ?><div class="alert alert-success"><?=$arParams["~AUTH_RESULT"]['MESSAGE']?></div><?
}

if($arResult["SHOW_FORM"])
{
?>

<div class="auth-registration auth-registration-container">
    <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" class="forgot-password">
        <input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="CHANGE_PWD">
        
        <div class="auth-title font-h3 mb-4"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></div>
        
        <?
        
        if (isset($arParams["~AUTH_RESULT"]['TYPE']) && $arParams["~AUTH_RESULT"]['TYPE'] == "ERROR")
        {
            ?><div class="alert alert-danger"><?=$arParams["~AUTH_RESULT"]['MESSAGE']?></div><?
        }
        elseif (isset($arParams["~AUTH_RESULT"]['TYPE']) && $arParams["~AUTH_RESULT"]['TYPE'] == "OK")
        {
            ?><div class="alert alert-success"><?=$arParams["~AUTH_RESULT"]['MESSAGE']?></div><?
        }
        
        if($arResult["PHONE_REGISTRATION"])
        {
            ?>
                <input type="hidden" name="USER_PHONE_NUMBER" value="<?=htmlspecialcharsbx($arResult["USER_PHONE_NUMBER"])?>" />
                <div class="custom-input-wrap">
                    <div class="custom-input">
                        <input type="text" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" autocomplete="off" />
                        <label><?echo GetMessage("sys_auth_chpass_code")?>*</label>
                    </div>
                </div>
                
                <script>
                new BX.PhoneAuth({
                        containerId: 'bx_chpass_resend',
                        errorContainerId: 'bx_chpass_error',
                        interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                        data:
                                <?=CUtil::PhpToJSObject([
                                        'signedData' => $arResult["SIGNED_DATA"]
                                ])?>,
                        onError:
                                function(response)
                                {
                                        var errorDiv = BX('bx_chpass_error');
                                        var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
                                        errorNode.innerHTML = '';
                                        for(var i = 0; i < response.errors.length; i++)
                                        {
                                                errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
                                        }
                                        errorDiv.style.display = '';
                                }
                });
                </script>

                <div id="bx_chpass_error" class="alert alert-danger" style="display:none"><?ShowError("error")?></div>

                <div id="bx_chpass_resend" class="mb-4 small" style="margin-top: -15px;"></div>
            <?
        }
        else
        {
            ?>
                <input type="hidden" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" />
            <?
            
            if($arResult["USE_PASSWORD"])
            {
                ?>
                <div class="custom-input-wrap">
                    <div class="custom-input">
                        <input type="password" name="USER_CURRENT_PASSWORD" maxlength="255" value="<?=$arResult["USER_CURRENT_PASSWORD"]?>" autocomplete="new-password" />
                        <label><?echo GetMessage("sys_auth_changr_pass_current_pass")?>*</label>
                    </div>
                </div>
                <?
            }
            else
            {
                ?>
                <input type="hidden" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" autocomplete="off" />
                
                <?
            }
            
        }

        ?>

        
        <div class="custom-input-wrap">
            <div class="custom-input">
                <input type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="new-password" />
                <label><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?>*</label>
            </div>
            
            <?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
            <script>
            document.getElementById('bx_auth_secure').style.display = 'inline-block';
            </script>
            <?endif?>
        </div>
                
        <div class="custom-input-wrap">
            <div class="custom-input">
                <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" />
                <label><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?>*</label>
            </div>
        </div>
                
        <?
        if($arResult["USE_CAPTCHA"])
        {
            echo Tools\Forms::getCaptchaField('<img src="/bitrix/tools/captcha.php?captcha_sid='.$arResult["CAPTCHA_CODE"].'" width="180" height="40" alt="CAPTCHA" />', '<input type="hidden" name="captcha_sid" value="'.$arResult["CAPTCHA_CODE"].'" /><input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />');
        }
        ?>
        
        <button type="submit" name="change_pwd" class="btn btn-primary btn-large"><?=GetMessage("AUTH_CHANGE")?></button>
        
        
    </form>
</div>

<?
}
?>
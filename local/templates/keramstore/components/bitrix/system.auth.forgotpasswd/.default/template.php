<?
use Nextype\Alpha\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

?>

<div class="auth-registration auth-registration-container">
    <form name="bform" method="post" class="forgot-password" target="_top" action="<?=$arResult["AUTH_URL"]?>">
        
        <input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="SEND_PWD">
        
        <div class="auth-title font-h3"><?echo GetMessage("FORGOT_PASSWORD_TITLE")?></div>
        
        <?
        if($arResult["PHONE_REGISTRATION"])
        {
            ?><div class="auth-desc font-body-2"><?=GetMessage('sys_forgot_pass_note_phone')?></div><?
        }
        else
        {
            ?><div class="auth-desc font-body-2"><?=GetMessage('sys_forgot_pass_note_email')?></div><?
        }
        
        if (isset($arParams["~AUTH_RESULT"]['TYPE']) && $arParams["~AUTH_RESULT"]['TYPE'] == "ERROR")
        {
            ?><div class="alert alert-danger"><?=$arParams["~AUTH_RESULT"]['MESSAGE']?></div><?
        }
        elseif (isset($arParams["~AUTH_RESULT"]['TYPE']) && $arParams["~AUTH_RESULT"]['TYPE'] == "OK")
        {
            ?><div class="alert alert-success"><?=$arParams["~AUTH_RESULT"]['MESSAGE']?></div><?
        }
        
        
        if($arResult["PHONE_REGISTRATION"])
        {
            ?>
            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="text" name="USER_PHONE_NUMBER" value="<?=$request->get("USER_PHONE_NUMBER")?>" data-field-type="phone" required="required" id='user_phone_number'>
                    <label for="user_phone_number"><?echo GetMessage("sys_forgot_pass_phone")?>*</label>
                </div>
            </div>
            <?
        }
        else
        {
            ?>
            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="text" name="USER_EMAIL" value="<?=$request->get("USER_LOGIN")?>" required="required" id='user_email'>
                    <label for="user_email"><?echo GetMessage("sys_forgot_pass_email")?>*</label>
                </div>
            </div>
            <?
        }
        
        if ($arResult["USE_CAPTCHA"])
        {
            echo Tools\Forms::getCaptchaField('<img src="/bitrix/tools/captcha.php?captcha_sid=' . $arResult["CAPTCHA_CODE"] . '" width="180" height="40" alt="CAPTCHA" />', '<input type="hidden" name="captcha_sid" value="' . $arResult["CAPTCHA_CODE"] . '" /><input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />');
        }
        
        ?>
        
        
        <button type="submit" name="send_account_info" value="y" class="btn btn-primary btn-large"><?=GetMessage("AUTH_SEND")?></button>
        
        <div class="subtitle font-body-small">
            <?=GetMessage('sys_auth_and_reg', array (
                "#AUTH_AUTH_URL#" => $arResult["AUTH_AUTH_URL"],
                "#REGISTRATION_URL#" => $APPLICATION->GetCurPageParam('register=yes', array ('forgot_password', 'login'))
            ))?>
        </div>
    </form>
</div>
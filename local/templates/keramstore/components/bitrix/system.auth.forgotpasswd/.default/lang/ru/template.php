<?
$MESS["AUTH_SEND"] = "Получить новый пароль";
$MESS["AUTH_AUTH"] = "Авторизация";
$MESS["FORGOT_PASSWORD_TITLE"] = "Напомнить пароль";
$MESS["system_auth_captcha"] = "Введите символы с картинки:";
$MESS["sys_forgot_pass_label"] = "Выберите, какую информацию использовать для изменения пароля:";
$MESS["sys_forgot_pass_email"] = "E-mail";
$MESS["sys_forgot_pass_phone"] = "Номер телефона";
$MESS["sys_forgot_pass_note_email"] = "Ссылку на смену пароля мы пришлем на e-mail, указанный при регистрации.";
$MESS["sys_forgot_pass_note_phone"] = "На ваш номер телефона будет выслано СМС с кодом для смены пароля.";
$MESS["sys_auth_and_reg"] = "Также можно <a href=\"#AUTH_AUTH_URL#\">войти</a> или <a href=\"#REGISTRATION_URL#\">зарегестрироваться</a>";
?>
<?php
use Nextype\Alpha\Tools;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;
use Bitrix\Main\Loader;

define('DISABLED_REGIONS', true);
define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if (!Loader::includeModule('nextype.alpha'))
    die;

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

try
{
    if (!empty($request->get('action')) && in_array($request->get('action'), array ('add', 'to-basket', 'delete', 'clear')))
    {
        if ($request->get('action') == "add" && !empty($request->get('id')))
        {
            Tools\Wishlist::addItem(intval($request->get('id')));
        }
        elseif ($request->get('action') == "to-basket" && !empty($request->get('id')))
        {
            Tools\Wishlist::toBasket(intval($request->get('id')));
        }
        elseif ($request->get('action') == "delete" && !empty($request->get('id')))
        {
            Tools\Wishlist::deleteItem(intval($request->get('id')));
        }
        elseif ($request->get('action') == "clear")
        {
            Tools\Wishlist::clear();
        }
    }
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
    die();
}
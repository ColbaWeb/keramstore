<?
use Nextype\Alpha\Tools;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;
use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (!Loader::includeModule('nextype.alpha'))
    die;

if (Options\Base::getInstance(SITE_ID)->getValue('wishList'))
{
    $arWishlist = Tools\Wishlist::getList();
    
    $arItemsList = array ();
    if (!empty($arResult['ITEMS']) && is_array($arResult['ITEMS']) && !empty($arWishlist))
    {
        foreach ($arResult['ITEMS'] as $arGroupItems)
        {
            if (is_array($arGroupItems) && !empty($arGroupItems))
            {
                foreach ($arGroupItems as $item)
                {
                    if (isset($arWishlist[$item['PRODUCT_ID']]) && !isset($arItemsList[$item['PRODUCT_ID']]))
                    {
                        $arItemsList[] = $item;
                    }
                }
            }
        }
    }
    
    echo "<pre>";
    //print_r($arWishlist);
    echo "</pre>";
    
    $arResult['ITEMS'] = $arItemsList;
    unset($arItemsList);
}
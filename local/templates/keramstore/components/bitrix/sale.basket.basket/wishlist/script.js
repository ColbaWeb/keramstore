(function (window) {
    'use strict';


    window.Alpha.ComponentWishlist = function (params)
    {
        this.params = {};
        this.obContainer = null;

        if (params.containerId !== undefined && !!params.containerId)
        {
            this.params = params;
            this.obContainer = BX(params.containerId);
            
            if (!!this.obContainer)
            {
                this.init();
            }
        }
    };

    window.Alpha.ComponentWishlist.prototype = {

        init: function () {
            var _ = this;

            var obBtnToBasket = this.obContainer.querySelectorAll('[data-entity="to-basket"]');
            if (!!obBtnToBasket && obBtnToBasket.length > 0)
            {
                for (var i=0; i < obBtnToBasket.length; i++)
                {
                    BX.bind(obBtnToBasket[i], 'click', BX.delegate(function () {
                        var target = BX.proxy_context;
                        if (target.getAttribute('data-entity-id') !== undefined)
                        {
                            this.sendRequest({
                                action: 'to-basket',
                                id: target.getAttribute('data-entity-id')
                            });
                            
                            if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                            {
                                new window.Alpha.Messages({
                                    type: 'addToBasket',
                                    title: BX.message('SUCCESS_ADD_PRODUCT_TO_BASKET'),
                                    message: target.getAttribute('data-entity-name'),
                                });
                            }
                        }
                        
                    }, this));
                }
            }
            
            var obBtnDelete = this.obContainer.querySelectorAll('[data-entity="delete"]');
            if (!!obBtnDelete && obBtnDelete.length > 0)
            {
                for (var i=0; i < obBtnDelete.length; i++)
                {
                    BX.bind(obBtnDelete[i], 'click', BX.delegate(function () {
                        var target = BX.proxy_context;
                        
                        if (target.getAttribute('data-entity-id') !== undefined)
                        {
                            this.sendRequest({
                                action: 'delete',
                                id: target.getAttribute('data-entity-id')
                            });
                            
                            if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                            {
                                new window.Alpha.Messages({
                                    type: 'default',
                                    title: BX.message('DELETE_FROM_WISHLIST_TITLE'),
                                    message: target.getAttribute('data-entity-name')
                                });
                            }
                        }
                        
                    }, this));
                }
            }
            
            var obBtnClear = this.obContainer.querySelectorAll('[data-entity="clear"]');
            
            if (!!obBtnClear && obBtnClear.length > 0)
            {
                for (var i=0; i < obBtnClear.length; i++)
                {
                    BX.bind(obBtnClear[i], 'click', BX.delegate(function () {

                        var target = BX.proxy_context;

                        this.sendRequest({
                            action: 'clear'
                        });

                    }, this));
                }
            }

        },

        sendRequest: function (data)
        {
            var _ = this;
            data = (data) || {};
            
            data.sessid = BX.bitrix_sessid();
            data.siteId = this.params.siteId;
            data.templateName = this.params.templateName;
            data.signedParamsString = this.params.signedParamsString;
            data.charset = BX.message('LANG_CHARSET');
            
            BX.ajax({
                url: this.params.ajaxPath,
                method: 'POST',
                dataType: 'html',
                data: data,
                timeout: 10,
                emulateOnload: true,
                onsuccess: BX.delegate(function (result) {
                    
                    if (!result)
                        return;

                    var ob = BX.processHTML(result);

                    BX(this.params.containerId).innerHTML = ob.HTML;
                    BX.ajax.processScripts(ob.SCRIPT, true);
                    BX.onCustomEvent('OnWishlistChange');
                    BX.onCustomEvent('OnBasketChange');
                    
                    this.init();
                    
                }, this)
            });

        },

    };

})(window);

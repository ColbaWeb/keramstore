<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
?>
<div class="wrapper-section">
    <div class="favorites-section empty">
        <div class="content">
            <div class="image">
                <svg xmlns="http://www.w3.org/2000/svg" width="96" height="96" viewBox="0 0 96 96" fill="none">
                    <path d="M77.7638 24.9372L77.7641 24.9375C79.4286 26.4981 80.7453 28.3476 81.6423 30.3784L82.5571 29.9744L81.6423 30.3784C82.5393 32.4091 83 34.5831 83 36.777C83 38.9709 82.5393 41.145 81.6423 43.1756C80.7453 45.2065 79.4286 47.0559 77.7641 48.6166L77.764 48.6167L74.1128 52.0417L47.9991 76.538L21.8853 52.0417L18.2342 48.6167C14.8745 45.4651 13 41.2047 13 36.777C13 32.3493 14.8745 28.089 18.2342 24.9373C21.5961 21.7837 26.1687 20.0017 30.949 20.0017C35.7293 20.0017 40.3019 21.7837 43.6638 24.9373L47.3149 28.3623L47.9991 29.0041L48.6832 28.3623L52.3344 24.9373L52.3345 24.9372C53.9983 23.3757 55.9772 22.1339 58.1596 21.2856C60.3421 20.4373 62.6834 20 65.0492 20C67.415 20 69.7562 20.4373 71.9387 21.2856C74.1211 22.1339 76.1 23.3757 77.7638 24.9372Z" stroke="black" stroke-opacity="0.4" stroke-width="2"/>
                </svg>
            </div>
            <div class="text font-title bold"><?=Loc::getMessage("SBB_EMPTY_BASKET_TITLE")?></div>
            
            <a href="<?=$arParams['EMPTY_BASKET_HINT_PATH']?>" class="btn btn-primary btn-large"><?=Loc::getMessage("SBB_EMPTY_BASKET_HINT")?> <i class="icon icon-arrow-right-text-button-white"></i></a>
        </div>
    </div>
</div>
<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Bitrix\Main\Grid\Declension;

/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */
$arParams['USE_DYNAMIC_SCROLL'] = isset($arParams['USE_DYNAMIC_SCROLL']) && $arParams['USE_DYNAMIC_SCROLL'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_FILTER'] = isset($arParams['SHOW_FILTER']) && $arParams['SHOW_FILTER'] === 'N' ? 'N' : 'Y';
$arParams['PRICE_DISPLAY_MODE'] = isset($arParams['PRICE_DISPLAY_MODE']) && $arParams['PRICE_DISPLAY_MODE'] === 'N' ? 'N' : 'Y';
$arParams['USE_PRICE_ANIMATION'] = isset($arParams['USE_PRICE_ANIMATION']) && $arParams['USE_PRICE_ANIMATION'] === 'N' ? 'N' : 'Y';
$arParams['EMPTY_BASKET_HINT_PATH'] = isset($arParams['EMPTY_BASKET_HINT_PATH']) ? (string) $arParams['EMPTY_BASKET_HINT_PATH'] : '/';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

$containerId = "bx_wishlist_" . $this->randString();

Layout\Assets::getInstance(SITE_ID)->addLess(SITE_TEMPLATE_PATH . "/less/favorites.less");
?>
<div id="<?=$containerId?>">
<?
if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
    $APPLICATION->RestartBuffer();

if (empty($arResult['ERROR_MESSAGE']) && Options\Base::getInstance(SITE_ID)->getValue('wishList') && !empty($arResult['ITEMS']))
{
    $itemDeclension = new Declension(GetMessage('SBB_GOOD'), GetMessage('SBB_GOOD_2'), GetMessage('SBB_GOODS'));
    
    ?>
    <div class="favorites-section">
        <div class="content">
            <div class="mobile-buttons">
                <a href="javascript:void(0)" data-entity="clear" class="delete font-large-button">
                    <?=GetMessage('SBB_DELETE_ALL')?>
                    <i class="icon icon-trash"></i>
                </a>
            </div>
            <div class="sort-filter">
                
                <div class="quantity font-body-2"><span class='count'><?=count($arResult['ITEMS'])?></span> <span class="quantity-text"><?=$itemDeclension->get(count($arResult['ITEMS']))?></span></div>

                <div class="buttons">
                    <a href="javascript:void(0);" data-entity="clear" class="delete font-large-button">
                        <?=GetMessage('SBB_DELETE_ALL')?>
                        <i class="icon icon-trash"></i>
                    </a>
                </div>
            </div>

            <div class="product-items-block">
                <div class="product-items items row items-col-3">
                    
                    <?
                    foreach ($arResult['ITEMS'] as $item)
                    {
                        $imageSrc = isset($item['PREVIEW_PICTURE_SRC_2X']) && !empty($item['PREVIEW_PICTURE_SRC_2X']) ? $item['PREVIEW_PICTURE_SRC_2X'] : null;
                        
                        if (Options\Base::getInstance(SITE_ID)->getValue('catalogReviews'))
                        {
                            $reviewsSummary = Reviews\Base::getInstance()->getSummaryByProduct($item['PRODUCT_ID']);
                        }
                    ?>
                    <div class="item-wrap col">
                        <div class="product-card item">
                            <div class="product-image-block">
                                <?
                                if (isset($reviewsSummary['avg']) && $reviewsSummary['avg'] > 0)
                                {
                                    ?>
                                    <div class="label badge-in-stock">
                                        <div class="rating">
                                            <i class="icon icon-star-12"></i>
                                            <div class="value"><?= number_format($reviewsSummary['avg'], 1, ",", " ")?></div>
                                        </div>
                                    </div>
                                    <?
                                }
                                ?>
                                
                                <a href="<?=$item['DETAIL_PAGE_URL']?>" class="image-slider">
                                    <?
                                    echo Layout\Assets::showBackgroundImage($imageSrc, array(
                                        "attributes" => array(
                                            "class=\"product-image\""

                                            )
                                    ));
                                    ?>
                                </a>
                            </div>
                            <div class="product-info">
                                <div class="price-block">
                                    <div class="price font-body">
                                        <?=$item['PRICE_FORMATED']?>
                                    </div>
                                    
                                    <?
                                    if ($item['SUM_DISCOUNT_PRICE'] > 0)
                                    {
                                        ?><div class="old-price font-body-small"><span><?=$item['FULL_PRICE_FORMATED']?></span></div><?
                                    }
                                    
                                    if ($item['DISCOUNT_PRICE_PERCENT'] > 0)
                                    {
                                        ?><div class="sale label action"><?="-" . $item['DISCOUNT_PRICE_PERCENT_FORMATED']?></div><?
                                    }
                                    ?>
                                    
                                </div>
                                <a href="<?=$item['DETAIL_PAGE_URL']?>" class="name font-title"><?=$item['NAME']?></a>
                                
                                <div class="buttons">
                                    <a href="javascript:void(0)" data-entity="to-basket" data-entity-name="<?= htmlentities($item['NAME'])?>" data-entity-id="<?=$item['ID']?>" class="btn btn-primary btn-medium to-basket">
                                        <span><?=GetMessage('SBB_TO_BASKET')?></span>
                                        <i class="icon icon-basket-white"></i>
                                    </a>
                                    <a href="javascript:void(0)" data-entity="delete" data-entity-name="<?= htmlentities($item['NAME'])?>" data-entity-id="<?=$item['ID']?>" class="btn btn-secondary btn-delete"><i class="icon icon-trash"></i></a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?
                    }
                    ?>
                    

                </div>
            </div>
        </div>
    </div>
    <?
}
elseif (empty($arResult['ITEMS']))
{
    include(Main\Application::getDocumentRoot() . $templateFolder . '/empty.php');
}
else
{
    ShowError($arResult['ERROR_MESSAGE']);
}

if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
    return;

$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'bitrix.sale.basket.basket');
?>
</div>

<script>
        BX.ready(function () {
            BX.message({
                SUCCESS_ADD_PRODUCT_TO_BASKET: '<?= GetMessageJS('SBB_SUCCESS_ADD_PRODUCT_TO_BASKET') ?>',
                BASKET_URL: '<?= $arParams['BASKET_URL'] ?>'
            });
            
            new window.Alpha.ComponentWishlist({
                siteId: '<?=SITE_ID?>',
                containerId: '<?=CUtil::JSEscape($containerId)?>',
                ajaxPath: '<?=$templateFolder?>/ajax.php',
                templateName: '<?=CUtil::JSEscape($templateName)?>',
                signedParamsString: '<?= CUtil::JSEscape($signedParams) ?>'
            });

        });
    </script>
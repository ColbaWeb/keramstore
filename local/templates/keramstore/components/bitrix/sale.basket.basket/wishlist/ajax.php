<?php
use Nextype\Alpha\Tools;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;
use Bitrix\Main\Loader;

define('DISABLED_REGIONS', true);
define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

if (!isset($_REQUEST['siteId']) || !is_string($_REQUEST['siteId']))
    die();

if (!isset($_REQUEST['templateName']) || !is_string($_REQUEST['templateName']))
    die();

if ($_SERVER['REQUEST_METHOD'] != 'POST' ||
    preg_match('/^[A-Za-z0-9_]{2}$/', $_POST['siteId']) !== 1 ||
    preg_match('/^[.A-Za-z0-9_-]+$/', $_POST['templateName']) !== 1)
    die;

define('SITE_ID', $_REQUEST['siteId']);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if (!Loader::includeModule('nextype.alpha'))
    die;

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);


$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
    $signedParamsString = $request->get('signedParamsString') ?: '';
    $params = $signer->unsign($signedParamsString, 'bitrix.sale.basket.basket');
    $params = unserialize(base64_decode($params));
    
    if (!empty($request->get('action')) && in_array($request->get('action'), array ('to-basket', 'delete', 'clear')))
    {
        if ($request->get('action') == "to-basket" && !empty($request->get('id')))
        {
            Tools\Wishlist::toBasket(intval($request->get('id')));
        }
        elseif ($request->get('action') == "delete" && !empty($request->get('id')))
        {
            Tools\Wishlist::deleteBasketItem(intval($request->get('id')));
        }
        elseif ($request->get('action') == "clear")
        {
            Tools\Wishlist::clear();
        }
    }
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
    die();
}

$params['AJAX_REQUEST'] = 'Y';

$APPLICATION->RestartBuffer();
header('Content-Type: text/html; charset='.LANG_CHARSET);

$APPLICATION->IncludeComponent('bitrix:sale.basket.basket', $request->get('templateName'), $params);
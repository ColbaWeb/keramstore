<?php

use Nextype\Alpha\Layout\Assets;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

Assets::getInstance()->addLess(array(
    SITE_TEMPLATE_PATH . '/less/categories-with-photo.less',
));
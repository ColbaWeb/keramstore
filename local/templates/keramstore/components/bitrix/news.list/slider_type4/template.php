<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
    Nextype\Alpha\Layout\Assets;

$arSlide = $arResult['SLIDE'];
?>
<div class="wrapper-section">
    <div class="hero type-4">
    <?
        $this->AddEditAction($arSlide['ID'], $arSlide['EDIT_LINK'], CIBlock::GetArrayByID($arSlide["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arSlide['ID'], $arSlide['DELETE_LINK'], CIBlock::GetArrayByID($arSlide["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
        <div class="head" data-src="<?=$arSlide['DETAIL_PICTURE']['SRC']?>" data-role="lazyload-background" id="<?=$this->GetEditAreaId($arSlide['ID']);?>">
            <?=Assets::showSpinner()?>
            <div class="title-block">
                <div class="title d4"><?=$arSlide['PROPERTIES']['TITLE']['VALUE']?></div>
                <div class="subtitle font-lead"><?=$arSlide['PROPERTIES']['DESCRIPTION']['VALUE']?></div>
            </div>
            <?if (!empty($arSlide['PROPERTIES']['EXTRA_SECTIONS']['VALUE'])):?>
                <div class="tags">
                    <div class="tags-inner">
                        <?foreach ($arSlide['PROPERTIES']['EXTRA_SECTIONS']['VALUE'] as $sectionID):?>
                            <a href="<?=$arResult['EXTRA_SECTIONS'][(int)$sectionID]['URL']?>" class="btn btn-tag">
                                <?=$arResult['EXTRA_SECTIONS'][(int)$sectionID]['NAME']?>
                            </a>
                        <?endforeach?>
                    </div>
                </div>
            <?endif?>
        </div>
        <?if (!empty($arSlide['PROPERTIES']['EXTRA_SECTIONS']['VALUE'])):?>
            <div class="content">
                <? $GLOBALS['arrFilterSliderSections'] = array(
                    '=ID' => $arResult['LINKED_SECTIONS']
                );
                $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list",
                        "categories_with_photo_homepage_slider",
                        Array(
                                "CACHE_FILTER" => $arParams['CACHE_FILTER'],
                                "CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
                                "CACHE_TIME" => $arParams['CACHE_TIME'],
                                "CACHE_TYPE" => $arParams['CACHE_TYPE'],
                                "FILTER_NAME" => "arrFilterSliderSections",
                                "IBLOCK_ID" => $arResult['CATALOG_IBLOCK_ID'],
                                "SECTION_COUNT" => "5",
                                "SHOW_PRODUCTS_COUNT" => $arParams['SHOW_PRODUCTS_COUNT'],
                                "SHOW_CATEGORY_DESCTIPTION" => $arParams['SHOW_CATEGORY_DESCTIPTION'],
                                "SHOW_CATEGORY_BUTTON" => $arParams['SHOW_CATEGORY_BUTTON']
                        )
                );?>
            </div>
        <?endif?>
    </div>
</div>

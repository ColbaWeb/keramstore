<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult['ITEMS']))
{
    $arSection = \Bitrix\Iblock\SectionTable::getList(array(
        'filter' => array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ACTIVE' => 'Y',
            'CODE' => array('big')
        ), 
        'select' =>  array('ID'),
        'limit' => 1
    ))->fetch();
    
    if (!$arSection)
    {
        ShowError(GetMessage('T_SECTION_NOT_FOUND'));
        return;
    }
        
    $arExtraSections = $arResult['LINKED_SECTIONS'] = array();
    foreach ($arResult['ITEMS'] as $arItem)
    {
        if ($arItem['IBLOCK_SECTION_ID'] != $arSection['ID'])
            continue;
        
        $arItem['EXTRA_SECTIONS'] = $arItem['PROPERTIES']['EXTRA_SECTIONS']['VALUE'];
        if (is_array($arItem['EXTRA_SECTIONS']))
        {
            foreach ($arItem['EXTRA_SECTIONS'] as $sectionID)
            {
                if (!in_array($sectionID, $arExtraSections) && !empty($sectionID))
                    $arExtraSections[] = $sectionID;
            }
        }
        elseif(!empty($arItem['EXTRA_SECTIONS']))
        {
            $arExtraSections[] = $sectionID;
        }
        
        $arItem['LINKED_SECTIONS'] = $arItem['PROPERTIES']['MAIN_SECTIONS']['VALUE'];
        if (is_array($arItem['LINKED_SECTIONS']))
        {
            foreach ($arItem['LINKED_SECTIONS'] as $sectionID)
            {
                if (!in_array($sectionID, $arResult['LINKED_SECTIONS']) && !empty($sectionID))
                    $arResult['LINKED_SECTIONS'][] = $sectionID;
            }
        }
        elseif(!empty($arItem['LINKED_SECTIONS']))
        {
            $arResult['LINKED_SECTIONS'][] = $sectionID;
        }

        $arResult['SLIDE'] = $arItem;
        break;
    }
    
    $arResult['EXTRA_SECTIONS'] = array();
    
    if (!empty($arExtraSections))
    {
        $arSections = \Nextype\Alpha\Tools\Cache::CIBlockSection_GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', '=ID' => $arExtraSections));
        if ($arSections)
        {
            foreach ($arSections as $arSection)
            {
                $arResult['EXTRA_SECTIONS'][(int)$arSection['ID']] = array(
                    'NAME' => $arSection['NAME'],
                    'URL' => $arSection['SECTION_PAGE_URL']
                );
            }
            $arResult['CATALOG_IBLOCK_ID'] = $arSection['IBLOCK_ID'];
        }
    }
}
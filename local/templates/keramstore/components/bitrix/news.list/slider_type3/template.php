<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
    Nextype\Alpha\Layout\Assets;
$arSlide = $arResult['SLIDE'];
?>

<?if (!empty($arSlide)):?>
    <?
        $this->AddEditAction($arSlide['ID'], $arSlide['EDIT_LINK'], CIBlock::GetArrayByID($arSlide["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arSlide['ID'], $arSlide['DELETE_LINK'], CIBlock::GetArrayByID($arSlide["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="wrapper-section">
        <div class="hero type-3" >
            <div class="head" data-src="<?=$arSlide['DETAIL_PICTURE']['SRC']?>" data-role="lazyload-background" id="<?=$this->GetEditAreaId($arSlide['ID']);?>">
                <?=Assets::showSpinner()?>
                <div class="title-block">
                    <div class="title d4"><?=$arSlide['PROPERTIES']['TITLE']['VALUE']?></div>
                    <div class="subtitle font-lead"><?=$arSlide['PROPERTIES']['DESCRIPTION']['VALUE']?></div>
                </div>
                <?if (!empty($arResult['EXTRA_SECTIONS']) && !empty($arSlide['PROPERTIES']['EXTRA_SECTIONS']['VALUE'])):?>
                <div class="tags">
                    <div class="tags-inner">
                        <?foreach ($arSlide['PROPERTIES']['EXTRA_SECTIONS']['VALUE'] as $sectionID):?>
                            <a href="<?=$arResult['EXTRA_SECTIONS'][(int)$sectionID]['URL']?>" class="btn btn-tag">
                                <?=$arResult['EXTRA_SECTIONS'][(int)$sectionID]['NAME']?>
                            </a>
                        <?endforeach?>
                    </div>
                </div>
                <?endif?>
            </div>
        </div>
    </div>
<?endif?>

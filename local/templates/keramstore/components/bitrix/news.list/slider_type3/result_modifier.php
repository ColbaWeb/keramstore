<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult['ITEMS']))
{
    $arSection = \Bitrix\Iblock\SectionTable::getList(array(
        'filter' => array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ACTIVE' => 'Y',
            'CODE' => 'big'
        ), 
        'select' =>  array('ID')
    ))->fetch();
    
    if (!$arSection)
    {
        ShowError(GetMessage('T_SECTION_NOT_FOUND'));
        return;
    }
    
    $arSlides = $arExtraSections = array();
    foreach ($arResult['ITEMS'] as $arItem)
    {
        if ($arItem['IBLOCK_SECTION_ID'] != $arSection['ID'])
            continue;

        $arItem['TITLE'] = $arItem['PROPERTIES']['TITLE']['VALUE'];
        $arItem['DESCRIPTION'] = $arItem['PROPERTIES']['DESCRIPTION']['VALUE'];
        
        $arItem['EXTRA_SECTIONS'] = $arItem['PROPERTIES']['EXTRA_SECTIONS']['VALUE'];
        if (is_array($arItem['EXTRA_SECTIONS']))
        {
            foreach ($arItem['EXTRA_SECTIONS'] as $sectionID)
            {
                if (!in_array($sectionID, $arExtraSections) && !empty($sectionID))
                    $arExtraSections[] = $sectionID;
            }
        }
        elseif(!empty($arItem['EXTRA_SECTIONS']))
        {
            $arExtraSections[] = $sectionID;
        }
        
        $arResult['SLIDE'] = $arItem;
        break;
    }
    unset($arSection);
    
    $arResult['EXTRA_SECTIONS'] = array();
    
    if (!empty($arExtraSections))
    {
        $arSections = \Nextype\Alpha\Tools\Cache::CIBlockSection_GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', '=ID' => $arExtraSections));
        if ($arSections)
        {
            foreach ($arSections as $arSection)
            {
                $arResult['EXTRA_SECTIONS'][(int)$arSection['ID']] = array(
                    'NAME' => $arSection['NAME'],
                    'URL' => $arSection['SECTION_PAGE_URL']
                );
            }
        }
    }
}
<?php

use Nextype\Alpha\Layout,
    Nextype\Alpha\Application;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$callbackFunctionName = $templateData['CONTAINER_ID'] . "Callback";
?>

<script>
    BX.ready(BX.delegate(function () {

        var sliderMobile = false;
        var obSlider = null;

        <? if(!Application::getInstance()->isMobile()): ?>
        $('#<?=$templateData['CONTAINER_ID']?> .hero .item').attr('data-role', 'lazyload-background');
        <? endif; ?>

        var <?=$callbackFunctionName?> = function ()
        {
            if ((sliderMobile === false) && document.body.clientWidth <= 1200)
            {
                $('#<?=$templateData['CONTAINER_ID']?> .hero .items').addClass('swiper-wrapper').wrap('<div class="swiper-container"></div>');
                $('#<?=$templateData['CONTAINER_ID']?> .hero').append('<div class="swiper-pagination"></div>');
                $('#<?=$templateData['CONTAINER_ID']?> .hero .item').each(function(){
                    $(this).addClass('swiper-slide');
                    if ($(this).data('temp-src') !== undefined) {
                        $(this).attr({
                            'data-src': $(this).data('temp-src'),
                            'data-background': $(this).data('temp-src'),
                            'data-role': false
                        });
                    }
                });

                obSlider = new Swiper('#<?=$templateData['CONTAINER_ID']?> .swiper-container', {
                    slidesPerView: 1,
                    spaceBetween: 12,
                    lazy: true,
                    preloadImages: false,
                    watchSlidesVisibility: 1,
                    pagination: {
                        el: '#<?=$templateData['CONTAINER_ID']?> .swiper-pagination',
                        clickable: true,
                    },

                    navigation: {
                        nextEl: '#<?=$templateData['CONTAINER_ID']?> .swiper-button-next',
                        prevEl: '#<?=$templateData['CONTAINER_ID']?> .swiper-button-prev',
                    },

                });

                sliderMobile = true;
            }

            else if (document.body.clientWidth > 1200)
            {
                $('#<?=$templateData['CONTAINER_ID']?> .hero .item').each(function(){
                    $(this).removeClass('swiper-slide');
                    if ($(this).data('temp-src') !== undefined) {
                        $(this).attr({
                            'data-src': $(this).data('temp-src'),
                            'data-background': $(this).data('temp-src'),
                            'data-role': 'lazyload-background'
                        });
                    }
                });

                $('#<?=$templateData['CONTAINER_ID']?> [data-role="lazyload-background"]').each(function(){
                    $(this).css({
                        'background-image': 'url("' + $(this).data('temp-src') + '")'
                    });
                });

                $('#<?=$templateData['CONTAINER_ID']?> [data-role="lazyload-background"]').Lazy({
                    effect: "fadeIn",
                    effectTime: 250,
                    threshold: 0,

                    afterLoad: function(element) {
                        element.find('.swiper-lazy-preloader').remove();

                    }
                });

                new window.Alpha.LazyForceLoad({
                    parentNode: $('#<?=$templateData['CONTAINER_ID']?>')
                });

                if (sliderMobile === true) {
                    $('#<?=$templateData['CONTAINER_ID']?> .hero .items').removeClass('swiper-wrapper').unwrap();
                    $('#<?=$templateData['CONTAINER_ID']?> .hero .swiper-pagination').remove();
                    obSlider.destroy(true, true);
                    sliderMobile = false;
                }
            }
        };

        <?=$callbackFunctionName?>();
        $(window).resize(BX.delegate(<?=$callbackFunctionName?>, this));

    }, this));

</script>
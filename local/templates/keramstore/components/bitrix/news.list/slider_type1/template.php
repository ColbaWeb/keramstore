<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc,
    Nextype\Alpha\Layout\Assets,
    Nextype\Alpha\Application;

if ($arResult['SHOW_PRODUCTS_COUNT'])
{
    $itemDeclension = new \Bitrix\Main\Grid\Declension(
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_1'),
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_2'),
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_3'));
}

$templateData['CONTAINER_ID'] = "bx_top_slider" . $this->randString();

?>

<div class="wrapper-section" id="<?=$templateData['CONTAINER_ID']?>">
    <div class="hero type-1">
        <div class="items">
            <?foreach ($arResult['BANNERS'] as $arBanner):?>
            <?
                $this->AddEditAction($arBanner['ID'], $arBanner['EDIT_LINK'], CIBlock::GetArrayByID($arBanner["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arBanner['ID'], $arBanner['DELETE_LINK'], CIBlock::GetArrayByID($arBanner["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="item swiper-lazy" data-temp-src="<?=$arBanner['DETAIL_PICTURE']['SRC']?>" data-src="<?=$arBanner['DETAIL_PICTURE']['SRC']?>" data-background="<?=$arBanner['DETAIL_PICTURE']['SRC']?>" id="<?=$this->GetEditAreaId($arBanner['ID']);?>">
                    <?=Assets::showSpinner()?>
                    <?if (!empty($arBanner['PROPERTIES']['BANNER_LINK']['VALUE'])):?>
                        <a class="link" href="<?=$arBanner['PROPERTIES']['BANNER_LINK']['VALUE']?>"></a>
                    <?endif?>
                        
                    <?if (!empty($arBanner['PROPERTIES']['TITLE']['VALUE'])):?>
                        <div class="overlay">
                            <?if ($arResult['SHOW_PRODUCTS_COUNT'] && $arBanner['SUBSECTION_PRODUCTS_COUNT'] > 0):?>
                                <div class="subtitle font-tiny-text">
                                    <?=$arBanner['SUBSECTION_PRODUCTS_COUNT'] . ' ' . $itemDeclension->get($arBanner['SUBSECTION_PRODUCTS_COUNT']);?>
                                </div>
                            <?endif?>
                            <a class="title font-title"><?=$arBanner['PROPERTIES']['TITLE']['VALUE']?></a>
                            <div class="hover-content">
                                <?if (!empty($arBanner['PROPERTIES']['DESCRIPTION']['VALUE'])):?>
                                    <div class="desc font-body-2"><?=$arBanner['PROPERTIES']['DESCRIPTION']['VALUE']?></div>
                                <?endif?>
                                <?if (is_array($arBanner['PROPERTIES']['EXTRA_SECTIONS']['VALUE']) && !empty($arBanner['PROPERTIES']['EXTRA_SECTIONS']['VALUE'][0])):?>
                                    <div class="buttons">
                                        <?foreach ($arBanner['PROPERTIES']['EXTRA_SECTIONS']['VALUE'] as $sectionID):?>
                                            <a href="<?=$arResult['EXTRA_SECTIONS_RESULT'][(int)$sectionID]['URL']?>" class="btn btn-tag">
                                                <?=$arResult['EXTRA_SECTIONS_RESULT'][(int)$sectionID]['NAME']?>
                                            </a>
                                        <?endforeach?>
                                    </div>
                                <?endif?>
                            </div>
                        </div>
                    <?endif?>
                </div>
            <?endforeach?>
        </div>
    </div>
</div>

<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult['ITEMS']))
{
    $arSection = \Bitrix\Iblock\SectionTable::getList(array(
        'filter' => array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ACTIVE' => 'Y',
            'CODE' => 'small'
        ), 
        'select' =>  array('ID'),
        'limit' => 1
    ))->fetch();
    
    if (!$arSection)
    {
        ShowError(GetMessage('T_SECTION_NOT_FOUND'));
        return;
    }
    
    $arBanners = $arExtraSections = array();
    foreach ($arResult['ITEMS'] as $arItem)
    {
        if ($arItem['IBLOCK_SECTION_ID'] != $arSection['ID'])
            continue;

        $arItem['PROPERTIES']['BANNER_LINK']['VALUE'] = str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, SITE_DIR . $arItem['PROPERTIES']['BANNER_LINK']['VALUE']);
        
        $arItem['EXTRA_SECTIONS'] = $arItem['PROPERTIES']['EXTRA_SECTIONS']['VALUE'];
        if (is_array($arItem['EXTRA_SECTIONS']))
        {
            foreach ($arItem['EXTRA_SECTIONS'] as $sectionID)
            {
                if (!in_array($sectionID, $arExtraSections) && !empty($sectionID))
                    $arExtraSections[] = $sectionID;
            }
        }
        elseif(!empty($arItem['EXTRA_SECTIONS']))
        {
            $arExtraSections[] = $sectionID;
        }
        
        $arBanners[] = $arItem;
    }
    unset($arSection);

    $arResult['EXTRA_SECTIONS_RESULT'] = array();
    
    if (!empty($arExtraSections))
    {
        $arSort = array('SORT' => 'ASC');
        $arFilter = array('ACTIVE' => 'Y', '=ID' => $arExtraSections);
        $bIncCnt = ($arParams['SHOW_PRODUCTS_COUNT'] === 'Y') ? true : false;
        $arSections = \Nextype\Alpha\Tools\Cache::CIBlockSection_GetList($arSort, $arFilter, $bIncCnt);
        if ($arSections)
        {
            foreach ($arSections as $arSection)
            {
                $arResult['EXTRA_SECTIONS_RESULT'][(int)$arSection['ID']] = array(
                    'NAME' => $arSection['NAME'],
                    'URL' => $arSection['SECTION_PAGE_URL'],
                    'COUNT' => (int)$arSection['ELEMENT_CNT']
                );
            }
        }
    }
    
    if ($arResult['SHOW_PRODUCTS_COUNT'] = ($arParams['SHOW_PRODUCTS_COUNT'] === 'Y'))
    {
        foreach ($arBanners as &$arBanner)
        {
            $arBanner['SUBSECTION_PRODUCTS_COUNT'] = 0;
            if (is_array($arBanner['PROPERTIES']['EXTRA_SECTIONS']['VALUE']) && !empty($arBanner['PROPERTIES']['EXTRA_SECTIONS']['VALUE'][0]))
            {
                foreach ($arBanner['PROPERTIES']['EXTRA_SECTIONS']['VALUE'] as $sectionID)
                {
                    $arBanner['SUBSECTION_PRODUCTS_COUNT'] += $arResult['EXTRA_SECTIONS_RESULT'][(int)$sectionID]['COUNT'];
                }
            }
        }
    }
    
    $arResult['BANNERS'] = $arBanners;
}
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout\Assets;

$linkShowAll = (empty($arParams['LINK_SHOW_ALL'])) ? $arResult['LIST_PAGE_URL'] : $arParams['LINK_SHOW_ALL'];
$linkShowAll = str_replace('#SITE_DIR#', SITE_DIR, $linkShowAll);

$templateData['CONTAINER_ID'] = "bx_blog_" . $this->randString();
?>
        <div class="wrapper-section">
            <div class="blog-section" id="<?=$templateData['CONTAINER_ID']?>">
                <div class="container">
                    <div class="blog-item">
                        <?if (!empty($arResult['ITEMS'])):?>
                            <div class="row">
                                <div class="col-6">
                                    <div class="section-name">
                                        <div class="h2 title">
                                            <?=(empty($arParams['PAGE_BLOCK_TITLE'])) ? Loc::getMessage('TPL_PAGE_BLOCK_TITLE') : $arParams['PAGE_BLOCK_TITLE']?>
                                        </div>
                                        <a href="<?=$linkShowAll?>" class="link font-large-button">
                                            <?=(empty($arParams['LINK_SHOW_ALL_TEXT'])) ? Loc::getMessage('TPL_LINK_SHOW_ALL_TEXT') : $arParams['LINK_SHOW_ALL_TEXT']?>
                                            <i class="icon icon-arrow-right-text-button"></i>
                                        </a>
                                    </div>
                                    <div class="blog-links">
                                        <? $bFirst = true; 
                                        foreach ($arResult['ITEMS'] as $arItem):
                                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                        ?>
                                            <div class="blog-link <?if ($bFirst):?>active<?endif?>" data-entity-value="<?=$arItem['ID']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="title font-title"><?=$arItem['NAME']?></a>
                                                <div class="date font-body-small"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>
                                            </div>
                                            <? $bFirst = false; ?>
                                        <?endforeach;?>
                                    </div>

                                    <div class="blog-slider">
                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                                <?foreach ($arResult['ITEMS'] as $arItem):?>
                                                    <div class="swiper-slide">
                                                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="slider-link">
                                                            <div class="image-wrap">
                                                                <? Assets::showSpinner(); ?>
                                                                <div data-background="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="image swiper-lazy" style="height:600px;"></div>
                                                            </div>
                                                            <div class="title font-title"><?=$arItem['NAME']?></div>
                                                            <div class="date font-body-small"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>

                                                        </a>
                                                    </div>
                                                <?endforeach?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-6">
                                    <div class="blog-links-content">
                                        <? $bFirst = true; ?>
                                        <?foreach ($arResult['ITEMS'] as $arItem):?>
                                            <div data-entity-id="<?=$arItem['ID']?>" class="blog-link-content <?if ($bFirst):?>active<?endif?>">
                                                <div class="image-wrap">
                                                    <?
                                                    echo Assets::showBackgroundImage(
                                                        $arItem["PREVIEW_PICTURE"]["SRC"],
                                                        array(
                                                            'attributes' => array(
                                                                'class="image"',
                                                                'style="height:600px;"'
                                                            )
                                                       )
                                                    );
                                                    ?>
                                                    
                                                </div>
                                            </div>
                                            <? $bFirst = false; ?>
                                        <?endforeach?>
                                    </div>
                                </div>
                            </div>
                        <?endif?>
                    </div>
                </div>
            </div>
        </div>
    
    
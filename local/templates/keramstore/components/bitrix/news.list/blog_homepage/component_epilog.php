<?php

use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
?>

<script>
    BX.ready(BX.delegate(function () {

        $("body").on('mouseenter', '#<?=$templateData["CONTAINER_ID"]?> .blog-link', function () {
                var itemId = $(this).data('entity-value');
                
                if (itemId !== undefined)
                {
                    var item = $("#<?=$templateData['CONTAINER_ID']?> .blog-links-content [data-entity-id='"+itemId+"']");
                    
                    if (item !== undefined && item.length > 0)
                    {
                        $(this).addClass('active');
                        $("#<?=$templateData['CONTAINER_ID']?> .blog-links-content .blog-link-content, #<?=$templateData['CONTAINER_ID']?> .blog-links .blog-link").removeClass('active');
                        item.parent().addClass('show');
                        item.addClass('active');
                        if (typeof(window.Alpha.LazyForceLoad) == 'function')
                        {
                            new window.Alpha.LazyForceLoad({
                                parentNode: item.get(0)
                            });
                        }
                    }
                }
            });
            
            $('body').on('mouseleave', '#<?=$templateData['CONTAINER_ID']?> .blog-links', function() {
                $(this).closest('.blog-item').find('.blog-links-content').removeClass('show');
            });
            
            function resizeEvent<?=$templateData['CONTAINER_ID']?>() {
                if ($(window).innerWidth() <= 1024)
                {
                    new Swiper('#<?=$templateData['CONTAINER_ID']?> .blog-slider .swiper-container', {
                        slidesPerView: 'auto',
                        spaceBetween: 8,
                        lazy: true,
                        watchSlidesVisibility: 'auto',
                    });
                }
            }
            
            $(window).resize(function () {
                resizeEvent<?=$templateData['CONTAINER_ID']?>();
            });
            
            resizeEvent<?=$templateData['CONTAINER_ID']?>();
    }, this));
</script>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"PAGE_BLOCK_TITLE" => array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage('T_PAGE_BLOCK_TITLE'),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"LINK_SHOW_ALL_TEXT" => array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage('T_LINK_SHOW_ALL_TEXT'),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"LINK_SHOW_ALL" => array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage('T_LINK_SHOW_ALL'),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	)
);
?>
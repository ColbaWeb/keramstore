<?
$MESS['TPL_LAZY_BTN_TEXT'] = 'Показать еще';
$MESS['TPL_SIDEBAR_TAGS_TEXT'] = 'Тэги';
$MESS['TPL_SIDEBAR_SECTIONS_TITLE'] = 'Категории';
$MESS['TPL_BTN_MESSAGE_LAZY_LOAD_WAITER'] = '...';
$MESS['TPL_LIST_EMPTY_MESSAGE'] = 'Отсутствуют записи по указанному фильтру';
$MESS['T_SENDER_SUBSCRIBE_HEADER'] = 'Подпишитесь на рассылку';
$MESS['T_SENDER_SUBSCRIBE_TEXT'] = 'Получайте интересные новости каждую неделю';
$MESS['T_SENDER_SUBSCRIBE_BTN_TEXT'] = 'Подписаться';
?>

<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
if (!\Bitrix\Main\Loader::includeModule('nextype.alpha'))
    return;

use Nextype\Alpha\Tools\Cache;

$arResult['ALL_SECTIONS'] = Cache::CIBlockSection_GetList(
        array('SORT' => 'ASC'),
        array('ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'IBLOCK_ID' => $arParams['IBLOCK_ID']),
        true,
        array('NAME', 'SECTION_PAGE_URL')
);

$arElements = Cache::CIBlockElement_GetList(
        array($arParams["SORT_BY1"] => $arParams["SORT_ORDER1"], $arParams["SORT_BY2"] => $arParams["SORT_ORDER2"]),
        array('ACTIVE' => 'Y', '!PROPERTY_TAGS' => false, 'IBLOCK_ID' => $arParams['IBLOCK_ID']),
        true
);

$arTags = array();
foreach($arElements as $arElement)
{
    $obProps = \CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID'], array(), Array("CODE" => 'TAGS'));
    while ($arProp = $obProps->fetch())
    {
        if (!empty($arProp['VALUE']))
            $arTags[$arProp['VALUE_XML_ID']] = array(
                'VALUE' => $arProp['VALUE'],
                'DISPLAY_VALUE' => $arProp['VALUE_ENUM']
            );
    }
}
$arResult['ALL_TAGS'] = $arTags;

<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
//Bitrix\Main\Localization\Loc::loadMessages(__DIR__ . "/template.php");
//	lazy load data json answers
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest() && $request->get('action') === 'showMore')
{
	$content = ob_get_contents();
	ob_end_clean();

	list(, $itemsContainer) = explode('<!-- blog-articles-container -->', $content);
	list(, $paginationContainer) = explode('<!-- pagination-container -->', $content);
	list(, $epilogue) = explode('<!-- blog-component-end -->', $content);

	if ($arParams['AJAX_MODE'] === 'Y')
	{
		$component->prepareLinks($paginationContainer);
	}
        
        $APPLICATION->RestartBuffer();
                
        echo \Bitrix\Main\Web\Json::encode(array(
            'items' => $itemsContainer,
            'pagination' => $paginationContainer,
            'epilogue' => $epilogue,
            'JS' => Bitrix\Main\Page\Asset::getInstance()->getJs()
	));
        
        \CMain::FinalActions();
        die();
}
else
{
    \Nextype\Alpha\Layout\Assets::getInstance(SITE_ID)->addLess(SITE_TEMPLATE_PATH . "/less/blog-list.less");
}
?>

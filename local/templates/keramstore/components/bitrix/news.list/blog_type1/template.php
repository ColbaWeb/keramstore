<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc,
    Nextype\Alpha\Layout;

$this->setFrameMode(true);

$itemsHasTags = !empty($arResult['ALL_TAGS']);
$bHideLink = ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] == 'Y' || $arParams['HIDE_LINK_WHEN_NO_DETAIL'] == true);
$bUseSender = ($arParams['SHOW_SUBSCRIBE_BLOCK'] == 'Y' && \Bitrix\Main\Loader::includeModule('sender'));

if (empty($arResult['ITEMS']))
{
    Layout\Partial::getInstance(__DIR__)->render('empty.php', array (
        'arParams' => $arParams,
        'arResult' => $arResult,
        'component' => $component,
    ), false, false);
    //return;
}

if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}


$showBottomPager = false;
$showLazyLoad = false;

if ($arParams['NEWS_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['SHOW_LAZY_LOAD_BTN'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}
$templateData = array(
        'NAV_RECORD_COUNT' => $arResult['NAV_RESULT']->NavRecordCount
);

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
$containerName = 'container-'.$navParams['NavNum'];

$btnText = (empty($arParams['LAZY_LOAD_BTN_TEXT'])) ? Loc::getMessage('TPL_LAZY_BTN_TEXT') : $arParams['LAZY_LOAD_BTN_TEXT'];
?> 
<div class="wrapper-section">
    <div class="blog-list <?if($bIsType2):?>type-2<?endif?>">
        <div class="row">
            <div class="col">
                <?if (isset($arParams['ITEMS']))
                    $arResult['ITEMS'] = $arParams['ITEMS']; ?>
                <?if (!empty($arResult['ITEMS'])):?>
                    <div class="list blog-items row" data-entity="<?=$containerName?>">
                        <!-- blog-articles-container -->
                        <?
                        foreach($arResult['ITEMS'] as $key => $arItem):
                            
                            
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                            <a <?if (!$bHideLink):?>href="<?=$arItem['DETAIL_PAGE_URL']?>"<?endif?> data-entity="blog-article"
                                class="blog-list-item col-12 col-sm-6 col-md-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                <div class="image-wrap">
                                    <?if (!empty($arItem['PROPERTIES']['TAGS']['VALUE'][0])):?>
                                        <div class="labels">
                                            <?foreach ($arItem['PROPERTIES']['TAGS']['VALUE'] as $tagName):?>
                                                <div class="label blog-label"><?=$tagName?></div>
                                            <?endforeach?>
                                        </div>
                                    <?endif;
                                    echo \Nextype\Alpha\Layout\Assets::showBackgroundImage(
                                            $arItem["PREVIEW_PICTURE"]["SRC"],
                                            array('attributes' => array('class="image"'))
                                        );?>
                                </div>
                                
                                <div class="info">
                                    <div class="date font-body-small"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>
                                    <div class="title font-title"><?=$arItem['NAME']?></div>
                                </div>
                            </a>
                            <? endforeach;?>
                        
                        <!-- blog-articles-container -->
                    </div>
                <?endif?>
                
                <?if ($showLazyLoad):?>
                    <div data-entity="lazy-<?= $containerName ?>">
                        <a class="btn btn-show-more"
                            data-use="show-more-<?=$navParams['NavNum']?>">
                            <?=$btnText?>
                        </a>
                    </div>
                <?endif;?>

                <?if ($arParams['DISPLAY_BOTTOM_PAGER'] && !empty($arResult['NAV_STRING'])):?>
                    <div data-pagination-num="<?=$navParams['NavNum']?>">
                        <!-- pagination-container -->
                            <?=$arResult['NAV_STRING']?>
                        <!-- pagination-container -->
                    </div>
                <?endif;?>

                <div class="inner-text">
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => $arParams['IBLOCK_URL'] . "/.description.php"
                            )
                    );?>
                </div>
            </div>
            <div class="col-auto sidebar-left">
                <div class="blog-sidebar-menu font-body-2">
                    <?if (!empty($arResult['ALL_SECTIONS'])):?>
                        <div class="title bold"><?=Loc::getMessage('TPL_SIDEBAR_SECTIONS_TITLE')?></div>
                        <div class="list">
                            <ul class="reset-ul-list">
                                <?foreach ($arResult['ALL_SECTIONS'] as $arSection):
                                    if ($arSection['ELEMENT_CNT'] == '0')
                                        continue; ?>
                                    <li>
                                        <a class="item" href="<?=$arSection['SECTION_PAGE_URL']?>">
                                            <span class="category-name"><?=$arSection['NAME']?></span>
                                            <span class="count"><?=$arSection['ELEMENT_CNT']?></span>
                                        </a>
                                    </li>
                                <?endforeach?>
                                <?if ($arParams['SIDEBAR_SHOW_ALL'] == 'Y'):?>
                                    <li>
                                        <a class="item" href="<?=$arResult['ITEMS'][0]['LIST_PAGE_URL']?>">
                                            <span class="category-name"><?=$arParams['SIDEBAR_SHOW_ALL_TEXT']?></span>
                                        </a>
                                    </li>
                                <?endif?>
                            </ul>
                        </div>
                    <?endif;?>
                </div>
                
                <?if ($itemsHasTags):?>
                    <div class="blog-sidebar-menu font-body-2">
                        <div class="title bold"><?=Loc::getMessage('TPL_SIDEBAR_TAGS_TEXT')?></div>
                        <div class="blog-tags">
                            <?foreach ($arResult['ALL_TAGS'] as $xmlId => $arTag):?>
                                <div class="item">
                                    <a href="<?=$APPLICATION->GetCurPageParam('tag=' . $xmlId, array('tag'))?>" class="tag font-button-large">
                                        <?=$arTag['DISPLAY_VALUE']?>
                                    </a>
                                </div>
                            <?endforeach?>
                        </div>
                    </div>
                <?endif?>
                
                <?if ($bUseSender):?>
                    <? $uniqId = 'blog_' . $arResult['ID'] . randString(5);?>
                    <div class="subscribe">
                        <div class="subscribe-image" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/icons/icon-mailbox.svg');"></div>
                        <div class="title font-h5 bold"><?=Loc::getMessage('T_SENDER_SUBSCRIBE_HEADER')?></div>
                        <div class="text font-body"><?=Loc::getMessage('T_SENDER_SUBSCRIBE_TEXT')?></div>
                        <a id="senderbtn_<?=$uniqId?>" href="#modal_block_<?=$uniqId?>" class="btn btn-primary btn-medium" data-toggle="modal">
                            <?=Loc::getMessage('T_SENDER_SUBSCRIBE_BTN_TEXT')?>
                        </a>
                    </div>
                <?endif?>
            </div>
        </div>
    </div>
</div>

<?if ($bUseSender):?>
    <div class="modal fade" id="modal_block_<?=$uniqId?>" tabindex="-1" aria-labelledby="modal_block_<?=$uniqId?>" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon icon-close-big"></i>
                </a>
                <div class="scrollbar-inner">
                    <?$APPLICATION->IncludeComponent(
	"nextype:alpha.sender.subscribe", 
	"popup", 
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CONFIRMATION" => "Y",
		"HIDE_MAILINGS" => "N",
		"SET_TITLE" => "N",
		"SHOW_HIDDEN" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "N",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_PERSONALIZATION" => "Y",
		"COMPONENT_TEMPLATE" => "popup"
	),
	false
);?>
                </div>
            </div>
        </div>
    </div>
<?endif?>

<?
    $generalParams = array ();
    
    foreach ($arParams as $key => $value)
    {
        if (strpos($key, "~") !== false)
        {
            $generalParams[substr($key, 1)] = $value;
        }
    }
    
    $generalParams = array_merge($generalParams, array (
        'PAGER_BASE_LINK_ENABLE' => "Y",
        'PAGER_BASE_LINK' => $APPLICATION->GetCurPageParam('', array ("PAGER_" . $navParams['NavNum']))
    ));
    
    $signer = new \Bitrix\Main\Security\Sign\Signer;
    $signedTemplate = $signer->sign($templateName, 'news.list');
    $signedParams = $signer->sign(base64_encode(serialize($generalParams)), 'news.list');
?>

<script>
    BX.ready(function () {
        BX.message({
                BTN_MESSAGE_LAZY_LOAD: '<?=$btnText?>',
                BTN_MESSAGE_LAZY_LOAD_WAITER: '<?=Loc::GetMessage('TPL_BTN_MESSAGE_LAZY_LOAD_WAITER')?>'
        });
        var <?= $obName ?> = new window.Alpha.NewsListComponent({
            siteId: '<?= CUtil::JSEscape($component->getSiteId()) ?>',
            ajaxPath: '<?= CUtil::JSEscape($templateFolder . "/ajax.php") ?>',
            navParams: <?= CUtil::PhpToJSObject($navParams) ?>,
            filter: <?= !empty($generalParams['FILTER_NAME']) && isset($GLOBALS[$generalParams['FILTER_NAME']]) ? CUtil::PhpToJSObject($GLOBALS[$generalParams['FILTER_NAME']]) : CUtil::PhpToJSObject(array()) ?>,
            lazyLoad: !!'<?= $showLazyLoad ?>',
            template: '<?= CUtil::JSEscape($signedTemplate) ?>',
            ajaxId: '<?= CUtil::JSEscape($arParams['AJAX_ID']) ?>',
            parameters: '<?= CUtil::JSEscape($signedParams) ?>',
            container: '<?= $containerName ?>'
        });

        $(window).scroll(function () {
            let header = $('.header-fixed');
            let offset = header.height();
            $('.subscribe').css('top', offset);
        });
    });

</script>
<!-- blog-component-end -->
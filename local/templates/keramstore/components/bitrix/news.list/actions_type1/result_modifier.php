<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

if (!empty($arResult['ITEMS']))
{
    foreach ($arResult['ITEMS'] as $key => $arItem)
    {
        if (
                (isset($arItem['PROPERTIES']['DATE_START']) && !empty($arItem['PROPERTIES']['DATE_START']['VALUE']))
                || (isset($arItem['PROPERTIES']['DATE_END']) && !empty($arItem['PROPERTIES']['DATE_END']['VALUE']))
        )
        {
            if (!empty($arItem['PROPERTIES']['DATE_START']['VALUE']) && !empty($arItem['PROPERTIES']['DATE_END']['VALUE']))
            {
                // set only date start and end
                $timeStart = new DateTime($arItem['PROPERTIES']['DATE_START']['VALUE']);
                $timeEnd = new DateTime($arItem['PROPERTIES']['DATE_END']['VALUE']);
                
                if ($timeStart->format("Y") == $timeEnd->format("Y") && $timeStart->format("m") == $timeEnd->format("m"))
                {
                    $messageID = $timeStart->format("Y") == date("Y") ? 'CP_TPL_DATE_FORMAT_1' : 'CP_TPL_DATE_FORMAT_2';
                }
                elseif ($timeStart->format("Y") == $timeEnd->format("Y") && $timeStart->format("m") != $timeEnd->format("m"))
                {
                    $messageID = 'CP_TPL_DATE_FORMAT_3';
                }
                else
                {
                    $messageID = 'CP_TPL_DATE_FORMAT_4';
                }
                
                $arItem['DATE_TIME'] = GetMessage($messageID, array (
                        '#DAY_START#' => $timeStart->format("d"),
                        '#DAY_END#' => $timeEnd->format("d"),
                        '#MONTH_START#' => FormatDate('F', $timeStart->getTimestamp()),
                        '#MONTH_END#' => FormatDate('F', $timeEnd->getTimestamp()),
                        '#YEAR_START#' => $timeStart->format('Y'),
                        '#YEAR_END#' => $timeEnd->format('Y')
                    ));

            }
            elseif (!empty($arItem['PROPERTIES']['DATE_START']['VALUE']) && empty($arItem['PROPERTIES']['DATE_END']['VALUE']))
            {
                // set only date start
                $timeStart = new DateTime($arItem['PROPERTIES']['DATE_START']['VALUE']);
                $messageID = $timeStart->format("Y") == date("Y") ? 'CP_TPL_DATE_FORMAT_5' : 'CP_TPL_DATE_FORMAT_6';
                                
                $arItem['DATE_TIME'] = GetMessage($messageID, array (
                        '#DAY#' => $timeStart->format("d"),
                        '#MONTH#' => FormatDate('F', $timeStart->getTimestamp()),
                        '#YEAR#' => $timeStart->format('Y'),
                ));
            }
            elseif (empty($arItem['PROPERTIES']['DATE_START']['VALUE']) && !empty($arItem['PROPERTIES']['DATE_END']['VALUE']))
            {
                // set only date end
                $timeEnd = new DateTime($arItem['PROPERTIES']['DATE_END']['VALUE']);
                $messageID = $timeEnd->format("Y") == date("Y") ? 'CP_TPL_DATE_FORMAT_7' : 'CP_TPL_DATE_FORMAT_8';
                                
                $arItem['DATE_TIME'] = GetMessage($messageID, array (
                        '#DAY#' => $timeEnd->format("d"),
                        '#MONTH#' => FormatDate('F', $timeEnd->getTimestamp()),
                        '#YEAR#' => $timeEnd->format('Y'),
                ));
            }
        }
        
        $arResult['ITEMS'][$key] = $arItem;
    }
}
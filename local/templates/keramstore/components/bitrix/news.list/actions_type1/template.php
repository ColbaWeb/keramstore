<?
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout\Assets;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

if (!Loader::includeModule('nextype.alpha'))
    return;

$this->setFrameMode(true);

if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

$showTopPager = false;
$showBottomPager = false;
$showLazyLoad = false;


if ($arParams['NEWS_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showTopPager = $arParams['DISPLAY_TOP_PAGER'];
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['DISPLAY_BOTTOM_PAGER'] && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}


$templateData = array(
        'NAV_RECORD_COUNT' => $arResult['NAV_RESULT']->NavRecordCount
);
unset($currencyList, $templateLibrary);

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
$containerName = 'container-'.$navParams['NavNum'];
?>
<div class="action-items">
<?
if (count($arResult['ITEMS']) > 2 && $navParams['NavPageNomer'] == 1)
{
    $arItem = $arResult['ITEMS'][0];
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            
    ?>
    <div class="row">
        <div class="col-12 col-lg-8">
            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item action" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="image-wrap">
                        <?
                        echo Assets::showBackgroundImage(
                                $arItem["PREVIEW_PICTURE"]["SRC"],
                                array(
                                    'attributes' => array(
                                        'class="bg-img"',
                                    )
                                )
                        );
                        ?>
                </div>
                <div class="overlay">
                    <div class="title font-title"><?=$arItem['NAME']?></div>
                    <?
                    if (!empty($arItem['DATE_TIME']))
                    {
                        ?><div class="subtitle font-body-small"><?=$arItem['DATE_TIME']?></div><?
                    }
                    ?>
                    
                </div>
            </a>
        </div>
        <div class="col-12 col-lg-4 action-items-col-right">
            <?
            for ($i = 1; $i <= 2; $i++)
            {
                if (!isset($arResult['ITEMS'][$i]))
                    continue;
                
                $arItem = $arResult['ITEMS'][$i];
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            
            ?>
            <div class="row">
                <div class="col">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item action" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="image-wrap">
                            <?
                            echo Assets::showBackgroundImage(
                                    $arItem["PREVIEW_PICTURE"]["SRC"],
                                    array(
                                        'attributes' => array(
                                            'class="bg-img"',
                                        )
                                    )
                            );
                            ?>
                        </div>
                        <div class="overlay">
                            <div class="title font-title"><?=$arItem['NAME']?></div>
                            <?
                            if (!empty($arItem['DATE_TIME']))
                            {
                                ?><div class="subtitle font-body-small"><?=$arItem['DATE_TIME']?></div><?
                            }
                            ?>
                        </div>
                    </a>
                </div>
            </div>
            <?
            }
            ?>
        </div>
    </div>
    
    <?
    if (count($arResult['ITEMS']) > 3)
    {
        ?>
        <div class="row" data-entity="<?=$containerName?>">
        <?
        for ($i = 3; $i < count($arResult['ITEMS']); $i++)
        {
            if (!isset($arResult['ITEMS'][$i]))
                continue;
            
            $arItem = $arResult['ITEMS'][$i];
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            
            ?>
            <div class="col-lg-4 col-12">
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item action" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="image-wrap">
                            <?
                            echo Assets::showBackgroundImage(
                                    $arItem["PREVIEW_PICTURE"]["SRC"],
                                    array(
                                        'attributes' => array(
                                            'class="bg-img"',
                                        )
                                    )
                            );
                            ?>
                        </div>
                        <div class="overlay">
                            <div class="title font-title"><?=$arItem['NAME']?></div>
                            <?
                            if (!empty($arItem['DATE_TIME']))
                            {
                                ?><div class="subtitle font-body-small"><?=$arItem['DATE_TIME']?></div><?
                            }
                            ?>
                        </div>
                    </a>
            </div>
            <?
        }
        ?>
        </div>  
        <?
    }
    
    //region LazyLoad Button
    if ($showLazyLoad)
    {
        ?>
        <div data-entity="lazy-<?= $containerName ?>">
            <a href="javascript:void(0)" class="btn btn-show-more" data-use="show-more-<?= $navParams['NavNum'] ?>"><?=GetMessage('CT_TPL_LIST_BTN_LAZY_LOAD')?></a>
        </div>
        <?
    }
    //endregion
    //region Pagination
    if ($showBottomPager)
    {
        ?>
            <div data-pagination-num="<?= $navParams['NavNum'] ?>">
                <!-- pagination-container -->
                <?= $arResult['NAV_STRING'] ?>
                <!-- pagination-container -->
            </div>
        <?
    }
    //endregion
    
    $generalParams = array ();
    
    foreach ($arParams as $key => $value)
    {
        if (strpos($key, "~") !== false)
        {
            $generalParams[substr($key, 1)] = $value;
        }
    }
    
    $generalParams = array_merge($generalParams, array (
        'PAGER_BASE_LINK_ENABLE' => "Y",
        'PAGER_BASE_LINK' => $APPLICATION->GetCurPageParam('', array ("PAGER_" . $navParams['NavNum']))
    ));
    
    
    $signer = new \Bitrix\Main\Security\Sign\Signer;
    $signedTemplate = $signer->sign($templateName, 'news.list');
    $signedParams = $signer->sign(base64_encode(serialize($generalParams)), 'news.list');
    ?>

    <script>
        BX.ready(function () {
            BX.message({
                'CT_TPL_LIST_BTN_LAZY_LOAD': '<?=GetMessageJS('CT_TPL_LIST_BTN_LAZY_LOAD')?>',
                'CT_TPL_LIST_BTN_LAZY_LOAD_WAITER': '<?=GetMessageJS('CT_TPL_LIST_BTN_LAZY_LOAD_WAITER')?>'
            });
            
            var <?= $obName ?> = new window.Alpha.NewsListComponent({
                siteId: '<?= CUtil::JSEscape($component->getSiteId()) ?>',
                ajaxPath: '<?= CUtil::JSEscape($templateFolder . "/ajax.php") ?>',
                navParams: <?= CUtil::PhpToJSObject($navParams) ?>,
                filter: <?= !empty($generalParams['FILTER_NAME']) && isset($GLOBALS[$generalParams['FILTER_NAME']]) ? CUtil::PhpToJSObject($GLOBALS[$generalParams['FILTER_NAME']]) : CUtil::PhpToJSObject(array()) ?>,
                lazyLoad: !!'<?= $showLazyLoad ?>',
                template: '<?= CUtil::JSEscape($signedTemplate) ?>',
                ajaxId: '<?= CUtil::JSEscape($arParams['AJAX_ID']) ?>',
                parameters: '<?= CUtil::JSEscape($signedParams) ?>',
                container: '<?= $containerName ?>'
            });
        });
        </script>
    <?
    
}
elseif (count($arResult['ITEMS']) > 0)
{
    ?>
    <div class="row">
        <!-- items-container -->
        <?
        foreach ($arResult['ITEMS'] as $arItem)
        {
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            
            ?>
            <div class="col-lg-4 col-12" data-entity="item">
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item action" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="image-wrap">
                                <?
                                echo Assets::showBackgroundImage(
                                        $arItem["PREVIEW_PICTURE"]["SRC"],
                                        array(
                                            'attributes' => array(
                                                'class="bg-img"',
                                            )
                                        )
                                );
                                ?>
                    </div>
                    <div class="overlay">
                        <div class="title font-title"><?=$arItem['NAME']?></div>
                        <?
                        if (!empty($arItem['DATE_TIME']))
                        {
                            ?><div class="subtitle font-body-small"><?=$arItem['DATE_TIME']?></div><?
                        }
                        ?>
                    </div>
                </a>
            </div>
            <?
        }
        ?>
        <!-- items-container -->
    </div>
    <?
    
    //region LazyLoad Button
    if ($showLazyLoad)
    {
        ?>
        <div data-entity="lazy-<?= $containerName ?>">
            <a href="javascript:void(0)" class="btn btn-show-more" data-use="show-more-<?= $navParams['NavNum'] ?>"><?=GetMessage('CT_TPL_LIST_BTN_LAZY_LOAD')?></a>
        </div>
        <?
    }
    //endregion
    //region Pagination
    if ($showBottomPager)
    {
        ?>
            <div data-pagination-num="<?= $navParams['NavNum'] ?>">
                <!-- pagination-container -->
                <?= $arResult['NAV_STRING'] ?>
                <!-- pagination-container -->
            </div>
        <?
    }
    //endregion
}
else
{
    ?>
      <div class="catalog-empty-section">
        <div class="image">
            <svg xmlns="http://www.w3.org/2000/svg" width="96" height="96" viewBox="0 0 96 96" fill="none">
                <path d="M89.954 31.9956L48.9401 18.822C48.3446 18.6129 47.6962 18.6088 47.098 18.8103L6.04013 31.9971C4.82372 32.3893 3.99945 33.5218 4 34.7999V73.9452C4.00091 75.8222 5.19241 77.4919 6.96707 78.1032L46.5524 91.752C47.0173 91.9166 47.5069 92.0004 48 91.9999C48.4867 91.9999 48.9699 91.9165 49.4285 91.7535L89.0285 78.0988C90.8034 77.4896 91.9966 75.8217 92 73.9452V34.7999C91.9999 33.5198 91.173 32.3864 89.954 31.9956ZM46.5333 22.0751V42.1332C46.5333 42.9432 47.19 43.5999 48 43.5999C48.81 43.5999 49.4667 42.9432 49.4667 42.1332V22.0721L89.0667 34.7881V34.7999V34.8116H89.0579L48 47.9999L6.94213 34.7881L46.5333 22.0751ZM6.93333 73.9452V37.8901L46.5333 50.6091V88.6427L7.92627 75.3312C7.33317 75.1288 6.93421 74.5719 6.93333 73.9452ZM88.0752 75.3312L49.4667 88.6441V50.6091L89.0667 37.8887V73.9452C89.066 74.5715 88.6677 75.1283 88.0752 75.3312Z" fill="black" fill-opacity="0.4" stroke="white" stroke-width="0.5"/>
                <path d="M47.9979 15.7333C48.8079 15.7333 49.4646 15.0767 49.4646 14.2667V5.46667C49.4646 4.65665 48.8079 4 47.9979 4C47.1879 4 46.5312 4.65665 46.5312 5.46667V14.2667C46.5312 15.0767 47.1879 15.7333 47.9979 15.7333Z" fill="black" fill-opacity="0.4" stroke="white" stroke-width="0.5"/>
                <path d="M27.6213 17.8553C27.9834 18.5803 28.8646 18.8745 29.5896 18.5124C30.3146 18.1503 30.6088 17.2691 30.2467 16.5441L25.8467 7.74413C25.4846 7.01916 24.6034 6.72498 23.8784 7.08706C23.1534 7.44914 22.8593 8.33036 23.2213 9.05533L27.6213 17.8553Z" fill="black" fill-opacity="0.4" stroke="white" stroke-width="0.5"/>
                <path d="M66.4105 18.5197C66.7603 18.6956 67.1658 18.7243 67.5369 18.5995C67.908 18.4747 68.2137 18.2068 68.3861 17.8553L72.7861 9.05533C73.1482 8.33036 72.854 7.44914 72.1291 7.08706C71.4041 6.72498 70.5229 7.01916 70.1608 7.74413L65.7608 16.5441C65.5839 16.8921 65.553 17.2962 65.675 17.667C65.7969 18.0378 66.0617 18.3447 66.4105 18.5197Z" fill="black" fill-opacity="0.4" stroke="white" stroke-width="0.5"/>
            </svg>
        </div>
        <div class="title font-title bold"><?=GetMessage('CT_TPL_LIST_EMPTY_TITLE')?></div>
        <a href="<?=SITE_DIR?>" class="btn btn-primary btn-large"><?=GetMessage('CT_TPL_LIST_EMPTY_BUTTON')?> <i class="icon icon-arrow-right-text-button-white"></i></a>
    </div>  
    <?
}
?>
</div>

<!-- component-end -->

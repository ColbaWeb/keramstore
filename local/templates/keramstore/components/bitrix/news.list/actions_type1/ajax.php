<?
/** @global \CMain $APPLICATION */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

$siteId = isset($_REQUEST['siteId']) && is_string($_REQUEST['siteId']) ? $_REQUEST['siteId'] : '';
$siteId = mb_substr(preg_replace('/[^a-z0-9_]/i', '', $siteId), 0, 2);
if (!empty($siteId) && is_string($siteId))
{
    define('SITE_ID', $siteId);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
    return;

$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
    $template = $signer->unsign($request->get('template') ?: '', 'news.list') ?: '.default';
    $paramString = $signer->unsign($request->get('parameters') ?: '', 'news.list');
    $parameters = unserialize(base64_decode($paramString));
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
    die();
}

$parameters['AJAX_REQUEST'] = 'Y';

if (isset($parameters['FILTER_NAME']) && !empty($parameters['FILTER_NAME']) && $request->get('filter'))
{
    $GLOBALS[$parameters['FILTER_NAME']] = $request->get('filter');
}

$APPLICATION->IncludeComponent(
    'bitrix:news.list',
    $template,
    $parameters
);
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Web\Json;
use Bitrix\Main\Grid\Declension;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;

Loc::loadMessages(__DIR__ . "/template.php");

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;


//	lazy load data json answers
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest() && $request->get('action') === 'showMore')
{
	$content = ob_get_contents();
	ob_end_clean();

	list(, $itemsContainer) = explode('<!-- items-container -->', $content);
	list(, $paginationContainer) = explode('<!-- pagination-container -->', $content);
	list(, $epilogue) = explode('<!-- component-end -->', $content);

	if ($arParams['AJAX_MODE'] === 'Y')
	{
		$component->prepareLinks($paginationContainer);
	}
        
        $APPLICATION->RestartBuffer();
                
        echo Json::encode(array(
            'items' => $itemsContainer,
            'pagination' => $paginationContainer,
            'epilogue' => $epilogue,
            'JS' => Bitrix\Main\Page\Asset::getInstance()->getJs()
	));
        
        \CMain::FinalActions();
        die();
}
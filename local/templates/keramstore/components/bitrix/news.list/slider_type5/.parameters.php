<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arTemplateParameters = array(
	"SHOW_PRODUCTS_COUNT" => array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage('T_SHOW_PRODUCTS_COUNT'),
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"DEFAULT" => "Y",
	)
);
?>
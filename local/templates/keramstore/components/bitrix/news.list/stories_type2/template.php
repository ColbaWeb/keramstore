<?

use Bitrix\Main\Localization\Loc,
    Nextype\Alpha\Layout;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$containerId = "bx_slider_" . randString();
?>
    <div class="wrapper-section" id="<?= $containerId ?>">
        <div class="stories type-2">
            <div class="container">
                <div class="stories-wrapper">
                    <div class="text-container">
                        <div class="title h2"><?= (empty($arParams['BLOCK_TITLE'])) ? GetMessage('NL_TMP_BLOCK_TITLE') : $arParams['BLOCK_TITLE'] ?></div>
                        <a href="javascript:void(0)"
                           class="link open-all-stories"><?= (empty($arParams['SHOW_ALL_TEXT'])) ? GetMessage('NL_TMP_SHOW_ALL_TEXT') : $arParams['SHOW_ALL_TEXT'] ?>
                            <i class="icon icon-arrow-right-text-button"></i>
                        </a>
                    </div>
                    <? if (!empty($arResult['ITEMS'])): ?>
                        <div class="stories-list">
                            <div class="swiper-slider">
                                <!-- Swiper -->
                                <div class="swiper-container main-slider">
                                    <div class="swiper-wrapper">
                                        <?
                                        foreach ($arResult['ITEMS'] as $key => $arItem):
                                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                            ?>
                                            <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                                <a href="javascript:void(0)" class="item">
                                                    <div class="image swiper-lazy"
                                                         data-background="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>">
                                                        <?= Layout\Assets::showSpinner() ?>
                                                    </div>
                                                </a>
                                            </div>
                                        <? endforeach ?>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-arrows">
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        </div>
                    <? endif ?>
                </div>
            </div>
        </div>

        <? if (!empty($arResult['ITEMS'])): ?>
            <div class="stories-inner stories type-1">
                <a href="javascript:void(0)" class="close"><i class="icon icon-close-big"></i></a>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                            <div class="swiper-slide">
                                <div class="item unseen">
                                    <div class="image"
                                         style="background-image: url('<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>');">
                                        <div class="progress-bar"></div>
                                        <? if (!empty($arItem['PROPERTIES']['BUTTON_TEXT']['VALUE']) && !empty($arItem['PROPERTIES']['BUTTON_LINK']['VALUE'])): ?>
                                            <a href="<?= str_replace("#SITE_DIR#", SITE_DIR, $arItem['PROPERTIES']['BUTTON_LINK']['VALUE']) ?>"
                                               class="btn btn-primary btn-large"><?= $arItem['PROPERTIES']['BUTTON_TEXT']['VALUE'] ?>
                                                <i class="icon icon-arrow-right-text-button-white"></i>
                                            </a>
                                        <? endif; ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        <? endif; ?>
    </div>

<? if (!empty($arResult['ITEMS'])): ?>
    <script>
        $(document).ready(function () {

            let sliderContainerLength = $('#<?=$containerId?> .stories-wrapper .swiper-wrapper').children().length;
            let count = "<?=count($arResult['ITEMS'])?>";

            function createPlaceholders() {
                $('#<?=$containerId?> .stories-wrapper .swiper-wrapper').append(
                    "<div class='swiper-slide placeholder-slide'><div class='placeholder-img'></div></div>"
                );
            }

            if (sliderContainerLength <= count) {
                for (let i = 0; i < (count - sliderContainerLength); i++) {
                    createPlaceholders();
                }
            }

            var swiperMain = new Swiper('#<?=$containerId?> .stories-wrapper .swiper-container.main-slider', {
                slidesPerView: 'auto',
                spaceBetween: 8,
                freeMode: true,
                lazy: true,
                watchSlidesVisibility: 'auto',
                breakpoints: {
                    1024: {
                        spaceBetween: 24,
                        navigation: {
                            nextEl: '#<?=$containerId?> .stories-wrapper .swiper-button-next',
                            prevEl: '#<?=$containerId?> .stories-wrapper .swiper-button-prev',
                        },
                    },
                }
            });

            function initInnerSwiperSlider() {

                if (!!window.innerStoriesSwiperSlider)
                    return;

                let animationDuration = 7000;
                $('#<?=$containerId?> .stories-inner .progress-bar').css('animation-duration', animationDuration + 'ms');

                window.innerStoriesSwiperSlider = new Swiper('#<?=$containerId?> .stories-inner .swiper-container', {
                    slidesPerView: 'auto',
                    spaceBetween: 0,
                    centeredSlides: true,
                    simulateTouch: false,
                    autoplay: {
                        delay: animationDuration,
                        disableOnInteraction: false,
                    },
                    on: {
                        slideChange: function () {
                            $('#<?=$containerId?> .swiper-slide-active .item').removeClass('unseen');
                        }
                    },
                });

                $('#<?=$containerId?> .stories-inner .swiper-container .swiper-slide').on('click', function () {
                    window.innerStoriesSwiperSlider.slideTo($(this).index(), 400)
                });

                $('#<?=$containerId?> .stories-inner').on('click', function (e) {
                    if (!($(e.target).parents('.swiper-slide').length || $(e.target).hasClass('swiper-slide'))) {
                        $('#<?=$containerId?> .stories-inner').removeClass('opened');
                        $('body').removeClass('modal-open').trigger('modalClose');
                    }
                });
            }

            $('#<?=$containerId?> .stories-wrapper .swiper-slide').on('click', function () {
                $('#<?=$containerId?> .stories-inner').addClass('opened');
                $('body').trigger('modalOpen').addClass('modal-open');

                initInnerSwiperSlider();

                if (!!window.innerStoriesSwiperSlider) {
                    window.innerStoriesSwiperSlider.update();
                    window.innerStoriesSwiperSlider.slideTo($(this).index(), 0);
                }
            });

            $('#<?=$containerId?> .open-all-stories').on('click', function () {
                $('#<?=$containerId?> .stories-inner').addClass('opened');
                $('body').trigger('modalOpen').addClass('modal-open');
                initInnerSwiperSlider();
            });

            $(window).on('orientationchange', function () {
                $('.stories-inner').removeClass('opened');
                $('body').removeClass('modal-open').trigger('closeModal');
            });
        });
    </script>
<? endif; ?>
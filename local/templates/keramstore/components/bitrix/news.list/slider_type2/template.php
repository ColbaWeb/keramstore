<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
    Nextype\Alpha\Layout;

$templateData = array (
    "CONTAINER_ID" => "bx_top_slider" . $this->randString(),
    "SLIDER_DELAY" => (int)$arParams['SLIDER_DELAY'] * 1000,
);
?>

<div class="wrapper-section" id="<?=$templateData['CONTAINER_ID']?>">
    <div class="hero type-2">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?foreach ($arResult['SLIDES'] as $arSlide):?>
            <?
                $this->AddEditAction($arSlide['ID'], $arSlide['EDIT_LINK'], CIBlock::GetArrayByID($arSlide["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arSlide['ID'], $arSlide['DELETE_LINK'], CIBlock::GetArrayByID($arSlide["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
                    <div class="swiper-slide banner swiper-lazy <?=$arSlide['TEXT_COLOR']?>" id="<?=$this->GetEditAreaId($arSlide['ID']);?>" 
                         data-background="<?=$arSlide['BACKGROUND_IMAGE_SRC']?>">
                       
                        <div class="img swiper-lazy" data-background="<?=$arSlide['BACKGROUND_IMAGE_SRC']?>"><?=Layout\Assets::showSpinner(true)?></div>
                        <div class="text-block">
                            <div class="h1"><?=$arSlide['PROPERTIES']['TITLE']['VALUE']?></div>
                            <div class="font-body text"><?=$arSlide['PROPERTIES']['DESCRIPTION']['VALUE']?></div>
                            <div class="buttons">
                                <?if (!empty($arSlide['ABOUT_BTN']['TEXT'])):?>
                                    <a href="<?=$arSlide['ABOUT_BTN']['URL']?>" class="btn btn-large btn-primary">
                                        <?=$arSlide['ABOUT_BTN']['TEXT']?>
                                    </a>
                                <?endif?>
                                <?if (!empty($arSlide['CATALOG_BTN']['TEXT'])):?>
                                    <a href="<?=$arSlide['CATALOG_BTN']['URL']?>" class="btn btn-large btn-secondary">
                                        <?=$arSlide['CATALOG_BTN']['TEXT']?>
                                    </a>
                                <?endif?>
                            </div>
                        </div>
                    </div>
                <?endforeach?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination animation"></div>
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            if (document.body.clientWidth <= 575) {
                $('.swiper-container .swiper-slide').outerHeight($('.swiper-container').height());
            }
        })
    </script>
</div>
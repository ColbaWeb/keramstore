<?php

use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
?>

<script>
    BX.ready(BX.delegate(function () {
        
        $('#<?=$templateData['CONTAINER_ID']?> .swiper-pagination.animation').css('animation-duration', '<?=$templateData['SLIDER_DELAY']?>ms');

        var swiper = new Swiper('#<?=$templateData['CONTAINER_ID']?> .swiper-container', {
            slidesPerView: 'auto',
            centeredSlides: true,
            spaceBetween: 12,
            loop: true,
            speed: 600,
            lazy: true,
            preloadImages: true,
            watchSlidesVisibility: 10,
            pagination: {
                el: '#<?=$templateData['CONTAINER_ID']?> .swiper-pagination',
                clickable: true,
            },
            on: {
                slideChangeTransitionStart: function(){
                    $(window).scroll();
                }
            },
            autoplay: {
                delay: <?=$templateData['SLIDER_DELAY']?>,
                disableOnInteraction: false,
                
            },
            breakpoints: {
                575: {
                    spaceBetween: 12,
                },
                768: {
                    spaceBetween: 24,
                },
                1367: {
                    spaceBetween: 64,
                }
            },
            touchStartPreventDefault: false,
            on: {
                slideChangeTransitionStart: function() {
                    $('#<?=$templateData['CONTAINER_ID']?> .swiper-button-next, #<?=$templateData['CONTAINER_ID']?> .swiper-button-prev').addClass('hidden');
                },
                slideChangeTransitionEnd: function(){
                    $('#<?=$templateData['CONTAINER_ID']?> .swiper-button-next, #<?=$templateData['CONTAINER_ID']?> .swiper-button-prev').removeClass('hidden');
                },
            },
            navigation: {
                nextEl: '#<?=$templateData['CONTAINER_ID']?> .swiper-button-next',
                prevEl: '#<?=$templateData['CONTAINER_ID']?> .swiper-button-prev',
            },

        });
        $('#<?=$templateData['CONTAINER_ID']?> .swiper-container').on('mouseup touchend', function(e){  
            $('#<?=$templateData['CONTAINER_ID']?> .swiper-pagination').addClass('animation');
            $('#<?=$templateData['CONTAINER_ID']?> .swiper-pagination').removeClass('pause');
        })
        $('#<?=$templateData['CONTAINER_ID']?> .swiper-container').on('mousedown touchstart', function(e){
            $('#<?=$templateData['CONTAINER_ID']?> .swiper-pagination').removeClass('animation');
            $('#<?=$templateData['CONTAINER_ID']?> .swiper-pagination').addClass('pause');
        })
        
    }, this));
    
    </script>
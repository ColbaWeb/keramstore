<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arTemplateParameters = array(
	"ENABLE_AUTOSLIDE" => array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage('T_ENABLE_AUTOSLIDE'),
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"DEFAULT" => "Y",
	),
    
	"SLIDER_DELAY" => array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage('T_SLIDER_DELAY'),
		"TYPE" => "STRING",
		"MULTIPLE" => "N",
		"DEFAULT" => "7",
	)
);
?>
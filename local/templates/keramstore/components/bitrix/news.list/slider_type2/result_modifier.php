<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult['ITEMS']))
{
    $arSection = \Bitrix\Iblock\SectionTable::getList(array(
        'filter' => array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ACTIVE' => 'Y',
            'CODE' => 'slider'
        ), 
        'select' =>  array('ID')
    ))->fetch();
    
    if (!$arSection)
    {
        ShowError(GetMessage('T_SECTION_NOT_FOUND'));
        return;
    }
    
    $arSlides = $arExtraSection = array();
    foreach ($arResult['ITEMS'] as $arItem)
    {
        if ($arItem['IBLOCK_SECTION_ID'] != $arSection['ID'])
            continue;

        $arItem['ABOUT_BTN'] = array(
            'URL' => str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, SITE_DIR . $arItem['PROPERTIES']['BANNER_LINK']['VALUE']),
            'TEXT' => $arItem['PROPERTIES']['DETAIL_BTN_TEXT']['VALUE']
        );
        $arItem['CATALOG_BTN'] = array(
            'URL' => \Nextype\Alpha\Tools\Cache::CIBlockSection_GetByID($arItem['PROPERTIES']['SECTION_LINK']['VALUE'])['SECTION_PAGE_URL'],
            'TEXT' => $arItem['PROPERTIES']['CATALOG_BTN_TEXT']['VALUE']
        );

        $arItem['TEXT_COLOR'] = ($arItem['PROPERTIES']['TEXT_COLOR']['VALUE_XML_ID'] == 'white') ? 'white' : '';
        if (!empty($arItem['PROPERTIES']['BACKGROUND_IMAGE']['VALUE']))
            $arItem['BACKGROUND_IMAGE_SRC'] = CFile::GetPath((int)$arItem['PROPERTIES']['BACKGROUND_IMAGE']['VALUE']);
        
        $arSlides[] = $arItem;
    }
    
    $arResult['SLIDES'] = $arSlides;
}
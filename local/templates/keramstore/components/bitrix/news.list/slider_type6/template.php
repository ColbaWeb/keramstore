<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;

$arSlide = $arResult['SLIDE'];

$templateData = array (
    "CONTAINER_ID" => "bx_top_slider" . $this->randString(),
);

?>

<div class="wrapper-section" id="<?=$templateData['CONTAINER_ID']?>">
    <div class="hero type-6" >
        <?
            $this->AddEditAction($arSlide['ID'], $arSlide['EDIT_LINK'], CIBlock::GetArrayByID($arSlide["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arSlide['ID'], $arSlide['DELETE_LINK'], CIBlock::GetArrayByID($arSlide["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="head" data-src="<?=$arSlide['DETAIL_PICTURE']['SRC']?>" data-role="lazyload-background" id="<?=$this->GetEditAreaId($arSlide['ID']);?>">
            <?=Layout\Assets::showSpinner()?>
            
            <div class="title-block">
                <div class="title d4"><?=$arSlide['PROPERTIES']['TITLE']['VALUE']?></div>
                <div class="subtitle font-lead"><?=$arSlide['PROPERTIES']['DESCRIPTION']['VALUE']?></div>
            </div>
            <?if (!empty($arSlide['PROPERTIES']['EXTRA_SECTIONS']['VALUE'])):?>
                <div class="tags">
                    <div class="tags-inner">
                        <?foreach ($arSlide['PROPERTIES']['EXTRA_SECTIONS']['VALUE'] as $sectionID):?>
                            <a href="<?=$arResult['EXTRA_SECTIONS'][(int)$sectionID]['URL']?>" class="btn btn-tag">
                                <?=$arResult['EXTRA_SECTIONS'][(int)$sectionID]['NAME']?>
                            </a>
                        <?endforeach?>
                    </div>
                </div>
            <?endif?>
        </div>
        <?if (!empty($arResult['PRODUCT_IDS'])):?>
            <div class="content">
                <? $GLOBALS['arrFilterSliderItems']['=ID'] = $arResult['PRODUCT_IDS']; ?>
                <?$APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "homepage_slider",
                        Array(
                                "ACTION_VARIABLE" => "action",
                                "ADD_PICT_PROP" => "-",
                                "ADD_PROPERTIES_TO_BASKET" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "ADD_TO_BASKET_ACTION" => "BUY",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "BACKGROUND_IMAGE" => "-",
                                "BASKET_URL" => $arParams['BASKET_URL'],
                                "~BASKET_URL" => $arParams['BASKET_URL'],
                                "BROWSER_TITLE" => "-",
                                "CACHE_FILTER" => $arParams['CACHE_FILTER'],
                                "CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
                                "CACHE_TIME" => $arParams['CACHE_TIME'],
                                "CACHE_TYPE" => $arParams['CACHE_TYPE'],
                                "COMPATIBLE_MODE" => "N",
                                "CONVERT_CURRENCY" => "N",
                                "CUSTOM_FILTER" => "",
                                "DETAIL_URL" => "",
                                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "DISPLAY_COMPARE" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "ELEMENT_SORT_FIELD" => "sort",
                                "ELEMENT_SORT_FIELD2" => "id",
                                "ELEMENT_SORT_ORDER" => "asc",
                                "ELEMENT_SORT_ORDER2" => "desc",
                                "ENLARGE_PRODUCT" => "STRICT",
                                "FILTER_NAME" => "arrFilterSliderItems",
                                "HIDE_NOT_AVAILABLE" => "L",
                                "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                                "IBLOCK_ID" => $arParams['CAT_IBLOCK_ID'],
                                "IBLOCK_TYPE" => $arParams['CAT_IBLOCK_TYPE'],
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "LABEL_PROP" => array($arParams['CAT_LABEL_PROPERTY']),
                                "LABEL_PROP_MOBILE" => array($arParams['CAT_LABEL_PROPERTY']),
                                "LAZY_LOAD" => "Y",
                                "LINE_ELEMENT_COUNT" => "3",
                                "LOAD_ON_SCROLL" => "N",
                                "MESSAGE_404" => "",
                                "META_DESCRIPTION" => "-",
                                "META_KEYWORDS" => "-",
                                "OFFERS_FIELD_CODE" => array(),
                                "OFFERS_LIMIT" => "",
                                "OFFERS_SORT_FIELD" => "sort",
                                "OFFERS_SORT_FIELD2" => "id",
                                "OFFERS_SORT_ORDER" => "asc",
                                "OFFERS_SORT_ORDER2" => "desc",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "",
                                "PAGE_ELEMENT_COUNT" => "5",
                                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                "PRICE_CODE" => array("BASE"),
                                "PRICE_VAT_INCLUDE" => "Y",
                                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                                "PRODUCT_DISPLAY_MODE" => $arParams['CAT_PRODUCT_DISPLAY_MODE'],
                                "PRODUCT_ID_VARIABLE" => "id",
                                "PRODUCT_PROPS_VARIABLE" => "prop",
                                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                "PRODUCT_SUBSCRIPTION" => "Y",
                                "PROPERTY_CODE_MOBILE" => array(),
                                "RCM_TYPE" => "personal",
                                "SECTION_CODE" => "",
                                "SECTION_ID" => "",
                                "SECTION_ID_VARIABLE" => "",
                                "SECTION_URL" => "",
                                "SECTION_USER_FIELDS" => array("",""),
                                "SEF_MODE" => "N",
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SHOW_ALL_WO_SECTION" => "N",
                                "SHOW_CLOSE_POPUP" => "N",
                                "SHOW_DISCOUNT_PERCENT" => "N",
                                "SHOW_FROM_SECTION" => "N",
                                "SHOW_MAX_QUANTITY" => $arParams['CAT_SHOW_MAX_QUANTITY'],
                                "SHOW_OLD_PRICE" => "N",
                                "SHOW_PRICE_COUNT" => "1",
                                "SHOW_SLIDER" => "Y",
                                "SLIDER_INTERVAL" => "3000",
                                "SLIDER_PROGRESS" => "N",
                                "TEMPLATE_THEME" => "blue",
                                "USE_ENHANCED_ECOMMERCE" => "N",
                                "USE_MAIN_ELEMENT_SECTION" => "N",
                                "USE_PRICE_COUNT" => "N",
                                "USE_PRODUCT_QUANTITY" => $arParams['CAT_USE_PRODUCT_QUANTITY'],
                                "MESS_SHOW_MAX_QUANTITY" => $arParams['CAT_MESS_SHOW_MAX_QUANTITY'],
                                "RELATIVE_QUANTITY_FACTOR" => $arParams['CAT_RELATIVE_QUANTITY_FACTOR'],
                                "MESS_RELATIVE_QUANTITY_MANY" => $arParams['CAT_MESS_RELATIVE_QUANTITY_MANY'],
                                "MESS_RELATIVE_QUANTITY_FEW" => $arParams['CAT_MESS_RELATIVE_QUANTITY_FEW'],
                                "MESS_NOT_AVAILABLE" => $arParams['CAT_MESS_NOT_AVAILABLE']
                        )
                );?>
            </div>
        <?endif?>
    </div>
</div>
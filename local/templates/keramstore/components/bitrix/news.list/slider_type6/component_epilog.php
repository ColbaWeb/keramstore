<?php

use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
?>
<script>
    BX.ready(BX.delegate(function () {
        
        if ($(window).width() > 767)
        {
            var tabsWithGoodsHeight = 0, tabsWithGoodsHeight = 0,
                firstElemTabsWithGoods = $('#<?=$templateData['CONTAINER_ID']?> .tabs-with-goods .col-md-6:nth-child(2) .col-6:nth-child(3) .product-info').height(),
                secondElemTabsWithGoods = $('#<?=$templateData['CONTAINER_ID']?> .tabs-with-goods .col-md-6:nth-child(2) .col-6:nth-child(4) .product-info').height();
            
            firstElemTabsWithGoods >= secondElemTabsWithGoods ? tabsWithGoodsHeight = firstElemTabsWithGoods : tabsWithGoodsHeight = secondElemTabsWithGoods;
            
            $('#<?=$templateData['CONTAINER_ID']?> .tabs-with-goods .item.big').css('padding-bottom', tabsWithGoodsHeight + 17);
        }
        else
        {
            $('#<?=$templateData['CONTAINER_ID']?> .tabs-with-goods .item.big').css('padding-bottom', 0);
        }

    }, this));
    
    </script>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
\Bitrix\Main\Loader::includeModule('iblock');
$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-" => " "));

$arIBlocks = array();
$db_iblock = CIBlock::GetList(array("SORT" => "ASC"), array("SITE_ID" => $_REQUEST["site"], "TYPE" => ($arCurrentValues["CAT_IBLOCK_TYPE"] != "-" ? $arCurrentValues["CAT_IBLOCK_TYPE"] : "")));
while ($arRes = $db_iblock->Fetch())
    $arIBlocks[$arRes["ID"]] = "[" . $arRes["ID"] . "] " . $arRes["NAME"];

$arSorts = array("ASC" => GetMessage("T_IBLOCK_DESC_ASC"), "DESC" => GetMessage("T_IBLOCK_DESC_DESC"));
$arSortFields = array(
    "ID" => GetMessage("T_IBLOCK_DESC_FID"),
    "NAME" => GetMessage("T_IBLOCK_DESC_FNAME"),
    "ACTIVE_FROM" => GetMessage("T_IBLOCK_DESC_FACT"),
    "SORT" => GetMessage("T_IBLOCK_DESC_FSORT"),
    "TIMESTAMP_X" => GetMessage("T_IBLOCK_DESC_FTSAMP")
);

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(array("sort" => "asc", "code" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => (isset($arCurrentValues["CAT_IBLOCK_ID"]) ? $arCurrentValues["CAT_IBLOCK_ID"] : $arCurrentValues["IBLOCK_ID"])));
while ($arr = $rsProp->Fetch()) {
    $arProperty[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
    if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S"))) {
        $arProperty_LNS[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
    }
}

$arPrice = array();
if (\Bitrix\Main\Loader::includeModule('catalog'))
{
    $arPrice = \CCatalogIBlockParameters::getPriceTypesList();
}

$arTemplateParameters = array(
	"CAT_IBLOCK_TYPE" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('T_CAT_IBLOCK_TYPE'),
		"TYPE" => "LIST",
		"MULTIPLE" => "N",
		"DEFAULT" => "nt_alpha_catalog",
                "REFRESH" => "Y",
                "VALUES" => $arTypesEx
	),
	"CAT_IBLOCK_ID" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('T_CAT_IBLOCK_ID'),
		"TYPE" => "LIST",
		"MULTIPLE" => "N",
		"DEFAULT" => '={$_REQUEST["ID"]}',
                "REFRESH" => "Y",
                "ADDITIONAL_VALUES" => "Y",
                "VALUES" => $arIBlocks
	),
	"CAT_LABEL_PROPERTY" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('T_CAT_LABEL_PROPERTY'),
		"TYPE" => "LIST",
		"MULTIPLE" => "N",
		"DEFAULT" => array('TAGS'),
                "VALUES" => $arProperty_LNS
	),
	"CAT_PRICE_CODE" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('T_CAT_PRICE_CODE'),
		"TYPE" => "LIST",
		"MULTIPLE" => "Y",
                "VALUES" => $arPrice
	),
	"CAT_SHOW_MAX_QUANTITY" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('T_CAT_SHOW_MAX_QUANTITY'),
		"TYPE" => "LIST",
                "REFRESH" => "Y",
		"MULTIPLE" => "N",
		'VALUES' => array(
			'N' => GetMessage('T_CAT_SHOW_MAX_QUANTITY_N'),
			'Y' => GetMessage('T_CAT_SHOW_MAX_QUANTITY_Y'),
			'M' => GetMessage('T_CAT_SHOW_MAX_QUANTITY_M')
		),
                'DEFAULT' => array('N')
	),
	"CAT_PRODUCT_DISPLAY_MODE" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('T_CAT_PRODUCT_DISPLAY_MODE'),
		"TYPE" => "LIST",
                "REFRESH" => "Y",
                'ADDITIONAL_VALUES' => 'N',
		'VALUES' => array(
                    'N' => GetMessage('T_CAT_PRODUCT_DISPLAY_MODE_SIMPLE'),
                    'Y' => GetMessage('T_CAT_PRODUCT_DISPLAY_MODE_EXT')
		),
                'DEFAULT' => 'N',
	)
);

if (isset($arCurrentValues['CAT_SHOW_MAX_QUANTITY']))
{
        if ($arCurrentValues['CAT_SHOW_MAX_QUANTITY'] !== 'N')
        {
                $arTemplateParameters['CAT_MESS_SHOW_MAX_QUANTITY'] = array(
                        'PARENT' => 'BASE',
                        'NAME' => GetMessage('T_CAT_MESS_SHOW_MAX_QUANTITY'),
                        'TYPE' => 'STRING',
                        'DEFAULT' => GetMessage('T_CAT_MESS_SHOW_MAX_QUANTITY_DEFAULT')
                );
        }

        if ($arCurrentValues['CAT_SHOW_MAX_QUANTITY'] === 'M')
        {
                $arTemplateParameters['CAT_RELATIVE_QUANTITY_FACTOR'] = array(
                        'PARENT' => 'BASE',
                        'NAME' => GetMessage('T_CAT_RELATIVE_QUANTITY_FACTOR'),
                        'TYPE' => 'STRING',
                        'DEFAULT' => '5'
                );
                $arTemplateParameters['CAT_MESS_RELATIVE_QUANTITY_MANY'] = array(
                        'PARENT' => 'BASE',
                        'NAME' => GetMessage('T_CAT_MESS_RELATIVE_QUANTITY_MANY'),
                        'TYPE' => 'STRING',
                        'DEFAULT' => GetMessage('T_T_CAT_MESS_RELATIVE_QUANTITY_MANY_DEFAULT')
                );
                $arTemplateParameters['CAT_MESS_RELATIVE_QUANTITY_FEW'] = array(
                        'PARENT' => 'BASE',
                        'NAME' => GetMessage('T_CAT_MESS_RELATIVE_QUANTITY_FEW'),
                        'TYPE' => 'STRING',
                        'DEFAULT' => GetMessage('T_CAT_MESS_RELATIVE_QUANTITY_FEW_DEFAULT')
                );
        }
}

$arTemplateParameters['CAT_MESS_NOT_AVAILABLE'] = array(
	'PARENT' => 'BASE',
	'NAME' => GetMessage('T_CAT_MESS_NOT_AVAILABLE'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('T_CAT_MESS_NOT_AVAILABLE_DEFAULT')
);
$arTemplateParameters['CAT_USE_PRODUCT_QUANTITY'] = array(
	'PARENT' => 'BASE',
	'NAME' => GetMessage('T_CAT_USE_PRODUCT_QUANTITY'),
	'TYPE' => 'CHECKBOX',
	'DEFAULT' => 'Y'
);
$arTemplateParameters['BASKET_URL'] = array(
	'PARENT' => 'BASE',
	'NAME' => GetMessage('T_BASKET_URL'),
	'TYPE' => 'STRING',
	'DEFAULT' => '/personal/basket/'
);

?>
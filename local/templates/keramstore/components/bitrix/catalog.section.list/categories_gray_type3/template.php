<?
use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

$containerId = "categories_gray_slider";
$sliderId = "obSlider" . $this->randString();

if (!empty($arResult['SECTIONS']))
{
?>

    <div class="wrapper-section">
        <div class="container">
            <div class="categories-block type-3">
                <div class="section-name">
                    <? if (!empty($arParams['SECTION_TITLE'])): ?>
                    <div class="h2 title"><?=$arParams['SECTION_TITLE']?></div>
                    <? endif; ?>
                    
                    <? if (!empty($arResult['SECTION_LINK_TITLE'])): ?>
                    <a href="<?=$arResult['SECTION_LINK_URL']?>" class="subtitle font-large-button"><?=$arResult['SECTION_LINK_TITLE']?> <i
                            class="icon icon-arrow-right-text-button"></i></a>
                    <? endif; ?>
                    
                </div>
                <div class="categories-list row">
                    <div class="swiper-slider inline-slider">
                        <!-- Swiper -->
                        <div class="swiper-container" id="<?=$containerId?>">
                            <div class="swiper-wrapper">
                                <?
                                foreach ($arResult['SECTIONS'] as $arSection) 
                                {
                                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                                    ?>
                                <div class="swiper-slide" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                                    <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="item">
                                        <div class="image">
                                            <img class="img swiper-lazy" alt="<?=$arSection['PICTURE']['ALT']?>" title="<?=$arSection['PICTURE']['TITLE']?>" data-src="<?=$arSection['PICTURE']['SRC']?>" />
                                        </div>
                                        <div class="title font-title"><?=$arSection['NAME'];?></div>
                                    </a>
                                </div>
                                <?
                                }
                                ?>
                                
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-arrows">
                            <div class="swiper-button-next" data-container="<?=$containerId?>"></div>
                            <div class="swiper-button-prev" data-container="<?=$containerId?>"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var <?=$sliderId?> = new Swiper('#<?=$containerId?>', {
            slidesPerView: "auto",
            spaceBetween: 8,
            freeMode: true,
            lazy: true,
            watchSlidesVisibility: 'auto',
            breakpoints: {
                992: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                    slidesPerGroup: 4,
                    freeMode: false,
                    navigation: {
                        nextEl: '.swiper-button-next[data-container="<?=$containerId?>"]',
                        prevEl: '.swiper-button-prev[data-container="<?=$containerId?>"]',
                    },
                }
            }
        });

        $(window).resize(function () {
            if ($(window).innerWidth() <= 992) {
                <?=$sliderId?>.update();
            }
        });
    </script>
    
<?
}
?>
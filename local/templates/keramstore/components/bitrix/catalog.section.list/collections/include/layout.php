<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Bitrix\Main\Grid\Declension;

if (count($arResult['SECTIONS']) > 0)
{
    ?>
    <h3><?= Loc::getMessage('COLLECTION_H3') ?></h3>
    <div class="categories-with-photo type5">
        <div class="categories-list row">
            <?
            for ($i = 0; $i < count($arResult['SECTIONS']); $i++)
            {
                if (!isset($arResult['SECTIONS'][$i]) || empty($arResult['SECTIONS'][$i]))
                    continue;

                $arSection = $arResult['SECTIONS'][$i];
                $component->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $component->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                ?>
                <div class="category-wrap" id="<?= $component->GetEditAreaId($arSection['ID']); ?>">
                    <a href="<?= $arSection['SECTION_PAGE_URL'] ?>" class="category">
                        <div class="image-wrap">
                            <?
                            echo Layout\Assets::showBackgroundImage($arSection['PICTURE']['SRC'], array(
                                "attributes" => array(
                                    "class=\"bg-img\""
                                )
                            ));
                            ?>
                            <div class="collection-property">
                                <?if($arSection["UF_NEW"]){?>
                                    <span class="collection-new"><?= Loc::getMessage('NEW') ?></span>
                                <?}?>
                                <?if($arSection["UF_HIT"]){?>
                                    <span class="collection-hit"><?= Loc::getMessage('HIT') ?></span>
                                <?}?>
                                <?if($arSection["UF_SALE"]){?>
                                    <span class="collection-sale"><?= Loc::getMessage('SALE') ?></span>
                                <?}?>
                            </div>
                            <?if($arSection["UF_IN_SALON"]){?>
                                <div class="collection-in-salon">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/in_salon.svg" alt="Интернет-магазин керамической плитки Keramstore">
                                    <span class="in-salon"><?= Loc::getMessage('IN_SALON') ?></span>
                                </div>
                            <?}?>
                        </div>
                        <div class="overlay">
                            <?
                            if ($arParams["COUNT_ELEMENTS"] && $arSection['ELEMENT_CNT'] !== null)
                            {
                                $itemDeclension = new Declension(Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_1'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_2'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_3'));
                                ?><div class="count font-body-small"><?= $arSection['ELEMENT_CNT'] . " " . $itemDeclension->get($arSection['ELEMENT_CNT']) ?></div><?
                            }
                            ?>
                            <div class="title font-title"><?= $arSection['NAME'] ?></div>
                            <?if(!empty($arSection['BRAND']['NAME'])) {?>
                                <div class="title font-title coursive">
                                    <?= $arSection['BRAND']['NAME'] ?> (<?=$arSection['COUNTRY']["NAME"]?>)
                                    <!--<a href="<?/*= $arSection['BRAND']['DETAIL_PAGE_URL'] */?>"> <?/*= $arSection['BRAND']['NAME'] */?></a>-->
                                    <!-- <?/*=$arSection['COUNTRY']["NAME"]*/?> <img width="20px;" src="<?/*=$arSection['COUNTRY']["IMG"]*/?>">-->
                                </div>
                            <? }?>
                            <?if($arSection['UF_MIN_PRICE']){?>
                                <div class="collection_min-price">
                                    <?echo Loc::getMessage(
                                        'MIN_PRICE',
                                        array(
                                            '#PRICE#' => $arSection["UF_MIN_PRICE"],
                                        )
                                    );?>
                                </div>
                            <?} else {?>
                                <div class="collection_min-price">
                                    <?echo Loc::getMessage(
                                        'MIN_PRICE_NULL',
                                        array(
                                            '#MESSAGE#' => "по запросу",
                                        )
                                    );?>
                                </div>
                            <? } ?>
                            <div class="hover-content">
                                <?if($arSection['UF_SURFACE']){?>
                                    <div class="count font-body-small"><?= Loc::getMessage('SURFACE') ?><?=(implode(", ", $arSection['UF_SURFACE']))?></div>
                                <?}?>
                                <?if($arSection['UF_SIZES']){?>
                                    <div class="count font-body-small"><?= Loc::getMessage('SIZES') ?><?=(implode(", ", $arSection['UF_SIZES']))?></div>
                                <?}?>
                                <?if($arSection["UF_SIZES_LINE"]){
                                    $sizes_line = str_replace(';', ', ', $arSection["UF_SIZES_LINE"]);
                                    ?>
                                    <div class="collection_min-price">
                                        <?echo Loc::getMessage(
                                            'SIZES_LINE',
                                            array(
                                                '#SIZES_LINE#' => $sizes_line,
                                            )
                                        );?>
                                    </div>
                                <?}?>
                                <?if($arSection["UF_SURFACE_LINE"]){
                                    $surface_line = str_replace(';', ', ', $arSection["UF_SURFACE_LINE"]);
                                    ?>
                                    <div class="collection_min-price">
                                        <?echo Loc::getMessage(
                                            'SURFACE_LINE',
                                            array(
                                                '#SURFACE_LINE#' => $surface_line,
                                            )
                                        );?>
                                    </div>
                                <?}?>
                                <?
                                /*if (!empty($arSection['DESCRIPTION']))
                                {
                                    */?><!--<div class="desc font-body-small"><?php /*= $arSection['DESCRIPTION'] */?></div>--><?/*
                        }*/
                                ?>
                                <div class="link font-large-button">
                                    <span><?= Loc::getMessage('CT_BCSL_DETAIL_SECTION_URL') ?></span>
                                    <i class="icon icon-arrow-right-text-button-white"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <?
            }
            ?>
        </div>
    </div>
    <?
//    Bitrix\Main\Diag\Debug::dump($arResult['SECTIONS']);
}
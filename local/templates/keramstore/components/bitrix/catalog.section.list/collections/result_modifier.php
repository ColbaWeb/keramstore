<?
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

Loader::includeModule("highloadblock");

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$arResizeParams = array ('width' => 450, 'height' => 450);
$arBigResizeParams = array ('width' => 850, 'height' => 850);

if (!empty($arResult['SECTIONS']))
{


    $arUfFieldsId = array("UF_SURFACE" => 80, "UF_SIZES" => 81);
    $enum = array();

    foreach ($arUfFieldsId as $code => $id){
        $obEnum = new \CUserFieldEnum;
        $rsEnum = $obEnum->GetList(array(), array("USER_FIELD_ID" => $id));

        while($arEnum = $rsEnum->Fetch())
        {
            $enum[$code][$arEnum["ID"]] = $arEnum["VALUE"];
        }
    }

    $obTree = new Tools\Tree($arResult['SECTIONS']);
    $arResult['SECTIONS'] = $obTree->getTree();
    
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        $arSection['PICTURE'] = array ('SRC' => '');
        if (!empty($arSection['DETAIL_PICTURE']))
        {
            $pictureId = is_array($arSection['DETAIL_PICTURE']) ? $arSection['DETAIL_PICTURE']['ID'] : $arSection['DETAIL_PICTURE'];
            $arResize = CFile::ResizeImageGet($pictureId, 
                    (count($arResult['SECTIONS']) >= 5 && $key == 0) ? $arBigResizeParams : $arResizeParams,
                    BX_RESIZE_IMAGE_EXACT
            );
            
            if ($arResize)
            {
                $arSection['PICTURE']['SRC'] = $arResize['src'];
            }
        }

        if($arSection["UF_SURFACE"]){
            foreach ($arSection["UF_SURFACE"] as &$field){
                $field = $enum["UF_SURFACE"][$field];
            }
        }

        if($arSection["UF_SIZES"]){
            foreach ($arSection["UF_SIZES"] as &$field){
                $field = $enum["UF_SIZES"][$field];
            }
        }

        if($arSection["UF_BRANDS"]) {
            $arSelect = Array("ID", "NAME", "PROPERTY_COUNTRY", "DETAIL_PAGE_URL");
            $arFilter = Array("IBLOCK_ID"=>16, "ID" =>$arSection["UF_BRANDS"] ,"ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
            if($ob = $res->GetNextElement())
            {
                $brand = $ob->GetFields();
            }
            $arSection["BRAND"] = $brand;

            $hlbl = 7;
            $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $rsData = $entity_data_class::getList(array(
                "select" => array("UF_NAME", "UF_FILE"),
                "filter" => array("UF_XML_ID" => $brand["PROPERTY_COUNTRY_VALUE"])
            ));
            while($arData = $rsData->Fetch()){
                $country["NAME"] = $arData["UF_NAME"];
                $country["IMG"] = CFile::GetPath($arData["UF_FILE"]);
            }
            $arSection["COUNTRY"] = $country;
           /* echo "<pre>";
            print_r($arSection["COUNTRY"]);
            echo "</pre>";*/
        }
        
        $arResult['SECTIONS'][$key] = $arSection;
    }
}

//echo("<pre>");
//var_dump($arResult);
//die();
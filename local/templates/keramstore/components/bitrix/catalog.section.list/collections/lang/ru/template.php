<?
$MESS["CT_BCSL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BCSL_ELEMENTS_COUNT_1"] = "товар";
$MESS["CT_BCSL_ELEMENTS_COUNT_2"] = "товара";
$MESS["CT_BCSL_ELEMENTS_COUNT_3"] = "товаров";
$MESS["CT_BCSL_DETAIL_SECTION_URL"] = "В коллекцию";
$MESS["NEW"] = "NEW";
$MESS["HIT"] = "HIT";
$MESS["SALE"] = "SALE";
$MESS["SURFACE"] = "Поверхность: ";
$MESS["SIZES"] = "Размеры: ";
$MESS["IN_SALON"] = "В салоне";
$MESS["COLLECTION_H3"] = "Коллекции";
$MESS["MIN_PRICE"] = "Цена от: #PRICE# руб. за м²";
$MESS["SIZES_LINE"] = "Размеры: #SIZES_LINE#";
$MESS["SURFACE_LINE"] = "Поверхность: #SURFACE_LINE#";

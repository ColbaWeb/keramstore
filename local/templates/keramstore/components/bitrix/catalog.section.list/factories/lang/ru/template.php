<?
$MESS["CT_BCSL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BCSL_ELEMENTS_COUNT_1"] = "товар";
$MESS["CT_BCSL_ELEMENTS_COUNT_2"] = "товара";
$MESS["CT_BCSL_ELEMENTS_COUNT_3"] = "товаров";
$MESS["CT_BCSL_SECTIONS_COUNT_1"] = "коллекция";
$MESS["CT_BCSL_SECTIONS_COUNT_2"] = "коллекции";
$MESS["CT_BCSL_SECTIONS_COUNT_3"] = "коллекций";
$MESS["CT_BCSL_DETAIL_SECTION_URL"] = "Перейти";
$MESS["FACTORY_H3"] = "Фабрики";
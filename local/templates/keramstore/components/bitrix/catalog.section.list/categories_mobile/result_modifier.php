<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (!function_exists('getSectionsMultilevel'))
{
    function getMinimalDepthLevel($arItems)
    {
        $depth = intval($arItems[0]['DEPTH_LEVEL']);
        foreach ($arItems as $arItem)
            if (intval($arItem['DEPTH_LEVEL']) < $depth)
                $depth = intval($arItem['DEPTH_LEVEL']);
            
        return (empty($depth)) ? $depth : $depth - 1;
    }
    
    function getSectionsMultilevel($arItems, $currentDepth = 0, &$currentKey = 0)
    {

        if (is_array($arItems))
        {
            if (empty($currentKey))
                $currentDepth = getMinimalDepthLevel($arItems);


            $arResult = Array();
            for ($key = $currentKey; $key < count($arItems); $key++)
            {
                $arItem = $arItems[$key];
                if ($currentDepth > $arItem['DEPTH_LEVEL'] - 1)
                    break;

                if (!empty($arItems[$key + 1]) && $arItems[$key + 1]['DEPTH_LEVEL'] > $currentDepth)
                {
                    $key++;
                    $arItem['SUB'] = getSectionsMultilevel($arItems, $currentDepth + 1, $key);
                    $key--;
                }

                $arResult[] = $arItem;
            }

            $currentKey = $key;

            return $arResult;
        }

        return false;
    }
}

$arResult['SECTIONS'] = getSectionsMultilevel(array_values($arResult['SECTIONS']));

if (isset($arResult['SECTION']['IBLOCK_SECTION_ID']) && !empty($arResult['SECTION']['IBLOCK_SECTION_ID']))
{
    if ($arParentSection = \CIBlockSection::GetByID($arResult['SECTION']['IBLOCK_SECTION_ID'])->GetNext())
    {
        $arResult['PARENT_SECTION'] = $arParentSection;
    }
}
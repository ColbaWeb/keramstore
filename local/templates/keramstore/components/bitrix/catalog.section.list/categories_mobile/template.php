<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Alpha\Layout;
use Bitrix\Main\Grid\Declension;
use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$strHeadTitleId = "mobile_menu_" . randString(6);

$strHeadTitleText = $strHeadTitleLink = "";
if (isset($arResult['PARENT_SECTION']) && isset($arResult['PARENT_SECTION']['NAME']))
{
    $strHeadTitleText = $arResult['SECTION']['NAME'];
    $strHeadTitleLink = $arResult['PARENT_SECTION']['SECTION_PAGE_URL'];
}
elseif (!empty($arResult['SECTION']['LIST_PAGE_URL']))
{
    $strHeadTitleText = Loc::getMessage('CT_BCSL_CATALOG_TITLE');
    $strHeadTitleLink = $arResult['SECTION']['LIST_PAGE_URL'];
}


if (!empty($arResult['SECTIONS']))
{
?>

    <div class="modal-container mobile hidden js-catalog-mobile">
        <div class="modal-content-container">
            
                <div class="content-wrapper">
                    <div class="menu-catalog-modal">
                        <div class="head-title<?=empty($strHeadTitleText) ? ' d-none' : ''?>" id="<?=$strHeadTitleId?>">
                                <a class="back" onclick="<?=$strHeadTitleId?>_back(this); event.preventDefault();" href="<?= $strHeadTitleLink ?>">
                                    <i class="icon icon-arrow-light-left"></i>
                                </a>
                                <span class="title"><?= $strHeadTitleText; ?></span>
                            </div>
                        
                        <div class="scrollbar-inner">
                            <div class="menu-container">

                                <ul class="menu-list-1 list reset-ul-list">

                                    <?
                                        foreach ($arResult['SECTIONS'] as $arSection)
                                        {
                                            $arItemClasses = array ();

                                            if (!empty($arSection['SUB']))
                                                $arItemClasses[] = "has-sub";

                                            ?>
                                            <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                                                <a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?=$arSection['NAME']; ?></a>

                                                <?
                                                if (!empty($arSection['SUB']))
                                                {
                                                    ?>
                                                    <div class="has-sub" onclick="<?=$strHeadTitleId?>('<?=$arSection['NAME']; ?>', '<?=$strHeadTitleLink?>')"></div>
                                                    <div class="sub">
                                                        <ul class="menu-list-2 list reset-ul-list">
                                                            <?
                                                            foreach ($arSection['SUB'] as $arSection2)
                                                            {
                                                                $arItemClasses = array ();

                                                                if (!empty($arSection2['SUB']))
                                                                    $arItemClasses[] = "has-sub";
                                                                ?>
                                                                <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                                                                    <a href="<?= $arSection2['SECTION_PAGE_URL'] ?>"><?=$arSection2['NAME']; ?></a>

                                                                    <?
                                                                    if (!empty($arSection2['SUB']))
                                                                    {
                                                                        ?>
                                                                        <div class="has-sub" onclick="<?=$strHeadTitleId?>('<?=$arSection['NAME']; ?>', '<?=$arSection['SECTION_PAGE_URL']?>')"></div>
                                                                        <div class="sub">
                                                                            <ul class="menu-list-3 list reset-ul-list">
                                                                                <?
                                                                                foreach ($arSection2['SUB'] as $arSection3)
                                                                                {
                                                                                    ?>
                                                                                    <li>
                                                                                        <a href="<?= $arSection3['SECTION_PAGE_URL'] ?>"><?=$arSection3['NAME']; ?></a>
                                                                                    </li>    
                                                                                    <?
                                                                                }
                                                                                ?>
                                                                                
                                                                            </ul>
                                                                        </div>
                                                                        <?
                                                                    }
                                                                    ?>
                                                                </li>
                                                                <?
                                                            }
                                                            ?>
                                                            
                                                        </ul>
                                                    </div>
                                                    <?
                                                }
                                                ?>

                                            </li>
                                            <?
                                        }

                                    
                                    ?>

                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
        </div>
        <div class="close-area"></div>
    </div>
    <script>
        function <?=$strHeadTitleId?>(title, backUrl) {
            if (title != "") {
                $("#<?=$strHeadTitleId?>").find('.title').text(title);
                $("#<?=$strHeadTitleId?>").find('a.back').attr('href', backUrl);
                
                $("#<?=$strHeadTitleId?>").removeClass('d-none');
            } else {
                $("#<?=$strHeadTitleId?>").addClass('d-none');
            }
        }
        
        function <?=$strHeadTitleId?>_back(element) {
            var parent = $(element).parents('.js-catalog-mobile'),
                sub = parent.find('.sub.open');
            
            if (sub.length > 0) {
                sub.removeClass('open');
                
                $(".menu-container .list").css('height', 'auto');
                $(".menu-container .list").css('height', sub.parent().parent().outerHeight());
            } else {
                window.location = $(element).attr('href');
            }
        }
    </script>
<?
}
else
{
    ?>
    <script>
    document.querySelector('.btn-show-catalog').closest('.mobile-buttons').classList.add('hide-sections-button');
    </script>    
    <?
}
?>

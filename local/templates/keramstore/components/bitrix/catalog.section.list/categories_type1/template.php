<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Alpha\Layout;
use Bitrix\Main\Grid\Declension;
use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (!empty($arResult['SECTIONS']))
{
    $containerId = "bx_slider_" . $this->randString();
    ?>

    <div class="subcategories">

        <div class="swiper-slider">
            <div class="swiper-container" id="<?=$containerId?>">
                <div class="swiper-wrapper">
                    <?
                    foreach ($arResult['SECTIONS'] as $arSection)
                    {
                        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);


                        ?>
                        <div class="swiper-slide" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                            <a href="<?=$arSection['SECTION_PAGE_URL']; ?>" class="subcategory">
                                <div class="image-wrap">
                                    <div class="bg-img swiper-lazy" data-background="<?=$arSection['DETAIL_PICTURE']['SRC']?>"><?=Layout\Assets::showSpinner(true)?></div>
                                </div>
                                <div class="overlay">
                                    <?
                                    if ($arParams["COUNT_ELEMENTS"] && $arSection['ELEMENT_CNT'] !== null)
                                    {
                                        $itemDeclension = new Declension(Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_1'), Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_2'), Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_3'));
                                        $itemDeclensionText = $itemDeclension->get($arSection['ELEMENT_CNT']);

                                        ?><div class="quantity-goods font-tiny-text"><?=$arSection['ELEMENT_CNT']?> <?=$itemDeclensionText?></div><?
                                    }
                                    ?>
                                    <div class="title font-title-small"><?=$arSection['NAME']; ?></div>
                                </div>
                            </a>
                        </div>
                        <?
                    }
                    ?>

                </div>
            </div>
            <div class="swiper-arrows">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
    <script>
        BX.ready(function() {
            var ob<?=$containerId?> = new Swiper('#<?=$containerId?>', {
                slidesPerView: 6,
                spaceBetween: 24,
                slidesPerGroup: 6,
                freeMode: false,
                lazy: true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });

            $(window).resize(function () {
                if ($(window).innerWidth() <= 1024) {
                    ob<?=$containerId?>.init();
                }
            });
        });
    </script>
    <?
}
?>
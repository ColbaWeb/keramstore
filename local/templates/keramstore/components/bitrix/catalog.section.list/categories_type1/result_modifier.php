<?
use Nextype\Alpha\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!empty($arResult['SECTIONS']))
{
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        if (!empty($arParams["COUNT_ELEMENTS"]) && $arParams['COUNT_ELEMENTS_FILTER'] == "CNT_AVAILABLE" && empty($arSection['ELEMENT_CNT']))
        {
            unset($arResult['SECTIONS'][$key]);
            continue;
        }
        
        if (!empty($arSection['DETAIL_PICTURE']))
        {
            if ($arImage = CFile::ResizeImageGet($arSection['DETAIL_PICTURE'], array('width' => 350, 'height' => 350), BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
            {
                $arSection['DETAIL_PICTURE'] = array (
                    'SRC' => $arImage['src']
                );
            }
        }
        
        $arResult['SECTIONS'][$key] = $arSection;
    }
}

<?
use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

$containerId = "categories_with_photo_slider";
$sliderId = "obSlider" . $this->randString();

if (!empty($arResult['SECTIONS']))
{
?>
    <div class="wrapper-section">
        <div class="categories-with-photo type3">
            <div class="container">
                <div class="section-name">
                        <? if (!empty($arParams['SECTION_TITLE'])): ?>
                            <div class="h2 title"><?= $arParams['SECTION_TITLE'] ?></div>
                        <? endif; ?>

                        <? if (!empty($arResult['SECTION_LINK_TITLE'])): ?>
                            <a href="<?= $arResult['SECTION_LINK_URL'] ?>" class="subtitle font-large-button"><?= $arResult['SECTION_LINK_TITLE'] ?> <i class="icon icon-arrow-right-text-button"></i></a>
                        <? endif; ?>
                </div>
            </div>

            <div class="swiper-slider">
                <div class="swiper-container" id="<?=$containerId?>">
                    <div class="swiper-wrapper">
                        <?
                        foreach ($arResult['SECTIONS'] as $arSection)
                        {
                            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                            ?>
                        <div class="swiper-slide" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                            <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="category">
                                <div class="image-wrap">
                                    <div class="bg-img swiper-lazy" data-background="<?=$arSection['DETAIL_PICTURE']['SRC']?>">
                                        <?=Layout\Assets::showSpinner(true)?>
                                    </div>
                                </div>
                                <div class="overlay">
                                    <div class="title font-title"><?=$arSection['NAME'];?></div>
                                    <div class="hover-content">
                                        <? if (!empty($arSection['DESCRIPTION'])): ?>
                                            <div class="desc font-body-small"><?=$arSection['DESCRIPTION']?></div>
                                            <? endif; ?>
                                        <div class="link font-large-button">
                                            <span><?=GetMessage('CP_TPL_DETAIL_PAGE_TITLE')?></span>
                                            <i class="icon icon-arrow-right-text-button-white"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?
                        }
                        ?>
                        
                        
                        
                    </div>
                </div>
                <div class="swiper-arrows">
                    <div class="swiper-button-next" data-container="<?=$containerId?>"></div>
                    <div class="swiper-button-prev" data-container="<?=$containerId?>"></div>
                </div>

            </div>


            

        </div>
    </div>

    <script>
        var <?=$sliderId?> = new Swiper('#<?=$containerId?>', {
            slidesPerView: "auto",
            spaceBetween: 8,
            freeMode: true,
            lazy: true,
            watchSlidesVisibility: 'auto',
            breakpoints: {
                992: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                    slidesPerGroup: 4,
                    freeMode: false,
                    navigation: {
                        nextEl: '.swiper-button-next[data-container="<?=$containerId?>"]',
                        prevEl: '.swiper-button-prev[data-container="<?=$containerId?>"]',
                    },
                }
            }
        });


        $(window).resize(function() {
            if ($(window).innerWidth() <= 1024)
            {
                <?=$sliderId?>.init();
            }
        });
    </script>

<?
}
?>
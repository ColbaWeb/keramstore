<?php

$MESS['CP_TPL_SECTION_TITLE'] = "Заголовок блока";
$MESS['CP_TPL_SECTION_MORE_LINK_TYPE'] = "Вариант отображения ссылки рядом с заголовком блока";
$MESS['CP_TPL_SECTION_MORE_LINK_TYPE_HIDE'] = "Не отображать";
$MESS['CP_TPL_SECTION_MORE_LINK_TYPE_BY_SECTION'] = "Динамическая ссылка на раздел";
$MESS['CP_TPL_SECTION_MORE_LINK_TYPE_BY_CUSTOM'] = "Своя ссылка";
$MESS['CP_TPL_SECTION_MORE_LINK_TITLE'] = "Текст ссылки";
$MESS['CP_TPL_SECTION_MORE_LINK_URL'] = "Url ссылки";
$MESS['CP_TPL_SECTION_MORE_LINK_SECTION_ID'] = "Раздел для генерации ссылки";
$MESS['CP_TPL_SHOW_SECTIONS_ID'] = "Разделы для отображения (если не выбрано, отображаются все)";
$MESS['CP_TPL_SHOW_SECTIONS_COUNT'] = "Количество разделов для отображения";
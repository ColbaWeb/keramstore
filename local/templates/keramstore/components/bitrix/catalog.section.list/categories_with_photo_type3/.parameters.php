<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

/**
 * @var string $componentPath
 * @var string $componentName
 * @var array $arCurrentValues
 * @var array $arTemplateParameters
 */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Web\Json;
use Bitrix\Iblock;

if (!Loader::includeModule('iblock'))
    return;

$treeValues = array();
$tree = CIBlockSection::GetTreeList(
                $arFilter = Array('IBLOCK_ID' => $arCurrentValues['IBLOCK_ID']),
                $arSelect = Array()
);

while ($section = $tree->fetch())
{
    $value = "";
    for ($depth = 1; $depth < $section['DEPTH_LEVEL']; $depth++)
        $value .= "-";

    $value = !empty($value) ? $value . " " . $section['NAME'] : $section['NAME'];

    $treeValues[$section['ID']] = $value;
}

$arTemplateParameters = array(
    
    "SECTION_TITLE" => array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_TPL_SECTION_TITLE'),
        'TYPE' => 'STRING',
        'DEFAULT' => '',
    ),
    
    "SECTION_LINK_TYPE" => array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_TPL_SECTION_MORE_LINK_TYPE'),
        'TYPE' => 'LIST',
        'MULTIPLE' => 'N',
        'ADDITIONAL_VALUES' => 'N',
        'REFRESH' => 'Y',
        'DEFAULT' => 'C',
        'VALUES' => array(
            'N' => GetMessage('CP_TPL_SECTION_MORE_LINK_TYPE_HIDE'),
            'S' => GetMessage('CP_TPL_SECTION_MORE_LINK_TYPE_BY_SECTION'),
            'C' => GetMessage('CP_TPL_SECTION_MORE_LINK_TYPE_BY_CUSTOM')
        )
    )
);


if ($arCurrentValues['SECTION_LINK_TYPE'] == "S" && !empty($arCurrentValues['IBLOCK_ID']))
{
    $arTemplateParameters["SECTION_LINK_SECTION_ID"] = array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_TPL_SECTION_MORE_LINK_SECTION_ID'),
        'TYPE' => 'LIST',
        'VALUES' => $treeValues,
        'DEFAULT' => '',
    );
    
}
elseif ($arCurrentValues['SECTION_LINK_TYPE'] == "C")
{
    $arTemplateParameters["SECTION_LINK_TITLE"] = array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_TPL_SECTION_MORE_LINK_TITLE'),
        'TYPE' => 'STRING',
        'DEFAULT' => '',
    );
    
    $arTemplateParameters["SECTION_LINK_URL"] = array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_TPL_SECTION_MORE_LINK_URL'),
        'TYPE' => 'STRING',
        'DEFAULT' => '',
    );
}

$arTemplateParameters["SHOW_SECTIONS_ID"] = array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_TPL_SHOW_SECTIONS_ID'),
        'TYPE' => 'LIST',
        'MULTIPLE' => 'Y',
        'VALUES' => $treeValues,
        'DEFAULT' => '',
    );

$arTemplateParameters["SHOW_SECTIONS_COUNT"] = array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_TPL_SHOW_SECTIONS_COUNT'),
        'TYPE' => 'STRING',
        'DEFAULT' => '2',
    );
<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

if (!empty($arResult['SECTIONS']))
{
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        if (!empty($arSection['DETAIL_PICTURE']))
        {
            if ($arImage = CFile::ResizeImageGet($arSection['DETAIL_PICTURE'], array('width' => 700, 'height' => 700), BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
            {
                $arSection['DETAIL_PICTURE'] = array (
                    'SRC' => $arImage['src']
                );
            }
        }

        $arResult['SECTIONS'][$key] = $arSection;
    }
}
<?
use Nextype\Alpha\Layout,
    Bitrix\Main\Localization\Loc;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

$bShowProductsCount = ($arParams['SHOW_PRODUCTS_COUNT'] === 'Y');
if ($bShowProductsCount)
{
    $itemDeclension = new \Bitrix\Main\Grid\Declension(
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_1'),
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_2'),
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_3'));
}

if (!empty($arResult['SECTIONS'])):?>
    <div class="categories-with-photo type4">
        <div class="categories-list row">
            <? $sectionNum = 0; 
            foreach ($arResult['SECTIONS'] as $arSection): ?>
            <?
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                $sectionNum++;
            ?>
                <?if ($sectionNum == 1):?>
                    <div class="category-wrap col-12 col-md-6" id="<?=$this->GetEditAreaId($arSection['ID'])?>">
                        <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="category">
                            <div class="image-wrap">
                                <?=Layout\Assets::showBackgroundImage($arSection['DETAIL_PICTURE']['SRC'], array('attributes' => array('class="bg-img"')), true)?>
                            </div>
                            <div class="overlay">
                                
                                <?if ($bShowProductsCount && !empty($arSection['ELEMENT_CNT'])):?>
                                    <div class="subtitle font-tiny-text">
                                        <?=$arSection['ELEMENT_CNT'] . ' ' . $itemDeclension->get($arSection['ELEMENT_CNT']);?>
                                    </div>
                                <?endif?>
                                
                                <div class="title font-title"><?=$arSection['NAME']?></div>
                                <div class="hover-content">
                                    
                                    <?if (!empty($arSection['DESCRIPTION']) && $arParams['SHOW_CATEGORY_DESCTIPTION'] == 'Y'):?>
                                        <div class="desc font-body-small"><?=$arSection['DESCRIPTION']?></div>
                                    <?endif?>
                                        
                                    <?if ($arParams['SHOW_CATEGORY_BUTTON'] == 'Y'):?>
                                        <div class="link font-large-button">
                                            <span><?=Loc::getMessage('CP_TPL_DETAIL_PAGE_TITLE')?></span>
                                            <i class="icon icon-arrow-right-text-button-white"></i>
                                        </div>
                                    <?endif?>
                                        
                                </div>
                            </div>
                        </a>
                    </div>
                <?else:?>
                    <?if ($sectionNum == 2):?>
                        <div class="category-wrap col-12 col-md-6">
                            <div class="row">
                    <?endif?>
                                
                            <div class="category-wrap col-6" id="<?=$this->GetEditAreaId($arSection['ID'])?>">
                                <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="category">
                                    <div class="image-wrap">
                                        <?=Layout\Assets::showBackgroundImage($arSection['DETAIL_PICTURE']['SRC'], array('attributes' => array('class="bg-img"')), true)?>
                                    </div>
                                    <div class="overlay">
                                        
                                        <?if ($bShowProductsCount && !empty($arSection['ELEMENT_CNT'])):?>
                                            <div class="subtitle font-tiny-text">
                                                <?=$arSection['ELEMENT_CNT'] . ' ' . $itemDeclension->get($arSection['ELEMENT_CNT']);?>
                                            </div>
                                        <?endif?>
                                        
                                        <div class="title font-title"><?=$arSection['NAME']?></div>
                                        <div class="hover-content">
                                            <?if (!empty($arSection['DESCRIPTION']) && $arParams['SHOW_CATEGORY_DESCTIPTION'] == 'Y'):?>
                                                <div class="desc font-body-small"><?=$arSection['DESCRIPTION']?></div>
                                            <?endif?>
                                                
                                            <?if ($arParams['SHOW_CATEGORY_BUTTON'] == 'Y'):?>
                                                <div class="link font-large-button">
                                                    <span><?=Loc::getMessage('CP_TPL_DETAIL_PAGE_TITLE')?></span>
                                                    <i class="icon icon-arrow-right-text-button-white"></i>
                                                </div>
                                            <?endif?>
                                                
                                        </div>
                                    </div>
                                </a>
                            </div>

                    <?if ($sectionNum == 5):?>
                            </div>
                        </div>
                    <?endif?>
                <?endif?>
            
                <?if ($sectionNum == 5) break; ?>
            
            <?endforeach?>
        </div>
    </div>
<? endif; ?>

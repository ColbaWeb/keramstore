<?
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

Loader::includeModule("highloadblock");

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

if ($arParams['SECTION_LINK_TYPE'] == 'C' && !empty($arParams['SECTION_LINK_TITLE']) && !empty($arParams['SECTION_LINK_URL']))
{
    $arResult['SECTION_LINK_TITLE'] = $arParams['SECTION_LINK_TITLE'];
    $arResult['SECTION_LINK_URL'] = $arParams['SECTION_LINK_URL'];
}
elseif ($arParams['SECTION_LINK_TYPE'] == 'S' && !empty($arParams['SECTION_LINK_SECTION_ID']))
{
    if ($arSection = CIBlockSection::GetByID($arParams['SECTION_LINK_SECTION_ID'])->GetNext())
    {
        $arResult['SECTION_LINK_TITLE'] = $arSection['NAME'];
        $arResult['SECTION_LINK_URL'] = $arSection['SECTION_PAGE_URL'];
    }
    
}

if (is_array($arParams['SHOW_SECTIONS_ID']) && !empty($arParams['SHOW_SECTIONS_ID']) && !empty($arResult['SECTIONS']))
{
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        if (!in_array($arSection['ID'], $arParams['SHOW_SECTIONS_ID']))
        {
            unset($arResult['SECTIONS'][$key]);
            continue;
        }
    }
}

if (intval($arParams['SHOW_SECTIONS_COUNT']) > 0)
{
    $counter = 0;
    
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        if (intval($arParams['SHOW_SECTIONS_COUNT']) <= $counter)
        {
            unset($arResult['SECTIONS'][$key]);
        }
        
        $counter++;
    }
}

foreach ($arResult['SECTIONS'] as $key => $arSection)
{
    if (!empty($arSection['DETAIL_PICTURE']))
    {
        if ($arImage = CFile::ResizeImageGet($arSection['DETAIL_PICTURE'], array('width' => 450, 'height' => 450), BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arSection['DETAIL_PICTURE'] = array (
                'SRC' => $arImage['src']
            );
        }
    }

    if(!empty($arSection['IBLOCK_SECTION_ID'])){
        $arResult["PARENTS_ID"][] = $arSection['IBLOCK_SECTION_ID'];
    }

    if($arSection["UF_BRANDS"]) {
        $arSelect = Array("ID", "NAME", "PROPERTY_COUNTRY", "DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID"=>16, "ID" =>$arSection["UF_BRANDS"] ,"ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
        if($ob = $res->GetNextElement())
        {
            $brand = $ob->GetFields();
        }
        $arSection["BRAND"] = $brand;

        $hlbl = 7;
        $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $rsData = $entity_data_class::getList(array(
            "select" => array("UF_NAME", "UF_FILE"),
            "filter" => array("UF_XML_ID" => $brand["PROPERTY_COUNTRY_VALUE"])
        ));
        while($arData = $rsData->Fetch()){
            $country["NAME"] = $arData["UF_NAME"];
            $country["IMG"] = CFile::GetPath($arData["UF_FILE"]);
        }
        $arSection["COUNTRY"] = $country;
        /* echo "<pre>";
         print_r($arSection["COUNTRY"]);
         echo "</pre>";*/
    }
    
    $arResult['SECTIONS'][$key] = $arSection;
}

$arResult["PARENTS_ID"] = array_unique($arResult["PARENTS_ID"]);

$arParent = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arResult["PARENTS_ID"]), false, array("ID", "NAME", "SECTION_PAGE_URL"));
while($parent = $arParent->GetNext())
{
    $arResult["PARENTS"][$parent["ID"]] =  $parent;
}
<?php

$MESS['CP_TPL_DETAIL_PAGE_TITLE'] = "В коллекцию";
$MESS["NEW"] = "NEW";
$MESS["HIT"] = "HIT";
$MESS["SALE"] = "SALE";
$MESS["SURFACE"] = "Поверхность: ";
$MESS["SIZES"] = "Размеры: ";
$MESS["IN_SALON"] = "В салоне";
$MESS["MIN_PRICE"] = "Цена от: #PRICE# руб. за м²";
$MESS["COLLECTION_H3"] = "Коллекции";
$MESS["SIZES_LINE"] = "Размеры: #SIZES_LINE#";
$MESS["SURFACE_LINE"] = "Поверхность: #SURFACE_LINE#";
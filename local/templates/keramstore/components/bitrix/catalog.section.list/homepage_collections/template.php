<?
use Nextype\Alpha\Layout;
use Bitrix\Main\Localization\Loc;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

$containerId = "homepage_collections".$arParams["TYPE_HOME"]. $this->randString();
$sliderId = "obSlider" .$arParams["TYPE_HOME"]. $this->randString();

if (!empty($arResult['SECTIONS']))
{
    ?>
    <div class="wrapper-section">
        <div class="categories-with-photo type3">
            <div class="container">
                <div class="section-name">
                    <? if (!empty($arParams['SECTION_TITLE'])): ?>
                        <h2 class="h2 title"><?= $arParams['SECTION_TITLE'] ?></h2>
                    <? endif; ?>

                    <!--                        --><?// if (!empty($arResult['SECTION_LINK_TITLE'])): ?>
                    <!--                            <a href="--><?//= $arResult['SECTION_LINK_URL'] ?><!--" class="subtitle font-large-button">--><?//= $arResult['SECTION_LINK_TITLE'] ?><!-- <i class="icon icon-arrow-right-text-button"></i></a>-->
                    <!--                        --><?// endif; ?>
                </div>
            </div>

            <div class="swiper-slider">
                <?if($arParams['SHOW_ALL'] != "Y"){?>
                    <div class="swiper-container" id="<?=$containerId?>">
                        <div class="swiper-wrapper hmcollect" id="hmcollect">
                            <?
                            foreach ($arResult['SECTIONS'] as $key => $arSection)
                            {
                                #\Bitrix\Main\Diag\Debug::dump($arSection);
                                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                                if($key < 3) {
                                    ?>
                                    <div class="swiper-slide" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                                        <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="category">
                                            <div class="image-wrap">
                                                <div class="bg-img swiper-lazy" style="background-image:url(<?=$arSection['DETAIL_PICTURE']['SRC']?>)" data-background="<?=$arSection['DETAIL_PICTURE']['SRC']?>">
                                                    <?php /*=Layout\Assets::showSpinner(true)*/?>
                                                </div>
                                                <div class="collection-property">
                                                    <?if($arSection["UF_NEW"]){?>
                                                        <span class="collection-new"><?= Loc::getMessage('NEW') ?></span>
                                                    <?}?>
                                                    <?if($arSection["UF_HIT"]){?>
                                                        <span class="collection-hit"><?= Loc::getMessage('HIT') ?></span>
                                                    <?}?>
                                                    <?if($arSection["UF_SALE"]){?>
                                                        <span class="collection-sale"><?= Loc::getMessage('SALE') ?></span>
                                                    <?}?>
                                                </div>
                                                <?if($arSection["UF_IN_SALON"]){?>
                                                    <div class="collection-in-salon">
                                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/in_salon.svg" alt="Интернет-магазин керамической плитки Keramstore">
                                                        <span class="in-salon"><?= Loc::getMessage('IN_SALON') ?></span>
                                                    </div>
                                                <?}?>
                                            </div>
                                            <div class="overlay">
                                                <div class="title font-title">
                                                    <?=$arSection['NAME'];?>
                                                    <!--                                        <a href="--><?//=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["SECTION_PAGE_URL"]?><!--">--><?//=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["NAME"]?><!--</a>-->
                                                    <!--<span class="title_gold"><?/*=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["NAME"]*/?></span>-->
                                                </div>
                                                <? if(!empty($arSection['BRAND']['NAME'])) {?>
                                                    <div class="title font-title coursive">
                                                        <?= $arSection['BRAND']['NAME'] ?> (<?=$arSection['COUNTRY']["NAME"]?>)
                                                        <!--<a href="<?/*= $arSection['BRAND']['DETAIL_PAGE_URL'] */?>"> <?/*= $arSection['BRAND']['NAME'] */?></a>-->
                                                        <!-- <?/*=$arSection['COUNTRY']["NAME"]*/?> <img width="20px;" src="<?/*=$arSection['COUNTRY']["IMG"]*/?>">-->
                                                    </div>
                                                <? } ?>
                                                <?if($arSection["UF_MIN_PRICE"]){?>
                                                    <div class="collection_min-price">
                                                        <?echo Loc::getMessage(
                                                            'MIN_PRICE',
                                                            array(
                                                                '#PRICE#' => $arSection["UF_MIN_PRICE"],
                                                            )
                                                        );?>
                                                    </div>
                                                <?} else {?>
                                                    <div class="collection_min-price">
                                                        <?echo Loc::getMessage(
                                                            'MIN_PRICE_NULL',
                                                            array(
                                                                '#MESSAGE#' => "по запросу",
                                                            )
                                                        );?>
                                                    </div>
                                                <? } ?>
                                                <div class="hover-content">
                                                    <?/* if (!empty($arSection['DESCRIPTION'])): */?><!--
                                            <div class="desc font-body-small"><?php /*=$arSection['DESCRIPTION']*/?></div>
                                            --><?/* endif; */?>
                                                    <?if($arSection["UF_SIZES_LINE"]){
                                                        $sizes_line = str_replace(';', ', ', $arSection["UF_SIZES_LINE"]);
                                                        ?>
                                                        <div class="collection_min-price">
                                                            <?echo Loc::getMessage(
                                                                'SIZES_LINE',
                                                                array(
                                                                    '#SIZES_LINE#' => $sizes_line,
                                                                )
                                                            );?>
                                                        </div>
                                                    <?}?>
                                                    <?if($arSection["UF_SURFACE_LINE"]){
                                                        $surface_line = str_replace(';', ', ', $arSection["UF_SURFACE_LINE"]);
                                                        ?>
                                                        <div class="collection_min-price">
                                                            <?echo Loc::getMessage(
                                                                'SURFACE_LINE',
                                                                array(
                                                                    '#SURFACE_LINE#' => $surface_line,
                                                                )
                                                            );?>
                                                        </div>
                                                    <?}?>
                                                    <div class="link font-large-button">
                                                        <span><?=GetMessage('CP_TPL_DETAIL_PAGE_TITLE')?></span>
                                                        <i class="icon icon-arrow-right-text-button-white"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?
                                }
                            }
                            ?>
                            <div class="collazy">
                                <a class="btn btn-show-more" href="<?=$arParams['DETAIL_LINK']?>">Показать ещё</a>
                            </div>
                            <div class="hiddenblock">
                                <?
                                foreach ($arResult['SECTIONS'] as $key => $arSection)
                                {
                                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                                    if($key >2) {
                                        ?>
                                        <div class="swiper-slide" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                                            <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="category">
                                                <div class="image-wrap">
                                                    <div class="bg-img swiper-lazy" style="background-image:url(<?=$arSection['DETAIL_PICTURE']['SRC']?>)" data-background="<?=$arSection['DETAIL_PICTURE']['SRC']?>">
                                                        <?php /*=Layout\Assets::showSpinner(true)*/?>
                                                    </div>
                                                    <div class="collection-property">
                                                        <?if($arSection["UF_NEW"]){?>
                                                            <span class="collection-new"><?= Loc::getMessage('NEW') ?></span>
                                                        <?}?>
                                                        <?if($arSection["UF_HIT"]){?>
                                                            <span class="collection-hit"><?= Loc::getMessage('HIT') ?></span>
                                                        <?}?>
                                                        <?if($arSection["UF_SALE"]){?>
                                                            <span class="collection-sale"><?= Loc::getMessage('SALE') ?></span>
                                                        <?}?>
                                                    </div>
                                                    <?if($arSection["UF_IN_SALON"]){?>
                                                        <div class="collection-in-salon">
                                                            <img src="<?=SITE_TEMPLATE_PATH?>/images/in_salon.svg" alt="Интернет-магазин керамической плитки Keramstore">
                                                            <span class="in-salon"><?= Loc::getMessage('IN_SALON') ?></span>
                                                        </div>
                                                    <?}?>
                                                </div>
                                                <div class="overlay">
                                                    <div class="title font-title">
                                                        <?=$arSection['NAME'];?>
                                                        <!--                                        <a href="--><?//=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["SECTION_PAGE_URL"]?><!--">--><?//=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["NAME"]?><!--</a>-->
                                                        <!--<span class="title_gold"><?/*=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["NAME"]*/?></span>-->
                                                    </div>
                                                    <? if(!empty($arSection['BRAND']['NAME'])) {?>
                                                        <div class="title font-title coursive">
                                                            <?= $arSection['BRAND']['NAME'] ?> (<?=$arSection['COUNTRY']["NAME"]?>)
                                                            <!--<a href="<?/*= $arSection['BRAND']['DETAIL_PAGE_URL'] */?>"> <?/*= $arSection['BRAND']['NAME'] */?></a>-->
                                                            <!-- <?/*=$arSection['COUNTRY']["NAME"]*/?> <img width="20px;" src="<?/*=$arSection['COUNTRY']["IMG"]*/?>">-->
                                                        </div>
                                                    <? } ?>
                                                    <?if($arSection["UF_MIN_PRICE"]){?>
                                                        <div class="collection_min-price">
                                                            <?echo Loc::getMessage(
                                                                'MIN_PRICE',
                                                                array(
                                                                    '#PRICE#' => $arSection["UF_MIN_PRICE"],
                                                                )
                                                            );?>
                                                        </div>
                                                    <?} else {?>
                                                        <div class="collection_min-price">
                                                            <?echo Loc::getMessage(
                                                                'MIN_PRICE_NULL',
                                                                array(
                                                                    '#MESSAGE#' => "по запросу",
                                                                )
                                                            );?>
                                                        </div>
                                                    <? } ?>
                                                    <div class="hover-content">
                                                        <? if (!empty($arSection['DESCRIPTION'])): ?>
                                                            <div class="desc font-body-small"><?=$arSection['DESCRIPTION']?></div>
                                                        <? endif; ?>
                                                        <?if($arSection["UF_SIZES_LINE"]){
                                                            $sizes_line = str_replace(';', ', ', $arSection["UF_SIZES_LINE"]);
                                                            ?>
                                                            <div class="collection_min-price">
                                                                <?echo Loc::getMessage(
                                                                    'SIZES_LINE',
                                                                    array(
                                                                        '#SIZES_LINE#' => $sizes_line,
                                                                    )
                                                                );?>
                                                            </div>
                                                        <?}?>
                                                        <?if($arSection["UF_SURFACE_LINE"]){
                                                            $surface_line = str_replace(';', ', ', $arSection["UF_SURFACE_LINE"]);
                                                            ?>
                                                            <div class="collection_min-price">
                                                                <?echo Loc::getMessage(
                                                                    'SURFACE_LINE',
                                                                    array(
                                                                        '#SURFACE_LINE#' => $surface_line,
                                                                    )
                                                                );?>
                                                            </div>
                                                        <?}?>
                                                        <div class="link font-large-button">
                                                            <span><?=GetMessage('CP_TPL_DETAIL_PAGE_TITLE')?></span>
                                                            <i class="icon icon-arrow-right-text-button-white"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?
                                    }
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                <?} else {?>
                    <div class="swiper-container" id="<?=$containerId?>">
                        <div class="swiper-wrapper hmcollect">
                            <?
                            foreach ($arResult['SECTIONS'] as $key => $arSection)
                            {
                                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                                ?>
                                <div class="swiper-slide" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                                    <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="category">
                                        <div class="image-wrap">
                                            <div class="bg-img swiper-lazy" style="background-image:url(<?=$arSection['DETAIL_PICTURE']['SRC']?>)" data-background="<?=$arSection['DETAIL_PICTURE']['SRC']?>">
                                                <?php /*=Layout\Assets::showSpinner(true)*/?>
                                            </div>
                                            <div class="collection-property">
                                                <?if($arSection["UF_NEW"]){?>
                                                    <span class="collection-new"><?= Loc::getMessage('NEW') ?></span>
                                                <?}?>
                                                <?if($arSection["UF_HIT"]){?>
                                                    <span class="collection-hit"><?= Loc::getMessage('HIT') ?></span>
                                                <?}?>
                                                <?if($arSection["UF_SALE"]){?>
                                                    <span class="collection-sale"><?= Loc::getMessage('SALE') ?></span>
                                                <?}?>
                                            </div>
                                            <?if($arSection["UF_IN_SALON"]){?>
                                                <div class="collection-in-salon">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/in_salon.svg" alt="Интернет-магазин керамической плитки Keramstore">
                                                    <span class="in-salon"><?= Loc::getMessage('IN_SALON') ?></span>
                                                </div>
                                            <?}?>
                                        </div>
                                        <div class="overlay">
                                            <div class="title font-title">
                                                <?=$arSection['NAME'];?>
                                                <!--                                        <a href="--><?//=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["SECTION_PAGE_URL"]?><!--">--><?//=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["NAME"]?><!--</a>-->
                                                <!--<span class="title_gold"><?/*=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["NAME"]*/?></span>-->
                                            </div>
                                            <? if(!empty($arSection['BRAND']['NAME'])) {?>
                                                <div class="title font-title coursive">
                                                    <?= $arSection['BRAND']['NAME'] ?> (<?=$arSection['COUNTRY']["NAME"]?>)
                                                    <!--<a href="<?/*= $arSection['BRAND']['DETAIL_PAGE_URL'] */?>"> <?/*= $arSection['BRAND']['NAME'] */?></a>-->
                                                    <!-- <?/*=$arSection['COUNTRY']["NAME"]*/?> <img width="20px;" src="<?/*=$arSection['COUNTRY']["IMG"]*/?>">-->
                                                </div>
                                            <? } ?>
                                            <?if($arSection["UF_MIN_PRICE"]){?>
                                                <div class="collection_min-price">
                                                    <?echo Loc::getMessage(
                                                        'MIN_PRICE',
                                                        array(
                                                            '#PRICE#' => $arSection["UF_MIN_PRICE"],
                                                        )
                                                    );?>
                                                </div>
                                            <?} else {?>
                                                <div class="collection_min-price">
                                                    <?echo Loc::getMessage(
                                                        'MIN_PRICE_NULL',
                                                        array(
                                                            '#MESSAGE#' => "по запросу",
                                                        )
                                                    );?>
                                                </div>
                                            <? } ?>
                                            <div class="hover-content">
                                                <?/* if (!empty($arSection['DESCRIPTION'])): */?><!--
                                                        <div class="desc font-body-small"><?php /*=$arSection['DESCRIPTION']*/?></div>
                                                    --><?/* endif; */?>
                                                <?if($arSection["UF_SIZES_LINE"]){
                                                    $sizes_line = str_replace(';', ', ', $arSection["UF_SIZES_LINE"]);
                                                    ?>
                                                    <div class="collection_min-price">
                                                        <?echo Loc::getMessage(
                                                            'SIZES_LINE',
                                                            array(
                                                                '#SIZES_LINE#' => $sizes_line,
                                                            )
                                                        );?>
                                                    </div>
                                                <?}?>
                                                <?if($arSection["UF_SURFACE_LINE"]){
                                                    $surface_line = str_replace(';', ', ', $arSection["UF_SURFACE_LINE"]);
                                                    ?>
                                                    <div class="collection_min-price">
                                                        <?echo Loc::getMessage(
                                                            'SURFACE_LINE',
                                                            array(
                                                                '#SURFACE_LINE#' => $surface_line,
                                                            )
                                                        );?>
                                                    </div>
                                                <?}?>
                                                <div class="link font-large-button">
                                                    <span><?=GetMessage('CP_TPL_DETAIL_PAGE_TITLE')?></span>
                                                    <i class="icon icon-arrow-right-text-button-white"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                <?}?>
                <!--<div class="swiper-arrows">
                    <div class="swiper-button-next" data-container="<?php /*=$containerId*/?>"></div>
                    <div class="swiper-button-prev" data-container="<?php /*=$containerId*/?>"></div>
                </div>-->

            </div>
        </div>
    </div>

    <?
    #\Bitrix\Main\Diag\Debug::dump($arResult["SECTIONS"]);
}
?>
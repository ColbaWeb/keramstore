<?
use Nextype\Alpha\Layout;
use Bitrix\Main\Localization\Loc;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

$containerId = "other_collections".$arParams["TYPE_HOME"]. $this->randString();
$sliderId = "obSlider" .$arParams["TYPE_HOME"]. $this->randString();

if (!empty($arResult['SECTIONS']))
{
?>
    <div class="wrapper-section">
        <div class="categories-with-photo other-collections-slider">

            <?if($arParams['TYPE_HOME'] == 'FACTORY'):?>
                <div class="other_collections-title">
                    <div class="h4 title"><?= $arParams['SECTION_TITLE'] ?></div>
                </div>
            <?endif;?>


            <div class="swiper-slider">
                <div class="swiper-container" id="<?=$containerId?>">
                    <div class="swiper-wrapper">
                        <?
                        foreach ($arResult['SECTIONS'] as $arSection)
                        {
                            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                            ?>
                        <div class="swiper-slide" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                            <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="category">
                                <div class="image-wrap">
                                    <div class="bg-img swiper-lazy" data-background="<?=$arSection['DETAIL_PICTURE']['SRC']?>">
                                        <?=Layout\Assets::showSpinner(true)?>
                                    </div>
                                    <div class="collection-property">
                                        <?if($arSection["UF_NEW"]){?>
                                            <span class="collection-new"><?= Loc::getMessage('NEW') ?></span>
                                        <?}?>
                                        <?if($arSection["UF_HIT"]){?>
                                            <span class="collection-hit"><?= Loc::getMessage('HIT') ?></span>
                                        <?}?>
                                        <?if($arSection["UF_SALE"]){?>
                                            <span class="collection-sale"><?= Loc::getMessage('SALE') ?></span>
                                        <?}?>
                                    </div>
                                    <?if($arSection["UF_IN_SALON"]){?>
                                        <div class="collection-in-salon">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/images/in_salon.svg" alt="">
                                            <span class="in-salon"><?= Loc::getMessage('IN_SALON') ?></span>
                                        </div>
                                    <?}?>
                                </div>
                                <div class="overlay">
                                    <div class="title font-title">
                                        <?=$arSection['NAME'];?>
                                        <!--                                        <a href="--><?//=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["SECTION_PAGE_URL"]?><!--">--><?//=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["NAME"]?><!--</a>-->
                                        <!--<span class="title_gold"><?/*=$arResult["PARENTS"][$arSection['IBLOCK_SECTION_ID']]["NAME"]*/?></span>-->
                                    </div>
                                    <? if(!empty($arSection['BRAND']['NAME'])) {?>
                                        <div class="title font-title coursive">
                                            <?= $arSection['BRAND']['NAME'] ?> (<?=$arSection['COUNTRY']["NAME"]?>)
                                            <!--<a href="<?/*= $arSection['BRAND']['DETAIL_PAGE_URL'] */?>"> <?/*= $arSection['BRAND']['NAME'] */?></a>-->
                                            <!-- <?/*=$arSection['COUNTRY']["NAME"]*/?> <img width="20px;" src="<?/*=$arSection['COUNTRY']["IMG"]*/?>">-->
                                        </div>
                                    <? } ?>
                                    <div class="hover-content">
                                        <? if (!empty($arSection['DESCRIPTION'])): ?>
                                            <div class="desc font-body-small"><?=$arSection['DESCRIPTION']?></div>
                                        <? endif; ?>
                                        <?if($arSection["UF_MIN_PRICE"]){?>
                                            <div class="collection_min-price">
                                                <?echo Loc::getMessage(
                                                    'MIN_PRICE',
                                                    array(
                                                        '#PRICE#' => $arSection["UF_MIN_PRICE"],
                                                    )
                                                );?>
                                            </div>
                                        <?}?>
                                        <div class="link font-large-button">
                                            <span><?=GetMessage('CP_TPL_DETAIL_PAGE_TITLE')?></span>
                                            <i class="icon icon-arrow-right-text-button-white"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?
                        }
                        ?>
                        
                        
                        
                    </div>
                </div>
                <div class="swiper-arrows">
                    <div class="swiper-button-next" data-container="<?=$containerId?>"></div>
                    <div class="swiper-button-prev" data-container="<?=$containerId?>"></div>
                </div>

            </div>


            

        </div>
    </div>

    <script>
        var <?=$sliderId?> = new Swiper('#<?=$containerId?>', {
            slidesPerView: "auto",
            spaceBetween: 8,
            freeMode: true,
            lazy: true,
            watchSlidesVisibility: 'auto',
            breakpoints: {
                992: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                    slidesPerGroup: 4,
                    freeMode: false,
                    navigation: {
                        nextEl: '.swiper-button-next[data-container="<?=$containerId?>"]',
                        prevEl: '.swiper-button-prev[data-container="<?=$containerId?>"]',
                    },
                }
            }
        });


        $(window).resize(function() {
            if ($(window).innerWidth() <= 1024)
            {
                <?=$sliderId?>.init();
            }
        });
    </script>

<?
}
?>
<?php

$MESS['CP_TPL_DETAIL_PAGE_TITLE'] = "В коллекцию";
$MESS["NEW"] = "NEW";
$MESS["HIT"] = "HIT";
$MESS["SALE"] = "SALE";
$MESS["SURFACE"] = "Поверхность: ";
$MESS["SIZES"] = "Размеры: ";
$MESS["IN_SALON"] = "В салоне";
$MESS["COLLECTION_H3"] = "Коллекции";
$MESS["MIN_PRICE"] = "Цена от: #PRICE# руб.";
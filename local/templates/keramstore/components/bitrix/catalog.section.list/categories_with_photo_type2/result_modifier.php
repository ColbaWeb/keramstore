<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

if ($arParams['SECTION_LINK_TYPE'] == 'C' && !empty($arParams['SECTION_LINK_TITLE']) && !empty($arParams['SECTION_LINK_URL']))
{
    $arResult['SECTION_LINK_TITLE'] = $arParams['SECTION_LINK_TITLE'];
    $arResult['SECTION_LINK_URL'] = $arParams['SECTION_LINK_URL'];
}
elseif ($arParams['SECTION_LINK_TYPE'] == 'S' && !empty($arParams['SECTION_LINK_SECTION_ID']))
{
    if ($arSection = CIBlockSection::GetByID($arParams['SECTION_LINK_SECTION_ID'])->GetNext())
    {
        $arResult['SECTION_LINK_TITLE'] = $arSection['NAME'];
        $arResult['SECTION_LINK_URL'] = $arSection['SECTION_PAGE_URL'];
    }
    
}

if (is_array($arParams['SHOW_SECTIONS_ID']) && !empty($arParams['SHOW_SECTIONS_ID']) && !empty($arResult['SECTIONS']))
{
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        if (!in_array($arSection['ID'], $arParams['SHOW_SECTIONS_ID']))
        {
            unset($arResult['SECTIONS'][$key]);
            continue;
        }
    }
}

if (intval($arParams['SHOW_SECTIONS_COUNT']) > 0)
{
    $counter = 0;
    
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        if (intval($arParams['SHOW_SECTIONS_COUNT']) <= $counter)
        {
            unset($arResult['SECTIONS'][$key]);
        }
        
        $counter++;
    }
}

foreach ($arResult['SECTIONS'] as $key => $arSection)
{
    if (!empty($arSection['DETAIL_PICTURE']))
    {
        if ($arImage = CFile::ResizeImageGet($arSection['DETAIL_PICTURE'], array('width' => 700, 'height' => 700), BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arSection['DETAIL_PICTURE'] = array (
                'SRC' => $arImage['src']
            );
        }
    }
    
    $arResult['SECTIONS'][$key] = $arSection;
}
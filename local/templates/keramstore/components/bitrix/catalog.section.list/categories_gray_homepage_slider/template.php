<?
use Nextype\Alpha\Layout,
    Bitrix\Main\Localization\Loc;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

$bShowProductsCount = ($arParams['SHOW_PRODUCTS_COUNT'] === 'Y');
if ($bShowProductsCount)
{
    $itemDeclension = new \Bitrix\Main\Grid\Declension(
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_1'),
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_2'),
            Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_3'));
}

if (!empty($arResult['SECTIONS'])):?>
    <div class="catalog-with-gray-cards">
        <div class="categories-block type-4">
            <div class="categories-list row">
                <? $sectionNum = 0; 
                foreach ($arResult['SECTIONS'] as $arSection): ?>
                <?
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                    $sectionNum++;
                ?>
                    <?if ($sectionNum == 1):?>
                        <div class="col-12 col-md-6" id="<?=$this->GetEditAreaId($arSection['ID'])?>">
                            <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="item big">
                                <?=Layout\Assets::showBackgroundImage($arSection['PICTURE']['SRC'], array('attributes' => array('class="image"')), true)?>
                                <div class="title font-title"><?=$arSection['NAME']?></div>
                                
                                <?if ($bShowProductsCount && !empty($arSection['ELEMENT_CNT'])):?>
                                    <div class="subtitle font-body-small  count">
                                        <?=$arSection['ELEMENT_CNT'] . ' ' . $itemDeclension->get($arSection['ELEMENT_CNT']);?>
                                    </div>
                                <?endif?>
                            </a>
                        </div>
                    <?else:?>
                        <?if ($sectionNum == 2):?>
                            <div class="category-wrap col-12 col-md-6">
                                <div class="row">
                        <?endif?>
                                    <div class="col-6" id="<?=$this->GetEditAreaId($arSection['ID'])?>">
                                        <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="item">
                                            <?=Layout\Assets::showBackgroundImage($arSection['PICTURE']['SRC'], array('attributes' => array('class="image"')), true)?>
                                            <div class="title font-title"><?=$arSection['NAME']?></div>
                                            
                                            <?if ($bShowProductsCount && !empty($arSection['ELEMENT_CNT'])):?>
                                                <div class="subtitle font-body-small  count">
                                                    <?=$arSection['ELEMENT_CNT'] . ' ' . $itemDeclension->get($arSection['ELEMENT_CNT']);?>
                                                </div>
                                            <?endif?>
                                        </a>
                                    </div>
                        <?if ($sectionNum == 5):?>
                                </div>
                            </div>
                        <?endif?>
                    <?endif?>
                
                    <?if ($sectionNum == 5) break; ?>
                
                <?endforeach?>
            </div>
        </div>
    </div>
<? endif; ?>
<?
use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

Layout\Assets::getInstance()->addLess(array (
    SITE_TEMPLATE_PATH . '/less/categories-block.less',
));
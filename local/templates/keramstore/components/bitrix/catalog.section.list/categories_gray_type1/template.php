<?
use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (!empty($arResult['SECTIONS']))
{
?>
<div class="wrapper-section">
    <div class="container">
        <div class="categories-block type-1">
            <div class="section-name">
                <? if (!empty($arParams['SECTION_TITLE'])): ?>
                <div class="h2 title"><?=$arParams['SECTION_TITLE']?></div>
                <? endif; ?>
                
                <? if (!empty($arResult['SECTION_LINK_TITLE'])): ?>
                <a href="<?=$arResult['SECTION_LINK_URL']?>" class="subtitle font-large-button"><?=$arResult['SECTION_LINK_TITLE']?> <i class="icon icon-arrow-right-text-button"></i></a>
                <? endif; ?>
            </div>
            
            <div class="categories-list row">
                <?
                foreach ($arResult['SECTIONS'] as $arSection) 
                {
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                    ?>
                    <div class="col-12 col-sm-6" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                        <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="item">
                            <?
                            echo Layout\Assets::showBackgroundImage($arSection['PICTURE']['SRC'], array (
                                "attributes" => array (
                                    "class=\"image\""
                                )
                            ), true);
                            ?>
                            <div class="title font-title"><?=$arSection['NAME'];?></div>
                        </a>
                    </div>
                <?
                }
                ?>
            </div>  
        </div>
    </div>
</div>
<?
}
?>
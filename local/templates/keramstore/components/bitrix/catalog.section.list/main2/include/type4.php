<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Bitrix\Main\Grid\Declension;

?>
<div class="categories-block type-4">
    <div class="categories-list row">
        <?
            $arSection = $arResult['SECTIONS'][0];
            $component->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $component->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
        ?>
        <div class="col-12 col-md-6" id="<?=$component->GetEditAreaId($arSection['ID']); ?>">
            <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="item big">
                <?
                    echo Layout\Assets::showBackgroundImage($arSection['PICTURE']['SRC'], array (
                        "attributes" => array (
                            "class=\"image\""
                            )
                        ));
                    ?>
                
                <div class="title font-title"><?=$arSection['NAME']?></div>
                <?
                    if ($arParams["COUNT_ELEMENTS"] && $arSection['ELEMENT_CNT'] !== null)
                    {
                        $itemDeclension = new Declension(Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_1'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_2'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_3'));
                        
                        ?><div class="count font-body-small"><?=$arSection['ELEMENT_CNT'] . " " . $itemDeclension->get($arSection['ELEMENT_CNT'])?></div><?
                    }
                    ?>
            </a>
        </div>
        
        <div class="col-12 col-md-6">
            <div class="row">
                <?
                for ($i = 1; $i < 5; $i++)
                {
                    if (!isset($arResult['SECTIONS'][$i]) || empty($arResult['SECTIONS'][$i]))
                        continue;
                    
                    $arSection = $arResult['SECTIONS'][$i];
                    $component->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $component->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                    
                    ?>
                    <div class="col-6" id="<?=$component->GetEditAreaId($arSection['ID']); ?>">
                        <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="item">
                            <?
                            echo Layout\Assets::showBackgroundImage($arSection['PICTURE']['SRC'], array (
                                "attributes" => array (
                                    "class=\"image\""
                                    )
                                ));
                            ?>
                            
                            <div class="title font-title"><?=$arSection['NAME']?></div>
                            <?
                                if ($arParams["COUNT_ELEMENTS"] && $arSection['ELEMENT_CNT'] !== null)
                                {
                                    $itemDeclension = new Declension(Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_1'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_2'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_3'));

                                    ?><div class="count font-body-small"><?=$arSection['ELEMENT_CNT'] . " " . $itemDeclension->get($arSection['ELEMENT_CNT'])?></div><?
                                }
                                ?>
                        </a>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
    </div>
    
    <?
    if (count($arResult['SECTIONS']) > 5)
    {
        ?>
        <div class="row categories-list-2">
            <?
            for ($i = 5; $i < count($arResult['SECTIONS']); $i++)
            {
                if (!isset($arResult['SECTIONS'][$i]) || empty($arResult['SECTIONS'][$i]))
                    continue;

                $arSection = $arResult['SECTIONS'][$i];
                $component->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $component->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                
                ?>
                <div class="col-6 col-md-3" id="<?= $component->GetEditAreaId($arSection['ID']); ?>">
                    <a href="<?= $arSection['SECTION_PAGE_URL'] ?>" class="item">
                        <?
                        echo Layout\Assets::showBackgroundImage($arSection['PICTURE']['SRC'], array(
                            "attributes" => array(
                                "class=\"image\""
                            )
                        ));
                        ?>
                        
                        <div class="title font-title"><?= $arSection['NAME'] ?></div>
                        <?
                        if ($arParams["COUNT_ELEMENTS"] && $arSection['ELEMENT_CNT'] !== null)
                        {
                            $itemDeclension = new Declension(Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_1'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_2'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_3'));
                            ?><div class="count font-body-small"><?= $arSection['ELEMENT_CNT'] . " " . $itemDeclension->get($arSection['ELEMENT_CNT']) ?></div><?
                        }
                        ?>
                    </a>
                </div>
                <?
            }
            ?>
        </div>
        <?
    }
    ?>
    
    
</div>
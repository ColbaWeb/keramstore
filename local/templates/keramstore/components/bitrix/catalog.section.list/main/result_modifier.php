<?
use Nextype\Alpha\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$arResizeParams = array ('width' => 450, 'height' => 450);
$arBigResizeParams = array ('width' => 850, 'height' => 850);

if (!empty($arResult['SECTIONS']))
{
    $obTree = new Tools\Tree($arResult['SECTIONS']);
    $arResult['SECTIONS'] = $obTree->getTree();
    
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        $arSection['PICTURE'] = array ('SRC' => '');
        if (!empty($arSection['DETAIL_PICTURE']))
        {
            $pictureId = is_array($arSection['DETAIL_PICTURE']) ? $arSection['DETAIL_PICTURE']['ID'] : $arSection['DETAIL_PICTURE'];
            $arResize = CFile::ResizeImageGet($pictureId, 
                    (count($arResult['SECTIONS']) >= 5 && $key == 0) ? $arBigResizeParams : $arResizeParams,
                    BX_RESIZE_IMAGE_EXACT
            );
            
            if ($arResize)
            {
                $arSection['PICTURE']['SRC'] = $arResize['src'];
            }
        }
        
        
        
        $arResult['SECTIONS'][$key] = $arSection;
    }
}
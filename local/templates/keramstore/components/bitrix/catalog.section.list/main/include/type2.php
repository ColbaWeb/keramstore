<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Bitrix\Main\Grid\Declension;

if (count($arResult['SECTIONS']) > 0)
{
    ?>
<div class="categories-with-photo type2">
    <div class="categories-list row">
    <?
    for ($i = 0; $i < count($arResult['SECTIONS']); $i++)
    {
        if (!isset($arResult['SECTIONS'][$i]) || empty($arResult['SECTIONS'][$i]))
            continue;

        $arSection = $arResult['SECTIONS'][$i];
        $component->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
        $component->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
        ?>
        <div class="category-wrap col-6 col-md-4" id="<?= $component->GetEditAreaId($arSection['ID']); ?>">
            <a href="<?= $arSection['SECTION_PAGE_URL'] ?>" class="category">
                <div class="image-wrap">
                    <?
                    echo Layout\Assets::showBackgroundImage($arSection['PICTURE']['SRC'], array(
                        "attributes" => array(
                            "class=\"bg-img\""
                        )
                    ));
                    ?>
                </div>
                <div class="overlay">
                    <?
                    if ($arParams["COUNT_ELEMENTS"] && $arSection['ELEMENT_CNT'] !== null)
                    {
                        $itemDeclension = new Declension(Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_1'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_2'), Loc::getMessage('CT_BCSL_ELEMENTS_COUNT_3'));
                        ?><div class="count font-body-small"><?= $arSection['ELEMENT_CNT'] . " " . $itemDeclension->get($arSection['ELEMENT_CNT']) ?></div><?
                    }
                    ?>
                        <div class="title font-title"><?= $arSection['NAME'] ?></div>
                        <div class="hover-content">
                        <?
                        if (!empty($arSection['DESCRIPTION']))
                        {
                            ?><div class="desc font-body-small"><?= $arSection['DESCRIPTION'] ?></div><?
                        }
                        ?>
                            <div class="link font-large-button">
                                <span><?= Loc::getMessage('CT_BCSL_DETAIL_SECTION_URL') ?></span>
                                <i class="icon icon-arrow-right-text-button-white"></i>
                            </div>
                        </div>
                </div>
            </a>
        </div>
        <?
    }
    ?>
    </div>
</div>
    <?
}
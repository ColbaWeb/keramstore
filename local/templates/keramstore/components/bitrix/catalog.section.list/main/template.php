<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);



$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (count($arResult['SECTIONS']) > 0)
{
    $layoutTemplate = "type4.php";

    if (count($arResult['SECTIONS']) == 4)
        $layoutTemplate = "type3.php";
    elseif (count($arResult['SECTIONS']) == 3)
        $layoutTemplate = "type2.php";
    elseif (count($arResult['SECTIONS']) < 3)
        $layoutTemplate = "type1.php";
    
    ?>
    <div class="catalog-with-photo-wrapper">
    <?
    Layout\Partial::getInstance(__DIR__)->render($layoutTemplate, array (
        'strSectionEdit' => $strSectionEdit,
        'strSectionDelete' => $strSectionDelete,
        'arSectionDeleteParams' => $arSectionDeleteParams,
        'arResult' => $arResult,
        'arParams' => $arParams,
        'component' => $this
    ), false, false);
    ?>
    </div>
    <?
}

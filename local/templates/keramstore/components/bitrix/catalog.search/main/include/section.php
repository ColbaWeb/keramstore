<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;


$APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        "main",
        $componentParams,
        $arResult["THEME_COMPONENT"],
        array('HIDE_ICONS' => 'Y')
);

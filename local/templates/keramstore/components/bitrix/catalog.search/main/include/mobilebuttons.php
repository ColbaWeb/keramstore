<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

$APPLICATION->ShowViewContent('catalog_section_nav_record_count_mobile_print');

?>

<div class="mobile-buttons">
    <a href="javascript:void(0)" class="btn btn-show-catalog ">
        <span><?=Loc::getMessage('CATALOG_MOBILE_SECTIONS_BUTTON_TITLE')?></span>
        <i class="icon icon-catalog"></i>
    </a>
    
    <?
    if (is_array($arParams['LIST_SORTS']) && !empty($arParams['LIST_SORTS']))
    {
        ?>
        <a href="javascript:void(0)" class="btn btn-show-sorting">
            <i class="icon icon-sorting"></i>
        </a>
        <?
    }
    
    if ($arParams['USE_FILTER'] == "Y")
    {
        ?>
        <a href="javascript:void(0)" class="btn btn-show-filter">
            <i class="icon icon-filter"></i>
        </a>
        <?
    }
    ?>

</div>
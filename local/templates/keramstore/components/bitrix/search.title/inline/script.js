function JCAlphaTitleSearch(arParams)
{
    var _this = this;

    this.arParams = {
        'AJAX_PAGE': arParams.AJAX_PAGE,
        'CONTAINER_ID': arParams.CONTAINER_ID,
        'PHRASE_CONTAINER_ID': arParams.PHRASE_CONTAINER_ID,
        'INPUT_ID': arParams.INPUT_ID,
        'RESULT_CONTAINER_ID': arParams.RESULT_CONTAINER_ID,
        'MIN_QUERY_LEN': parseInt(arParams.MIN_QUERY_LEN),
        'SITE_ID': arParams.SITE_ID,
        'SIGNED_PARAMS_STRING': arParams.SIGNED_PARAMS_STRING,
    };
    if (arParams.WAIT_IMAGE)
        this.arParams.WAIT_IMAGE = arParams.WAIT_IMAGE;
    if (arParams.MIN_QUERY_LEN <= 0)
        arParams.MIN_QUERY_LEN = 1;

    this.cache = [];
    this.cache_key = null;

    this.startText = '';
    this.running = false;
    this.runningCall = false;
    this.currentRow = -1;
    this.RESULT = null;
    this.CONTAINER = null;
    this.PHRASE_CONTAINER = null;
    this.INPUT = null;
    this.WAIT = null;
    this.SITE_ID = null;
    this.SIGNED_PARAMS_STRING = null;

    this.ShowResult = function (result)
    {

        BX.removeClass(_this.PHRASE_CONTAINER, 'visible');
        
        if (BX.type.isString(result))
        {
            _this.RESULT.innerHTML = result;
            BX.addClass(_this.RESULT, 'visible');
        }
        else
        {
            BX.removeClass(_this.RESULT, 'visible');
        }
    };

    this.onKeyPress = function (keyCode)
    {

        switch (keyCode)
        {
            case 27: // escape key - close search div
                _this.RESULT.innerHTML = "";
                _this.currentRow = -1;
                return true;

        }

        return false;
    };

    this.onTimeout = function ()
    {
        _this.onChange(function () {
            setTimeout(_this.onTimeout, 500);
        });
    };

    this.onChange = function (callback)
    {
        if (_this.running)
        {
            _this.runningCall = true;
            return;
        }
        _this.running = true;

        if (_this.INPUT.value != _this.oldValue && _this.INPUT.value != _this.startText)
        {
            _this.oldValue = _this.INPUT.value;
            if (_this.INPUT.value.length >= _this.arParams.MIN_QUERY_LEN)
            {
                _this.cache_key = _this.arParams.INPUT_ID + '|' + _this.INPUT.value;
                if (_this.cache[_this.cache_key] == null)
                {

                    BX.ajax.post(
                            _this.arParams.AJAX_PAGE,
                            {
                                'ajax_call': 'y',
                                'INPUT_ID': _this.arParams.INPUT_ID,
                                'q': _this.INPUT.value,
                                'l': _this.arParams.MIN_QUERY_LEN,
                                'signedParamsString': _this.arParams.SIGNED_PARAMS_STRING,
                                'siteId': _this.arParams.SITE_ID

                            },
                            function (result)
                            {
                                _this.cache[_this.cache_key] = result;
                                _this.ShowResult(result);
                                _this.currentRow = -1;

                                if (!!callback)
                                    callback();
                                _this.running = false;
                                if (_this.runningCall)
                                {
                                    _this.runningCall = false;
                                    _this.onChange();
                                }
                            }
                    );
                    return;
                } else
                {
                    _this.ShowResult(_this.cache[_this.cache_key]);
                    _this.currentRow = -1;
                }
            } else
            {
                _this.currentRow = -1;
                _this.ShowResult('');
            }
        }
        /*else
        {
            _this.ShowResult();
        }*/
        
        if (!!callback)
            callback();
        _this.running = false;
    };


    this.onKeyDown = function (e)
    {
        if (!e)
            e = window.event;

        if (_this.onKeyPress(e.keyCode))
            return BX.PreventDefault(e);
    };




    this.Init = function ()
    {
        this.CONTAINER = document.getElementById(this.arParams.CONTAINER_ID);
        this.PHRASE_CONTAINER = document.getElementById(this.arParams.PHRASE_CONTAINER_ID);

        this.RESULT = document.getElementById(this.arParams.RESULT_CONTAINER_ID);
        this.INPUT = document.getElementById(this.arParams.INPUT_ID);

        this.startText = this.oldValue = this.INPUT.value;
        this.INPUT.onkeydown = this.onKeyDown;
        
        this.SIGNED_PARAMS_STRING = this.arParams.SIGNED_PARAMS_STRING;
        this.SITE_ID = this.arParams.SITE_ID;
        
       
        if (!!this.PHRASE_CONTAINER)
        {
            
            BX.bind(this.INPUT, 'focus', function () {
                BX.addClass(_this.PHRASE_CONTAINER, 'visible');
            });
            
            
            BX.bind(this.INPUT, 'keyup', function () {
                BX.removeClass(_this.PHRASE_CONTAINER, 'visible');
            });
            
            var phraseLinks = this.PHRASE_CONTAINER.querySelectorAll('[data-entity="phrase"]');
            
            if (!!phraseLinks && phraseLinks.length > 0)
            {
                for (var i = 0; i < phraseLinks.length; i++)
                {
                    BX.bind(phraseLinks[i], 'click', function (event) {
                        if (!!this.getAttribute('data-value') && this.getAttribute('data-value') != "")
                        {
                            _this.INPUT.value = this.getAttribute('data-value');
                            BX.removeClass(_this.PHRASE_CONTAINER, 'visible');
                            _this.onChange();
                        }
                    });
                }
            }
        }

        BX.bind(this.INPUT, 'bxchange', function () {
            _this.onChange()
        });
        
        $(document).click(function(event) {
            if ($(event.target).closest(this.CONTAINER).length) return;
            
            BX.removeClass(_this.RESULT, 'visible');
            
            event.stopPropagation();
        });
        
        
    };

    BX.ready(function () {
        _this.Init(arParams)
    });
}

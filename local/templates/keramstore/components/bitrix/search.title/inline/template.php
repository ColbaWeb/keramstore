<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";

$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

if($arParams["SHOW_INPUT"] !== "N"):?>

    <div class="search-header">
        <div class="search-line">

            <form action="<?echo $arResult["FORM_ACTION"]?>" autocomplete="off" class="search-form" id="<?=$CONTAINER_ID?>">
                <div class="input"> 
                    <i class="icon icon-search-2-line"></i>
                    <input type="search" id="<?echo $INPUT_ID?>" autocomplete="off" name="q" placeholder="<?=GetMessage("CT_BST_INPUT_PLACEHOLDER");?>">
                </div>
            </form>
            
            <div class="search-list-found font-body" id="<?echo $CONTAINER_ID?>-results"></div>

        </div>

    </div>

<?
endif;

$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'bitrix.search.title');

?>
<script>
    BX.loadScript('<?=$templateFolder?>/script.js', function () {
        new JCAlphaTitleSearch({
            'AJAX_PAGE' : '<?=$templateFolder?>/component.php',
            'CONTAINER_ID': '<?=$CONTAINER_ID?>',
            'RESULT_CONTAINER_ID': '<?=$CONTAINER_ID?>-results',
            'INPUT_ID': '<?=$INPUT_ID?>',
            'MIN_QUERY_LEN': 2,
            'SITE_ID': '<?=SITE_ID?>',
            'SIGNED_PARAMS_STRING': '<?= CUtil::JSEscape($signedParams) ?>'
	    });
    });
    $(document).ready(function(){
        $('#title-inline-search-input').on('focus active',function() {
            $(this).parents('#title-inline-search').addClass('active');
        });
        $('#title-inline-search-input').on('blur',function() {
            $(this).parents('#title-inline-search').removeClass('active');
        });
    })

	
</script>

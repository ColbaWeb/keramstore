<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";

$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

$PHRASE_CONTAINER_ID = $CONTAINER_ID . "-phrase";

$arPopularPhrases = array ();
$arPopularPhraseFilter = array (
    "PAGES" => 1,
    "SITE_ID" => SITE_ID,
    ">RESULT_COUNT" => 2
);

$obCache = new CPHPCache();
$cacheId = md5(serialize(array ($arParams['CATEGORY_0'], $arParams['PRICE_CODE'], $arPopularPhraseFilter)));
if ($obCache->InitCache(36000, $cacheId, "/iblock/search.title"))
{
    $arPopularPhrases = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
    $rsPopularPhrases = CSearchStatistic::GetList(array('TIMESTAMP_X' => 'DESC'), $arPopularPhraseFilter);
    while ($arPopularPhrase = $rsPopularPhrases->fetch())
    {
        $arPopularPhrases[] = $arPopularPhrase;
    }
    
    $obCache->EndDataCache($arPopularPhrases);
}

if($arParams["SHOW_INPUT"] !== "N"):?>

    <div class="search-modal">
        <div class="container">
            <form action="<?echo $arResult["FORM_ACTION"]?>" autocomplete="off" class="search-wrap" id="<?=$CONTAINER_ID?>">
                <input type="text" class="font-body" id="<?echo $INPUT_ID?>" autocomplete="off" name="q" placeholder="<?=GetMessage("CT_BST_INPUT_PLACEHOLDER");?>">
                <a href="javascript:void(0)" class="clear"><i class="icon icon-close-big"></i></a>
                <button class="search-button"></button>
            </form>
            
            <?
            if (!empty($arPopularPhrases))
            {
                ?>
                <div class="search-list-suggestions font-body" id="<?=$PHRASE_CONTAINER_ID?>">
                    <div class="list">
                        <?
                        for ($i = 0; $i < 4; $i++)
                        {
                            if (!isset($arPopularPhrases[$i]))
                                continue;

                            $item = $arPopularPhrases[$i];
                            ?>
                            <a href="javascript:void(0)" data-entity="phrase" data-value="<?=$item['PHRASE']?>" class="item"><?=$item['PHRASE']?></a>
                            <?
                        }
                        ?>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
            <div class="search-list-found font-body" id="<?echo $CONTAINER_ID?>-results"></div>

    </div>

<?
endif;

$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'bitrix.search.title');

?>
<script>
    BX.loadScript('<?=$templateFolder?>/script.js', function () {
        new JCAlphaTitleSearch({
            'AJAX_PAGE' : '<?=$templateFolder?>/component.php',
            'CONTAINER_ID': '<?=$CONTAINER_ID?>',
            'PHRASE_CONTAINER_ID': '<?=$PHRASE_CONTAINER_ID?>',
            'RESULT_CONTAINER_ID': '<?=$CONTAINER_ID?>-results',
            'INPUT_ID': '<?=$INPUT_ID?>',
            'MIN_QUERY_LEN': 2,
            'SITE_ID': '<?=SITE_ID?>',
            'SIGNED_PARAMS_STRING': '<?= CUtil::JSEscape($signedParams) ?>'
	});
    });

    if ($('.search-modal').length > 0) {
        let searchModal = $('.search-modal');
        let searchInput = searchModal.find('input[type="text"]');
        let searchResult = searchModal.find('.search-list-found');

        searchInput.contextmenu(function () {
            searchResult.removeClass('visible');
        });

        searchInput.on('keyup', function () {
            if ($(this).val() !== '') {
                $(this).addClass('filled');
            }
            else $(this).removeClass('filled');
        });
    }
	
</script>

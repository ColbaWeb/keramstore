<?php
define('DISABLED_REGIONS', true);
define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

if (!isset($_REQUEST['siteId']) || !is_string($_REQUEST['siteId']))
    die();


if ($_SERVER['REQUEST_METHOD'] != 'POST' || preg_match('/^[A-Za-z0-9_]{2}$/', $_POST['siteId']) !== 1)
    die;

define('SITE_ID', $_REQUEST['siteId']);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);


$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
    $signedParamsString = $request->get('signedParamsString') ?: '';
    $params = $signer->unsign($signedParamsString, 'bitrix.search.title');
    $params = unserialize(base64_decode($params));
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
    die();
}

$params['AJAX_REQUEST'] = 'Y';

$APPLICATION->RestartBuffer();
header('Content-Type: text/html; charset='.LANG_CHARSET);

$APPLICATION->IncludeComponent('bitrix:search.title', "main", $params);
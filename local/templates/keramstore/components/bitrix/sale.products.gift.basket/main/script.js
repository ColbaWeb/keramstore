(function() {
	'use strict';

	if (!!window.JCSaleProductsGiftBasketComponent)
		return;

	window.JCSaleProductsGiftBasketComponent = function(params) {
		this.formPosting = false;
		this.lastItemsInnerHTML = null;
		this.siteId = params.siteId || '';
		this.template = params.template || '';
		this.componentPath = params.componentPath || '';
		this.parameters = params.parameters || '';

		this.container = document.querySelector('[data-entity="' + params.container + '"]');
		this.currentProductId = params.currentProductId;

		if (params.initiallyShowHeader)
		{
			BX.ready(BX.proxy(this.showHeader, this));
		}

		if (params.deferredLoad)
		{
			BX.ready(BX.proxy(this.deferredLoad, this));
		}

		BX.addCustomEvent('OnBasketChange', BX.proxy(this.reloadGifts, this));
		BX.addCustomEvent('OnCouponApply', BX.proxy(this.reloadGifts, this));
	};

	window.JCSaleProductsGiftBasketComponent.prototype =
	{
		reloadGifts: function()
		{
			this.sendRequest({action: 'deferredLoad', recalculateDiscounts: 'Y'});
		},

		deferredLoad: function()
		{
			this.sendRequest({action: 'deferredLoad'});
		},

		sendRequest: function(data)
		{
			var defaultData = {
				siteId: this.siteId,
				template: this.template,
				parameters: this.parameters
			};

			BX.ajax({
				url: this.componentPath + '/ajax.php' + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : ''),
				method: 'POST',
				dataType: 'json',
				timeout: 60,
				data: BX.merge(defaultData, data),
				onsuccess: BX.delegate(function(result){

					if (!result || !result.JS)
					{
						BX.cleanNode(this.container);
						this.hideHeader();
						return;
					}

					BX.ajax.processScripts(
						BX.processHTML(result.JS).SCRIPT,
						false,
						BX.delegate(function(){this.showAction(result, data);}, this)
					);
				}, this)
			});
		},

		showAction: function(result, data)
		{
			if (!data)
				return;

			switch (data.action)
			{
				case 'deferredLoad':
					this.processDeferredLoadAction(result);
					break;
			}
		},

		processDeferredLoadAction: function(result)
		{
			if (!result)
				return;

			this.processItems(result.items);
		},

		processItems: function(itemsHtml)
		{
			if (!itemsHtml)
				return;

			var processed = BX.processHTML(itemsHtml, false),
				temporaryNode = BX.create('DIV');
                        var items, k;

			temporaryNode.innerHTML = processed.HTML;
                        
			items = temporaryNode.querySelectorAll('[data-entity="items-row"]');

			if (items && (this.lastItemsInnerHTML !== items.innerHTML))
			{
				if (items.length)
				{
					BX.cleanNode(this.container);

					for (k in items)
					{
						if (items.hasOwnProperty(k))
						{
							this.container.appendChild(items[k]);
						}
					}
				}
				this.lastItemsInnerHTML = items.innerHTML;
				BX.ajax.processScripts(processed.SCRIPT);
			}

			BX.onCustomEvent('onAjaxSuccess');
		},

		showHeader: function(animate)
		{
			var parentNode = BX.findParent(this.container, {attr: {'data-entity': 'parent-container'}}),
				header;

			if (parentNode && BX.type.isDomNode(parentNode))
			{
				header = parentNode.querySelector('[data-entity="header"]');

				if (header)
				{
                                    BX.removeClass(header, 'd-none');
				}
			}
		},

		hideHeader: function()
		{
			var parentNode = BX.findParent(this.container, {attr: {'data-entity': 'parent-container'}}),
				header;

			if (parentNode && BX.type.isDomNode(parentNode))
			{
				header = parentNode.querySelector('[data-entity="header"]');

				if (header)
				{
                                    BX.addClass(header, 'd-none');
				}
			}
		}
	}
})();
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Nextype\Alpha\Layout;

$cartId = isset($arParams['CONTAINER_ID']) && !empty($arParams['CONTAINER_ID']) ? $arParams['CONTAINER_ID'] : md5(serialize($arParams));

?>

<div id="<?=$cartId?>" class="small-basket-wrapper">
    <? if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
            $APPLICATION->RestartBuffer();
    ?>

    <div class="small-basket-container">

        <a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket<?= $arResult['NUM_PRODUCTS'] > 0 ? ' no-empty' : ''?>">
            <i class="icon icon-basket"></i>
            <span><?= $arResult['NUM_PRODUCTS'] > 0 ? $arResult['TOTAL_PRICE'] : GetMessage('TSB1_EMPTY')?></span>
        </a>

        <? if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0): ?>
        <div class="small-basket loading">
            <div class="items<?=$arResult['NUM_PRODUCTS'] > 5 ? ' has-scroll' : ''?>">
                <div class="scrollbar-inner">
                    <? foreach ($arResult['CATEGORIES']['READY'] as $arItem):
                        if($arItem["BASE_PRICE"] == 0) {
                            $arItem["PRICE_FMT"] = "По запросу";
                            $arItem["FULL_PRICE"] = "";
                        }
                        ?>
                    <div class="item" data-item-id="<?=$arItem['ID']?>" data-item-qty-ratio="<?= $arItem['MEASURE_RATIO'] ?>">
                        <?if ($arParams["SHOW_IMAGE"] == "Y"):?>
                            <?if ($arItem["DETAIL_PAGE_URL"]):?>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="img-wrap">
                                <?=Layout\Assets::showImage($arItem["PICTURE_SRC"]) ?>
                            </a>
                            <? else: ?>
                            <div class="img-wrap">
                                <?=Layout\Assets::showImage($arItem["PICTURE_SRC"]) ?>
                            </div>
                            <? endif; ?>
                        <? endif; ?>
                        <div class="info">
                            <div class="name-block">
                                <?if ($arItem["DETAIL_PAGE_URL"]):?>
                                    <a class="name font-body-small" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                <?else:?>
                                    <span class="name font-body-small"><?=$arItem["NAME"]?></span>
                                <?endif?>

                                <a href="javascript:void(0)" data-entity="delete" class="icon icon-trash"></a>
                            </div>
                            <div class="price-block">
                                <div class="counter">
                                    <a href="javascript:void(0)" data-entity="quantity-minus" class="minus"><i class="icon icon-bx-minus"></i></a>
                                    <input type="number" data-entity="quantity-input" value="<?=$arItem["QUANTITY"]?>">
                                    <span class="measure"><?=$arItem['MEASURE_NAME']?></span>
                                    <a href="javascript:void(0)" data-entity="quantity-plus" class="plus"><i class="icon icon-bx-plus"></i></a>
                                </div>
                                <? if ($arParams["SHOW_PRICE"] == "Y"): ?>
                                <div class="price <? if($arItem["BASE_PRICE"] == 0) {?>zapros<?}?>">

                                    <? if ($arItem["FULL_PRICE"] != $arItem["PRICE_FMT"]): ?>
                                    <span class="old-price"><?= $arItem["FULL_PRICE"] ?></span>
                                    <? endif; ?>

                                    <?= $arItem["PRICE_FMT"] ?>

                                </div>
                                <? endif ?>

                            </div>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
            </div>
            <?if ($arParams["SHOW_PRICE"] == "Y"):?>
            <div class="total-info">
                <div class="total-name"><?=GetMessage('TSB1_SUM')?></div>
                <div class="total"><?=$arResult['TOTAL_PRICE']?></div>
            </div>
            <? endif; ?>

            <?if ($arParams["PATH_TO_ORDER"]):?>
            <div class="buttons">
                <a href="<?=$arParams["PATH_TO_ORDER"]?>" class="btn btn-primary btn-large"><?=GetMessage("TSB1_2ORDER")?></a>
            </div>
            <? endif; ?>


        </div>

        <? endif; ?>

       
    </div>
    
        <? if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
            die;
        ?>

        <script>
            BX.ready(function(){

                new window.Alpha.Basket({
                    siteId: '<?=SITE_ID?>',
                    containerId: '<?=$cartId?>',
                    ajaxPath: '<?=$templateFolder?>/ajax.php',
                    templateName: '<?=$templateName?>',
                    params: <?=CUtil::PhpToJSObject ($arParams)?>
                });
                
                $("body").on('mouseover', '#<?=$cartId?> .basket', function () {
                    $('#<?=$cartId?> .small-basket').removeClass('loading');
                });
                
                
            });

        </script>
</div>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$arResult = array_merge($arResult, Array (
    'NUM_PRODUCTS' => 0,
    'TOTAL_PRICE' => "",
    'TOTAL_PRICE_RAW' => 0,
));

if (isset($arResult['CATEGORIES']['READY']) && !empty($arResult['CATEGORIES']['READY']))
{
    $diffCurrency = false;
    $currency = $arResult['CATEGORIES']['READY'][0]['CURRENCY'];
    $arResult['NUM_PRODUCTS'] = count($arResult['CATEGORIES']['READY']);

    $productsIDs = array_column($arResult['CATEGORIES']['READY'], 'PRODUCT_ID');
    $rsCatalogRatios = CCatalogMeasureRatio::getList(
        array(),
        array('PRODUCT_ID' => $productsIDs),
        false,
        false,
        array('PRODUCT_ID', 'RATIO')
    );

    while ($arCatalogRatio = $rsCatalogRatios->Fetch())
    {
        $key = array_search($arCatalogRatio['PRODUCT_ID'], $productsIDs);
        $arResult['CATEGORIES']['READY'][$key]['MEASURE_RATIO'] = $arCatalogRatio['RATIO'];
    }

    foreach ($arResult['CATEGORIES']['READY'] as $key => $arItem)
    {
        $arResult['TOTAL_PRICE_RAW'] += floatval($arItem['SUM_VALUE']);

        if ($arItem['CURRENCY'] != $currency)
            $diffCurrency = true;
    }
    
    if (!$diffCurrency)
        $arResult['TOTAL_PRICE'] = CurrencyFormat($arResult['TOTAL_PRICE_RAW'], $currency);
    else
        $arResult['TOTAL_PRICE'] = number_format ($arResult['TOTAL_PRICE_RAW'], 0, '', ' ');
}
<?php
define('DISABLED_REGIONS', true);
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

use Nextype\Alpha\Tools;

if (!isset($_REQUEST['siteId']) || !is_string($_REQUEST['siteId']))
    die();

if (!isset($_REQUEST['templateName']) || !is_string($_REQUEST['templateName']))
    die();

if ($_SERVER['REQUEST_METHOD'] != 'POST' ||
    preg_match('/^[A-Za-z0-9_]{2}$/', $_POST['siteId']) !== 1 ||
    preg_match('/^[.A-Za-z0-9_-]+$/', $_POST['templateName']) !== 1)
    die;

define('SITE_ID', $_REQUEST['siteId']);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if (!check_bitrix_sessid())
    die;

if (!CModule::IncludeModule('nextype.alpha'))
    die();

$_REQUEST['arParams']['AJAX_REQUEST'] = 'Y';

$APPLICATION->RestartBuffer();
header('Content-Type: text/html; charset='.LANG_CHARSET);

if (isset($_REQUEST['action']) && in_array($_REQUEST['action'], array ('delete', 'update')))
{
    if ($_REQUEST['action'] == 'delete' && intval($_REQUEST['itemId']) > 0)
    {
        Tools\Basket::deleteItem(intval($_REQUEST['itemId']));
    }
    elseif ($_REQUEST['action'] == 'update' && isset($_REQUEST['quantity']) && intval($_REQUEST['itemId']) > 0)
    {
        Tools\Basket::updateItem(intval($_REQUEST['itemId']), floatval($_REQUEST['quantity']));
    }
}

$APPLICATION->IncludeComponent('bitrix:sale.basket.basket.line', $_REQUEST['templateName'], $_REQUEST['arParams']);
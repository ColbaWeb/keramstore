<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>

<div class="dropdown-menu-wrap">
    <? foreach ($arResult as $item): ?>
        <a class="dropdown-item font-body-2" href="<?=$item['LINK']?>">
            <span><?=$item['TEXT']?></span>
        </a>
    <? endforeach; ?>

    <a class="dropdown-item font-body-2 exit" href="<?= SITE_DIR ?>?logout=yes&<?= bitrix_sessid() ?>">
        <span><?= GetMessage('PERSONAL_MENU_LOGOUT_BUTTON') ?></span>
    </a>
</div>
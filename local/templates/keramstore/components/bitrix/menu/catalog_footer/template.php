<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;

?>
<? if (!empty($arResult))
{
    $count = 1;
    $bShowMoreLink = false;
    ?>

    <div class="catalog-list">
        <?
        foreach ($arResult as $arItem)
        {
            if (intval($arParams['ITEMS_COUNT']) > 0 && intval($arParams['ITEMS_COUNT']) < $count)
            {
                $bShowMoreLink = true;
                break;
            }
		?><a href="<?= $arItem['LINK'] ?>" class="item<?=$arItem['SELECTED'] ? ' active' : ''?>" title="<?=$arItem['TEXT']?>"><h3><?=$arItem['TEXT']?></h3></a><?
            
            $count++;
        }
        ?>
        
        <?
        if ($bShowMoreLink)
        {
            ?><a href="<?=$arParams['PATH_TO_CATALOG']?>" class="item all"><?=GetMessage('PATH_TO_CATALOG_TITLE')?> <i class="icon icon-arrow-right-white"></i></a><?
        }
        ?>
        
    </div>

<?
}
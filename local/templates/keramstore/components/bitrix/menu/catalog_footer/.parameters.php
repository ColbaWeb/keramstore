<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTemplateParameters = array(
    
    "PATH_TO_CATALOG" => array (
        "NAME" => GetMessage("TP_BM_PATH_TO_CATALOG"),
        "TYPE" => "STRING",
        "DEFAULT" => "/catalog/",
    ),
    
    "ITEMS_COUNT" => array (
        "NAME" => GetMessage("TP_BM_ITEMS_COUNT"),
        "TYPE" => "STRING",
        "DEFAULT" => "5",
    ),
    
);

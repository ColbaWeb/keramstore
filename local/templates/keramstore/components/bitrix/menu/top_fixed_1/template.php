<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;

$containerId = "top_fixed_menu_" . randString(6);
?>
<? if (!empty($arResult)): ?>
<ul class="menu-list-0 reset-ul-list" id="<?=$containerId?>">
    <? foreach ($arResult as $arItem):
            $arItemClasses = array ();
            if (!empty($arItem['SUB']))
                $arItemClasses[] = "has-sub";
            
            if (!empty($arItem['SELECTED']))
                $arItemClasses[] = "active";
    ?>
    <li data-depth="1" data-max-depth="1" <?=!empty($arItemClasses) ? 'class="'.implode(" ", $arItemClasses).'"' : ''?>>
        <a href="<?= $arItem['LINK'] ?>">
            <span ><?=$arItem['TEXT']?></span>
        </a>
        
        <? if (!empty($arItem['SUB'])): ?>
        <div class="sub">
            <ul class="menu-list-1 reset-ul-list">
                <? foreach ($arItem['SUB'] as $arItem2): ?>
                <li data-depth="2" <? if (!empty($arItem2['SUB'])): ?>class="has-sub"<?endif;?>>
                    <a href="<?=$arItem2['LINK']?>"><?=$arItem2['TEXT']?></a>
                    
                    <? if (!empty($arItem2['SUB'])): ?>
                    <div class="sub">
                        <ul class="menu-list-2 reset-ul-list">
                            <? foreach ($arItem2['SUB'] as $arItem3): ?>
                            <li data-depth="3">
                                <a href="<?=$arItem3['LINK']?>"><?=$arItem3['TEXT']?></a>
                            </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                    <? endif; ?>
                </li>
                <? endforeach; ?>
                
            </ul>
        </div>
        <? endif; ?>
    </li>
    <? endforeach; ?>
    
    
</ul>

<script>
    BX.ready(function() {
        new window.Alpha.Menu({
            containerId: "<?=$containerId?>"
        });
    });
</script>

<? endif; ?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
?>

<script>
    $(document).ready(function () {

        /*$('body').on('click', '.menu-button:not(.type3) .btn' , function () {
            $('body').toggleClass('fixed');
            $('.menu-button:not(.type3) .btn').toggleClass('opened').addClass('animating');
            $('.menu-container-wrapper').toggleClass('open');
            $('.catalog-list-wrap .catalog-list').children().removeClass('active');
            $('.catalog-list-wrap .catalog-list').children().first().addClass('active');
            $('.menu-content-wrapper .content').removeClass('visible');
            $('.menu-content-wrapper .content#menu-catalog-subcontent-1').addClass('visible');
        });*/

        $('body').on('mouseover', '.catalog-list .item', function () {
            $('.menu-content-wrapper .content').removeClass('visible');
            $('.menu-content-wrapper .content#menu-catalog-subcontent-'+($(this).index()+1)).addClass('visible');
            $('.catalog-list .item').removeClass('active');
            $(this).addClass('active');
        });

        $('body').on('mouseover', '.other-links', function () {
            $('.catalog-list .item').removeClass('active');
        });

        $('body').on('mouseover', '.menu-list-0 li', function () {
            if ($(this).hasClass('has-sub'))
                $(this).addClass('open');

            if (parseInt($(this).data('depth')) > 0) {
                var depth = $('.menu-list-0 li.open').length;
                if (depth > 0)
                    $('.menu-list-0 .shadow').css('width', (depth * 100) + '%');

                // set max height
                var maxHeight = $(this).hasClass('has-sub') ? parseInt($(this).find("> .sub")
                    .outerHeight(true)) : 0;

                $('.menu-list-0 li.open.has-sub').each(function () {

                    var currentHeight = parseInt($(this).find("> .sub").outerHeight(true));
                    if (currentHeight > maxHeight)
                        maxHeight = currentHeight;
                });

                $('.menu-list-0 .sub').css('min-height', maxHeight + 'px');
            }
        });

        $('body').on('mouseout', '.menu-list-0 li', function () {
            if ($(this).hasClass('has-sub'))
                $(this).removeClass('open');

            if (parseInt($(this).data('depth')) > 0) {
                $('.menu-list-0 .shadow').css('width', ($('.menu-list-0 li.open')
                    .length * 100) + '%');

                $('.menu-list-0 .sub').css('min-height', 'auto');
            }
        });

        var resizeCallback = function () {
            $('.menu-list-0 > li').each(function () {
                var maxDepth = parseInt($(this).data('max-depth')),
                    rightOffset = parseInt($(window).width() - ($(this).offset().left + $(this).outerWidth()));

                if (maxDepth >= 1) {
                    var sumWidth = parseInt($(this).find("> .sub").outerWidth()) * maxDepth;

                    if (sumWidth > rightOffset)
                        $(this).addClass('offset-left');
                }
            });
        };

        resizeCallback();
        $(window).resize(resizeCallback);
    });
</script>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;



if (is_array($arResult) && !empty($arResult))
{
    foreach ($arResult as $key => $arItem)
    {
        if (isset($arItem['PARAMS']['UF_SECTION_MENU_BANNER']) && !empty($arItem['PARAMS']['UF_SECTION_MENU_BANNER']['VALUE']))
        {
            $arImage = CFile::ResizeImageGet($arItem['PARAMS']['UF_SECTION_MENU_BANNER']['VALUE'], array ('width' => 600, 'height' => 600), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
            if (!empty($arImage))
            {
                $arItem['MENU_BANNER'] = array (
                    'IMAGE' => $arImage['src'],
                    'LINK' => str_replace("#SITE_DIR#", SITE_DIR, $arItem['PARAMS']['UF_SECTION_MENU_BANNER_URL']['VALUE'])
                );
            }
        }
        
        if (isset($arItem['PARAMS']['UF_BRANDS']) && !empty($arItem['PARAMS']['UF_BRANDS']['VALUE']) && !empty($arItem['PARAMS']['UF_BRANDS']['SETTINGS']['HLBLOCK_ID']))
        {
            foreach ($arItem['PARAMS']['UF_BRANDS']['VALUE'] as $itemId)
            {
                if (!empty($itemId))
                {
                    $arBrandItem = Tools\Cache::HLBlock_GetElementById($arItem['PARAMS']['UF_BRANDS']['SETTINGS']['HLBLOCK_ID'], $itemId);
                    if ($arBrandItem)
                    {
                        if (!empty($arParams['TEMPLATE_BRAND_URL']))
                        {
                            $arBrandItem['LINK'] = str_replace(array ("#SITE_DIR#", "#CODE#"), array (SITE_DIR, $arBrandItem['UF_LINK']), $arParams['TEMPLATE_BRAND_URL']);
                        }
                        
                        $arItem['BRANDS'][] = $arBrandItem;
                    }
                }
            }
        }
        
        if (!empty($arItem['PARAMS']['UF_MENU_ICON']['VALUE']))
        {
            $arItem['MENU_ICON'] = \CFile::GetPath((int)$arItem['PARAMS']['UF_MENU_ICON']['VALUE']);
        }
        
        $arResult[$key] = $arItem;
    }
        
    $menu = new Tools\Menu($arResult);
    $arResult = $menu->getTree();
}
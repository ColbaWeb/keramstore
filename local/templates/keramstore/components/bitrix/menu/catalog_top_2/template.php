<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid\Declension;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;
use Nextype\Alpha\Layout;

?>
<? if (!empty($arResult)): ?>

    <div class="menu-container-wrapper">
        <div class="container">
            <div class="menu-container row">
                <div class="list-wrap col-3">
                    <div class="catalog-list-wrap">
                        <div class="catalog-list">
                            <div class="scrollbar-inner">
                                <? foreach ($arResult as $arItem) {
                                    $arItemClasses = array();
                                    if (!empty($arItem['SUB']))
                                        $arItemClasses[] = "has-sub";

                                    if (!empty($arItem['SELECTED']))
                                        $arItemClasses[] = "active";
                                    ?>
                                    <a href="<?= $arItem['LINK'] ?>" class="item <?= implode(" ", $arItemClasses) ?>">
                                        <? if (!empty($arItem['MENU_ICON'])) { ?>
                                            <div class="menu-icon" style="background-image: url('<?= $arItem['MENU_ICON'] ?>')"></div>
                                        <? } ?>
                                        <?= $arItem['TEXT'] ?>
                                    </a>
                                <? } ?>
                                <div class="other-links">
                                    <? Layout\Partial::getInstance()->render('header/catalog_menu_sub_links.php') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-9 p-0">
                    <div class="menu-content-wrapper">
                        <? foreach ($arResult as $key => $arItem) { ?>
                            <div class="content<?= $key == 0 ? ' visible' : '' ?>
<? if ( isset($arItem['MENU_BANNER']) || !empty($arItem['BRANDS'])) : echo(' with-banners');
                            else : echo('');
                            endif; ?> scrollbar-inner"
                                 id="menu-catalog-subcontent-<?= ($key + 1) ?>">
                                <? if (!empty($arItem['SUB'])) { ?>
                                    <div class="catalog-inner">
                                        <div class="items font-body-2">
                                            <? foreach ($arItem['SUB'] as $arItem2) { ?>
                                                <div class="menu-category">
                                                    <div class="category-wrapper">
                                                        <a href="<?= $arItem2['LINK'] ?>"
                                                           class="item item-title font-body bold<?= $arItem2['SELECTED'] ? ' active' : '' ?>"><?= $arItem2['TEXT'] ?>
                                                        </a>
                                                        <? if (!empty($arItem2['SUB'])) {
                                                            foreach ($arItem2['SUB'] as $arItem3) { ?>
                                                                <a href="<?= $arItem3['LINK'] ?>"
                                                                   class="item<?= $arItem3['SELECTED'] ? ' active' : '' ?>"><?= $arItem3['TEXT'] ?>
                                                                </a>
                                                            <? }
                                                        } ?>
                                                    </div>
                                                </div>
                                            <? } ?>
                                        </div>
                                    </div>

                                    <? if(isset($arItem['MENU_BANNER']) || isset($arItem['BRANDS'])) { ?>
                                        <div class="brands-img-wrap">
                                            <? if (isset($arItem['MENU_BANNER']) && !empty($arItem['MENU_BANNER']['IMAGE'])) { ?>
                                                <div class="img-wrap">
                                                    <? if (!empty($link = $arItem['MENU_BANNER']['LINK'])) { ?>
                                                        <div class="img"
                                                             style="background-image: url('<?= $arItem['MENU_BANNER']['IMAGE'] ?>'); cursor: pointer;"
                                                             onclick="window.open('<?= $link ?>')">
                                                        </div>
                                                    <? }
                                                    else { ?>
                                                        <div class="img"
                                                             style="background-image: url('<?= $arItem['MENU_BANNER']['IMAGE'] ?>');"></div>
                                                    <? } ?>
                                                </div>
                                            <? }
                                            if (isset($arItem['BRANDS']) && !empty($arItem['BRANDS'])) { ?>
                                                <div class="brands-list">
                                                    <? foreach ($arItem['BRANDS'] as $arBrand) { ?>
                                                        <div class="item-wrap">
                                                            <a href="<?= $arBrand['LINK'] ?>"
                                                               class="brand-item font-body-small">
                                                                <? $previewImage = null;
                                                                if (!empty($arBrand['UF_FILE'])) {
                                                                    $previewImage = CFile::GetPath($arBrand['UF_FILE']);
                                                                }

                                                                echo Layout\Assets::showBackgroundImage($previewImage, array(
                                                                    "attributes" => array(
                                                                        "class=\"item-img\""
                                                                    )
                                                                )); ?>
                                                            </a>
                                                        </div>
                                                    <? } ?>
                                                </div>
                                            <? } ?>
                                        </div>
                                    <? } ?>
                                <? } ?>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>
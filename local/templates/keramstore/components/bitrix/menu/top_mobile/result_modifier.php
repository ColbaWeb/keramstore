<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;

if (is_array($arResult) && !empty($arResult))
{
    $menu = new Tools\Menu($arResult);
    $arResult = $menu->getTree();
}
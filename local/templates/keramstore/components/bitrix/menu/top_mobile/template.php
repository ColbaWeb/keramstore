<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;

if (!empty($arResult))
{
    foreach ($arResult as $arItem)
    {
        $arItemClasses = array (
            "info-page"
        );
        
        if (!empty($arItem['SUB']))
            $arItemClasses[] = "has-sub";

        if (!empty($arItem['SELECTED']))
            $arItemClasses[] = "active open";
        
        ?>
            <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                <a href="<?= $arItem['LINK'] ?>"><span><?=$arItem['TEXT']?></span></a>
                <?
                if (!empty($arItem['SUB']))
                {
                    ?>
                    <div class="sub">
                        <ul class="menu-list-2 list reset-ul-list">
                            <?
                            foreach ($arItem['SUB'] as $arItem2)
                            {
                                $arItemClasses = array ();
        
                                if (!empty($arItem2['SUB']))
                                    $arItemClasses[] = "has-sub";

                                if (!empty($arItem2['SELECTED']))
                                    $arItemClasses[] = "active open";
                                ?>
                                <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                                    <a href="<?= $arItem2['LINK'] ?>"><span><?=$arItem2['TEXT']?></span></a>

                                    <?
                                    if (!empty($arItem2['SUB']))
                                    {
                                        ?>
                                        <div class="sub">
                                            <ul class="menu-list-3 list reset-ul-list">
                                                <?
                                                foreach ($arItem2['SUB'] as $arItem3)
                                                {
                                                    ?>
                                                        <li<?=!empty($arItem3['SELECTED']) ? ' class="active"' : ''?>>
                                                                <a href="<?= $arItem3['LINK'] ?>"><span><?=$arItem3['TEXT']?></span></a>
                                                        </li>
                                                    <?
                                                }
                                                ?>
                                                <li class="menu-back">
                                                    <a href="javascript:void(0)">
                                                        <i class="icon icon-arrow-light-right"></i>
                                                        <span><?=GetMessage('BACK_LINK')?></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </li>
                                <?
                            }
                            ?>
                                <li class="menu-back">
                                    <a href="javascript:void(0)">
                                        <i class="icon icon-arrow-light-right"></i>
                                        <span><?=GetMessage('BACK_LINK')?></span>
                                    </a>
                                </li>
                        </ul>
                    </div>
                    <?
                }
                ?>
            </li>
        <?
    }
}

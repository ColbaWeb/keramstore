<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;

?>
<? if (!empty($arResult))
{
    ?>

    <ul class="right-menu reset-ul-list">
        
        <?
        foreach ($arResult as $arItem)
        {
            ?>
            <li<?=$arItem['SELECTED'] ? ' class="active"' : ''?>>
                <a href="<?= $arItem['LINK'] ?>" class="item"><?=$arItem['TEXT']?></a>
            </li>
        <?
        }
        ?>
    </ul>

<?
}
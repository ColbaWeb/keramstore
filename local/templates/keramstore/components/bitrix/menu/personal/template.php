<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>

<div class="navigation">
    <div class="list font-body-2">
        <? foreach ($arResult as $item): ?>
        <a href="<?=$item['LINK']?>" class="item<?=$item['SELECTED'] ? ' active' : ''?>"><?=$item['TEXT']?></a>
        <? endforeach; ?>

        <a href="<?= SITE_DIR ?>?logout=yes&<?= bitrix_sessid() ?>" class="item exit"><?= GetMessage('PERSONAL_MENU_LOGOUT_BUTTON') ?></a>
    </div>
</div>
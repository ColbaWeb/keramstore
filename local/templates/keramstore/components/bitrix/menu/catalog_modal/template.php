<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;

if (!empty($arResult))
{
    ?>

    <div class="menu-catalog-modal">
        <div class="sidebar-menu font-body-2">
            <ul class="reset-ul-list list-0">
                <?
                foreach ($arResult as $arItem)
                {
                    $arItemClasses = array ();
        
                    if (!empty($arItem['SUB']))
                        $arItemClasses[] = "has-sub";

                    if (!empty($arItem['SELECTED']))
                        $arItemClasses[] = "active";
                    ?>
                    <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                        <span>
                            <a href="<?= $arItem['LINK'] ?>"><?=$arItem['TEXT']?></a>
                            <i class="icon icon-arrow-light-down"></i>
                        </span>
                        
                        <?
                        if (!empty($arItem['SUB']))
                        {
                            ?>
                            <div class="sub">
                                <ul class="reset-ul-list list-1">
                                    <?
                                    foreach ($arItem['SUB'] as $arItem2)
                                    {
                                        $arItemClasses = array ();
        
                                        if (!empty($arItem2['SUB']))
                                            $arItemClasses[] = "has-sub";

                                        if (!empty($arItem2['SELECTED']))
                                            $arItemClasses[] = "active";
                                        
                                        ?>
                                        <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                                            <span>
                                                <a href="<?= $arItem2['LINK'] ?>"><?=$arItem2['TEXT']?></a>
                                                <i class="icon icon-arrow-light-down"></i>
                                            </span>
                                            
                                            <?
                                            if (!empty($arItem2['SUB']))
                                            {
                                                ?>
                                                <div class="sub">
                                                    <ul class="reset-ul-list list-2">
                                                        <?
                                                        foreach ($arItem2['SUB'] as $arItem3)
                                                        {
                                                            ?>
                                                            <li<?=$arItem3['SELECTED'] ? ' class="active"' : ''?>>
                                                                <span>
                                                                    <a href="<?=$arItem3['LINK']?>" class="text"><?=$arItem3['TEXT']?></a>
                                                                </span>
                                                            </li>
                                                            <?
                                                        }
                                                        ?>
                                                        
                                                    </ul>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </li>
                                        <?
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?
                        }
                        ?>
                    </li>
                    <?
                }
                ?>
            </ul>
        </div>
    </div>

    <?
}

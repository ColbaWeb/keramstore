<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;

?>
<? if (!empty($arResult)): ?>

    <div class="menu-list">
        <div class="items">
            <?
            foreach ($arResult as $arItem)
            {
                ?><a href="<?= $arItem['LINK'] ?>" class="item<?=$arItem['SELECTED'] ? ' active' : ''?>"><?=$arItem['TEXT']?></a><?
            }
            ?>
        </div>
    </div>
<? endif; ?>
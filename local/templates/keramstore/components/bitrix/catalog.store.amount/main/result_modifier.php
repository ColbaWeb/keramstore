<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arParams["SHOW_EMPTY_STORE"] != "Y")
{
    foreach ($arResult["STORES"] as $key => $arStore)
    {
        if ((int) $arStore["REAL_AMOUNT"] <= 0)
            unset($arResult["STORES"][$key]);
    }
}
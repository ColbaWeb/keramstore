<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;
use Bitrix\Main\Grid\Declension;


$storeDeclension = new Declension(Loc::getMessage("CT_BCSA_DECLENSION_STORE_ONE"),
    Loc::getMessage("CT_BCSA_DECLENSION_STORE_MANY"), Loc::getMessage("CT_BCSA_DECLENSION_STORE_MANY"));
$storeDeclText = $storeDeclension->get(count($arResult["STORES"]));
?>

<? if (count($arResult["STORES"]) > 0): ?>
    <?= Loc::getMessage("CT_BCSA_LINK_TITLE_TEXT"); ?>
    <a class="in-store-link" onclick="openModal();" href="javascript:void(0);">
        <?= Loc::getMessage("CT_BCSA_LINK_STORES_TEXT", array("#COUNT#" => count($arResult["STORES"]), "#DECLENSION#" => $storeDeclText)) ?>
    </a>
<? else: ?>
    <?= Loc::getMessage("CT_BCSA_LINK_TITLE_TEXT_EMPTY") ?>
<? endif; ?>

<div class="modal-container hidden modal-right in-store-modal">
    <div class="modal-content-container">
        <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
            <i class="icon icon-close-big"></i>
        </a>
        <div class="scrollbar-inner">
            <div class="content-wrapper">
                <div class="h2 in-store-heading"><?= $arParams["MAIN_TITLE"] ?></div>
                <div class="in-store-items">
                    <? foreach ($arResult["STORES"] as $arStore): ?>
                        <div class="in-store-item">
                            <div class="in-store-img">
                                <? $storeImageURL = CFile::ResizeImageGet($arStore["IMAGE_ID"], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL)['src']; ?>
                                <img src="<?= $storeImageURL ? $storeImageURL : SITE_TEMPLATE_PATH."/images/no-image.svg"; ?>" alt="<?= $arStore["TITLE"] ?>" title="<?= $arStore["TITLE"] ?>">
                            </div>
                            <div class="in-store-content">
                                <div class="in-store-title font-title"><?= $arStore["TITLE"] ?></div>
                                <div class="in-store-address font-body-2"><?= $arStore["ADDRESS"] ?></div>
                                <div class="in-store-time font-body-2"><?= $arStore["SCHEDULE"] ?></div>
                                <div class="in-store-address font-body-2"><?= $arStore["DESCRIPTION"] ?></div>
                                <div class="in-store-address font-body-2"><?= $arStore["PHONE"] ?></div>
                                <div class="in-store-address font-body-2"><?= $arStore["EMAIL"] ?></div>
                                <div class="in-store-address font-body-2"><?= implode(", ", $arStore["COORDINATES"]) ?></div>
                            </div>
                            <?
                            if ((float) $arStore["REAL_AMOUNT"] == 0)
                                $amountClass = "red";
                            elseif ($arParams["USE_MIN_AMOUNT"] == 'Y' && ((float) $arStore["REAL_AMOUNT"] < (float) $arParams["MIN_AMOUNT"]))
                                $amountClass = "orange";
                            else
                                $amountClass = "green";
                            ?>

                            <div class="in-store-qty font-body-small <?= $amountClass ?>"><?= $arStore["AMOUNT"] ?></div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="close-area"></div>
</div>

<script>
    function openModal() {
        $('.in-store-modal').removeClass('hidden');
        $('body').trigger('modalOpen').addClass('modal-opened');
    }
</script>


<?
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$arResult = Tools\Forms::modifierTemplateResult($arResult);        
?>
<div class="inline-form">
    <?=$arResult['FORM_HEADER']?>
        <?=bitrix_sessid_post();?>

        <?
        if ($arResult["isFormErrors"] == "N" && $arResult["isFormNote"] == "Y")
        {
            ?>
            <div class="confirm">
            <?
            if ($arResult["isFormTitle"])
            {
                ?><h2><?=$arResult["FORM_TITLE"]?></h2><?
            }
            ?>
            <div class="alert alert-success"><?=$arResult['FORM_SUCCESS_MESSAGE']?></div>
            </div>
        <?
        }
        else
        {
            if ($arResult["isFormTitle"])
            {
            ?>
            <h2><?=$arResult["FORM_TITLE"]?></h2>
            <?
            }

            if (is_array($arResult['FORM_ERRORS']))
            {
                $arErrors = array ();
                foreach ($arResult['FORM_ERRORS'] as $key => $value)
                {
                    if (!is_string($key))
                    {
                        $arErrors[] = $value;
                    }
                }

                if (!empty($arErrors))
                {
                    ?>
                    <div class="alert alert-danger">
                        <?=implode("<br>", $arErrors);?>
                    </div>
                    <?
                }
            }
        ?>

        <div class="row">
        <?
        $counter = 0;
        foreach ($arResult['QUESTIONS'] as $sid => $arQuestion): ?>
            <? if ($sid == $arParams['AUTOCOMPLETE_FIELD_NAME'] || $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea') continue; ?>
            <div class="col">
                <div class="custom-input-wrap">
                    <div class="custom-input<?=(is_array($arResult['FORM_ERRORS']) && array_key_exists ($sid, $arResult['FORM_ERRORS'])) ? ' error' : ''?>">
                        <?= Tools\Forms::getHtml($sid, $arQuestion, $arResult); ?>
                    </div>
                    <? if (is_array($arResult['FORM_ERRORS']) && array_key_exists ($sid, $arResult['FORM_ERRORS'])): ?>
                    <div class="error-message"><?=$arResult['FORM_ERRORS'][$sid]?></div>
                    <? endif; ?>
                </div>
            </div>
            <? if ($counter % 2 ): ?>
            </div>
            <div class="row">
            <? endif; ?>
        <? $counter++; endforeach; ?>
        </div>

        <?
        foreach ($arResult['QUESTIONS'] as $sid => $arQuestion)
        {
            if ($sid == $arParams['AUTOCOMPLETE_FIELD_NAME'] || $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] != 'textarea') continue;
            ?>
            <div class="row">
                <div class="col">
                    <div class="custom-input-wrap">
                        <div class="custom-input<?=(is_array($arResult['FORM_ERRORS']) && array_key_exists ($sid, $arResult['FORM_ERRORS'])) ? ' error' : ''?>">
                            <?= Tools\Forms::getHtml($sid, $arQuestion, $arResult); ?>
                        </div>
                        <? if (is_array($arResult['FORM_ERRORS']) && array_key_exists ($sid, $arResult['FORM_ERRORS'])): ?>
                        <div class="error-message"><?=$arResult['FORM_ERRORS'][$sid]?></div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <?
        }
        ?>

        <? if ($arResult['arForm']['USE_CAPTCHA'] == "Y")
        {
            $arResult['CAPTCHA_FIELD'] = str_replace('name=', 'required name=', $arResult['CAPTCHA_FIELD']);
            echo Tools\Forms::getCaptchaField($arResult['CAPTCHA_IMAGE'], $arResult['CAPTCHA_FIELD']);
        }
        ?>
            
        <div class="d-flex justify-content-between align-items-center">
            <?
            if ($arParams['USER_CONSENT'] == 'Y')
            {
                ?><div class="userconsent-request"><?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.userconsent.request",
                    "",
                    array(
                        "ID" => $arParams["USER_CONSENT_ID"],
                        "IS_CHECKED" => $arParams["USER_CONSENT_IS_CHECKED"],
                        "AUTO_SAVE" => "Y",
                        "IS_LOADED" => $arParams["USER_CONSENT_IS_LOADED"],
                        
                    )
                );
                ?></div><?
            }    
            ?>
            <button type='submit' value="submit" name="web_form_submit" class="btn btn-primary btn-large"><?=$arResult['arForm']['BUTTON']?></button>
        </div>

        <? } ?>

    <?=$arResult["FORM_FOOTER"]?>
</div>
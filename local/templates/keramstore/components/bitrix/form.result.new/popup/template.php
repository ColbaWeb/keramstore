<?
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$arResult = Tools\Forms::modifierTemplateResult($arResult);        
?>

<?=$arResult['FORM_HEADER']?>
    <?=bitrix_sessid_post();?>
    
    <?
    if ($arResult["isFormErrors"] == "N" && $arResult["isFormNote"] == "Y")
    {
        ?>
        <div class="confirm">
        <?
        if ($arResult["isFormTitle"])
        {
            ?><div class="modal-title font-h3"><?=$arResult["FORM_TITLE"]?></div><?
        }
        ?>
        <div class="modal-desc font-body-2"><?=$arResult['FORM_SUCCESS_MESSAGE']?></div>
        <button class="btn btn-primary btn-large" type="button" onclick="window.Alpha.Popups.closeAll();"><?=GetMessage('FORM_OK_BUTTON')?></button>
        </div>
    <?
    }
    else
    {
        if ($arResult["isFormTitle"])
        {
        ?>
        <div class="modal-title font-h3"><?=$arResult["FORM_TITLE"]?></div>
        <?
        }
        
        if (is_array($arResult['FORM_ERRORS']))
        {
            $arErrors = array ();
            foreach ($arResult['FORM_ERRORS'] as $key => $value)
            {
                if (!is_string($key))
                {
                    $arErrors[] = $value;
                }
            }

            if (!empty($arErrors))
            {
                ?>
                <div class="alert alert-danger">
                    <?=implode("<br>", $arErrors);?>
                </div>
                <?
            }
        }
    ?>
    
    <? foreach ($arResult['QUESTIONS'] as $sid => $arQuestion): ?>
        <? if ($sid == $arParams['AUTOCOMPLETE_FIELD_NAME']) continue; ?>
        <div class="custom-input-wrap">
            <div class="custom-input<?=(is_array($arResult['FORM_ERRORS']) && array_key_exists ($sid, $arResult['FORM_ERRORS'])) ? ' error' : ''?>">
                <?= Tools\Forms::getHtml($sid, $arQuestion, $arResult); ?>
            </div>
            <? if (is_array($arResult['FORM_ERRORS']) && array_key_exists ($sid, $arResult['FORM_ERRORS'])): ?>
            <div class="error-message"><?=$arResult['FORM_ERRORS'][$sid]?></div>
            <? endif; ?>
        </div>
    <? endforeach; ?>
    
    <? if ($arResult['arForm']['USE_CAPTCHA'] == "Y")
    {
        $arResult['CAPTCHA_FIELD'] = str_replace('name=', 'required name=', $arResult['CAPTCHA_FIELD']);
        echo Tools\Forms::getCaptchaField($arResult['CAPTCHA_IMAGE'], $arResult['CAPTCHA_FIELD']);
    }
    ?>
        
    <button type='submit' value="submit" name="web_form_submit" class="btn btn-primary btn-large"><?=$arResult['arForm']['BUTTON']?></button>
    
    <? } ?>

<?=$arResult["FORM_FOOTER"]?>
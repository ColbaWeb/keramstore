<?
use Bitrix\Main\Config\Option;
use Nextype\Alpha\Options;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (Options\Base::getInstance(SITE_ID)->getValue('smsAuthorizationActive'))
    return;

if(isset($APPLICATION->arAuthResult))
    $arResult['ERROR_MESSAGE'] = $APPLICATION->arAuthResult;

$bLoginError = false;
if (isset($arResult['ERROR_MESSAGE']['ERROR_TYPE']) && $arResult['ERROR_MESSAGE']['ERROR_TYPE'] == "LOGIN")
    $bLoginError = true;

$bUserPhoneAuth = Option::get("main", "new_user_phone_auth", "N") == "Y";
$bUserPhoneRequired = Option::get("main", "new_user_phone_required", "N") == "Y";
$bUserEmailAuth = Option::get("main", "new_user_email_auth", "N") == "Y";
$bUserEmailRequired = Option::get("main", "new_user_email_required", "N") == "Y";

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

?>

<div class="auth-registration auth-registration-container">
    <ul class="nav nav-tabs reset-ul-list">
        <li class="nav-item">
            <a class="nav-link active" href="<?=$APPLICATION->GetCurDir()?>"><?=GetMessage('AUTH_AUTHORIZE')?></a>
        </li>
        
        <?if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
        <noindex>
            <li class="nav-item">
                <a class="nav-link" href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage('AUTH_REGISTER')?></a>
            </li>
        </noindex>
        <?endif?>

    </ul>
    <div class="tab-content">
        <div class="tab-pane active">
            <?
            if ($USER->IsAuthorized())
            {
                ?>
                <div class="alert alert-success"><?=GetMessage('SUCCESS_AUTH_NOTE')?></div>
                <script>
                    window.location.href = "<?=$arParams['REDIRECT_URL']?>";
                </script>
                <?
            }
            else
            {
                if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'] && !$bLoginError)
                {
                    ?>
                    <div class="alert alert-warning"><?=$arResult['ERROR_MESSAGE']?></div>
                    <?
                }
            ?>
            <form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                <input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
                <input type="hidden" name="USER_REMEMBER" value="Y" />
                
		<?if ($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
		<?endforeach?>
                
                <? if ($bUserPhoneAuth && $bUserPhoneRequired): ?>
                <div class="custom-input-wrap">
                    <div class="custom-input<?=$bLoginError ? ' error' : ''?>">
                        <input type="text" value="<?=$request->get('USER_LOGIN')?>" data-field-type="phone" required="required" name="USER_LOGIN" maxlength="50">
                        <label><?=GetMessage("AUTH_PHONE_NUMBER")?></label>
                    </div>
                    <? if ($bLoginError): ?>
                    <div class="error-message"><?=GetMessage('USER_WITH_PHONE_NUMBER_NOT_FOUND')?></div>
                    <? endif; ?>
                </div>
                <? elseif ($bUserEmailAuth && $bUserEmailRequired): ?>
                <div class="custom-input-wrap">
                    <div class="custom-input<?=$bLoginError ? ' error' : ''?>">
                        <input type="text" value="<?=$request->get('USER_LOGIN')?>" required="required" name="USER_LOGIN" maxlength="50">
                        <label><?=GetMessage("AUTH_EMAIL")?></label>
                    </div>
                    <? if ($bLoginError): ?>
                    <div class="error-message"><?=GetMessage('USER_WITH_EMAIL_NOT_FOUND')?></div>
                    <? endif; ?>
                </div>
                <? else: ?>
                <div class="custom-input-wrap">
                    <div class="custom-input<?=$bLoginError ? ' error' : ''?>">
                        <input type="text" required="required" value="<?=$arResult['USER_LOGIN']?>" name="USER_LOGIN" maxlength="50">
                        <label><?=GetMessage("AUTH_LOGIN")?></label>
                    </div>
                    <? if ($bLoginError): ?>
                    <div class="error-message"><?= strip_tags($arResult['ERROR_MESSAGE']['MESSAGE'])?></div>
                    <? endif; ?>

                </div>
                <? endif; ?>
                
                
                <div class="custom-input-wrap">
                    <div class="custom-input">
                        <input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" />
                        <label><?=GetMessage("AUTH_PASSWORD")?></label>
                    </div>
                </div>
                
                <?if ($arResult["CAPTCHA_CODE"]):?>
                <div class="custom-input-wrap captcha">
                    <div class="custom-input">
                        <input name="captcha_word" maxlength="50" value="" autocomplete="off">
                        <label><?=GetMessage("AUTH_CAPTCHA_PROMT")?></label>
                    </div>

                    <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                </div>
                <?endif?>
                
                
                <button class="btn btn-primary btn-large" name="Login" value="y"><?=GetMessage("AUTH_AUTHORIZE_BUTTON")?></button>
                
                <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                <noindex>
                <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow" class="link font-large-button"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
		</noindex>
                <?endif?>
                
            </form>
            
            <?if($arResult["AUTH_SERVICES"]):?>
            <div class="social-networks">
                <div class="title font-body-2">
                    <?=GetMessage('socserv_as_user_form')?>
                </div>
                <div class="list">
                    <?
                    $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "main",
                            array(
                                    "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                                    "CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
                                    "AUTH_URL" => $arResult["AUTH_URL"],
                                    "POST" => $arResult["POST"],
                                    "SHOW_TITLES" => $arResult["FOR_INTRANET"]?'N':'Y',
                                    "FOR_SPLIT" => $arResult["FOR_INTRANET"]?'Y':'N',
                                    "AUTH_LINE" => $arResult["FOR_INTRANET"]?'N':'Y',
                            ),
                            $component,
                            array("HIDE_ICONS"=>"Y")
                    );
                    ?>
                </div>
            </div>
            <? endif; ?>
                    
            <? } ?>
        </div>
        
    </div>
</div>

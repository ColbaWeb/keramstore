<?
use Bitrix\Main\Config\Option;
use Nextype\Alpha\Options;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (Options\Base::getInstance(SITE_ID)->getValue('smsAuthorizationActive'))
{
    $APPLICATION->IncludeComponent("nextype:alpha.authorize", "main", Array(
                            "RESEND_DELAY" => Options\Base::getInstance(SITE_ID)->getValue('smsAuthorizationResendTime'),
                            "PHONE_REGISTRATION" => "Y",
                            "EMAIL_REGISTRATION" => "N",
                            "EMAIL_REQUIRED" => "Y",
                                )
                        );
}
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 */

$this->setFrameMode(true);

$templateLibrary = array('popup');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);


$arParams['~MESS_BTN_BUY'] = $arParams['~MESS_BTN_BUY'] ?: Loc::getMessage('CT_SPG_TPL_MESS_BTN_CHOOSE');
$arParams['~MESS_BTN_DETAIL'] = $arParams['~MESS_BTN_DETAIL'] ?: Loc::getMessage('CT_SPG_TPL_MESS_BTN_DETAIL');
$arParams['~MESS_BTN_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_SPG_TPL_MESS_BTN_COMPARE');
$arParams['~MESS_BTN_SUBSCRIBE'] = $arParams['~MESS_BTN_SUBSCRIBE'] ?: Loc::getMessage('CT_SPG_TPL_MESS_BTN_SUBSCRIBE');
$arParams['~MESS_BTN_ADD_TO_BASKET'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_SPG_TPL_MESS_BTN_CHOOSE');
$arParams['~MESS_NOT_AVAILABLE'] = $arParams['~MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_SPG_TPL_MESS_PRODUCT_NOT_AVAILABLE');
$arParams['~MESS_SHOW_MAX_QUANTITY'] = $arParams['~MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_SPG_CATALOG_SHOW_MAX_QUANTITY');
$arParams['~MESS_RELATIVE_QUANTITY_MANY'] = $arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_SPG_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['~MESS_RELATIVE_QUANTITY_FEW'] = $arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_SPG_CATALOG_RELATIVE_QUANTITY_FEW');

$arResizeParams = array('width' => 200, 'height' => 200);
$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($this->randString()));
$containerName = 'sale-products-gift-container';
?>

<div class="list" data-entity="<?=$containerName?>">
    <?
	if (!empty($arResult['ITEMS']))
	{
           
            ?>
            <!-- items-container -->
            <?
            foreach ($arResult['ITEMS'] as $item)
            {         
                $arPicture = array (
                    'SRC' => null
                );
                
                if (isset($item['PREVIEW_PICTURE']['ID']))
                {
                    if ($arImage = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
                    {
                        $arPicture['SRC'] = $arImage['src'];
                    }
                }
                elseif (isset($item['DETAIL_PICTURE']['ID']))
                {
                    if ($arImage = CFile::ResizeImageGet($item['DETAIL_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
                    {
                        $arPicture['SRC'] = $arImage['src'];
                    }
                }

                $price = empty($item['OFFERS']) ? $item['ITEM_PRICES'][$item['ITEM_PRICE_SELECTED']] : $item['OFFERS'][0]['ITEM_PRICES'][$item['OFFERS'][0]['ITEM_PRICE_SELECTED']];
                $measureRatio = $price['MIN_QUANTITY'];
                
                
                ?>
                <div class="custom-radio-gift" data-entity="item">
                    <input type="radio" value="<?=$item['ID']?>" data-entity="selected-item-id" id="gift-<?=$item['ID']?>" name="gifts">
                    <label class="label" for="gift-<?=$item['ID']?>">
                        
                        <?
                        echo Layout\Assets::showBackgroundImage($arPicture['SRC'], array(
                            "attributes" => array(
                                "class=\"img\""

                                )
                        ));
                        ?>

                        <div class="info">
                            <div class="gift-price-block">
                                <div class="price font-body-2 bold"><?=$price['PRINT_RATIO_DISCOUNT']?></div>
                                
                                <div class="font-body-small price-old-block">
                                    <span class="price-old"></span>
                                </div>
                                <?
                                if ($price['PERCENT'] > 0)
                                {
                                    ?><div class="labels"><span class="label action">-<?=$price['PERCENT']?>%</span></div><?
                                }
                                ?>
                            </div>
                            <a href="<?=$item['DETAIL_PAGE_URL']?>" target="_blank" class="gift-title font-title-small">
                                <?=$item['NAME']?>
                            </a>
                        </div>
                    </label>
                </div>
                <?
            }
            ?>
            <!-- items-container -->
            <?
        }
	
        $signer = new \Bitrix\Main\Security\Sign\Signer;
	$signedTemplate = $signer->sign($templateName, 'sale.products.gift');
	$signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'sale.products.gift');
	?>
</div>

<script>
	BX.message({
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_SPG_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BASKET_URL: '<?=$arParams['BASKET_URL']?>',
		ADD_TO_BASKET_OK: '<?=GetMessageJS('ADD_TO_BASKET_OK')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_SPG_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_SPG_CATALOG_TITLE_BASKET_PROPS')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('ADD_TO_BASKET_OK')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_SPG_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_MESSAGE_SEND_PROPS: '<?=GetMessageJS('CT_SPG_CATALOG_BTN_MESSAGE_SEND_PROPS')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_SPG_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_SPG_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_SPG_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_SPG_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_SPG_CATALOG_MESS_COMPARE_TITLE')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_SPG_CATALOG_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_SPG_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>'
	});

	var <?=$obName?> = new JAlphaSaleProductsGiftComponent({
		siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
		componentPath: '<?=CUtil::JSEscape($componentPath)?>',
		deferredLoad: true,
		initiallyShowHeader: '<?=!empty($arResult['ITEM_ROWS'])?>',
		currentProductId: <?=CUtil::JSEscape((int)$arResult['POTENTIAL_PRODUCT_TO_BUY']['ID'])?>,
		template: '<?=CUtil::JSEscape($signedTemplate)?>',
		parameters: '<?=CUtil::JSEscape($signedParams)?>',
		container: '<?=$containerName?>'
	});
</script>
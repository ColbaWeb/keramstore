(function() {
	'use strict';

	if (!!window.JAlphaSaleProductsGiftComponent)
		return;

	window.JAlphaSaleProductsGiftComponent = function(params) {
		this.formPosting = false;
		this.siteId = params.siteId || '';
		this.template = params.template || '';
		this.componentPath = params.componentPath || '';
		this.parameters = params.parameters || '';

		this.container = document.querySelector('[data-entity="' + params.container + '"]');
		this.currentProductId = params.currentProductId;

		if (params.initiallyShowHeader)
		{
			BX.ready(BX.delegate(this.showHeader, this));
		}

		if (params.deferredLoad)
		{
			BX.ready(BX.delegate(this.deferredLoad, this));
		}

		BX.addCustomEvent(
			'onCatalogStoreProductChange',
			BX.delegate(function(offerId){
				offerId = parseInt(offerId);

				if (this.currentProductId === offerId)
				{
					return;
				}

				this.currentProductId = offerId;
				this.offerChangedEvent();
			}, this)
		);
	};

	window.JAlphaSaleProductsGiftComponent.prototype =
	{
		offerChangedEvent: function()
		{
			this.sendRequest({action: 'deferredLoad', offerId: this.currentProductId});
		},

		deferredLoad: function()
		{
			this.sendRequest({action: 'deferredLoad'});
		},

		sendRequest: function(data)
		{
			var defaultData = {
				siteId: this.siteId,
				template: this.template,
				parameters: this.parameters
			};

			BX.ajax({
				url: this.componentPath + '/ajax.php' + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : ''),
				method: 'POST',
				dataType: 'json',
				timeout: 60,
				data: BX.merge(defaultData, data),
				onsuccess: BX.delegate(function(result){
					BX.cleanNode(this.container);
					if (!result || !result.JS)
					{
						this.hideHeader();
						return;
					}

					BX.ajax.processScripts(
						BX.processHTML(result.JS).SCRIPT,
						false,
						BX.delegate(function(){this.showAction(result, data);}, this)
					);
				}, this)
			});
		},

		showAction: function(result, data)
		{
			if (!data)
				return;

			switch (data.action)
			{
				case 'deferredLoad':
					this.processDeferredLoadAction(result);
					break;
			}
		},

		processDeferredLoadAction: function(result)
		{
			if (!result)
				return;

			this.processItems(result.items);
		},

		processItems: function(itemsHtml)
		{
			if (!itemsHtml)
				return;

			var processed = BX.processHTML(itemsHtml, false),
				temporaryNode = BX.create('DIV');

			var items, k;

			temporaryNode.innerHTML = processed.HTML;
			items = temporaryNode.querySelectorAll('[data-entity="item"]');
                        

			if (items.length > 0)
			{
				this.showHeader();
                                
				for (k in items)
				{
					if (items.hasOwnProperty(k))
					{
                                            this.container.appendChild(items[k]);
					}
				}

			}
                        else
                        {
                            this.hideHeader();
                        }
                        
                        var radios = this.container.querySelectorAll('[data-entity="selected-item-id"]');
                        if (radios.length > 0)
                        {
                            for (var i = 0; i < radios.length; i++)
                            {
                                BX.bind(radios[i], 'change', BX.delegate(function (e) {
                                    var target = BX.proxy_context;
                                    if (!!target.value && parseInt(target.value) > 0)
                                    {
                                        BX.onCustomEvent('onSaleGiftChange', [parseInt(target.value)]);
                                    }
                                    
                                }, this));
                            }
                        }
                        
			BX.ajax.processScripts(processed.SCRIPT);
                        BX.onCustomEvent('onAjaxSuccess');
		},

		showHeader: function()
		{
			var parentNode = BX.findParent(this.container, {attr: {'data-entity': 'parent-container'}}),
				header;

			if (parentNode && BX.type.isDomNode(parentNode))
			{
				header = parentNode.querySelector('[data-entity="header"]');
                                

				if (header)
				{
                                    BX.removeClass(header, 'd-none');
				}
			}
		},

		hideHeader: function()
		{
			var parentNode = BX.findParent(this.container, {attr: {'data-entity': 'parent-container'}}),
				header;

			if (parentNode && BX.type.isDomNode(parentNode))
			{
				header = parentNode.querySelector('[data-entity="header"]');

				if (header)
				{
                                    BX.addClass(header, 'd-none');
				}
			}
		}
	}
})();
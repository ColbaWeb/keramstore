<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;

if($arResult["SHOW_SMS_FIELD"] == true)
{
    CJSCore::Init('phone_auth');
}

?>
<? ShowError($arResult["strProfileError"]);

if ($arResult['DATA_SAVED'] == 'Y')
{
    ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
}

?>
<div class="bx_profile">

    <?if($arResult["SHOW_SMS_FIELD"] == true):?>

        <form method="post" action="<?=$arResult["FORM_TARGET"]?>">
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
            <input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />
            <div class="main-profile-block-shown">
                <div class="row">
                    <div class="col-sm-10 col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-4 col-md-5 col-form-label main-profile-form-label"><?=Loc::getMessage("SMS_CODE")?></label>
                            <div class="col-12">
                                <input class="form-control" size="30" type="tel" name="SMS_CODE" value="<?=htmlspecialcharsbx($arResult["SMS_CODE"])?>" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="bx_profile_error" style="display:none"><?ShowError("error")?></div>

            <div id="bx_profile_resend"></div>

            <div class="row">
                <div class="col col-sm-10 col-md-6">
                    <input class="btn btn-themes btn-primary btn-md main-profile-submit" type="submit" name="code_submit_button" value="<?=Loc::getMessage("SMS_CODE_APPLY")?>" /></p>
                    <input type="submit" class="btn btn-themes btn-link btn-md"  name="reset" value="<?echo GetMessage("MAIN_RESET")?>">
                </div>
            </div>
        </form>

        <script>
            new BX.PhoneAuth({
                containerId: 'bx_profile_resend',
                errorContainerId: 'bx_profile_error',
                interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                data:
                    <?=CUtil::PhpToJSObject([
                        'signedData' => $arResult["SIGNED_DATA"],
                    ])?>,
                onError:
                    function(response)
                    {
                        var errorDiv = BX('bx_profile_error');
                        var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
                        errorNode.innerHTML = '';
                        for(var i = 0; i < response.errors.length; i++)
                        {
                            errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
                        }
                        errorDiv.style.display = '';
                    }
            });
        </script>

    <?else:?>
        <form method="post" name="form1" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data" role="form">
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value="<?=$arResult["ID"]?>" />
            <input type="hidden" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" />
            <div class="main-profile-block-shown" id="user_div_reg">
                <div class="row main-profile-block-date-info">
                    <?
                    if($arResult["ID"]>0)
                    {
                        if ($arResult["arUser"]["TIMESTAMP_X"] <> '')
                        {
                            ?>
                            <div class="col-12">
                                <strong><?=Loc::getMessage('LAST_UPDATE')?></strong>
                                <strong><?=$arResult["arUser"]["TIMESTAMP_X"]?></strong>
                            </div>
                            <?
                        }

                        if ($arResult["arUser"]["LAST_LOGIN"] <> '')
                        {
                            ?>
                            <div class="col-12">
                                <strong><?=Loc::getMessage('LAST_LOGIN')?></strong>
                                <strong><?=$arResult["arUser"]["LAST_LOGIN"]?></strong>
                            </div>
                            <?
                        }
                    }
                    ?>
                </div>

                <div class="row">
                    <div class="col-12">
                        <?
                        if (!in_array(LANGUAGE_ID,array('ru', 'ua')))
                        {
                            ?>
                            <div class="row">
                                <div class="col align-items-center">
                                    <div class="form-group">
                                        <label class="main-profile-form-label" for="main-profile-title"><?=Loc::getMessage('main_profile_title')?></label>
                                        <input class="form-control" type="text" name="TITLE" maxlength="50" id="main-profile-title" value="<?=$arResult["arUser"]["TITLE"]?>" />
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                        ?>
                        <div class="form-group row">
                            <label class="col-sm-4 col-md-3 col-form-label main-profile-form-label" for="main-profile-name"><?=Loc::getMessage('NAME')?></label>
                            <div class="col-sm-8 col-md-9">
                                <input class="form-control" type="text" name="NAME" maxlength="50" id="main-profile-name" value="<?=$arResult["arUser"]["NAME"]?>" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-md-3 col-form-label main-profile-form-label" for="main-profile-last-name"><?=Loc::getMessage('LAST_NAME')?></label>
                            <div class="col-sm-8 col-md-9">
                                <input class="form-control" type="text" name="LAST_NAME" maxlength="50" id="main-profile-last-name" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-md-3 col-form-label main-profile-form-label" for="main-profile-second-name"><?=Loc::getMessage('SECOND_NAME')?></label>
                            <div class="col-sm-8 col-md-9">
                                <input class="form-control" type="text" name="SECOND_NAME" maxlength="50" id="main-profile-second-name" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" />
                            </div>
                        </div>
                        <?if($arResult["PHONE_REGISTRATION"] && false):?>
                            <div class="form-group row">
                                <label class="col-sm-4 col-md-3 col-form-label main-profile-form-label" for="main-profile-phone-number"><?=Loc::getMessage("PHONE_NUMBER")?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input class="form-control" type="tel" data-field-type="phone" name="PHONE_NUMBER" maxlength="50" id="main-profile-phone-number" value="<?=$arResult["arUser"]["PHONE_NUMBER"]?>" />
                                </div>
                            </div>
                        <?endif?>
                        <div class="form-group row">
                            <label class="col-sm-4 col-md-3 col-form-label main-profile-form-label" for="main-profile-email"><?=Loc::getMessage('EMAIL')?></label>
                            <div class="col-sm-8 col-md-9">
                                <input class="form-control" type="text" name="EMAIL" maxlength="50" id="main-profile-email" value="<?=$arResult["arUser"]["EMAIL"]?>" />
                            </div>
                        </div>
                        <?
                        if ($arResult['CAN_EDIT_PASSWORD'])
                        {
                            ?>
                            <div class="form-group row">
                                <label class="col-sm-4 col-md-3 col-form-label main-profile-form-label" for="main-profile-password"><?=Loc::getMessage('NEW_PASSWORD_REQ')?></label>
                                <div class="col-sm-8 col-md-9">
                                    <input class=" form-control bx-auth-input main-profile-password" type="password" name="NEW_PASSWORD" maxlength="50" id="main-profile-password" value="" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-md-3 col-form-label main-profile-form-label main-profile-password" for="main-profile-password-confirm">
                                    <?=Loc::getMessage('NEW_PASSWORD_CONFIRM')?>
                                </label>
                                <div class="col-sm-8 col-md-9">
                                    <input class="form-control" type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" id="main-profile-password-confirm" autocomplete="off" />
                                    <small id="emailHelp" class="form-text text-muted"><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></small>
                                </div>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col">
                    <input type="submit" class="btn btn-themes btn-primary btn-md main-profile-submit" name="save" value="<?=(($arResult["ID"]>0) ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD"))?>">
                    <input type="submit" class="btn btn-themes btn-link btn-md"  name="reset" value="<?echo GetMessage("MAIN_RESET")?>">
                </div>
            </div>

        </form>
        <?
        $disabledSocServices = isset($arParams['DISABLE_SOCSERV_AUTH']) && $arParams['DISABLE_SOCSERV_AUTH'] === 'Y';

        if (!$disabledSocServices)
        {
            ?>
            <div class="col-sm-12 main-profile-social-block">
                <?
                if ($arResult["SOCSERV_ENABLED"])
                {
                    $APPLICATION->IncludeComponent(
                        "bitrix:socserv.auth.split",
                        ".default",
                        [
                            "SHOW_PROFILES" => "Y",
                            "ALLOW_DELETE" => "Y",
                        ],
                        false
                    );
                }
                ?>
            </div>
            <?
        }
        ?>
        <div class="clearfix"></div>
        <script>
            BX.Sale.PrivateProfileComponent.init();
        </script>
    <? endif; ?>
</div>
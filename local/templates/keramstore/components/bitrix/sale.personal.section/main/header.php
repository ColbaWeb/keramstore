<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;

Layout\Assets::getInstance(SITE_ID)->addLess(SITE_TEMPLATE_PATH . "/less/profile-detail.less");

?>
<div class="profile-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
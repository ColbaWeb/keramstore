(function(window){
	'use strict';

	if (window.JAlphaApplyFilter)
		return;
            
        window.JAlphaApplyFilter = function()
	{
            
        }
        
        window.JAlphaApplyFilter.unset = function(ids)
	{
            if (typeof(ids) == 'object')
            {
                var element = null;
                
                for (var key in ids)
                {
                    element = $("#" + ids[key]);
                    if (element.length > 0)
                    {
                        if (element.is(":checkbox") || element.is(":radio"))
                        {
                            element.attr('checked', false);
                        }
                        else if (element.attr("type") == "text" || element.attr("type") == "number")
                        {
                            element.val('');
                        }
                    }
                }
                
                if (!!element)
                {
                    element.trigger('filterRefresh');
                }
                
            }
            
        }
        
        
})(window);
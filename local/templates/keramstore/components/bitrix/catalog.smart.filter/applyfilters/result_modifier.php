<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$arResult['SELECTED'] = Array ();

if (!empty($arResult['PRICES']))
{
    foreach ($arResult['PRICES'] as $priceCode => $arPrice)
    {
        if (isset($arResult['ITEMS'][$priceCode]['VALUES']))
        {
            $arItem = $arResult['ITEMS'][$priceCode]['VALUES'];
            if ( (floatval($arItem['MIN']['HTML_VALUE']) > 0 && floatval($arItem['MIN']['VALUE']) <> floatval($arItem['MIN']['HTML_VALUE'])) ||
                 (floatval($arItem['MAX']['HTML_VALUE']) > 0 && floatval($arItem['MAX']['VALUE']) <> floatval($arItem['MAX']['HTML_VALUE']))
            )
            {
                $arResult['SELECTED'][$priceCode] = array (
                    "NAME" => $arPrice['TITLE'],
                    "RANGE_TYPE" => true,
                    "VALUES" => array (
                        0 => $arItem['MIN']['HTML_VALUE'],
                        1 => $arItem['MAX']['HTML_VALUE']
                    ),
                    "UNSET_VALUES" => array (
                        0 => $arItem['MIN']['CONTROL_ID'],
                        1 => $arItem['MAX']['CONTROL_ID'],
                    )
                );
            }
        }
    }
}

if (!empty($arResult['ITEMS']))
{
    foreach ($arResult['ITEMS'] as $propId => $arItem)
    {
        if (in_array($arItem['PROPERTY_TYPE'], array ('N')) && !empty($arItem['VALUES']) && 
           ( floatval($arItem['VALUES']['MIN']['HTML_VALUE']) > 0 || floatval($arItem['VALUES']['MAX']['HTML_VALUE']) > 0)
        )
        {
            $arResult['SELECTED'][$propId] = array (
                'NAME' => $arItem['NAME'],
                'RANGE_TYPE' => true,
                'VALUES' => array (
                    0 => isset($arItem['VALUES']['MIN']['HTML_VALUE']) ? $arItem['VALUES']['MIN']['HTML_VALUE'] : $arItem['VALUES']['MIN']['VALUE'],
                    1 => isset($arItem['VALUES']['MAX']['HTML_VALUE']) ? $arItem['VALUES']['MAX']['HTML_VALUE'] : $arItem['VALUES']['MAX']['VALUE'],
                ),
                "UNSET_VALUES" => array (
                        0 => $arItem['VALUES']['MIN']['CONTROL_ID'],
                        1 => $arItem['VALUES']['MAX']['CONTROL_ID'],
                )
            );
            
        }
        elseif (!in_array($arItem['PROPERTY_TYPE'], array ('N')) && !empty($arItem['VALUES']))
        {
            foreach ($arItem['VALUES'] as $arValue)
            {
                if (!empty($arValue['CHECKED']))
                {
                    $arResult['SELECTED'][$propId]['NAME'] = $arItem['NAME'];
                    $arResult['SELECTED'][$propId]['RANGE_TYPE'] = false;
                    
                    if (!is_array($arResult['SELECTED'][$propId]['VALUES']))
                    {
                        $arResult['SELECTED'][$propId]['VALUES'] = array ();
                    }

                    $arResult['SELECTED'][$propId]['VALUES'][] = $arValue['VALUE'];
                    $arResult['SELECTED'][$propId]['UNSET_VALUES'][] = $arValue['CONTROL_ID'];
                }
            }
        }
    }
}

if ($arResult['SEF_DEL_FILTER_URL'] && isset($_GET['mode']))
{
    $modeParam = strpos($arResult['SEF_DEL_FILTER_URL'], "?") ? '&mode=' . $_GET['mode'] : '?mode=' . $_GET['mode'];
    $arResult['SEF_DEL_FILTER_URL'] .= $modeParam;
}
<?
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$containerId = empty($arResult["FILTER_NAME"]) ? "apply_filter_container" : $arResult["FILTER_NAME"] . "_apply_container";


if (!empty($arResult['SELECTED']))
{
?>

<div class="added-filters-wrap" id="<?=$containerId?>">
    <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
        <?
        foreach($arResult["HIDDEN"] as $arItem)
        {
            ?><input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" /><?
        }
        ?>
        <!-- <div class="container"> -->
            <div class="added-filters">
                <?
                foreach ($arResult['SELECTED'] as $arItem)
                {
                    ?>
                    <div class="added-filter">
                        <div class="info-block">
                            <div class="title font-tiny-text"><?=$arItem['NAME']?></div>
                            <div class="value font-small-button">
                                <?
                                if ($arItem['RANGE_TYPE'] && count($arItem['VALUES']) > 1)
                                {
                                    echo $arItem['VALUES'][0] . " - " . end($arItem['VALUES']);
                                }
                                elseif (!$arItem['RANGE_TYPE'] && count($arItem['VALUES']) > 1)
                                {
                                    echo implode(", " , $arItem['VALUES']);
                                }
                                else
                                {
                                    echo $arItem['VALUES'][0];
                                }
                                ?>
                            </div>
                        </div>
                        <a href="javascript:void(0);" onclick="window.JAlphaApplyFilter.unset(<?=CUtil::PhpToJSObject($arItem['UNSET_VALUES'])?>)" class="added-close btn-close">
                            <i class="icon icon-close"></i>
                        </a>
                    </div>
                    <?
                }
                ?>
                
                <a href="<?=$arResult['SEF_DEL_FILTER_URL']?>" class="btn-reset font-small-button">
                    <?=GetMessage("CT_BCSF_DEL_FILTER")?>
                </a>

            </div>
        <!-- </div> -->
    </form>
</div>

<?
}
?>
<?
use Bitrix\Main\Page\Asset;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$_SESSION["SMARTFILTER"] = $arResult["ITEMS"];

$this->setFrameMode(true);
$containerId = empty($arResult["FILTER_NAME"]) ? "filter_container" : $arResult["FILTER_NAME"] . "_container";


$varclearlink = "/catalog/filter/clear/apply/";

if (strpos($arResult["FORM_ACTION"], "catalogprods") !== false) {
    $varclearlink  = str_replace("catalog","catalogprods", $varclearlink);
} else {
    $arResult["FORM_ACTION_PRODS"] = str_replace("catalog","catalogprods", $arResult["FORM_ACTION"]);

}

if (strpos($arResult["FORM_ACTION"], "catalogprods") !== false) {
    $arResult["FORM_ACTION_COLS"] = str_replace("catalogprods","catalog", $arResult["FORM_ACTION"]);
} else {
    $arResult["FORM_ACTION_COLS"] = $arResult["FORM_ACTION"];
}

/*echo "<pre>";
print_r($arResult);
echo "</pre>";*/

if (!empty($arResult["ITEMS"]))
{

    ?>

    <!--    --><?// Bitrix\Main\Diag\Debug::dump("hello"); ?>

    <div class="horizontal-filter" data-entity="smart-filter-container" id="<?=$containerId?>">
        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smart-filter horizontal" id="smart-filter">
            <fieldset class="smart-filter_fieldset fieldset_flex fieldset_row">
                <button type="button" class="btn btn-primary font-large-button smart-filter_btn-filter" id="filter-btn">
                    <i class="icon icon-filter"></i>
                </button>
            </fieldset>
            <fieldset class="smart-filter_fieldset fieldset_flex fieldset_col smart-filter_fieldset_filter" id="fieldset-filter">
                <div class="justmobile">
                    <div class="forscroll">
                        <a href="javascript:void(0)" class="btn btn-primary btn-show-categories ">
                            <i class="icon icon-catalog-white"></i>
                            <span><?=GetMessage('CT_BCSF_CATALOG_SECTIONS_BUTTON_TITLE')?></span>
                        </a>
                        <div class="forscroll_icon">
                            <i class="icon icon-filter"></i>
                        </div>
                        <div class="forscroll_list">
                            <?
                            foreach($arResult["HIDDEN"] as $arItem)
                            {
                                ?><input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" /><?
                            }

                            foreach ($arResult["ITEMS"] as $key => $arItem)//prices
                            {
                                $key = $arItem["ENCODED_ID"];
                                if (isset($arItem["PRICE"]))
                                {
                                    if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                                        continue;

                                    $step_num = 4;
                                    $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
                                    $prices = array();
                                    if (Bitrix\Main\Loader::includeModule("currency"))
                                    {
                                        for ($i = 0; $i < $step_num; $i++)
                                        {
                                            $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
                                        }
                                        $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
                                    }
                                    else
                                    {
                                        $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
                                        for ($i = 0; $i < $step_num; $i++)
                                        {
                                            $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $precision, ".", "");
                                        }
                                        $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                                    }

                                    include(__DIR__ . "/include/price.php");
                                }
                            }

                            ?>

                            <?


                            //not prices
                            foreach ($arResult["ITEMS"] as $key => $arItem)
                            {
                                if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
                                    continue;

                                if ($arItem["DISPLAY_TYPE"] == "A" && ( $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
                                    continue;

                                ?>

                                <div class="accordion-head filter-block" data-role="horizontal-filter-block">
                                    <a href="javascript:void(0);" class="accordeon-title filter-link">
                                        <span class="text font-large-button"><?=$arItem["NAME"]?></span>
                                        <i class="icon icon-arrow-light-down"></i>
                                    </a>
                                    <div class="accordion-body sub filter-body">
                                        <?
                                        $arCur = current($arItem["VALUES"]);
                                        switch ($arItem["DISPLAY_TYPE"])
                                        {
                                            //region NUMBERS_WITH_SLIDER +
                                            case "A":
                                                include(__DIR__ . "/include/numbers_with_slider.php");
                                                break;

                                            //region NUMBERS +
                                            case "B":
                                                include(__DIR__ . "/include/numbers.php");
                                                break;

                                            //region CHECKBOXES_WITH_PICTURES +
                                            case "G":
                                                include(__DIR__ . "/include/checkboxes_with_pictures.php");
                                                break;

                                            case "H":
                                                include(__DIR__ . "/include/checkboxes_with_pictures_and_labels.php");
                                                break;

                                            //region DROPDOWN +
                                            case "P":
                                            case "R":
                                                include(__DIR__ . "/include/dropdown.php");
                                                break;

                                            //region RADIO_BUTTONS
                                            case "K":
                                                include(__DIR__ . "/include/radio.php");
                                                break;

                                            default:
                                                include(__DIR__ . "/include/checkbox.php");
                                                break;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="smart-filter_btns">
                    <a href="<?=$arResult["FORM_ACTION_PRODS"]?>" class="btn btn-primary font-large-button flex_row">
                        <span>Товаров</span>
                        <span class="element-count" data-role="find-counter"><?=intval($arResult["ELEMENT_COUNT"])?></span>
                        <i class="icon-arrow-right-white icon"></i>
                    </a>
                    <a href="<?=$arResult["FORM_ACTION_COLS"]?>" class="btn btn-primary font-large-button flex_row">
                        <span>Коллекций</span>
                        <span class="element-count" data-role="find-counter-sect"><?=intval($arResult["SECT_COUNT"])?></span>
                        <i class="icon-arrow-right-white icon"></i>
                    </a>
                    <a href="<?=$varclearlink?>" class="btn btn-primary font-large-button flex_row">
                        <span>Сбросить фильтр</span>
                    </a>
                </div>
            </fieldset>
        </form>
        <!--<form name="<?/*echo $arResult["FILTER_NAME"]."_form"*/?>" action="<?/*echo $arResult["FORM_ACTION"]*/?>" method="get" class="smart-filter horizontal nomobile">
            <a href="javascript:void(0)" class="btn btn-primary btn-show-categories ">
                <i class="icon icon-catalog-white"></i>
                <span><?/*=GetMessage('CT_BCSF_CATALOG_SECTIONS_BUTTON_TITLE')*/?></span>
            </a>
                    <i class="icon icon-filter"></i>

                    <?/*
                    foreach($arResult["HIDDEN"] as $arItem)
                    {
                        */?><input type="hidden" name="<?/*echo $arItem["CONTROL_NAME"]*/?>" id="<?/*echo $arItem["CONTROL_ID"]*/?>" value="<?/*echo $arItem["HTML_VALUE"]*/?>" /><?/*
                    }

                    foreach ($arResult["ITEMS"] as $key => $arItem)//prices
                    {
                        $key = $arItem["ENCODED_ID"];
                        if (isset($arItem["PRICE"]))
                        {
                            if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                                continue;

                            $step_num = 4;
                            $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
                            $prices = array();
                            if (Bitrix\Main\Loader::includeModule("currency"))
                            {
                                for ($i = 0; $i < $step_num; $i++)
                                {
                                    $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
                                }
                                $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
                            }
                            else
                            {
                                $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
                                for ($i = 0; $i < $step_num; $i++)
                                {
                                    $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $precision, ".", "");
                                }
                                $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                            }

                            include(__DIR__ . "/include/price.php");
                        }
                    }

                    */?>

                    <?/*
                    //not prices
                    foreach ($arResult["ITEMS"] as $key => $arItem)
                    {
                        if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
                            continue;

                        if ($arItem["DISPLAY_TYPE"] == "A" && ( $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
                            continue;

                        */?>

                        <div class="accordion-head filter-block" data-role="horizontal-filter-block">
                            <a href="javascript:void(0);" class="accordeon-title filter-link">
                                <span class="text font-large-button"><?/*=$arItem["NAME"]*/?></span>
                                <i class="icon icon-arrow-light-down"></i>
                            </a>
                            <div class="accordion-body sub filter-body">
                                <?/*
                                $arCur = current($arItem["VALUES"]);
                                switch ($arItem["DISPLAY_TYPE"])
                                {
                                    //region NUMBERS_WITH_SLIDER +
                                    case "A":
                                        include(__DIR__ . "/include/numbers_with_slider.php");
                                        break;

                                    //region NUMBERS +
                                    case "B":
                                        include(__DIR__ . "/include/numbers.php");
                                        break;

                                    //region CHECKBOXES_WITH_PICTURES +
                                    case "G":
                                        include(__DIR__ . "/include/checkboxes_with_pictures.php");
                                        break;

                                    case "H":
                                        include(__DIR__ . "/include/checkboxes_with_pictures_and_labels.php");
                                        break;

                                    //region DROPDOWN +
                                    case "P":
                                    case "R":
                                        include(__DIR__ . "/include/dropdown.php");
                                        break;

                                    //region RADIO_BUTTONS
                                    case "K":
                                        include(__DIR__ . "/include/radio.php");
                                        break;

                                    default:
                                        include(__DIR__ . "/include/checkbox.php");
                                        break;
                                }
                                */?>
                            </div>
                        </div>
                        <?/*
                    }
                    */?>

            <a href="<?/*=$arResult["FORM_ACTION_PRODS"]*/?>" class="finded-container finded-horizontal font-large-button">
                <div  class="find-link">
                    Товаров
                    <span class="element-count" data-role="find-counter"><?/*=intval($arResult["ELEMENT_COUNT"])*/?></span>
                    <i class="icon-arrow-right-white icon"></i>
                </div>
            </a>
            <a href="<?/*=$arResult["FORM_ACTION_COLS"]*/?>" class="finded-container second finded-horizontal font-large-button">
                <div  class="find-link">
                    Коллекций
                    <span class="element-count" data-role="find-counter-sect"><?/*=intval($arResult["SECT_COUNT"])*/?></span>
                    <i class="icon-arrow-right-white icon"></i>
                </div>
            </a>
            <a href="/catalog/filter/clear/apply/" class="btn btn-primary font-large-button m-sm-3">
                <div  class="find-link">
                    Сбросить фильтр
                </div>
            </a>
        </form>-->
    </div>

    <script src="/js/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.smart-filter.horizontal .filter-block').each(function(item){
                if($(this).position().left + 364 > document.body.clientWidth){
                    $(this).addClass('offset-right');
                }
            });
        });
        BX.message({
            'CT_BCSF_FILTER_COUNT': '<?=GetMessageJS('CT_BCSF_FILTER_COUNT')?>',
            'CT_BCS_FILTER_NO_RESULT': '<?=GetMessageJS('CT_BCS_FILTER_NO_RESULT')?>',
        });

        var smartFilter = new JAlphaFilter(<?=CUtil::PhpToJSObject(Array (
            'CONTAINER_ID' => $containerId,
            'FORM_ACTION' => CUtil::JSEscape($arResult["FORM_ACTION"]),
            'VIEW_MODE' => $_GET['mode']
        ), false, true)?>);

    </script>
    <?
}
?>
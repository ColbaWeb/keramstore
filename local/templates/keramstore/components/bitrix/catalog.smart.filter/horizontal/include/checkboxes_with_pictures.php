<div class="form-check form-check-row">
    <? foreach ($arItem["VALUES"] as $val => $ar): ?>
    <div class="custom-checkbox">
        <input type="checkbox" value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> <?=$ar["DISABLED"] ? 'disabled' : '' ?> class="custom-control-input" id="<?=$ar["CONTROL_ID"] ?>">
        <label class="custom-control-label image" for="<?=$ar["CONTROL_ID"] ?>">
            <span style="background-image: url('<?= $ar["FILE"]["SRC"] ?>');"></span>
        </label>
    </div>
    <? endforeach; ?>

</div>
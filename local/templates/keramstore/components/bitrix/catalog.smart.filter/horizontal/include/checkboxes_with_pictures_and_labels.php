<div class="scrollbar-inner">
    <div class="form-check">
        <? foreach ($arItem["VALUES"] as $val => $ar): ?>
        <div class="custom-checkbox ">
            <input type="checkbox" value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> class="custom-control-input" <?=$ar["DISABLED"] ? 'disabled' : '' ?> id="<?=$ar["CONTROL_ID"] ?>">
            <label class="custom-control-label" for="<?=$ar["CONTROL_ID"] ?>">
                <?
                if(!empty($ar["FILE"]["SRC"]))
                {
                    ?><span class="image" style="background-image: url('<?= $ar["FILE"]["SRC"] ?>');"></span><?
                }
                ?>
                
                <span><?=$ar["VALUE"]?></span>
                
                <? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
                <span class="count font-body-2"><? echo $ar["ELEMENT_COUNT"]; ?></span>
                <? endif; ?>
            </label>
        </div>
        <? endforeach; ?>

    </div>

</div>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var CBitrixComponentTemplate $this
 *  * @var array $arResult
 */

foreach ($arResult["ITEMS"] as $key => $items) {
    if($items["CODE"] == "SIZES") {
        usort($arResult['ITEMS'][$key]['VALUES'], fn($a, $b) => (int)explode("x",$a["VALUE"])[0] <=> (int)explode("x",$b["VALUE"])[0]);
    }
}
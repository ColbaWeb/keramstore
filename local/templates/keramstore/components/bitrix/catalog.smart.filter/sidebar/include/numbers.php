
<div class="range-control">
    <div class="form-row">
        <div class="col"> 
            <input class="min-price form-control font-body-2" type="number" id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>" name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>" placeholder="<?= GetMessage("CT_BCSF_FILTER_FROM") ?>" value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>">
        </div>
        <div class="col"> 
            <input class="max-price form-control font-body-2" type="number" name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>" id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>" placeholder="<?= GetMessage("CT_BCSF_FILTER_TO") ?>" value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>">
        </div>
    </div>

</div>

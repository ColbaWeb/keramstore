
<div class="range-control">
    <div class="form-row">
        <div class="col"> 
            <input class="min-price form-control font-body-2" type="text" id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"] . "_mobile" ?>" name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>" placeholder="<?= GetMessage("CT_BCSF_FILTER_FROM") ?>" value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>" size="5">
        </div>
        <div class="col"> 
            <input class="max-price form-control font-body-2" type="text" name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"] . "_mobile" ?>" id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>" placeholder="<?= GetMessage("CT_BCSF_FILTER_TO") ?>" value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>" size="5">
        </div>
    </div>

</div>

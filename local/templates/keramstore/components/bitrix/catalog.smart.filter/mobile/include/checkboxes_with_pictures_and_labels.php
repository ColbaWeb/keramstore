<div class="form-check">
        <?
        $keyValue = 0;
        foreach ($arItem["VALUES"] as $val => $ar): ?>
        <div class="custom-checkbox<?=$ar['HIDE_ITEM'] == 'Y' ? ' oveflow' : ''?>">
            <input type="checkbox" value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> class="custom-control-input" <?=$ar["DISABLED"] ? 'disabled' : '' ?> id="<?=$ar["CONTROL_ID"] . "_mobile" ?>">
            <label class="custom-control-label" for="<?=$ar["CONTROL_ID"] . "_mobile" ?>">
                <?
                if(!empty($ar["FILE"]["SRC"]))
                {
                    ?><span class="image" style="background-image: url('<?= $ar["FILE"]["SRC"] ?>');"></span><?
                }
                ?>
                
                <span><?=$ar["VALUE"]?></span>
                
                <? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
                <span class="count font-body-2"><? echo $ar["ELEMENT_COUNT"]; ?></span>
                <? endif; ?>
            </label>
        </div>
        <?
        $keyValue++;
        endforeach;
        ?>
</div>


<div class="form-check form-check-row">
    <?
    $keyValue = 1;
    foreach ($arItem["VALUES"] as $val => $ar): ?>
    <div class="custom-checkbox<?=$ar['HIDE_ITEM'] == 'Y' ? ' oveflow' : ''?>">
        <input type="checkbox" value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> <?=$ar["DISABLED"] ? 'disabled' : '' ?> class="custom-control-input" id="<?=$ar["CONTROL_ID"] . "_mobile" ?>">
        <label class="custom-control-label image" for="<?=$ar["CONTROL_ID"] . "_mobile" ?>">
            <span style="background-image: url('<?= $ar["FILE"]["SRC"] ?>');"></span>
        </label>
    </div>
    <? $keyValue++;
    endforeach; ?>

</div>
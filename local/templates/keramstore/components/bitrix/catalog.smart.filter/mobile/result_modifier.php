<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$maxStringLength = 70;
foreach ($arResult["ITEMS"] as $key => $arItem)
{
    if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
    {
        continue;
    }
    
    usort($arItem["VALUES"], function ($a, $b) {
        
        if ($a['SORT'] == $b['SORT'])
            return 0;
        
        return $a['SORT'] < $b['SORT'] ? -1 : 1;
    });
    

    if ($arItem['DISPLAY_EXPANDED'] != "Y" && in_array($arItem["DISPLAY_TYPE"], array ("G", "H", "F")))
    {
        $itemsStringLength = 0;
        foreach ($arItem["VALUES"] as $keyItem => $ar)
        {
            $itemsStringLength += mb_strlen($ar['VALUE']);

            if ($itemsStringLength > $maxStringLength && !isset($ar["CHECKED"]))
            {
                $arItem["VALUES"][$keyItem]['HIDE_ITEM'] = "Y";
                $arItem["HAS_HIDE_ITEMS"] = "Y";
            }
        }
    }
    
    $arResult["ITEMS"][$key] = $arItem;
}

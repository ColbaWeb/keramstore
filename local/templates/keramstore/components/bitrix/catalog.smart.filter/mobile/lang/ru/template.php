<?
$MESS ['CT_BCSF_FILTER_TITLE'] = "Подбор параметров";
$MESS ['CT_BCSF_FILTER_FROM'] = "от";
$MESS ['CT_BCSF_FILTER_TO'] = "до";
$MESS ['CT_BCSF_SET_FILTER'] = "Посмотреть";
$MESS ['CT_BCSF_DEL_FILTER'] = "Сбросить фильтры";
$MESS ['CT_BCSF_FILTER_COUNT'] = "Найдено:";
$MESS ['CT_BCS_FILTER_NO_RESULT'] = "Нет результатов";
$MESS ['CT_BCSF_FILTER_SHOW'] = "Показать";
$MESS ['CT_BCSF_FILTER_ALL'] = "Все";
$MESS ['CT_BCS_FILTER_GOOD'] = "товар";
$MESS ['CT_BCS_FILTER_GOODS_2'] = "товара";
$MESS ['CT_BCS_FILTER_GOODS'] = "товаров";
$MESS ['CT_BCSF_FILTER_SHOW_ALL'] = "Показать все";
$MESS ['CT_BCSF_FILTER_HIDE_ALL'] = "Скрыть";
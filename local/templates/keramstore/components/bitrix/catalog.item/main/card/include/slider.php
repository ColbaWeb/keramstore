<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;


$res = \CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => 5, "ID" => $item["ID"]),
    false,
    false,
    array()
);
if($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $prodcode = $arFields["CODE"];
}

$arFilter = Array('IBLOCK_ID'=>5, 'ID'=>$arFields["IBLOCK_SECTION_ID"]);
$db_list = \CIBlockSection::GetList(Array(), $arFilter, true);
while($ob = $db_list->GetNextElement()) {
    $arFields = $ob->GetFields();
    $secturl = $arFields["SECTION_PAGE_URL"];
}

$urlprod = $secturl.$prodcode."/";

if (strpos($urlprod, "catalogprods") !== false) {
    $urlprod = str_replace("catalogprods","catalog", $urlprod);
    $item['DETAIL_PAGE_URL'] = $urlprod;
}

if ((!empty($item['PICTURES']) && count($item['PICTURES']) == 1) || empty($item['PICTURES']))
{
    $arPicture = !empty($item['PICTURES']) ? end($item['PICTURES']) : array ('SRC' => '');
    ?>
    <div class="image-slider">
        <a href="<?=$urlprod?><?php /*=$item['DETAIL_PAGE_URL']*/?>" class="product-card-link test">
            <?
            if ($arParams['SWIPER_LAZYLOAD'] && $arParams['SWIPER_LAZYLOAD'] == "Y")
            {
                ?><div class="product-image swiper-lazy" data-background="<?=!empty($arPicture['SRC']) ? $arPicture['SRC'] : SITE_TEMPLATE_PATH . "/images/no-image.svg";?>"><?=Layout\Assets::showSpinner(true)?></div><?
            }
            else
            {
                echo Layout\Assets::showBackgroundImage($arPicture['SRC'], array(
                    "attributes" => array(
                        "class=\"product-image\""

                        )
                ), Options\Base::getInstance(SITE_ID)->getValue('imageGrayBackground'));
            }
            ?>
        </a>
    </div>
    <?
}
elseif (!empty($item['PICTURES']) && count($item['PICTURES']) > 1)
{
    ?>
    <div class="image-slider" id="<?=$itemIds['PICT_SLIDER']?>">
        <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
            <?
            foreach ($item['PICTURES'] as $arPicture)
            {
                ?>
                <div class="swiper-slide">
                    <?
                    if ($arParams['SWIPER_LAZYLOAD'] && $arParams['SWIPER_LAZYLOAD'] == "Y")
                    {
                        ?><div class="product-image swiper-lazy" data-background="<?=$arPicture['SRC']?>"><?=Layout\Assets::showSpinner(true)?></div><?
                    }
                    else
                    {
                        echo Layout\Assets::showBackgroundImage($arPicture['SRC'], array(
                            "attributes" => array(
                                "class=\"product-image\""

                                )
                        ), Options\Base::getInstance(SITE_ID)->getValue('imageGrayBackground'));
                    }
                    ?>
                </div>
                <?
            }
            ?>
            </div>
            <div class="swiper-pagination"></div>
        </div>

        <a href="<?=$item['DETAIL_PAGE_URL']?>" class="product-card-link">
            <div class="swiper-container gallery-thumbs">
                <div class="swiper-wrapper">
                    <? foreach ($item['PICTURES'] as $arPicture)
                    {
                        ?><div class="swiper-slide"></div><?
                    }
                    ?>
                </div>

            </div>

        </a>
    </div>
    <?
}
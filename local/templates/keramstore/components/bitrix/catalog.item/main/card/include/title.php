<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

/*echo "<pre>";
print_r($item);
echo "</pre>";*/

$res = \CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => 5, "ID" => $item["ID"]),
    false,
    false,
    array()
);
if($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $prodcode = $arFields["CODE"];
}

$arFilter = Array('IBLOCK_ID'=>5, 'ID'=>$arFields["IBLOCK_SECTION_ID"]);
$db_list = \CIBlockSection::GetList(Array(), $arFilter, true);
while($ob = $db_list->GetNextElement()) {
    $arFields = $ob->GetFields();
    $secturl = $arFields["SECTION_PAGE_URL"];
}

$urlprod = $secturl.$prodcode."/";

if (strpos($urlprod, "catalogprods") !== false) {
    $urlprod = str_replace("catalogprods","catalog", $urlprod);
}
?>

<a href="<?=$urlprod?>" title="<?=$productTitle?>" class="name font-title"><?=$productTitle?></a>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

?>

<div class="button-block" data-entity="buttons-block">
        <?
        if (!$haveOffers)
        {
            if ($actualItem['CAN_BUY'] && $arParams['USE_PRODUCT_QUANTITY'])
            {
                ?>
                <div class="counter" data-entity="quantity-block">
                    <a href="javascript:void(0)" class="minus" id="<?= $itemIds['QUANTITY_DOWN'] ?>"><i class="icon icon-bx-minus"></i></a>
                    <input id="<?= $itemIds['QUANTITY'] ?>" type="number" name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>" value="<?= $measureRatio ?>" >
                    <span class="measure" id="<?= $itemIds['QUANTITY_MEASURE'] ?>"><?= $actualItem['ITEM_MEASURE']['TITLE'] ?></span>
                    <a href="javascript:void(0)" class="plus" id="<?= $itemIds['QUANTITY_UP'] ?>"><i class="icon icon-bx-plus"></i></a>
                </div>
                <?
            }
        
            if ($actualItem['CAN_BUY'] && $item['ITEM_PRICES'][0]['PRICE'] > 0)
            {
                ?>
                <div class="basket-actions" id="<?=$itemIds['BASKET_ACTIONS']?>">
                    <a href="javascript:void(0)" rel="nofollow" id="<?=$itemIds['BUY_LINK']?>" class="btn btn-primary"><?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?></a>
                </div>
                <?
            }
            else
            {
                if (!$item['ITEM_PRICES'][0]['PRICE'] || $item['ITEM_PRICES'][0]['PRICE'] == 0)
                {?>
                    <div class="basket-actions" id="<?=$itemIds['BASKET_ACTIONS']?>">
                    <a href="javascript:void(0)" rel="nofollow" id="<?=$itemIds['BUY_LINK']?>" class="btn btn-primary">По запросу</a>
                </div>
                <?}
                /*if ($showSubscribe || !$item['ITEM_PRICES'][0]['PRICE'])
                {
                    $APPLICATION->IncludeComponent(
                        'bitrix:catalog.product.subscribe',
                        'main',
                        array(
                            'PRODUCT_ID' => $actualItem['ID'],
                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                            'BUTTON_CLASS' => 'btn btn-primary',
                            'DEFAULT_DISPLAY' => true,
                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    );
                }*/
            }
        }
        else
        {
            ?><a href="<?=$item['DETAIL_PAGE_URL']?>" class="btn btn-primary"><?=$arParams['MESS_BTN_DETAIL']?></a><?
        }
        ?>
        
    </div>
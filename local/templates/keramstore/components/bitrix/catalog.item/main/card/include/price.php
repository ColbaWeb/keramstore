<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

?>
<div class="price-block" data-entity="price-block">
        <div class="pricetext">Цена: </div>
        <div class="price font-body" id="<?=$itemIds['PRICE']?>">
            <?
/*            echo "<pre>";
            print_r($price);
            echo "</pre>";*/
            if (!empty($price) && $price["RATIO_BASE_PRICE"] > 0)
            {
                if ($haveOffers && $actualItem['OFFERS_RANGE_PRICES'])
                {
                    echo Loc::getMessage(
                            'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                            array(
                                '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                '#VALUE#' => $measureRatio,
                                '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                            )
                    );
                }
                else
                {
                    echo $price['PRINT_RATIO_PRICE'];
                }
            } else {
                //echo "По запросу";
            }
            ?>
        </div>
        <?
        if ($arParams['SHOW_OLD_PRICE'] === 'Y')
        {
            ?><div class="old-price font-body-small"><span <?=($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '')?> id="<?=$itemIds['PRICE_OLD']?>"><?=$price['PRINT_RATIO_BASE_PRICE']?></span></div><?
        }

        if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
        {
            ?><div class="sale label action" <?=($price['PERCENT'] > 0 ? '' : 'style="display: none;"')?>><span><?=-$price['PERCENT']?>%</span></div><?
        }
        ?>
        
    </div>
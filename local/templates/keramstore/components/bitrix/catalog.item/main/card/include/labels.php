<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

if ($item['LABEL'])
    {

        ?>
        <div class="labels <?=$labelPositionClass?>" id="<?=$itemIds['STICKER_ID']?>">
                <?
                if (!empty($item['LABEL_ARRAY_VALUE']))
                {
                    foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value)
                    {
                        if (isset($item['PROPERTIES'][$code]['VALUE']) && !empty($item['PROPERTIES'][$code]['VALUE']))
                        {
                            foreach ($item['PROPERTIES'][$code]['VALUE'] as $labelKey => $labelValue)
                            {
                                ?><div class="label <?=$item['PROPERTIES'][$code]['VALUE_XML_ID'][$labelKey]?><?= (!isset($item['LABEL_PROP_MOBILE'][$code]) ? ' d-none' : '') ?>"><?= $labelValue ?></div><?
                            }
                        }
                    }
                }
                ?>
        </div>
        <?
    }
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

if (Options\Base::getInstance(SITE_ID)->getValue('wishList') && ($actualItem['CAN_BUY'] || !Options\Base::getInstance(SITE_ID)->getValue('wishListBasketEnabled')))
{
    ?>
        <div class="icon-block<?= $arParams['IS_WISHLIST'] == "Y" ? ' icon-block-wish' : ''; ?>">
            <a href="javascript:void(0)" id="<?=$itemIds['WISHLIST_ID']?>"><i class="icon icon-favorites"></i></a>
        </div>
    <?
}
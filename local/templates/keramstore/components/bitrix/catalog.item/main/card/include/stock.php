<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

if (Options\Base::getInstance(SITE_ID)->getValue('catalogReviews'))
    {
        $reviewsSummary = Reviews\Base::getInstance()->getSummaryByProduct($arResult['ITEM']['ID']);
    }
                
//    if ( ($arParams['SHOW_MAX_QUANTITY'] !== 'N' && $arParams['PRODUCT_DISPLAY_MODE'] === 'Y') || (isset($reviewsSummary['avg']) && $reviewsSummary['avg'] > 0))
    if (false)
    {
    ?>
    <div class="label badge-in-stock">
        <?
        if ($arParams['SHOW_MAX_QUANTITY'] !== 'N' && $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
        {
            if ($actualItem['CAN_BUY'] && $measureRatio)
            {
                ?>
                <div class="in-stock">
                    <?
                    if ($arParams['SHOW_MAX_QUANTITY'] != "M")
                        echo $arParams['MESS_SHOW_MAX_QUANTITY'] . ":";
                    ?>
                    <span>
                    <?
                    if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
                    {
                        if ((float) $actualItem['CATALOG_QUANTITY'] > 0 && (float) $actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
                        {
                            echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
                        }
                        elseif ((float) $actualItem['CATALOG_QUANTITY'] > 0 && (float) $actualItem['CATALOG_QUANTITY'] / $measureRatio < $arParams['RELATIVE_QUANTITY_FACTOR'])
                        {
                            echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
                        }
                        else
                        {
                            echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
                        }
                    }
                    else
                    {
                        echo $actualItem['CATALOG_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'];
                    }
                    ?>
                    </span>
                </div>
                <?
            }
            else
            {
                ?><div class="out-stock"><?= $arParams['MESS_NOT_AVAILABLE'] ?></div><?
            }

        }

        if (isset($reviewsSummary['avg']) && $reviewsSummary['avg'] > 0)
        {
        ?>
            <div class="rating">
                <i class="icon icon-star-12"></i>
                <div class="value"><?= number_format($reviewsSummary['avg'], 1, ",", " ")?></div>
            </div>
        <?
        }
        ?>
    </div>
    <?
    }
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

$arPartialData = array (
    'item' => $item,
    'itemIds' => $itemIds,
    'arParams' => $arParams,
    'arResult' => $arResult,
    'showSubscribe' => $showSubscribe,
    'productTitle' => $productTitle,
    'actualItem' => $actualItem,
    'measureRatio' => $measureRatio,
    'price' => $price,
    'minOffer' => $minOffer,
    'haveOffers' => $haveOffers,
    'component' => $component,
);
/*echo "<pre>";
print_r($item);
echo "</pre>";*/
?>

<div class="product-image-block">
    
    <?
    /*
     * Labels
     */
    Layout\Partial::getInstance(__DIR__)->render('labels.php', $arPartialData, false, false);
    
    /*
     * Stock & rating
     */
    Layout\Partial::getInstance(__DIR__)->render('stock.php', $arPartialData, false, false);
    
    /*
     * Preview picture & slider
     */
    Layout\Partial::getInstance(__DIR__)->render('slider.php', $arPartialData, false, false);
    
    /*
     * Wishlist link
     */
    Layout\Partial::getInstance(__DIR__)->render('wishlist.php', $arPartialData, false, false);

    
    ?>

</div>
<div class="product-info">
    <?
    /*
 * Product title
 */
    Layout\Partial::getInstance(__DIR__)->render('title.php', $arPartialData, false, false);

    /*
 * Product property
 */
    Layout\Partial::getInstance(__DIR__)->render('property.php', $arPartialData, false, false);


    /*
     * Prices
     */
    Layout\Partial::getInstance(__DIR__)->render('price.php', $arPartialData, false, false);

    /*
 * Buttons area
 */
    Layout\Partial::getInstance(__DIR__)->render('buttons.php', $arPartialData, false, false);
    ?>

</div>

<?
$arPicture = array_shift($item['PICTURES']);

$jsParams = array(
    'PRODUCT_TYPE' => $item['PRODUCT']['TYPE'],
    'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
    'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
    'VIEW_MODE' => $arResult['TYPE'],
    'IS_WISHLIST' => $arParams['IS_WISHLIST'],
    'USE_SUBSCRIBE' => $showSubscribe,
    'PRODUCT' => array(
        'ID' => $item['ID'],
        'NAME' => $productTitle,
        'DETAIL_PAGE_URL' => $item['DETAIL_PAGE_URL'],
        'CAN_BUY' => $item['CAN_BUY'],
        'CHECK_QUANTITY' => $item['CHECK_QUANTITY'],
        'MAX_QUANTITY' => $item['CATALOG_QUANTITY'],
        'STEP_QUANTITY' => $item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
        'QUANTITY_FLOAT' => is_float($item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
        'ITEM_PRICE_MODE' => $item['ITEM_PRICE_MODE'],
        'ITEM_PRICES' => $item['ITEM_PRICES'],
        'ITEM_PRICE_SELECTED' => $item['ITEM_PRICE_SELECTED'],
        'ITEM_QUANTITY_RANGES' => $item['ITEM_QUANTITY_RANGES'],
        'ITEM_QUANTITY_RANGE_SELECTED' => $item['ITEM_QUANTITY_RANGE_SELECTED'],
        'ITEM_MEASURE_RATIOS' => $item['ITEM_MEASURE_RATIOS'],
        'ITEM_MEASURE_RATIO_SELECTED' => $item['ITEM_MEASURE_RATIO_SELECTED'],
        'PICTURE' => isset($arPicture['SRC']) ? $arPicture['SRC'] : '',
    ),
    'BASKET' => array(
        'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
        'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
        'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
        'EMPTY_PROPS' => empty($item['PRODUCT_PROPERTIES']),
        'BASKET_URL' => $arParams['~BASKET_URL'],
        'ADD_URL_TEMPLATE' => $arParams['~ADD_URL_TEMPLATE'],
        'BUY_URL_TEMPLATE' => $arParams['~BUY_URL_TEMPLATE'],
        'WISHLIST_PAGE_URL' => $arParams['WISHLIST_PAGE_URL'],
        'WISHLIST_URL_TEMPLATE' => Tools\Wishlist::getChangeUrl(Options\Base::getInstance(SITE_ID)->getValue('wishListBasketEnabled') ? $actualItem['ID'] : $item['ID'])
    ),
    'VISUAL' => array(
        'ID' => $itemIds['ID'],
        'PRICE' => $itemIds['PRICE'],
        'PRICE_ID' => $itemIds['PRICE'],
        'PRICE_OLD_ID' => $itemIds['PRICE_OLD'],
        'PRICE_TOTAL_ID' => $itemIds['PRICE_TOTAL'],
        'QUANTITY_ID' => $itemIds['QUANTITY'],
        'QUANTITY_UP_ID' => $itemIds['QUANTITY_UP'],
        'QUANTITY_DOWN_ID' => $itemIds['QUANTITY_DOWN'],
        'BUY_ID' => $itemIds['BUY_LINK'],
        'BASKET_PROP_DIV' => $itemIds['BASKET_PROP_DIV'],
        'BASKET_ACTIONS_ID' => $itemIds['BASKET_ACTIONS'],
        'COMPARE_LINK_ID' => $itemIds['COMPARE_LINK'],
        'SUBSCRIBE_ID' => $itemIds['SUBSCRIBE_LINK'],
        'PICT_SLIDER' => $itemIds['PICT_SLIDER'],
        'WISHLIST_ID' => $itemIds['WISHLIST_ID'],
    )
);

if ($arParams['DISPLAY_COMPARE']) {
    $jsParams['COMPARE'] = array(
        'COMPARE_URL_TEMPLATE' => $arParams['~COMPARE_URL_TEMPLATE'],
        'COMPARE_DELETE_URL_TEMPLATE' => $arParams['~COMPARE_DELETE_URL_TEMPLATE'],
        'COMPARE_PATH' => $arParams['COMPARE_PATH']
    );
}

if ($item['BIG_DATA']) {
    $jsParams['PRODUCT']['RCM_ID'] = $item['RCM_ID'];
}

$jsParams['PRODUCT_DISPLAY_MODE'] = $arParams['PRODUCT_DISPLAY_MODE'];
$jsParams['USE_ENHANCED_ECOMMERCE'] = $arParams['USE_ENHANCED_ECOMMERCE'];
$jsParams['DATA_LAYER_NAME'] = $arParams['DATA_LAYER_NAME'];

$templateData = array(
    'JS_OBJ' => $obName,
    'ITEM' => array(
        'ID' => $actualItem['ID'],
        'PRODUCT_ID' => $item['ID'],
        'IBLOCK_ID' => $actualItem['IBLOCK_ID'],
        'OFFERS_SELECTED' => $item['OFFERS_SELECTED'],
    )
);
?>
<script>
    BX.ready(function () {
        var interval = setInterval(function () {
            if (!!window.BX.Currency)
            {
                window.<?= $obName ?> = new window.Alpha.CatalogItem(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
                clearTimeout(interval);
            }
        }, 1000);
    });
</script>

<?
unset($arPartialData, $jsParams, $item, $arParams);

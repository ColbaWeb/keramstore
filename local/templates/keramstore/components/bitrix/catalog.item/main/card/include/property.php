<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");

$fabric = $arResult["ITEM"]["PROPERTIES"]["FACTORY"]["VALUE"];
$collection = $arResult["ITEM"]["PROPERTIES"]["COLLECTION"]["VALUE"];
$sectionid = $arResult["ITEM"]["IBLOCK_SECTION_ID"];

/*echo "<pre>";
print_r($arResult["ITEM"]["PROPERTIES"]["FACTORY"]["VALUE"]);
echo "</pre>";*/

$fabric = trim($arResult["ITEM"]["PROPERTIES"]["FACTORY"]["VALUE"]);

$res123 = \CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => 16, "NAME" => $fabric),
    false,
    false,
    array('IBLOCK_ID', 'ID', 'NAME', 'DETAIL_PAGE_URL')
);
if($ob123 = $res123->GetNextElement()) {
    $arFields2 = $ob123->GetFields();
    $fabricname = $arFields2["NAME"];
    $fabricurl = $arFields2["DETAIL_PAGE_URL"];
}

/*$arFilter = Array('IBLOCK_ID'=>5, 'NAME'=>$collection);
$db_list = \CIBlockSection::GetList(Array(), $arFilter, true);
while($ob = $db_list->GetNext()) {
    //$collectname = $ob["NAME"];
    $collectcurl = $ob["SECTION_PAGE_URL"];
}*/
?>
<div class="property-block" data-entity="property-block">
    <div class="fabric">Фабрика: <a href="<?=$fabricurl?>"><?=$fabric?></a></div>
    <!--<div class="fabric">Коллекция: <a href="<?/*=$collectcurl*/?>"><?/*=$collection*/?></a></div>-->
    <div><?=$arResult["ITEM"]["PROPERTIES"]["TYPE_TILE"]["NAME"]?>: <?=$arResult["ITEM"]["PROPERTIES"]["TYPE_TILE"]["VALUE"]?></div>
    <div><?=$arResult["ITEM"]["PROPERTIES"]["ARTICLE"]["NAME"]?>: <?=$arResult["ITEM"]["PROPERTIES"]["ARTICLE"]["VALUE"]?></div>
    <div><?=$arResult["ITEM"]["PROPERTIES"]["SURFACE"]["NAME"]?>: <?=$arResult["ITEM"]["PROPERTIES"]["SURFACE"]["VALUE"]?></div>
</div>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;


if (!$haveOffers)
{
    if ($showPropsBlock && $showDisplayProps)
    {
?>
                        <div class="chars-block">
                            <div class="chars" data-entity="props-block">
                                <div class="props">
        <?
        foreach ($item['DISPLAY_PROPERTIES'] as $code => $displayProperty)
        {
        ?>
                                            <div itemprop="additionalProperty" itemscope="" itemtype="http://schema.org/PropertyValue" class="char">
                                                <div class="char-name">
                                                    <span>
                                                        <span itemprop="name"><?= $displayProperty['NAME'] ?></span>
                                                    </span>
                                                </div>
                                                <div class="char-value">
                                                    <span itemprop="value"><?=
            (is_array($displayProperty['DISPLAY_VALUE']) ? implode(' / ', $displayProperty['DISPLAY_VALUE']) : $displayProperty['DISPLAY_VALUE'])
        ?></span>
                                                </div>
                                            </div>
            <?
        }
            ?>
                                </div>
                            </div>
                            <a href="javascript:void(0)" onclick="$(this).siblings('.chars').toggleClass('opened');" class="chars-head font-large-button">
        <?= Loc::getMessage('CT_BCI_TPL_PROPS_TITLE') ?>
                                <i class="icon icon-arrow-down-text-button "></i>
                            </a>
                        </div>
        <?
    }
}
else
{
    if ($showPropsBlock)
    {
        ?>
                                <div class="chars-block">
                                    <div class="chars">
                                        <div class="props" data-entity="props-block">
        <?
        if ($showDisplayProps)
        {
            foreach ($item['DISPLAY_PROPERTIES'] as $code => $displayProperty)
            {
        ?>
                                                        <div itemprop="additionalProperty" itemscope="" itemtype="http://schema.org/PropertyValue" class="char">
                                                            <div class="char-name">
                                                                <span>
                                                                    <span itemprop="name"><?= $displayProperty['NAME'] ?></span>
                                                                </span>
                                                            </div>
                                                            <div class="char-value">
                                                                <span itemprop="value"><?=
                (is_array($displayProperty['DISPLAY_VALUE']) ? implode(' / ', $displayProperty['DISPLAY_VALUE']) : $displayProperty['DISPLAY_VALUE'])
        ?></span>
                                                            </div>
                                                        </div>
                <?
            }
        }
                ?>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" onclick="$(this).siblings('.chars').toggleClass('opened');" class="chars-head font-large-button">
        <?= Loc::getMessage('CT_BCI_TPL_PROPS_TITLE') ?>
                                        <i class="icon icon-arrow-down-text-button "></i>
                                    </a>
                                </div>
        <?
    }

    if ($showSkuBlock)
    {
        ?>
                            <div id="<?= $itemIds['PROP_DIV'] ?>">
        <?
        foreach ($arParams['SKU_PROPS'] as $skuProperty)
        {
            $propertyId = $skuProperty['ID'];
            $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
            if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                continue;
        ?>
                                            <div class="sku-list<?= $skuProperty['SHOW_MODE'] === 'PICT' ? ' images' : '' ?>" data-entity="sku-block">
                                                    <div class="title font-body-small"><?= $skuProperty['NAME'] ?></div>
                                                    <div class="list" data-entity="sku-line-block">
            <?
            foreach ($skuProperty['VALUES'] as $value)
            {
                if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                    continue;

                $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                if ($skuProperty['SHOW_MODE'] === 'PICT')
                {
            ?>
                                                                            <a href="javascript:void(0)" title="<?= $value['NAME'] ?>" class="item sku-element image" data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>" data-onevalue="<?= $value['ID'] ?>">
                                                                                <div style="background-image:url('<?= $value['PICT']['SRC'] ?>')"></div>
                                                                            </a>
                    <?
                }
                else
                {
                    ?>
                                                                        <a href="javascript:void(0)" class="item sku-element" data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>" data-onevalue="<?= $value['ID'] ?>"><?= $value['NAME'] ?></a>
                    <?
                }
            }
                    ?>
                                                    </div>
                                                </div>
            <?
        }
            ?>
                            </div>
        <?
        foreach ($arParams['SKU_PROPS'] as $skuProperty)
        {
            if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                continue;

            $skuProps[] = array(
                'ID' => $skuProperty['ID'],
                'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                'VALUES' => $skuProperty['VALUES'],
                'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
            );
        }

        unset($skuProperty, $value);

        if ($item['OFFERS_PROPS_DISPLAY'])
        {
            foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer)
            {
                $strProps = '';

                if (!empty($jsOffer['DISPLAY_PROPERTIES']))
                {
                    foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty)
                    {
                        $strProps .= ''
                                . '<div itemprop="additionalProperty" itemscope="" itemtype="http://schema.org/PropertyValue" class="char">'
                                . '<div class="char-name"><span><span itemprop="name">' . $displayProperty['NAME'] . '</span></span></div>'
                                . '<div class="char-value"><span itemprop="value">' . (is_array($displayProperty['VALUE']) ? implode(' / ', $displayProperty['VALUE']) : $displayProperty['VALUE']) . '</span></div>'
                                . '</div>';
                    }
                }
                $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
            }
            unset($jsOffer, $strProps);
        }
    }
}
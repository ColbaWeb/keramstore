<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;
?>

<a href="<?=$item['DETAIL_PAGE_URL']?>" class="image-slider" id="<?=$itemIds['PICT_SLIDER']?>">
            <?
            if ((!empty($item['PICTURES']) && count($item['PICTURES']) == 1) || empty($item['PICTURES']))
            {
                $arPicture = !empty($item['PICTURES']) ? end($item['PICTURES']) : array ('SRC' => '');
                echo Layout\Assets::showImage($arPicture['SRC'], array("attributes" => array("class=\"product-image\"")));
            }
            elseif (!empty($item['PICTURES']) && count($item['PICTURES']) > 1)
            {
                ?>
                <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                    <?
                    foreach ($item['PICTURES'] as $arPicture)
                    {
                        ?>
                        <div class="swiper-slide">
                            <?
                            echo Layout\Assets::showImage($arPicture['SRC'], array(
                                "attributes" => array(
                                    "class=\"product-image\""

                                    )
                            ));
                            ?>
                        </div>
                        <?
                    }
                    ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>

                    <div class="swiper-container gallery-thumbs">
                        <div class="swiper-wrapper">
                            <? foreach ($item['PICTURES'] as $arPicture)
                            {
                                ?><div class="swiper-slide"></div><?
                            }
                            ?>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>

                <?
            }
            ?>
        </a>
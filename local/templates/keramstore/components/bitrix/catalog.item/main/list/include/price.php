<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

?>

<div class="price-block" data-entity="price-block">
    <div class="price font-price-medium" id="<?= $itemIds['PRICE'] ?>">
        <?
        if (!empty($price))
        {
            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
            {
                echo Loc::getMessage(
                        'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                        array(
                            '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                            '#VALUE#' => $measureRatio,
                            '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                        )
                );
            }
            else
            {
                echo $price['PRINT_RATIO_PRICE'];
            }
        }
        ?>
    </div>

    <div class="price-economy">
        <?
        if ($arParams['SHOW_OLD_PRICE'] === 'Y')
        {
            ?>
            <div class="font-body-small price-old-block">
                <span class="price-old" id="<?= $itemIds['PRICE_OLD'] ?>"<?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? ' style="display: none;"' : '') ?>>
                    <?= $price['PRINT_RATIO_BASE_PRICE'] ?>
                </span>
            </div>
            <?
        }

        if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
        {
            ?>
            <div class="labels">
                <span class="label action" id="<?= $itemIds['DSC_PERC'] ?>" <?= ($price['PERCENT'] > 0 ? '' : 'style="display: none;"') ?>><?= -$price['PERCENT'] ?>%</span>
            </div>
            <?
        }
        ?>
    </div>

    <?
    if (Options\Base::getInstance(SITE_ID)->getValue('buyoneclickActive'))
    {
        ?><a href="javascript:void(0)" onclick="window.Alpha.Popups.openAjaxContent({url: '<?= SITE_DIR ?>include/ajax/buy_one_click.php?id=<?= $actualItem['ID'] ?>'})" class="one-click font-button-large"><?= Loc::getMessage('CT_BCI_TPL_MESS_BUY_ONE_CLICK') ?></a><?
    }
    ?>

</div>
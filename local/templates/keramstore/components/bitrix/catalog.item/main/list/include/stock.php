<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

?>
<div class="in-stock">
    <?
    if ($arParams['SHOW_MAX_QUANTITY'] !== 'N')
    {
        if ($haveOffers)
        {
            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
            {
                ?>
                <span id="<?= $itemIds['QUANTITY_LIMIT'] ?>" style="display: none;" data-entity="quantity-limit-block">
                    <?
                    if ($arParams['SHOW_MAX_QUANTITY'] != "M")
                        echo $arParams['MESS_SHOW_MAX_QUANTITY'] . ":";
                    ?>
                    <span data-entity="quantity-limit-value"></span>
                </span>
                <span id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>" <?= ($actualItem['CAN_BUY'] ? 'style="display: none;"' : '') ?>>
                    <?= $arParams['MESS_NOT_AVAILABLE'] ?>
                </span>
                <?
            }
        }
        else
        {
            if ($measureRatio && (float) $actualItem['CATALOG_QUANTITY'] > 0 && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y' && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N')
            {
                ?>
                <span id="<?= $itemIds['QUANTITY_LIMIT'] ?>">
                    <?
                    if ($arParams['SHOW_MAX_QUANTITY'] != "M")
                        echo $arParams['MESS_SHOW_MAX_QUANTITY'] . ":";
                    ?>
                    <span data-entity="quantity-limit-value">
                        <?
                        if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
                        {
                            if ((float) $actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
                            {
                                echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
                            }
                            else
                            {
                                echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
                            }
                        }
                        else
                        {
                            echo $actualItem['CATALOG_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'];
                        }
                        ?>
                    </span>
                </span>
                <?
            }
            else
            {
                ?><div class="out-stock"><?= $arParams['MESS_NOT_AVAILABLE'] ?></div><?
            }
        }
    }
    ?>
</div>
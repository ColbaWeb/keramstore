<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */


if ($haveOffers)
{
    $showDisplayProps = !empty($item['DISPLAY_PROPERTIES']);
    $showProductProps = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $item['OFFERS_PROPS_DISPLAY'];
    $showPropsBlock = $showDisplayProps || $showProductProps;
    $showSkuBlock = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !empty($item['OFFERS_PROP']);
}
else
{
    $showDisplayProps = !empty($item['DISPLAY_PROPERTIES']);
    $showProductProps = $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !empty($item['PRODUCT_PROPERTIES']);
    $showPropsBlock = $showDisplayProps || $showProductProps;
    $showSkuBlock = false;
}

if ($haveOffers)
{
    foreach ($arParams['SKU_PROPS'] as $skuProperty)
        {
            if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                continue;

            $skuProps[] = array(
                'ID' => $skuProperty['ID'],
                'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                'VALUES' => $skuProperty['VALUES'],
                'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
            );
        }

        unset($skuProperty, $value);

        if ($item['OFFERS_PROPS_DISPLAY'])
        {
            foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer)
            {
                $strProps = '';

                if (!empty($jsOffer['DISPLAY_PROPERTIES']))
                {
                    foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty)
                    {
                        $strProps .= ''
                                . '<div itemprop="additionalProperty" itemscope="" itemtype="http://schema.org/PropertyValue" class="char">'
                                . '<div class="char-name"><span><span itemprop="name">' . $displayProperty['NAME'] . '</span></span></div>'
                                . '<div class="char-value"><span itemprop="value">' . (is_array($displayProperty['VALUE']) ? implode(' / ', $displayProperty['VALUE']) : $displayProperty['VALUE']) . '</span></div>'
                                . '</div>';
                    }
                }
                $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
            }
            unset($jsOffer, $strProps);
        }
}

$arPartialData = array (
    'item' => $item,
    'itemIds' => $itemIds,
    'arParams' => $arParams,
    'arResult' => $arResult,
    'showSubscribe' => $showSubscribe,
    'productTitle' => $productTitle,
    'actualItem' => $actualItem,
    'measureRatio' => $measureRatio,
    'price' => $price,
    'minOffer' => $minOffer,
    'showDisplayProps' => $showDisplayProps,
    'showProductProps' => $showProductProps,
    'showPropsBlock' => $showPropsBlock,
    'showSkuBlock' => $showSkuBlock,
    'haveOffers' => $haveOffers,
    'component' => $component
);

?>

<div class="product-card-list">
    <div class="img-wrap">
        <div class="label badge-in-stock">
            
            <?
            /*
             * Stock
             */
            Layout\Partial::getInstance(__DIR__)->render('stock.php', $arPartialData, false, false);
            
            /*
             * Rating
             */
            Layout\Partial::getInstance(__DIR__)->render('rating.php', $arPartialData, false, false);
            
            ?>

        </div>
        
        <?
        /*
         * Slider
         */
        Layout\Partial::getInstance(__DIR__)->render('slider.php', $arPartialData, false, false);
        
        /*
         * Wishlist
         */
        Layout\Partial::getInstance(__DIR__)->render('wishlist.php', $arPartialData, false, false);
        
        ?>
        
    </div>
    <div class="info">
        <div class="item-col">
            <div class="title-block">
                
                <?
                /*
                 * Product title
                 */
                Layout\Partial::getInstance(__DIR__)->render('title.php', $arPartialData, false, false);
                
                /*
                 * Labels
                 */
                Layout\Partial::getInstance(__DIR__)->render('labels.php', $arPartialData, false, false);

                ?>
            </div>
            
            <?
            
            /*
             * Preview text
             */
            Layout\Partial::getInstance(__DIR__)->render('preview_text.php', $arPartialData, false, false);
            
            /*
             * Properties
             */
            Layout\Partial::getInstance(__DIR__)->render('props.php', $arPartialData, false, false);

            ?>

        </div>

        <div class="item-col">
            
            <?
            /*
             * Prices
             */
            Layout\Partial::getInstance(__DIR__)->render('price.php', $arPartialData, false, false);
            ?>
            
            <div class="buttons">
                
                <?
                /*
                 * Buttons
                 */
                Layout\Partial::getInstance(__DIR__)->render('buttons.php', $arPartialData, false, false);
                ?>

            </div>
        </div>
    </div>
</div>

<?
if (!$haveOffers)
{
    $jsParams = array(
        'PRODUCT_TYPE' => $item['PRODUCT']['TYPE'],
        'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
        'SHOW_ADD_BASKET_BTN' => false,
        'SHOW_BUY_BTN' => true,
        'SHOW_ABSENT' => true,
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
        'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
        'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
        'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
        'BIG_DATA' => $item['BIG_DATA'],
        'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
        'VIEW_MODE' => $arResult['TYPE'],
        'USE_SUBSCRIBE' => $showSubscribe,
        'PRODUCT' => array(
            'ID' => $item['ID'],
            'NAME' => $productTitle,
            'DETAIL_PAGE_URL' => $item['DETAIL_PAGE_URL'],
            'PICT' => $item['SECOND_PICT'] ? $item['PREVIEW_PICTURE_SECOND'] : $item['PREVIEW_PICTURE'],
            'CAN_BUY' => $item['CAN_BUY'],
            'CHECK_QUANTITY' => $item['CHECK_QUANTITY'],
            'MAX_QUANTITY' => $item['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
            'QUANTITY_FLOAT' => is_float($item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
            'ITEM_PRICE_MODE' => $item['ITEM_PRICE_MODE'],
            'ITEM_PRICES' => $item['ITEM_PRICES'],
            'ITEM_PRICE_SELECTED' => $item['ITEM_PRICE_SELECTED'],
            'ITEM_QUANTITY_RANGES' => $item['ITEM_QUANTITY_RANGES'],
            'ITEM_QUANTITY_RANGE_SELECTED' => $item['ITEM_QUANTITY_RANGE_SELECTED'],
            'ITEM_MEASURE_RATIOS' => $item['ITEM_MEASURE_RATIOS'],
            'ITEM_MEASURE_RATIO_SELECTED' => $item['ITEM_MEASURE_RATIO_SELECTED'],
            'MORE_PHOTO' => $item['MORE_PHOTO'],
            'MORE_PHOTO_COUNT' => $item['MORE_PHOTO_COUNT']
        ),
        'BASKET' => array(
            'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => empty($item['PRODUCT_PROPERTIES']),
            'BASKET_URL' => $arParams['~BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arParams['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arParams['~BUY_URL_TEMPLATE'],
            'WISHLIST_PAGE_URL' => $arParams['WISHLIST_PAGE_URL'],
            'ADD_WISH_TO_DELAYED' => Options\Base::getInstance(SITE_ID)->getValue('wishListBasketEnabled') ? "Y" : "N",
            'WISHLIST_URL_TEMPLATE' => Tools\Wishlist::getChangeUrl()
        ),
        'VISUAL' => array(
            'ID' => $itemIds['ID'],
            'PICT_ID' => $item['SECOND_PICT'] ? $itemIds['SECOND_PICT'] : $itemIds['PICT'],
            'PICT_SLIDER_ID' => $itemIds['PICT_SLIDER'],
            'QUANTITY_ID' => $itemIds['QUANTITY'],
            'QUANTITY_UP_ID' => $itemIds['QUANTITY_UP'],
            'QUANTITY_DOWN_ID' => $itemIds['QUANTITY_DOWN'],
            'PRICE_ID' => $itemIds['PRICE'],
            'PRICE_OLD_ID' => $itemIds['PRICE_OLD'],
            'PRICE_TOTAL_ID' => $itemIds['PRICE_TOTAL'],
            'BUY_ID' => $itemIds['BUY_LINK'],
            'BASKET_PROP_DIV' => $itemIds['BASKET_PROP_DIV'],
            'BASKET_ACTIONS_ID' => $itemIds['BASKET_ACTIONS'],
            'NOT_AVAILABLE_MESS' => $itemIds['NOT_AVAILABLE_MESS'],
            'COMPARE_LINK_ID' => $itemIds['COMPARE_LINK'],
            'SUBSCRIBE_ID' => $itemIds['SUBSCRIBE_LINK'],
            'ITEM_CODE_ID' => $itemIds['ITEM_CODE_ID'],
            'WISHLIST_ID' => $itemIds['WISHLIST_ID']
        )
    );
}
else
{
    $jsParams = array(
        'PRODUCT_TYPE' => $item['PRODUCT']['TYPE'],
        'SHOW_QUANTITY' => false,
        'SHOW_ADD_BASKET_BTN' => false,
        'SHOW_BUY_BTN' => true,
        'SHOW_ABSENT' => true,
        'SHOW_SKU_PROPS' => false,
        'SECOND_PICT' => $item['SECOND_PICT'],
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
        'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
        'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
        'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
        'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
        'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
        'BIG_DATA' => $item['BIG_DATA'],
        'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
        'VIEW_MODE' => $arResult['TYPE'],
        'USE_SUBSCRIBE' => $showSubscribe,
        'DEFAULT_PICTURE' => array(
            'PICTURE' => $item['PRODUCT_PREVIEW'],
            'PICTURE_SECOND' => $item['PRODUCT_PREVIEW_SECOND']
        ),
        'VISUAL' => array(
            'ID' => $itemIds['ID'],
            'PICT_ID' => $itemIds['PICT'],
            'SECOND_PICT_ID' => $itemIds['SECOND_PICT'],
            'PICT_SLIDER_ID' => $itemIds['PICT_SLIDER'],
            'QUANTITY_ID' => $itemIds['QUANTITY'],
            'QUANTITY_UP_ID' => $itemIds['QUANTITY_UP'],
            'QUANTITY_DOWN_ID' => $itemIds['QUANTITY_DOWN'],
            'QUANTITY_MEASURE' => $itemIds['QUANTITY_MEASURE'],
            'QUANTITY_LIMIT' => $itemIds['QUANTITY_LIMIT'],
            'PRICE_ID' => $itemIds['PRICE'],
            'PRICE_OLD_ID' => $itemIds['PRICE_OLD'],
            'PRICE_TOTAL_ID' => $itemIds['PRICE_TOTAL'],
            'TREE_ID' => $itemIds['PROP_DIV'],
            'TREE_ITEM_ID' => $itemIds['PROP'],
            'BUY_ID' => $itemIds['BUY_LINK'],
            'DSC_PERC' => $itemIds['DSC_PERC'],
            'SECOND_DSC_PERC' => $itemIds['SECOND_DSC_PERC'],
            'DISPLAY_PROP_DIV' => $itemIds['DISPLAY_PROP_DIV'],
            'BASKET_ACTIONS_ID' => $itemIds['BASKET_ACTIONS'],
            'NOT_AVAILABLE_MESS' => $itemIds['NOT_AVAILABLE_MESS'],
            'COMPARE_LINK_ID' => $itemIds['COMPARE_LINK'],
            'SUBSCRIBE_ID' => $itemIds['SUBSCRIBE_LINK'],
            'ITEM_CODE_ID' => $itemIds['ITEM_CODE_ID'],
            'WISHLIST_ID' => $itemIds['WISHLIST_ID']
        ),
        'BASKET' => array(
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'SKU_PROPS' => $item['OFFERS_PROP_CODES'],
            'BASKET_URL' => $arParams['~BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arParams['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arParams['~BUY_URL_TEMPLATE'],
            'WISHLIST_PAGE_URL' => $arParams['WISHLIST_PAGE_URL'],
            'ADD_WISH_TO_DELAYED' => Options\Base::getInstance(SITE_ID)->getValue('wishListBasketEnabled') ? "Y" : "N",
            'WISHLIST_URL_TEMPLATE' => Tools\Wishlist::getChangeUrl()
        ),
        'PRODUCT' => array(
            'ID' => $item['ID'],
            'NAME' => $productTitle,
            'DETAIL_PAGE_URL' => $item['DETAIL_PAGE_URL'],
            'MORE_PHOTO' => $item['MORE_PHOTO'],
            'MORE_PHOTO_COUNT' => $item['MORE_PHOTO_COUNT']
        ),
        'OFFERS' => array(),
        'OFFER_SELECTED' => 0,
        'TREE_PROPS' => array()
    );

    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !empty($item['OFFERS_PROP']))
    {
        $jsParams['SHOW_QUANTITY'] = $arParams['USE_PRODUCT_QUANTITY'];
        $jsParams['SHOW_SKU_PROPS'] = $item['OFFERS_PROPS_DISPLAY'];
        $jsParams['OFFERS'] = $item['JS_OFFERS'];
        $jsParams['OFFER_SELECTED'] = $item['OFFERS_SELECTED'];
        $jsParams['TREE_PROPS'] = $skuProps;
    }
}

if ($arParams['DISPLAY_COMPARE'])
{
    $jsParams['COMPARE'] = array(
        'COMPARE_URL_TEMPLATE' => $arParams['~COMPARE_URL_TEMPLATE'],
        'COMPARE_DELETE_URL_TEMPLATE' => $arParams['~COMPARE_DELETE_URL_TEMPLATE'],
        'COMPARE_PATH' => $arParams['COMPARE_PATH']
    );
}

if ($item['BIG_DATA'])
{
    $jsParams['PRODUCT']['RCM_ID'] = $item['RCM_ID'];
}

$jsParams['PRODUCT_DISPLAY_MODE'] = $arParams['PRODUCT_DISPLAY_MODE'];
$jsParams['USE_ENHANCED_ECOMMERCE'] = $arParams['USE_ENHANCED_ECOMMERCE'];
$jsParams['DATA_LAYER_NAME'] = $arParams['DATA_LAYER_NAME'];
$jsParams['BRAND_PROPERTY'] = !empty($item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]) ? $item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE'] : null;

$templateData = array(
    'TYPE' => $arResult['TYPE'],
    'JS_OBJ' => $obName,
    'ITEM' => array(
        'ID' => $item['ID'],
        'PRODUCT_ID' => $item['ID'],
        'IBLOCK_ID' => $item['IBLOCK_ID'],
        'OFFERS_SELECTED' => $item['OFFERS_SELECTED'],
        'JS_OFFERS' => $item['JS_OFFERS']
    )
);
?>
<script>
    BX.ready(function () {
        var interval = setInterval(function () {
            if (!!window.BX.Currency)
            {
                window.<?= $obName ?> = new window.Alpha.CatalogListItem(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
                clearTimeout(interval);
            }
        }, 1000);
    });
    
</script>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;
?>

<div class="labels <?= $labelPositionClass ?>">
    <div id="<?= $itemIds['STICKER_ID'] ?>">
        <?
        if (!empty($item['LABEL_ARRAY_VALUE']))
        {
            foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value)
            {
                if (isset($item['PROPERTIES'][$code]['VALUE']) && !empty($item['PROPERTIES'][$code]['VALUE']))
                {
                    foreach ($item['PROPERTIES'][$code]['VALUE'] as $labelKey => $labelValue)
                    {
                        ?><div class="label <?= $item['PROPERTIES'][$code]['VALUE_XML_ID'][$labelKey] ?><?= (!isset($item['LABEL_PROP_MOBILE'][$code]) ? ' d-none' : '') ?>"><?= $labelValue ?></div><?
                    }
                }
            }
        }
        ?>
    </div>             
    <?
    if (!$haveOffers && !empty($item['ITEM_CODE']))
    {
        ?>
        <div class="article font-body-small" id="<?= $itemIds['ITEM_CODE_ID'] ?>">
            <span class="article-title"><?= Loc::getMessage('CT_BCI_TPL_MESS_ARTICLE') ?> </span>
            <span class="article-value" data-entity="item-code-value"><?= $item['ITEM_CODE'] ?></span>
        </div>
        <?
    }
    elseif ($haveOffers)
    {
        ?>
        <div class="article font-body-small" <? if (empty($actualItem['ITEM_CODE'])): ?>style="display: none"<? endif; ?> id="<?= $itemIds['ITEM_CODE_ID'] ?>">
            <span class="article-title"><?= Loc::getMessage('CT_BCI_TPL_MESS_ARTICLE') ?> </span>
            <span class="article-value" data-entity="item-code-value"><?= $item['ITEM_CODE'] ?></span>
        </div>        
        <?
    }
    ?>
</div>
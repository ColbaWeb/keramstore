<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

if (!$haveOffers)
{
    if ($actualItem['CAN_BUY'] && $arParams['USE_PRODUCT_QUANTITY'])
    {
        ?>
        <div class="counter" data-entity="quantity-block">
            <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_DOWN'] ?>" class="minus"><i class="icon icon-bx-minus"></i></a>
            <input type="number" id="<?= $itemIds['QUANTITY'] ?>" type="number" name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>" value="<?= $measureRatio ?>">
            <span class="measure" id="<?= $itemIds['QUANTITY_MEASURE'] ?>"><?= $actualItem['ITEM_MEASURE']['TITLE'] ?></span>
            <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_UP'] ?>" class="plus"><i class="icon icon-bx-plus"></i></a>
        </div>
        <?
    }
}
elseif ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
{
    if ($arParams['USE_PRODUCT_QUANTITY'])
    {
        ?>
        <div class="counter" data-entity="quantity-block">
            <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_DOWN'] ?>" class="minus"><i class="icon icon-bx-minus"></i></a>
            <input type="number" id="<?= $itemIds['QUANTITY'] ?>" type="number" name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>" value="<?= $measureRatio ?>">
            <span class="measure" id="<?= $itemIds['QUANTITY_MEASURE'] ?>"><?= $actualItem['ITEM_MEASURE']['TITLE'] ?></span>
            <a href="javascript:void(0)" id="<?= $itemIds['QUANTITY_UP'] ?>" class="plus"><i class="icon icon-bx-plus"></i></a>
        </div>
        <?
    }
}

if (!$haveOffers)
{
    if ($actualItem['CAN_BUY'])
    {
        ?>
        <div class="buttons-wrap" id="<?= $itemIds['BASKET_ACTIONS'] ?>">
            <a href="javascript:void(0)" rel="nofollow" id="<?= $itemIds['BUY_LINK'] ?>" class="btn btn-primary btn-large btn-to-cart">
        <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?> <i class="icon icon-basket-white"></i>
            </a>
        </div>
        <?
    }
    else
    {
        ?>
        <div class="buttons-wrap">
            <?
            if ($showSubscribe)
            {
                $APPLICATION->IncludeComponent(
                        'bitrix:catalog.product.subscribe',
                        'main',
                        array(
                            'PRODUCT_ID' => $actualItem['ID'],
                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                            'BUTTON_CLASS' => 'btn btn-primary btn-large btn-to-cart',
                            'DEFAULT_DISPLAY' => true,
                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                );
            }
            ?>
        </div>
        <?
    }
}
else
{
    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
    {
        if ($showSubscribe)
        {
            $APPLICATION->IncludeComponent(
                    'bitrix:catalog.product.subscribe',
                    'main',
                    array(
                        'PRODUCT_ID' => $actualItem['ID'],
                        'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                        'BUTTON_CLASS' => 'btn btn-primary btn-large btn-to-cart',
                        'DEFAULT_DISPLAY' => true,
                        'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                    ),
                    $component,
                    array('HIDE_ICONS' => 'Y')
            );
        }
        ?>
        <div class="buttons-wrap" id="<?= $itemIds['BASKET_ACTIONS'] ?>" <?= ($actualItem['CAN_BUY'] ? '' : 'style="display: none;"') ?>>
            <a href="javascript:void(0)" rel="nofollow" id="<?= $itemIds['BUY_LINK'] ?>" class="btn btn-primary btn-large btn-to-cart">
        <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?> <i class="icon icon-basket-white"></i>
            </a>
        </div>
        <?
    }
    else
    {
        ?>
        <div class="buttons-wrap">
            <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="btn btn-primary btn-large btn-to-cart">
        <?= $arParams['MESS_BTN_DETAIL'] ?>
            </a>
        </div>
        <?
    }
}
                
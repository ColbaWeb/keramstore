<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;
?>
<a href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$productTitle?>" class="title font-title">
                    <?=$productTitle?>
                </a>
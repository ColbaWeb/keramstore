<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

if (Options\Base::getInstance(SITE_ID)->getValue('catalogReviews'))
            {
                $summary = Reviews\Base::getInstance()->getSummaryByProduct($arResult['ITEM']['ID']);
                if ($summary['avg'] > 0)
                {
                ?>
                <div class="rating">
                    <i class="icon icon-star-12"></i>
                    <div class="value"><?= number_format($summary['avg'], 1, ",", " ")?></div>
                </div>
                <?
                }
            }
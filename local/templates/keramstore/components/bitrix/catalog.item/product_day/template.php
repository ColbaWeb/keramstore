<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Tools;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

if (!Loader::includeModule('nextype.alpha'))
    return;

if (isset($arResult['ITEM']))
{
	$item = $arResult['ITEM'];
	$areaId = $arResult['AREA_ID'];
	$itemIds = array(
		'ID' => $areaId,
		'PICT' => $areaId.'_pict',
		'SECOND_PICT' => $areaId.'_secondpict',
		'PICT_SLIDER' => $areaId.'_pict_slider',
		'STICKER_ID' => $areaId.'_sticker',
		'SECOND_STICKER_ID' => $areaId.'_secondsticker',
		'QUANTITY' => $areaId.'_quantity',
		'QUANTITY_DOWN' => $areaId.'_quant_down',
		'QUANTITY_UP' => $areaId.'_quant_up',
		'QUANTITY_MEASURE' => $areaId.'_quant_measure',
		'QUANTITY_LIMIT' => $areaId.'_quant_limit',
		'BUY_LINK' => $areaId.'_buy_link',
		'BASKET_ACTIONS' => $areaId.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
		'SUBSCRIBE_LINK' => $areaId.'_subscribe',
		'COMPARE_LINK' => $areaId.'_compare_link',
		'PRICE' => $areaId.'_price',
		'PRICE_OLD' => $areaId.'_price_old',
		'PRICE_TOTAL' => $areaId.'_price_total',
		'DSC_PERC' => $areaId.'_dsc_perc',
		'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
		'PROP_DIV' => $areaId.'_sku_tree',
		'PROP' => $areaId.'_prop_',
		'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
		'BASKET_PROP_DIV' => $areaId.'_basket_prop',
                'WISHLIST_ID' => $areaId.'_wishlist',
                'ITEM_CODE_ID' => $areaId.'_itemcode',
	);
	$obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);
	$isBig = isset($arResult['BIG']) && $arResult['BIG'] === 'Y';

	$productTitle = isset($item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $item['NAME'];

	$imgTitle = isset($item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $item['NAME'];

	$skuProps = array();

	$haveOffers = !empty($item['OFFERS']);
	if ($haveOffers)
	{
		$actualItem = isset($item['OFFERS'][$item['OFFERS_SELECTED']])
			? $item['OFFERS'][$item['OFFERS_SELECTED']]
			: reset($item['OFFERS']);
	}
	else
	{
		$actualItem = $item;
	}

	if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
	{
		$price = $item['ITEM_START_PRICE'];
		$minOffer = $item['OFFERS'][$item['ITEM_START_PRICE_SELECTED']];
		$measureRatio = $minOffer['ITEM_MEASURE_RATIOS'][$minOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
	}
	else
	{
		$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
		$measureRatio = $price['MIN_QUANTITY'];
	}

	$showSlider = is_array($morePhoto) && count($morePhoto) > 1;
	$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($item['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

	$discountPositionClass = isset($arResult['BIG_DISCOUNT_PERCENT']) && $arResult['BIG_DISCOUNT_PERCENT'] === 'Y'
		? 'product-item-label-big'
		: 'product-item-label-small';
	$discountPositionClass .= $arParams['DISCOUNT_POSITION_CLASS'];

	$labelPositionClass = isset($arResult['BIG_LABEL']) && $arResult['BIG_LABEL'] === 'Y'
		? 'product-item-label-big'
		: 'product-item-label-small';
	$labelPositionClass .= $arParams['LABEL_POSITION_CLASS'];

	$buttonSizeClass = isset($arResult['BIG_BUTTONS']) && $arResult['BIG_BUTTONS'] === 'Y' ? 'btn-md' : 'btn-sm';
	$itemHasDetailUrl = isset($item['DETAIL_PAGE_URL']) && $item['DETAIL_PAGE_URL'] != '';
        
        if ($haveOffers)
        {
            $showDisplayProps = !empty($item['DISPLAY_PROPERTIES']);
            $showProductProps = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $item['OFFERS_PROPS_DISPLAY'];
            $showPropsBlock = $showDisplayProps || $showProductProps;
            $showSkuBlock = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !empty($item['OFFERS_PROP']);
        }
        else
        {
            $showDisplayProps = !empty($item['DISPLAY_PROPERTIES']);
            $showProductProps = $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !empty($item['PRODUCT_PROPERTIES']);
            $showPropsBlock = $showDisplayProps || $showProductProps;
            $showSkuBlock = false;
        }
        
        Tools\Product::modifierStock($actualItem, $arParams);
        Tools\Product::modifierStock($item, $arParams);
        
	?>

        <div class="product-day">
            <div class="container">
                <div class="product-day-item" id="<?=$areaId?>">
                    <div class="info-block">
                        <?
                        if (!empty($arParams['SECTION_TITLE']))
                        {
                            ?><div class="subtitle font-title"><?=$arParams['SECTION_TITLE']?></div><?
                        }
                        ?>
                        
                        <div class="main-info">
                            <a href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$productTitle?>" class="title d4"><?=$productTitle?></a>
                            <?
                            if (!empty($item['PREVIEW_TEXT']))
                            {
                                ?>
                                <div class="desc font-body">
                                    <?=$item['PREVIEW_TEXT']?>
                                </div>
                                <?
                            }
                            
                            if ($showSkuBlock)
                            {
                                ?>
                                <div id="<?=$itemIds['PROP_DIV']?>">
                                    <?
                                    foreach ($arParams['SKU_PROPS'] as $skuProperty)
                                    {
                                        $propertyId = $skuProperty['ID'];
                                        $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                                        if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                                            continue;

                                        ?>
                                            <div class="sku-list<?=$skuProperty['SHOW_MODE'] === 'PICT' ? ' images' : ''?>" data-entity="sku-block">
                                                    <div class="sku-title font-body-small"><?=$skuProperty['NAME']?></div>
                                                    <div class="list" data-entity="sku-line-block">
                                                        <?
                                                        foreach ($skuProperty['VALUES'] as $value)
                                                        {
                                                            if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                                                continue;

                                                            $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                            if ($skuProperty['SHOW_MODE'] === 'PICT')
                                                            {
                                                                ?>
                                                                    <a href="javascript:void(0)" title="<?=$value['NAME']?>" class="item sku-element image" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
                                                                        <div style="background-image:url('<?=$value['PICT']['SRC']?>')"></div>
                                                                    </a>
                                                                <?
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                <a href="javascript:void(0)" class="item sku-element" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>"><?=$value['NAME']?></a>
                                                                <?
                                                            }

                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                        <?
                                    }
                                    ?>
                                </div>
                                <?
                                foreach ($arParams['SKU_PROPS'] as $skuProperty)
                                {
                                    if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                                        continue;

                                    $skuProps[] = array(
                                        'ID' => $skuProperty['ID'],
                                        'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                        'VALUES' => $skuProperty['VALUES'],
                                        'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                                    );
                                }

                                unset($skuProperty, $value);

                                if ($item['OFFERS_PROPS_DISPLAY'])
                                {
                                    foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer)
                                    {
                                        $strProps = '';

                                        if (!empty($jsOffer['DISPLAY_PROPERTIES']))
                                        {
                                            foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty)
                                            {
                                                $strProps .= ''
                                                        . '<div itemprop="additionalProperty" itemscope="" itemtype="http://schema.org/PropertyValue" class="char">'
                                                        . '<div class="char-name"><span><span itemprop="name">'.$displayProperty['NAME'].'</span></span></div>'
                                                        . '<div class="char-value"><span itemprop="value">'.(is_array($displayProperty['VALUE']) ? implode(' / ', $displayProperty['VALUE']) : $displayProperty['VALUE']).'</span></div>'
                                                        . '</div>';
                                            }
                                        }
                                        $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
                                    }
                                    unset($jsOffer, $strProps);
                                }

                            }
                            
                            ?>
                            
                            <div class="price-block" data-entity="price-block">
                                <div class="price price font-price-large bold" id="<?=$itemIds['PRICE']?>">
                                    <?
                                    if (!empty($price))
                                    {
                                        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
                                        {
                                            echo Loc::getMessage(
                                                    'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                                                    array(
                                                        '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                                        '#VALUE#' => $measureRatio,
                                                        '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                                    )
                                            );
                                        }
                                        else
                                        {
                                            echo $price['PRINT_RATIO_PRICE'];
                                        }
                                    }
                                    ?>
                                </div>
                                <?
                                if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                                {
                                    ?>
                                    <div class="old-price font-body-2" id="<?= $itemIds['PRICE_OLD'] ?>"<?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? ' style="display: none;"' : '') ?>><?= $price['PRINT_RATIO_BASE_PRICE'] ?></div>
                                    <?
                                }
                                
                                if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
                                {
                                    ?>
                                    <div class="sale label action" id="<?=$itemIds['DSC_PERC']?>" <?=($price['PERCENT'] > 0 ? '' : 'style="display: none;"')?>><?=-$price['PERCENT']?>%</div>
                                    <?
                                }
                                ?>
                            </div>
                            
                            <?
                            if (Options\Base::getInstance(SITE_ID)->getValue('buyoneclickActive'))
                            {
                                ?>
                                <a href="javascript:void(0)" onclick="window.Alpha.Popups.openAjaxContent({url: '<?=SITE_DIR?>include/ajax/buy_one_click.php?id=<?=$actualItem['ID']?>'})" class="button-link">
                                    <?=Loc::getMessage('CT_BCI_TPL_MESS_BUY_ONE_CLICK')?>
                                    <i class="icon icon-arrow-right-text-button"></i>
                                </a>
                                <?
                            }

                            ?>

                            <div class="buttons-block">
                                
                                <?
                                if (!$haveOffers)
                                {
                                    if ($actualItem['CAN_BUY'] && $arParams['USE_PRODUCT_QUANTITY'])
                                    {
                                        ?>
                                        <div class="counter" data-entity="quantity-block">
                                            <a href="javascript:void(0)" id="<?=$itemIds['QUANTITY_DOWN']?>" class="minus"><i class="icon icon-bx-minus"></i></a>
                                            <input type="number" id="<?=$itemIds['QUANTITY']?>" type="number" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>" value="<?=$measureRatio?>">
                                            <span class="measure" id="<?=$itemIds['QUANTITY_MEASURE']?>"><?=$actualItem['ITEM_MEASURE']['TITLE']?></span>
                                            <a href="javascript:void(0)" id="<?=$itemIds['QUANTITY_UP']?>" class="plus"><i class="icon icon-bx-plus"></i></a>
                                        </div>
                                        <?
                                    }
                                }
                                elseif ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
                                {
                                    if ($arParams['USE_PRODUCT_QUANTITY'])
                                    {
                                        ?>
                                        <div class="counter" data-entity="quantity-block">
                                            <a href="javascript:void(0)" id="<?=$itemIds['QUANTITY_DOWN']?>" class="minus"><i class="icon icon-bx-minus"></i></a>
                                            <input type="number" id="<?=$itemIds['QUANTITY']?>" type="number" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>" value="<?=$measureRatio?>">
                                            <span class="measure" id="<?=$itemIds['QUANTITY_MEASURE']?>"><?=$actualItem['ITEM_MEASURE']['TITLE']?></span>
                                            <a href="javascript:void(0)" id="<?=$itemIds['QUANTITY_UP']?>" class="plus"><i class="icon icon-bx-plus"></i></a>
                                        </div>
                                        <?
                                    }
                                }
                                
                                if (!$haveOffers)
                                {
                                    if ($actualItem['CAN_BUY'])
                                    {
                                        ?>
                                        <div class="buttons-wrap" id="<?=$itemIds['BASKET_ACTIONS']?>">
                                            <a href="javascript:void(0)" rel="nofollow" id="<?=$itemIds['BUY_LINK']?>" class="btn btn-primary btn-large to-basket">
                                                <span><?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?></span>
                                                <i class="icon icon-basket-white"></i>
                                            </a>
                                            
                                            <?
                                            if (Options\Base::getInstance(SITE_ID)->getValue('wishList') && $actualItem['CAN_BUY'])
                                            {
                                                ?><a href="javascript:void(0)" class="btn-favorites" id="<?=$itemIds['WISHLIST_ID']?>"><i class="icon icon-bx-heart"></i></a><?
                                            }
                                            ?>
                                        </div>
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <div class="buttons-wrap">
                                            <?
                                                if ($showSubscribe) {
                                                    $APPLICATION->IncludeComponent(
                                                            'bitrix:catalog.product.subscribe',
                                                            'main',
                                                            array(
                                                                'PRODUCT_ID' => $actualItem['ID'],
                                                                'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                                                'BUTTON_CLASS' => 'btn btn-primary btn-large btn-to-cart',
                                                                'DEFAULT_DISPLAY' => true,
                                                                'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                                            ),
                                                            $component,
                                                            array('HIDE_ICONS' => 'Y')
                                                    );
                                                }
                                                ?>
                                        </div>
                                        <?
                                    }
                                }
                                else
                                {
                                    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
                                    {
                                        if ($showSubscribe)
                                        {
                                            $APPLICATION->IncludeComponent(
                                                    'bitrix:catalog.product.subscribe',
                                                    'main',
                                                    array(
                                                        'PRODUCT_ID' => $actualItem['ID'],
                                                        'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                                        'BUTTON_CLASS' => 'btn btn-primary btn-large btn-to-cart',
                                                        'DEFAULT_DISPLAY' => true,
                                                        'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                                    ),
                                                    $component,
                                                    array('HIDE_ICONS' => 'Y')
                                            );
                                        }
                                        ?>
                                        <div class="buttons-wrap" id="<?=$itemIds['BASKET_ACTIONS']?>" <?=($actualItem['CAN_BUY'] ? '' : 'style="display: none;"')?>>
                                            <a href="javascript:void(0)" rel="nofollow" id="<?=$itemIds['BUY_LINK']?>" class="btn btn-primary btn-large to-basket">
                                                <span><?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?></span>
                                                <i class="icon icon-basket-white"></i>
                                            </a>
                                            
                                            <?
                                            if (Options\Base::getInstance(SITE_ID)->getValue('wishList') && $actualItem['CAN_BUY'])
                                            {
                                                ?><a href="javascript:void(0)" class="btn-favorites" id="<?=$itemIds['WISHLIST_ID']?>"><i class="icon icon-bx-heart"></i></a><?
                                            }
                                            ?>
                                            
                                        </div>
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <div class="buttons-wrap">
                                            <a href="<?=$item['DETAIL_PAGE_URL']?>" class="btn btn-primary btn-large to-basket">
                                                <?=$arParams['MESS_BTN_DETAIL']?>
                                            </a>
                                        </div>
                                        <?
                                    }
                                }
                                ?>
                            </div>

                        </div>
                         
                        <?
                        if (!empty($arParams['SECTION_LINK_TITLE']) && !empty($arParams['SECTION_LINK_URL']))
                        {
                            ?>
                            <a href="<?=$arParams['SECTION_LINK_URL']?>" class="btn btn-link">
                                <?=$arParams['SECTION_LINK_TITLE']?>
                                <i class="icon icon-arrow-right-text-button"></i>
                            </a>
                            <?
                        }
                        ?>

                    </div>
                    <div class="image-block">
                        <div class="labels" id="<?=$itemIds['STICKER_ID']?>">
                            <?
                            if (!empty($item['LABEL_ARRAY_VALUE']))
                            {
                                foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value)
                                {
                                    if (isset($item['PROPERTIES'][$code]['VALUE']) && !empty($item['PROPERTIES'][$code]['VALUE']))
                                    {
                                        foreach ($item['PROPERTIES'][$code]['VALUE'] as $labelKey => $labelValue)
                                        {
                                            ?><div class="label <?=$item['PROPERTIES'][$code]['VALUE_XML_ID'][$labelKey]?><?= (!isset($item['LABEL_PROP_MOBILE'][$code]) ? ' d-none' : '') ?>"><?= $labelValue ?></div><?
                                        }
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="label badge-in-stock">
                            <div class="in-stock">
                                <?
                                if ($arParams['SHOW_MAX_QUANTITY'] !== 'N')
                                {
                                    if ($haveOffers)
                                    {
                                        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
                                        {
                                            ?>
                                            <span id="<?=$itemIds['QUANTITY_LIMIT']?>" style="display: none;" data-entity="quantity-limit-block">
                                                <?
                                                if ($arParams['SHOW_MAX_QUANTITY'] != "M")
                                                    echo $arParams['MESS_SHOW_MAX_QUANTITY'] . ":";
                                                ?>
                                                <span data-entity="quantity-limit-value"></span>
                                            </span>
                                            <span id="<?=$itemIds['NOT_AVAILABLE_MESS']?>" <?=($actualItem['CAN_BUY'] ? 'style="display: none;"' : '')?>>
                                                <?=$arParams['MESS_NOT_AVAILABLE']?>
                                            </span>
                                            <?
                                        }
                                    }
                                    else
                                    {
                                        if ($measureRatio && (float)$actualItem['CATALOG_QUANTITY'] > 0 && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y' && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N')
                                        {
                                            ?>
                                            <span id="<?=$itemIds['QUANTITY_LIMIT']?>">
                                                <?
                                                if ($arParams['SHOW_MAX_QUANTITY'] != "M")
                                                    echo $arParams['MESS_SHOW_MAX_QUANTITY'] . ":";
                                                ?>
                                                <span data-entity="quantity-limit-value">
                                                    <?
                                                    if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
                                                    {
                                                        if ((float) $actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
                                                        {
                                                            echo $arParams['MESS_RELATIVE_QUANTITY_MANY']; 
                                                        }
                                                        else
                                                        {
                                                            echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
                                                        }

                                                     }
                                                     else
                                                     {
                                                         echo $actualItem['CATALOG_QUANTITY'] . ' ' . $actualItem['ITEM_MEASURE']['TITLE'];
                                                     }
                                                     ?>
                                                 </span>
                                            </span>
                                            <?
                                        }
                                        else
                                        {
                                            ?><div class="out-stock"><?= $arParams['MESS_NOT_AVAILABLE'] ?></div><?
                                        }
                                    }
                                }
                                ?>
                            </div>
                            
                            <?
                            if (Options\Base::getInstance(SITE_ID)->getValue('catalogReviews'))
                            {
                                $summary = Reviews\Base::getInstance()->getSummaryByProduct($arResult['ITEM']['ID']);
                                if ($summary['avg'] > 0)
                                {
                                ?>
                                <div class="rating">
                                    <i class="icon icon-star-12"></i>
                                    <div class="value"><?= number_format($summary['avg'], 1, ",", " ")?></div>
                                </div>
                                <?
                                }
                            }
                            ?>

                        </div>
                        <div class="image-slider" id="<?=$itemIds['PICT_SLIDER']?>">
                            <?
                            if ((!empty($item['PICTURES']) && count($item['PICTURES']) == 1) || empty($item['PICTURES']))
                            {
                                $arPicture = !empty($item['PICTURES']) ? end($item['PICTURES']) : array ('SRC' => '');
                                echo Layout\Assets::showImage($arPicture['SRC'], array("attributes" => array("class=\"product-image\"")), Options\Base::getInstance(SITE_ID)->getValue('imageGrayBackground'));
                            }
                            elseif (!empty($item['PICTURES']) && count($item['PICTURES']) > 1)
                            {
                                ?>
                                <div class="swiper-container gallery-top">
                                    <div class="swiper-wrapper">
                                    <?
                                    foreach ($item['PICTURES'] as $arPicture)
                                    {
                                        ?>
                                        <div class="swiper-slide">
                                            <?
                                            echo Layout\Assets::showImage($arPicture['SRC'], array(
                                                "attributes" => array(
                                                    "class=\"product-image\""

                                                    )
                                            ), Options\Base::getInstance(SITE_ID)->getValue('imageGrayBackground'));
                                            ?>
                                        </div>
                                        <?
                                    }
                                    ?>
                                    </div>
                                    <div class="swiper-pagination"></div>
                                </div>
                                <a href="<?=$item['DETAIL_PAGE_URL']?>" class="product-card-link">
                                    <div class="swiper-container gallery-thumbs">
                                        <div class="swiper-wrapper">
                                            <? foreach ($item['PICTURES'] as $arPicture)
                                            {
                                                ?><div class="swiper-slide"></div><?
                                            }
                                            ?>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                </a>
                                <?
                            }
                            ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <?
        if (!$haveOffers)
        {
            $jsParams = array(
                'PRODUCT_TYPE' => $item['PRODUCT']['TYPE'],
                'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                'SHOW_ADD_BASKET_BTN' => false,
                'SHOW_BUY_BTN' => true,
                'SHOW_ABSENT' => true,
                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
                'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
                'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
                'BIG_DATA' => $item['BIG_DATA'],
                'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
                'VIEW_MODE' => $arResult['TYPE'],
                'USE_SUBSCRIBE' => $showSubscribe,
                'PRODUCT' => array(
                    'ID' => $item['ID'],
                    'NAME' => $productTitle,
                    'DETAIL_PAGE_URL' => $item['DETAIL_PAGE_URL'],
                    'PICT' => $item['SECOND_PICT'] ? $item['PREVIEW_PICTURE_SECOND'] : $item['PREVIEW_PICTURE'],
                    'CAN_BUY' => $item['CAN_BUY'],
                    'CHECK_QUANTITY' => $item['CHECK_QUANTITY'],
                    'MAX_QUANTITY' => $item['CATALOG_QUANTITY'],
                    'STEP_QUANTITY' => $item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
                    'QUANTITY_FLOAT' => is_float($item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
                    'ITEM_PRICE_MODE' => $item['ITEM_PRICE_MODE'],
                    'ITEM_PRICES' => $item['ITEM_PRICES'],
                    'ITEM_PRICE_SELECTED' => $item['ITEM_PRICE_SELECTED'],
                    'ITEM_QUANTITY_RANGES' => $item['ITEM_QUANTITY_RANGES'],
                    'ITEM_QUANTITY_RANGE_SELECTED' => $item['ITEM_QUANTITY_RANGE_SELECTED'],
                    'ITEM_MEASURE_RATIOS' => $item['ITEM_MEASURE_RATIOS'],
                    'ITEM_MEASURE_RATIO_SELECTED' => $item['ITEM_MEASURE_RATIO_SELECTED'],
                    'MORE_PHOTO' => $item['MORE_PHOTO'],
                    'MORE_PHOTO_COUNT' => $item['MORE_PHOTO_COUNT']
                ),
                'BASKET' => array(
                    'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
                    'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                    'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                    'EMPTY_PROPS' => empty($item['PRODUCT_PROPERTIES']),
                    'BASKET_URL' => $arParams['~BASKET_URL'],
                    'ADD_URL_TEMPLATE' => $arParams['~ADD_URL_TEMPLATE'],
                    'BUY_URL_TEMPLATE' => $arParams['~BUY_URL_TEMPLATE'],
                    'WISHLIST_URL_TEMPLATE' => Tools\Wishlist::getChangeUrl()
                ),
                'VISUAL' => array(
                    'ID' => $itemIds['ID'],
                    'PICT_ID' => $item['SECOND_PICT'] ? $itemIds['SECOND_PICT'] : $itemIds['PICT'],
                    'PICT_SLIDER_ID' => $itemIds['PICT_SLIDER'],
                    'QUANTITY_ID' => $itemIds['QUANTITY'],
                    'QUANTITY_UP_ID' => $itemIds['QUANTITY_UP'],
                    'QUANTITY_DOWN_ID' => $itemIds['QUANTITY_DOWN'],
                    'PRICE_ID' => $itemIds['PRICE'],
                    'PRICE_OLD_ID' => $itemIds['PRICE_OLD'],
                    'PRICE_TOTAL_ID' => $itemIds['PRICE_TOTAL'],
                    'BUY_ID' => $itemIds['BUY_LINK'],
                    'BASKET_PROP_DIV' => $itemIds['BASKET_PROP_DIV'],
                    'BASKET_ACTIONS_ID' => $itemIds['BASKET_ACTIONS'],
                    'NOT_AVAILABLE_MESS' => $itemIds['NOT_AVAILABLE_MESS'],
                    'COMPARE_LINK_ID' => $itemIds['COMPARE_LINK'],
                    'SUBSCRIBE_ID' => $itemIds['SUBSCRIBE_LINK'],
                    'WISHLIST_ID' => $itemIds['WISHLIST_ID']
                )
            );
        }
        else
        {
            $jsParams = array(
                'PRODUCT_TYPE' => $item['PRODUCT']['TYPE'],
                'SHOW_QUANTITY' => false,
                'SHOW_ADD_BASKET_BTN' => false,
                'SHOW_BUY_BTN' => true,
                'SHOW_ABSENT' => true,
                'SHOW_SKU_PROPS' => false,
                'SECOND_PICT' => $item['SECOND_PICT'],
                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
                'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
                'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
                'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
                'BIG_DATA' => $item['BIG_DATA'],
                'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
                'VIEW_MODE' => $arResult['TYPE'],
                'USE_SUBSCRIBE' => $showSubscribe,
                'DEFAULT_PICTURE' => array(
                    'PICTURE' => $item['PRODUCT_PREVIEW'],
                    'PICTURE_SECOND' => $item['PRODUCT_PREVIEW_SECOND']
                ),
                'VISUAL' => array(
                    'ID' => $itemIds['ID'],
                    'PICT_ID' => $itemIds['PICT'],
                    'SECOND_PICT_ID' => $itemIds['SECOND_PICT'],
                    'PICT_SLIDER_ID' => $itemIds['PICT_SLIDER'],
                    'QUANTITY_ID' => $itemIds['QUANTITY'],
                    'QUANTITY_UP_ID' => $itemIds['QUANTITY_UP'],
                    'QUANTITY_DOWN_ID' => $itemIds['QUANTITY_DOWN'],
                    'QUANTITY_MEASURE' => $itemIds['QUANTITY_MEASURE'],
                    'QUANTITY_LIMIT' => $itemIds['QUANTITY_LIMIT'],
                    'PRICE_ID' => $itemIds['PRICE'],
                    'PRICE_OLD_ID' => $itemIds['PRICE_OLD'],
                    'PRICE_TOTAL_ID' => $itemIds['PRICE_TOTAL'],
                    'TREE_ID' => $itemIds['PROP_DIV'],
                    'TREE_ITEM_ID' => $itemIds['PROP'],
                    'BUY_ID' => $itemIds['BUY_LINK'],
                    'DSC_PERC' => $itemIds['DSC_PERC'],
                    'SECOND_DSC_PERC' => $itemIds['SECOND_DSC_PERC'],
                    'DISPLAY_PROP_DIV' => $itemIds['DISPLAY_PROP_DIV'],
                    'BASKET_ACTIONS_ID' => $itemIds['BASKET_ACTIONS'],
                    'NOT_AVAILABLE_MESS' => $itemIds['NOT_AVAILABLE_MESS'],
                    'COMPARE_LINK_ID' => $itemIds['COMPARE_LINK'],
                    'SUBSCRIBE_ID' => $itemIds['SUBSCRIBE_LINK'],
                    'WISHLIST_ID' => $itemIds['WISHLIST_ID']
                ),
                'BASKET' => array(
                    'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                    'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                    'SKU_PROPS' => $item['OFFERS_PROP_CODES'],
                    'BASKET_URL' => $arParams['~BASKET_URL'],
                    'ADD_URL_TEMPLATE' => $arParams['~ADD_URL_TEMPLATE'],
                    'BUY_URL_TEMPLATE' => $arParams['~BUY_URL_TEMPLATE'],
                    'WISHLIST_URL_TEMPLATE' => Tools\Wishlist::getChangeUrl()
                ),
                'PRODUCT' => array(
                    'ID' => $item['ID'],
                    'NAME' => $productTitle,
                    'DETAIL_PAGE_URL' => $item['DETAIL_PAGE_URL'],
                    'MORE_PHOTO' => $item['MORE_PHOTO'],
                    'MORE_PHOTO_COUNT' => $item['MORE_PHOTO_COUNT']
                ),
                'OFFERS' => array(),
                'OFFER_SELECTED' => 0,
                'TREE_PROPS' => array()
            );

            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !empty($item['OFFERS_PROP']))
            {
                $jsParams['SHOW_QUANTITY'] = $arParams['USE_PRODUCT_QUANTITY'];
                $jsParams['SHOW_SKU_PROPS'] = $item['OFFERS_PROPS_DISPLAY'];
                $jsParams['OFFERS'] = $item['JS_OFFERS'];
                $jsParams['OFFER_SELECTED'] = $item['OFFERS_SELECTED'];
                $jsParams['TREE_PROPS'] = $skuProps;
            }
        }

        if ($arParams['DISPLAY_COMPARE'])
        {
            $jsParams['COMPARE'] = array(
                'COMPARE_URL_TEMPLATE' => $arParams['~COMPARE_URL_TEMPLATE'],
                'COMPARE_DELETE_URL_TEMPLATE' => $arParams['~COMPARE_DELETE_URL_TEMPLATE'],
                'COMPARE_PATH' => $arParams['COMPARE_PATH']
            );
        }

        if ($item['BIG_DATA'])
        {
            $jsParams['PRODUCT']['RCM_ID'] = $item['RCM_ID'];
        }

        $jsParams['PRODUCT_DISPLAY_MODE'] = $arParams['PRODUCT_DISPLAY_MODE'];
        $jsParams['USE_ENHANCED_ECOMMERCE'] = $arParams['USE_ENHANCED_ECOMMERCE'];
        $jsParams['DATA_LAYER_NAME'] = $arParams['DATA_LAYER_NAME'];
        $jsParams['BRAND_PROPERTY'] = !empty($item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]) ? $item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE'] : null;

        $templateData = array(
            'TYPE' => $arResult['TYPE'],
            'JS_OBJ' => $obName,
            'ITEM' => array(
                'ID' => $item['ID'],
                'IBLOCK_ID' => $item['IBLOCK_ID'],
                'OFFERS_SELECTED' => $item['OFFERS_SELECTED'],
                'JS_OFFERS' => $item['JS_OFFERS']
            )
        );
        ?>
        <script>
            BX.ready(function () {
                var interval = setInterval(function () {
                    if (!!window.BX.Currency)
                    {
                        window.<?= $obName ?> = new window.Alpha.CatalogListItem(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
                        clearInterval(interval);
                    }
                }, 1000);
            });
            

        </script>

	<?
	unset($item, $actualItem, $minOffer, $itemIds, $jsParams);
}
?>
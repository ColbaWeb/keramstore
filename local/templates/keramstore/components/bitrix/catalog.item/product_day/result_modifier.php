<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

$haveOffers = !empty($arResult['ITEM']['OFFERS']);
$arResizeParams = array('width' => 700, 'height' => 700);

$arResult['ITEM']['PICTURES'] = array();

if ($haveOffers)
{
    $actualItem = isset($arResult['ITEM']['OFFERS'][$arResult['ITEM']['OFFERS_SELECTED']]) ? $arResult['ITEM']['OFFERS'][$arResult['ITEM']['OFFERS_SELECTED']] : reset($arResult['ITEM']['OFFERS']);
    
    if (!empty($arParams['OFFER_CODE_PROP']) && isset($actualItem['PROPERTIES'][$arParams['OFFER_CODE_PROP']]))
    {
        $arResult['ITEM']['ITEM_CODE'] = is_array($actualItem['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE']) ? implode(", ", $actualItem['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE']) : $actualItem['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE'];
    }
}
else
{
    $actualItem = $arResult['ITEM'];
    
    if (!empty($arParams['PRODUCT_CODE_PROP']) && isset($actualItem['PROPERTIES'][$arParams['PRODUCT_CODE_PROP']]))
    {
        $arResult['ITEM']['ITEM_CODE'] = is_array($actualItem['PROPERTIES'][$arParams['PRODUCT_CODE_PROP']]['VALUE']) ? implode(", ", $actualItem['PROPERTIES'][$arParams['PRODUCT_CODE_PROP']]['VALUE']) : $actualItem['PROPERTIES'][$arParams['PRODUCT_CODE_PROP']]['VALUE'];
    }
}


if (!empty($actualItem['PREVIEW_PICTURE']))
{
    if ($arImage = CFile::ResizeImageGet($actualItem['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
    {
        $arResult['ITEM']['PICTURES'][$actualItem['PREVIEW_PICTURE']['ID']] = array(
            'SRC' => $arImage['src'],
            'ALT' => $actualItem['NAME'],
            'TITLE' => $actualItem['NAME'],
        );
    }
}
elseif (!empty($actualItem['DETAIL_PICTURE']))
{
    if ($arImage = CFile::ResizeImageGet($actualItem['DETAIL_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
    {
        $arResult['ITEM']['PICTURES'][$actualItem['DETAIL_PICTURE']['ID']] = array(
            'SRC' => $arImage['src'],
            'ALT' => $actualItem['NAME'],
            'TITLE' => $actualItem['NAME'],
        );
    }
}

if (!empty($arParams['ADD_PICT_PROP']) && isset($actualItem['PROPERTIES'][$arParams['ADD_PICT_PROP']]) && !empty($actualItem['PROPERTIES'][$arParams['ADD_PICT_PROP']]['VALUE']))
{
    foreach ($actualItem['PROPERTIES'][$arParams['ADD_PICT_PROP']]['VALUE'] as $morePhotoId)
    {
        if (!empty($morePhotoId) && !isset($arResult['ITEM']['PICTURES']['PICTURES'][$morePhotoId]))
        {
            if ($arImage = CFile::ResizeImageGet($morePhotoId, $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
            {
                $arResult['ITEM']['PICTURES'][$morePhotoId] = array(
                    'SRC' => $arImage['src'],
                    'ALT' => $actualItem['NAME'],
                    'TITLE' => $actualItem['NAME'],
                );
            }
        }
    }
}


if ($haveOffers)
{
    $arResult['ITEM']['OFFERS_RANGE_PRICES'] = false;

    if (count($arResult['ITEM']['OFFERS']) > 1)
    {
        $diffPrice = isset($arResult['ITEM']['OFFERS'][0]['MIN_PRICE']) ? $arResult['ITEM']['OFFERS'][0]['MIN_PRICE']['DISCOUNT_VALUE'] : 0;

        foreach ($arResult['ITEM']['OFFERS'] as $offer)
        {
            if (isset($offer['MIN_PRICE']) && $offer['MIN_PRICE']['DISCOUNT_VALUE'] <> $diffPrice)
            {
                $arResult['ITEM']['OFFERS_RANGE_PRICES'] = true;
                break;
            }
        }
    }

    foreach ($arResult['ITEM']['JS_OFFERS'] as $key => $arOffer)
    {
        $arOffer['MORE_PHOTO'] = array();

        if (!empty($arOffer['PREVIEW_PICTURE']) && $arImage = CFile::ResizeImageGet($arOffer['PREVIEW_PICTURE']['ID'], $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
        {
            $arOffer['PREVIEW_PICTURE'] = array(
                'SRC' => $arImage['src']
            );
        }

        if (!empty($arParams['OFFER_ADD_PICT_PROP']) && isset($arResult['ITEM']['OFFERS'][$key]['PROPERTIES'][$arParams['OFFER_ADD_PICT_PROP']]))
        {
            $morePhotos = $arResult['ITEM']['OFFERS'][$key]['PROPERTIES'][$arParams['OFFER_ADD_PICT_PROP']];

            if (!empty($morePhotos['VALUE']))
            {
                foreach ($morePhotos['VALUE'] as $photoId)
                {
                    if (!empty($photoId) && $arImage = CFile::ResizeImageGet($photoId, $arResizeParams, BX_RESIZE_IMAGE_PROPORTIONAL_ALT))
                    {
                        $arOffer['MORE_PHOTO'][] = array(
                            'SRC' => $arImage['src']
                        );
                    }
                }
            }
        }

        if (!empty($arParams['OFFER_CODE_PROP']) && isset($arResult['ITEM']['OFFERS'][$key]['PROPERTIES'][$arParams['OFFER_CODE_PROP']]))
        {
            $arOffer['ITEM_CODE'] = is_array($arResult['ITEM']['OFFERS'][$key]['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE']) ? implode(", ", $arResult['ITEM']['OFFERS'][$key]['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE']) : $arResult['ITEM']['OFFERS'][$key]['PROPERTIES'][$arParams['OFFER_CODE_PROP']]['VALUE'];
        }

        $arResult['ITEM']['JS_OFFERS'][$key] = $arOffer;
    }
    
    foreach ($arParams['SKU_PROPS'] as $key => $skuProperty)
    {
        if ($skuProperty['SHOW_MODE'] === "PICT" && !empty($skuProperty['VALUES']))
        {
            foreach ($skuProperty['VALUES'] as $keyValue => $skuValue)
            {
                foreach ($arResult['ITEM']['OFFERS'] as $arOffer)
                {
                    if (!isset($arOffer['PREVIEW_PICTURE']['ID']) || empty($arOffer['PREVIEW_PICTURE']['ID']))
                        continue;
                    
                    if (isset($arOffer['TREE']) && is_array($arOffer['TREE']) && isset($arOffer['TREE']['PROP_' . $skuProperty['ID']]) && $arOffer['TREE']['PROP_' . $skuProperty['ID']] == $skuValue['ID'])
                    {
                        $arResize = CFile::ResizeImageGet($arOffer['PREVIEW_PICTURE']['ID'], array ('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT);
                        if ($arResize)
                        {
                            $skuValue['PICT']['SRC'] = $arResize['src'];
                            break;
                        }
                    }
                }
                
                $arParams['SKU_PROPS'][$key]['VALUES'][$keyValue] = $skuValue;
            }
        }
    }
    
}


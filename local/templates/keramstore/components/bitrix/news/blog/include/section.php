<?
use Nextype\Alpha\Options,
    Nextype\Alpha\Tools;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams['FILTER_NAME'] = (empty($arParams['FILTER_NAME'])) ? 'arrBlogFilter' : $arParams['FILTER_NAME'];

if (empty($GLOBALS[$arParams['FILTER_NAME']]) || !is_array($GLOBALS[$arParams['FILTER_NAME']]))
        $GLOBALS[$arParams['FILTER_NAME']] = array();

$bFilterInSection = $arParams['USE_SUBSECTIONS_TAGS_FILTER'] == 'Y';

if ($tagFilter = \Bitrix\Main\Context::getCurrent()->getRequest()->get('tag'))
{
    $tagValue = \Bitrix\Iblock\PropertyEnumerationTable::getList(array(
        'filter' => array('XML_ID' => $tagFilter),
        'select' => array('ID'),
        'limit' => 1
    ))->fetch();
    if ($tagValue && $bFilterInSection)
    {
        $GLOBALS[$arParams['FILTER_NAME']]['PROPERTY_TAGS'] = $tagValue['ID'];
    }
    elseif ($tagValue)
    {
        $GLOBALS[$arParams['FILTER_NAME']] = array(
            'PROPERTY_TAGS' => $tagValue['ID']
        );
    }
}

if (isset($arResult['VARIABLES']['SECTION_ID']) && intval($arResult['VARIABLES']['SECTION_ID']) > 0)
{
    $GLOBALS[$arParams['FILTER_NAME']] = array(
        'IBLOCK_SECTION_ID' => intval($arResult['VARIABLES']['SECTION_ID']),
        'INCLUDE_SUBSECTIONS' => 'Y'
    );
}

$template = $arParams['LIST_TEMPLATE'] != "CUSTOM" ? Options\Base::getInstance(SITE_ID)->getValue('blogListTemplate') : $arParams['LIST_TEMPLATE'];

if ($template == "blog_type1" && intval($arParams["NEWS_COUNT"]) % 3 != 0)
    $arParams["NEWS_COUNT"] = (int) 3 * round(intval($arParams["NEWS_COUNT"]) / 3);
elseif (($template == "blog_type2" || $template == "blog_type3") && intval($arParams["NEWS_COUNT"]) % 2 != 0)
    $arParams["NEWS_COUNT"] = (int) 2 * round(intval($arParams["NEWS_COUNT"]) / 2);

$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	$template,
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NEWS_COUNT" => $arParams["NEWS_COUNT"],
		"SORT_BY1" => $arParams["SORT_BY1"],
		"SORT_ORDER1" => $arParams["SORT_ORDER1"],
		"SORT_BY2" => $arParams["SORT_BY2"],
		"SORT_ORDER2" => $arParams["SORT_ORDER2"],
		"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME" => $arParams['FILTER_NAME'],
		"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],

		"PARENT_SECTION" => ($bFilterInSection) ? $arResult["VARIABLES"]["SECTION_ID"] : '',
		"PARENT_SECTION_CODE" => ($bFilterInSection) ? $arResult["VARIABLES"]["SECTION_CODE"] : '',
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
            

		"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"SHOW_LAZY_LOAD_BTN" => $arParams["SHOW_LAZY_LOAD_BTN"],
		"LAZY_LOAD_BTN_TEXT" => $arParams["LAZY_LOAD_BTN_TEXT"],
                "SIDEBAR_SHOW_ALL" => $arParams["SIDEBAR_SHOW_ALL"],
                "SIDEBAR_SHOW_ALL_TEXT" => $arParams["SIDEBAR_SHOW_ALL_TEXT"],
                "LIST_PAGE_TYPE" => (empty($arParams['LIST_CARDS_TYPE'])) ? 'type1' : $arParams['LIST_CARDS_TYPE'],
                "SHOW_SUBSCRIBE_BLOCK" => $arParams['SHOW_SUBSCRIBE_BLOCK']
	),
	$component
);

if (isset($arResult['VARIABLES']['SECTION_ID']) && intval($arResult['VARIABLES']['SECTION_ID']) > 0)
{
    Tools\Seo::setPagePropertiesBySection($arParams, $arResult['VARIABLES']['SECTION_ID']);
}

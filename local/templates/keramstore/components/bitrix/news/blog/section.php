<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Alpha\Layout;

$this->setFrameMode(true);

Layout\Partial::getInstance(__DIR__)->render('section.php', array (
    'arParams' => $arParams,
    'arResult' => $arResult,
    'component' => $component,
), false, false);
?>

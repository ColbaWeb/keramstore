<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DATE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"USE_SHARE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_USE_SHARE"),
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"VALUE" => "Y",
		"DEFAULT" =>"N",
		"REFRESH"=> "Y",
	),
        
);

$arTemplateParameters['LIST_TEMPLATE'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('T_TPL_LIST_TEMPLATE'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'Y',
		'DEFAULT' => 'CUSTOM',
		'VALUES' => array (
                    'MODULE' => GetMessage('T_TPL_LIST_TEMPLATE_MODULE'),
                    'CUSTOM' => GetMessage('T_TPL_LIST_TEMPLATE_CUSTOM'),
                )
	);

if ($arCurrentValues["LIST_TEMPLATE"] == "CUSTOM")
{
    $arTemplateParameters["LIST_TEMPLATE_CUSTOM"] = array(
            "NAME" => GetMessage("T_TPL_LIST_TEMPLATE_CUSTOM_STRING"),
            "TYPE" => "STRING",
            "DEFAULT" => "type1",
            "PARENT" => "VISUAL"
    );
}

$arTemplateParameters['DETAIL_TEMPLATE'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('T_TPL_DETAIL_TEMPLATE'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'Y',
		'DEFAULT' => 'CUSTOM',
		'VALUES' => array (
                    'MODULE' => GetMessage('T_TPL_DETAIL_TEMPLATE_MODULE'),
                    'CUSTOM' => GetMessage('T_TPL_DETAIL_TEMPLATE_CUSTOM'),
                )
	);

if ($arCurrentValues["DETAIL_TEMPLATE"] == "CUSTOM")
{
    $arTemplateParameters["DETAIL_TEMPLATE_CUSTOM"] = array(
            "NAME" => GetMessage("T_TPL_DETAIL_TEMPLATE_CUSTOM_STRING"),
            "TYPE" => "STRING",
            "DEFAULT" => "type1",
            "PARENT" => "VISUAL"
    );
}

$arTemplateParameters["SHOW_LAZY_LOAD_BTN"] = array(
        "NAME" => GetMessage("T_CMP_SHOW_LAZY_LOAD_BTN"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "Y",
        "REFRESH" => "Y",
        "PARENT" => "LIST_SETTINGS"
);

if ($arCurrentValues["SHOW_LAZY_LOAD_BTN"] == "Y")
{
    $arTemplateParameters["LAZY_LOAD_BTN_TEXT"] = array(
            "NAME" => GetMessage("T_CMP_LAZY_LOAD_BTN_TEXT"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
            "PARENT" => "LIST_SETTINGS"
    );
}

$arTemplateParameters["LIST_CARDS_TYPE"] = array(
        "NAME" => GetMessage("T_TPL_LIST_CARDS_TYPE"),
        "TYPE" => "LIST",
        "MULTIPLE" => "N",
        "VALUES" => array(
            'type1' => GetMessage("T_TPL_LIST_CARDS_TYPE_1"),
            'type2' => GetMessage("T_TPL_LIST_CARDS_TYPE_2"),
            'type3' => GetMessage("T_TPL_LIST_CARDS_TYPE_3"),
        ),
        "DEFAULT" => 'type1',
        "PARENT" => "LIST_SETTINGS"
);

$arTemplateParameters["DETAIL_PAGE_TYPE"] = array(
        "NAME" => GetMessage("T_TPL_DETAIL_PAGE_TYPE"),
        "TYPE" => "LIST",
        "MULTIPLE" => "N",
        "VALUES" => array(
            'type1' => GetMessage("T_TPL_DETAIL_PAGE_TYPE_1"),
            'type2' => GetMessage("T_TPL_DETAIL_PAGE_TYPE_2")
        ),
        "DEFAULT" => 'type1',
);

$arTemplateParameters["USE_SUBSECTIONS_TAGS_FILTER"] = array(
        "NAME" => GetMessage("T_CMP_USE_SUBSECTIONS_TAGS_FILTER"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
        "PARENT" => "LIST_SETTINGS"
);

$arTemplateParameters["SIDEBAR_SHOW_ALL"] = array(
        "NAME" => GetMessage("T_CMP_SIDEBAR_SHOW_ALL"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "Y",
        "REFRESH" => "Y"
);

if ($arCurrentValues["SIDEBAR_SHOW_ALL"] == "Y")
{
    $arTemplateParameters["SIDEBAR_SHOW_ALL_TEXT"] = array(
            "NAME" => GetMessage("T_CMP_SIDEBAR_SHOW_ALL_TEXT"),
            "TYPE" => "STRING",
            "DEFAULT" => "���",
    );
}
$arTemplateParameters["DETAIL_SHOW_TAGS_TOP"] = array(
        "NAME" => GetMessage("T_CMP_DETAIL_SHOW_TAGS_TOP"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "Y",
        "PARENT" => "DETAIL_SETTINGS"
);
$arTemplateParameters["DETAIL_SHOW_TAGS_BOTTOM"] = array(
        "NAME" => GetMessage("T_CMP_DETAIL_SHOW_TAGS_BOTTOM"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "Y",
        "PARENT" => "DETAIL_SETTINGS"
);

$arTemplateParameters["SHOW_SUBSCRIBE_BLOCK"] = array(
        "NAME" => GetMessage("T_CMP_SHOW_SUBSCRIBE_BLOCK"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "Y"
);
?>

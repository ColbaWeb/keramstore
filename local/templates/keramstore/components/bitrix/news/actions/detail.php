<?
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="actions-detail">
    <?
        Layout\Partial::getInstance(__DIR__)->render('detail.php', array (
            'arParams' => $arParams,
            'arResult' => $arResult,
            'component' => $component,
        ), false, false);
    
        if (!empty($GLOBALS['ELEMENT_ID']))
        {
            $arElement = Tools\Cache::CIBlockElement_GetByID($GLOBALS['ELEMENT_ID'], true);
            
            if (!empty($arElement) && isset($arElement['PROPERTIES']['ITEMS']) && $arElement['PROPERTIES']['ITEMS']['USER_TYPE'] == "AlphaProductConditions" && !empty($arElement['PROPERTIES']['ITEMS']['VALUE']))
            {
                if (CheckSerializedData($arElement['PROPERTIES']['ITEMS']['VALUE']))
                {
                    $filter = new Tools\Filter;
                    $arFilter = $filter->getFilterFromConditions(unserialize($arElement['PROPERTIES']['ITEMS']['VALUE']));
                    
                    Layout\Partial::getInstance(__DIR__)->render('detail/products.php', array (
                        'arParams' => $arParams,
                        'arResult' => $arResult,
                        'arFilter' => $arFilter,
                        'component' => $component,
                    ), false, false);
                }
                
            }
        }
        
    ?>
    
    
</div>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\ModuleManager,
    Bitrix\Main\Loader,
    Bitrix\Main\Web\Json,
    Bitrix\Iblock,
    Bitrix\Catalog,
    Bitrix\Currency;

if(!CModule::IncludeModule("iblock"))
	return;

$mediaProperty = array(
	"" => GetMessage("MAIN_NO"),
);
$sliderProperty = array(
	"" => GetMessage("MAIN_NO"),
);
$propertyList = CIBlockProperty::GetList(
	array("sort"=>"asc", "name"=>"asc"),
	array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"])
);
while ($property = $propertyList->Fetch())
{
	$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	$id = $property["CODE"]? $property["CODE"]: $property["ID"];
	if ($property["PROPERTY_TYPE"] == "S")
	{
		$mediaProperty[$id] = "[".$id."] ".$property["NAME"];
	}
	if ($property["PROPERTY_TYPE"] == "F")
	{
		$sliderProperty[$id] = "[".$id."] ".$property["NAME"];
	}
}

$arTemplateParameters = array(
	"DISPLAY_DATE" => array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	
	
);

// catalog parameters

$catalogIncluded = Loader::includeModule('catalog');
$arSort = CIBlockParameters::GetElementSortFields(
    array('SHOWS', 'SORT', 'TIMESTAMP_X', 'NAME', 'ID', 'ACTIVE_FROM', 'ACTIVE_TO'),
    array('KEY_LOWERCASE' => 'Y')
);

$offersIblock = $arPrice = array();
if ($catalogIncluded)
{
    $iterator = Catalog\CatalogIblockTable::getList(array(
        'select' => array('IBLOCK_ID'),
        'filter' => array('!=PRODUCT_IBLOCK_ID' => 0)
    ));
    while ($row = $iterator->fetch())
        $offersIblock[$row['IBLOCK_ID']] = true;
    unset($row, $iterator);
    
    $arSort = array_merge($arSort, CCatalogIBlockParameters::GetCatalogSortFields());
    if (isset($arSort['CATALOG_AVAILABLE']))
        unset($arSort['CATALOG_AVAILABLE']);
    $arPrice = CCatalogIBlockParameters::getPriceTypesList();
}

$arAscDesc = array(
    'asc' => GetMessage('IBLOCK_SORT_ASC'),
    'desc' => GetMessage('IBLOCK_SORT_DESC'),
);

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$iblockFilter = !empty($arCurrentValues['CATALOG_IBLOCK_TYPE'])
    ? array('TYPE' => $arCurrentValues['CATALOG_IBLOCK_TYPE'], 'ACTIVE' => 'Y')
    : array('ACTIVE' => 'Y');

$rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $iblockFilter);
while ($arr = $rsIBlock->Fetch())
{
    $id = (int)$arr['ID'];
    if (isset($offersIblock[$id]))
        continue;
    $arIBlock[$id] = '['.$id.'] '.$arr['NAME'];
}
unset($id, $arr, $rsIBlock, $iblockFilter);

$arAllPropList = array();
$arListPropList = array();
$arHighloadPropList = array();

$boolSKU = false;
if ($catalogIncluded && isset($arCurrentValues['CATALOG_IBLOCK_ID']) && !empty($arCurrentValues['CATALOG_IBLOCK_ID']))
{
	$arSKU = CCatalogSKU::GetInfoByProductIBlock($arCurrentValues['CATALOG_IBLOCK_ID']);
	$boolSKU = !empty($arSKU) && is_array($arSKU);
}

if (isset($arCurrentValues['CATALOG_IBLOCK_ID']) && !empty($arCurrentValues['CATALOG_IBLOCK_ID']))
{
    $rsProps = CIBlockProperty::GetList(
		array('SORT' => 'ASC', 'ID' => 'ASC'),
		array('IBLOCK_ID' => $arCurrentValues['CATALOG_IBLOCK_ID'], 'ACTIVE' => 'Y')
	);
	while ($arProp = $rsProps->Fetch())
	{
		$strPropName = '['.$arProp['ID'].']'.('' != $arProp['CODE'] ? '['.$arProp['CODE'].']' : '').' '.$arProp['NAME'];
		if ('' == $arProp['CODE'])
		{
			$arProp['CODE'] = $arProp['ID'];
		}

		$arAllPropList[$arProp['CODE']] = $strPropName;

		if ('F' == $arProp['PROPERTY_TYPE'])
		{
			$arFilePropList[$arProp['CODE']] = $strPropName;
		}

		if ('L' == $arProp['PROPERTY_TYPE'])
		{
			$arListPropList[$arProp['CODE']] = $strPropName;
		}

		if ('S' == $arProp['PROPERTY_TYPE'] && 'directory' == $arProp['USER_TYPE'] && CIBlockPriceTools::checkPropDirectory($arProp))
		{
			$arHighloadPropList[$arProp['CODE']] = $strPropName;
		}
	}
}


$arTemplateParameters = array_merge($arTemplateParameters, array (
    
        'CATALOG_SECTION_TITLE' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_SECTION_TITLE'),
            'TYPE' => 'STRING',
            'DEFAULT' => ''
        ),
    
        'CATALOG_IBLOCK_TYPE' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_IBLOCK_TYPE'),
            'TYPE' => 'LIST',
            'VALUES' => $arIBlockType,
            'REFRESH' => 'Y',
        ),
        'CATALOG_IBLOCK_ID' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_IBLOCK_IBLOCK'),
            'TYPE' => 'LIST',
            'ADDITIONAL_VALUES' => 'Y',
            'VALUES' => $arIBlock,
            'REFRESH' => 'Y',
        ),
    
        'CATALOG_ELEMENT_SORT_FIELD' => array(
            'PARENT' => 'SORT_SETTINGS',
            'NAME' => GetMessage('CP_CATALOG_ELEMENT_SORT_FIELD'),
            'TYPE' => 'VISUAL',
            'VALUES' => $arSort,
            'ADDITIONAL_VALUES' => 'Y',
            'DEFAULT' => 'sort',
        ),
        'CATALOG_ELEMENT_SORT_ORDER' => array(
            'PARENT' => 'SORT_SETTINGS',
            'NAME' => GetMessage('CP_CATALOG_ELEMENT_SORT_ORDER'),
            'TYPE' => 'VISUAL',
            'VALUES' => $arAscDesc,
            'DEFAULT' => 'asc',
            'ADDITIONAL_VALUES' => 'Y',
        ),
        'CATALOG_ELEMENT_SORT_FIELD2' => array(
            'PARENT' => 'SORT_SETTINGS',
            'NAME' => GetMessage('CP_CATALOG_ELEMENT_SORT_FIELD2'),
            'TYPE' => 'VISUAL',
            'VALUES' => $arSort,
            'ADDITIONAL_VALUES' => 'Y',
            'DEFAULT' => 'id',
        ),
        'CATALOG_ELEMENT_SORT_ORDER2' => array(
            'PARENT' => 'SORT_SETTINGS',
            'NAME' => GetMessage('CP_CATALOG_ELEMENT_SORT_ORDER2'),
            'TYPE' => 'VISUAL',
            'VALUES' => $arAscDesc,
            'DEFAULT' => 'desc',
            'ADDITIONAL_VALUES' => 'Y',
        ),
    
        'CATALOG_PAGE_ELEMENT_COUNT' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_PAGE_ELEMENT_COUNT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '12'
        ),
    
        'CATALOG_LINE_ELEMENT_COUNT' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_LINE_ELEMENT_COUNT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '6'
        ),
    
        'CATALOG_LABEL_PROP' => array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_CATALOG_LABEL_PROP'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'Y',
		'VALUES' => $arListPropList
	),
    
        'CATALOG_ADD_PICT_PROP' => array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_CATALOG_ADD_PICT_PROP'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arFilePropList
	),
    
        'CATALOG_PRICE_CODE' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_PRICE_CODE'),
            'TYPE' => 'LIST',
            'MULTIPLE' => 'Y',
            'VALUES' => $arPrice,
        ),
        'CATALOG_USE_PRICE_COUNT' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_USE_PRICE_COUNT'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N',
        ),
        'CATALOG_SHOW_PRICE_COUNT' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_SHOW_PRICE_COUNT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '1',
        ),
        'CATALOG_PRICE_VAT_INCLUDE' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_VAT_INCLUDE'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
        ),
        'CATALOG_BASKET_URL' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_BASKET_URL'),
            'TYPE' => 'STRING',
            'DEFAULT' => '/personal/basket.php',
        ),
        'CATALOG_ACTION_VARIABLE' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_ACTION_VARIABLE'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'action',
        ),
        'CATALOG_PRODUCT_ID_VARIABLE' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_PRODUCT_ID_VARIABLE'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'id',
        ),
        'CATALOG_USE_PRODUCT_QUANTITY' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_USE_PRODUCT_QUANTITY'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N',
            'REFRESH' => 'Y',
        ),
        'CATALOG_PRODUCT_QUANTITY_VARIABLE' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_PRODUCT_QUANTITY_VARIABLE'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'quantity',
            'HIDDEN' => (isset($arCurrentValues['CATALOG_USE_PRODUCT_QUANTITY']) && $arCurrentValues['CATALOG_USE_PRODUCT_QUANTITY'] === 'Y' ? 'N' : 'Y')
        ),
        'CATALOG_ADD_PROPERTIES_TO_BASKET' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_ADD_PROPERTIES_TO_BASKET'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
            'REFRESH' => 'Y'
        ),
    
        'CATALOG_CONVERT_CURRENCY' => array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_CONVERT_CURRENCY'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N',
            'REFRESH' => 'Y',
        ),
    )
);

if (isset($arCurrentValues['CATALOG_CONVERT_CURRENCY']) && $arCurrentValues['CATALOG_CONVERT_CURRENCY'] === 'Y')
    {
        $arTemplateParameters['CATALOG_CURRENCY_ID'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => GetMessage('CP_CATALOG_CURRENCY_ID'),
            'TYPE' => 'LIST',
            'VALUES' => Currency\CurrencyManager::getCurrencyList(),
            'DEFAULT' => Currency\CurrencyManager::getBaseCurrency(),
            'ADDITIONAL_VALUES' => 'Y',
        );
    }
    
$arTemplateParameters = array_merge($arTemplateParameters, array (
    
    'CATALOG_HIDE_NOT_AVAILABLE' => array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_CATALOG_HIDE_NOT_AVAILABLE'),
        'TYPE' => 'LIST',
        'DEFAULT' => 'N',
        'VALUES' => array(
            'Y' => GetMessage('CP_CATALOG_HIDE_NOT_AVAILABLE_HIDE'),
            'L' => GetMessage('CP_CATALOG_HIDE_NOT_AVAILABLE_LAST'),
            'N' => GetMessage('CP_CATALOG_HIDE_NOT_AVAILABLE_SHOW')
        ),
        'ADDITIONAL_VALUES' => 'N'
    ),
    
    'CATALOG_HIDE_NOT_AVAILABLE_OFFERS' => array(
        'PARENT' => 'DATA_SOURCE',
        'NAME' => GetMessage('CP_CATALOG_HIDE_NOT_AVAILABLE_OFFERS'),
        'TYPE' => 'LIST',
        'DEFAULT' => 'N',
        'VALUES' => array(
            'Y' => GetMessage('CP_CATALOG_HIDE_NOT_AVAILABLE_OFFERS_HIDE'),
            'L' => GetMessage('CP_CATALOG_HIDE_NOT_AVAILABLE_OFFERS_SUBSCRIBE'),
            'N' => GetMessage('CP_CATALOG_HIDE_NOT_AVAILABLE_OFFERS_SHOW')
        )
    )
    
));


if ($boolSKU)
{
    
    $arTemplateParameters['CATALOG_PRODUCT_DISPLAY_MODE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_CATALOG_PRODUCT_DISPLAY_MODE'),
        'TYPE' => 'LIST',
        'MULTIPLE' => 'N',
        'ADDITIONAL_VALUES' => 'N',
        'REFRESH' => 'Y',
        'DEFAULT' => 'N',
        'VALUES' => array(
            'N' => GetMessage('CP_CATALOG_DML_SIMPLE'),
            'Y' => GetMessage('CP_CATALOG_DML_EXT')
        )
    );

    $arAllOfferPropList = array();
    $arFileOfferPropList = array(
        '-' => "-"
    );
    $arTreeOfferPropList = array(
        '-' => "-"
    );
    $rsProps = CIBlockProperty::GetList(
                    array('SORT' => 'ASC', 'ID' => 'ASC'),
                    array('IBLOCK_ID' => $arSKU['IBLOCK_ID'], 'ACTIVE' => 'Y')
    );
    while ($arProp = $rsProps->Fetch())
    {
        if ($arProp['ID'] == $arSKU['SKU_PROPERTY_ID'])
            continue;
        $arProp['USER_TYPE'] = (string) $arProp['USER_TYPE'];
        $strPropName = '[' . $arProp['ID'] . ']' . ('' != $arProp['CODE'] ? '[' . $arProp['CODE'] . ']' : '') . ' ' . $arProp['NAME'];
        if ('' == $arProp['CODE'])
            $arProp['CODE'] = $arProp['ID'];
        $arAllOfferPropList[$arProp['CODE']] = $strPropName;
        if ('F' == $arProp['PROPERTY_TYPE'])
            $arFileOfferPropList[$arProp['CODE']] = $strPropName;
        if ('N' != $arProp['MULTIPLE'])
            continue;
        if (
                'L' == $arProp['PROPERTY_TYPE'] || 'E' == $arProp['PROPERTY_TYPE'] || ('S' == $arProp['PROPERTY_TYPE'] && 'directory' == $arProp['USER_TYPE'] && CIBlockPriceTools::checkPropDirectory($arProp))
        )
            $arTreeOfferPropList[$arProp['CODE']] = $strPropName;
    }
    
    $arTemplateParameters['CATALOG_OFFER_ADD_PICT_PROP'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => GetMessage('CP_CATALOG_OFFER_ADD_PICT_PROP'),
        'TYPE' => 'LIST',
        'MULTIPLE' => 'N',
        'ADDITIONAL_VALUES' => 'N',
        'REFRESH' => 'N',
        'DEFAULT' => '-',
        'VALUES' => $arFileOfferPropList
    );
}

$arTemplateParameters['CATALOG_SHOW_MAX_QUANTITY'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => GetMessage('CP_CATALOG_SHOW_MAX_QUANTITY'),
    'TYPE' => 'LIST',
    'REFRESH' => 'Y',
    'MULTIPLE' => 'N',
    'VALUES' => array(
        'N' => GetMessage('CP_CATALOG_SHOW_MAX_QUANTITY_N'),
        'Y' => GetMessage('CP_CATALOG_SHOW_MAX_QUANTITY_Y'),
        'M' => GetMessage('CP_CATALOG_SHOW_MAX_QUANTITY_M')
    ),
    'DEFAULT' => array('N')
);

	if (isset($arCurrentValues['CATALOG_SHOW_MAX_QUANTITY']))
	{
		if ($arCurrentValues['CATALOG_SHOW_MAX_QUANTITY'] !== 'N')
		{
			$arTemplateParameters['CATALOG_MESS_SHOW_MAX_QUANTITY'] = array(
				'PARENT' => 'VISUAL',
				'NAME' => GetMessage('CP_CATALOG_MESS_SHOW_MAX_QUANTITY'),
				'TYPE' => 'STRING',
				'DEFAULT' => GetMessage('CP_CATALOG_MESS_SHOW_MAX_QUANTITY_DEFAULT')
			);
		}

		if ($arCurrentValues['CATALOG_SHOW_MAX_QUANTITY'] === 'M')
		{
			$arTemplateParameters['CATALOG_RELATIVE_QUANTITY_FACTOR'] = array(
				'PARENT' => 'VISUAL',
				'NAME' => GetMessage('CP_CATALOG_RELATIVE_QUANTITY_FACTOR'),
				'TYPE' => 'STRING',
				'DEFAULT' => '5'
			);
			$arTemplateParameters['CATALOG_MESS_RELATIVE_QUANTITY_MANY'] = array(
				'PARENT' => 'VISUAL',
				'NAME' => GetMessage('CP_CATALOG_MESS_RELATIVE_QUANTITY_MANY'),
				'TYPE' => 'STRING',
				'DEFAULT' => GetMessage('CP_CATALOG_MESS_RELATIVE_QUANTITY_MANY_DEFAULT')
			);
			$arTemplateParameters['CATALOG_MESS_RELATIVE_QUANTITY_FEW'] = array(
				'PARENT' => 'VISUAL',
				'NAME' => GetMessage('CP_CATALOG_MESS_RELATIVE_QUANTITY_FEW'),
				'TYPE' => 'STRING',
				'DEFAULT' => GetMessage('CP_CATALOG_MESS_RELATIVE_QUANTITY_FEW_DEFAULT')
			);
		}
	}
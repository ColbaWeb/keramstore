<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (!empty($arResult["ACCOUNT_LIST"]))
{
    foreach($arResult["ACCOUNT_LIST"] as $accountValue)
    {
    ?>
        <div class="sale-personal-account-wallet-container">	
            <div class="sale-personal-account-wallet-sum"><?= $accountValue['SUM'] ?></div>
            <div class="sale-personal-account-wallet-date">
                <?= Bitrix\Main\Localization\Loc::getMessage('SPA_BILL_AT') ?> <?= $arResult["DATE"]; ?>
            </div>  
        </div>
    <?
    }
}
?>


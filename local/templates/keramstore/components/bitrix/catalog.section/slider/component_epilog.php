<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Grid\Declension;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;

Loc::loadMessages(__DIR__ . "/template.php");

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
	}

	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);

	if ($loadCurrency)
	{
		?>
		<script>
                    BX.ready(function () {
                        var interval = setInterval(function () {
                            if (!!window.BX.Currency)
                            {
                                window.BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
                                clearTimeout(interval);
                            }
                        }, 1000);
                        
                    });
		</script>
		<?
	}
}
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;

use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

Layout\Assets::getInstance()->addLess(array (
    SITE_TEMPLATE_PATH . '/less/tabs-with-goods.less',
));

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
	}

	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);

	if ($loadCurrency)
	{
		?>
		<script>
                    BX.ready(function () {
                        var interval = setInterval(function () {
                            if (!!window.BX.Currency)
                            {
                                window.BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
                                clearTimeout(interval);
                            }
                        }, 1000);
                        
                    });
		</script>
		<?
	}
}
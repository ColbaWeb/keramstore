<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

if (intval($arParams['INTERACTIVE_IMAGE_ID']) > 0 && Loader::includeModule("skyweb24.interactivepictures"))
{
    $GLOBALS['APPLICATION']->IncludeComponent(
        "skyweb24:interactivepictures",
        "section",
        array_merge($generalParams, array (
            'IMAGE' => intval($arParams['INTERACTIVE_IMAGE_ID']),
            'VIEW_MODE' => $arParams['INTERACTIVE_IMAGE_VIEWMODE'],
            'ITEMS_CONTAINER_NAME' => $containerName
        )),
        $component,
        array (
            'HIDE_ICONS' => 'Y'
        )
    );
}
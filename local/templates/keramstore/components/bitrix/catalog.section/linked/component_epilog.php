<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Grid\Declension;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;

Loc::loadMessages(__DIR__ . "/template.php");

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;

if(is_object($this->__parent) && isset($this->__parent->arParams) && $this->__parent->arParams['AJAX_MODE'] == "Y")
{
    Layout\Assets::getInstance(SITE_ID)->addLess(SITE_TEMPLATE_PATH . "/less/item-detail.less");
}

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
	}

	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);

	if ($loadCurrency)
	{
		?>
		<script>
                    BX.ready(function () {
                        var interval = setInterval(function () {
                            if (!!window.BX.Currency)
                            {
                                window.BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
                                clearTimeout(interval);
                            }
                        }, 1000);
                        
                    });
		</script>
		<?
	}
}

if (isset($templateData['NAV_RECORD_COUNT']) && !empty($templateData['NAV_RECORD_COUNT']))
{
    
    if(!$this->__template)
        $this->InitComponentTemplate();
    
    
    $itemDeclension = new Declension(Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_1'), Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_2'), Loc::getMessage('CT_BCSL_ELEMENT_COUNT_DECLENSION_3'));
    

    $this->__template->SetViewTarget('catalog_section_nav_record_count_print');
    ?>
        <div class="quantity font-body-2"><span class='count'><?=$templateData['NAV_RECORD_COUNT']?></span> <span class="quantity-text"><?=$itemDeclension->get($templateData['NAV_RECORD_COUNT'])?></span></div>
    <?
    $this->__template->EndViewTarget();
    
    $this->__template->SetViewTarget('catalog_section_nav_record_count_mobile_print');
    ?>
        <div class="mobile-quantity font-body-small"><span class="count"><?=$templateData['NAV_RECORD_COUNT']?></span> <span class="quantity-text"><?=$itemDeclension->get($templateData['NAV_RECORD_COUNT'])?></span></div>
    <?
    $this->__template->EndViewTarget();
}

//	lazy load and big data json answers
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest() && ($request->get('action') === 'showMore' || $request->get('action') === 'deferredLoad'))
{
	$content = ob_get_contents();
	ob_end_clean();

	list(, $itemsContainer) = explode('<!-- items-container -->', $content);
	list(, $paginationContainer) = explode('<!-- pagination-container -->', $content);
	list(, $epilogue) = explode('<!-- component-end -->', $content);

	if ($arParams['AJAX_MODE'] === 'Y')
	{
		$component->prepareLinks($paginationContainer);
	}

	$component::sendJsonAnswer(array(
		'items' => $itemsContainer,
		'pagination' => $paginationContainer,
		'epilogue' => $epilogue,
	));
}
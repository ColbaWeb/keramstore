(function (window) {
    'use strict';

    if (window.Alpha.CatalogSetConstructor)
        return;

    window.Alpha.CatalogSetConstructor = function (params)
    {
        this.jsId = params.jsId || "";
        this.ajaxPath = params.ajaxPath || "";
        this.currency = params.currency || "";
        this.lid = params.lid || "";
        this.iblockId = params.iblockId || "";
        this.basketUrl = params.basketUrl || "";
        this.setIds = params.setIds || null;
        this.offersCartProps = params.offersCartProps || null;
        this.itemsRatio = params.itemsRatio || null;
        this.messages = params.messages;

        this.canBuy = params.canBuy;
        this.mainElementPrice = params.mainElementPrice || 0;
        this.mainElementOldPrice = params.mainElementOldPrice || 0;
        this.mainElementDiffPrice = params.mainElementDiffPrice || 0;
        this.mainElementBasketQuantity = params.mainElementBasketQuantity || 1;

        this.parentCont = BX(params.parentContId) || null;
        
        
        this.setItems = this.parentCont.querySelector("[data-role='set-items']");

        this.setPriceCont = this.parentCont.querySelector("[data-role='set-price']");
        this.setOldPriceCont = this.parentCont.querySelector("[data-role='set-old-price']");
        
        BX.ready(BX.delegate(this.init, this));

        
    };
    
    window.Alpha.CatalogSetConstructor.prototype = {

        init: function () {
            
            var items = this.setItems.querySelectorAll('[data-entity="set-item-changer"]');
            if (items.length > 0)
            {
                for(var i = 0; i < items.length; i++)
                {
                    BX.bind(BX(items[i]), 'change', BX.proxy(this.changeSetState, this));
                }
            }
            

            var buyButton = this.parentCont.querySelector("[data-role='set-buy-btn']");

            if (this.canBuy)
            {
                BX.bind(buyButton, "click", BX.proxy(this.addToBasket, this));
            }
        },
        
        changeSetState: function (event)
        {
            var target = BX.getEventTarget(event),
                item = BX.findParent(target, {
                    className: 'set-item'
                }),
                    itemId,
                    itemName,
                    itemUrl,
                    itemImg,
                    itemPrintPrice,
                    itemPrice,
                    itemPrintOldPrice,
                    itemOldPrice,
                    itemDiffPrice,
                    itemMeasure,
                    itemBasketQuantity;
            
            if (!!item)
            {
                itemId = parseInt(item.getAttribute("data-id"));
                itemName = item.getAttribute("data-name");
                itemUrl = item.getAttribute("data-url");
                itemImg = item.getAttribute("data-img");
                itemPrintPrice = item.getAttribute("data-print-price");
                itemPrice = item.getAttribute("data-price");
                itemPrintOldPrice = item.getAttribute("data-print-old-price");
                itemOldPrice = item.getAttribute("data-old-price");
                itemDiffPrice = item.getAttribute("data-diff-price");
                itemMeasure = item.getAttribute("data-measure");
                itemBasketQuantity = item.getAttribute("data-quantity");
                
                if (target.checked)
                {
                    // add to setIds
                    if (this.setIds.indexOf(itemId) == -1)
                    {
                        this.setIds.push(itemId);
                    }
                }
                else
                {
                    // delete from setIds
                    for (var i = 0, l = this.setIds.length; i < l; i++)
                    {
                        if (this.setIds[i] == itemId)
                            this.setIds.splice(i, 1);
                    }
                }
                
                this.recountPrice();
                
                if (this.setIds.length <= 1)
                {
                    BX.addClass(this.parentCont.querySelector('[data-role="total-block"]'), 'd-none');
                }
                else
                {
                    BX.removeClass(this.parentCont.querySelector('[data-role="total-block"]'), 'd-none');
                }
            }
            
        },
        
        recountPrice: function () {
            
            var sumPrice = this.mainElementPrice * this.mainElementBasketQuantity,
		sumOldPrice = this.mainElementOldPrice * this.mainElementBasketQuantity,
                ratio, sumDiffDiscountPrice,
                items = this.setItems.querySelectorAll('.set-item');
        
            if (!!items)
            {
                for (var i = 0; i < items.length; i++)
                {
                    var itemId = parseInt(items[i].getAttribute("data-id"));
                    
                    if (!!itemId && this.setIds.indexOf(itemId) != -1)
                    {
                        ratio = Number(items[i].getAttribute("data-quantity")) || 1;
			sumPrice += Number(items[i].getAttribute("data-price"))*ratio;
			sumOldPrice += Number(items[i].getAttribute("data-old-price"))*ratio;
                        sumDiffDiscountPrice += Number(items[i].getAttribute("data-diff-price"))*ratio;
                    }
                }
                
                this.setPriceCont.innerHTML = BX.Currency.currencyFormat(sumPrice, this.currency, true);
                
                if (sumOldPrice > 0)
		{
			this.setOldPriceCont.innerHTML = BX.Currency.currencyFormat(sumOldPrice, this.currency, true);
                        BX.removeClass(this.setOldPriceCont, 'd-none');
		}
		else
		{
                        BX.addClass(this.setOldPriceCont, 'd-none');
			this.setOldPriceCont.innerHTML = '';
		}
            }
            
        },
        
        addToBasket: function () {
            
            var target = BX.proxy_context;
            
            //BX.showWait(target.parentNode);

            BX.ajax.post(
                    this.ajaxPath,
                    {
                        sessid: BX.bitrix_sessid(),
                        action: 'catalogSetAdd2Basket',
                        set_ids: this.setIds,
                        lid: this.lid,
                        iblockId: this.iblockId,
                        setOffersCartProps: this.offersCartProps,
                        itemsRatio: this.itemsRatio
                    },
                    BX.proxy(function (result)
                    {
                        //BX.closeWait();
                        
                        new window.Alpha.Messages({
                            type: 'default',
                            title: this.messages.ADD_TO_BASKET_TITLE,
                            message: this.messages.ADD_TO_BASKET_MESSAGE,
                            url: this.basketUrl
                        });
                        
                        BX.onCustomEvent('OnBasketChange');

                    }, this)
                    );
            
        }
    }
    
})(window);

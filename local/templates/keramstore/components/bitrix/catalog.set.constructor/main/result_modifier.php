<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;



if ($arResult["ELEMENT"]['DETAIL_PICTURE'] || $arResult["ELEMENT"]['PREVIEW_PICTURE'])
{
	$arFileTmp = CFile::ResizeImageGet(
		$arResult["ELEMENT"]['DETAIL_PICTURE'] ? $arResult["ELEMENT"]['DETAIL_PICTURE'] : $arResult["ELEMENT"]['PREVIEW_PICTURE'],
		array("width" => "250", "height" => "250"),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT
	);
	$arResult["ELEMENT"]['DETAIL_PICTURE'] = $arFileTmp;
}

$arResult["ELEMENT"]["DISPLAY_PROPERTIES"] = array ();
if (isset($arParams['MAIN_BLOCK_PROPERTY_CODE']) && !empty($arParams['MAIN_BLOCK_PROPERTY_CODE']))
{
    foreach ($arParams['MAIN_BLOCK_PROPERTY_CODE'] as $propCode)
    {
        if (!empty($propCode))
        {
            $arProperty = CIBlockProperty::GetList(Array(), Array("ACTIVE"=>"Y", "IBLOCK_ID" => $arResult["ELEMENT"]['IBLOCK_ID'],"CODE" => $propCode))->fetch();
            
            if ($arProperty)
            {
                if ($arProperty['MULTIPLE'] == "Y")
                {
                    $arProperty['VALUE'] = array ();
                    $rsPropValues = CIBlockElement::GetProperty($arResult["ELEMENT"]['IBLOCK_ID'], $arResult["ELEMENT"]['ID'], array(), Array("ID" => $arProperty['ID']));
                    while ($arPropValue = $rsPropValues->fetch())
                    {
                        if (!empty($arPropValue['VALUE']))
                            $arProperty['VALUE'][] = isset($arPropValue['VALUE_ENUM']) && !empty($arPropValue['VALUE_ENUM']) ? $arPropValue['VALUE_ENUM'] : $arPropValue['VALUE'];
                    }
                }
                else
                {
                    $arPropValue = CIBlockElement::GetProperty($arResult["ELEMENT"]['IBLOCK_ID'], $arResult["ELEMENT"]['ID'], array(), Array("ID" => $arProperty['ID']))->fetch();
                    if ($arPropValue)
                    {
                        $arProperty['VALUE'] = isset($arPropValue['VALUE_ENUM']) && !empty($arPropValue['VALUE_ENUM']) ? $arPropValue['VALUE_ENUM'] : $arPropValue['VALUE'];
                    }
                }

                if ($arProperty['USER_TYPE'] == "directory")
                {
                    $sTableName = $arProperty['USER_TYPE_SETTINGS']['TABLE_NAME'];
                    if (Loader::IncludeModule('highloadblock') && !empty($sTableName) && !empty($arProperty["VALUE"]))
                    {
                        $hlblock = HL\HighloadBlockTable::getRow(array(
                            'filter' => array('=TABLE_NAME' => $sTableName),
                        ));

                        if ($hlblock)
                        {
                            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                            $entityClass = $entity->getDataClass();

                            $arRecords = $entityClass::getList(array(
                                'filter' => array('UF_XML_ID' => $arProperty["VALUE"]),
                            ));

                            $arProperty['VALUE'] = array ();
                            foreach ($arRecords as $record)
                                $arProperty['VALUE'][] = $record['UF_NAME'];
                        }
                    }
                }

                $arResult["ELEMENT"]["DISPLAY_PROPERTIES"][] = $arProperty;
            }

        }
    }
    
}

$arDefaultSetIDs = array($arResult["ELEMENT"]["ID"]);
foreach (array("DEFAULT", "OTHER") as $type)
{
	foreach ($arResult["SET_ITEMS"][$type] as $key=>$arItem)
	{
		$arElement = array(
			"ID"=>$arItem["ID"],
			"NAME" =>$arItem["NAME"],
			"DETAIL_PAGE_URL"=>$arItem["DETAIL_PAGE_URL"],
			"DETAIL_PICTURE"=>$arItem["DETAIL_PICTURE"],
			"PREVIEW_PICTURE"=> $arItem["PREVIEW_PICTURE"],
			"PRICE_CURRENCY" => $arItem["PRICE_CURRENCY"],
			"PRICE_DISCOUNT_VALUE" => $arItem["PRICE_DISCOUNT_VALUE"],
			"PRICE_PRINT_DISCOUNT_VALUE" => $arItem["PRICE_PRINT_DISCOUNT_VALUE"],
			"PRICE_VALUE" => $arItem["PRICE_VALUE"],
			"PRICE_PRINT_VALUE" => $arItem["PRICE_PRINT_VALUE"],
			"PRICE_DISCOUNT_DIFFERENCE_VALUE" => $arItem["PRICE_DISCOUNT_DIFFERENCE_VALUE"],
			"PRICE_DISCOUNT_DIFFERENCE" => $arItem["PRICE_DISCOUNT_DIFFERENCE"],
			"CAN_BUY" => $arItem['CAN_BUY'],
			"SET_QUANTITY" => $arItem['SET_QUANTITY'],
			"MEASURE_RATIO" => $arItem['MEASURE_RATIO'],
			"BASKET_QUANTITY" => $arItem['BASKET_QUANTITY'],
			"MEASURE" => $arItem['MEASURE']
		);
		if ($arItem["PRICE_CONVERT_DISCOUNT_VALUE"])
			$arElement["PRICE_CONVERT_DISCOUNT_VALUE"] = $arItem["PRICE_CONVERT_DISCOUNT_VALUE"];
		if ($arItem["PRICE_CONVERT_VALUE"])
			$arElement["PRICE_CONVERT_VALUE"] = $arItem["PRICE_CONVERT_VALUE"];
		if ($arItem["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"])
			$arElement["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"] = $arItem["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"];

		if ($type == "DEFAULT")
			$arDefaultSetIDs[] = $arItem["ID"];
		if ($arItem['DETAIL_PICTURE'] || $arItem['PREVIEW_PICTURE'])
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arItem['DETAIL_PICTURE'] ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'],
				array("width" => "150", "height" => "150"),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT
			);
			$arElement['DETAIL_PICTURE'] = $arFileTmp;
		}

		$arResult["SET_ITEMS"][$type][$key] = $arElement;
	}
}
$arResult["DEFAULT_SET_IDS"] = $arDefaultSetIDs;
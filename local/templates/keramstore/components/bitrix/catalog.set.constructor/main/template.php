<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Reviews;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
	'CURRENCIES' => CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true)
);
$curJsId = $this->randString();
?>

<div class="set" id="bx-set-const-<?=$curJsId?>">
    <div class="h2"><?=GetMessage("CATALOG_SET_BUY_SET")?></div>
    <div class="main-element">
        <?
            echo Layout\Assets::showBackgroundImage($arResult["ELEMENT"]["DETAIL_PICTURE"]["src"], array(
                                "attributes" => array(
                                    "class=\"img\""

                                    )
                        ));
        ?>
        
        <div class="info">
            <div class="price-block">
                <div class="price font-body"><?=$arResult["ELEMENT"]["PRICE_PRINT_DISCOUNT_VALUE"]?></div>
                <div class="price-wrap">
                    <?
                    if (!($arResult["ELEMENT"]["PRICE_VALUE"] == $arResult["ELEMENT"]["PRICE_DISCOUNT_VALUE"]))
                    {
                    ?>
                        <div class="font-body-small price-old-block"><span class="price-old"><?=$arResult["ELEMENT"]["PRICE_PRINT_VALUE"]?></span></div>
                        <?
                        if (intval($arResult["ELEMENT"]["PRICE_DISCOUNT_PERCENT"]) > 0)
                        {
                            ?>
                            <div class="labels">
                                <span class="label action">-<?=$arResult["ELEMENT"]["PRICE_DISCOUNT_PERCENT"]?>%</span>
                            </div>
                            <?
                        }
                    }
                    ?>
                    
                    <div class="count font-body-small"><?=$arResult["ELEMENT"]["BASKET_QUANTITY"];?> <?=$arResult["ELEMENT"]["MEASURE"]["SYMBOL"];?></div>
                </div>
            </div>
            <div class="item-title font-title"><?=$arResult["ELEMENT"]["NAME"]?></div>
            
            <?
            if (!empty($arResult["ELEMENT"]["DISPLAY_PROPERTIES"]))
            {
                ?>
                <div class="elem-chars font-body-2">
                    <?
                    foreach ($arResult["ELEMENT"]["DISPLAY_PROPERTIES"] as $arProp)
                    {
                        if (empty($arProp['VALUE']))
                            continue;
                        
                        ?><div class="item"><span><?=$arProp['NAME']?>:</span><span><?=is_array($arProp['VALUE']) ? implode(", ", $arProp['VALUE']) : $arProp['VALUE']?></span></div><?
                    }
                    ?>
                </div>
                <?
            }
            ?>
            
        </div>
    </div>
    
    <div class="list" data-role="set-items">
        <?
        foreach($arResult["SET_ITEMS"]["DEFAULT"] as $key => $arItem)
        {
            if (!$arItem['CAN_BUY'])
                continue;
            ?>
            <div class="set-item"
                 data-id="<?= htmlspecialcharsbx($arItem["ID"]) ?>"
                     data-img="<?= htmlspecialcharsbx($arItem["DETAIL_PICTURE"]["src"]) ?>"
                     data-url="<?= htmlspecialcharsbx($arItem["DETAIL_PAGE_URL"]) ?>"
                     data-name="<?= htmlspecialcharsbx($arItem["NAME"]) ?>"
                     data-price="<?= htmlspecialcharsbx($arItem["PRICE_DISCOUNT_VALUE"]) ?>"
                     data-print-price="<?= htmlspecialcharsbx($arItem["PRICE_PRINT_DISCOUNT_VALUE"]) ?>"
                     data-old-price="<?= htmlspecialcharsbx($arItem["PRICE_VALUE"]) ?>"
                     data-print-old-price="<?= htmlspecialcharsbx($arItem["PRICE_PRINT_VALUE"]) ?>"
                     data-diff-price="<?= htmlspecialcharsbx($arItem["PRICE_DISCOUNT_DIFFERENCE_VALUE"]) ?>"
                     data-measure="<?= htmlspecialcharsbx($arItem["MEASURE"]["SYMBOL_RUS"]) ?>"
                     data-quantity="<?= htmlspecialcharsbx($arItem["BASKET_QUANTITY"]) ?>"
            >
                <input type="checkbox" data-entity="set-item-changer" checked="checked" id='set-item-<?=$arItem['ID']?>'>
                <label for="set-item-<?=$arItem['ID']?>">
                    <?
                        echo Layout\Assets::showBackgroundImage($arItem["DETAIL_PICTURE"]["src"], array(
                            "attributes" => array(
                                "class=\"img\""
                            )
                        ));
                        ?>
                    
                    <div class="info">
                        <div class="price-block">
                            <div class="price font-body-2 bold"><?=$arItem["PRICE_PRINT_DISCOUNT_VALUE"]?></div>
                            <div class="price-wrap">
                                <?
                                if ($arItem["PRICE_VALUE"] != $arItem["PRICE_DISCOUNT_VALUE"])
                                {
                                    ?>
                                    <div class="font-small price-old-block"><span class="price-old"><?=$arItem["PRICE_PRINT_VALUE"]?></span></div>
                                    <?
                                }
                                ?>
                                
                                <div class="count font-body-small"><?=$arItem["BASKET_QUANTITY"];?> <?=$arItem["MEASURE"]["SYMBOL"];?></div>
                            </div>

                        </div>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank" class="set-item-title font-title"><?=$arItem["NAME"]?></a>
                    </div>
                </label>
            </div>
            <?
        }
        
        foreach($arResult["SET_ITEMS"]["OTHER"] as $key => $arItem)
        {
            if (!$arItem['CAN_BUY'])
                continue;
            ?>
            <div class="set-item"
                    data-id="<?= $arItem["ID"] ?>"
                     data-img="<?= $arItem["DETAIL_PICTURE"]["src"] ?>"
                     data-url="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                     data-name="<?= $arItem["NAME"] ?>"
                     data-price="<?= $arItem["PRICE_DISCOUNT_VALUE"] ?>"
                     data-print-price="<?= $arItem["PRICE_PRINT_DISCOUNT_VALUE"] ?>"
                     data-old-price="<?= $arItem["PRICE_VALUE"] ?>"
                     data-print-old-price="<?= $arItem["PRICE_PRINT_VALUE"] ?>"
                     data-diff-price="<?= $arItem["PRICE_DISCOUNT_DIFFERENCE_VALUE"] ?>"
                     data-measure="<?= $arItem["MEASURE"]["SYMBOL_RUS"]; ?>"
                     data-quantity="<?= $arItem["BASKET_QUANTITY"]; ?>"
            >
                <input type="checkbox" data-entity="set-item-changer" id='set-item-<?=$arItem['ID']?>'>
                <label for="set-item-<?=$arItem['ID']?>">
                    <?
                        echo Layout\Assets::showBackgroundImage($arItem["DETAIL_PICTURE"]["src"], array(
                            "attributes" => array(
                                "class=\"img\""
                            )
                        ));
                        ?>
                    
                    <div class="info">
                        <div class="price-block">
                            <div class="price font-body-2 bold"><?=$arItem["PRICE_PRINT_DISCOUNT_VALUE"]?></div>
                            <div class="price-wrap">
                                <?
                                if ($arItem["PRICE_VALUE"] != $arItem["PRICE_DISCOUNT_VALUE"])
                                {
                                    ?>
                                    <div class="font-small price-old-block"><span class="price-old"><?=$arItem["PRICE_PRINT_VALUE"]?></span></div>
                                    <?
                                }
                                ?>
                                
                                <div class="count font-body-small"><?=$arItem["BASKET_QUANTITY"];?> <?=$arItem["MEASURE"]["SYMBOL"];?></div>
                            </div>

                        </div>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank" class="set-item-title font-title"><?=$arItem["NAME"]?></a>
                    </div>
                </label>
            </div>
            <?
        }
        ?>
        
        
        
    </div>
    <div class="total-block" data-role="total-block">
        <div class="sum">
            <div class="subtitle font-body-2"><?=GetMessage("CATALOG_SET_TOTAL_TITLE")?></div>
            <div class="price-block">
                <div class="price font-price-large bold" data-role="set-price"><?=$arResult["SET_ITEMS"]["PRICE"]?></div>
                
                <div class="price-wrap">
                    <?
                    if ($arResult['SHOW_DEFAULT_SET_DISCOUNT'])
                    {
                        ?>
                        <div class="price-old-block font-body-small">
                            <span class="price-old" data-role="set-old-price"><?=$arResult["SET_ITEMS"]["OLD_PRICE"]?></span>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="buttons"><a href="javascript:void(0)" data-role="set-buy-btn" <?=($arResult["ELEMENT"]["CAN_BUY"] ? '' : 'style="display: none;"')?> class="btn btn-primary btn-large"><?=GetMessage("CATALOG_SET_BUY")?><i class="icon icon-basket-white"></i></a>
        </div>
    </div>
</div>

<?
$arJsParams = array(
    "jsId" => $curJsId,
    "parentContId" => "bx-set-const-" . $curJsId,
    "ajaxPath" => $this->GetFolder() . '/ajax.php',
    "canBuy" => $arResult["ELEMENT"]["CAN_BUY"],
    "currency" => $arResult["ELEMENT"]["PRICE_CURRENCY"],
    "mainElementPrice" => $arResult["ELEMENT"]["PRICE_DISCOUNT_VALUE"],
    "mainElementOldPrice" => $arResult["ELEMENT"]["PRICE_VALUE"],
    "mainElementDiffPrice" => $arResult["ELEMENT"]["PRICE_DISCOUNT_DIFFERENCE_VALUE"],
    "mainElementBasketQuantity" => $arResult["ELEMENT"]["BASKET_QUANTITY"],
    "lid" => SITE_ID,
    "iblockId" => $arParams["IBLOCK_ID"],
    "basketUrl" => $arParams["BASKET_URL"],
    "setIds" => $arResult["DEFAULT_SET_IDS"],
    "offersCartProps" => $arParams["OFFERS_CART_PROPERTIES"],
    "itemsRatio" => $arResult["BASKET_QUANTITY"],
    "messages" => array(
        "ADD_TO_BASKET_TITLE" => GetMessage('CATALOG_SET_SUCCESS_ADD_TO_BASKET_TITLE'),
        "ADD_TO_BASKET_MESSAGE" => GetMessage("CATALOG_SET_SUCCESS_ADD_TO_BASKET_MESSAGE")
    )
);
?>
<script>
        BX.ready(function(){
                new window.Alpha.CatalogSetConstructor(<?= CUtil::PhpToJSObject($arJsParams, false, true, true) ?>);
        });
</script>
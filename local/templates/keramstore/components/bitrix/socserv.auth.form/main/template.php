<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$map = array (
    "bitrix24" => "bx24",
    "facebook" => "fb",
    "yandex" => "yandex",
    "mailru2" => "mail",
    "mymailru" => "mail",
    "openid-mail-ru" => "mail",
    "vkontakte" => "vk",
    "openid" => "openid",
    "livejournal" => "lj",
    "liveinternet" => "li",
    "blogger" => "blogger",
    "twitter" => "tw",
    "google" => "login-google",
    "liveid" => "life-id",
);
?>

<?foreach($arParams["~AUTH_SERVICES"] as $service):?>
    <a href="javascript:void(0)" onclick="BxShowAuthFloat('<?=$service["ID"]?>', '<?=$arParams["SUFFIX"]?>')" title="<?=htmlspecialcharsbx($service["NAME"])?>" class="item"><i class="icon icon-<?=isset($map[$service["ICON"]]) ? $map[$service["ICON"]] : $service["ICON"]?>"></i></a>
<? endforeach; ?>

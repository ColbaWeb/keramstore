<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/*TAGS*/
if ($arParams["SEARCH_PAGE"])
{
	if ($arResult["FIELDS"] && isset($arResult["FIELDS"]["TAGS"]))
	{
		$tags = array();
		foreach (explode(",", $arResult["FIELDS"]["TAGS"]) as $tag)
		{
			$tag = trim($tag, " \t\n\r");
			if ($tag)
			{
				$url = CHTTP::urlAddParams(
					$arParams["SEARCH_PAGE"],
					array(
						"tags" => $tag,
					),
					array(
						"encode" => true,
					)
				);
				$tags[] = '<a href="'.$url.'">'.$tag.'</a>';
			}
		}
		$arResult["FIELDS"]["TAGS"] = implode(", ", $tags);
	}
}


if (
        (isset($arResult['PROPERTIES']['DATE_START']) && !empty($arResult['PROPERTIES']['DATE_START']['VALUE'])) || (isset($arResult['PROPERTIES']['DATE_END']) && !empty($arResult['PROPERTIES']['DATE_END']['VALUE']))
)
{
    if (!empty($arResult['PROPERTIES']['DATE_START']['VALUE']) && !empty($arResult['PROPERTIES']['DATE_END']['VALUE']))
    {
        // set only date start and end
        $timeStart = new DateTime($arResult['PROPERTIES']['DATE_START']['VALUE']);
        $timeEnd = new DateTime($arResult['PROPERTIES']['DATE_END']['VALUE']);

        if ($timeStart->format("Y") == $timeEnd->format("Y") && $timeStart->format("m") == $timeEnd->format("m"))
        {
            $messageID = $timeStart->format("Y") == date("Y") ? 'CP_TPL_DATE_FORMAT_1' : 'CP_TPL_DATE_FORMAT_2';
        }
        elseif ($timeStart->format("Y") == $timeEnd->format("Y") && $timeStart->format("m") != $timeEnd->format("m"))
        {
            $messageID = 'CP_TPL_DATE_FORMAT_3';
        }
        else
        {
            $messageID = 'CP_TPL_DATE_FORMAT_4';
        }

        $arResult['DATE_TIME'] = GetMessage($messageID, array(
            '#DAY_START#' => $timeStart->format("d"),
            '#DAY_END#' => $timeEnd->format("d"),
            '#MONTH_START#' => FormatDate('F', $timeStart->getTimestamp()),
            '#MONTH_END#' => FormatDate('F', $timeEnd->getTimestamp()),
            '#YEAR_START#' => $timeStart->format('Y'),
            '#YEAR_END#' => $timeEnd->format('Y')
        ));
    }
    elseif (!empty($arResult['PROPERTIES']['DATE_START']['VALUE']) && empty($arResult['PROPERTIES']['DATE_END']['VALUE']))
    {
        // set only date start
        $timeStart = new DateTime($arResult['PROPERTIES']['DATE_START']['VALUE']);
        $messageID = $timeStart->format("Y") == date("Y") ? 'CP_TPL_DATE_FORMAT_5' : 'CP_TPL_DATE_FORMAT_6';

        $arResult['DATE_TIME'] = GetMessage($messageID, array(
            '#DAY#' => $timeStart->format("d"),
            '#MONTH#' => FormatDate('F', $timeStart->getTimestamp()),
            '#YEAR#' => $timeStart->format('Y'),
        ));
    }
    elseif (empty($arResult['PROPERTIES']['DATE_START']['VALUE']) && !empty($arResult['PROPERTIES']['DATE_END']['VALUE']))
    {
        // set only date end
        $timeEnd = new DateTime($arResult['PROPERTIES']['DATE_END']['VALUE']);
        $messageID = $timeEnd->format("Y") == date("Y") ? 'CP_TPL_DATE_FORMAT_7' : 'CP_TPL_DATE_FORMAT_8';

        $arResult['DATE_TIME'] = GetMessage($messageID, array(
            '#DAY#' => $timeEnd->format("d"),
            '#MONTH#' => FormatDate('F', $timeEnd->getTimestamp()),
            '#YEAR#' => $timeEnd->format('Y'),
        ));
    }
    
    if (!isset($arResult['PROPERTIES']['DATE_END']) ||
            (isset($arResult['PROPERTIES']['DATE_END']) && empty($arResult['PROPERTIES']['DATE_END']['VALUE'])) ||
            (isset($arResult['PROPERTIES']['DATE_END']) && strtotime($arResult['PROPERTIES']['DATE_END']['VALUE']) > time())
    )
    {
        $arResult['DATE_TIME_ACTIVE'] = "Y";
    }
}
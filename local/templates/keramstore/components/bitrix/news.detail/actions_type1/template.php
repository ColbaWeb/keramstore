<?
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout\Assets;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<div class="action-info" id="<?echo $this->GetEditAreaId($arResult['ID'])?>">
    <div class="row">
        <? if($arParams["DISPLAY_PICTURE"] != "N" && (is_array($arResult["DETAIL_PICTURE"]) || is_array($arResult["PREVIEW_PICTURE"])))
        {
            ?>
            <div class="col-md-4">
                <?
                if (is_array($arResult["DETAIL_PICTURE"]))
                {
                        echo Assets::showBackgroundImage(
                                $arResult["DETAIL_PICTURE"]["SRC"],
                                array(
                                    'attributes' => array(
                                        'class="img"',
                                    )
                                )
                        );
                }
                elseif (is_array($arResult["PREVIEW_PICTURE"]))
                {
                    echo Assets::showBackgroundImage(
                                $arResult["PREVIEW_PICTURE"]["SRC"],
                                array(
                                    'attributes' => array(
                                        'class="img"',
                                    )
                                )
                        );
                }
                ?>
            </div>
            <div class="col-md-8">
                <div class="text-block">
                    <div class="date">
                        <?
                        if (isset($arResult['DATE_TIME']) && !empty($arResult['DATE_TIME']))
                        {
                            if (isset($arResult['DATE_TIME_ACTIVE']))
                            {
                                ?><div class="label"><?=GetMessage('CP_TPL_ACTION_ACTIVE')?></div><?
                            }
                            else
                            {
                                ?><div class="label ended"><?=GetMessage('CP_TPL_ACTION_ENDED')?></div><?
                            }
                            ?>
                            
                            <div class="date-value font-body-small"><?=$arResult['DATE_TIME']?></div>
                            <?
                        }
                        ?>
                        
                    </div>
                    <div class="text font-p">
                        
                        <?
                        if($arResult["NAV_RESULT"])
                        {
                            if($arParams["DISPLAY_TOP_PAGER"])
                            {
                                ?><?=$arResult["NAV_STRING"]?><br><?
                            }
                            
                            echo $arResult["NAV_TEXT"];
                            
                            if($arParams["DISPLAY_BOTTOM_PAGER"])
                            {
                                ?><?=$arResult["NAV_STRING"]?><br><?
                            }
                        }
                        elseif ($arResult["DETAIL_TEXT"] <> '')
                        {
                            echo $arResult["DETAIL_TEXT"];
                        }
                        else
                        {
                            echo $arResult["PREVIEW_TEXT"];
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?
        }
        else
        {
            ?>
            <div class="col-12">
                <div class="text-block">
                    <div class="date">
                        <?
                        if (isset($arResult['DATE_TIME']) && !empty($arResult['DATE_TIME']))
                        {
                            if (isset($arResult['DATE_TIME_ACTIVE']))
                            {
                                ?><div class="label"><?=GetMessage('CP_TPL_ACTION_ACTIVE')?></div><?
                            }
                            else
                            {
                                ?><div class="label ended"><?=GetMessage('CP_TPL_ACTION_ENDED')?></div><?
                            }
                            ?>
                            
                            <div class="date-value font-body-small"><?=$arResult['DATE_TIME']?></div>
                            <?
                        }
                        ?>
                    </div>
                    <div class="text font-p">
                        <?
                        if($arResult["NAV_RESULT"])
                        {
                            if($arParams["DISPLAY_TOP_PAGER"])
                            {
                                ?><?=$arResult["NAV_STRING"]?><br><?
                            }
                            
                            echo $arResult["NAV_TEXT"];
                            
                            if($arParams["DISPLAY_BOTTOM_PAGER"])
                            {
                                ?><?=$arResult["NAV_STRING"]?><br><?
                            }
                        }
                        elseif ($arResult["DETAIL_TEXT"] <> '')
                        {
                            echo $arResult["DETAIL_TEXT"];
                        }
                        else
                        {
                            echo $arResult["PREVIEW_TEXT"];
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?
        }
        ?>
    </div>
</div>

<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Localization\Loc,
    \Nextype\Alpha\Layout;

$tagUrl = ($arParams['USE_SUBSECTIONS_TAGS_FILTER'] == 'Y') ? $arResult['SECTION_URL'] : $arResult['LIST_PAGE_URL'];
$tagUrl .= (strstr($tagUrl, '?') !== false) ? '&' : '?';
$bUseSender = ($arParams['SHOW_SUBSCRIBE_BLOCK'] == 'Y' && \Bitrix\Main\Loader::includeModule('sender'));
?>

<div class="blog-list inner inner-type-2">
        <div class="row">
            <div class="col">

                    <div class="content-head">
                        <?if (!empty($arResult['DETAIL_PICTURE']['SRC']))
                            echo \Nextype\Alpha\Layout\Assets::showBackgroundImage(
                                  $arResult["DETAIL_PICTURE"]["SRC"],
                                  array('attributes' => array('class="background"'))
                            );?>
                        <div class="wrap">
                            <div class="article-info">
                                <?if (!empty($arResult['PROPERTIES']['TAGS']['VALUE'][0])):?>
                                <div class="blog-tags">
                                    <?foreach ($arResult['PROPERTIES']['TAGS']['VALUE'] as $key => $tagValue):?>
                                        <div class="item">
                                            <a href="<?=$tagUrl . 'tag=' . $arResult['PROPERTIES']['TAGS']['VALUE_XML_ID'][$key]?>" 
                                               class="tag font-button-large">
                                                <?=$tagValue?>
                                            </a>
                                        </div>
                                    <?endforeach?>
                                </div>
                                <?endif?>
                                
                                <? if (!empty($arResult['DISPLAY_ACTIVE_FROM'])): ?>
                                <div class="date"><i class="icon icon-calendar-white"></i><?=$arResult['DISPLAY_ACTIVE_FROM']?></div>
                                <? endif; ?>

                            </div>
                            <div class="title font-h1 bold"><?=$arResult['NAME']?></div>
                        </div>
                    </div>

                <div class="content-wrapper">

                    <?
                    if ($arParams['USE_SHARE'] == 'Y')
                    {
                        Layout\Partial::getInstance()->render('share_button.php', array (), false, false);
                    }
                    ?>

                    <div class="content">

                        <div class="article-body">
                            
                            <?=$arResult["DETAIL_TEXT"]?>
                        </div>
                        <!-- <div class="inner-text"> 

                        </div> -->

                        <?if ($arParams['DETAIL_SHOW_TAGS_BOTTOM'] == 'Y'):?>
                            <div class="article-info">
                                <?if (!empty($arResult['PROPERTIES']['TAGS']['VALUE'][0])):?>
                                <div class="blog-tags">
                                    <?foreach ($arResult['PROPERTIES']['TAGS']['VALUE'] as $key => $tagValue):?>
                                        <div class="item">
                                            <a href="<?=$tagUrl . 'tag=' . $arResult['PROPERTIES']['TAGS']['VALUE_XML_ID'][$key]?>" 
                                               class="tag font-button-large">
                                                <?=$tagValue?>
                                            </a>
                                        </div>
                                    <?endforeach?>
                                </div>
                                <?endif?>
                                <div class="date">
                                    <i class="icon icon-calendar"></i>
                                    <?=$arResult['DISPLAY_ACTIVE_FROM']?> 
                                </div>

                            </div>
                        <?endif?>
                    </div>

                </div>
            </div>
            <div class="col-auto sidebar-left">
                <?if (!empty($arResult['ALL_SECTIONS'])):?>
                    <div class="blog-sidebar-menu font-body-2">
                        <div class="title bold"><?=Loc::getMessage('TPL_SIDEBAR_SECTIONS_TITLE')?></div>
                        <div class="list">
                            <ul class="reset-ul-list">
                                <?foreach ($arResult['ALL_SECTIONS'] as $arSection):
                                    if ($arSection['ELEMENT_CNT'] == '0')
                                        continue; ?>
                                    <li>
                                        <a class="item" href="<?=$arSection['SECTION_PAGE_URL']?>">
                                            <span class="category-name"><?=$arSection['NAME']?></span>
                                            <span class="count"><?=$arSection['ELEMENT_CNT']?></span>
                                        </a>
                                    </li>
                                <?endforeach?>
                            </ul>
                        </div>
                    </div>
                <?endif?>

                <?if (!empty($arResult['ALL_TAGS'])):?>
                    <div class="blog-sidebar-menu font-body-2">
                        <div class="title bold"><?=Loc::getMessage('TPL_SIDEBAR_TAGS_TEXT')?></div>
                        <div class="blog-tags">
                            <?foreach ($arResult['ALL_TAGS'] as $xmlId => $arTag):?>
                                <div class="item">
                                    <a href="<?=$tagUrl . 'tag=' . $xmlId?>" class="tag font-button-large">
                                        <?=$arTag['DISPLAY_VALUE']?>
                                    </a>
                                </div>
                            <?endforeach?>
                        </div>  
                    </div>
                <?endif?>

                <?if ($bUseSender):?>
                    <? $uniqId = 'blog_' . $arResult['ID'] . randString(5);?>
                    <div class="subscribe">
                        <div class="subscribe-image" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/icons/icon-mailbox.svg');"></div>
                        <div class="title font-h5 bold"><?=Loc::getMessage('T_SENDER_SUBSCRIBE_HEADER')?></div>
                        <div class="text font-body"><?=Loc::getMessage('T_SENDER_SUBSCRIBE_TEXT')?></div>
                        <a id="senderbtn_<?=$uniqId?>" href="#modal_block_<?=$uniqId?>" class="btn btn-primary btn-medium" data-toggle="modal">
                            <?=Loc::getMessage('T_SENDER_SUBSCRIBE_BTN_TEXT')?>
                        </a>
                    </div>
                <?endif?>
            </div>
        </div>
</div>


<?if ($bUseSender):?>
    <div class="modal fade" id="modal_block_<?=$uniqId?>" tabindex="-1" aria-labelledby="modal_block_<?=$uniqId?>" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon icon-close-big"></i>
                </a>
                <div class="scrollbar-inner">
                    <?$APPLICATION->IncludeComponent(
	"nextype:alpha.sender.subscribe", 
	"popup", 
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CONFIRMATION" => "Y",
		"HIDE_MAILINGS" => "N",
		"SET_TITLE" => "N",
		"SHOW_HIDDEN" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "N",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_PERSONALIZATION" => "Y",
		"COMPONENT_TEMPLATE" => "popup"
	),
	false
);?>
                </div>
            </div>
        </div>
    </div>
<?endif?>
<script>
    $(document).ready(function () {

        $(window).scroll(function () {
            let header = $('.header-fixed');
            let offset = header.height();
            $('.subscribe').css('top', offset);
        });
    });
</script>
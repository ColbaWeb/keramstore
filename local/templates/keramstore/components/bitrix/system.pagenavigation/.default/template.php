<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);

/*if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}*/

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

?>

<div class="pagination-nav">
    <nav>
        <div class="pagination font-bold-p reset-ul-list">
            <? if ($arResult["NavPageNomer"] > 1): ?>
            <div class="page-item">
                <a class="page-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" aria-label="Previous">
                    <i class="icon icon-arrow-left"></i>
                </a>
            </div>
            <? endif; ?>
            
            <ul class="page-items reset-ul-list">
                
                <? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>
                    <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                        <li class="page-item active">
                            <a href="javascript:void(0)" class="page-link"><?= $arResult["nStartPage"] ?></a>
                        </li>
                    <? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
                        <li class="page-item">
                            <a class="page-link" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
                        </li>
                    <? else: ?>
                        <li class="page-item">
                            <a class="page-link" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
                        </li>
                    <? endif ?>
                    <? $arResult["nStartPage"] ++ ?>
                
                <? endwhile; ?>
            </ul>
            
            <? if ($arResult["NavPageNomer"] < $arResult['NavPageCount']): ?>
            <div class="page-item ">
                <a class="page-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" aria-label="Next">
                    <i class="icon icon-arrow-right"></i>
                </a>
            </div>
            <? endif; ?>
        </div>
    </nav>
</div>
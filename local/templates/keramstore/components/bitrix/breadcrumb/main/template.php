<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;



//delayed function must return a string
if(empty($arResult)) {
	//if (Layout\Base::getInstance()->isHomepage()){
			$strReturn = '<div class="container ">';
	//$strReturn .= '<ol itemscope itemtype="https://schema.org/BreadcrumbList">'
	//. '<li itemprop="itemListElement" itemscope'
	//. 'itemtype="https://schema.org/ListItem">'
        				$strReturn .= '<a href="'.SITE_DIR.'" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'
							. '<span itemscope="" itemtype="http://schema.org/Thing" itemid="'.SITE_DIR.'" itemprop="item">'.GetMessage('HOMEPAGE_LINK').'</span>'
        				. '<meta itemprop="position" content="1" />'
						. '<meta itemprop="name" content="'.GetMessage('HOMEPAGE_LINK').'">'
      				. '</a>'
    			. '</ol>';
	/*$strReturn .= '<a href="'.SITE_DIR.'" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'
						. GetMessage('HOMEPAGE_LINK')
						. '<span itemscope="" itemtype="http://schema.org/Thing" itemid="'.SITE_DIR.'" itemprop="item"></span>'
						. '<meta itemprop="position" content="1">'
						. '<meta itemprop="name" content="'.GetMessage('HOMEPAGE_LINK').'">'
. '</a>';*/
			$strReturn .= '</div>';
			return $strReturn;
		//} else {
		//return "";
		//}

} else {

$strReturn = '<div class="container">';

$itemSize = count($arResult);

if ($itemSize > 0)
{
    $strReturn .= '<a href="'.SITE_DIR.'" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'
            . GetMessage('HOMEPAGE_LINK')
            . '<span itemscope="" itemtype="http://schema.org/Thing" itemid="'.SITE_DIR.'" itemprop="item"></span>'
            . '<meta itemprop="position" content="1">'
            . '<meta itemprop="name" content="'.GetMessage('HOMEPAGE_LINK').'">'
            . '</a>';
    
    $strReturn .= '<span class="icon icon-arrow-light-right"></span>';
}
 

for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
            $strReturn .= ''
                    . '<a href="'.$arResult[$index]["LINK"].'" class="item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'
                    . $title
                    . '<span itemscope="" itemtype="http://schema.org/Thing" itemid="'.$arResult[$index]["LINK"].'" itemprop="item"></span>'
                    . '<meta itemprop="position" content="'.($index + 2).'">'
                    . '<meta itemprop="name" content="'.$title.'">'
                    . '</a>';
                
	}
	else
	{
            $strReturn .= '<div class="breadcrumbs-elem">'
                    .'<span class="item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">'
                    . '<span class="icon icon-arrow-light-right"></span>'
                    . $title
                    . '<meta itemprop="position" content="'.($index + 2).'">'
                    . '<meta itemprop="name" content="'.$title.'">'
                    . '</span>'
                    . '</div>';
	}
        
        if ($index < $itemSize - 1)
        {
            $strReturn .= '<span class="icon icon-arrow-light-right"></span>';
        }
}

$strReturn .= '</div>';

return $strReturn;
}

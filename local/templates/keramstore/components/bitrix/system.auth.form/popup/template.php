<?
use Bitrix\Main\Config\Option;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$formId = "bx_system_auth_form" . $this->randString();

$bLoginError = false;
if ($arResult['SHOW_ERRORS'] == 'Y' && isset($arResult['ERROR_MESSAGE']['ERROR_TYPE']) && $arResult['ERROR_MESSAGE']['ERROR_TYPE'] == "LOGIN")
    $bLoginError = true;

$bUserPhoneAuth = Option::get("main", "new_user_phone_auth", "N") == "Y";
$bUserPhoneRequired = Option::get("main", "new_user_phone_required", "N") == "Y";
$bUserEmailAuth = Option::get("main", "new_user_email_auth", "N") == "Y";
$bUserEmailRequired = Option::get("main", "new_user_email_required", "N") == "Y";

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

?>

    <form id="<?=$formId?>" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=SITE_DIR?>auth/?login=yes">
        
        <? if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
            $APPLICATION->RestartBuffer();
        ?>

        <?
        if ($USER->IsAuthorized())
        {
            ?>
            <div class="alert alert-success"><?=GetMessage('SUCCESS_AUTH_NOTE')?></div>
            <script>
                window.location.href = "<?=$arParams['REDIRECT_URL']?>";
            </script>
            <?
        }
        else
        {
            if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'] && !$bLoginError)
            {
                ?>
                <div class="alert alert-warning"><?=$arResult['ERROR_MESSAGE']?></div>
                <?
            }
            ?>

            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
            <input type="hidden" name="USER_REMEMBER" value="Y" />
            <input type="hidden" name="Login" value="Y" />

            <? if ($bUserPhoneAuth && $bUserPhoneRequired): ?>
            <div class="custom-input-wrap">
                <div class="custom-input<?=$bLoginError ? ' error' : ''?>">
                    <input type="text" value="<?=$request->get('USER_LOGIN')?>" data-field-type="phone" required="required" name="USER_LOGIN" maxlength="50">
                    <label><?=GetMessage("AUTH_PHONE_NUMBER")?></label>
                </div>
                <? if ($bLoginError): ?>
                <div class="error-message"><?=GetMessage('USER_WITH_PHONE_NUMBER_NOT_FOUND')?></div>
                <? endif; ?>
            </div>
            <? elseif ($bUserEmailAuth && $bUserEmailRequired): ?>
            <div class="custom-input-wrap">
                <div class="custom-input<?=$bLoginError ? ' error' : ''?>">
                    <input type="text" value="<?=$request->get('USER_LOGIN')?>" required="required" name="USER_LOGIN" maxlength="50">
                    <label><?=GetMessage("AUTH_EMAIL")?></label>
                </div>
                <? if ($bLoginError): ?>
                <div class="error-message"><?=GetMessage('USER_WITH_EMAIL_NOT_FOUND')?></div>
                <? endif; ?>
            </div>
            <? else: ?>
            <div class="custom-input-wrap">
                <div class="custom-input<?=$bLoginError ? ' error' : ''?>">
                    <input type="text" required="required" value="<?=$arResult['USER_LOGIN']?>" name="USER_LOGIN" maxlength="50">
                    <label><?=GetMessage("AUTH_LOGIN")?></label>
                </div>
                <? if ($bLoginError): ?>
                <div class="error-message"><?= strip_tags($arResult['ERROR_MESSAGE']['MESSAGE'])?></div>
                <? endif; ?>

            </div>
            <? endif; ?>


            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="password" required="required" name="USER_PASSWORD" maxlength="255" autocomplete="off">
                    <label><?=GetMessage("AUTH_PASSWORD")?></label>
                </div>
            </div>


            <?if ($arResult["CAPTCHA_CODE"]):?>
            <div class="custom-input-wrap captcha">
                <div class="custom-input">
                    <input name="captcha_word" maxlength="50" value="" autocomplete="off">
                    <label><?=GetMessage("AUTH_CAPTCHA_PROMT")?></label>
                </div>

                <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
            </div>
            <?endif?>

            <button class="btn btn-primary btn-large"><?=GetMessage('AUTH_LOGIN_BUTTON')?></button>
            <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="link font-large-button"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>

            <? if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
                die;
            ?>

        </form>

        <?
        if($arResult["AUTH_SERVICES"])
        {
        ?>
        <div class="social-networks">
            <div class="title font-body-2">
                <?=GetMessage("socserv_as_user_form")?>
            </div>
            <div class="list">
                <?
                $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "main", 
                        array(
                                "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                                "SUFFIX"=>"form",
                        ), 
                        $component, 
                        array("HIDE_ICONS"=>"Y")
                );
                ?>

            </div>
        </div>

        <?
        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
                array(
                        "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                        "AUTH_URL"=>$arResult["AUTH_URL"],
                        "POST"=>$arResult["POST"],
                        "POPUP"=>"Y",
                        "SUFFIX"=>"form",
                ), 
                $component, 
                array("HIDE_ICONS"=>"Y")
        );
        
        }
}

$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'bitrix.system.auth.form');
?>
<script>
        BX.ready(function(){

            new window.Alpha.SignForm({
                siteId: '<?=SITE_ID?>',
                formId: '<?=CUtil::JSEscape($formId)?>',
                ajaxPath: '<?=$templateFolder?>/ajax.php?login=yes',
                templateName: '<?=CUtil::JSEscape($templateName)?>',
                signedParamsString: '<?= CUtil::JSEscape($signedParams) ?>'
            });

        });
    </script>

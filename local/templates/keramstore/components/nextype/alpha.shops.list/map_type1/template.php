<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;
use Nextype\Alpha\Options;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

if (!Loader::includeModule('nextype.alpha'))
    return;

$this->setFrameMode(true);

$templateData = array (
    
);

if ($arParams['MAP_PROVIDER'] && $arParams['MAP_PROVIDER'] == "YANDEX")
{
    include( __DIR__ . "/yandex.php" );
}

?>

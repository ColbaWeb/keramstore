<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;
use Nextype\Alpha\Options;

$templateData['YANDEX_MAP_ID'] = "bx_shops_map" . $this->randString();
$templateData['ITEMS'] = $arResult['ITEMS'];
?>

<div class="wrapper-section">
    <div class="map-section">
        <div class="container">
            <div id="<?=$templateData['YANDEX_MAP_ID']?>" class="map"></div>
        </div>
    </div>
</div>
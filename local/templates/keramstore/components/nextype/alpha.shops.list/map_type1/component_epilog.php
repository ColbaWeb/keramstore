<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;

/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */

global $APPLICATION;

if ($arParams['MAP_PROVIDER'] && $arParams['MAP_PROVIDER'] == "YANDEX")
{
    Layout\Assets::getInstance(SITE_ID)->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU");
    
    if (!empty($templateData['YANDEX_MAP_ID']))
    {
        $arJsData = array (
            0 => array (
                'items' => array ()
            )
        );
        
        if (!empty($templateData['ITEMS']))
        {
            foreach ($templateData['ITEMS'] as $item)
            {
                if (isset($item['PROPERTIES']['COORDS']['VALUE']) && strpos($item['PROPERTIES']['COORDS']['VALUE'], ",") !== false)
                {
                    $arCoords = explode(",", $item['PROPERTIES']['COORDS']['VALUE']);
                    if (is_array($arCoords) && count($arCoords) == 2)
                    {
                        $data = array (
                            'center' => array (
                                floatval(trim($arCoords[0])),
                                floatval(trim($arCoords[1])),
                            ),
                            'phones' => [],
                            'type' => !empty($item['PROPERTIES']['TYPE']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['TYPE']['VALUE']) : '',
                            'address' => !empty($item['PROPERTIES']['ADDRESS']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['ADDRESS']['VALUE']) : '',
                            'email' => !empty($item['PROPERTIES']['EMAIL']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['EMAIL']['VALUE']) : '',
                            'worktime' => !empty($item['PROPERTIES']['WORKTIME']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['WORKTIME']['VALUE']) : '',
                        );
                                                
                        if (!empty($item['PROPERTIES']['PHONES']['VALUE']))
                        {
                            
                            if ($item['PROPERTIES']['PHONES']['USER_TYPE'] == "AlphaPhonesList" && is_array($item['PROPERTIES']['PHONES']['VALUE']))
                            {          
                                
                                foreach ($item['PROPERTIES']['PHONES']['VALUE'] as $value)
                                {
                                    $value = htmlspecialchars_decode($value);
                                    
                                    if (CheckSerializedData($value) && ($value = unserialize($value)) && is_array($value))
                                    {
                                        $data['phones'][] = htmlspecialchars($value['phone']);
                                    }
                                }
                            }
                            elseif ($item['PROPERTIES']['PHONES']['USER_TYPE'] != "AlphaPhonesList" && is_array($item['PROPERTIES']['PHONES']['VALUE']))
                            {
                                foreach ($item['PROPERTIES']['ADDRESS']['VALUE'] as $value)
                                {
                                    if (!empty($value))
                                    {
                                        $data['phones'][] = htmlspecialchars($value);
                                    }
                                }
                            }
                            else
                            {
                                $data['phones'][] = htmlspecialchars($value);
                            }
                            
                        }
                        
                        $arJsData[0]['items'][] = $data;
                    }
                }
                
            }
        }
        
        ?>
        <script>
            BX.ready(function(){

                var groups = <?=CUtil::PhpToJSObject($arJsData)?>,
                    myMap;
                        
                var interval = setInterval(function () {
            
                    if (!!window.ymaps)
                    {
                        ymaps.ready(['Panel']).then(function() {

                            myMap = new ymaps.Map('<?=$templateData['YANDEX_MAP_ID']?>', {
                                    center: [55.159897, 61.402554],
                                    zoom: 14,
                                    controls: []
                                }, {
                                    searchControlProvider: 'yandex#search'
                                });

                            panel = new ymaps.Panel();
                                myMap.controls.add(panel, {
                                float: 'left'
                            });

                            var collection = new ymaps.GeoObjectCollection(null, {
                                hasBalloon: false,
                                iconColor: '#3b5998'
                            });

                            for (var i = 0, l = groups.length; i < l; i++) {
                                var group = groups[i];
                                for (var j = 0, m = group.items.length; j < m; j++) {
                                    addCollection(group.items[j], collection);
                                }
                            }

                            function addCollection(item, collection) {
                                
                                var placemark = new ymaps.Placemark([
                                    parseFloat(item.center[0]),
                                    parseFloat(item.center[1])
                                ], { 
                                        address: item.address,
                                        type: item.type,
                                        phones: item.phones,
                                        email: item.email,
                                        worktime: item.worktime,
                                        center: item.center
                                    },
                                    {
                                    iconLayout: 'default#image',
                                    iconImageHref: '<?=SITE_TEMPLATE_PATH?>/images/pin.svg',
                                    iconImageSize: [58, 58],
                                    iconImageOffset: [-29, -58]
                                    }
                                );

                                collection.add(placemark);
                            }

                            myMap.geoObjects.add(collection);

                            collection.events.add('click', function (e) {
                                var target = e.get('target');
                                panel.setContent(target.properties.get(target));
                                myMap.panTo(target.geometry.getCoordinates(), {useMapMargin: true});
                            });

                            myMap.setBounds(myMap.geoObjects.getBounds());

                            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
                                myMap.behaviors.disable('drag');
                            }

                        });

                        ymaps.modules.define('Panel', [
                            'util.augment',
                            'collection.Item'
                        ], function (provide, augment, item) {
                            var Panel = function (options) {
                                Panel.superclass.constructor.call(this, options);
                            };

                            augment(Panel, item, {
                                onAddToMap: function (map) {
                                    Panel.superclass.onAddToMap.call(this, map);
                                    this.getParent().getChildElement(this).then(this._onGetChildElement, this);
                                },

                                onRemoveFromMap: function (oldMap) {
                                    if (this._$control) {
                                        this._$control.remove();
                                    }
                                    Panel.superclass.onRemoveFromMap.call(this, oldMap);
                                },

                                _onGetChildElement: function (parentDomContainer) {
                                    var html = `<div class="customControl">
                                                    <div class="content">
                                                        <div class="title font-title" data-entity="ymap-ballon-type"></div>
                                                        <div class="location font-body-2 item" data-entity="ymap-ballon-address"></div>
                                                        <div data-entity="ymap-ballon-phones"></div>
                                                        <a href="#" data-entity="ymap-ballon-email" class="email font-body-small item"></a>
                                                        <div data-entity="ymap-ballon-worktime" class="time font-body-small item"></div>
                                                        <button data-entity="ymap-ballon-button" class="btn btn-link"><?=GetMessage('CT_SHOPS_LIST_BUTTON_SHOW_ON_MAP')?><i class="icon icon-arrow-right-text-button-white"></i></button>
                                                    </div>
                                                </div>`;
                                    this._$control = $(html).appendTo(parentDomContainer);
                                    this._$type = $('[data-entity="ymap-ballon-type"]');
                                    this._$address = $('[data-entity="ymap-ballon-address"]');
                                    this._$phones = $('[data-entity="ymap-ballon-phones"]');
                                    this._$email = $('[data-entity="ymap-ballon-email"]');
                                    this._$worktime = $('[data-entity="ymap-ballon-worktime"]');
                                    this._$button = $('[data-entity="ymap-ballon-button"]');

                                    $('[data-entity="ymap-ballon-button"]').on('click', this._onMove);
                                },
                                _onMove: function () {
                                    let centerPlacemark = $(this).attr('data-coords').split(', ').map(Number);
                                    myMap.panTo(centerPlacemark, {
                                        delay: 1500
                                    }).then(function() {
                                        myMap.setZoom(16, {duration: 500});
                                    });
                                },
                                setContent: function (item) {
                                    
                                    this._$control.css('display', 'flex');
                                    
                                    if (item.type != "")
                                        this._$type.html(item.type).removeClass('d-none');
                                    else
                                        this._$type.addClass('d-none');
                                    
                                    if (item.address != "")
                                        this._$address.html(item.address).removeClass('d-none');
                                    else
                                        this._$address.addClass('d-none');
                                    
                                    if (item.email != "")
                                    {
                                        this._$email.attr('href', 'mailto:' + item.email);
                                        this._$email.html(item.email).removeClass('d-none');
                                    }
                                    else
                                        this._$email.addClass('d-none');
                                    
                                    if (item.worktime != "")
                                        this._$worktime.html(item.worktime).removeClass('d-none');
                                    else
                                        this._$worktime.addClass('d-none');
                                    
                                    this._$phones.html('');
                                    if (!!item.phones && typeof(item.phones) == 'object' && item.phones.length > 0)
                                    {
                                        for (var i=0; i<item.phones.length; i++)
                                        {
                                            var link = $('<a href="tel:'+item.phones[i]+'" class="phone font-body-small item">'+item.phones[i]+'</a>');
                                            this._$phones.append(link);
                                        }
                                    }
                                    
                                    this._$button.attr('data-coords', item.center.join(', '));

                                }
                            });

                            provide(Panel);
                        });

                        clearTimeout(interval);
                    }
            
                }, 1000);
            });

        </script>
        <?
    }
}
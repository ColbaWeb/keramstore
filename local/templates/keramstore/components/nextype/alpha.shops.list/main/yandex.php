<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;
use Nextype\Alpha\Options;

$templateData['YANDEX_MAP_ID'] = "bx_shops_map" . $this->randString();
$templateData['ITEMS'] = $arResult['ITEMS'];
?>



<div class="map-section shops-list-page">
    <div class="pickup-points">
        <div class="map-wrap">
            <div class="pickup-points-list ">
                <?
                if (!empty($arResult['LOCATIONS']) && count($arResult['LOCATIONS']) > 1)
                {
                    ?>
                    <div class="city-select">
                        <div class="accordion-head filter-block inner-block" data-role="horizontal-filter-block">
                            <a href="javascript:void(0);" class="accordeon-title filter-link" style="">
                                <span class="text font-large-button"><?=!empty($arResult['LOCATION_CODE']) && isset($arResult['LOCATIONS'][$arResult['LOCATION_CODE']]) ? $arResult['LOCATIONS'][$arResult['LOCATION_CODE']]['NAME_RU'] : Loc::getMessage('SELECT_OPTION_ALL')?></span>
                                <i class="icon icon-arrow-light-down"></i>
                            </a>
                            <div class="accordion-body sub inner-body">
                                <div class="scrollbar-inner">
                                    <?
                                    foreach ($arResult['LOCATIONS'] as $arLocation)
                                    {
                                        ?><a class="item" href="<?=$arLocation['SECTION_PAGE_URL']?>"><?=$arLocation['NAME_RU']?></a><?
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tabs-icons-block-wrap">
                            <div class="tabs-icons-block">
                                <a href="javascript:void(0);" class="list-btn tab-btn active"><i class="icon icon-list-table"></i></a>
                                <a href="javascript:void(0);" class="location-btn tab-btn"><i class="icon icon-bx-location"></i></a>
                            </div>
                        </div>
                    </div>  
                    <?
                }
                ?>
                <div class="list opened">
                    <div class="scrollbar-inner">
                        <?
                        foreach ($arResult['ITEMS'] as $key => $arItem)
                        {
                            ?>
                            <a href="javascript:void(0);" class="item" data-placemark-id="<?=$arItem['ID']?>">
                                <?
                                if (isset($arItem['PROPERTIES']['SUBWAY']['VALUE']) && !empty($arItem['PROPERTIES']['SUBWAY']['VALUE']))
                                {
                                    ?><div class="subway font-body-small"><?=$arItem['PROPERTIES']['SUBWAY']['VALUE']?></div><?
                                }

                                if (isset($arItem['PROPERTIES']['ADDRESS']['VALUE']) && !empty($arItem['PROPERTIES']['ADDRESS']['VALUE']))
                                {
                                    ?><div class="adress font-title bold">
                                    <?=$arItem['NAME']?><br>
                                    <?=(!isset($arResult['LOCATION_CODE']) || empty($arResult['LOCATION_CODE'])) && isset($arResult['LOCATIONS'][$arItem['PROPERTIES']['LOCATION']['VALUE']]['NAME_RU']) ? $arResult['LOCATIONS'][$arItem['PROPERTIES']['LOCATION']['VALUE']]['NAME_RU'] . ", " : "" ?>
                                    <?=$arItem['PROPERTIES']['ADDRESS']['VALUE']?>
                                    </div><?
                                }

                                if (isset($arItem['PROPERTIES']['WORKTIME']['VALUE']) && !empty($arItem['PROPERTIES']['WORKTIME']['VALUE']))
                                {
                                    ?><div class="time font-body-2"><?=$arItem['PROPERTIES']['WORKTIME']['VALUE']?></div><?
                                }
                                ?>

                            </a>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div id="<?=$templateData['YANDEX_MAP_ID']?>" class="map"></div>
        </div>
    </div>
</div>

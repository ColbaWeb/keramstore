<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;

/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */

global $APPLICATION;

if ($arParams['MAP_PROVIDER'] && $arParams['MAP_PROVIDER'] == "YANDEX")
{
    Layout\Assets::getInstance(SITE_ID)->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU");
    
    if (!empty($templateData['YANDEX_MAP_ID']))
    {
        $arJsData = array (
            0 => array (
                'items' => array ()
            )
        );
                
        if (!empty($templateData['ITEMS']))
        {
            foreach ($templateData['ITEMS'] as $item)
            {
                if (isset($item['PROPERTIES']['COORDS']['VALUE']) && strpos($item['PROPERTIES']['COORDS']['VALUE'], ",") !== false)
                {
                    $arCoords = explode(",", $item['PROPERTIES']['COORDS']['VALUE']);
                    if (is_array($arCoords) && count($arCoords) == 2)
                    {
                        $data = array (
                            'id' => $item['ID'],
                            'center' => array (
                                floatval(trim($arCoords[0])),
                                floatval(trim($arCoords[1])),
                            ),
                            'subway' => !empty($item['PROPERTIES']['SUBWAY']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['SUBWAY']['VALUE']) : '',
                            'phones' => [],
                            'name' => !empty($item['NAME']) ? htmlspecialchars ($item['NAME']) : '',
                            'includedServices' => !empty($item['PROPERTIES']['INCLUDED_SERVICES']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['INCLUDED_SERVICES']['VALUE']) : '',
                            'excludedServices' => !empty($item['PROPERTIES']['EXCLUDED_SERVICES']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['EXCLUDED_SERVICES']['VALUE']) : '',
                            'address' => !empty($item['PROPERTIES']['ADDRESS']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['ADDRESS']['VALUE']) : '',
                            'email' => !empty($item['PROPERTIES']['EMAIL']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['EMAIL']['VALUE']) : '',
                            'worktime' => !empty($item['PROPERTIES']['WORKTIME']['VALUE']) ? htmlspecialchars ($item['PROPERTIES']['WORKTIME']['VALUE']) : '',
                            'description' => !empty($item['PREVIEW_TEXT']) ? trim(htmlspecialchars ($item['PREVIEW_TEXT'])) : '',
                        );
                                                
                        if (!empty($item['PROPERTIES']['PHONES']['VALUE']))
                        {
                            if ($item['PROPERTIES']['PHONES']['USER_TYPE'] == "AlphaPhonesList" && is_array($item['PROPERTIES']['PHONES']['VALUE']))
                            {          
                                
                                foreach ($item['PROPERTIES']['PHONES']['VALUE'] as $value)
                                {
                                    $value = htmlspecialchars_decode($value);
                                    
                                    if (CheckSerializedData($value) && ($value = unserialize($value)) && is_array($value))
                                    {
                                        $data['phones'][] = array(
                                        	'phone' => htmlspecialchars($value['phone']),
                                        	'additional' => htmlspecialchars($value['additional'])
                                        );
                                    }
                                }
                            }
                            elseif ($item['PROPERTIES']['PHONES']['USER_TYPE'] != "AlphaPhonesList" && is_array($item['PROPERTIES']['PHONES']['VALUE']))
                            {
                                foreach ($item['PROPERTIES']['ADDRESS']['VALUE'] as $value)
                                {
                                    if (!empty($value))
                                    {
                                        $data['phones'][] = array(
                                        	'phone' => htmlspecialchars($value)
                                        );
                                    }
                                }
                            }
                            else
                            {
                                $data['phones'][] = htmlspecialchars($value);
                            }
                            
                        }
                        
                        $arJsData[0]['items'][] = $data;
                    }
                }
                
            }
        }
        
        ?>
        <script>
            BX.ready(function(){
                
                var groups = <?=CUtil::PhpToJSObject($arJsData)?>,
                    myMap,
                    myCluster,
                    myPlacemarks = {};
            
                $('.pickup-points-list [data-placemark-id]').on('click', function(){
                    var placemarkId = parseInt($(this).data('placemark-id'));
                    
                    if (!!myPlacemarks[placemarkId]) {
                        myMap.setCenter(myPlacemarks[placemarkId].geometry.getCoordinates());
                        myMap.setZoom(12, {
                            checkZoomRange: true,
                            duration: 500
                        }).then(function () {
                            var objectState = myCluster.getObjectState(myPlacemarks[placemarkId]);
                            if (objectState.isClustered)
                            {
                                objectState.cluster.state.set('activeObject', myPlacemarks[placemarkId]);
                                myCluster.balloon.open(objectState.cluster.id);
                            }
                            else
                            {
                                myPlacemarks[placemarkId].balloon.open();
                            }
                        });
                    }
                });
                
                $('.tabs-icons-block .tab-btn').on('click', function(){
                    $(this).addClass('active').siblings().removeClass('active');
                    if ($(this).hasClass('list-btn')){
                        $('.pickup-points-list .list').addClass('opened');
                        $('.map').removeClass('opened');
                    }
                    else {
                        $('.pickup-points-list .list').removeClass('opened');
                        $('.map').addClass('opened');
                    }
                });

                $('.pickup-popup .btn-link').on('click', function(){
                    $('.pickup-popup.custom-popup').removeClass('visible');
                    $('.pickup-points-list .list').removeClass('opened');
                    $('.map').addClass('opened');
                    $('.tabs-icons-block .location-btn').addClass('active');
                    $('.tabs-icons-block .list-btn').removeClass('active');
                });
                        
                var interval = setInterval(function () {
            
                    if (!!window.ymaps)
                    {
                        ymaps.ready(function () {
                            myMap = new ymaps.Map('<?=$templateData['YANDEX_MAP_ID']?>', {
                                        center: [55.159897, 61.402554],
                                        zoom: 7,
                                        controls: []
                                    }, {
                                        searchControlProvider: 'yandex#search'
                            });

                            myCluster = new ymaps.Clusterer({
                                clusterHideIconOnBalloonOpen: false,
                                geoObjectHideIconOnBalloonOpen: false,
                            });

                            var MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
                                    '<div class="custom-popup pickup-popup">' +
                                        '<div class="arrow"></div>' +
                                        '<div class="content">' +
                                            '<a href="javascript:void(0)" class="close"><i class="icon icon-close-big"></i></a>' +
                                            '$[[options.contentLayout]]' +
                                        '</div>' +
                                    '</div>'
                                    , {
                                    
                                    build: function () {
                                        this.constructor.superclass.build.call(this);

                                        this._$element = $('.pickup-popup', this.getParentElement());

                                        this.applyElementOffset();

                                        this._$element.find('.close')
                                            .on('click', $.proxy(this.onCloseClick, this));
                                    },

                                    clear: function () {
                                        this._$element.find('.close')
                                            .off('click');

                                        this.constructor.superclass.clear.call(this);
                                    },

                                    onSublayoutSizeChange: function () {
                                        MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

                                        if(!this._isElement(this._$element)) {
                                            return;
                                        }

                                        this.applyElementOffset();

                                        this.events.fire('shapechange');
                                    },

                                    applyElementOffset: function () {
                                        this._$element.css({
                                            left: -(this._$element[0].offsetWidth / 2),
                                            top: -(this._$element[0].offsetHeight)
                                        });
                                    },

                                    onCloseClick: function (e) {
                                        e.preventDefault();

                                        this.events.fire('userclose');
                                    },

                                    getShape: function () {
                                        if(!this._isElement(this._$element)) {
                                            return MyBalloonLayout.superclass.getShape.call(this);
                                        }

                                        var position = this._$element.position();

                                        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                                            [position.left, position.top], [
                                                position.left + this._$element[0].offsetWidth,
                                                position.top + this._$element[0].offsetHeight
                                            ]
                                        ]));
                                    },

                                    _isElement: function (element) {
                                        return element && element[0] && element.find('.arrow')[0];
                                    }
                                }),

                            MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                                '{% if properties.subway != "" %}<div class="subway font-body-small">$[properties.subway]</div>{% endif %}' +
                                '<div class="adress font-title bold">$[properties.name] <br>$[properties.address]</div>' +
                                '<div class="pickup-contacts">' +
                                    '{% if properties.worktime != "" %}<div class="worktime font-body-2">$[properties.worktime]</div>{% endif %}' +
                                    '{% for arPhone in properties.phones %}' +
                                    '<div class="d-flex">' + 
                                    '<a href="tel:{{ arPhone.phone }}" class="phone font-body-2">{{ arPhone.phone }}</a>' +
                                    '<span class="font-body-small w-50 pl-3">{{ arPhone.additional }}</span>' + 
                                    '</div>' +
                                    '{% endfor %}' +
                                '</div>' +
                                '<div class="text font-body-2">$[properties.description]</div>' +
                                '{% if properties.includedServices != "" || properties.excludedServices != "" %}' + 
                                '<div class="pickup-advantages">' +
                                    '{% if properties.includedServices != "" %}<div class="pickup-adv present font-body-small">$[properties.includedServices]</div>{% endif %}' +
                                    '{% if properties.excludedServices != "" %}<div class="pickup-adv absent font-body-small">$[properties.excludedServices]</div>{% endif %}' +
                                '</div>' +
                                '{% endif %}'
                            );

                            var myCollection = new ymaps.GeoObjectCollection();

                            for (var i = 0, l = groups.length; i < l; i++) {
                                var group = groups[i];
                                for (var j = 0, m = group.items.length; j < m; j++)
                                {
                                    var item = group.items[j];

                                    myPlacemarks[item.id] = new ymaps.Placemark([
                                            parseFloat(item.center[0]),
                                            parseFloat(item.center[1])
                                        ], {
                                            subway: item.subway,
                                            address: item.address,
                                            name: item.name,
                                            phones: item.phones,
                                            email: item.email,
                                            worktime: item.worktime,
                                            description: item.description,
                                            includedServices: item.includedServices,
                                            excludedServices: item.excludedServices,
                                            center: item.center
                                        }, {
                                            balloonShadow: false,
                                            balloonLayout: MyBalloonLayout,
                                            balloonContentLayout: MyBalloonContentLayout,
                                            balloonPanelMaxMapArea: 0,
                                            iconLayout: 'default#image',
                                            iconImageHref: '<?=SITE_TEMPLATE_PATH?>/images/pin.svg',
                                            iconImageSize: [58, 58],
                                            iconImageOffset: [-29, -58]
                                        });

                                    myCluster.add(myPlacemarks[item.id]);
                                }

                                myMap.geoObjects.add(myCluster);

                                myMap.setBounds(myCluster.getBounds(), {
                                    checkZoomRange: true
                                });

                                /*var mySearchControl = new ymaps.control.SearchControl({
                                    options: {
                                        provider: new CustomSearchProvider(),
                                        noPlacemark: true,
                                        resultsPerPage: 5
                                    }
                                });

                                myMap.controls.add(mySearchControl, { float: 'left' });*/
                            }
                        });
                                    

                        clearTimeout(interval);
                    }
            
                }, 1000);
            });

        </script>
        <?
    }
}
<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$sID = "buy1click_" . randString(5);

if (!empty($arResult['ORDER']))
{
    include_once __DIR__ . '/confirm.php';
    return;
}
?>
<form method="post" id="<?=$sID?>" action="#" class="one-click-buy">
    <?
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['is_ajax'] == 'y')
        $APPLICATION->RestartBuffer();
    ?>
    <div class="modal-title font-h3"><?=GetMessage('HEADER_TITLE')?></div>
    
    <?
    if (!empty($arResult['ERRORS']))
    {
        $arSystemErrors = array();
        foreach ($arResult['ERRORS'] as $errCode => $errValue)
        {
            if (strpos($errCode, "PROP") !== false)
                continue;

            $arSystemErrors[] = $errValue;
        }

        if (!empty($arSystemErrors))
        {
            ?><div class="alert alert-danger"><?= implode("<br>", $arSystemErrors) ?></div><?
        }
    }
    
    
    foreach ($arResult['DISPLAY_PROPS'] as $arField)
    {
        ?>
        <div class="custom-input-wrap">
            <div class="custom-input<?=$arField['ERROR'] == 'Y' ? ' error' : ''?>">
                <input type="text" <?=($arField['IS_PHONE'] == "Y" || $arField['CODE'] == "PHONE") ? 'data-field-type="phone"' : ''?> value="<?=$arField['VALUE']?>" name="BUY1CLICK[<?=$arField['ID']?>]" id='buy_1_click_field_<?=$arField['ID']?>'>
                <label for="buy_1_click_field_<?=$arField['ID']?>"><?=$arField['NAME'] . (($arField['REQUIED'] == 'Y') ? ' *' : '')?></label>
            </div>
            <? if ($arField['ERROR'] == 'Y')
            {
                ?><div class="error-message"><?=$arResult['ERRORS']['PROP_' . $arField['ID']]?></div><?
            }
            ?>
        </div>
        <?
    }
    ?>
    
        <div class="custom-input-wrap">
            <div class="custom-input">
                <textarea name="BUY1CLICK[COMMENT]"><?=htmlspecialchars($_REQUEST['BUY1CLICK']['COMMENT'])?></textarea>
                <label><?=GetMessage('TEXTAREA_LABEL')?></label>
            </div>
        </div>
    
    <button type="submit" class="btn btn-primary btn-large"><?=GetMessage('SUBMIT_BUTTON')?></button>
    
    
    <?
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['is_ajax'] == 'y')
        return;
    ?>
</form>


<script>
    BX.ready(function () {

        BX.bind(BX('<?=$sID?>'), 'submit', BX.proxy(function (event) {

            BX.PreventDefault(event);

            var form = BX('<?=$sID?>'),
                    prepared = BX.ajax.prepareForm(form),
                    i, data;

            for (i in prepared.data)
            {
                if (prepared.data.hasOwnProperty(i) && i == '')
                {
                    delete prepared.data[i];
                }
            }

            data = !!prepared && prepared.data ? prepared.data : {};

            BX.ajax({
                    url: '<?=$APPLICATION->GetCurPageParam()?>',
                    method: 'POST',
                    dataType: 'html',
                    data: data,
                    timeout: 30,
                    emulateOnload: true,
                    onsuccess: function (result) {
                        if (!result)
                            return;

                        var ob = BX.processHTML(result);

                        BX('<?=$sID?>').innerHTML = ob.HTML;
                        BX.ajax.processScripts(ob.SCRIPT, true);

                        let scrollContainer = $('form.one-click-buy').parent('.scrollbar-inner');

                        scrollContainer.scrollTop(- 1);
                        scrollContainer.scrollTop(1);
                    }
            });
            
            return false;
        }, this));
    });

</script>
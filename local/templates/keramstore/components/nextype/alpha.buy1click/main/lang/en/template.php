<?
$MESS['HEADER_TITLE'] = "Buy in 1 click";
$MESS['SUBMIT_BUTTON'] = "Checkout";
$MESS['CONFIRM_MESSAGE'] = "You have successfully placed an order in 1 click. Order number ##ORDER_ID#";
$MESS['TEXTAREA_LABEL'] = "Comment";
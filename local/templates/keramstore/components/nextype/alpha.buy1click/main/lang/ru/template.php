<?
$MESS['HEADER_TITLE'] = "Купить в 1 клик";
$MESS['SUBMIT_BUTTON'] = "Оформить заказ";
$MESS['CONFIRM_MESSAGE'] = "Вы успешно оформили заказ в 1 клик. Номер заказа ##ORDER_ID#";
$MESS['TEXTAREA_LABEL'] = "Комментарий";
<div class="modal-title font-h3"><?=GetMessage('HEADER_TITLE')?></div>

<div class="one-click-buy-confirm">
    <div class="text font-body-2"><?= GetMessage('CONFIRM_MESSAGE', Array("#ORDER_ID#" => $arResult['ORDER']['ACCOUNT_NUMBER'])) ?></div>
</div>
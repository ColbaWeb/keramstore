<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;


$GLOBALS['CATALOG_SECTION_ID'] = $APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        "main",
        $arSectionParams,
        $component
);

$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;

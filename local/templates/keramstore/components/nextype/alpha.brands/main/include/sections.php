<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;


$APPLICATION->IncludeComponent(
	"nextype:alpha.brands.list",
	"main",
	array (
            "HL_BLOCK_ID" => $arParams['HL_BRANDS_ID'],
            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
            "LINK_PROP_ID" => $arParams['BRAND_PROP_ID'],
        ),
	$component
);
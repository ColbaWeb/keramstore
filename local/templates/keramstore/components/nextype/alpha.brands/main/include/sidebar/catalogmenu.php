<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

if (isset($arItemsSections) && !empty($arItemsSections))
{
    $arSections = array ();
    
    $requestURL = $APPLICATION->GetCurPage(true);
    $cacheId = $requestURL.implode("|", $arItemsSections)."|".SITE_ID."|".$arParams['IBLOCK_ID'];
    $cache = new CPHPCache;
    if ($cache->StartDataCache(3600, $cacheId, "ib_sections_tree"))
    {
        if (defined("BX_COMP_MANAGED_CACHE"))
        {
            $GLOBALS['CACHE_MANAGER']->StartTagCache("ib_sections_tree");
        }
        
        $selectedId = false;

        foreach ($arItemsSections as $sectionId)
        {
            $rsChains = CIBlockSection::GetNavChain($arParams['IBLOCK_ID'], $sectionId);
            while ($arChain = $rsChains->fetch())
            {
                $bNotFound = true;
                foreach ($arSections as $arSection)
                {
                    if ($arSection['ID'] == $arChain['ID'])
                    {
                        $bNotFound = false;
                        continue;
                    }

                }

                if ($bNotFound)
                {

                    $arChain['LINK'] = $arResult["FOLDER"] . CComponentEngine::MakePathFromTemplate($arParams["SEF_URL_TEMPLATES"]['brands_section'], array (
                            "BRAND_CODE" => $arResult['VARIABLES']['BRAND_CODE'],
                            "BRAND_ID" => $arResult['VARIABLES']['BRAND_ID'],
                            "SECTION_CODE" => $arChain['CODE'],
                    ));

                    if (isset($arResult['VARIABLES']['SECTION_ID']) && $arResult['VARIABLES']['SECTION_ID'] == $arChain['ID'])
                    {
                        $selectedId = $arChain['ID'];
                    }

                    $arSections[] = $arChain;
                }
            }
        }


        $obTree = new Tools\Tree($arSections);

        if ($selectedId)
            $obTree->modifChildToParent($selectedId, array (
                'SELECTED' => true
            ));

        $arSections = $obTree->getTree();

        if (!$selectedId && count($arSections) == 1)
        {
            $arSections[0]['SELECTED'] = true;
        }
        
        if (defined("BX_COMP_MANAGED_CACHE"))
            $GLOBALS['CACHE_MANAGER']->EndTagCache();
        
        $cache->EndDataCache($arSections);
    }
    else
    {
        $arSections = $cache->GetVars();
    }
}

if (!empty($arSections))
{
    ?>
    <div class="sidebar-menu font-body-2">
        <ul class="reset-ul-list list-0">
    <?
    foreach ($arSections as $arItem)
    {
        $arItemClasses = array ();
        
        if (!empty($arItem['SUB']))
            $arItemClasses[] = "has-sub";

        if (!empty($arItem['SELECTED']))
            $arItemClasses[] = "active open";
        
        ?>
            <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                <span>
                    <a href="<?= $arItem['LINK'] ?>"><?=$arItem['NAME']?></a>
                    <i class="icon icon-arrow-light-down"></i>
                </span>
                <?
                if (!empty($arItem['SUB']))
                {
                    ?>
                    <div class="sub">
                        <ul class="reset-ul-list list-1">
                            <?
                            foreach ($arItem['SUB'] as $arItem2)
                            {
                                $arItemClasses = array ();
        
                                if (!empty($arItem2['SUB']))
                                    $arItemClasses[] = "has-sub";

                                if (!empty($arItem2['SELECTED']))
                                    $arItemClasses[] = "active open";
                                ?>
                                <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                                    <span>
                                        <a href="<?= $arItem2['LINK'] ?>"><?=$arItem2['NAME']?></a>
                                        <i class="icon icon-arrow-light-down"></i>
                                    </span>
                                    
                                    <?
                                    if (!empty($arItem2['SUB']))
                                    {
                                        ?>
                                        <div class="sub">
                                            <ul class="reset-ul-list list-2">
                                                <?
                                                foreach ($arItem2['SUB'] as $arItem3)
                                                {
                                                    ?>
                                                        <li<?=!empty($arItem3['SELECTED']) ? ' class="active"' : ''?>>
                                                            <span>
                                                                <a href="<?= $arItem3['LINK'] ?>" class="text"><?=$arItem3['NAME']?></a>
                                                            </span>
                                                        </li>
                                                    <?
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                    <?
                }
                ?>
            </li>
        <?
    }
    ?>
        </ul>
    </div>    

    <div class="modal-container mobile hidden js-catalog-mobile">
        <div class="modal-content-container">
            <div class="scrollbar-inner">
                    <div class="content-wrapper">
                        <div class="menu-catalog-modal">

                                <div class="menu-container">
                                    <ul class="menu-list-1 list reset-ul-list">
                                        
                                        <?
                                        foreach ($arSections as $arItem)
                                        {
                                            $arItemClasses = array ();

                                            if (!empty($arItem['SUB']))
                                                $arItemClasses[] = "has-sub";

                                            if (!empty($arItem['SELECTED']))
                                                $arItemClasses[] = "active open";

                                            ?>
                                                <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                                                    <a href="<?= $arItem['LINK'] ?>"><?=$arItem['NAME']?></a>
                                                    <?
                                                    if (!empty($arItem['SUB']))
                                                    {
                                                        ?>
                                                        <div class="sub">
                                                            <ul class="menu-list-2 list reset-ul-list">
                                                                <?
                                                                foreach ($arItem['SUB'] as $arItem2)
                                                                {
                                                                    $arItemClasses = array ();

                                                                    if (!empty($arItem2['SUB']))
                                                                        $arItemClasses[] = "has-sub";

                                                                    if (!empty($arItem2['SELECTED']))
                                                                        $arItemClasses[] = "active open";
                                                                    ?>
                                                                    <li<?=!empty($arItemClasses) ? " class='".implode(" ", $arItemClasses)."'" : ""?>>
                                                                        <a href="<?= $arItem2['LINK'] ?>"><?=$arItem2['NAME']?></a>

                                                                        <?
                                                                        if (!empty($arItem2['SUB']))
                                                                        {
                                                                            ?>
                                                                            <div class="sub">
                                                                                <ul class="menu-list-3 list reset-ul-list">
                                                                                    <?
                                                                                    foreach ($arItem2['SUB'] as $arItem3)
                                                                                    {
                                                                                        ?>
                                                                                            <li<?=!empty($arItem3['SELECTED']) ? ' class="active"' : ''?>>
                                                                                                    <a href="<?= $arItem3['LINK'] ?>"><?=$arItem3['NAME']?></a>
                                                                                            </li>
                                                                                        <?
                                                                                    }
                                                                                    ?>
                                                                                    <li class="menu-back">
                                                                                        <a href="javascript:void(0)">
                                                                                            <i class="icon icon-arrow-light-right"></i>
                                                                                            <span><?=GetMessage('BACK_LINK')?></span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <?
                                                                        }
                                                                        ?>
                                                                    </li>
                                                                    <?
                                                                }
                                                                ?>
                                                                    <li class="menu-back">
                                                                        <a href="javascript:void(0)">
                                                                            <i class="icon icon-arrow-light-right"></i>
                                                                            <span><?=GetMessage('BACK_LINK')?></span>
                                                                        </a>
                                                                    </li>
                                                            </ul>
                                                        </div>
                                                        <?
                                                    }
                                                    ?>
                                                </li>
                                            <?
                                        }
                                        ?>
                                    </ul>
                                    
                                </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="close-area"></div>
    </div>


    <?
}
?>
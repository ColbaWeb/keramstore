<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

if (!empty($arResult['VARIABLES']))
{
?>
<div class="brand-info">
    <?
    if (!empty($arResult['VARIABLES']['UF_FILE']))
    {
        $arResize = CFile::ResizeImageGet($arResult['VARIABLES']['UF_FILE'], array ('width' => 250, 'height' => 250), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
        if ($arResize && !empty($arResize['src']))
        {
            echo Layout\Assets::showBackgroundImage($arResize['src'], array(
                    "attributes" => array(
                        "class=\"img\""
                    )
                ));
            }
    }
    
    if (!empty($arResult['VARIABLES']['UF_DESCRIPTION']))
    {
        ?><div class="desc font-body-small"><?=$arResult['VARIABLES']['UF_DESCRIPTION']?></div><?
    }
    ?>
</div>
<?
}
?>
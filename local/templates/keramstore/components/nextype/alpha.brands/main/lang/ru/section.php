<?
$MESS["CATALOG_MOBILE_SECTIONS_BUTTON_TITLE"] = "Категории";
$MESS["CATALOG_SECTIONS_BUTTON_TITLE"] = "Категории";
$MESS["CATALOG_PERSONAL_RECOM"] = "Персональные рекомендации";
$MESS["CATALOG_SORT_LABEL"] = "Сортировать:";
$MESS["CATALOG_SORT_DEFAULT"] = "По умолчанию";
$MESS["CATALOG_SORT_PRICE_ASC"] = "Cначала дешевые";
$MESS["CATALOG_SORT_PRICE_DESC"] = "Cначала дорогие";
$MESS["CATALOG_SORT_NAME"] = "По наименованию";
$MESS["CATALOG_SORT_RATING"] = "По рейтингу";
$MESS["CATALOG_VIEW_MODE_CARDS"] = "Отобразить плиткой";
$MESS["CATALOG_VIEW_MODE_LIST"] = "Отобразить списоком";
$MESS['NOT_SET_CATALOG_BRAND_PROP'] = "Не задано свойство привязки бренда к товару в настройках компонента";
$MESS['BACK_LINK'] = "Назад";
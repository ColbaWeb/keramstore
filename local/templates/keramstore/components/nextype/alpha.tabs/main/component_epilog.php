<?php

use Nextype\Alpha\Layout\Assets;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

Assets::getInstance()->addLess(array(
    SITE_TEMPLATE_PATH . '/less/tabs-with-goods.less',
));

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
	}

	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);

	if ($loadCurrency)
	{
		?>
		<script>
                    BX.ready(function () {
                        var interval = setInterval(function () {
                            if (!!window.BX.Currency)
                            {
                                window.BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
                                clearTimeout(interval);
                            }
                        }, 1000);
                        
                    });
		</script>
		<?
	}
}

?>
                <script>
                    BX.ready(function () {
                        var checkTabsWithGoodsHeight = function() {
                            if ($(window).width() > 767)
                            {
                                var tabsWithGoodsHeight = 0, tabsWithGoodsHeight = 0,
                                    firstElemTabsWithGoods = $('.tabs-with-goods .col-md-6:nth-child(2) .col-6:nth-child(3) .product-info').height(),
                                    secondElemTabsWithGoods = $('.tabs-with-goods .col-md-6:nth-child(2) .col-6:nth-child(4) .product-info').height();

                                firstElemTabsWithGoods >= secondElemTabsWithGoods ? tabsWithGoodsHeight = firstElemTabsWithGoods : tabsWithGoodsHeight = secondElemTabsWithGoods;

                                $('.tabs-with-goods .item.big').css('padding-bottom', tabsWithGoodsHeight + 17);
                            }
                            else
                            {
                                $('.tabs-with-goods .item.big').css('padding-bottom', 0);
                            }
                        };
                        
                        checkTabsWithGoodsHeight();
                        
                        $('body').on('click', '.tabs-with-goods .nav-item', function(){
                            checkTabsWithGoodsHeight();
                            
                            var content = $('.tabs-with-goods .tab-content .tab-pane:eq('+$(this).index()+')');
                            if (!!content && content.length > 0)
                            {
                                new window.Alpha.LazyForceLoad({
                                    parentNode: content
                                });
                            }
                        });
                        
                    });
                </script>
<?
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$templateData['TABS_ID'] = "bx_tabs" . $this->randString();

?>

<div class="wrapper-section">

    <div class="tabs-with-goods">
        <div class="section-name">
            <div class="container">
                <? if (!empty($arParams['SECTION_TITLE'])): ?>
                <div class="h2 title"><?=$arParams['SECTION_TITLE']?></div>
                <? endif; ?>
                
                <? if (!empty($arParams['SECTION_LINK_TITLE'])): ?>
                <a href="<?=$arParams['SECTION_LINK_URL']?>" class="subtitle font-large-button"><?=$arParams['SECTION_LINK_TITLE']?> <i class="icon icon-arrow-right-text-button"></i></a>
                <? endif; ?>
            </div>
        </div>
        <div class="nav-scroll">
            <ul class="nav nav-tabs reset-ul-list" id="<?=$templateData['TABS_ID']?>" role="tablist">
                <?
                $bFirst = true;
                foreach ($arResult['TABS'] as $key => $arTab)
                {
                    ?>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link<?=$bFirst ? ' active' : ''?>" data-toggle="tab" href="#<?=$templateData['TABS_ID'] . "_tab_" . $key?>" role="tab" aria-controls="<?=$templateData['TABS_ID'] . "_tab_" . $key?>" aria-selected="true"><?=$arTab['TITLE']?></a>
                    </li>
                    <?
                    $bFirst = false;
                }
                ?>
                
            </ul>
        </div>

        <div class="container">
            <div class="tab-content">
                
                <?
                $bFirst = true;
                foreach ($arResult['TABS'] as $key => $arTab)
                {
                    $componentParams = array(
                            'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                            'SECTION_ID' => "",
                            'SECTION_CODE' => "",
                            'CUSTOM_FILTER' => $arTab['FILTER'],
                            'FILTER_NAME' => $arParams['FILTER_NAME'],
                            'ELEMENT_SORT_FIELD' => $arParams['ELEMENT_SORT_FIELD'],
                            'ELEMENT_SORT_ORDER' => $arParams['ELEMENT_SORT_ORDER'],
                            'ELEMENT_SORT_FIELD2' => $arParams['ELEMENT_SORT_FIELD2'],
                            'ELEMENT_SORT_ORDER2' => $arParams['ELEMENT_SORT_ORDER2'],
                            'PROPERTY_CODE' => (isset($arParams['LIST_PROPERTY_CODE']) ? $arParams['LIST_PROPERTY_CODE'] : []),
                            'PROPERTY_CODE_MOBILE' => $arParams['LIST_PROPERTY_CODE_MOBILE'],
                            'INCLUDE_SUBSECTIONS' => $arParams['INCLUDE_SUBSECTIONS'],
                            'BASKET_URL' => $arParams['BASKET_URL'],
                            'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                            'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                            'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
                            'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                            'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
                            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                            'CACHE_TIME' => $arParams['CACHE_TIME'],
                            'CACHE_FILTER' => $arParams['CACHE_FILTER'],
                            'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                            'DISPLAY_COMPARE' => $arParams['USE_COMPARE'],
                            'PRICE_CODE' => $arParams['PRICE_CODE'],
                            'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                            'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
                            'PAGE_ELEMENT_COUNT' =>  $arParams['PAGE_ELEMENT_COUNT'],
                            'ELEMENT_COUNT_ROW' => $arParams['ELEMENT_COUNT_ROW'],
                            "SET_TITLE" => "N",
                            "SHOW_ALL_WO_SECTION" => $arParams['SHOW_ALL_WO_SECTION'],
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
                            'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                            'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
                            'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
                            'PRODUCT_PROPERTIES' => (isset($arParams['PRODUCT_PROPERTIES']) ? $arParams['PRODUCT_PROPERTIES'] : []),
                            'OFFERS_CART_PROPERTIES' => (isset($arParams['OFFERS_CART_PROPERTIES']) ? $arParams['OFFERS_CART_PROPERTIES'] : []),
                            'OFFERS_FIELD_CODE' => $arParams['LIST_OFFERS_FIELD_CODE'],
                            'OFFERS_PROPERTY_CODE' => (isset($arParams['LIST_OFFERS_PROPERTY_CODE']) ? $arParams['LIST_OFFERS_PROPERTY_CODE'] : []),
                            'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
                            'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
                            'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
                            'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],
                            'OFFERS_LIMIT' => (isset($arParams['LIST_OFFERS_LIMIT']) ? $arParams['LIST_OFFERS_LIMIT'] : 0),
                            'SECTION_URL' => $arParams['SECTION_URL'],
                            'DETAIL_URL' => $arParams['DETAIL_URL'],
                            'USE_MAIN_ELEMENT_SECTION' => $arParams['USE_MAIN_ELEMENT_SECTION'],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
                            'HIDE_NOT_AVAILABLE_OFFERS' => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],
                            'LABEL_PROP' => $arParams['LABEL_PROP'],
                            'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                            'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                            'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                            'PRODUCT_ROW_VARIANTS' => "[{'VARIANT':'3','BIG_DATA':false}]",
                            'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                            'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                            'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                            'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                            'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',
                            'DISPLAY_TOP_PAGER' => $arParams['DISPLAY_TOP_PAGER'],
                            'DISPLAY_BOTTOM_PAGER' => $arParams['DISPLAY_BOTTOM_PAGER'],
                            'HIDE_SECTION_DESCRIPTION' => $arParams['HIDE_SECTION_DESCRIPTION'],
                            'SHOW_FROM_SECTION' => $arParams['SHOW_FROM_SECTION'],
                            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                            'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
                            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                            'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                            'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['MESS_SHOW_MAX_QUANTITY']) ? $arParams['MESS_SHOW_MAX_QUANTITY'] : ''),
                            'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                            'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['MESS_RELATIVE_QUANTITY_MANY'] : ''),
                            'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['MESS_RELATIVE_QUANTITY_FEW'] : ''),
                            'MESS_BTN_BUY' => (isset($arParams['MESS_BTN_BUY']) ? $arParams['MESS_BTN_BUY'] : ''),
                            'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['MESS_BTN_ADD_TO_BASKET']) ? $arParams['MESS_BTN_ADD_TO_BASKET'] : ''),
                            'MESS_BTN_SUBSCRIBE' => (isset($arParams['MESS_BTN_SUBSCRIBE']) ? $arParams['MESS_BTN_SUBSCRIBE'] : ''),
                            'MESS_BTN_DETAIL' => (isset($arParams['MESS_BTN_DETAIL']) ? $arParams['MESS_BTN_DETAIL'] : ''),
                            'MESS_NOT_AVAILABLE' => (isset($arParams['MESS_NOT_AVAILABLE']) ? $arParams['MESS_NOT_AVAILABLE'] : ''),
                            'MESS_BTN_COMPARE' => (isset($arParams['MESS_BTN_COMPARE']) ? $arParams['MESS_BTN_COMPARE'] : ''),
                            'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                            'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                            'ADD_TO_BASKET_ACTION' => $basketAction,
                            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                            'COMPARE_PATH' => $arParams['COMPARE_PATH'],
                            'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                            'USE_COMPARE_LIST' => 'N',
                            'BACKGROUND_IMAGE' => '',
                            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
                                );
                    ?>
                    <div class="tab-pane<?=$bFirst ? ' active' : ''?>" id="<?=$templateData['TABS_ID'] . "_tab_" . $key?>" role="tabpanel" aria-labelledby="<?=$templateData['TABS_ID'] . "_tab_" . $key?>" >
                        <?
                        $APPLICATION->IncludeComponent('bitrix:catalog.section', 'tabs', $componentParams, $component);
                        ?>
                        
                    </div>
                    <?
                    $bFirst = false;
                }
                ?>
            </div>
        </div>
    </div>
</div>

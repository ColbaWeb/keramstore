<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;
use Nextype\Alpha\Options;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

if (!Loader::includeModule('nextype.alpha'))
    return;

$this->setFrameMode(true);

if (empty($arResult['ITEMS']))
    return;
?>

<div class="wrapper-section">
    <div class="advantages">
        <div class="container">
            <div class="advantage-list type-collection scroll">
                <?
                foreach ($arResult['ITEMS'] as $item)
                {
                    ?>
                    <div class="advantage">
                        <?
                        if (!empty($item['UF_FILE']))
                            {
                                if ($item['UF_FILE']['CONTENT_TYPE'] == "image/svg+xml")
                                {
                                    ?><div class="icon"><?= @file_get_contents($_SERVER['DOCUMENT_ROOT'] . $item['UF_FILE']['SRC']) ?></div><?
                                }
                                else
                                {
                                    ?><div class="icon"><img src="<?= $item['UF_FILE']['SRC'] ?>" alt="" title="" /></div><?
                                }
                            }
                        ?>

                        <div class="text-block">
                            <div class="advantage-title font-title"><?=$item['UF_NAME']?></div>
                            <?
                            if (!empty($item['UF_DESCRIPTION']))
                            {
                                ?><div class="advantage-text font-body-2"><?=$item['UF_DESCRIPTION']?></div><?
                            }
                            ?>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
    </div>
</div>
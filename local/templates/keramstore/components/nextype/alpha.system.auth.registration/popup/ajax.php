<?php
use Bitrix\Main\Text\Encoding;

define('DISABLED_REGIONS', true);
define('STOP_STATISTICS', true);

if (!isset($_REQUEST['siteId']) || !is_string($_REQUEST['siteId']))
    die();

if (!isset($_REQUEST['templateName']) || !is_string($_REQUEST['templateName']))
    die();

if ($_SERVER['REQUEST_METHOD'] != 'POST' ||
    preg_match('/^[A-Za-z0-9_]{2}$/', $_POST['siteId']) !== 1 ||
    preg_match('/^[.A-Za-z0-9_-]+$/', $_POST['templateName']) !== 1)
    die;

define('SITE_ID', $_REQUEST['siteId']);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/type/dictionary.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/errorcollection.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/text/encoding.php');

if (isset($_REQUEST['charset']) && strtolower($_REQUEST['charset']) != "utf-8")
{
    $_POST = Encoding::convertEncoding($_POST, 'UTF-8', $_REQUEST['charset']);
    $_GET = Encoding::convertEncoding($_GET, 'UTF-8', $_REQUEST['charset']);
    $_REQUEST = Encoding::convertEncoding($_REQUEST, 'UTF-8', $_REQUEST['charset']);
    $_COOKIE = Encoding::convertEncoding($_COOKIE, 'UTF-8', $_REQUEST['charset']);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

if (!\Bitrix\Main\Loader::includeModule('nextype.alpha'))
    die;

$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
    $signedParamsString = $request->get('signedParamsString') ?: '';
    $params = $signer->unsign($signedParamsString, 'nextype.alpha.system.auth.registration');
    $params = unserialize(base64_decode($params));
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
    die();
}

$params['AJAX_REQUEST'] = 'Y';

$APPLICATION->RestartBuffer();
header('Content-Type: text/html; charset='.LANG_CHARSET);

$APPLICATION->IncludeComponent('nextype:alpha.system.auth.registration', $request->get('templateName'), $params);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
<?
use Nextype\Alpha\Tools;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$formId = "bx_system_reg_form" . $this->randString();

if(isset($APPLICATION->arAuthResult))
    $arResult['REG_RESULT'] = $APPLICATION->arAuthResult;

?>
<form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data" id="<?=$formId?>">
    <? if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
        $APPLICATION->RestartBuffer();
    
    
    if ($USER->IsAuthorized())
    {
        ?>
                <div class="alert alert-success"><?= GetMessage('SUCCESS_REG_NOTE') ?></div>
                <script>
                    setTimeout(function () {
                        window.location.href = "<?= SITE_DIR?>";
                    }, 1500);
                </script>
        <?
    }
    else
    {

        if (isset($arResult['REG_RESULT']) && $arResult['REG_RESULT']['TYPE'] == "ERROR")
        {
            ?><div class="alert alert-danger"><?=is_array($arResult['REG_RESULT']['MESSAGE']) ? implode("<br>", $arResult['REG_RESULT']['MESSAGE']) : $arResult['REG_RESULT']['MESSAGE']?></div><?
        }

        if($arResult["SHOW_EMAIL_SENT_CONFIRMATION"])
        {
            ?><div class="alert alert-info"><?=GetMessage("AUTH_EMAIL_SENT")?></div><?
        }

        if(!$arResult["SHOW_EMAIL_SENT_CONFIRMATION"] && $arResult["USE_EMAIL_CONFIRMATION"] === "Y")
        {
            ?><div class="alert alert-info"><?=GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></div><?
        }
        ?>


        <? if($arResult["SHOW_SMS_FIELD"] == true || (isset($arResult['REG_RESULT']['SIGNED_DATA']) && !empty($arResult['REG_RESULT']['SIGNED_DATA']))): ?>

            <? ShowMessage($arParams["~AUTH_RESULT"]); ?>
            
        <div id="bx_register_error" class="alert alert-danger" style="display:none"><?ShowError("error")?></div>

        <div class="custom-input-wrap">
                <div class="custom-input">
                    <input size="30" type="text" name="SMS_CODE" value="" autocomplete="off" id='regform-field-smscode'>
                    <label for="regform-field-smscode"><?=GetMessage("main_register_sms_code")?> *</label>
                </div>
        </div>

        <div id="bx_register_resend"></div>

        <button type="submit" class="btn btn-primary btn-large"><?=GetMessage("main_register_sms_send")?></button>
        <input type="hidden" name="SIGNED_DATA" value="<?=isset($arResult['SIGNED_DATA']) ? htmlspecialcharsbx($arResult['SIGNED_DATA']) : htmlspecialcharsbx($arResult['REG_RESULT']['SIGNED_DATA'])?>" />
        <input type="hidden" name="code_submit_button" value="y" />

        <script>
        new BX.PhoneAuth({
                containerId: 'bx_register_resend',
                errorContainerId: 'bx_register_error',
                interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                data:
                        <?=CUtil::PhpToJSObject([
                                'signedData' => isset($arResult['SIGNED_DATA']) ? $arResult['SIGNED_DATA'] : $arResult['REG_RESULT']['SIGNED_DATA'],
                        ])?>,
                onError:
                        function(response)
                        {
                                var errorDiv = BX('bx_register_error');
                                var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
                                errorNode.innerHTML = '';
                                for(var i = 0; i < response.errors.length; i++)
                                {
                                        errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
                                }
                                errorDiv.style.display = '';
                        }
        });
        </script>

        <? elseif (!$arResult["SHOW_EMAIL_SENT_CONFIRMATION"]): ?>
            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="REGISTRATION" />

            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="text" id="regform-field-name" name="USER_NAME" maxlength="50" value="<?=$arResult["USER_NAME"]?>" />
                    <label for="regform-field-name"><?=GetMessage("AUTH_NAME")?></label>
                </div>
            </div>

            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="text" id="regform-field-last-name" name="USER_LAST_NAME" maxlength="50" value="<?=$arResult["USER_LAST_NAME"]?>" />
                    <label for="regform-field-last-name"><?=GetMessage("AUTH_LAST_NAME")?></label>
                </div>
            </div>

            <? if( (!$arResult["PHONE_REGISTRATION"] && !$arResult["EMAIL_REGISTRATION"]) ||
                 (($arResult["PHONE_REGISTRATION"] || $arResult["EMAIL_REGISTRATION"]) && (!$arResult["PHONE_REQUIRED"] && !$arResult["EMAIL_REQUIRED"]))
            ): ?>
            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="text" id="regform-field-login" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" />
                    <label for="regform-field-login"><?=GetMessage("AUTH_LOGIN_MIN")?></label>
                </div>
            </div>
            <? endif; ?>
            
            <?if($arResult["PHONE_REGISTRATION"]):?>
            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="text" id="regform-field-phone" data-field-type="phone" <?if($arResult["PHONE_REQUIRED"]):?>required="required"<?endif?> name="USER_PHONE_NUMBER" maxlength="255" value="<?=$arResult["USER_PHONE_NUMBER"]?>" />
                    <label for="regform-field-phone"><?=GetMessage("main_register_phone_number")?><?if($arResult["PHONE_REQUIRED"]):?> *<?endif?></label>
                </div>
            </div>
            <?endif?>

            <?if($arResult["EMAIL_REGISTRATION"]):?>
            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="text" id="regform-field-email" <?if($arResult["EMAIL_REQUIRED"]):?>required="required"<?endif?> name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" />
                    <label for="regform-field-email"><?=GetMessage("AUTH_EMAIL")?><?if($arResult["EMAIL_REQUIRED"]):?> *<?endif?></label>
                </div>
            </div>
            <?endif?>

            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="password" name="USER_PASSWORD" maxlength="50" />
                    <label><?=GetMessage("AUTH_PASSWORD_REQ")?></label>
                </div>
                <?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure" title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
                        <div class="bx-auth-secure-icon"></div>
                    </span>
                    <noscript>
                        <span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                            <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                        </span>
                    </noscript>
                    <script>
                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                    </script>
                <?endif?>
            </div>

            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" />
                    <label><?=GetMessage("AUTH_CONFIRM")?></label>
                </div>
            </div>

            <?
            if ($arResult["USE_CAPTCHA"] == "Y")
            {
                echo Tools\Forms::getCaptchaField('<img src="/bitrix/tools/captcha.php?captcha_sid='.$arResult["CAPTCHA_CODE"].'" width="180" height="40" alt="CAPTCHA" />', '<input type="hidden" name="captcha_sid" value="'.$arResult["CAPTCHA_CODE"].'" /><input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />');
            }

            $APPLICATION->IncludeComponent("bitrix:main.userconsent.request", "",
                        array(
                            "ID" => COption::getOptionString("main", "new_user_agreement", ""),
                            "IS_CHECKED" => "Y",
                            "AUTO_SAVE" => "N",
                            "IS_LOADED" => "Y",
                            "ORIGINATOR_ID" => $arResult["AGREEMENT_ORIGINATOR_ID"],
                            "ORIGIN_ID" => $arResult["AGREEMENT_ORIGIN_ID"],
                            "INPUT_NAME" => $arResult["AGREEMENT_INPUT_NAME"],
                            "REPLACE" => array(
                                "button_caption" => GetMessage("AUTH_REGISTER"),
                                "fields" => array(
                                    rtrim(GetMessage("AUTH_NAME"), ":"),
                                    rtrim(GetMessage("AUTH_LAST_NAME"), ":"),
                                    rtrim(GetMessage("AUTH_LOGIN_MIN"), ":"),
                                    rtrim(GetMessage("AUTH_PASSWORD_REQ"), ":"),
                                    rtrim(GetMessage("AUTH_EMAIL"), ":"),
                                )
                            ),
                        )
                );
                ?>

            <button type="submit" class="btn btn-primary btn-large" value="y" name="Register"><?=GetMessage("AUTH_REGISTER")?></button>

        <? endif; ?>
    
    <?
    }
    
    if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
        return;
    ?>
    
</form>

<?
$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'nextype.alpha.system.auth.registration');
?>
<script>
        BX.ready(function(){

            new window.Alpha.SignForm({
                siteId: '<?=SITE_ID?>',
                formId: '<?=CUtil::JSEscape($formId)?>',
                ajaxPath: '<?=$templateFolder?>/ajax.php?register=yes',
                templateName: '<?=CUtil::JSEscape($templateName)?>',
                signedParamsString: '<?= CUtil::JSEscape($signedParams) ?>'
            });

        });
    </script>

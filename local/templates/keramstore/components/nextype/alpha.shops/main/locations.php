<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

if (!Loader::includeModule('nextype.alpha'))
    return;

$this->setFrameMode(true);

$APPLICATION->IncludeComponent(
	"nextype:alpha.shops.list", 
	"main", 
	array(
		"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"LOCATION_CODE" => "",
		"MAP_PROVIDER" => $arParams['MAP_PROVIDER'],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["location"],
	),
	false
);
?>
<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;
use Nextype\Alpha\Options;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

if (!Loader::includeModule('nextype.alpha'))
    return;

$this->setFrameMode(true);

?>

<div class="wrapper-section">
    <div class="company-section">
        <div class="container">
            <div class="section-name">
                <?
                if (!empty($arParams['SECTION_TITLE']))
                {
                    ?><div class="h2 title"><?=htmlspecialchars_decode($arParams['SECTION_TITLE'])?></div><?
                }
                
                if (!empty($arParams['SECTION_LINK_TITLE']) && !empty($arParams['SECTION_LINK_URL']))
                {
                    ?>
                    <a href="<?=$arParams['SECTION_LINK_URL']?>" class="link font-large-button"><?=$arParams['SECTION_LINK_TITLE']?>
                        <i class="icon icon-arrow-right-text-button"></i>
                    </a>
                    <?
                }
                ?>
            </div>

            <div class="item">
                <div class="row">
                    <div class="col-6">
                        <?
                        if (!empty($arParams['DESCRIPTION_TITLE']))
                        {
                            ?><div class="title font-h3"><?=htmlspecialchars_decode($arParams['DESCRIPTION_TITLE'])?></div><?
                        }
                        
                        if (!empty($arParams['DESCRIPTION_TEXT']))
                        {
                            ?><div class="desc font-p"><?=htmlspecialchars_decode($arParams['DESCRIPTION_TEXT'])?></div><?
                        }
                        
                        if (!empty($arParams['DESCRIPTION_LINK_TITLE']) && !empty($arParams['DESCRIPTION_LINK_URL']))
                        {
                            ?>
                                <a href="<?=$arParams['DESCRIPTION_LINK_URL']?>" class="btn btn-link">
                                    <?=$arParams['DESCRIPTION_LINK_TITLE']?>
                                    <i class="icon icon-arrow-right-text-button"></i>
                                </a>
                            <?
                        }?>
                            <p>Интернет-магазин керамической плитки KERAMSTORE - это новые коллекции настенных, напольных, фасадных отделочных материалов. Мы работаем с брендами из:</p>
                            <ul>
                                <li>Италии;</li>
                                <li>Испании;</li>
                                <li>ОАЭ;</li>
                                <li>России;</li>
                                <li>Индии;</li>
                                <li>Турции;</li>
                                <li>Германии;</li>
                                <li>Португалии;</li>
                                <li>Китая.</li>
                            </ul>
                            <p>Предлагаем купить керамическую плитку в интернет-магазине или приехать к нам в салон, где представлены материалы европейских и российских производителей.</p>
                        <h2>Что мы предлагаем</h2>
                        <p>В каталоге можно увидеть разный товар, например, серии:</p>
                        <ul>
                            <li>Керамогранита: структурированного, матового, полуполированного, полированного.</li>
                            <li>Плитки с оригинальной текстурой и естественными оттенками.</li>
                            <li>Мозаики из камня, керамики и стекла. Различается по цветам, дизайну, размерам, - подходит для облицовки разных архитектурных форм.</li>
                            <li>Клинкера для садовых дорожек, фасада, приусадебных участков.</li>
                        </ul>
                        <p>Внушительный выбор дизайна, фактур и цветов – в салоне есть все для оформления пространства.</p>
                    </div>
                    <div class="col-6">
                        <div class="contacts">
                            <div class="image-block">
                                <?
                                if (!empty($arParams['SECTION_IMAGE']))
                                {
                                    echo Layout\Assets::showBackgroundImage($arParams['SECTION_IMAGE'], array(
                                            "attributes" => array(
                                                "class=\"bg-image\""
                                            )
                                    ));
                                }
                                
                                if ($arParams['DISPLAY_SOCIAL_LINKS'] && $arParams['DISPLAY_SOCIAL_LINKS'] == "Y")
                                {
                                    $arSocialLinks = array ();
                                    foreach (Array ('vk', 'fb', 'Im', 'ok', 'yt', 'tw', 'zen', 'telegram', 'ws', 'tiktok') as $key)
                                    {
                                        if (strlen(Options\Base::getInstance(SITE_ID)->getValue('socialLink' . ucfirst($key))) > 0)
                                        {
                                            $arSocialLinks[$key] = array (
                                                'icon' => 'icon-' . $key . '-white',
                                                'url' => Options\Base::getInstance(SITE_ID)->getValue('socialLink' . ucfirst($key))
                                            );
                                        }
                                    }
                                    
                                    if (!empty($arSocialLinks))
                                    {
                                        ?>
                                        <div class="social-block">
                                            <?
                                            foreach ($arSocialLinks as $item)
                                            {
                                                ?><a href="<?=$item['url']?>" target="_blank" class="item"><i class="icon <?=$item['icon']?>"></i></a><?
                                            }
                                            ?>
                                        </div>
                                        <?
                                    }
                                }
                                ?>
                            </div>
                            <div class="content">
                                <?
                                if (!empty($arResult['CONTACTS']['ADDRESS']))
                                {
                                    ?><div class="location font-body-2"><?=$arResult['CONTACTS']['ADDRESS']?></div><?
                                }
                                
                                if (!empty($arResult['CONTACTS']['WORKTIME']))
                                {
                                    ?><div class="time font-body-2"><?=$arResult['CONTACTS']['WORKTIME']?></div><?
                                }
                                
                                if (!empty($arResult['CONTACTS']['PHONE']))
                                {
                                    ?><a href="tel:<?=Tools\Formatter::phoneToInt($arResult['CONTACTS']['PHONE'])?>" class="phone font-body-2"><?=$arResult['CONTACTS']['PHONE']?></a><?
                                }
                                
                                if (!empty($arResult['CONTACTS']['EMAIL']))
                                {
                                    ?><a href="mailto:<?=$arResult['CONTACTS']['EMAIL']?>" class="email font-body-2"><?=$arResult['CONTACTS']['EMAIL']?></a><?
                                }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <h2>Как мы работаем</h2>
                        <p>Предлагаем оригинальные коллекции европейских и российских производителей, в которых собраны качественные, сертифицированные отделочные материалы, соответствующие актуальным тенденциям. Интернет-каталог регулярно обновляется. Ответим на вопросы покупателей: консультанты расскажут о характеристиках материалов, посоветуют, какой вид облицовки стоит взять для конкретного случая.</p>
                        <p>В KERAMSTORE можно приобрести керамическую плитку оптом или в розницу. Для постоянных клиентов предусмотрены скидки. Сотрудничаем с дизайнерскими и архитектурными бюро, строительно-ремонтными организациями.</p>
                        <p>По интересующим вопросам просьба обращаться к менеджерам по тел. 8 (495) 134-66-55.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
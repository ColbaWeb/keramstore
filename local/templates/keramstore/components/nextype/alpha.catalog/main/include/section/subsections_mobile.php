<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

$arParams["ADD_SECTIONS_CHAIN"] = "N";
$arParams["TOP_DEPTH"] = 1;

/*echo "<pre>";
print_r($arParams);
echo "</pre>";*/

$APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "categories_mobile",
        $arParams,
        $component,
        array("HIDE_ICONS" => "Y")
);
unset($sectionListParams);
?>
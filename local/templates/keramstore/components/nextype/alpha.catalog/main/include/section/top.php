<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

Loader::includeModule("highloadblock");

if($arResult['SECTION']['PROPERTIES']["UF_WAREHOUSE"]['VALUE'] || $arResult['SECTION']['PROPERTIES']["UF_IN_SALON"]['VALUE']){
    $showRightBlock = true;
}

if($arResult['SECTION']['PROPERTIES']['UF_SEO_BOTTOM']['VALUE']){
    $showLeftBlock = true;
}

/*echo "<pre>";
print_r($arResult['SECTION']['DESCRIPTION']);
echo "</pre>";*/

if(!empty($arResult['SECTION']['DESCRIPTION'])) {
    $textCollection = $arResult['SECTION']['DESCRIPTION'];
} else {
    $textCollection = htmlspecialchars_decode($arResult['SECTION']['PROPERTIES']['UF_SEO_BOTTOM']['VALUE']);
}


$arSelect = Array("ID", "NAME", "PROPERTY_COUNTRY", "DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>16, "ID" =>$arResult['SECTION']['PROPERTIES']["UF_BRANDS"]["VALUE"] ,"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $brand = $ob->GetFields();
}

$hlbl = 7;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
    "select" => array("UF_NAME", "UF_FILE"),
    "filter" => array("UF_XML_ID" => $brand["PROPERTY_COUNTRY_VALUE"])
));
while($arData = $rsData->Fetch()){
    $country["NAME"] = $arData["UF_NAME"];
    $country["IMG"] = CFile::GetPath($arData["UF_FILE"]);
}
?>
<p>Фабрика: <a href="<?=$brand["DETAIL_PAGE_URL"]?>"><?=$brand["NAME"]?></a></p>
<?php if(!empty($country["NAME"])) {?>
    <p>Страна: <?=$country["NAME"]?> <img width="20px;" src="<?=$country["IMG"]?>"></p>
    <?}?>


<?/*if($showRightBlock || $showLeftBlock):*/?>
    <div class="row">
        <div class="collect col-12 col-lg-<?=($showRightBlock ? '9' : '12')?>">
            <div class="text_collection">
                <?=$textCollection;?>
            </div>
            <p class="clbtn">
                <a class="btn btn-primary text_collection__btn">
                    Подробнее
                </a>
            </p>
        </div>
        <script>

        </script>
        <?if($showRightBlock):?>
            <div class="col-12 col-lg-3">
                <div class="additional-properties">
                    <?if($arResult['SECTION']['PROPERTIES']["UF_WAREHOUSE"]['VALUE']):?>
                        <div class="prop">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/warehouse.svg" alt="">
                            <span>Складская программа</span>
                        </div>
                    <?endif;?>
                    <?if($arResult['SECTION']['PROPERTIES']["UF_IN_SALON"]['VALUE']):?>
                        <div class="prop">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/showroom.svg" alt="">
                            <span>Представлена в салоне</span>
                        </div>
                    <?endif;?>
                    <div class="text">Узнать о наличии можно по телефону</div>
                    <a class="tel" href="tel:84951346655">8 495 134-66-55</a>
                </div>
            </div>
        <?endif;?>

    </div>
<?/*endif;*/?>
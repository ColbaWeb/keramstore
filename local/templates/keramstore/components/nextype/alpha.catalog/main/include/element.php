<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

$componentElementParams["DOPSEO"] = "каталог коллекций";

$GLOBALS['CATALOG_CURRENT_ELEMENT_ID'] = $APPLICATION->IncludeComponent('bitrix:catalog.element', 'main', $componentElementParams, $component);


<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

$arSelect = Array("ID");
$arFilter = Array("IBLOCK_ID"=>16, "ID" =>$arResult['SECTION']['PROPERTIES']["UF_BRANDS"]["VALUE"] ,"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $brand = $ob->GetFields();
}

global $arrOtherCollections;
$arrOtherCollections = array("=UF_BRANDS" => $brand["ID"], "!ID" => $arResult['SECTION']["ID"]);

$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "other_collections",
    array(
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "nt_alpha_catalog",
        "IBLOCK_ID" => "5",
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "3",
        "SECTION_FIELDS" => array(
            0 => "NAME",
            1 => "DESCRIPTION",
            2 => "PICTURE",
            3 => "DETAIL_PICTURE",
            4 => "IBLOCK_SECTION_ID",
        ),
        "SECTION_USER_FIELDS" => array(
            0 => "UF_NEW",
            1 => "UF_HIT",
            2 => "UF_SALE",
            3 => "UF_SURFACE",
            4 => "UF_SIZES",
            5 => "UF_IN_SALON",
            6 => "UF_MIN_PRICE",
            7 => "UF_BRANDS",
        ),
        "ADD_SECTIONS_CHAIN" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "N",
        "COMPONENT_TEMPLATE" => "other_collections",
        "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
        "FILTER_NAME" => "arrOtherCollections",
        "CACHE_FILTER" => "Y",
        "SECTION_TITLE" => "Другие коллекции данной фабрики",
        "SECTION_LINK_TYPE" => "C",
        "SECTION_LINK_TITLE" => "",
        "SECTION_LINK_URL" => "/catalog/",
        "SHOW_SECTIONS_ID" => array(
        ),
        "SHOW_SECTIONS_COUNT" => "20",
        "TYPE_HOME" => "FACTORY"
    ),
    false
);
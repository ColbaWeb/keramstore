<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;

/**
 * @global CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arParams
 * @var array $arResult
 * @var array $arCurSection
 * @var array $customFilter
 *
 */

/*session_start();

function search($array, $key, $value)
{
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, search($subarray, $key, $value));
        }
    }

    return $results;
}*/

$template = !empty(Options\Base::getInstance(SITE_ID)->getValue('catalogTemplateSectionsList')) ? Options\Base::getInstance(SITE_ID)->getValue('catalogTemplateSectionsList') : "main";






/*if($arResult["VARIABLES"]["SMART_FILTER_PATH"]) {

    $parts = explode("/", $arResult["VARIABLES"]["SMART_FILTER_PATH"]);

    $customFilter = [];
    foreach($parts as $filtitem) {
        $partsitem = explode("-is-", $filtitem);

        $partname = $partsitem[0];
        $partcode = $partsitem[1];
        $partname = strtoupper($partname);


        $find = search($_SESSION["SMARTFILTER"], 'URL_ID', $partcode);

        $valuefilt = $find[0]["VALUE"];
        $customFilter["PROPERTY_".$partname."_VALUE"] = $valuefilt;
    }

}

echo "<pre>";
print_r($arResult);
echo "</pre>";*/

global $arrFilter;


if(!empty($customFilter)){
    $arrFilter  = array_merge($customFilter, $arrFilter);
}

$idSections = array();

$arSelectEl = Array("ID", "NAME", "IBLOCK_SECTION_ID");
$res = CIBlockElement::GetList(Array(), $arrFilter, false, Array(), $arSelectEl);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $idSections[] = $arFields["IBLOCK_SECTION_ID"];
}

$idSections = array_unique($idSections);

global $arrFilterCollection;
//$arrFilterCollection = array("=ID" => $idSections);

$sectionListParams['DOPSECT'] = $idSections;
$sectionListParams['FILTER_NAME'] = 'arrFilterCollection';
$sectionListParams['SECTION_USER_FIELDS'] = array("UF_NEW","UF_HIT","UF_SALE","UF_IN_SALON","UF_SIZES","UF_SURFACE","UF_MIN_PRICE","UF_BRANDS", "UF_SIZES_LINE", "UF_SURFACE_LINE");

$sectionListParams["CUSTOM_SECTION_SORT"] = array("DATE_UPDATE" => "DESC");
/*echo "<pre>";
print_r($sectionListParams);
echo "</pre>";*/

$APPLICATION->SetPageProperty('title', "Каталог коллекций Keramstore");

$curdesc = "Каталог коллекций керамической плитки и керамогранита интернет-магазина Keramstore. Купите в Москве по выгодным ценам.";

$APPLICATION->SetPageProperty('description', $curdesc);

$APPLICATION->IncludeComponent(
	"custom:catalog.sectioncol.list",
    "collections",
	$sectionListParams,
	$component,
    array("HIDE_ICONS" => "Y")
);
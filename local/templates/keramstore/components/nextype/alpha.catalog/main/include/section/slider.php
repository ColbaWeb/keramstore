<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

$containerId = "bx_slider_" . randString();

?>
<?if($arResult["SECTION"]["PROPERTIES"]["UF_PICTURES"]["VALUE"]):?>
<link rel="stylesheet"  href="<?=SITE_TEMPLATE_PATH . '/vendor/fancybox/fancybox.css'?>"/>
<script src="<?=SITE_TEMPLATE_PATH . '/vendor/fancybox/fancybox.umd.js'?>"></script>
<div class="col-12 flexcol">
<div class="collection-fancybox">
    <h4>Фото коллекции</h4>

<!--    <div class="flex flex-wrap gap-5 justify-center max-w-5xl mx-auto px-6">-->
<!--        --><?//foreach($arResult["SECTION"]["PROPERTIES"]["UF_PICTURES"]["VALUE"] as $image){?>
<!--            <a data-fancybox="gallery" href="--><?//=CFile::GetPath($image)?><!--">-->
<!--                <img class="rounded" src="--><?//=CFile::GetPath($image)?><!--" />-->
<!--            </a>-->
<!--        --><?//}?>
<!--    </div>-->

    <div id="mainCarousel" class="carousel w-10/12 max-w-5xl mx-auto">
        <?foreach($arResult["SECTION"]["PROPERTIES"]["UF_PICTURES"]["VALUE"] as $image){?>
            <div
                    class="carousel__slide"
                    data-src="<?=CFile::GetPath($image)?>"
                    data-fancybox="gallery"
                    >
                <img src="<?=CFile::GetPath($image)?>" />
            </div>
        <?}?>
    </div>

    <div id="thumbCarousel" class="carousel max-w-xl mx-auto">
        <?foreach($arResult["SECTION"]["PROPERTIES"]["UF_PICTURES"]["VALUE"] as $image){?>
            <div class="carousel__slide">
                <img class="panzoom__content" src="<?=CFile::GetPath($image)?>" />
            </div>
        <?}?>
    </div>
</div>
    <div class="col-4 advant">
        <?
        $APPLICATION->IncludeComponent(
            "nextype:alpha.advantages",
            "collection_advantages",
            array(
                "COMPONENT_TEMPLATE" => "collection_advantages",
                "HL_ADVANTAGES_ID" => "2",
                "HL_ADVANTAGES_DISPLAY_ID" => array(
                    0 => "1",
                    1 => "2",
                    2 => "3",
                    3 => "4",
                )
            ),
            false
        );
        ?>
    </div>
</div>

<style>
    #mainCarousel {
        max-width: 1100px;
        height: 750px;
        margin: 0 auto 1rem auto;

        --carousel-button-color: #170724;
        --carousel-button-bg: #fff;
        --carousel-button-shadow: 0 2px 1px -1px rgb(0 0 0 / 20%),
        0 1px 1px 0 rgb(0 0 0 / 14%), 0 1px 3px 0 rgb(0 0 0 / 12%);

        --carousel-button-svg-width: 20px;
        --carousel-button-svg-height: 20px;
        --carousel-button-svg-stroke-width: 2.5;
    }

    #mainCarousel .carousel__slide {
        width: 100%;
        padding: 0;
    }

    #mainCarousel .carousel__button.is-prev {
        left: -1.5rem;
    }

    #mainCarousel .carousel__button.is-next {
        right: -1.5rem;
    }

    #mainCarousel .carousel__button:focus {
        outline: none;
        box-shadow: 0 0 0 4px #A78BFA;
    }

    #thumbCarousel .carousel__slide {
        opacity: 0.5;
        padding: 0;
        margin: 0.25rem;
        width: 96px;
        height: 64px;
    }

    #thumbCarousel .carousel__slide img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 4px;
    }

    #thumbCarousel .carousel__slide.is-nav-selected {
        opacity: 1;
    }

    .collection-fancybox img {
        width: 100% !important;
        /*object-fit: contain;*/
    }

    #mainCarousel .carousel__viewport{
        border-radius: 12px;
    }

    #mainCarousel .carousel__button:focus {
        box-shadow: 0 0 0 4px #b8aa83;
    }


    /*a[data-fancybox] img {*/
    /*    cursor: zoom-in;*/
    /*}*/

    /*.fancybox__backdrop::after {*/
    /*    content: "";*/
    /*    position: absolute;*/
    /*    width: 10%;*/
    /*    height: 10%;*/
    /*    filter: blur(2px);*/
    /*    left: 50%;*/
    /*    top: 50%;*/
    /*    transform: scale(11);*/
    /*    opacity: 0.3;*/
    /*    background-image: var(--bg-image);*/
    /*    background-size: cover;*/
    /*    background-repeat: no-repeat;*/
    /*    background-position: center center;*/
    /*}*/

    /*.fancybox__container {*/
    /*    --fancybox-bg: #000;*/

    /*    --fancybox-thumbs-width: 48px;*/
    /*    --fancybox-thumbs-ratio: 1;*/

    /*    --carousel-button-bg: rgb(91 78 76 / 74%);*/

    /*    --carousel-button-svg-width: 24px;*/
    /*    --carousel-button-svg-height: 24px;*/

    /*    --carousel-button-svg-stroke-width: 2.5;*/
    /*}*/

    /*.fancybox__nav {*/
    /*    --carousel-button-svg-width: 24px;*/
    /*    --carousel-button-svg-height: 24px;*/
    /*}*/

    /*.fancybox__nav .carousel__button.is-prev {*/
    /*    left: 20px;*/
    /*}*/

    /*.fancybox__nav .carousel__button.is-next {*/
    /*    right: 20px;*/
    /*}*/

    /*.carousel__button.is-close {*/
    /*    right: auto;*/
    /*    top: 20px;*/
    /*    left: 20px;*/
    /*}*/

    /*.fancybox__slide {*/
    /*    padding: 8px 88px;*/
    /*}*/

    /*!* Thumbnails *!*/
    /*.fancybox__thumbs .carousel__slide {*/
    /*    padding: 8px 8px 16px 8px;*/
    /*}*/

    /*.is-nav-selected::after {*/
    /*    display: none;*/
    /*}*/

    /*.fancybox__thumb {*/
    /*    border-radius: 6px;*/
    /*    opacity: 0.4;*/
    /*}*/

    /*.fancybox__thumb:hover,*/
    /*.is-nav-selected .fancybox__thumb {*/
    /*    border-radius: 6px;*/
    /*    opacity: 1;*/
    /*}*/

    /*.is-nav-selected .fancybox__thumb::after {*/
    /*    display: none;*/
    /*}*/

    /*.collection-fancybox img{*/
    /*    height: 157px;*/
    /*}*/
</style>

<script>
    // Initialise Carousel
    const mainCarousel = new Carousel(document.querySelector("#mainCarousel"), {
        Dots: false,
    });

    // Thumbnails
    const thumbCarousel = new Carousel(document.querySelector("#thumbCarousel"), {
        Sync: {
            target: mainCarousel,
            friction: 0,
        },
        Dots: false,
        Navigation: false,
        center: true,
        slidesPerPage: 1,
        infinite: false,
    });

    // Customize Fancybox
    Fancybox.bind('[data-fancybox="gallery"]', {
        Carousel: {
            on: {
                change: (that) => {
                    mainCarousel.slideTo(mainCarousel.findPageForSlide(that.page), {
                        friction: 0,
                    });
                },
            },
        },
    });


    // Fancybox.bind('[data-fancybox="gallery"]', {
    //     dragToClose: false,
    //
    //     Toolbar: false,
    //     closeButton: "top",
    //
    //     Image: {
    //         zoom: false,
    //     },
    //
    //     on: {
    //         initCarousel: (fancybox) => {
    //             const slide = fancybox.Carousel.slides[fancybox.Carousel.page];
    //
    //             fancybox.$container.style.setProperty(
    //                 "--bg-image",
    //                 `url("${slide.$thumb.src}")`
    //             );
    //         },
    //         "Carousel.change": (fancybox, carousel, to, from) => {
    //             const slide = carousel.slides[to];
    //
    //             fancybox.$container.style.setProperty(
    //                 "--bg-image",
    //                 `url("${slide.$thumb.src}")`
    //             );
    //         },
    //     },
    // });
</script>
<?endif;?>
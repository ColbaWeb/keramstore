<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

global $arrCatalogFilter;

$propertiesMap = array(
    '=PROPERTY_229' => 'SURFACE',
    '=PROPERTY_238' => 'FORM',
    '=PROPERTY_234' => 'TYPE_TILE',
    '=PROPERTY_232' => 'STYLE',
);

foreach ($propertiesMap as $prop => $code){
    if($arrCatalogFilter[$prop]){
        foreach ($arrCatalogFilter[$prop] as $v){
            $filterValue[$code][] = $v;
        }
    }
}

$res = CIBlockSection::GetList(array(), array("ACTIVE" => "Y", "SECTION_ID" => $arParams["SECTION_ID"], "DEPTH_LEVEL" => 3, 'PROPERTY'=>$filterValue), false, array("ID"));
while($ar_result = $res->GetNext())
{
    $idSections[] = $ar_result["ID"];
}

global $arrFilterCollection;
$arrFilterCollection = array("=ID" => $idSections);

$arParams['FILTER_NAME'] = 'arrFilterCollection';


$APPLICATION->IncludeComponent(
    "bitrix:catalog.sectioncol.list",
    "collections",
    $arParams,
    $component,
    array("HIDE_ICONS" => "Y")
);
unset($sectionListParams);
?>
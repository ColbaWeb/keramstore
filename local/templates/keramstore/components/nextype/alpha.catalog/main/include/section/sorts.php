<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

$arSorts = $arSelectedSort = $arViewModes = Array ();

if (is_array($arParams['LIST_SORTS']))
{
    foreach ($arParams['LIST_SORTS'] as $sortKey)
    {
        switch ($sortKey)
        {
            case 'PRICE':
                
                $arSorts['PRICE_ASC'] = array (
                    'NAME' => Loc::getMessage('CATALOG_SORT_PRICE_ASC'),
                    'FIELD' => 'property_MIN_PRICE',
                    'ORDER' => 'asc',
                    'CHECKED' => false
                );
                
                $arSorts['PRICE_DESC'] = array (
                    'NAME' => Loc::getMessage('CATALOG_SORT_PRICE_DESC'),
                    'FIELD' => 'property_MIN_PRICE',
                    'ORDER' => 'desc',
                    'CHECKED' => false
                );
                
                break;
            
            case 'NAME':
                
                $arSorts['NAME'] = array (
                    'NAME' => Loc::getMessage('CATALOG_SORT_NAME'),
                    'FIELD' => 'name',
                    'ORDER' => 'asc',
                    'CHECKED' => false
                );
                
                break;
            
            case 'RATING':
                
                $arSorts['RATING'] = array (
                    'NAME' => Loc::getMessage('CATALOG_SORT_RATING'),
                    'FIELD' => 'property_RATING',
                    'ORDER' => 'desc',
                    'CHECKED' => false
                );
                
                break;
        }
    }
    
    $arSorts['DEFAULT'] = Array(
        'NAME' => Loc::getMessage('CATALOG_SORT_DEFAULT'),
        'FIELD' => $arParams['ELEMENT_SORT_FIELD'],
        'ORDER' => $arParams['ELEMENT_SORT_ORDER'],
        'CHECKED' => false
    );
}

if (!empty($arSorts))
{
    $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    
    foreach ($arSorts as $key => $arSort)
    {
        if (strtolower($arSort['FIELD']) == strtolower($request->get('sort')) && strtolower($arSort['ORDER']) == strtolower($request->get('order')))
        {
            $arSelectedSort = $arSort;
            $arSorts[$key]['CHECKED'] = true;
            
            $GLOBALS['ELEMENT_SORT_FIELD'] = $arSort['FIELD'];
            $GLOBALS['ELEMENT_SORT_ORDER'] = $arSort['ORDER'];
        }
        
        if ($key != "DEFAULT")
            $arSorts[$key]['LINK'] = $APPLICATION->GetCurPageParam('sort=' . strtolower($arSort['FIELD']) . '&order=' . strtolower($arSort['ORDER']), array ('sort', 'order'));
        else
            $arSorts[$key]['LINK'] = $APPLICATION->GetCurPageParam('', array ('sort', 'order'));
    }
    
    if (!$arSelectedSort)
    {
        $arSelectedSort = $arSorts['DEFAULT'];
        $arSorts['DEFAULT']['CHECKED'] = true;
    }
    
}

if (is_array($arParams['LIST_VIEW_MODE_VARIANTS']) && !empty($arParams['LIST_VIEW_MODE_VARIANTS']))
{
    $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();    
    $requestMode = (strlen($request->get('mode'))) ? $request->get('mode') : false;

    foreach ($arParams['LIST_VIEW_MODE_VARIANTS'] as $mode)
    {
        switch ($mode)
        {
            case 'CARDS':
                $arViewModes['CARDS'] = array (
                    'NAME' => Loc::getMessage('CATALOG_VIEW_MODE_CARDS'),
                    'LINK' => $APPLICATION->GetCurPageParam('mode=cards', array ('mode')),
                    'CHECKED' => !$requestMode && !empty($arParams['LIST_VIEW_MODE']) && $arParams['LIST_VIEW_MODE'] == $mode,
                    'ICON' => 'icon-3col-grid',
                );
                
                if ($requestMode == "cards")
                {
                    $arViewModes['CARDS']['CHECKED'] = true;
                    $GLOBALS['LIST_VIEW_MODE'] = $mode;
                }
                
                break;
            
            case 'LIST':
                $arViewModes['LIST'] = array (
                    'NAME' => Loc::getMessage('CATALOG_VIEW_MODE_LIST'),
                    'LINK' => $APPLICATION->GetCurPageParam('mode=list', array ('mode')),
                    'CHECKED' => !$requestMode && !empty($arParams['LIST_VIEW_MODE']) && $arParams['LIST_VIEW_MODE'] == $mode,
                    'ICON' => 'icon-list-grid',
                );
                
                if ($requestMode == "list")
                {
                    $arViewModes['LIST']['CHECKED'] = true;
                    $GLOBALS['LIST_VIEW_MODE'] = $mode;
                }
                
                break;
        }
    }
}

?>
<h3>Товары</h3>
<!--noindex-->
<div class="sort-filter">
    <? if (!empty($arSorts)): ?>
    <div class="sort" data-role="sort-dropdown">
        <div class="sort-title font-body-2">
            <?=Loc::getMessage('CATALOG_SORT_LABEL')?>
            <a href="javascript:void(0);" data-role="sort-dropdown-title" class="sort-link">
                <span><?=$arSelectedSort['NAME']?></span>
                <i class="icon icon-arrow-light-down"></i>
            </a>

        </div>
        <ul class="sort-list reset-ul-list">
            <?
            foreach ($arSorts as $arSort)
            {
                ?>
                <li class="link">
                    <a href="<?=$arSort['LINK']?>" rel="nofollow" class="font-body-2<?=$arSort['CHECKED'] ? ' active' : ''?>" data-role="sort-dropdown-item" data-value="<?=$arSort['NAME']?>"><?=$arSort['NAME']?></a>
                </li>
                <?
            }
            ?>
            
        </ul>
    </div>
    <? endif; ?>

    <? $APPLICATION->ShowViewContent('catalog_section_nav_record_count_print'); ?>

    <?
    if (!empty($arViewModes))
    {
        ?>
        <div class="buttons">
            <? 
            foreach ($arViewModes as $arMode)
            {
                ?>
                <a href="<?=$arMode['LINK']?>" rel="nofollow" title="<?=$arMode['NAME']?>" class="show-link<?=$arMode['CHECKED'] ? ' active' : ''?>">
                    <i class="icon <?=$arMode['ICON']?>"></i>
                </a>
                <?
            }
            ?>
            
        </div>
        <?
    }
    ?>
    
</div>

    <div class="modal-container mobile hidden js-sort-mobile">
        <div class="modal-content-container">
            <div class="scrollbar-inner">
                <div class="content-wrapper">
                    <div class="sorting-modal">
                        <div class="sorting-title font-body-2 bold">
                            <?=Loc::getMessage('CATALOG_SORT_LABEL')?>
                        </div>
                        <div class="sorting-list">
                        <?
                        foreach ($arSorts as $arSort)
                        {
                            ?>
                            <a href="<?=$arSort['LINK']?>" rel="nofollow" class="font-body-2<?=$arSort['CHECKED'] ? ' active' : ''?>"><?=$arSort['NAME']?></a>
                            <?
                        }
                        ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="close-area"></div>
    </div>
<!--/noindex-->
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

?>

<div class="modal-container horizontal-filter js-catalog-categories hidden">
    <div class="modal-content-container">
        <div class="scrollbar-inner">
            <div class="content-wrapper">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:menu", "catalog_modal", Array(
                        "ROOT_MENU_TYPE" => "catalog",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "catalog",
                        "USE_EXT" => "Y",
                        "ALLOW_MULTI_SELECT" => "N",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600000",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => ""
                    )
                );
                ?>
            </div>
        </div>
    </div>
    <div class="close-area"></div>
</div>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

session_start();

function search($array, $key, $value)
{
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, search($subarray, $key, $value));
        }
    }

    return $results;
}


$this->setFrameMode(true);

$sectionListParams = array(
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
	"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
	"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
	"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
	"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
	"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
);
if ($sectionListParams["COUNT_ELEMENTS"] === "Y")
{
	$sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_ACTIVE";
	if ($arParams["HIDE_NOT_AVAILABLE"] == "Y")
	{
		$sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_AVAILABLE";
	}
}

/*echo "<pre>";
print_r($arResult["VARIABLES"]["SECTION_CODE_PATH"]);
echo "</pre>";*/

$arResult["VARIABLES"]["SMART_FILTER_PATH"] = str_replace("filter/","",$arResult["VARIABLES"]["SECTION_CODE_PATH"]);
$arResult["VARIABLES"]["SMART_FILTER_PATH"] = str_replace("/apply","",$arResult["VARIABLES"]["SMART_FILTER_PATH"]);

/*echo "<pre>";
print_r($arResult["VARIABLES"]["SMART_FILTER_PATH"]);
echo "</pre>";*/

/*echo "<pre>";
print_r($arResult);
echo "</pre>";*/



$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "applyfilters", array(
    "IBLOCK_ID" => "5",
    "IBLOCK_TYPE" => "nt_alpha_catalog",
    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    "FILTER_NAME" => "arrFilter",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "36000000",
    "CACHE_TYPE" => "A",
    "SAVE_IN_SESSION" => "N",
    "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
    "XML_EXPORT" => "N",
    "SECTION_TITLE" => "-",
    "SECTION_DESCRIPTION" => "-",
    "HIDE_NOT_AVAILABLE" => "Y",
    "TEMPLATE_THEME" => "blue",
    "CONVERT_CURRENCY" => "N",
    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
    "SEF_MODE" => "Y",
    "SEF_RULE" => "/catalog/filter/#SMART_FILTER_PATH#/apply/",
    "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
    "PAGER_PARAMS_NAME" => "arrPager",
    "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
),
    $component,
    array('HIDE_ICONS' => 'Y')
);


$APPLICATION->IncludeComponent(
    "bitrix:catalog.smart.filter",
    "horizontal",
    array(
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CONVERT_CURRENCY" => "N",
        "DISPLAY_ELEMENT_COUNT" => "Y",
        "FILTER_NAME" => "arrFilter",
        "FILTER_VIEW_MODE" => "horizontal",
        "HIDE_NOT_AVAILABLE" => "N",
        "IBLOCK_ID" => "5",
        "IBLOCK_TYPE" => "nt_alpha_catalog",
        "PAGER_PARAMS_NAME" => "arrPager",
        "POPUP_POSITION" => "left",
        "PREFILTER_NAME" => "smartPreFilter",
        /*"PRICE_CODE" => array(
            0 => "Выгрузка на сайт",
        ),*/
        "SAVE_IN_SESSION" => "N",
        "SECTION_CODE" => "",
        "SECTION_DESCRIPTION" => "-",
        "SECTION_ID" => "",
        "SECTION_TITLE" => "-",
        "SEF_MODE" => "Y",
        "TEMPLATE_THEME" => "wood",
        "XML_EXPORT" => "N",
        "SEF_RULE" => "/catalog/filter/#SMART_FILTER_PATH#/apply/",
        //"SMART_FILTER_PATH" => $_REQUEST["SMART_FILTER_PATH"],
        "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
        "SHOW_ALL_WO_SECTION" => "Y",
        "COMPONENT_TEMPLATE" => ".default",
        "SECTION_CODE_PATH" => ""
    ),
    false
);



if($arResult["VARIABLES"]["SECTION_CODE_PATH"]) {

    $parts = explode("/", $arResult["VARIABLES"]["SECTION_CODE_PATH"]);

    $customFilter = [];
    foreach($parts as $filtitem) {
        $partsitem = explode("-is-", $filtitem);

        if(count($partsitem) > 1) {
            $partname = $partsitem[0];
            $partcode = $partsitem[1];
            $partname = strtoupper($partname);


            $find = search($_SESSION["SMARTFILTER"], 'URL_ID', $partcode);

            $valuefilt = $find[0]["VALUE"];
            $customFilter["PROPERTY_".$partname."_VALUE"] = $valuefilt;
        }

    }

}

Layout\Partial::getInstance(__DIR__)->render('sections.php', array (
    'sectionListParams' => $sectionListParams,
    'customFilter' => $customFilter,
    'component' => $component,
    'arFunctionParams' => ($arParams["SHOW_TOP_ELEMENTS"] !== "N" ? array("HIDE_ICONS" => "Y") : array())
), false, false);



$afterContentFile = $arResult['FOLDER'] . "sections_after_content.php";
if (file_exists($_SERVER['DOCUMENT_ROOT'] . $afterContentFile) && strlen(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $afterContentFile)) > 0)
{
    ?><div class="inner-page-content catalog-sections-inner-text"><?$APPLICATION->IncludeFile($afterContentFile)?></div><?
}
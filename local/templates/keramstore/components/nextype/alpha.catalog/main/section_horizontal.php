<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

/**
 * @global CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arParams
 * @var array $arResult
 * @var array $arCurSection
 */

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
	$basketAction = isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '';
}
else
{
	$basketAction = isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '';
}

//region Catalog Section
$sectionListParams = array(
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
    "TOP_DEPTH" => 1,
    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
    "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
    "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
    "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
    "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
    "SECTION_USER_FIELDS" => array("UF_NEW", "UF_HIT", "UF_SALE", "UF_IN_SALON", "UF_SURFACE", "UF_SIZES", "UF_MIN_PRICE"),
);

if ($sectionListParams["COUNT_ELEMENTS"] === "Y")
{
    $sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_ACTIVE";
    
    if ($arParams["SECTIONS_HIDE_EMPTY"] == "Y")
    {
        $sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_AVAILABLE";
    }
}

if (ModuleManager::isModuleInstalled("sale"))
{
    $arRecomData = array();
    $recomCacheID = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
    $obCache = new CPHPCache();
    
    if ($obCache->InitCache(36000, serialize($recomCacheID), "/sale/bestsellers"))
    {
        $arRecomData = $obCache->GetVars();
    }
    elseif ($obCache->StartDataCache())
    {
        if (Loader::includeModule("catalog"))
        {
            $arSKU = CCatalogSku::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
            $arRecomData['OFFER_IBLOCK_ID'] = (!empty($arSKU) ? $arSKU['IBLOCK_ID'] : 0);
        }
        $obCache->EndDataCache($arRecomData);
    }
}

$arResult['SECTION_ID'] = CIBlockFindTools::GetSectionID(
    $arResult["VARIABLES"]["SECTION_ID"],
    $arResult["VARIABLES"]["SECTION_CODE"],
    array('IBLOCK_ID' => $arParams['IBLOCK_ID'])
);

global $arrFilter;
$idSections = array();

$arFilterSEC = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$arResult["VARIABLES"]["SECTION_ID"]);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilterSEC, true);
while($ar_result = $db_list->GetNext())
{
    $idSections[] = $ar_result['ID'];
}

$idSections = array_unique($idSections);

if(count($idSections) > 0) {

    $arFilter["=SECTION_ID"] = $idSections;
    global $arrCatalogFilter;
    $arrCatalogFilter["=SECTION_ID"] = $idSections;
    Layout\Partial::getInstance(__DIR__)->render('collection_sections.php', array (
        'sectionListParams' => $sectionListParams,
        'component' => $component,
        'arFunctionParams' => ($arParams["SHOW_TOP_ELEMENTS"] !== "N" ? array("HIDE_ICONS" => "Y") : array())
    ), false, false);
} else {
    include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/section_collection.php");
}

?>


<? Layout\Ajax::getInstance(__DIR__)->render('top/catalogmenu', array (
    'arParams' => $arParams,
    'arResult' => $arResult,
), false, false) ?>

<!--noindex-->
    <? Layout\Partial::getInstance(__DIR__)->render('section/subsections_mobile', array (
        'arParams' => $sectionListParams,
        'component' => $component
    ), false, false) ?>
<!--/noindex-->
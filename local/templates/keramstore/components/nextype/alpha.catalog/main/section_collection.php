<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

/**
 * @global CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arParams
 * @var array $arResult
 * @var array $arCurSection
 * @var array $sectionListParams
 * @var string $sectDepth
 * @var string $basketAction
 */

?>

<div class="wrapper-section">
    <?
    Layout\Partial::getInstance(__DIR__)->render('section/top.php', array (
        'arParams' => $arParams,
        'arResult' => $arResult,
        'component' => $component
    ), false, false)
    ?>
    <?
    Layout\Partial::getInstance(__DIR__)->render('section/slider.php', array (
    'arParams' => $arParams,
    'arResult' => $arResult,
    'component' => $component
    ), false, false)
    ?>


    <!-- <div class="container"> -->
    <div class="row">
        <div class="col">
            <div class="content-wrapper catalog-inner">
                <? Layout\Partial::getInstance(__DIR__)->render('section/mobilebuttons', array (
                    'arParams' => $arParams,
                    'arResult' => $arResult,
                    'component' => $component,
                ), false, false) ?>

                <? Layout\Partial::getInstance(__DIR__)->render('section/sorts', array (
                    'arParams' => $arParams,
                    'arResult' => $arResult,
                    'component' => $component
                ), false, false) ?>

                <? Layout\Partial::getInstance(__DIR__)->render('section.php', array (
                    'arParams' => $arParams,
                    'arResult' => $arResult,
                    'component' => $component
                ), false, false) ?>

                <?
//                if (ModuleManager::isModuleInstalled("sale"))
//                {
//                    if (!empty($arRecomData))
//                    {
//                        if (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N')
//                        {
//                            Layout\Partial::getInstance(__DIR__)->render('section/recommend', array (
//                                'intSectionID' => $GLOBALS['CATALOG_SECTION_ID'],
//                                'basketAction' => $basketAction,
//                                'arParams' => $arParams,
//                                'arResult' => $arResult,
//                                'component' => $component
//                            ), false, false);
//                        }
//                    }
//                }


                Layout\Partial::getInstance(__DIR__)->render('section/other_collections.php', array (
                    'arParams' => $arParams,
                    'arResult' => $arResult,
                    'component' => $component
                ), false, false)
                ?>

            </div>
        </div>
    </div>
    <!-- </div> -->
</div>

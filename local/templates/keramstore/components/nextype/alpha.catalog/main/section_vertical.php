<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

echo("sections_v");

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

/**
 * @global CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arParams
 * @var array $arResult
 * @var array $arCurSection
 */

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
	$basketAction = isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '';
}
else
{
	$basketAction = isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '';
}

//region Catalog Section
$sectionListParams = array(
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
    "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
    "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
    "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
    "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
    "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
);

if ($sectionListParams["COUNT_ELEMENTS"] === "Y")
{
    $sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_ACTIVE";
    
    if ($arParams["SECTIONS_HIDE_EMPTY"] == "Y")
    {
        $sectionListParams["COUNT_ELEMENTS_FILTER"] = "CNT_AVAILABLE";
    }
}

if (ModuleManager::isModuleInstalled("sale"))
{
    $arRecomData = array();
    $recomCacheID = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
    $obCache = new CPHPCache();
    
    if ($obCache->InitCache(36000, serialize($recomCacheID), "/sale/bestsellers"))
    {
        $arRecomData = $obCache->GetVars();
    }
    elseif ($obCache->StartDataCache())
    {
        if (Loader::includeModule("catalog"))
        {
            $arSKU = CCatalogSku::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
            $arRecomData['OFFER_IBLOCK_ID'] = (!empty($arSKU) ? $arSKU['IBLOCK_ID'] : 0);
        }
        $obCache->EndDataCache($arRecomData);
    }
}


?>

<div class="wrapper-section">
    <? Layout\Partial::getInstance(__DIR__)->render('section/subsections', array (
        'arParams' => $sectionListParams,
        'component' => $component
    ), false, false) ?>
    
    <? Layout\Partial::getInstance(__DIR__)->render('section/applyfilters', array (
        'arParams' => $arParams,
        'arResult' => $arResult,
        'component' => $component,
        'arCurSection' => $arCurSection
    ), false, false) ?>
    
    <!-- <div class="container"> -->
        <div class="row">
            <div class="col-auto sidebar-left">
                <? Layout\Partial::getInstance(__DIR__)->render('sidebar/catalogmenu', array (), false, false) ?>
                
                <? Layout\Partial::getInstance(__DIR__)->render('sidebar/filter', array (
                    'arParams' => $arParams,
                    'arResult' => $arResult,
                    'component' => $component,
                    'arCurSection' => $arCurSection
                ), false, false) ?>
                
            </div>
            <div class="col">
                <div class="content-wrapper catalog-inner">
                    
                    <? Layout\Partial::getInstance(__DIR__)->render('section/mobilebuttons', array (
                        'arParams' => $arParams,
                        'arResult' => $arResult,
                        'component' => $component,
                    ), false, false) ?>
                    
                    <? Layout\Partial::getInstance(__DIR__)->render('section/sorts', array (
                        'arParams' => $arParams,
                        'arResult' => $arResult,
                        'component' => $component
                    ), false, false) ?>
                    
                    <? Layout\Partial::getInstance(__DIR__)->render('section.php', array (
                        'arParams' => $arParams,
                        'arResult' => $arResult,
                        'component' => $component
                    ), false, false) ?>

                </div>
            </div>
        </div>
</div>

<?
    if (ModuleManager::isModuleInstalled("sale"))
    {
        if (!empty($arRecomData))
        {
            if (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N')
            {
                Layout\Partial::getInstance(__DIR__)->render('section/recommend', array(
                    'intSectionID' => $GLOBALS['CATALOG_SECTION_ID'],
                    'basketAction' => $basketAction,
                    'arParams' => $arParams,
                    'arResult' => $arResult,
                    'component' => $component
                        ), false, false);
            }
        }
    }
?>
<!--noindex-->
    <? Layout\Partial::getInstance(__DIR__)->render('section/subsections_mobile', array (
        'arParams' => array_merge($sectionListParams, array ('ADD_SECTIONS_CHAIN' => "N")),
        'component' => $component
    ), false, false) ?>
<!--/noindex-->
<?php

use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

Layout\Assets::getInstance()->addLess(array (
    SITE_TEMPLATE_PATH . '/less/instagram.less',
));

?>

<script>
    BX.ready(function () {
        new Swiper('#<?=$templateData['CONTAINER_ID']?>', {
            slidesPerView: 'auto',
            spaceBetween: 8,
            lazy: true,
            freeMode: true,
            on: {
                slideChange: function () {
                }
            },

            breakpoints: {
                992: {
                    spaceBetween: 24,
                    slidesPerView: 3,
                    slidesPerGroup: 3,
                    freeMode: false,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                },

            }
        });
    });
    </script>
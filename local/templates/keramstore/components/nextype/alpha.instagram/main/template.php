<?
use Nextype\Alpha\Layout;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$templateData['CONTAINER_ID'] = "bx_instagram_slider" . $this->randString();

if (!empty($arResult['ITEMS']))
{
?>

<div class="wrapper-section">
    <div class="instagram-section">
        <div class="container">
            <? if (!empty($arParams['SECTION_NAME']) || !empty($arParams['SECTION_LINK_TEXT'])): ?>
            <div class="section-name">
                <? if (!empty($arParams['SECTION_NAME'])): ?>
                <div class="h2 title"><?=$arParams['SECTION_NAME']?></div>
                <? endif; ?>
                
                <? if (!empty($arParams['SECTION_LINK_TEXT']) && !empty($arParams['SECTION_LINK_URL'])): ?>
                <a href="<?=$arParams['SECTION_LINK_URL']?>" target="_blank" class="link font-large-button"><?=$arParams['SECTION_LINK_TEXT']?> <i
                        class="icon icon-arrow-right-text-button"></i></a>
                <? endif; ?>
            </div>
            <? endif; ?>
            
            <div class="instagram-list ">
                <div class="swiper-slider ">
                    <!-- Swiper -->
                    <div class="swiper-container main-slider items" id="<?=$templateData['CONTAINER_ID']?>">
                        <div class="swiper-wrapper">
                            <? foreach ($arResult['ITEMS'] as $arItem):
                                if (in_array($arItem['TYPE'], array ('VIDEO')))
                                    continue;
                            ?>
                            <div class="swiper-slide">
                                <div class="item">
                                    <a href="<?=$arItem['DETAIL_URL']?>" class="link" target="_blank">
                                        <div class="image swiper-lazy" data-background="<?=$arItem['DETAIL_PICTURE']?>"><?=Layout\Assets::showSpinner()?></div>
                                        
                                        <? if (!empty($arItem['TEXT'])): ?>
                                        <div class="text_wrapper">
                                            <div class="scrollbar-inner">
                                                <div class="text font-body">
                                                    <!--<span class="font-body-small date"></span>-->
                                                    <span>
                                                        <?=$arItem['TEXT']?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <? endif; ?>
                                    </a>
                                </div>
                            </div>
                            <? endforeach; ?>
                            
                            
                            
                        </div>
                    </div>
                    <!-- Add Arrows -->
                </div>
                <div class="swiper-arrows">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
}
?>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Catalog;
use Bitrix\Iblock;


$arTemplateParameters = array(
	"SECTION_NAME" => array(
		"NAME" => GetMessage("SECTION_NAME"),
		"TYPE" => "STRING",
		'DEFAULT' => "",
		"PARENT" => "VISUAL"
	),
    
        "SECTION_LINK_TEXT" => array(
		"NAME" => GetMessage("SECTION_LINK_TEXT"),
		"TYPE" => "STRING",
		'DEFAULT' => "",
		"PARENT" => "VISUAL"
	),
    
	"SECTION_LINK_URL" => array(
		"NAME" => GetMessage("SECTION_LINK_URL"),
		"TYPE" => "STRING",
		'DEFAULT' => "",
		"PARENT" => "VISUAL"
	),
	
);

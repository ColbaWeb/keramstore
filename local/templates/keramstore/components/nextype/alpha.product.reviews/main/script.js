(function(window){
	'use strict';

	if (window.Alpha.ProductReviews)
		return;

	window.Alpha.ProductReviews = function(arParams)
	{
            this.obContainerList = null;
            this.obReviewsList = null;
            this.obReviewsItems = null;
            this.obContainerForm = null;
            this.obForm = null;
            this.siteId = null;
            this.templateName = null;
            this.messages = {};
            
            this.ajaxUrl = arParams.AJAX_URL;
            this.siteId = arParams.SITE_ID;
            this.templateName = arParams.TEMPLATE_NAME;
            this.signedParamsString = arParams.SIGNED_PARAMS;
            
            if (!!arParams.VISUAL.CONTAINER_LIST_ID)
            {
                this.obContainerList = BX(arParams.VISUAL.CONTAINER_LIST_ID);
                
                if (this.obContainerList === undefined)
                    return;
                
                this.obReviewsList = this.obContainerList.querySelector('[data-entity="list"]');
                
                if (!!this.obReviewsList)
                    this.obReviewsItems = this.obReviewsList.querySelectorAll('[data-entity="item"]');
            }
            
            if (!!arParams.VISUAL.CONTAINER_FORM_ID)
            {
                this.obContainerForm = BX(arParams.VISUAL.CONTAINER_FORM_ID);
                
                if (!!this.obContainerForm)
                    this.obForm = this.obContainerForm.querySelector("form");
            }
            
            if (!!arParams.MESSAGES)
            {
                this.messages = arParams.MESSAGES;
            }
            
            
            BX.ready(BX.delegate(this.initList, this));
            BX.ready(BX.delegate(this.initForm, this));
        };
        
        window.Alpha.ProductReviews.prototype = {
            
            initList: function () {
               
                if (!!this.obReviewsItems)
                {
                    
                    if (this.obReviewsItems.length > 0)
                    {
                        for (var i = 0; i < this.obReviewsItems.length; i++)
                        {
                            var item = this.obReviewsItems[i];
                            if (!!item && item.getAttribute('data-entity-id'))
                            {

                                var btnLike = BX.findChild(item, {
                                    attribute: {'data-entity': "btn-like"}
                                }, true);
                                BX.bind(btnLike, 'click', BX.proxy(this.doLike, this));

                                var btnDislike = BX.findChild(item, {
                                    attribute: {'data-entity': "btn-dislike"}
                                }, true);
                                BX.bind(btnDislike, 'click', BX.proxy(this.doDislike, this));
                            }
                        }
                    }
                
                }
                
                var buttons = this.obContainerList.querySelectorAll('[data-entity="button-show-form"]');
                    if (buttons.length > 0)
                    {
                        for (var i = 0; i < buttons.length; i++)
                        {
                            BX.bind(buttons[i], 'click', BX.proxy(this.doOpenForm, this));
                        }
                    }
                
                
            },
            
            initForm: function () {
               
                if (!!this.obContainerForm)
                {
                    this.initVoteStars();
                    
                    BX.bind(this.obForm, 'submit', BX.proxy(function (event) {
                        
                        BX.PreventDefault(event);
                        
                        var data = {
                            action: 'save',
                            values: this.getAllFormData()
                        };
                        
                        this.sendRequest(data, this.obForm, BX.delegate(this.initVoteStars, this));
                        
                        return false;
                        
                    }, this));
                }
                
            },
            
            initVoteStars: function () {
                if (!!this.obContainerForm)
                {
                    var voteStars = this.obContainerForm.querySelectorAll('[data-entity="vote-stars"] [data-entity="star"]');
                    if (voteStars.length > 0)
                    {
                        for (var i = 0; i < voteStars.length; i++)
                        {
                            BX.bind(voteStars[i], 'click', BX.proxy(function () {
                                var target = BX.proxy_context,
                                    starsContainer = BX.findParent(target, {
                                        attribute: {'data-entity': "vote-stars"}
                                    }),
                                    input = BX.findChild(starsContainer, {
                                        attribute: {'data-entity': "vote-stars-input"}
                                    }, true),
                                    index = parseInt(target.getAttribute('data-value'));
                                    
                                    
                                for (var i = 0; i < voteStars.length; i++)
                                {
                                    BX.addClass(voteStars[i], 'empty');
                                    
                                    if (i <= index - 1)
                                        BX.removeClass(voteStars[i], 'empty');
                                }
                                
                                input.value = index;
                                    
                            }, this));
                        }
                    }
                }
            },
            
            doOpenList: function () {
                
                if (!!this.obContainerList)
                {
                    BX.removeClass(this.obContainerList, 'hidden');
                    $('body').trigger('modalOpen');
                    BX.addClass(document.body, 'modal-opened');
                }
                
                if (!!this.obContainerForm)
                {
                    BX.addClass(this.obContainerForm, 'hidden');
                }
            },
            
            doOpenForm: function () {
                
                if (!!this.obContainerList)
                {
                    BX.addClass(this.obContainerList, 'hidden');
                    $('body').trigger('modalOpen');
                    BX.addClass(document.body, 'modal-opened');
                }
                
                if (!!this.obContainerForm)
                {
                    BX.removeClass(this.obContainerForm, 'hidden');
                }
            },
            
            doLike: function (event) {
                var target = BX.proxy_context;
                this.doLikeDislike(target, 'like');
            },
            
            doDislike: function (event) {
                var target = BX.proxy_context;
                this.doLikeDislike(target, 'dislike');
            },
            
            doLikeDislike(target, action) {

                if (!target)
                    return;
                
                var item = BX.findParent(target, {
                    attribute: {'data-entity': "item"}
                });
                
                if (!item || !item.getAttribute('data-entity-id'))
                    return;
                
                if (item.getAttribute('data-has-liked') != "false")
                {
                    if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                    {
                        new window.Alpha.Messages({
                            type: 'error',
                            title: this.messages.ALREADY_VOTED_TITLE,
                            message: this.messages.ALREADY_VOTED_MESSAGE
                        });
                    }
                    
                    return;
                }
                
                var reviewId = item.getAttribute('data-entity-id'),
                    diffContainer =  BX.findChild(item, {attribute: {'data-entity': "diff-likes-dislikes"}}, true),
                    diffValue = parseInt(diffContainer.innerText);
                
                
                diffValue = (action == "dislike") ? diffValue - 1 : diffValue + 1;
                
                BX.removeClass(diffContainer, 'positive');
                BX.removeClass(diffContainer, 'negative');
                if (diffValue < 0)
                {
                    BX.adjust(diffContainer, {
                        text: "-" + Math.abs(diffValue)
                    });
                    BX.addClass(diffContainer, 'negative');
                }
                else if (diffValue > 0)
                {
                    BX.adjust(diffContainer, {
                        text: "+" + Math.abs(diffValue)
                    });
                    BX.addClass(diffContainer, 'positive');
                }
                else
                {
                    BX.adjust(diffContainer, {
                        text: diffValue
                    });
                }
                
                BX.addClass(target, 'voted');
                BX.adjust(item, {
                    attrs: {
                        'data-has-liked': 'true'
                    }
                });
                
                this.sendRequest({
                    action: action,
                    reviewId: reviewId
                });
                
            },
            
            setDisabledLikeDislike: function (itemId) {
                
                if (this.obReviewsItems.length > 0)
                {
                    for (var i = 0; i < this.obReviewsItems.length; i++)
                    {
                        var item = this.obReviewsItems[i];
                        if (!!item && item.getAttribute('data-entity-id') && item.getAttribute('data-entity-id') == itemId)
                        {
                            BX.adjust(item, {
                                attrs: {
                                    'data-has-liked': 'true'
                                }
                            });
                            
                            break;
                        }
                    }
                }
            },
            
            sendRequest: function (data, ajaxContainer, callback)
            {
                ajaxContainer = (ajaxContainer) || null;
                callback = (callback) || null;
                
                data.sessid = BX.bitrix_sessid();
                data.siteId = this.siteId;
                data.templateName = this.templateName;
                data.signedParamsString = this.signedParamsString;
                data.charset = BX.message('LANG_CHARSET');
                
                BX.ajax({
                    url: this.ajaxUrl,
                    method: 'POST',
                    dataType: 'html',
                    data: data,
                    timeout: 30,
                    emulateOnload: true,
                    onsuccess: function (result) {
                        if (!result)
                            return;
                        
                        if (!!ajaxContainer)
                        {
                            var ob = BX.processHTML(result);

                            ajaxContainer.innerHTML = ob.HTML;
                            BX.ajax.processScripts(ob.SCRIPT, true);
                            
                            if (typeof(callback) == 'function')
                                callback.call(this);
                        }

                    }
                });

            },
            
            getAllFormData: function ()
            {
                var form = BX(this.obForm),
                    prepared = BX.ajax.prepareForm(form),
                    i;

                for (i in prepared.data)
                {
                    if (prepared.data.hasOwnProperty(i) && i == '')
                    {
                        delete prepared.data[i];
                    }
                }

                return !!prepared && prepared.data ? prepared.data : {};
            },
            
        }
        
})(window);
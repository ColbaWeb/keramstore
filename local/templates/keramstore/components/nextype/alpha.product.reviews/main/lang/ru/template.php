<?php

$MESS['CP_TPL_REVIEWS_LIST_TITLE'] = "Отзывы о #VALUE#";
$MESS['CP_TPL_REVIEWS_AVG_RATING_TITLE'] = "Общий рейтинг";
$MESS['CP_TPL_REVIEWS_MAKE_REVIEW_BUTTON'] = "Оставить отзыв";
$MESS['CP_TPL_REVIEW_ITEM_COMMENT'] = "Комментарий";
$MESS['CP_TPL_REVIEW_ITEM_ADVANTAGES'] = "Достоинства";
$MESS['CP_TPL_REVIEW_ITEM_DISADVANTAGES'] = "Недостатки";
$MESS['CP_TPL_REVIEW_ALREADY_VOTED_TITLE'] = "Голосовать за отзыв";
$MESS['CP_TPL_REVIEW_ALREADY_VOTED_MESSAGE'] = "Вы уже голосовали за этот отзыв";

$MESS['CP_TPL_REVIEW_FORM_TITLE'] = "Оставить отзыв";
$MESS['CP_TPL_REVIEW_FORM_DESCRIPTION'] = "Мы ожидаем от вас честный и корректный отзыв. Поэтому не публикуем отзывы не по теме, отзывы с нецензурными выражениями и оскорблениями, а также с сылками на другие сайты.";
$MESS['CP_TPL_REVIEW_FORM_FIELD_RATING'] = "Общая оценка";
$MESS['CP_TPL_REVIEW_FORM_FIELD_NAME'] = "Имя";
$MESS['CP_TPL_REVIEW_FORM_FIELD_EMAIL'] = "E-mail";
$MESS['CP_TPL_REVIEW_FORM_FIELD_ADVANTAGES'] = "Достоинства";
$MESS['CP_TPL_REVIEW_FORM_FIELD_DISADVANTAGES'] = "Недостатки";
$MESS['CP_TPL_REVIEW_FORM_FIELD_COMMENT'] = "Комментарий";
$MESS['CP_TPL_REVIEW_FORM_SUBMIT_BUTTON'] = "Отправить отзыв";
$MESS['CP_TPL_REVIEW_FORM_BACK_BUTTON'] = "Назад";
$MESS['CP_TPL_REVIEW_FORM_CAPTCHA_LABEL'] = "Введите код с картинки";
$MESS['CP_TPL_REVIEW_FORM_COMPLETED'] = "Ваш отзыв отправлен на модерацию.<br> После прохождения модерации, он появится на сайте.";
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (isset($arResult['PRODUCT']) && CModule::IncludeModule('catalog'))
{
    $arResult['PRODUCT'] = array_merge($arResult['PRODUCT'], CCatalogProduct::GetOptimalPrice($arResult['PRODUCT']['ID']));
}
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;
use Nextype\Alpha\Reviews;

/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */


if (!Loader::includeModule('nextype.alpha'))
    return;

global $APPLICATION;

if ($templateData['JS_OBJ'] && !empty($templateData['ITEMS']))
{
    ?>
    <script>
    BX.ready(BX.defer(function(){
        
        var waiter<?=$templateData['JS_OBJ']?> = setInterval(function () {
            
            if (!!window.<?=$templateData['JS_OBJ']?>)
            {
                clearInterval(waiter<?=$templateData['JS_OBJ']?>);
                
                <?
                foreach ($templateData['ITEMS'] as $item)
                {
                    if (isset($item['PROPERTIES']['LIKES_IP']['VALUE']['TEXT']) && strpos($item['PROPERTIES']['LIKES_IP']['VALUE']['TEXT'], Reviews\Base::getRealIp()) !== false)
                    {
                        ?> window.<?=$templateData['JS_OBJ']?>.setDisabledLikeDislike('<?=$item['ID']?>'); <?
                    }
                }
                ?>
            }
            
        }, 500);
    }));
    </script>
    <?
}

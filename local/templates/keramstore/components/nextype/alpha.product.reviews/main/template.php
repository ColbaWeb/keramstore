<? 
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$mainId = $this->GetEditAreaId($arResult['PRODUCT']['ID']);
$itemIds = array(
    'ID' => $mainId,
    'CONTAINER_LIST_ID' => $mainId.'_container_list',
    'CONTAINER_FORM_ID' => $mainId.'_container_form',
);

$templateData = array (
    'ITEMS' => $arResult['ITEMS']
);
$obName = $templateData['JS_OBJ'] = 'obProductReviews' . $arResult['PRODUCT']['ID'];
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
?>

<div class="modal-container hidden" id="<?=$itemIds['CONTAINER_FORM_ID']?>">
    <div class="modal-content-container">
        <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
            <i class="icon icon-close-big"></i>
        </a>
        <div class="scrollbar-inner">
            <div class="content-wrapper">
                <div class="rating-modal">
                    <div class="h2 reviews-title"><?=GetMessage('CP_TPL_REVIEW_FORM_TITLE')?></div>
                    <div class="reviews-send-text">
                        <?=GetMessage('CP_TPL_REVIEW_FORM_DESCRIPTION')?>
                    </div>

                    <form action="" method="post">
                        <?
                        if ($arResult['IS_AJAX'] && $arResult['IS_AJAX'] == "Y")
                            $APPLICATION->RestartBuffer();
                        
                        if (isset($arResult['COMPLETED']) && $arResult['COMPLETED'] == "Y")
                        {
                            ?><div class="alert alert-success"><?=GetMessage('CP_TPL_REVIEW_FORM_COMPLETED')?></div><?
                        }
                        else
                        {
                        
                            if (is_array($arResult['ERRORS']) && !empty($arResult['ERRORS']))
                            {
                                $arDisplayErrors = array ();
                                foreach ($arResult['ERRORS'] as $key => $value)
                                {
                                    if (!is_string($key))
                                        $arDisplayErrors[] = $value;
                                }

                                if (!empty($arDisplayErrors))
                                {
                                    ?>
                                    <div class="alert alert-danger"><?=implode("<br>", $arDisplayErrors)?></div>
                                    <?
                                }
                            }
                            ?>


                            <div class="review-count-label font-title"><?=GetMessage('CP_TPL_REVIEW_FORM_FIELD_RATING')?>*</div>
                            <div class="rating custom-radio-star-select" data-entity="vote-stars">
                                <?
                                for ($i=1; $i <= 5; $i++)
                                {
                                    ?><div class="custom-radio-star"><i data-entity="star" data-value="<?=$i?>" class="icon icon-star-32 <?=(!isset($arResult['VALUES']['VOTE']) || (isset($arResult['VALUES']['VOTE']) && intval($arResult['VALUES']['VOTE']) < $i)) ? 'empty' : ''?>"></i></div><?
                                }
                                ?>
                                <input type="hidden" data-entity="vote-stars-input" name="VOTE" value="<?=isset($arResult['VALUES']['VOTE']) ? $arResult['VALUES']['VOTE'] : '0'?>" />
                             </div>
                            <div class="row mb-4">
                                <div class="col">
                                    <div class="custom-input d-flex<?=isset($arResult['ERRORS']['USER_NAME']) ? ' error' : ''?>">
                                        <input type="text" value="<?=isset($arResult['VALUES']['USER_NAME']) ? $arResult['VALUES']['USER_NAME'] : ''?>" name="USER_NAME" id="review-field-user-name">
                                        <label for="review-field-user-name"><?=GetMessage('CP_TPL_REVIEW_FORM_FIELD_NAME')?>*</label>
                                    </div>
                                    <?
                                    if (isset($arResult['ERRORS']['USER_NAME']))
                                    {
                                        ?><div class="error-message"><?=$arResult['ERRORS']['USER_NAME']?></div><?
                                    }
                                    ?>

                                </div>
                                <div class="col">
                                    <div class="custom-input d-flex<?=isset($arResult['ERRORS']['USER_EMAIL']) ? ' error' : ''?>">
                                        <input type="text" value="<?=isset($arResult['VALUES']['USER_EMAIL']) ? $arResult['VALUES']['USER_EMAIL'] : ''?>" name="USER_EMAIL" id="review-field-user-email">
                                        <label for="review-field-user-email"><?=GetMessage('CP_TPL_REVIEW_FORM_FIELD_EMAIL')?>*</label>
                                    </div>
                                    <?
                                    if (isset($arResult['ERRORS']['USER_EMAIL']))
                                    {
                                        ?><div class="error-message"><?=$arResult['ERRORS']['USER_EMAIL']?></div><?
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="custom-input textarea<?=isset($arResult['ERRORS']['ADVANTAGES']) ? ' error mb-1' : ''?>">
                                <textarea name="ADVANTAGES" id="review-field-advantages" rows="4"><?=isset($arResult['VALUES']['ADVANTAGES']) ? $arResult['VALUES']['ADVANTAGES'] : ''?></textarea>
                                <label for="review-field-advantages"><?=GetMessage('CP_TPL_REVIEW_FORM_FIELD_ADVANTAGES')?>*</label>
                            </div>
                            <?
                            if (isset($arResult['ERRORS']['ADVANTAGES']))
                            {
                                ?><div class="error-message mb-4"><?=$arResult['ERRORS']['ADVANTAGES']?></div><?

                            }?>

                            <div class="custom-input textarea<?=isset($arResult['ERRORS']['DISADVANTAGES']) ? ' error mb-1' : ''?>">
                                <textarea name="DISADVANTAGES" id="review-field-disadvantages" rows="4"><?=isset($arResult['VALUES']['DISADVANTAGES']) ? $arResult['VALUES']['DISADVANTAGES'] : ''?></textarea>
                                <label for="review-field-disadvantages"><?=GetMessage('CP_TPL_REVIEW_FORM_FIELD_DISADVANTAGES')?>*</label>
                            </div>
                            <?
                            if (isset($arResult['ERRORS']['DISADVANTAGES']))
                            {
                                ?><div class="error-message mb-4"><?=$arResult['ERRORS']['DISADVANTAGES']?></div><?

                            }?>

                            <div class="custom-input textarea<?=isset($arResult['ERRORS']['COMMENT']) ? ' error mb-1' : ''?>">
                                <textarea name="COMMENT" id="review-field-comment" rows="4"><?=isset($arResult['VALUES']['COMMENT']) ? $arResult['VALUES']['COMMENT'] : ''?></textarea>
                                <label for="review-field-comment"><?=GetMessage('CP_TPL_REVIEW_FORM_FIELD_COMMENT')?>*</label>
                            </div>
                            <?
                            if (isset($arResult['ERRORS']['COMMENT']))
                            {
                                ?><div class="error-message mb-4"><?=$arResult['ERRORS']['COMMENT']?></div><?

                            }?>

                                <div class="custom-input-wrap captcha mb-4">
                                    <div class="custom-input">
                                        <input name="captcha_code" value="<?=htmlspecialchars($arResult['FORM_CAPTCHA_CODE']);?>" type="hidden" />
                                        <input name="captcha_word" type="text" id="review-field-captcha">
                                        <label for="review-field-captcha"><?=GetMessage('CP_TPL_REVIEW_FORM_CAPTCHA_LABEL')?></label>
                                        <img src="/bitrix/tools/captcha.php?captcha_code=<?=htmlspecialchars($arResult['FORM_CAPTCHA_CODE']);?>">

                                    </div>
                                </div>

                            <div class="buttons">
                                <a href="javascript:void(0)" class="back mobile"><i class="icon icon-arrow-left"></i><?=GetMessage('CP_TPL_REVIEW_FORM_BACK_BUTTON')?></a>
                                <button class="btn btn-primary btn-large"><?=GetMessage('CP_TPL_REVIEW_FORM_SUBMIT_BUTTON')?></button>
                            </div>
                        <?
                        }
                        
                        if ($arResult['IS_AJAX'] && $arResult['IS_AJAX'] == "Y")
                            return;
                        ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="close-area"></div>
</div>

<div class="modal-container<?=$arParams['OPENNED'] != "Y" ? ' hidden' : ''?>" id="<?=$itemIds['CONTAINER_LIST_ID']?>">
    <div class="modal-content-container">
        <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
            <i class="icon icon-close-big"></i>
        </a>
        <div class="scrollbar-inner">
            <div class="content-wrapper">
                <div class="rating-modal">
                    <div class="rating-circle-block">
                    <?
                    $circleClass = "empty";
                    if ($arResult['AVG_RATING'] > 0)
                    {
                        if ($arResult['AVG_RATING'] <= 0.25)
                            $circleClass = "small";
                        elseif ($arResult['AVG_RATING'] > 0.25 && $arResult['AVG_RATING'] <= 3.5)
                            $circleClass = "medium";
                        elseif ($arResult['AVG_RATING'] > 3.5 && $arResult['AVG_RATING'] <= 4.5)
                            $circleClass = "large";
                        else
                            $circleClass = "full";
                    }
                    ?>
                        <div class="circle h3 <?=$circleClass?>">
                            <?=(isset($arResult['AVG_RATING']) && $arResult['AVG_RATING'] > 0) ? number_format($arResult['AVG_RATING'], 1, ",", " ") : "0" ?>
                        </div>
                        <div class="title-block">
                            <div class="rating-title font-title"><?=GetMessage('CP_TPL_REVIEWS_AVG_RATING_TITLE')?></div>
                            <a href="javascript:void(0)" data-entity="button-show-form" class="chars-link dotted-link font-large-button"><span><?=GetMessage('CP_TPL_REVIEWS_MAKE_REVIEW_BUTTON')?></span><i class="icon icon-arrow-right-text-button"></i></a>
                        </div>
                    </div>
                    
                    <?
                    if (!empty($arResult['ITEMS']))
                    {
                        ?>
                        <div class="h2 reviews-title"><?=GetMessage('CP_TPL_REVIEWS_LIST_TITLE', array ('#VALUE#' => $arResult['PRODUCT']['NAME']))?></div>
                        <div class="list" data-entity="list">
                            <?
                            foreach ($arResult['ITEMS'] as $arItem)
                            {
                                ?>
                                <div class="review" itemscope itemtype="https://schema.org/Review" data-entity="item" data-entity-id="<?=$arItem['ID']?>" data-has-liked="false">
                                    <span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Product">
                                        <meta itemprop="name" content="<?=$arResult['PRODUCT']['NAME']?>"/>
                                        <meta itemprop="url" content="<?=$arResult['PRODUCT']['DETAIL_PAGE_URL']?>"/>
                                        <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                            <meta itemprop="price" content="<?=$arResult['PRODUCT']['RESULT_PRICE']['DISCOUNT_PRICE']?>" />
                                            <meta itemprop="priceCurrency" content="<?=$arResult['PRODUCT']['RESULT_PRICE']['CURRENCY']?>" />
                                        </span>
                                        
                                        
                                    </span>
                                    <div class="name-wrapper" itemprop="author" itemscope itemtype="https://schema.org/Person">
                                        <?
                                        if (!empty($arItem['PROPERTIES']['USER_NAME']['VALUE']))
                                        {
                                            ?><div class="name" itemprop="name"><?=$arItem['PROPERTIES']['USER_NAME']['VALUE']?></div><?
                                        }
                                        ?>

                                        <div class="rating">
                                            <?
                                            for ($vote = 1; $vote <= 5; $vote++)
                                            {
                                                ?><div class="icon <?=intval($arItem['PROPERTIES']['VOTE']['VALUE']) >= $vote ? 'icon-star' : 'icon-star-empty'?>"></div><?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    
                                    <?
                                    if (!empty($arItem['PROPERTIES']['COMMENT']['VALUE']['TEXT']))
                                    {
                                        ?>
                                        <div class="subtitle font-body-small"><?=GetMessage('CP_TPL_REVIEW_ITEM_COMMENT')?></div>
                                        <div class="text font-body" itemprop="reviewBody"><?=$arItem['PROPERTIES']['COMMENT']['VALUE']['TEXT']?></div>
                                        <?
                                    }
                                    
                                    if (!empty($arItem['PROPERTIES']['ADVANTAGES']['VALUE']['TEXT']))
                                    {
                                        ?>
                                        <div class="subtitle font-body-small"><?=GetMessage('CP_TPL_REVIEW_ITEM_ADVANTAGES')?></div>
                                        <div class="text font-body"><?=$arItem['PROPERTIES']['ADVANTAGES']['VALUE']['TEXT']?></div>
                                        <?
                                    }
                                    
                                    if (!empty($arItem['PROPERTIES']['DISADVANTAGES']['VALUE']['TEXT']))
                                    {
                                        ?>
                                        <div class="subtitle font-body-small"><?=GetMessage('CP_TPL_REVIEW_ITEM_DISADVANTAGES')?></div>
                                        <div class="text font-body"><?=$arItem['PROPERTIES']['DISADVANTAGES']['VALUE']['TEXT']?></div>
                                        <?
                                    }
                                    ?>
                                    
                                    <div class="info">
                                        <div class="votes-block">
                                            <?
                                            $diffLikesDislikes = intval($arItem['PROPERTIES']['LIKES']['VALUE']) - intval($arItem['PROPERTIES']['DISLIKES']['VALUE']);
                                            $containerClass = "";
                                            if ($diffLikesDislikes > 0)
                                                $containerClass = "positive";
                                            elseif ($diffLikesDislikes < 0)
                                                $containerClass = "negative";
                                            ?>
                                            
                                            <a href="javascript:void(0)" data-entity="btn-like" class="like"><i class="icon icon-thumbs-up"></i></a>
                                            <span class="value font-body-2 <?=$containerClass?>" data-entity="diff-likes-dislikes">
                                                <?
                                                if ($diffLikesDislikes > 0)
                                                {
                                                    echo "+" . $diffLikesDislikes;
                                                }
                                                elseif ($diffLikesDislikes < 0)
                                                {
                                                    echo $diffLikesDislikes;
                                                }
                                                else
                                                {
                                                    echo "0";
                                                }
                                                ?>
                                            </span>
                                            <a href="javascript:void(0)" data-entity="btn-dislike" class="dislike"><i class="icon icon-thumbs-down"></i></a>
                                        </div>
                                        <?
                                        if (!empty($arItem['ACTIVE_FROM']))
                                        {
                                            ?>
                                            <div class="date font-body-small"><?=$arItem['ACTIVE_FROM']?></div>
                                            <meta itemprop="datePublished" content="<?=date("Y-m-d", strtotime($arItem['ACTIVE_FROM']))?>"/>
                                            <?
                                        }
                                        ?>
                                    </div>
                                        <span itemprop="reviewRating" itemscope itemtype="https://schema.org/Rating">
                                            <meta itemprop="worstRating" content="1">
                                            <meta itemprop="ratingValue" content="<?=intval($arItem['PROPERTIES']['VOTE']['VALUE'])?>">
                                            <meta itemprop="bestRating" content="5"/>
                                        </span>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                        <?
                    }
                    ?>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="close-area"></div>
</div>
<?
$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'nextype.alpha.product.reviews');
?>
<script>
        BX.ready(function () {
            BX.loadScript('<?=$templateFolder?>/script.js', function () {
                window.<?=$obName?> = new window.Alpha.ProductReviews(<?=CUtil::PhpToJSObject(array (
                    'SITE_ID' => SITE_ID,
                    'TEMPLATE_NAME' => CUtil::JSEscape($templateName),
                    'AJAX_URL' => $templateFolder . "/ajax.php",
                    'SIGNED_PARAMS' => CUtil::JSEscape($signedParams),
                    'VISUAL' => $itemIds,
                    'MESSAGES' => array (
                        'ALREADY_VOTED_TITLE' => GetMessage('CP_TPL_REVIEW_ALREADY_VOTED_TITLE'),
                        'ALREADY_VOTED_MESSAGE' => GetMessage('CP_TPL_REVIEW_ALREADY_VOTED_MESSAGE'),
                     )
                ), false, true)?>);
            });
        });
</script>
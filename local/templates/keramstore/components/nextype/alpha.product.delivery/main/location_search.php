<?php
use Bitrix\Main\Text\Encoding;
use Bitrix\Main;
use Bitrix\Main\Loader;

define('DISABLED_REGIONS', true);
define('STOP_STATISTICS', true);

if (!isset($_REQUEST['siteId']) || !is_string($_REQUEST['siteId']))
    die();

if ($_SERVER['REQUEST_METHOD'] != 'POST' || preg_match('/^[A-Za-z0-9_]{2}$/', $_POST['siteId']) !== 1)
    die;

define('SITE_ID', $_REQUEST['siteId']);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/type/dictionary.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/errorcollection.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/text/encoding.php');

if (isset($_REQUEST['charset']) && strtolower($_REQUEST['charset']) != "utf-8")
{
    $_POST = Encoding::convertEncoding($_POST, 'UTF-8', $_REQUEST['charset']);
    $_GET = Encoding::convertEncoding($_GET, 'UTF-8', $_REQUEST['charset']);
    $_REQUEST = Encoding::convertEncoding($_REQUEST, 'UTF-8', $_REQUEST['charset']);
    $_COOKIE = Encoding::convertEncoding($_COOKIE, 'UTF-8', $_REQUEST['charset']);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

CBitrixComponent::includeComponentClass("bitrix:sale.location.selector.search");

$result = true;
$errors = array();
$data = array();

try
{
	CUtil::JSPostUnescape();

	$request = Main\Context::getCurrent()->getRequest()->getPostList();
	if($request['version'] == '2')
		$data = CBitrixLocationSelectorSearchComponent::processSearchRequestV2($_REQUEST, true);
	else
		$data = CBitrixLocationSelectorSearchComponent::processSearchRequest();
}
catch(Main\SystemException $e)
{
	$result = false;
	$errors[] = $e->getMessage();
}

if (!empty($data['ITEMS']))
{
    foreach ($data['ITEMS'] as $key => $arItem)
    {
        if (!empty($arItem['CODE']))
        {
            $data['ITEMS'][$key]['DISPLAY'] = \Bitrix\Sale\Location\Admin\LocationHelper::getLocationPathDisplay($arItem['CODE']);
            
            if ($arItem['TYPE_ID'] == 3) // is region
            {
                $data['ITEMS'][$key]['CODE'] = "R" . $arItem['CODE'];
            }
        }
    }
}

header('Content-Type: application/json');
echo \Bitrix\Main\Web\Json::encode(array(
	'result' => $result,
	'errors' => $errors,
	'data' => $data
));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
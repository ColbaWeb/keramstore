(function(window){
	'use strict';

	if (window.Alpha.ProductDelivery)
		return;

	window.Alpha.ProductDelivery = function(arParams)
	{
            this.obContainer = null;
            this.obQuantity = null;
            this.obQuantityInput = null;
            this.obLocationSelector = null;
            this.obLocationSelectorInput = null;
            this.obLocationSelectorItems = null;
            this.siteId = null;
            this.locationSelectorCode = null;
            this.includeBasket = !!arParams.INCLUDE_BASKET && arParams.INCLUDE_BASKET === true;
            this.product = {};
            this.templateName = null;
            this.messages = {};
            this.precisionFactor = Math.pow(10, 6);
            this.stepQuantity = 1;
            this.timer = 0;
            
            this.ajaxUrl = arParams.AJAX_URL;
            this.locationAjaxUrl = arParams.LOCATION_AJAX_URL;
            this.siteId = arParams.SITE_ID;
            this.templateName = arParams.TEMPLATE_NAME;
            this.signedParamsString = arParams.SIGNED_PARAMS;
            this.product = arParams.PRODUCT;
            
            if (!!this.product)
            {
                this.stepQuantity = parseFloat(this.product.RATIO);
            }
            
            if (!!arParams.VISUAL.CONTAINER_ID)
            {
                this.obContainer = BX(arParams.VISUAL.CONTAINER_ID);
                
                if (!!this.obContainer)
                {
                    BX.ready(BX.delegate(this.init, this));
                }

            }
        };
        
        window.Alpha.ProductDelivery.prototype = {
            
            init: function () {
               
                this.initQuantity();
                this.initLocationSelector();
                this.initIncludeBasket();
            },
            
            initIncludeBasket: function () {
                var checkbox = this.obContainer.querySelector('[data-entity="checkbox-include-basket"]');
                
                if (!!checkbox)
                {
                    BX.bind(checkbox, 'change', BX.delegate(function () {
                        var targetItem = BX.proxy_context;
                        this.includeBasket = targetItem.checked;
                        
                        this.sendRefreshRequest();
                        
                    }, this));
                }
            },
            
            initQuantity: function () {
                
                this.obQuantity = this.obContainer.querySelector('[data-entity="quantity-block"]');
                
                if (!!this.obQuantity)
                {
                    this.obQuantityInput = this.obQuantity.querySelector('input');
                    
                    var obDown = this.obQuantity.querySelector('[data-entity="quantity-down"]'),
                        obUp = this.obQuantity.querySelector('[data-entity="quantity-up"]');
                
                    BX.bind(obDown, 'click', BX.delegate(function () {
                        if (parseFloat(this.obQuantityInput.value) - this.stepQuantity > 0)
                            this.obQuantityInput.value = parseFloat(this.obQuantityInput.value) - this.stepQuantity;
                        else
                            this.obQuantityInput.value = this.stepQuantity;
                        
                        this.sendRefreshRequest();
                        
                    }, this));
                    
                    BX.bind(obUp, 'click', BX.delegate(function () {
                        
                        this.obQuantityInput.value = parseFloat(this.obQuantityInput.value) + this.stepQuantity;
                        this.sendRefreshRequest();
                        
                    }, this));
                    
                    BX.bind(this.obQuantityInput, 'change', BX.delegate(function () {
                        
                        var curValue = parseFloat(this.obQuantityInput.value);
                        var intCount = Math.floor(Math.round(curValue * this.precisionFactor / this.stepQuantity) / this.precisionFactor) || 1;
                        
                        curValue = (intCount <= 1 ? this.stepQuantity : intCount * this.stepQuantity);
                        curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
                        
                        this.obQuantityInput.value = curValue;
                        
                        this.sendRefreshRequest();
                        
                    }, this));
                }
                
            },
            
            initLocationSelector: function () {
                
                this.obLocationSelector = this.obContainer.querySelector('[data-entity="location-block"]');
                
                if (!!this.obLocationSelector)
                {
                    this.obLocationSelectorInput = this.obLocationSelector.querySelector('input');
                    this.obLocationSelectorItems = this.obLocationSelector.querySelector('[data-entity="items"]');

                    BX.bind(this.obLocationSelectorInput, 'keyup', BX.delegate(function () {
                        
                        var target = BX.proxy_context;
                        clearTimeout(this.timer);
                        this.timer = setTimeout(BX.delegate(function () {
                            if (target.value.toString().length >= 2)
                            {
                                var obData = {
                                    select: {1: 'CODE', 2: 'TYPE_ID', VALUE: 'ID', DISPLAY: 'NAME.NAME'},
                                    additionals: {1: 'PATH'},
                                    filter: {'=PHRASE': target.value, '=NAME.LANGUAGE_ID': 'ru'},
                                    siteId: this.siteId,
                                    version: 2, PAGE_SIZE: 10, PAGE: 0
                                };
                                
                                BX.ajax({
                                    url: this.locationAjaxUrl,
                                    method: 'POST',
                                    dataType: 'json',
                                    data: obData,
                                    timeout: 5,
                                    onsuccess: BX.delegate(function (result) {
                                        if (!result || typeof(result) != 'object' || !result.data)
                                        {
                                            BX.addClass(this.obLocationSelectorItems, 'd-none');
                                            return;
                                        }
                                        
                                        if (result.data.ITEMS.length == 0)
                                        {
                                            BX.cleanNode(this.obLocationSelectorItems);
                                            BX.addClass(this.obLocationSelectorItems, 'd-none');
                                            return;
                                        }
                                        else
                                        {
                                            BX.cleanNode(this.obLocationSelectorItems);
                                            for (var i = 0; i < result.data.ITEMS.length; i++)
                                            {
                                                BX(this.obLocationSelectorItems).append(BX.create('a', {
                                                    props: {className: 'autocomplete-item'},
                                                    dataset: {code: result.data.ITEMS[i].CODE, label: result.data.ITEMS[i].DISPLAY},
                                                    attrs: {href: 'javascript:void(0)'},
                                                    text: result.data.ITEMS[i].DISPLAY,
                                                    events: {
                                                        click: BX.delegate(function () {
                                                            var targetItem = BX.proxy_context;
                                                            this.locationSelectorCode = targetItem.getAttribute('data-code');
                                                            
                                                            BX.adjust(this.obLocationSelectorInput, {
                                                                props: {
                                                                    placeholder: targetItem.getAttribute('data-label'),
                                                                    value: ''
                                                                }
                                                            });
                                                            
                                                            BX.cleanNode(this.obLocationSelectorItems);
                                                            BX.addClass(this.obLocationSelectorItems, 'd-none');
                                                            
                                                            this.sendRefreshRequest();
                                                        }, this)
                                                    }
                                                }));

                                            }
                                            
                                            BX.removeClass(this.obLocationSelectorItems, 'd-none');
                                        }
                                        
                                    }, this)
                                });
                                
                                return;
                            }
                            
                            BX.addClass(this.obLocationSelectorItems, 'd-none');
                            BX.cleanNode(this.obLocationSelectorItems);
                            
                        }, this), 300);
                        
                    }, this));
                    
                    BX.bind(this.obLocationSelectorInput, 'focusin', BX.delegate(function () {
                        if (this.obLocationSelectorItems.innerHTML != '')
                            BX.removeClass(this.obLocationSelectorItems, 'd-none');
                    }, this));
                    
                    /*BX.bind(this.obLocationSelectorInput, 'focusout', BX.delegate(function () {
                        BX.addClass(this.obLocationSelectorItems, 'd-none');
                    }, this));*/
                }
            },
            
            openModal: function () {
                if (!!this.obContainer)
                {
                    BX.removeClass(this.obContainer, 'hidden');
                    $('body').trigger('modalOpen');
                    BX.addClass(document.body, 'modal-opened');
                }
            },
            
            sendRefreshRequest: function (callback)
            {
                callback = (callback) || null;
                
                var data = {
                    sessid: BX.bitrix_sessid(),
                    siteId: this.siteId,
                    templateName: this.templateName,
                    signedParamsString: this.signedParamsString,
                    charset: BX.message('LANG_CHARSET'),
                    productId: this.product.ID,
                    quantity: parseFloat(this.obQuantityInput.value),
                    locationCode: this.locationSelectorCode,
                    includeBasket: this.includeBasket ? 'Y' : 'N'
                };

                BX.style(this.obContainer.querySelector('[data-entity="quantity-block"] .plus'), 'pointer-events', 'none');
                BX.style(this.obContainer.querySelector('[data-entity="quantity-block"] .minus'), 'pointer-events', 'none');
                BX.style(this.obContainer.querySelector('.delivery-preloader-container'), 'display', 'block');

                BX.ajax({
                    url: this.ajaxUrl,
                    method: 'POST',
                    dataType: 'html',
                    data: data,
                    timeout: 30,
                    emulateOnload: true,
                    onsuccess: BX.delegate(function (result) {
                        if (!result)
                            return;

                        BX.style(this.obContainer.querySelector('[data-entity="quantity-block"] .plus'), 'pointer-events', 'unset');
                        BX.style(this.obContainer.querySelector('[data-entity="quantity-block"] .minus'), 'pointer-events', 'unset');

                        var ob = BX.processHTML(result);

                        this.obContainer.querySelector('[data-entity="ajax-container"]').innerHTML = ob.HTML;

                        BX.ajax.processScripts(ob.SCRIPT, true);

                        if (typeof(callback) == 'function')
                                callback.call(this);

                        this.init();

                    }, this)
                });

            }
        }
        
})(window);
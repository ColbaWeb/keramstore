<?php
use Bitrix\Main\Text\Encoding;

define('DISABLED_REGIONS', true);
define('STOP_STATISTICS', true);

if (!isset($_REQUEST['siteId']) || !is_string($_REQUEST['siteId']))
    die();

if (!isset($_REQUEST['templateName']) || !is_string($_REQUEST['templateName']))
    die();

if ($_SERVER['REQUEST_METHOD'] != 'POST' ||
    preg_match('/^[A-Za-z0-9_]{2}$/', $_POST['siteId']) !== 1 ||
    preg_match('/^[.A-Za-z0-9_-]+$/', $_POST['templateName']) !== 1)
    die;

define('SITE_ID', $_REQUEST['siteId']);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/type/dictionary.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/errorcollection.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/text/encoding.php');

if (isset($_REQUEST['charset']) && strtolower($_REQUEST['charset']) != "utf-8")
{
    $_POST = Encoding::convertEncoding($_POST, 'UTF-8', $_REQUEST['charset']);
    $_GET = Encoding::convertEncoding($_GET, 'UTF-8', $_REQUEST['charset']);
    $_REQUEST = Encoding::convertEncoding($_REQUEST, 'UTF-8', $_REQUEST['charset']);
    $_COOKIE = Encoding::convertEncoding($_COOKIE, 'UTF-8', $_REQUEST['charset']);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
    $signedParamsString = $request->get('signedParamsString') ?: '';
    $params = $signer->unsign($signedParamsString, 'nextype.alpha.product.delivery');
    $params = unserialize(base64_decode($params));
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
    die();
}

$params['AJAX_REQUEST'] = 'Y';

if (strlen($request->get('locationCode')) > 0)
    $params['LOCATION_CODE'] = $request->get('locationCode');

if (strlen($request->get('includeBasket')) > 0 && in_array($request->get('includeBasket'), array ('Y', 'N')))
    $params['INCLUDE_BASKET'] = $request->get('includeBasket');

if (floatval($request->get('quantity')) > 0)
    $params['PRODUCT_QUANTITY'] = $request->get('quantity');


$APPLICATION->IncludeComponent('nextype:alpha.product.delivery', $request->get('templateName'), $params);
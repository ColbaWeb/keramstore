<? 
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$mainId = $this->GetEditAreaId($arResult['PRODUCT']['ID']);
$itemIds = array(
    'ID' => $mainId,
    'CONTAINER_ID' => $mainId.'_container',
);

$obName = $templateData['JS_OBJ'] = 'obProductDelivery' . $arResult['PRODUCT']['ID'];
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if (!empty($arResult['MAIN_DELIVERIES_RESULT']))
{
    ?>
    <div class="delivery">
        <div class="delivery-title-block">
            <div class="font-title title"><?=GetMessage('CP_TPL_PRODUCT_DELIVERY_WIDGET_TITLE', array ('#CITY#' => $arResult['LOCATION']['NAME_LANG']))?></div>
        </div>
        <div class="list">
            <?
            foreach ($arResult['MAIN_DELIVERIES_RESULT'] as $arDelivery)
            {
                ?>
                <div class="item font-body-2">
                    <span class="delivery-type">
                        <?
                        echo $arDelivery['NAME'];
                        if (!empty($arDelivery['PERIOD_TEXT']))
                            echo ", ";
                        ?>
                    </span>
                    <?
                    if (!empty($arDelivery['PERIOD_TEXT']))
                    {
                        ?><span class="time"><?=$arDelivery['PERIOD_TEXT']?></span><?
                    }
                    ?>
                    <span> &mdash; </span>
                    <span class="price"><?=!empty($arDelivery['PRICE']) ? $arDelivery['PRICE_FORMATED'] : GetMessage('CP_TPL_PRODUCT_DELIVERY_FREE_PRICE')?></span>
                </div>
                <?
            }
            ?>
            
        </div>
        <? if (!empty($arResult['ALL_DELIVERIES_RESULT'])): ?>
        <a href="javascript:void(0)" onclick="if (!!window.<?=$obName?>) window.<?=$obName?>.openModal();" class="all font-large-button"><span><?=GetMessage('CP_TPL_PRODUCT_DELIVERY_ALL_DELIVERIES_BUTTON')?> </span><i class="icon icon-arrow-right-text-button"></i></a>
        <? endif; ?>
    </div>
    
        <div class="modal-container hidden modal-right" id="<?=$itemIds['CONTAINER_ID']?>">
            <div class="modal-content-container">
                <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon icon-close-big"></i>
                </a>
                <div class="scrollbar-inner">
                    <div class="content-wrapper">
                        
                        <div class="delivery-modal" data-entity="ajax-container">
                            <?
                            if ($arParams['AJAX_REQUEST'] && $arParams['AJAX_REQUEST'] == "Y")
                                $APPLICATION->RestartBuffer();
                            ?>
                            <div class="font-h2 bold"><?=GetMessage('CP_TPL_PRODUCT_DELIVERY_MODAL_TITLE')?></div>

                            <div class="delivery-preloader-container" style="display: none">
                                <div class="swiper-lazy-preloader"></div>
                            </div>

                            <div class="custom-input-wrap" data-entity="location-block">
                                <div class="font-body-2 subtitle"><?=GetMessage('CP_TPL_PRODUCT_DELIVERY_MODAL_CITY_SELECTOR')?></div>
                                <div class="custom-input">
                                    <input type="text" placeholder="<?=$arResult['LOCATION']['DISPLAY_NAME']?>" value="">
                                </div>
                                <div data-entity="items" class="autocomplete-items d-none"></div>
                            </div>
                            
                            <div class="quantity">
                                <div class="counter" data-entity="quantity-block">
                                    <a href="javascript:void(0)" class="minus" data-entity="quantity-down"><i class="icon icon-bx-minus"></i></a>
                                    <input type="number" value="<?=floatval($arParams['PRODUCT_QUANTITY'])?>">
                                    <a href="javascript:void(0)" class="plus" data-entity="quantity-up"><i class="icon icon-bx-plus"></i></a>
                                </div>
                                
                                <div class="custom-checkbox ">
                                    <input <?=$arParams['INCLUDE_BASKET'] == "Y" ? 'checked="checked"' : ''?> type="checkbox" data-entity="checkbox-include-basket" class="custom-control-input" id="product-delivery-modal-include-basket">
                                    <label class="custom-control-label font-body-2" for="product-delivery-modal-include-basket">
                                        <span><?=GetMessage('CP_TPL_PRODUCT_DELIVERY_MODAL_INCLUDE_BASKET')?></span>
                                    </label>
                                </div>
                            </div>

                            <div class="accordions">
                                <?
                                foreach ($arResult['ALL_DELIVERIES_RESULT'] as $arDelivery)
                                {
                                    ?>
                                    <div class="accordion">
                                        <a href="javascript:void(0)" class="accordion-head">
                                            <div class="title-block font-body-2">
                                                <div class="title"><?=$arDelivery['NAME']?></div>
                                            </div>
                                            <div class="price font-body">
                                                <?=!empty($arDelivery['PRICE']) ? $arDelivery['PRICE_FORMATED'] : GetMessage('CP_TPL_PRODUCT_DELIVERY_FREE_PRICE')?>
                                            </div>
                                            <i class="icon icon-bx-down-arrow-alt"></i>
                                        </a>
                                        <div class="accordion-content">
                                            <div class="text font-body-2">
                                                <?
                                                if (!empty($arDelivery['PERIOD_TEXT']))
                                                {
                                                    echo GetMessage('CP_TPL_PRODUCT_DELIVERY_MODAL_PERIOD_LABEL') . $arDelivery['PERIOD_TEXT'] . "<br>";
                                                }
                                                
                                                echo $arDelivery['DESCRIPTION'];
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                }
                                ?>
                                

                            </div>
                            <?
                            if ($arParams['AJAX_REQUEST'] && $arParams['AJAX_REQUEST'] == "Y")
                                return;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="close-area"></div>
        </div>

    <?
    $signer = new \Bitrix\Main\Security\Sign\Signer;
    $signedParams = $signer->sign(base64_encode(serialize($arParams)), 'nextype.alpha.product.delivery');
    ?>
    <script>
            BX.ready(function () {
                BX.loadScript('<?=$templateFolder?>/script.js', function () {
                    window.<?=$obName?> = new window.Alpha.ProductDelivery(<?=CUtil::PhpToJSObject(array (
                        'SITE_ID' => SITE_ID,
                        'TEMPLATE_NAME' => CUtil::JSEscape($templateName),
                        'AJAX_URL' => $templateFolder . "/ajax.php",
                        'LOCATION_AJAX_URL' => $templateFolder . "/location_search.php",
                        'SIGNED_PARAMS' => CUtil::JSEscape($signedParams),
                        'INCLUDE_BASKET' => $arParams['INCLUDE_BASKET'] == "Y",
                        'VISUAL' => $itemIds,
                        'PRODUCT' => array (
                            'ID' => $arResult['PRODUCT']['ID'],
                            'NAME' => $arResult['PRODUCT']['NAME'],
                            'RATIO' => floatval($arResult['PRODUCT']['RATIO']) > 0 ? floatval($arResult['PRODUCT']['RATIO']) : 1,
                            'MEASURE_TITLE' => isset($arResult['PRODUCT']['MEASURE']['SYMBOL']) ? $arResult['PRODUCT']['MEASURE']['SYMBOL'] : ''
                        )
                    ), false, true)?>);
                });
            });
    </script>
    <?
}
?>
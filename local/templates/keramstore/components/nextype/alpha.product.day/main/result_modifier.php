<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

if (isset($arResult['ITEMS'][0]))
{
    $arResult['ITEM'] = $arResult['ITEMS'][0];
    unset($arResult['ITEMS']);
}

if ($arParams['SECTION_LINK_TYPE'] == 'C' && !empty($arParams['SECTION_LINK_TITLE']) && !empty($arParams['SECTION_LINK_URL']))
{
    $arResult['SECTION_LINK_TITLE'] = $arParams['SECTION_LINK_TITLE'];
    $arResult['SECTION_LINK_URL'] = $arParams['SECTION_LINK_URL'];
}
elseif ($arParams['SECTION_LINK_TYPE'] == 'S' && !empty($arParams['SECTION_LINK_SECTION_ID']))
{
    if ($arSection = CIBlockSection::GetByID($arParams['SECTION_LINK_SECTION_ID'])->GetNext())
    {
        $arResult['SECTION_LINK_TITLE'] = $arSection['NAME'];
        $arResult['SECTION_LINK_URL'] = $arSection['SECTION_PAGE_URL'];
    }
    
}

<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

if (!Loader::includeModule('nextype.alpha'))
    return;

$this->setFrameMode(true);


$showTopPager = false;
$showBottomPager = false;
$showLazyLoad = false;


$templateLibrary = array();
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
);
unset($currencyList, $templateLibrary);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$arParams['~MESS_BTN_BUY'] = $arParams['~MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_BUY');
$arParams['~MESS_BTN_DETAIL'] = $arParams['~MESS_BTN_DETAIL'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_DETAIL');
$arParams['~MESS_BTN_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arParams['~MESS_BTN_SUBSCRIBE'] = $arParams['~MESS_BTN_SUBSCRIBE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_SUBSCRIBE');
$arParams['~MESS_BTN_ADD_TO_BASKET'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arParams['~MESS_NOT_AVAILABLE'] = $arParams['~MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE');
$arParams['~MESS_SHOW_MAX_QUANTITY'] = $arParams['~MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCS_CATALOG_SHOW_MAX_QUANTITY');
$arParams['~MESS_RELATIVE_QUANTITY_MANY'] = $arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['~MESS_RELATIVE_QUANTITY_FEW'] = $arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_FEW');

$arParams['MESS_BTN_LAZY_LOAD'] = $arParams['MESS_BTN_LAZY_LOAD'] ?: Loc::getMessage('CT_BCS_CATALOG_MESS_BTN_LAZY_LOAD');


if (isset($arResult['ITEM']['DETAIL_PAGE_URL']) && !empty($arResult['ITEM']['DETAIL_PAGE_URL']))
    $arResult['~SECTION_PAGE_URL'] = $arResult['ITEM']['DETAIL_PAGE_URL'];

$generalParams = array(
	'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
	'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
	'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
	'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
	'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
	'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
	'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
	'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
	'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
	'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
	'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
	'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'],
	'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
	'COMPARE_PATH' => $arParams['COMPARE_PATH'],
	'COMPARE_NAME' => $arParams['COMPARE_NAME'],
        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
	'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
	'LABEL_POSITION_CLASS' => $labelPositionClass,
	'DISCOUNT_POSITION_CLASS' => $discountPositionClass,
	'~BASKET_URL' => $arParams['~BASKET_URL'],
	'~ADD_URL_TEMPLATE' => $arResult['~SECTION_PAGE_URL'] . (strpos($arResult['~SECTION_PAGE_URL'], "?") !== false ? "&" . substr($arResult['~ADD_URL_TEMPLATE'], strpos($arResult['~ADD_URL_TEMPLATE'], "?") + 1) : substr($arResult['~ADD_URL_TEMPLATE'], strpos($arResult['~ADD_URL_TEMPLATE'], "?"))),
	'~BUY_URL_TEMPLATE' => $arResult['~SECTION_PAGE_URL'] . (strpos($arResult['~SECTION_PAGE_URL'], "?") !== false ? "&" . substr($arResult['~BUY_URL_TEMPLATE'], strpos($arResult['~BUY_URL_TEMPLATE'], "?") + 1) : substr($arResult['~BUY_URL_TEMPLATE'], strpos($arResult['~BUY_URL_TEMPLATE'], "?"))),
	'~COMPARE_URL_TEMPLATE' => $arResult['~SECTION_PAGE_URL'] . (strpos($arResult['~SECTION_PAGE_URL'], "?") !== false ? "&" . substr($arResult['~COMPARE_URL_TEMPLATE'], strpos($arResult['~COMPARE_URL_TEMPLATE'], "?") + 1) : substr($arResult['~COMPARE_URL_TEMPLATE'], strpos($arResult['~COMPARE_URL_TEMPLATE'], "?"))),
	'~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~SECTION_PAGE_URL'] . (strpos($arResult['~SECTION_PAGE_URL'], "?") !== false ? "&" . substr($arResult['~COMPARE_DELETE_URL_TEMPLATE'], strpos($arResult['~COMPARE_DELETE_URL_TEMPLATE'], "?") + 1) : substr($arResult['~COMPARE_DELETE_URL_TEMPLATE'], strpos($arResult['~COMPARE_DELETE_URL_TEMPLATE'], "?"))),
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY'],
	'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
	'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
	'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],
	'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
	'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
	'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE'],
        'PRODUCT_CODE_PROP' => $arParams['PRODUCT_CODE_PROP'],
        'OFFER_CODE_PROP' => $arParams['OFFER_CODE_PROP'],
        'SECTION_TITLE' => $arParams['SECTION_TITLE'],
        'SECTION_LINK_TITLE' => $arResult['SECTION_LINK_TITLE'],
        'SECTION_LINK_URL' => $arResult['SECTION_LINK_URL'],
);

if (!empty($arResult['ITEM']))
{
        ?>
        <script>
        BX.ready(function () {
            BX.message({
                                    BTN_MESSAGE_BASKET_REDIRECT: '<?= GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
                                    BASKET_URL: '<?= $arParams['BASKET_URL'] ?>',
                                    ADD_TO_BASKET_OK: '<?= GetMessageJS('ADD_TO_BASKET_OK') ?>',
                                    TITLE_ERROR: '<?= GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
                                    TITLE_BASKET_PROPS: '<?= GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
                                    TITLE_SUCCESSFUL: '<?= GetMessageJS('ADD_TO_BASKET_OK') ?>',
                                    BASKET_UNKNOWN_ERROR: '<?= GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
                                    BTN_MESSAGE_SEND_PROPS: '<?= GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS') ?>',
                                    BTN_MESSAGE_CLOSE: '<?= GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
                                    BTN_MESSAGE_CLOSE_POPUP: '<?= GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP') ?>',
                                    COMPARE_MESSAGE_TITLE: '<?= GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
                                    COMPARE_MESSAGE_OK: '<?= GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
                                    COMPARE_UNKNOWN_ERROR: '<?= GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
                                    COMPARE_TITLE: '<?= GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
                                    PRICE_TOTAL_PREFIX: '<?= GetMessageJS('CT_BCS_CATALOG_PRICE_TOTAL_PREFIX') ?>',
                                    RELATIVE_QUANTITY_MANY: '<?= CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY']) ?>',
                                    RELATIVE_QUANTITY_FEW: '<?= CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW']) ?>',
                                    BTN_MESSAGE_COMPARE_REDIRECT: '<?= GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
                                    BTN_MESSAGE_LAZY_LOAD: '<?= CUtil::JSEscape($arParams['MESS_BTN_LAZY_LOAD']) ?>',
                                    BTN_MESSAGE_LAZY_LOAD_WAITER: '<?= GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_LAZY_LOAD_WAITER') ?>',
                                    SITE_ID: '<?= CUtil::JSEscape($component->getSiteId()) ?>'
                            });
        });
        </script>
        
        <?
        $uniqueId = $arResult['ITEM']['ID'] . '_' . md5($this->randString() . $component->getAction());
        $areaId = $this->GetEditAreaId($uniqueId);
        $this->AddEditAction($uniqueId, $arResult['ITEM']['EDIT_LINK'], $elementEdit);
        $this->AddDeleteAction($uniqueId, $arResult['ITEM']['DELETE_LINK'], $elementDelete, $elementDeleteParams);
        
        $APPLICATION->IncludeComponent('bitrix:catalog.item', 'product_day', array(
                'RESULT' => array(
                    'ITEM' => $arResult['ITEM'],
                    'AREA_ID' => $areaId,
                    'TYPE' => "CARD",
                    'BIG_LABEL' => 'N',
                    'BIG_DISCOUNT_PERCENT' => 'N',
                    'BIG_BUTTONS' => 'N'
                ),
                'PARAMS' => $generalParams + array('BIG_CARD' => 'Y', 'SKU_PROPS' => $arResult['SKU_PROPS'][$arResult['ITEM']['IBLOCK_ID']])
                ),
                $component,
                array('HIDE_ICONS' => 'Y')
        );
}
?>
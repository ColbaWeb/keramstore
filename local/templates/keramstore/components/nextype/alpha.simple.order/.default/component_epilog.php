<?
use \Nextype\Alpha\Layout\Assets;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();


Assets::getInstance(SITE_ID)->addLess($templateFolder . "/styles.less");

if (!empty($arParams['YANDEXMAP_API_KEY']))
    Assets::getInstance(SITE_ID)->addJs("https://api-maps.yandex.ru/2.1/?apikey=".$arParams['YANDEXMAP_API_KEY']."&lang=ru_RU");

Assets::getInstance(SITE_ID)->addJs($templateFolder . "/js/vue.min.js");
Assets::getInstance(SITE_ID)->addJs($templateFolder . "/js/app.js");
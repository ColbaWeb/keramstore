(function () {
    'use strict';

    window.NextypeAlphaSimpleOrderComponent = {

        init: function (parameters)
        {
            this.containerId = parameters.containerId || false;
            this.data = parameters.data || {};
            this.dadata = parameters.dadata || {};
            this.params = parameters.params || {};
            this.signedParamsString = parameters.signedParamsString || '';
            this.siteID = parameters.siteID || '';
            this.ajaxUrl = parameters.ajaxUrl || '';
            this.locationsUrl = parameters.locationsUrl || '';
            //this.basketAjaxUrl = parameters.basketAjaxUrl || '';
            this.templateFolder = parameters.templateFolder || '';
            this.timeOut = false;
            this.smsCodeLength = 4;
            this.obMap = null;
            this.myPlacemarks = {};
            this.myClaster = false
            
            if (!this.containerId)
                return false;
            
            if (!!this.params.SMS_CODE_LENGTH && parseInt(this.params.SMS_CODE_LENGTH) > 0)
                this.smsCodeLength = parseInt(this.params.SMS_CODE_LENGTH);
            
            this.data.visual = {
                deliveryType: 'courier',
                pickupSelected: null,
                showCommentTextarea: false,
                locations: [],
                showLocationsList: false,
                pickupPopupOpened: false,
                showAddressSuggest: false,
                addressSuggest: [],
            };
            
            var _ = this;
            
            this.app = new Vue({
                el: _.containerId,
                data: _.data,
                
                mounted: function () {
                    
                    this.initJqueryComponents();
                    $(this.$el).removeClass('ajax-loading');
                    
                    if (this.visual.deliveryType == "pickup" && this.order.stores.length == 0)
                    {
                        this.visual.deliveryType = "courier";
                    }
                    else if (this.visual.deliveryType == "courier" && this.order.deliveries.length == 0)
                    {
                        this.visual.deliveryType = "pickup";
                    }
                },
                
                updated: function () {
                    
                    this.initJqueryComponents();
                    
                    if (this.visual.deliveryType == "pickup" && this.order.stores.length == 0)
                    {
                        this.setDeliveryType('courier');
                    }
                    else if (this.visual.deliveryType == "courier" && this.order.deliveries.length == 0)
                    {
                        this.setDeliveryType('pickup');
                    }

                },
                
                methods: {
                    
                    initJqueryComponents: function () {
                        
                        $(this.$el).find('[data-role="phone"]').mask(_.params['PHONE_MASK'], {
                            completed: function() {
                                _.app.properties.phone = this.val();
                            }
                        });
                        
                        $(this.$el).find('[data-role="phone"]').on('change', function () {
                            if ($(this).val() == "") {
                                _.app.properties.phone = '';
                            }
                        });
                        
                        
                        $(document).click(function(event) {
                            if ($(event.target).closest(".locations-list-container").length) return;
                            
                            _.app.visual.showLocationsList = false;
                            _.app.visual.showAddressSuggest = false;
                            
                            event.stopPropagation();
                        });

                        $('.custom-input input, .custom-input textarea').on('focusout', function () {
                            if ($(this).val() != '') $(this).addClass('filled')
                            else $(this).removeClass('filled');
                        });

                        $('.custom-input input, .custom-input textarea').each(function () {
                            if ($(this).val() != '') $(this).addClass('filled')
                            else $(this).removeClass('filled');
                        });
                        
                    },
                    
                    userSendSmsCode: function () {
                        
                        
                        _.sendAjaxRequest({
                            phone: _.app.properties.phone,
                            action: 'sendSmsCode'
                        }, BX.delegate( function (response)  {
                            
                            if (!this.checkError('user', 'phone'))
                            {
                                this.timeInterval = _.params.SMS_CODE_RESEND_DELAY;
                                this.startResendTimer();
                                this.user.state = 'sendedSmsCode';
                                
                                $('#order-simple-sms-code').focus();
                            }
                            
                        }, this));
                        
                        
                    },
                    
                    checkSmsCode: function (event) {
                        
                        if (_.smsCodeLength == (this.user.smsCode + '').length && this.user.state == "sendedSmsCode")
                        {
                            this.user.state = 'checkSmsCode';

                            _.sendAjaxRequest({
                                phone: _.app.properties.phone,
                                code: _.app.user.smsCode,
                                action: 'checkSmsCode'
                            }, BX.delegate( function (response)  {
                                
                                if (!!response && !!response.user.state && response.user.state == "confirmed")
                                {
                                    this.user.id = response.user.id;
                                    this.user.state = response.user.state;
                                }
                                
                            }, this)); 
                        }
                        else if (_.smsCodeLength > (this.user.smsCode + '').length)
                        {
                            this.user.state = "sendedSmsCode";
                        }
                        
                    },
                    
                    checkSmsCodeKeypress: function (event) {
                        
                        if (_.smsCodeLength == (this.user.smsCode + '').length)
                        {
                            event.preventDefault();
                        }
                    },
                    
                    setError: function (object, key, value) {
                        
                        if (typeof(this.errors) != 'object')
                            this.errors = {};
                        
                        if (typeof(this.errors[object]) != 'object')
                            this.$set(this.errors, object, {});
                        
                        this.$set(this.errors[object], key, value);
                        

                    },
                    
                    resetError: function (object, key) {
                        
                        key = (key) || null;
                        
                        if (typeof(this.errors) != 'object')
                            return true;
                        
                        if (typeof(this.errors[object]) == 'object' && !!typeof(this.errors[object][key]))
                        {
                            this.$set(this.errors[object], key, null);
                            return true;
                        }
                        
                    },
                    
                    checkError: function (object, key) {
                        
                        key = (key) || null;
                        
                        if (typeof(this.errors) != 'object')
                            return false;
                        
                        if (typeof(this.errors[object]) != 'object')
                            return false;
                        
                        if (!key)
                        {
                            return true;
                        }
                        
                        return !!this.errors[object][key];
                    },
                    
                    checkPropInMap: function (key) {
                        return (!!_.params.ORDER_PROP_MAP[key]);
                    },
                    
                    startResendTimer: function () {

                        this.timeInterval = _.params.SMS_CODE_RESEND_DELAY;

                        _.timeOut = setInterval(function () {
                            _.app.timeInterval--;
                            if (_.app.timeInterval <= 0) {
                                clearInterval(_.timeOut);
                            }
                        }, 1000);
                    },
                    
                    searchLocation: function (event) {
                        var phrase = event.target.value;
                        
                        if (event.keyCode == 27)
                        {
                            this.visual.showLocationsList = false;
                            return;
                        }
                   
                        if (phrase.length < 2)
                            return;
                        
                        phrase = phrase.toLowerCase();
                        
                        _.delay(BX.delegate(function () {
                            
                            var arData = {
                                    select: {
                                        1: 'CODE',
                                        2: 'TYPE_ID',
                                        VALUE: 'ID',
                                        DISPLAY: 'NAME.NAME'
                                    },
                                    additionals: {
                                        1: 'PATH',
                                    },
                                    filter: {
                                        '=PHRASE': phrase,
                                        '=NAME.LANGUAGE_ID': 'ru',
                                        '=SITE_ID': _.siteID
                                    },
                                    version: 2,
                                    PAGE_SIZE: 10,
                                    PAGE: 0,
                                    LOCATION_SERVICE_FIAS_ID: _.params.LOCATION_SERVICE_FIAS_ID
                            };
                            
                            $.ajax({
                                type: 'post',
                                url: _.locationsUrl,
                                data: arData,
                                success: BX.delegate(function (response) {
                                    if (typeof (response) == 'object' && response.data.ITEMS.length > 0)
                                    {
                                        this.visual.locations = [];
                                        
                                        for (var i in response.data.ITEMS)
                                        {
                                            var item = response.data.ITEMS[i], fullName = [];
                                            fullName.push(item.DISPLAY);
                                            
                                            if (!!response.data.ETC && !!response.data.ETC.PATH_ITEMS && typeof (item.PATH) == 'object' && item.PATH.length > 0)
                                            {
                                                for (var j in item.PATH)
                                                {
                                                    var pathId = item.PATH[j];
                                                    if (!!response.data.ETC.PATH_ITEMS[pathId])
                                                        fullName.push(response.data.ETC.PATH_ITEMS[pathId].DISPLAY);
                                                }
                                            }
                                            
                                            var fullNameDispaly = fullName.join(', '),
                                                lowerFullName = fullNameDispaly.toLowerCase(),
                                                index = lowerFullName.indexOf(phrase);
                                                    
                                                if (index != -1) {
                                                    fullNameDispaly = fullNameDispaly.substr(0, index) + '<b>' + fullNameDispaly.substr(index, phrase.length) + '</b>' + fullNameDispaly.substr(index + phrase.length);
                                                }
                                            
                                            this.visual.locations.push({
                                                code: item.CODE,
                                                fiasId: item.FIAS_ID,
                                                displayName: fullNameDispaly,
                                                fullName: fullName.join(', ')
                                            });

                                        }
                                        
                                        this.visual.showLocationsList = true;
                                    }
                                    
                                }, this),
                                error: function (jqXHR, exception) {
                                    _.showAjaxError(jqXHR, exception);
                                }
                            });

                        }, this), 400);
                    },
                    
                    setLocation: function (location) {
                        this.visual.showLocationsList = false;
                        
                        if (!!location && typeof(location) == 'object') {
                            this.properties.location = location.code;
                            this.properties.locationText = location.fullName;
                            this.properties.locationFiasId = location.fiasId;
                            
                            _.sendAjaxRequest({
                                'action': 'refreshOrderAjax'
                            });
                            
                            
                        }
                    },
                    
                    searchAddress: function (event) {
                        var phrase = event.target.value;
                        
                        if (!this.properties.locationFiasId || this.properties.locationFiasId == "")
                            return;
                        
                        if (event.keyCode == 27)
                        {
                            this.visual.showAddressSuggest = false;
                            return;
                        }
                   
                        if (phrase.length < 2)
                            return;
                        
                        phrase = phrase.toLowerCase();
                        
                        _.delay(BX.delegate(function () {
                            
                            var arData = {
                                query: phrase,
                                count: 10,
                                locations: [{
                                    city_fias_id: this.properties.locationFiasId
                                }]
                            };
                            
                            $.ajax({
                                type: 'post',
                                url: _.dadata.apiHost,
                                data: JSON.stringify(arData),
                                beforeSend: function(request) {
                                    request.setRequestHeader("Content-Type", "application/json");
                                    request.setRequestHeader("Accept", "application/json");
                                    request.setRequestHeader("Authorization", "Token " + _.dadata.token);
                                },
                                success: BX.delegate(function (response) {
                                    
                                    if (typeof(response) == 'object' && response.suggestions.length > 0)
                                    {
                                        this.visual.addressSuggest = [];
                                        
                                        for (var i in response.suggestions)
                                        {
                                            var suggestion = response.suggestions[i].data,
                                                item = [];
                                            
                                            if (!!suggestion.street_with_type)
                                                item.push(suggestion.street_with_type);
                                            
                                            if (!!suggestion.house)
                                                item.push(suggestion.house_type + ' ' + suggestion.house);
                                            
                                            
                                            this.visual.addressSuggest.push({
                                                fiasId: suggestion.fias_id,
                                                displayName: item.join(', ')
                                            });
                                        }
                                        
                                        this.visual.showAddressSuggest = true;
                                    }
                                    
                                }, this),
                                error: function (jqXHR, exception) {
                                    _.showAjaxError(jqXHR, exception);
                                }
                            });
                            
                            
                        }, this), 400);
                    },
                    
                    setAddress: function (address) {
                        this.visual.showAddressSuggest = false;
                        
                        if (!!address && typeof(address) == 'object') {
                            this.properties.address = address.displayName;
                        }
                    },
                    
                    setStore: function (itemId, itemAddress) {
                        itemAddress = (itemAddress) || '';
                        
                        this.properties.storeId = itemId;
                        this.properties.deliveryId = this.order.storeDeliveryId;
                        this.visual.pickupSelected = true;
                        this.visual.pickupPopupOpened = false;

                    },
                    
                    setDeliveryType(type) {
                        
                        
                        if (type == "courier" && this.order.deliveries.length > 0) {
                            this.visual.deliveryType = "courier";
                            this.properties.storeId = "";
                            this.visual.pickupSelected = null;

                            let this_ = this;
                            let selectedDelivery = this.order.deliveries.find(function (o){
                                return o.id != this_.order.storeDeliveryId;
                            });

                            if(selectedDelivery){
                                this.properties.deliveryId = selectedDelivery.id;
                            }
                            
                            _.sendAjaxRequest({
                                'action': 'refreshOrderAjax'
                            });
                        }
                        else if (type == "pickup" && this.order.stores.length > 0) {
                            
                            this.visual.deliveryType = "pickup";
                            this.properties.storeId = "";
                            this.properties.deliveryId = this.order.storeDeliveryId;
                            this.visual.pickupSelected = null;
                            
                            _.sendAjaxRequest({
                                'action': 'refreshOrderAjax'
                            });
                        }

                    },
                    
                    scrollToStart: function() {
                        let node = document.getElementsByTagName('body'), phoneNode = BX('order-simple-phone');
                        if (node[0])
                            node[0].scrollIntoView({behavior: 'smooth'});
                        if (phoneNode && !BX.hasClass(phoneNode, 'filled'))
                            BX.addClass(phoneNode, 'filled');
                        this.showNodeError(BX.findParent(phoneNode, {tag: 'div', class: 'field-row'}, true));
                    },
                    
                    showNodeError: function(node) {
                        if (!node)
                            return;
                        node.classList.add("delay-error");
                        setTimeout(() => {
                            node.classList.remove("delay-error");
                        }, 1200)
                    },
                    
                    refreshOrder: function () {
                        _.sendAjaxRequest({
                            'action': 'refreshOrderAjax'
                        });
                    },
                    
                    saveOrder: function () {
                        
                        var bValid = true,
                            fieldOffsetTop = 0;
                        
                        if (this.user.state != 'confirmed')
                        {
                            this.setError('user', 'phone', BX.message('TPL_PHONE_CONFIRM_ERROR'));
                            bValid = false;
                            
                            if (fieldOffsetTop <= 0)
                                fieldOffsetTop = $("#order-simple-phone").offset().top;
                        }
                        
                        var requiredProperties = ['name', 'surname', 'email'];
                        
                        if (this.visual.deliveryType == "courier")
                        {
                            requiredProperties.push('address');
                        }
                        else
                        {
                            if (!this.properties.storeId || this.properties.storeId == "")
                            {
                                this.setError('properties', 'storeId', BX.message('TPL_SELECT_PICKPOINT'));
                                
                                if (fieldOffsetTop <= 0)
                                    fieldOffsetTop = $("#order-simple-storeId").offset().top;
                            }
                                
                                
                        }
                        
                        for (var key in requiredProperties)
                        {
                            var fieldKey = requiredProperties[key];
                            if ($.trim(this.properties[fieldKey]) == "")
                            {
                                if (fieldOffsetTop <= 0)
                                    fieldOffsetTop = $("#order-simple-" + fieldKey).offset().top;

                                this.setError('properties', fieldKey, BX.message('TPL_REQUIRED_FIELD'));
                                bValid = false;
                            }
                        }
                        
                        
                        
                        if (bValid)
                        {
                        
                            _.sendAjaxRequest({
                                'action': 'saveOrderAjax'
                            }, BX.delegate( function (response)  {

                                    if (!!response && !!response.order.id && parseInt(response.order.id) > 0)
                                    {
                                        if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                                        {
                                            new window.Alpha.Messages({
                                                type: 'default',
                                                title: BX.message('TPL_MESSAGE_SUCCESS_SAVE_TITLE'),
                                                message: BX.message('TPL_MESSAGE_SUCCESS_SAVE_CONTENT')
                                            });
                                        }

                                        if (!!response.order.redirectUrl && response.order.redirectUrl != false) {
                                            window.location = response.order.redirectUrl;
                                        }

                                    }

                             }, this)); 
                         }
                         else
                         {
                             if (fieldOffsetTop > 0)
                                $("html, body").stop().animate({scrollTop: fieldOffsetTop - 40}, 500, 'swing');
                         }
                        
                    },
                    
                    
                },
                
                watch: {
                    'visual.pickupPopupOpened': function () {
                        
                        if (!!this.$refs.pickupMap)
                        {
                            var pickupMap = this.$refs.pickupMap;
                            
                            if (!!_.obMap)
                                _.obMap.destroy();
                            
                            ymaps.ready(function () {
                                _.obMap = new ymaps.Map(pickupMap, {
                                    center: [55.76, 37.64],
                                    zoom: 7,
                                    controls: [
                                        'zoomControl'
                                    ]
                                });
                                
                                if (!$.isEmptyObject(_.app.order.stores))
                                {
                                    var myPlacemarks = {};
                                    _.myClaster = new ymaps.Clusterer({
                                        clusterHideIconOnBalloonOpen: false,
                                        geoObjectHideIconOnBalloonOpen: false
                                    });
                                    
                                    _.myCollection = new ymaps.GeoObjectCollection();
 
                                    for (var key in _.app.order.stores)
                                    {
                                        var item = _.app.order.stores[key];
                                        
                                        
                                        if (item.coords == "")
                                            continue;
                                        
                                        var coords = item.coords.split(","),
                                            content = '<div class="ballon-content">',
                                            placemarkOptions = {};
                                            
                                        content += '<div class="info">'+item.title+'</div>';

                                        if (item.schedule != "")
                                            content += '<div class="worktime">' + item.schedule + '</div>';

                                        content += '<button class="btn btn-primary mt-3" onclick="window.obNextypeAlphaSimpleOrderComponent.selectPickPoint(\'' + item.id + '\')" type="button">'+BX.message('TPL_PICKUP_MODAL_SELECT_PICKUP')+'</button>';
                                        content += '</div>';
                                            
                                        _.myPlacemarks[item.id] = new ymaps.Placemark([coords[0], coords[1]],
                                                {
                                                    balloonContentHeader: item.address,
                                                    balloonContent: content
                                                    
                                                },
                                                placemarkOptions
                                        );
                                
                                        _.myClaster.add(_.myPlacemarks[item.id]);   
                                     }
                                     
                                     _.obMap.geoObjects.add(_.myClaster);
                                    
                                    _.obMap.setBounds(_.myClaster.getBounds(), {
                                        checkZoomRange: true
                                    });
                                }
                            });
                        }

                    }
                },
                
                computed: {
                    isCourierDelivery: function () { return this.visual.deliveryType == 'courier'; },
                    isPickupDelivery: function () { return this.visual.deliveryType == 'pickup'; },
                    isPickupSelected: function () { return this.visual.pickupSelected != null; },
                    resendTimeoutMessage: function () { return BX.message('AUTH_CHECK_CODE_REPEAT_WAITING').replace('#SECONDS#', this.timeInterval); },
                    
                    storeAddress: function () {

                        if (!!this.properties.storeId && parseInt(this.properties.storeId) > 0 && !$.isEmptyObject(this.order.stores))
                        {
                            for (var i in this.order.stores) {
                                if (this.order.stores[i].id + '' == this.properties.storeId + '') {
                                    return this.order.stores[i].address;
                                }
                            }
                        }
                        
                        return '';
                    },
                    
                    storeWorktime: function () {
                        if (!!this.properties.storeId && parseInt(this.properties.storeId) > 0 && !$.isEmptyObject(this.order.stores))
                        {
                            for (var i in this.order.stores) {
                                if (this.order.stores[i].id + '' == this.properties.storeId + '') {
                                    return this.order.stores[i].schedule;
                                }
                            }
                        }
                        return '';
                    }
                }
            });
            
            return this;
        },
        
        sendAjaxRequest: function (data, callback) {
            
                callback = (callback) || false;
                data = (data) || {};
                
                data.signedParamsString = this.signedParamsString;
                data.siteID = this.siteID;
                data.sessid = BX.bitrix_sessid();
                
                data = this.prepareProperties(data);
                
                $(this.app.$el).addClass('ajax-loading');
                
                $.ajax({
                    type: 'post',
                    url: this.ajaxUrl,
                    data: data,
                    success: BX.delegate( function (response)  {
                        
                        $(this.app.$el).removeClass('ajax-loading');
                        
                        if (!!response && typeof(response) == 'object') {

                                var types = ['basket', 'order', 'properties', 'errors'];
                                for (var typeKey in types) {
                                    var responseType = response[types[typeKey]];

                                    if (responseType !== undefined && typeof (responseType) == 'object') {
                                        if (!$.isEmptyObject(responseType))
                                        {
                                            for (var key in responseType) {
                                                if (key !== "comment")
                                                    this['app'][types[typeKey]][key] = responseType[key];
                                            }
                                        }
                                        else
                                        {
                                            this['app'][types[typeKey]] = [];
                                        }
                                    }
                                }

                                if (typeof callback == 'function')
                                    callback.call(this, response);
                        }
                    }, this),
                    error: BX.delegate( function (jqXHR, exception) {
                        $(this.app.$el).removeClass('ajax-loading');
                        this.showAjaxError(jqXHR, exception);
                    }, this)
                });
        },
        
        prepareProperties: function (data) {
            
            data['order'] = {
                'sessid': BX.bitrix_sessid(),
                'soa-action': 'saveOrderAjax',
                'location_type': 'code',
                'PERSON_TYPE': this['app']['properties']['personTypeId'],
                'PERSON_TYPE_OLD': this['app']['properties']['personTypeId'],
                'ORDER_DESCRIPTION': this['app']['properties']['comment'],
                'ZIP_PROPERTY_CHANGED': 'N',
                'profile_change': 'N'
            };
            
            data['location_type'] = 'code';
            data['PERSON_TYPE'] = this['app']['properties']['personTypeId'];
            data['PERSON_TYPE_OLD'] = this['app']['properties']['personTypeId'];
            data['ORDER_DESCRIPTION'] = this['app']['properties']['comment'];   
            data['ZIP_PROPERTY_CHANGED'] = "N";
            
            //data['soa-action'] = 'refreshOrderAjax';
            
            if (!!this.params.ORDER_PROP_MAP)
            {
                for (var propCode in this.params.ORDER_PROP_MAP)
                {
                    if (!!this['app']['properties'][propCode])
                    {
                        data['order'][this.params.ORDER_PROP_MAP[propCode]] = this['app']['properties'][propCode];
                        data[this.params.ORDER_PROP_MAP[propCode]] = this['app']['properties'][propCode];
                        
                        if (propCode == "location")
                        {
                            data['order']['RECENT_DELIVERY_VALUE'] = this['app']['properties'][propCode];
                            data['RECENT_DELIVERY_VALUE'] = this['app']['properties'][propCode];
                        }
                    }
                }
            }

            return data;
        },
        
        selectPickPoint: function (key) {

            if (typeof (this.app.order.stores) == 'object' && !!this.myPlacemarks[key])
            {

                this.myPlacemarks[key].balloon.close();

                for (var i in this.app.order.stores)
                {
                    if (this.app.order.stores[i].id == key)
                    {
                        var shop = this.app.order.stores[i];
                        
                        this.app.setStore(shop.id, shop.address);

                        break;
                    }
                }
            }
            
            this.app.visual.pickupPopupOpened = false;
        },
        
        showAjaxError: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            console.log(msg);
        },
        
        delay: function (callback, ms) {
            clearTimeout(this.timeOut);
            this.timeOut = setTimeout(callback.bind(this), ms);
                
        }
    }
})();
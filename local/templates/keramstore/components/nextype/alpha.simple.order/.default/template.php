<?
use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

use Nextype\Alpha\Options;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

$arParams['PHONE_MASK'] = Options\Base::getInstance(SITE_ID)->getValue('phoneNumberMask');

$context = Main\Application::getInstance()->getContext();
$request = $context->getRequest();

if (strlen($request->get('ORDER_ID')) > 0)
{
    include(Main\Application::getDocumentRoot().$templateFolder.'/confirm.php');
}
elseif ($arParams['DISABLE_BASKET_REDIRECT'] === 'Y' && $arResult['SHOW_EMPTY_BASKET'])
{
	include(Main\Application::getDocumentRoot().$templateFolder.'/empty.php');
}
else
{
?>
<div class="short-header-title align-items-center d-flex flex-column container">
    <? if (!empty($arParams['PATH_TO_BASKET'])): ?>
    <div class="back-to-basket">
        <a href="<?=$arParams['PATH_TO_BASKET']?>"><?=GetMessage('TPL_HEADER_BACK_TO_BASKET')?></a>
    </div>
    <? endif; ?>
    <h1><? $APPLICATION->ShowTitle(false) ?></h1>
</div>

<div class="order-simple-container ajax-loading" id="order-simple">
    <div class="row" v-bind:class="{'unconfirmed': user.state != 'confirmed'}">
        <div class="col-lg-6 col-md-12">
            <div class="alert alert-danger" v-if="checkError('main')">
                <div v-for="error in errors.main" v-html="error"></div>
            </div>
            <div class="properties-container">
                <div class="head-title"><?=GetMessage('TPL_CONTACTS_BLOCK_TITLE')?></div>
                <div class="head-sub-text"><?=GetMessage('TPL_REQUIRED_DESCRIPTION')?></div>
                
                <div class="properties-list">
                    
                    <div class="row field-row" v-bind:class="{'sended-sms': user.id == false && properties.phone != '' && (user.state == 'sendedSmsCode' || user.state == 'checkSmsCode')}">
                        <div class="col-lg-6 col-md-12 col-phone-input">
                            <div class="custom-input-wrap">
                                <div class="custom-input" v-bind:class="{'confirmed': user.state == 'confirmed', 'error': checkError('user', 'phone')}">
                                    <input autocomplete="off" v-on:keyup="resetError('user', 'phone')" type="text" v-bind:readonly="user.state == 'confirmed'" v-model="properties.phone" data-role="phone" id="order-simple-phone"/>
                                    <label for="order-simple-phone"><?=GetMessage('TPL_FIELD_PHONE_NUMBER')?></label>
                                </div>
                                <div class="error-message" v-if="checkError('user', 'phone')" v-html="errors.user.phone"></div>
                            </div>
                        </div>
                        
                        
                        <div class="col-lg-6 col-md-12 col-smscode-input" v-if="user.id == false && properties.phone != '' && (user.state == 'sendedSmsCode' || user.state == 'checkSmsCode')">
                            <div class="custom-input-wrap">
                                <div class="custom-input" v-bind:class="{'error': checkError('user', 'smsCode')}">
                                    <input type="text" v-model.number="user.smsCode" v-on:keyup="checkSmsCode" v-on:keypress="checkSmsCodeKeypress" id="order-simple-sms-code"/>
                                    <label for="order-simple-sms-code"><?=GetMessage('TPL_FIELD_SMS_CODE')?></label>
                                </div>
                                <div class="error-message" v-if="checkError('user', 'smsCode')" v-html="errors.user.smsCode"></div>
                            </div>
                        </div>
                        
                        <div v-if="user.id == false && properties.phone == '' && user.state == false" class="field-description col-12">
                            <?=GetMessage('TPL_FIELD_SMS_CODE_DESCRIPTION')?>
                        </div>
                        
                        <div v-else-if="user.id == false && properties.phone != '' && user.state == false" class="field-description col-12">
                            <a href="javascript:void(0)" class="send-smscode" v-on:click.prevent="userSendSmsCode"><?=GetMessage('TPL_FIELD_SEND_SMS_CODE')?></a>
                        </div>
                        
                        <div v-else-if="user.id == false && properties.phone != '' && (user.state == 'sendedSmsCode' || user.state == 'checkSmsCode')" class="field-description col-12">
                            <span v-if="timeInterval >= 1" v-html="resendTimeoutMessage"></span>
                            <a href="javascript:void(0)" v-if="timeInterval < 1" class="send-smscode" v-on:click.prevent="userSendSmsCode"><?=GetMessage('TPL_FIELD_REPEAT_SMS_CODE')?></a>
                        </div>
                        
                        <div v-else-if="user.id != false && user.state != 'confirmed' && properties.phone != ''" class="field-description col-12">
                            <a href="javascript:void(0)" class="send-smscode" v-on:click.prevent="userSendSmsCode"><?=GetMessage('TPL_FIELD_SEND_SMS_CODE')?></a>
                        </div>
                        
                        <div v-else-if="user.state == 'confirmed'" class="field-description col-12">
                            <?=GetMessage('TPL_FIELD_PHONE_NUMBER_CONFIRMED')?> <a href="<?=$APPLICATION->GetCurDir() . "?logout=yes&" . bitrix_sessid() ?>"><?=GetMessage('TPL_FIELD_CHANGE_PHONE_NUMBER')?></a>
                        </div>
                        
                    </div>

                    
                    <div class="row field-row">
                        <div class="col-6">
                            <div class="custom-input-wrap">
                                <div class="custom-input" v-bind:class="{'error': checkError('properties', 'name')}">
                                    <input type="text" autocomplete="off" v-model="properties.name" id="order-simple-name"/>
                                    <label for="order-simple-name"><?=GetMessage('TPL_FIELD_NAME')?></label>
                                </div>
                                <div class="error-message" v-if="checkError('properties', 'name')" v-html="errors.properties.name"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="custom-input-wrap">
                                <div class="custom-input" v-bind:class="{'error': checkError('properties', 'surname')}">
                                    <input type="text" autocomplete="off" v-model="properties.surname" id="order-simple-surname"/>
                                    <label for="order-simple-surname"><?=GetMessage('TPL_FIELD_SURNAME')?></label>
                                </div>
                                <div class="error-message" v-if="checkError('properties', 'surname')" v-html="errors.properties.surname"></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row field-row">
                        <div class="col-12">
                            <div class="custom-input-wrap">
                                <div class="custom-input" v-bind:class="{'error': checkError('properties', 'email')}">
                                    <input type="email" autocomplete="off" v-model="properties.email" id="order-simple-email"/>
                                    <label for="order-simple-email"><?=GetMessage('TPL_FIELD_EMAIL')?></label>
                                </div>
                                <div class="error-message" v-if="checkError('properties', 'email')" v-html="errors.properties.email"></div>
                            </div>
                        </div>
                        <div class="col-12 field-description">
                            <?=GetMessage('TPL_FIELD_EMAIL_DESCRIPTION')?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="properties-container">
                <div class="head-title"><?=GetMessage('TPL_DELIVERY_BLOCK_TITLE')?></div>
                
                <div class="properties-list">
                    
                    <div class="row field-row">
                        <div class="col-12">
                            <div class="custom-input-wrap locations-list-container">
                                <div class="custom-input">
                                    <input type="text" autocomplete="off" v-on:keyup="searchLocation" v-on:focus="visual.showLocationsList = true" v-model="properties.locationText" id="order-simple-city"/>
                                    <label for="order-simple-city"><?=GetMessage('TPL_FIELD_CITY')?></label>
                                </div>
                                <div class="locations-list" v-if="visual.showLocationsList && visual.locations.length > 0">
                                    <a href="#" v-on:click.prevent="setLocation(location)" class="item" v-for="location in visual.locations" v-html="location.displayName"></a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="field-description col-12">
                            <?=GetMessage('TPL_FIELD_CITY_DESCRIPTION')?>
                        </div>

                    </div>
                    
                    <div class="delivery-types">
                        <a href="#" v-on:click.prevent="setDeliveryType('courier')" v-bind:class="{'active': isCourierDelivery, 'disabled': order.deliveries.length == 0}" class="item"><?=GetMessage('TPL_DELIVERY_TAB_TITLE')?></a>
                        <a href="#" v-on:click.prevent="setDeliveryType('pickup')" v-bind:class="{'active': isPickupDelivery, 'disabled': order.stores.length == 0}" class="item"><?=GetMessage('TPL_PICKUP_TAB_TITLE')?></a>
                    </div>
                    
                    <div v-if="isCourierDelivery && order.deliveries.length > 0" class="radio-list">
                        <div v-for="item in order.deliveries" class="item custom-radio" v-show="item.id != order.storeDeliveryId">
                            <input type="radio" name="delivery" v-on:change="refreshOrder" v-model="properties.deliveryId" v-bind:value="item.id" class="custom-control-input" v-bind:id="'delivery-' + item.id">
                            <label class="custom-control-label" v-bind:for="'delivery-' + item.id">
                                <span v-html="item.name"></span><span class="period" v-if="item.periodPrint != ''" v-html="item.periodPrint"></span><span class="price" v-if="item.pricePrint != ''" v-html="item.pricePrint"></span>
                            </label>
                        </div>
                    </div>
                    
                    <div v-if="isPickupDelivery && !isPickupSelected" class="select-pickpoint-container" v-bind:class="{'error': checkError('properties', 'storeId')}">
                        <div class="head-title">
                            <?=GetMessage('TPL_SELECT_PICKUP_TITLE')?>
                        </div>
                        <a href="#" v-on:click.prevent="visual.pickupPopupOpened = true" class="btn btn-primary btn-large js-open-modal"><?=GetMessage('TPL_SELECT_PICKUP')?></a>
                        
                        <div class="error-message" v-if="checkError('properties', 'storeId')" v-html="errors.properties.storeId"></div>
                    </div>
                    
                    <div v-if="isPickupDelivery && isPickupSelected" id="order-simple-storeId" class="selected-pickpoint-container">
                        <div class="head-title" v-html="storeAddress"></div>
                        <div class="description" v-if="storeWorktime != ''" v-html="storeWorktime"></div>
                        <a href="#" v-on:click.prevent="visual.pickupPopupOpened = true" class="btn btn-show-more js-open-modal"><?=GetMessage('TPL_CHANGE_PICKUP')?></a>
                    </div>
                    
                    <div class="row field-row" v-if="isCourierDelivery && checkPropInMap('address')">
                        <div class="col-12">
                            <div class="custom-input-wrap locations-list-container">
                                <div class="custom-input" v-bind:class="{'error': checkError('properties', 'address')}">
                                    <input type="text" v-on:keyup="searchAddress" v-on:focus="visual.showAddressSuggest = true" v-model="properties.address" id="order-simple-address"/>
                                    <label for="order-simple-address"><?=GetMessage('TPL_FIELD_ADDRESS')?></label>
                                </div>
                                <div class="locations-list" v-if="visual.showAddressSuggest && visual.addressSuggest.length > 0">
                                    <a href="#" v-on:click.prevent="setAddress(address)" class="item" v-for="address in visual.addressSuggest" v-html="address.displayName"></a>
                                </div>
                                <div class="error-message" v-if="checkError('properties', 'address')" v-html="errors.properties.address"></div>
                            </div>
                        </div>
                        
                        <div class="field-description col-12">
                            <?=GetMessage('TPL_FIELD_ADDRESS_DESCRIPTION')?>
                        </div>

                    </div>
                    
                    <div class="row field-row" v-if="isCourierDelivery">
                        <div class="col-6" v-if="checkPropInMap('flat')">
                            <div class="custom-input-wrap">
                                <div class="custom-input">
                                    <input v-model="properties.flat" type="text" id="order-simple-flat"/>
                                    <label for="order-simple-flat"><?=GetMessage('TPL_FIELD_FLAT')?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-6" v-if="checkPropInMap('floor')">
                            <div class="custom-input-wrap">
                                <div class="custom-input">
                                    <input v-model="properties.floor" type="text" id="order-simple-floor"/>
                                    <label for="order-simple-floor"><?=GetMessage('TPL_FIELD_FLOOR')?></label>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <a href="#" v-if="!visual.showCommentTextarea" v-on:click.prevent="visual.showCommentTextarea = true" class="order-add-comnent"><?=GetMessage('TPL_ADD_COMMENT')?></a>
                    
                    <div class="row field-row" v-if="visual.showCommentTextarea">
                        <div class="col-12">
                            <div class="custom-input-wrap">
                                <div class="custom-input">
                                    <textarea v-model="properties.comment" id="order-simple-comment"></textarea>
                                    <label for="order-simple-comment"><?=GetMessage('TPL_FIELD_COMMENT')?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="properties-container">
                <div class="head-title"><?=GetMessage('TPL_PAYSYSTEMS_BLOCK_TITLE')?></div>
                
                <div class="properties-list">
                    <div v-if="order.paySystems.length > 0" class="radio-list">
                        <div v-for="item in order.paySystems" class="item custom-radio">
                            <input type="radio" name="paysystem" v-on:change="refreshOrder" v-bind:value="item.id" v-model="properties.paySystemId" class="custom-control-input" v-bind:id="'paysystem-' + item.id">
                            <label class="custom-control-label" v-bind:for="'paysystem-' + item.id">
                                <span v-html="item.name"></span>
                                <div class="description" v-if="item.description != ''" v-html="item.description"></div>
                            </label>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="order-actions">
                <button type="button" v-on:click.prevent="saveOrder" class="btn btn-primary btn-large"><?=GetMessage('TPL_SUBMIT_ORDER')?></button>
                
                <div class="agree-policy">
                    <?=GetMessage('TPL_SUBMIT_ORDER_AGREE_1')?><a href="<?=SITE_DIR?>policy/" target="_blank"><?=GetMessage('TPL_SUBMIT_ORDER_AGREE_2')?></a>
                </div>
            </div>
            
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="order-simple-basket">
                <div class="head-title"><?=GetMessage('TPL_BASKET_BLOCK_TITLE')?></div>
                <div class="basket-items">
                    <div class="item" v-for="item in basket">
                        <div class="image" v-bind:style="{'background-image': 'url('+item.previewPicture+')'}"></div>
                        <div class="info">
                            <a target="_blank" v-bind:href="item.url" class="name" v-html="item.name"></a>
                            <div class="price">
                                <span v-html="item.price.print"></span><span class="measure" v-if="item.measure != ''" v-html="item.quantity + ' ' + item.measure + '.'"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="summary-container">
                    <div class="item">
                        <div class="item-label"><?=GetMessage('TPL_BASKET_PRODUCTS_PRICE')?></div>
                        <div class="item-value" v-html="order.priceWithoutDiscountPrint"></div>
                    </div>
                    <div class="item" v-if="order.deliveryPricePrint">
                        <div class="item-label"><?=GetMessage('TPL_BASKET_DELIVERY_PRICE')?></div>
                        <div class="item-value" v-html="order.deliveryPricePrint"></div>
                    </div>
                    <div class="item item-discount" v-if="order.discountPrice > 0">
                        <div class="item-label"><?=GetMessage('TPL_BASKET_DISCOUNT_VALUE')?></div>
                        <div class="item-value" v-html="order.discountPricePrint"></div>
                    </div>
                    <div class="item item-total">
                        <div class="item-label"><?=GetMessage('TPL_BASKET_TOTAL_PRICE')?></div>
                        <div class="item-value" v-html="order.totalPricePrint"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="pickup-container-wrapper" v-bind:class="{'open': visual.pickupPopupOpened == true}">
        <div class="container">
            <a href="#" v-on:click.prevent="visual.pickupPopupOpened = false" class="close-button"></a>
            <div class="pickup-container">
                <div class="pickup-list">
                    <div class="scrollbar-inner">
                        <div class="pickup-item" v-for="item in order.stores">
                            <div class="address" v-html="item.address"></div>
                            <div class="additional" v-if="item.title != ''" v-html="item.title"></div>
                            <div class="additional" v-if="item.description != ''" v-html="item.description"></div>
                            <div class="additional" v-if="item.phone != ''" v-html="item.phone"></div>
                            <div class="additional" v-if="item.schedule != ''" v-html="item.schedule"></div>
                            <a href="#" v-on:click.prevent="setStore(item.id, item.address)" class="btn close-button"><?=GetMessage('TPL_PICKUP_POPUP_SELECT_BUTTON')?></a>
                        </div>
                    </div>
                </div>
                <div class="pickup-map" ref="pickupMap"></div>
            </div>
        </div>
    </div>
    
</div>

<?
$signer = new Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'nextype.alpha.simple.order');
$messages = Loc::loadLanguageFile(__FILE__);

?>

<script>
            BX.message(<?= CUtil::PhpToJSObject($messages) ?>);
            
            $(document).ready(function () {
                    
                window.obNextypeAlphaSimpleOrderComponent = NextypeAlphaSimpleOrderComponent.init({
                    containerId: '#order-simple',
                    data: <?= CUtil::PhpToJSObject($arResult); ?>,
                    params: <?= CUtil::PhpToJSObject($arParams) ?>,
                    signedParamsString: '<?= CUtil::JSEscape($signedParams) ?>',
                    siteID: '<?= CUtil::JSEscape($component->getSiteId()) ?>',
                    ajaxUrl: '<?=CUtil::JSEscape($component->getPath().'/ajax.php')?>',
                    locationsUrl: '<?=CUtil::JSEscape($component->getPath().'/locations.php')?>',
                    templateFolder: '<?= CUtil::JSEscape($templateFolder) ?>',
                    dadata: {
                        apiHost: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
                        token: '<?=Options\Base::getInstance(SITE_ID)->getValue('simpleOrderDadataToken')?>'
                    }
                });
                    
                let header = $(document).find('.header-fixed');
                let offset = header.height();
                $('.order-simple-basket').css('top', offset);

                $(document).on('click', '.js-open-modal', function () {
                    $('body').trigger('modalOpen').addClass('modal-opened');
                });

                $('.close-button').click(function () {
                    $('body').removeClass('modal-opened').trigger('modalClose');
                });

                const ps = new PerfectScrollbar('.scrollbar-inner', {
                    wheelSpeed: 1,
                    wheelPropagation: false,
                    maxScrollbarLength: 100,
                    suppressScrollX: true,
                });
                $(this).on('ps:update', function() {
                    ps.update();
                });
            });
</script>
<?
}
?>
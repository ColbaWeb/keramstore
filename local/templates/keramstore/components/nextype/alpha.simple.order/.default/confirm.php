<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 */

if ($arParams["SET_TITLE"] == "Y")
{
    $APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_COMPLETE"));
}

?>

<? if (!empty($arResult["ORDER"])): ?>

<div class="order-simple-confirm">
    <h1><? $APPLICATION->ShowTitle(false) ?></h1>
    <div class="important-description">
        <?=Loc::getMessage("SOA_ORDER_SUC", array(
                    "#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"]->toUserTime()->format('d.m.Y H:i'),
                    "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]
                ))?>
    </div>
    <? if (!empty($arResult['ORDER']["PAYMENT_ID"])): ?>
    <div class="description">
        <?=Loc::getMessage("SOA_PAYMENT_SUC", array(
                        "#PAYMENT_ID#" => $arResult['PAYMENT'][$arResult['ORDER']["PAYMENT_ID"]]['ACCOUNT_NUMBER']
                    ))?>
    </div>
    <? endif ?>
    
    <? if ($arParams['NO_PERSONAL'] !== 'Y'): ?>
    <div class="description">
        <?=Loc::getMessage('SOA_ORDER_SUC1', ['#LINK#' => $arParams['PATH_TO_PERSONAL']])?>
    </div>
    <? endif; ?>
    
    <?
    if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y')
    {
        if (!empty($arResult["PAYMENT"]))
        {
            foreach ($arResult["PAYMENT"] as $payment)
            {
                if ($payment["PAID"] != 'Y')
                {
                    if (!empty($arResult['PAY_SYSTEM_LIST'])
                        && array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
                    )
                    {
                        $arPaySystem = $arResult['PAY_SYSTEM_LIST_BY_PAYMENT_ID'][$payment["ID"]];

                        if (empty($arPaySystem["ERROR"]))
                        {
                            ?>
                            
                            <div class="h5"><?=Loc::getMessage("SOA_PAY") ?></div>
                            
                            <div class="d-flex align-items-center">
                                <div class="col-auto">
                                    <?=CFile::ShowImage($arPaySystem["LOGOTIP"], 100, 100, "border=0\" style=\"width:100px\"", "", false) ?>
                                </div>
                                <div class="col-auto">
                                    <?=$arPaySystem["NAME"] ?>
                                </div>
                            </div>
                            
                            <?
                            
                            if ($arPaySystem["ACTION_FILE"] <> '' && $arPaySystem["NEW_WINDOW"] == "Y" && $arPaySystem["IS_CASH"] != "Y")
                            {
                                $orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
                                $paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
                                ?>
                                <script>
                                    window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=$orderAccountNumber?>&PAYMENT_ID=<?=$paymentAccountNumber?>');
                                </script>    
                                <?
                                echo Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber));
                                
                                if (CSalePdf::isPdfAvailable() && $arPaySystem['IS_AFFORD_PDF'])
                                {
                                    echo Loc::getMessage("SOA_PAY_PDF", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&pdf=1&DOWNLOAD=Y"));
                                }
                            }
                            else
                            {
                                ?><div class="description"><?
                                echo $arPaySystem["BUFFERED_OUTPUT"];
                                ?></div><?
                            }
                            
                        }
                        else
                        {
                            ?>
                            <div class="alert alert-danger"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></div>
                            <?
                        }
                    }
                    else
                    {
                        ?>
                        <div class="alert alert-danger"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></div>
                        <?
                    }
                }
            }
        }
    }
    else
    {
        ?>
        <div class="alert alert-danger"><?=$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR']?></div>
        <?
    }
    ?>
    
</div>


<? else:

    if ($arParams["SET_TITLE"] == "Y")
    {
        $APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_ERROR_TITLE"));
    }
    
?>
        
<div class="order-simple-confirm">
    <h1><? $APPLICATION->ShowTitle(false) ?></h1>
    <div class="important-description">
        <?=Loc::getMessage("SOA_ERROR_ORDER")?>
    </div>
    <div class="description">
        <?=Loc::getMessage("SOA_ERROR_ORDER_LOST", ["#ORDER_ID#" => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"])])?>
        <?=Loc::getMessage("SOA_ERROR_ORDER_LOST1")?>
    </div>
</div>

<? endif ?>
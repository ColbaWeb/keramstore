<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();


$arResult['ITEMS_BY_LETTERS'] = $arResult['FAVORITE_ITEMS'] = array ();

$ruChars = array ();
$ruBufferChars = GetMessage('CP_TPL_RU_LETTERS');

for ($i = 0; $i < mb_strlen($ruBufferChars); $i++)
{
    $ruChars[] = mb_substr($ruBufferChars, $i, 1);
}

foreach (array_merge(
        str_split(GetMessage('CP_TPL_EN_LETTERS')),
        array ("123", "~"),
        $ruChars
        ) as $letter)
{
    $arResult['ITEMS_BY_LETTERS'][$letter] = array ();
}


if (!empty($arResult['ITEMS']))
{
    foreach ($arResult['ITEMS'] as $arItem)
    {
        $firstLetter = mb_strtoupper(mb_substr($arItem['NAME'], 0, 1));
        if (is_numeric($firstLetter))
        {
            $arResult['ITEMS_BY_LETTERS']['123'][] = $arItem;
        }
        elseif (isset($arResult['ITEMS_BY_LETTERS'][$firstLetter]))
        {
            $arResult['ITEMS_BY_LETTERS'][$firstLetter][] = $arItem;
        }
        else
        {
            $arResult['ITEMS_BY_LETTERS']['~'][] = $arItem;
        }
        
        if (isset($arItem['UF_DEF']) && !empty($arItem['UF_DEF']))
        {
            $arResult['FAVORITE_ITEMS'][] = $arItem;
        }
    }
}
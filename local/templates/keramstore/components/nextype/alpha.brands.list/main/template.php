<?
use Nextype\Alpha\Layout;
use Bitrix\Main\Grid\Declension;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (!empty($arResult['ITEMS']))
{
    $itemDeclension = new Declension(GetMessage('CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_1'), GetMessage('CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_2'), GetMessage('CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_3'));
?>

<div class="brands-list-section">
    <div class="brands-search">
        <form class="custom-input-wrap" action="#">
            <div class="custom-input">
                <input type="text" class="inputtext" name="q" value="<?= $arResult['SEARCH_QUERY']?>" id="search-input-id">
                <label for="search-input-id"><?=GetMessage('CP_TPL_SEARCH_TITLE')?></label>
                <a href="javascript:void(0);" class="clear-btn"></a>
            </div>
            <button type="submit" class="btn btn-primary"><i class="icon icon-search-2-line-white"></i></button>
        </form>
    </div>
    
    <?
    
    if ($arResult['SEARCH_QUERY'])
    {
        ?>
        <div class="return-block">
            <a href="<?=$APPLICATION->GetCurDir()?>" class="btn btn-link"><i class="icon icon-arrow-right-text-button"></i><?=GetMessage('CP_TPL_BACK_TO_LIST')?></a>
        </div>
        
        <div class="brands-list">
                    <div class="brand-section">
                        <div class="brand-items-list row">
                            <?
                            foreach ($arResult['ITEMS'] as $arItem)
                            {
                                ?>
                                <div class="item-wrap col-4 col-md-3 col-lg-2">
                                    <a href="<?=$arItem['SECTION_PAGE_URL']?>" class="brand-item font-body-small">
                                        <?
                                        $source = isset($arItem['PICTURE']['src']) ? $arItem['PICTURE']['src'] : "";
                                        echo Layout\Assets::showBackgroundImage($source, array(
                                            "attributes" => array(
                                                "class=\"item-img\""

                                                )
                                        ));
                                        ?>
                                        <? if (!empty($arItem['COUNT_ELEMENTS']))
                                        {
                                            ?><div class="count "><?=$arItem['COUNT_ELEMENTS'] . " " . $itemDeclension->get($arItem['COUNT_ELEMENTS'])?></div><?
                                        }
                                        ?>
                                        <div class="item-title"><?=$arItem['NAME']?></div>
                                    </a>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
        </div>
        <?
    }
    else
    {
    
        if (!empty($arResult['FAVORITE_ITEMS']))
        {
            ?><div class="popular-brands font-large-button"><?
            foreach ($arResult['FAVORITE_ITEMS'] as $arItem)
            {
                ?><a href="<?=$arItem['SECTION_PAGE_URL']?>" class="item"><?=$arItem['NAME']?></a><?
            }
            ?></div><?
        }
        ?>

        <div class="letters-list font-large-button">
            <a href="javascript:void(0)" data-entity="letter-link" data-value="all" class="item active"><?=GetMessage('CP_TPL_ALL_BUTTON')?></a>
            <?
            foreach ($arResult['ITEMS_BY_LETTERS'] as $letter => $arItems)
            {
                if (!empty($arItems))
                {
                    ?><a href="javascript:void(0)" data-entity="letter-link" data-value="<?=$letter?>" class="item"><?=$letter?></a><?
                }
            }
            ?>
        </div>
        <div class="brands-list">
            <?

            foreach ($arResult['ITEMS_BY_LETTERS'] as $letter => $arItems)
            {
                if (!empty($arItems))
                {
                    ?>
                    <div class="brand-section" data-entity="letter-section" data-value="<?=$letter?>">
                        <div class="brand-title font-h2"><?=$letter?></div>
                        <div class="brand-items-list row">
                            <?
                            foreach ($arItems as $arItem)
                            {
                                ?>
                                <div class="item-wrap col-4 col-md-3">
                                    <a href="<?=$arItem['UF_LINK']?>" class="brand-item font-body-small">
                                        <?
                                        $source = isset($arItem['UF_IMAGE_URL']) ? $arItem['UF_IMAGE_URL']: "";
                                        echo Layout\Assets::showBackgroundImage($source, array(
                                            "attributes" => array(
                                                "class=\"item-img\""

                                                )
                                        ));
                                        ?>
                                        <? if (!empty($arItem['COUNT_ELEMENTS']))
                                        {
                                            ?><div class="count "><?=$arItem['COUNT_ELEMENTS'] . " " . $itemDeclension->get($arItem['COUNT_ELEMENTS'])?></div><?
                                        }
                                        ?>
                                        <div class="item-title"><?=$arItem['NAME']?></div>
                                    </a>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                    <?
                }
            }
        ?>
    </div>
    <?}?>
</div>
<?
}
else 
{
?>
<div class="brands-list-section">
    <div class="brands-search">
        <form class="custom-input-wrap" action="">
            <div class="custom-input">
                <input type="text" class="inputtext" name="q" value="<?= $arResult['SEARCH_QUERY']?>">
                <label for=""><?=GetMessage('CP_TPL_SEARCH_TITLE')?></label>
                <a href="javascript:void(0);" class="clear-btn"></a>
            </div>
            <button type="submit" class="btn btn-primary"><i class="icon icon-search-2-line-white"></i></button>
        </form>
    </div>
    <div class="not-found">
        <div class="font-h2 title"><?=GetMessage('CP_TPL_NOT_FOUND_TITLE')?></div>
        <div class="subtitle"><?=GetMessage('CP_TPL_NOT_FOUND_MESSAGE')?></div>
        <a href="<?=$APPLICATION->GetCurDir()?>" class="btn btn-link"><?=GetMessage('CP_TPL_GO_TO_LIST')?> <i class="icon icon-arrow-right-text-button"></i></a>
    </div>
</div>

<?
}
?>
<script>
$(document).ready(function () {
    $("body").on('click', '.brands-list-section [data-entity="letter-link"]', function () {
        $(this).parent().find('[data-entity="letter-link"]').removeClass('active');
        $(this).addClass('active');
        
        var value = $(this).data('value');
        if (value == "all")
        {
            $('.brands-list-section [data-entity="letter-section"], .brands-list-section [data-entity="letter-section"] .brand-title').removeClass('d-none');
        }
        else if (value != "")
        {
            $('.brands-list-section [data-entity="letter-section"]').addClass('d-none');
            $('.brands-list-section [data-entity="letter-section"][data-value="'+value+'"]').removeClass('d-none');
            $('.brands-list-section [data-entity="letter-section"][data-value="'+value+'"] .brand-title').addClass('d-none');
        }
        
        new window.Alpha.LazyForceLoad({
            parentNode: $('.brands-list-section')
        });
    });
});
</script>


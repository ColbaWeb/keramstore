<?php

$MESS['CP_TPL_RU_LETTERS'] = "АБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
$MESS['CP_TPL_EN_LETTERS'] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
$MESS['CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_1'] = "товар";
$MESS['CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_2'] = "товара";
$MESS['CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_3'] = "товаров";
$MESS['CP_TPL_ALL_BUTTON'] = "Все";
$MESS['CP_TPL_SEARCH_TITLE'] = "Поиск по товарам";
$MESS['CP_TPL_BACK_TO_LIST'] = "Назад к списку коллекций";
$MESS['CP_TPL_NOT_FOUND_TITLE'] = "К сожалению, по вашему запросу ничего не найдено";
$MESS['CP_TPL_NOT_FOUND_MESSAGE'] = "Этого товара нет или вы ошиблись в названии";
$MESS['CP_TPL_GO_TO_LIST'] = "Перейти к списку коллекций";
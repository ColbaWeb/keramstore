<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;
use Bitrix\Main\Grid\Declension;


/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult["BRAND_BLOCKS"]))
	return;

if (!empty($arResult["BRAND_BLOCKS"]))
{
    foreach ($arResult["BRAND_BLOCKS"] as $arItem)
    {
        if (!empty($arItem['LINK']))
        {
            ?><a href="<?=$arItem['LINK']?>" class="brand-item font-body-small"><?
        }
        else
        {
            ?><div class="brand-item font-body-small"><?
        }
        
        if (!empty($arItem['PREVIEW_PICTURE']))
        {
            echo Layout\Assets::showBackgroundImage($arItem['PREVIEW_PICTURE']['src'], array(
            "attributes" => array(
                "class=\"item-img\""
                )
            ));
        }
        
        if (!empty($arItem['COUNT_ELEMENTS']))
        {
            $itemDeclension = new Declension(Loc::getMessage('CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_1'), Loc::getMessage('CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_2'), Loc::getMessage('CP_TPL_CATALOG_BRAND_PRODUCT_COUNT_3'));
            ?><div class="count"><?=$arItem['COUNT_ELEMENTS'] . " " . $itemDeclension->get($arItem['COUNT_ELEMENTS'])?></div><?
        }
        
        ?>
        <div class="item-title"><?=$arItem['NAME']?></div>   
        <meta itemprop="brand" content="<?=$arItem['NAME']?>" />
        <?
        
        if (!empty($arItem['LINK']))
        {
            ?></a><?
        }
        else
        {
            ?></div><?
        }
    }
}
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$formId = "bx_subscribe_form" . $this->randString();
?>

<form id="<?=$formId?>" method="post" action="<?=SITE_DIR?>">
    <?
    if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
    {
        $APPLICATION->RestartBuffer();
    }
    ?>
    
    <? if (isset($arResult['MESSAGE']['CODE']) && $arResult['MESSAGE']['CODE'] == "message_success"): ?>
    <script>
    if (!!window.Alpha.Popups)
    {
        window.Alpha.Popups.open('<?=htmlspecialcharsbx(GetMessage('subscr_form_response_NOTE'))?>', '<?=htmlspecialcharsbx($arResult['MESSAGE']['TEXT'])?>');
    }
    </script>
    <? endif; ?>
    
    <div class="subscribe">
        <?=bitrix_sessid_post()?>
	<input type="hidden" name="sender_subscription" value="add">
        <input type="text" name="SENDER_SUBSCRIBE_EMAIL" value="<?=$arResult["EMAIL"]?>" placeholder="<?=GetMessage('subscr_form_email_title')?>" class="font-body-2">
            
        <button class="btn btn-primary btn-medium"><?=GetMessage('subscr_form_button')?> <i class="icon icon-envelop"></i></button>
    
    </div>
    
    <?
    if (count($arResult["RUBRICS"]) > 0)
    {
        ?>
    <div class="subscribe-rubrics">
        <? foreach ($arResult["RUBRICS"] as $itemID => $itemValue): ?>
            <div class="form-check">
                <input class="form-check-input" name="SENDER_SUBSCRIBE_RUB_ID[]" value="<?= $itemValue["ID"] ?>"<? if ($itemValue["CHECKED"]) echo " checked" ?> type="checkbox" id="SENDER_SUBSCRIBE_RUB_ID_<?= $itemValue["ID"] ?>">
                <label class="form-check-label" for="SENDER_SUBSCRIBE_RUB_ID_<?= $itemValue["ID"] ?>">
                    <?= htmlspecialcharsbx($itemValue["NAME"]) ?>
                </label>
            </div>
        <? endforeach; ?>
    </div>
        <?
    }
    ?>

    <? if ($arParams['USER_CONSENT'] == 'Y'): ?>
    <div class="userconsent-request">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:main.userconsent.request",
                    "",
                    array(
                        "ID" => $arParams["USER_CONSENT_ID"],
                        "IS_CHECKED" => $arParams["USER_CONSENT_IS_CHECKED"],
                        "AUTO_SAVE" => "Y",
                        "IS_LOADED" => $arParams["USER_CONSENT_IS_LOADED"],
                        "ORIGIN_ID" => "sender/sub",
                        "ORIGINATOR_ID" => "",
                        "REPLACE" => array(
                            "button_caption" => GetMessage("subscr_form_button"),
                            "fields" => array(GetMessage("subscr_form_email_title"))
                        ),
                    )
            );
            ?>
    </div>
    <? endif; ?>
    
    <?
    if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
        return;
    ?>
</form>
<div class="subtitle"><?=GetMessage('subscr_form_subtext')?></div>

<?
$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'nextype.alpha.sender.subscribe');
?>
<script>
        BX.loadScript('<?=$templateFolder?>/script.js', function () {

            new window.Alpha.SenderSubscribe({
                siteId: '<?=SITE_ID?>',
                formId: '<?=CUtil::JSEscape($formId)?>',
                ajaxPath: '<?=$templateFolder?>/ajax.php',
                signedParamsString: '<?= CUtil::JSEscape($signedParams) ?>'
            });

        });
</script>
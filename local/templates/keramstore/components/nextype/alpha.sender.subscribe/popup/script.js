(function (window){
	'use strict';

    if (window.Alpha.SenderSubscribe)
        return;
    
    window.Alpha.SenderSubscribe = function (params)
    {
        this.params = {};

        if (params.formId !== undefined && !!params.formId)
        {
            this.params = params;

            this.init();
        }
    };
    
    window.Alpha.SenderSubscribe.prototype = {
        
        init: function () {
            var _ = this;

            $("body").on('submit', '.modal-content #' + this.params.formId, function (event) {
                event.preventDefault();

                _.sendRequest();
                
                return false;
            });

        },

        sendRequest: function ()
        {
            var _ = this;
            
            BX.ajax({
                url: _.params.ajaxPath,
                method: 'POST',
                dataType: 'html',
                data: this.getData(),
                timeout: 30,
                emulateOnload: true,
                onsuccess: function (result) {
                    if (!result)
                        return;

                    var ob = BX.processHTML(result);

                    BX(_.params.formId).innerHTML = ob.HTML;
                    BX.ajax.processScripts(ob.SCRIPT, true);

                }
            });

        },
        
        getData: function ()
        {
            var data = this.getAllFormData();

            data.sessid = BX.bitrix_sessid();
            data.siteId = this.params.siteId;
            data.signedParamsString = this.params.signedParamsString;
            data.charset = BX.message('LANG_CHARSET');

            return data;
        },

        getAllFormData: function ()
        {
            var form = BX(this.params.formId),
                    prepared = BX.ajax.prepareForm(form),
                    i;

            for (i in prepared.data)
            {
                if (prepared.data.hasOwnProperty(i) && i == '')
                {
                    delete prepared.data[i];
                }
            }

            return !!prepared && prepared.data ? prepared.data : {};
        }
        
    };
    
})(window);

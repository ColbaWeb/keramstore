<?php
use Bitrix\Main\Text\Encoding;

define('DISABLED_REGIONS', true);
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
define('PUBLIC_AJAX_MODE', true);

$siteId = isset($_REQUEST['SITE_ID']) && is_string($_REQUEST['SITE_ID']) ? $_REQUEST['SITE_ID'] : '';
$siteId = substr(preg_replace('/[^a-z0-9_]/i', '', $siteId), 0, 2);
if (!empty($siteId) && is_string($siteId))
{
    define('SITE_ID', $siteId);
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/type/dictionary.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/errorcollection.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/lib/text/encoding.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
//$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);


Bitrix\Main\Localization\Loc::loadMessages(dirname(__FILE__).'/template.php');

$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
    $signedParamsString = $request->get('signedParamsString') ?: '';
    $params = $signer->unsign($signedParamsString, 'nextype.alpha.authorize');
    $params = unserialize(base64_decode($params));
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
    die();
}

global $APPLICATION;


$APPLICATION->IncludeComponent(
    'nextype:alpha.authorize',
    'popup',
    $params
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
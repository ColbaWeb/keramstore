<?php

$MESS['AUTH_TITLE'] = "Авторизация";
$MESS['AUTH_PHONE_TITLE'] = "Введите номер телефона";
$MESS['AUTH_USER_PROFILE_TITLE'] = "Завершение регистрации";
$MESS['AUTH_PHONE_CHECK_CODE_TITLE'] = "Подтвердите номер";
$MESS['AUTH_EMAIL_TITLE'] = "Введите ваш email";
$MESS['NEXT_BUTTON'] = "Отправить смс";
$MESS['FINISH_BUTTON'] = "Завершить регистрацию";
$MESS['AUTH_CHECK_CODE_PLACEHOLDER'] = "Код из SMS";
$MESS['AUTH_CHECK_CODE_REPEAT_WAITING'] = "Отправить повторно #SECONDS#с.";
$MESS['AUTH_CHECK_CODE_REPEAT_BUTTON'] = "Отправить повторно";
$MESS['AUTH_CHECK_CODE_REPEAT_TEXT'] = "Не пришел код?";

$MESS['AUTH_FIELD_EMAIL'] = "E-mail";
$MESS['AUTH_FIELD_PASSWORD'] = "Пароль";
$MESS['AUTH_FIELD_NAME'] = "Ваше имя";

$MESS['SUCCESS_AUTH_WAITING_FOR_REGIRECT'] = "Авторизация прошла успешно";
$MESS['SUCCESS_AUTH_WAITING_FOR_REGIRECT_2'] = "Вы успешно авторизованы, ожидайте перенаправления";

$MESS['AUTH_TEXT'] = "На него придет код по смс";
$MESS['AUTH_BACK_LINK'] = "Назад";
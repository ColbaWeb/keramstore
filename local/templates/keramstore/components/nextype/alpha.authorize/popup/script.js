(function (window){
	'use strict';

    if (window.Alpha.AuthorizeComponent)
        return;
    
    window.Alpha.AuthorizeComponent = function (parameters)
    {
        this.app = null;
        this.containerId = parameters.containerId || false;
        this.data = parameters.data || {
            error: false,
            values: {},
            order: {}
        };
        this.params = parameters.params || {};
        this.signedParamsString = parameters.signedParamsString || '';
        this.siteId = parameters.siteID || '';
        this.ajaxUrl = parameters.ajaxUrl || '';
        this.templateFolder = parameters.templateFolder || '';
        this.timeOut = false;

        if (!this.containerId)
            return false;
        
        this.init();
    };
    
    window.Alpha.AuthorizeComponent.prototype = {
        
        init: function ()
        {
            
            var _ = this;

            this.app = new Vue({
                el: _.containerId,
                data: _.data,

                mounted: function () {
                    var self = this;

                    $(this.$el).fadeTo(200, 1);

                    this.initJqueryComponents();
                },
                
                updated: function () {
                    
                    this.initJqueryComponents();
                    
                },

                methods: {
                    
                    initJqueryComponents: function () {

                        $('.custom-input input, .custom-input textarea').on('focusout', function () {
                            if ($(this).val() != '') $(this).addClass('filled')
                            else $(this).removeClass('filled');
                        });

                        $('.custom-input input, .custom-input textarea').each(function () {
                            if ($(this).val() != '') $(this).addClass('filled')
                            else $(this).removeClass('filled');
                        });
                        
                        var self = this;
                        
                        $(this.$el).find('[data-phonemask]').mask(BX.message('PHONE_NUMBER_MASK'), {
                            completed: function () {
                                if (this.data('phonemask') != "")
                                    self.values[this.data('phonemask')] = this.val();
                            }
                        });
                        
                    },
                    
                    doSendPhoneCode: function () {
                        var data = {
                            sessid: BX.bitrix_sessid(),
                            ajax: 'y',
                            SITE_ID: _.siteId,
                            signedParamsString: _.signedParamsString,
                            phone: this.values.phone,
                            action: this.result.action,
                            charset: BX.message('LANG_CHARSET'),
                            test: 'Привет',
                        };

                        _.sendAjaxRequest(data, function (response) {
                            this.app.doStartResendTimer();
                        });
                    },

                    doStartResendTimer: function () {

                        this.timeInterval = _.params.RESEND_DELAY;
                        
                        if (!!_.timeOut)
                            clearInterval(_.timeOut);
                        
                        _.timeOut = setInterval(function () {
                            _.app.timeInterval--;
                            
                            if (_.app.timeInterval <= 0) {
                                clearInterval(_.timeOut);
                            }
                        }, 1000);
                    },

                    doResendCode: function () {
                        var data = {
                            sessid: BX.bitrix_sessid(),
                            ajax: 'y',
                            SITE_ID: _.siteId,
                            signedParamsString: _.signedParamsString,
                            phone: this.values.phone,
                            action: this.result.action,
                            charset: BX.message('LANG_CHARSET')
                        };

                        this.timeInterval = _.params.RESEND_DELAY;

                        _.sendAjaxRequest(data, function (response) {
                            this.app.doStartResendTimer();
                        });
                    },

                    doActionCheckCode: function () {
                        var data = {
                            signedData: this.result.signedData,
                            code: this.values.code,
                            sessid: BX.bitrix_sessid(),
                            ajax: 'y',
                            SITE_ID: _.siteId,
                            signedParamsString: _.signedParamsString,
                            action: this.result.action,
                            charset: BX.message('LANG_CHARSET')
                        };

                        _.sendAjaxRequest(data, function (response) {
                            if (typeof this.app.result == 'object' && this.app.result.response.type == 'ok')
                            {
                                if (this.app.result.action == 'reload') {
                                    window.location.reload();
                                }

                            }
                        });
                    },

                    doSaveProfile: function () {
                        if (!!this.result.userId) {

                            var data = {
                                sessid: BX.bitrix_sessid(),
                                ajax: 'y',
                                SITE_ID: _.siteId,
                                signedParamsString: _.signedParamsString,
                                userProfile: {
                                    name: this.values.name,
                                    email: this.values.email
                                },
                                action: this.result.action,
                                charset: BX.message('LANG_CHARSET')

                            };

                            _.sendAjaxRequest(data, function (response) {
                                if (this.app.result.action == 'reload') {
                                    window.location.reload();
                                }
                            });
                        }
                    },

                },
                watch: {
                    'values.code': function (value)
                    {

                        if (typeof this.result == 'object' && !!this.result.codeLength) {
                            var length = value.length;
                            if (parseInt(value.length) == parseInt(this.result.codeLength)) {
                                this.doActionCheckCode();
                            }
                        }
                    }
                },
                computed: {

                    resendTimeoutMessage: function () {
                        return BX.message('AUTH_CHECK_CODE_REPEAT_WAITING').replace('#SECONDS#', this.timeInterval);
                    },

                }
            });

            this.app.doStartResendTimer();
        },

        sendAjaxRequest: function (data, callback) {
            callback = (callback) || false;
            var _ = this;

            $.ajax({
                type: 'post',
                url: this.ajaxUrl,
                data: data,
                success: function (response) {
                    if (typeof response == 'object') {
                        for (var key in response) {
                            _.app.result[key] = response[key];
                        }
                    }

                    if (typeof callback == 'function')
                        callback.call(_, response);
                },
                error: function (jqXHR, exception) {
                    _.showAjaxError(jqXHR, exception);
                }
            });
        },

        showAjaxError: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            console.log(msg);
        },
        
    };
    
})(window);

    

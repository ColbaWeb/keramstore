<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Nextype\Alpha\Layout;
?>

<form method="post" class="form-authorize" id="bx-nextype-authorize-popup" autocomplete="off" style="opacity: 0">
    
    
    
    <div v-if="result.method == 'phone'">
        <div v-if="result.action == 'getPhoneNumber'">
            
            <div class="modal-title font-h3 mb-4"><?=GetMessage('AUTH_TITLE')?></div>
            
            <div class="alert alert-info" v-if="result.response.type=='ok'" v-html="result.response.message"></div>
            
            <div class="custom-input-wrap">
                <div class="custom-input" v-bind:class="{error: result.response.type=='error'}">
                    <input type="text" pattern="\d*" required="" data-phonemask="phone" v-model="values.phone" />
                    <label><?=GetMessage('AUTH_PHONE_TITLE')?></label>
                </div>
                <div class="error-message" v-if="result.response.type=='error'" v-html="result.response.message"></div>
            </div>

            <button type="button" v-on:click.prevent="doSendPhoneCode" class="btn btn-primary btn-large"><?=GetMessage('NEXT_BUTTON')?></button>
        </div>
        <div v-if="result.action == 'checkPhoneCode'">
            
            <div class="modal-title font-h3 mb-4"><?=GetMessage('AUTH_PHONE_CHECK_CODE_TITLE')?></div>
            
            <div class="alert alert-info" v-if="result.response.type=='ok'" v-html="result.response.message"></div>
            
            <div class="custom-input-wrap">
                <div class="custom-input" v-bind:class="{error: result.response.type=='error'}">
                    <input type="text" pattern="\d*" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" required="required" v-model="values.code" />
                    <label><?=GetMessage('AUTH_CHECK_CODE_PLACEHOLDER')?></label>
                </div>
                <div class="error-message" v-if="result.response.type=='error'" v-html="result.response.message"></div>
            </div>
            
            <div class="d-flex font-body-small">
                <div class="resend" v-if="timeInterval >= 1" v-html="resendTimeoutMessage"></div>
                <div class="repeat" v-if="timeInterval < 1">
                    <span class="repeat-text"><?=GetMessage('AUTH_CHECK_CODE_REPEAT_TEXT')?></span>
                    <a href="#" v-on:click.prevent="doResendCode"><?=GetMessage('AUTH_CHECK_CODE_REPEAT_BUTTON')?></a>
                </div>
            </div>
            
        </div>
        <div class="form-authorize-wrap get-user-profile" v-if="result.action == 'getUserProfile'">

            <div class="modal-title font-h3 mb-4"><?=GetMessage('AUTH_USER_PROFILE_TITLE')?></div>

            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input required="required" type="text" v-model="values.name" id="bx-nextype-authorize-name">
                    <label for="bx-nextype-authorize-name"><?=GetMessage('AUTH_FIELD_NAME')?></label>
                </div>
            </div>
            
            <div class="custom-input-wrap">
                <div class="custom-input">
                    <input required="required" type="text" v-model="values.email" id="bx-nextype-authorize-email">
                    <label for="bx-nextype-authorize-email"><?=GetMessage('AUTH_FIELD_EMAIL')?></label>
                </div>
            </div>
            
            <button type="button" v-on:click.prevent="doSaveProfile" class="btn btn-primary btn-large"><?=GetMessage('FINISH_BUTTON')?></button>

        </div>
        <div v-if="result.action == 'reload'">

            <div class="modal-title font-h3 mb-4"><?=GetMessage('SUCCESS_AUTH_WAITING_FOR_REGIRECT')?></div>

            <div class="alert alert-success"><?=GetMessage('SUCCESS_AUTH_WAITING_FOR_REGIRECT_2')?></div>
        </div>
    </div>
</form>

<?
$signer = new Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'nextype.alpha.authorize');
$messages = Loc::loadLanguageFile(__FILE__);

?>

<script>
    BX.message(<?= CUtil::PhpToJSObject($messages) ?>);
            
    $(document).ready(function () {
        BX.loadScript('<?=$templateFolder?>/script.js', function () {
            new window.Alpha.AuthorizeComponent({
                    containerId: '#bx-nextype-authorize-popup',
                    data: <?= CUtil::PhpToJSObject(Array (
                        'result' => $arResult,
                        'values' => Array (
                            'code' => '',
                            'phone' => '',
                            'name' => '',
                            'email' => '',
                            'password' => '',
                        ),
                        'timeInterval' => $arParams['RESEND_DELAY']
                    )); ?>,
                    params: <?= CUtil::PhpToJSObject($arParams) ?>,
                    signedParamsString: '<?= CUtil::JSEscape($signedParams) ?>',
                    siteID: '<?= CUtil::JSEscape($component->getSiteId()) ?>',
                    ajaxUrl: '<?= CUtil::JSEscape($templateFolder . "/ajax.php") ?>',
                    templateFolder: '<?= CUtil::JSEscape($templateFolder) ?>'
                });
        });

        $('.scrollbar-inner').removeClass('ps--active-y');
    });
                    
</script>
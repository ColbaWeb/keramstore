<?
use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

use Nextype\Alpha\Options;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Zoovedov\Consts;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$containerId = "subscibe-coupon-" . randString(6);
?>

<div class="subscribe-coupon-container open" id="<?=$containerId?>">
    <?
        if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
        {
            $APPLICATION->RestartBuffer();
        }
        ?>
    <a href="javascript:void(0)" class="close-btn" data-entity="close"><i class="icon icon-close-big"></i></a>
    <div class="left">
        
        <? if (isset($arResult['CONFIRMED_SUBSCRIBE']) && $arResult['CONFIRMED_SUBSCRIBE'] == "Y"): ?>
        <div class="title"><?=GetMessage('CT_SC_CONFIRMED_SUBSCRIBE_TITLE')?></div>
        <div class="confirmed-text">
            <?=GetMessage('CT_SC_CONFIRMED_SUBSCRIBE_DESCRIPTION', array (
                '#VALUE#' => CurrencyFormat($arParams['COUPON_VALUE'], $arParams['CURRENCY_ID']),
                '#MIN_ORDER_PRICE#' => CurrencyFormat($arParams['MIN_ORDER_PRICE'], $arParams['CURRENCY_ID']),
            ))?>
        </div>
        <script>
        BX.onCustomEvent('onSuccessSubscribe', {
            email: '<?=$arResult['EMAIL']?>'
        });
        BX.setCookie('<?=CUtil::JSEscape($arParams['COOKIE_CODE'])?>', 'Y', {expires: 31536000, path: '/'});
        </script>
        <? else: ?>
        
        <div class="title">
            <?=GetMessage('CT_SC_TITLE', array ('#VALUE#' => CurrencyFormat($arParams['COUPON_VALUE'], $arParams['CURRENCY_ID'])))?>
        </div>
        <div class="sub-title"><?=GetMessage('CT_SC_SUB_TITLE')?></div>
        <form method="post">
            <div class="input-container<?=(isset($arResult['ERROR']) && !empty($arResult['ERROR'])) ? ' error' : ''?>">
                <input type="text" value="<?=$arResult['EMAIL']?>" name="email" placeholder="<?=GetMessage('CT_SC_INPUT_PLACEHOLDER')?>" />
                <button class="btn btn-primary"><span><?=GetMessage('CT_SC_BUTTON_TITLE')?></span></button>
            </div>
            <? if (isset($arResult['ERROR']) && !empty($arResult['ERROR'])): ?>
            <div class="error-message"><?=$arResult['ERROR']?></div>
            <? endif; ?>
            <input type="hidden" name="subscribeCouponSubmit" value="y" />
        </form>
        
        <div class="agreement-text"><?=GetMessage('CT_SC_AGREEMENT_TEXT')?></div>
        <? endif; ?>

    </div>
    <?
    if (strlen(Options\Base::getInstance(SITE_ID)->getValue('subscriptionCouponTemplateBackground')) > 0)
    {
        $background = \CFile::GetPath(Options\Base::getInstance(SITE_ID)->getValue('subscriptionCouponTemplateBackground'));
        if (!empty($background))
        {
            ?><div class="right" style="background-image: url('<?=$background?>')"></div><?
        }
    }
    ?>
    <?
        if (isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == "Y")
        {
            return;
        }
        ?>
</div>

<?
$signer = new Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'nextype.alpha.subscribe.coupon');
$messages = Loc::loadLanguageFile(__FILE__);

?>
<script>
        BX.message(<?= CUtil::PhpToJSObject($messages) ?>);
        BX.loadScript('<?=$templateFolder?>/script.js?<?=md5_file($_SERVER['DOCUMENT_ROOT'] . $templateFolder . "/script.js")?>', function () {

            new window.Alpha.SubscribeCoupon({
                siteId: '<?=SITE_ID?>',
                containerId: '<?=CUtil::JSEscape($containerId)?>',
                cookieCode: '<?=CUtil::JSEscape($arParams['COOKIE_CODE'])?>',
                ajaxPath: '<?=$templateFolder?>/ajax.php',
                signedParamsString: '<?= CUtil::JSEscape($signedParams) ?>'
            });

        });
</script>
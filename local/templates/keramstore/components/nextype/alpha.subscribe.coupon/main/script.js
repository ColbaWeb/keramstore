(function (window){
	'use strict';

    if (window.Alpha.SubscribeCoupon)
        return;
    
    window.Alpha.SubscribeCoupon = function (params)
    {
        this.params = {};

        if (params.containerId !== undefined && !!params.containerId)
        {
            this.params = params;
            this.init();
        }
    };
    
    window.Alpha.SubscribeCoupon.prototype = {
        
        init: function () {
            var _ = this;

            $("body").on('submit', '#' + this.params.containerId + ' form', function (event) {
                event.preventDefault();
                
                _.sendRequest();
                
                return false;
            });
            
            
            $("body").on('click touchstart', '#' +  this.params.containerId + ' [data-entity="close"]', function (event) {
                event.preventDefault();
                $('#' + _.params.containerId).removeClass('open');
                
                BX.setCookie(_.params.cookieCode, 'Y', {expires: 86400, path: '/'});
                
                return false;
            });

        },

        sendRequest: function ()
        {
            var _ = this;
            
            BX.ajax({
                url: _.params.ajaxPath,
                method: 'POST',
                dataType: 'html',
                data: this.getData(),
                timeout: 30,
                emulateOnload: true,
                onsuccess: function (result) {
                    if (!result)
                        return;

                    var ob = BX.processHTML(result);

                    BX(_.params.containerId).innerHTML = ob.HTML;
                    BX.ajax.processScripts(ob.SCRIPT, true);

                }
            });

        },
        
        getData: function ()
        {
            var data = this.getAllFormData();

            data.sessid = BX.bitrix_sessid();
            data.siteId = this.params.siteId;
            data.signedParamsString = this.params.signedParamsString;
            data.charset = BX.message('LANG_CHARSET');

            return data;
        },

        getAllFormData: function ()
        {
            var form = BX.findChild(BX(this.params.containerId), {
                        tag : "form"
                    }, true),
                    prepared = BX.ajax.prepareForm(form),
                    i;

            for (i in prepared.data)
            {
                if (prepared.data.hasOwnProperty(i) && i == '')
                {
                    delete prepared.data[i];
                }
            }

            return !!prepared && prepared.data ? prepared.data : {};
        }
        
    };
    
})(window);

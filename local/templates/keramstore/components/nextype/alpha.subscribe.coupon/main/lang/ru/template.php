<?php

$MESS["CT_SC_TITLE"] = "Получить купон на #VALUE#";
$MESS["CT_SC_SUB_TITLE"] = "за подписку на выгодные акции";
$MESS['CT_SC_INPUT_PLACEHOLDER'] = "Email";
$MESS['CT_SC_BUTTON_TITLE'] = "Подписаться";
$MESS['CT_SC_AGREEMENT_TEXT'] = "Нажимая на кнопку «Подписаться» вы соглашаетесь с условиями пользования и политикой конфиденциальности сайта";
$MESS['CT_SC_CONFIRMED_SUBSCRIBE_TITLE'] = "Спасибо за подписку!";
$MESS['CT_SC_CONFIRMED_SUBSCRIBE_DESCRIPTION'] = "На ваш электронный адрес был отправлен купон на #VALUE#. Вы можете активировать его в заказе на сумму не менее #MIN_ORDER_PRICE#.";
<?
$MESS ['CT_BCSF_FILTER_TITLE'] = "Подбор параметров";
$MESS ['CT_BCSF_FILTER_FROM'] = "От";
$MESS ['CT_BCSF_FILTER_TO'] = "До";
$MESS ['CT_BCSF_SET_FILTER'] = "Показать";
$MESS ['CT_BCSF_DEL_FILTER'] = "Сбросить фильтры";
$MESS ['CT_BCSF_FILTER_COUNT'] = "Найдено:";
$MESS ['CT_BCS_FILTER_NO_RESULT'] = "Нет результатов";
$MESS ['CT_BCSF_FILTER_SHOW'] = "Показать";
$MESS ['CT_BCSF_FILTER_ALL'] = "Все";
?>
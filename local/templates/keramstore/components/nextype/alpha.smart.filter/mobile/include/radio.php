<div class="scrollbar-inner">
    <div class="form-check">
        <? foreach ($arItem["VALUES"] as $val => $ar): ?>
        <div class="custom-radio ">
            <input type="radio" value="<? echo $ar["HTML_VALUE_ALT"] ?>" name="<? echo $ar["CONTROL_NAME_ALT"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> class="custom-control-input" <?=$ar["DISABLED"] ? 'disabled' : '' ?> id="<? echo $ar["CONTROL_ID"] . "_mobile" ?>">
            <label class="custom-control-label" for="<?=$ar["CONTROL_ID"] . "_mobile" ?>">
                <span><?= $ar["VALUE"]; ?></span>
                <? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
                <span class="count font-body-2"><? echo $ar["ELEMENT_COUNT"]; ?></span>
                <? endif; ?>
            </label>
        </div>
        <? endforeach; ?>

    </div>

</div>
<?
if (empty($arItem["VALUES"]["MIN"]["HTML_VALUE"]))
    $arItem["VALUES"]["MIN"]["HTML_VALUE"] = $arItem["VALUES"]["MIN"]["VALUE"];

if (empty($arItem["VALUES"]["MAX"]["HTML_VALUE"]))
    $arItem["VALUES"]["MAX"]["HTML_VALUE"] = $arItem["VALUES"]["MAX"]["VALUE"];

// change controll id
$arItem["VALUES"]["MIN"]["CONTROL_ID"] .= "_mobile";
$arItem["VALUES"]["MAX"]["CONTROL_ID"] .= "_mobile";

$controlID = "mobile_filter_price_slider_" . $key;
?>

<div class="accordion-head filter-block open">
    <a href="javascript:void(0);" class="accordeon-title filter-link">
        <span class="text font-body-2 bold"><?=$arItem["NAME"]?></span>
        <i class="icon icon-arrow-light-down"></i>
    </a>
    <div class="accordion-body sub">
        <div class="range-control">
            <div class="form-row">
                <div class="col"> 
                    <input class="min-price form-control font-body-2" type="number" id="<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>" name="<?=$arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>" value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>" size="5">
                </div>
                <div class="col"> 
                    <input class="max-price form-control font-body-2" type="number" name="<?=$arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>" id="<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>" value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>" size="5">
                </div>
            </div>
            <div class="form-row no-gutters">
                <div class="slider-container">
                    <input type="hidden" data-role="range-slider" id="<?=$controlID?>" />
                </div>
            </div>
        </div>
    </div>
</div>

<?
$precision = 2;
if (Bitrix\Main\Loader::includeModule("currency"))
{
    $res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
    $precision = $res['DECIMALS'];
}
?>

<script>
BX.ready(function () {
    BX.loadCSS('<?=SITE_TEMPLATE_PATH . '/vendor/bootstrap-slider/bootstrap-slider.min.css'?>');
    BX.loadScript('<?=SITE_TEMPLATE_PATH . '/vendor/bootstrap-slider/bootstrap-slider.min.js'?>', function () {
        $("#<?=$controlID?>").slider({
            min: <?=$arItem["VALUES"]["MIN"]["VALUE"]?>,
            max: <?=$arItem["VALUES"]["MAX"]["VALUE"]?>,
            value: [<?=floatval($arItem["VALUES"]["MIN"]["HTML_VALUE"])?>, <?=floatval($arItem["VALUES"]["MAX"]["HTML_VALUE"])?>],
            range: true,
            precision: <?=$precision?>
        }).on('change', function (event) {
            $("#<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>").val(event.value.newValue[0]);
            $("#<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>").val(event.value.newValue[1]);
        });

            $("#<?=$arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>").on('keyup', function () {
                var min = parseFloat($(this).val()), max = parseFloat($("#<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>").val());
                $("#<?=$controlID?>").slider('setValue', [min,max]);
            });

            $("#<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>").on('keyup', function () {
                var max = parseFloat($(this).val()), min = parseFloat($("#<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>").val());
                $("#<?=$controlID?>").slider('setValue', [min,max]);
            });
    });
});
</script>
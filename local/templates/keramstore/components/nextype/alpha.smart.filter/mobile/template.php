<?
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;
use Bitrix\Main\Grid\Declension;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$containerId = empty($arResult["FILTER_NAME"]) ? "mobile_filter_container" : $arResult["FILTER_NAME"] . "_mobile_filter_container";

$itemDeclension = new Declension(GetMessage('CT_BCS_FILTER_GOOD'), GetMessage('CT_BCS_FILTER_GOODS_2'), GetMessage('CT_BCS_FILTER_GOODS'));
?>

<div class="modal-container mobile hidden js-filter-mobile" id="<?=$containerId?>">
    <div class="modal-content-container">
        <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
            <i class="icon icon-close-big"></i>
        </a>
        <div class="scrollbar-inner">
            <div class="content-wrapper">
                <div class="filter-modal">

                    <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smart-filter">
                        
                        <?
                        foreach($arResult["HIDDEN"] as $arItem)
                        {
                            ?><input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" /><?
                        }

                        foreach ($arResult["ITEMS"] as $key => $arItem)//prices
                        {
                            $key = $arItem["ENCODED_ID"];
                            if (isset($arItem["PRICE"]))
                            {
                                if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                                    continue;

                                $step_num = 4;
                                $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
                                $prices = array();
                                if (Bitrix\Main\Loader::includeModule("currency"))
                                {
                                    for ($i = 0; $i < $step_num; $i++)
                                    {
                                        $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
                                    }
                                    $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
                                }
                                else
                                {
                                    $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
                                    for ($i = 0; $i < $step_num; $i++)
                                    {
                                        $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $precision, ".", "");
                                    }
                                    $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                                }

                                include(__DIR__ . "/include/price.php");
                            }
                        }

                        //not prices
                        foreach ($arResult["ITEMS"] as $key => $arItem)
                        {
                            if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
                                continue;

                            if ($arItem["DISPLAY_TYPE"] == "A" && ( $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
                                continue;

                            ?>
                            <div class="accordion-head filter-block<?=($arItem["DISPLAY_EXPANDED"]== "Y") ? ' open' : ''?>">
                                <a href="javascript:void(0);" class="accordeon-title filter-link">
                                    <span class="text font-body-2 bold"><?=$arItem["NAME"]?></span>
                                    <i class="icon icon-arrow-light-down"></i>
                                </a>
                                <div class="accordion-body sub">
                                    <?
                                    $arCur = current($arItem["VALUES"]);
                                    switch ($arItem["DISPLAY_TYPE"])
                                    {
                                        //region NUMBERS_WITH_SLIDER +
                                        case "A":
                                            include(__DIR__ . "/include/numbers_with_slider.php");
                                            break;

                                        //region NUMBERS +
                                        case "B":
                                            include(__DIR__ . "/include/numbers.php");
                                            break;

                                        //region CHECKBOXES_WITH_PICTURES +
                                        case "G":
                                            include(__DIR__ . "/include/checkboxes_with_pictures.php");
                                            break;

                                        case "H":
                                            include(__DIR__ . "/include/checkboxes_with_pictures_and_labels.php");
                                            break;

                                        //region DROPDOWN +
                                        case "P":
                                        case "R":
                                            include(__DIR__ . "/include/dropdown.php");
                                            break;

                                        //region RADIO_BUTTONS
                                        case "K":
                                            include(__DIR__ . "/include/radio.php");
                                            break;

                                        default:
                                            include(__DIR__ . "/include/checkbox.php");
                                            break;

                                    }

                                    ?>
                                </div>
                            </div>
                            <?
                        }

                        ?>
                        

                        <div class="buttons">
                            <button type="submit" data-entity="apply-button" value="y" name="set_filter" class="btn btn-mobile-filter-apply btn-apply btn-primary">
                                <span data-entity="label"><?=GetMessage("CT_BCSF_SET_FILTER")?></span>
                                <span class="font-body-small<?=intval($arResult["ELEMENT_COUNT"]) == 0 ? ' d-none' : ''?>" data-entity="count"><?=intval($arResult["ELEMENT_COUNT"]) . " " . $itemDeclension->get(intval($arResult["ELEMENT_COUNT"]))?></span>
                            </button>
                            <button type="submit" value="y" data-entity="delete-button" name="del_filter" class="btn btn-refresh">
                                <i class="icon icon-restore"></i>
                                <span><?=GetMessage("CT_BCSF_DEL_FILTER")?></span>
                            </button>
                        </div>

                    </form>
                    
                    

                </div>
            </div>
        </div>
    </div>
    <div class="close-area"></div>
</div>

<script>
    BX.ready(function () {
        
        BX.message({
            'CT_BCSF_SET_FILTER': '<?=GetMessageJS('CT_BCSF_SET_FILTER')?>',
            'CT_BCS_FILTER_NO_RESULT': '<?=GetMessageJS('CT_BCS_FILTER_NO_RESULT')?>',
            'CT_BCS_FILTER_GOOD': '<?=GetMessageJS('CT_BCS_FILTER_GOOD')?>',
            'CT_BCS_FILTER_GOODS_2': '<?=GetMessageJS('CT_BCS_FILTER_GOODS_2')?>',
            'CT_BCS_FILTER_GOODS': '<?=GetMessageJS('CT_BCS_FILTER_GOODS')?>'
        });
     
        new JAlphaMobileFilter(<?=CUtil::PhpToJSObject(Array (
                'CONTAINER_ID' => $containerId,
                'FORM_ACTION' => CUtil::JSEscape($arResult["FORM_ACTION"])
            ), false, true)?>);
    });
        
</script>
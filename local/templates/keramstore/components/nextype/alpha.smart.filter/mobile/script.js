(function(window){
	'use strict';

	if (window.JAlphaMobileFilter)
		return;
            
        window.JAlphaMobileFilter = function(arParams)
	{
            this.container = false;
            this.params = false;
            this.ajaxURL = "";
            this.form = null;
            this.timer = null;
            this.cacheKey = '';
            this.cache = [];
            this.instantRefresh = false;
            
            if (typeof arParams === 'object')
            {
                this.params = arParams;
                
                if (this.params.CONTAINER_ID === undefined || this.params.CONTAINER_ID == "")
                    return;
                
                this.ajaxURL = this.params.FORM_ACTION;

                BX.ready(BX.delegate(this.init, this));
            }
            
        }
        
        window.JAlphaMobileFilter.prototype = {
            
            init: function () {
                
                this.container = $("#" + this.params.CONTAINER_ID);
                var self = this;
                
                if (typeof(this.container) == 'object') {
                    
                    this.container.find('input[type="checkbox"], input[type="radio"], select').on('change', function () {
                        var input = $(this).get(0);

                        if(!!self.timer)
                        {
                            clearTimeout(self.timer);
                        }
                        
                        self.timer = setTimeout(BX.delegate(function () {
                            this.reload(input);
                        }, self), 500);
                        
                        
                    });
                    
                    this.container.find('input[type="text"], input[type="number"]').on('keyup', function () {
                        var input = $(this).get(0);

                        if(!!self.timer)
                        {
                            clearTimeout(self.timer);
                        }
                        
                        self.timer = setTimeout(BX.delegate(function () {
                            this.reload(input);
                        }, self), 1000);
                        
                    });
                    
                    this.container.find('[data-role="range-slider"]').on('change', function () {
                        var input = $(this).get(0);

                        if(!!self.timer)
                        {
                            clearTimeout(self.timer);
                        }
                        
                        self.timer = setTimeout(BX.delegate(function () {
                            this.reload(input);
                        }, self), 700);
                    });
                    
                    this.container.find('input, select').on('filterRefresh', function () {
                        var input = $(this).get(0);
                        clearTimeout(self.timer);
                        
                        self.instantRefresh = true;
                        self.reload(input);
                        
                    });

                }
                
            },
            
            reload: function (input) {
                
                input = (input) || false;
                
                if (this.cacheKey !== '')
                {
                        //Postprone backend query
                        if(!!this.timer)
                        {
                                clearTimeout(this.timer);
                        }
                        this.timer = setTimeout(() => {
                                this.reload(input);
                        }, 1000);
                        return;
                }
                
                this.cacheKey = '|';

                if (typeof(input) == 'object')
                {
                    var parentTopPosition = $(input).parents('.accordion-head').position().top;

                    if (!!parentTopPosition)
                    {
                        this.container.find(".finded-container").css('top', parentTopPosition + 'px');
                    }
                }
                
                this.form = BX.findParent(input, {'tag':'form'});
                if (this.form)
                {
                        var values = [];
                        values[0] = {name: 'ajax', value: 'y'};
                        this.gatherInputsValues(values, BX.findChildren(this.form, {'tag': new RegExp('^(input|select)$', 'i')}, true));
                        
                        for (var i = 0; i < values.length; i++)
                        {
                            if (!!values[i].name && values[i].name != "")
                                this.cacheKey += values[i].name + ':' + values[i].value + '|';
                        }

                        if (this.cache[this.cacheKey])
                        {
                                this.curFilterinput = input;
                                this.postHandler(this.cache[this.cacheKey], true);
                        }
                        else
                        {
                                this.curFilterinput = input;
                                BX.ajax.loadJSON(
                                        this.ajaxURL,
                                        this.values2post(values),
                                        BX.delegate(this.postHandler, this)
                                );
                        
                                
                        }
                }
            },
            
            values2post: function (values) {
                var post = [];
                var current = post;
                var i = 0;
                
                while(i < values.length)
                {
                        var p = values[i].name.indexOf('[');
                        if(p == -1)
                        {
                                current[values[i].name] = values[i].value;
                                current = post;
                                i++;
                        }
                        else
                        {
                                var name = values[i].name.substring(0, p);
                                var rest = values[i].name.substring(p+1);
                                if(!current[name])
                                        current[name] = [];

                                var pp = rest.indexOf(']');
                                if(pp == -1)
                                {
                                        //Error - not balanced brackets
                                        current = post;
                                        i++;
                                }
                                else if(pp == 0)
                                {
                                        //No index specified - so take the next integer
                                        current = current[name];
                                        values[i].name = '' + current.length;
                                }
                                else
                                {
                                        //Now index name becomes and name and we go deeper into the array
                                        current = current[name];
                                        values[i].name = rest.substring(0, pp) + rest.substring(pp+1);
                                }
                        }
                }
                return post;
            },
            
            gatherInputsValues: function (values, elements) {
                if(elements)
                {
                        for(var i = 0; i < elements.length; i++)
                        {
                                var el = elements[i];
                                if (el.disabled || !el.type || !el.name)
                                        continue;

                                switch(el.type.toLowerCase())
                                {
                                        case 'text':
                                        case 'textarea':
                                        case 'password':
                                        case 'hidden':
                                        case 'number':
                                        case 'phone':
                                        case 'email':
                                        case 'select-one':
                                                if(el.value.length)
                                                        values[values.length] = {name : el.name, value : el.value};
                                                break;
                                        case 'radio':
                                        case 'checkbox':
                                                if(el.checked)
                                                        values[values.length] = {name : el.name, value : el.value};
                                                break;
                                        case 'select-multiple':
                                                for (var j = 0; j < el.options.length; j++)
                                                {
                                                        if (el.options[j].selected)
                                                                values[values.length] = {name : el.name, value : el.options[j].value};
                                                }
                                                break;
                                        default:
                                                break;
                                }
                        }
                }
            },
            
            updateItem: function (PID, arItem)
            {
                if (arItem.PROPERTY_TYPE === 'N' || arItem.PRICE)
                {
                    if (arItem.VALUES.MIN && arItem.VALUES.MIN.FILTERED_VALUE)
                    {
                        $("#" + arItem.VALUES.MIN.CONTROL_ID + "_mobile").val(arItem.VALUES.MIN.FILTERED_VALUE);
                    }
                    
                    if (arItem.VALUES.MAX && arItem.VALUES.MAX.FILTERED_VALUE)
                    {
                        $("#" + arItem.VALUES.MAX.CONTROL_ID + "_mobile").val(arItem.VALUES.MAX.FILTERED_VALUE);
                    }
                    
                    if (arItem.PRICE && arItem.ENCODED_ID && arItem.VALUES.MIN.FILTERED_VALUE && arItem.VALUES.MAX.FILTERED_VALUE)
                    {
                        var slider = $("#filter_price_slider_" + arItem.ENCODED_ID);
                        if (slider.length > 0) {
                            slider.slider('setValue', [arItem.VALUES.MIN.FILTERED_VALUE,arItem.VALUES.MAX.FILTERED_VALUE]);
                        }
                    }
                }
                else if (arItem.VALUES)
                {
                        for (var i in arItem.VALUES)
                        {
                                if (arItem.VALUES.hasOwnProperty(i))
                                {
                                        var value = arItem.VALUES[i];
                                        var control = $("#" + value.CONTROL_ID + "_mobile");

                                        if (control.length > 0)
                                        {
                                            if (value.DISABLED)
                                            {
                                                control.attr('disabled', 'disabled');
                                            }
                                            else
                                            {
                                                control.removeAttr('disabled');
                                            }
                                            
                                            var counter = control.parent().find('[data-role="count"]');
                                            if (counter.length > 0) {
                                                counter.text(value.ELEMENT_COUNT);
                                            }
                                            
                                        }
                                }
                        }
                }
            },
            
            postHandler: function (result, fromCache) {
                var obButton = this.container.find('[data-entity="apply-button"]');
                
                if (!!result && !!result.ITEMS)
                {
                        for(var PID in result.ITEMS)
                        {
                                if (result.ITEMS.hasOwnProperty(PID))
                                {
                                        this.updateItem(PID, result.ITEMS[PID]);
                                }
                        }
                        
                        if (obButton.length > 0)
                        {
                            if (parseInt(result.ELEMENT_COUNT) > 0)
                            {
                                var countText = result.ELEMENT_COUNT;
                                if (parseInt(result.ELEMENT_COUNT) == 1)
                                    countText += " " + BX.message('CT_BCS_FILTER_GOOD');
                                else if (parseInt(result.ELEMENT_COUNT) > 1 && parseInt(result.ELEMENT_COUNT) < 5)
                                    countText += " " + BX.message('CT_BCS_FILTER_GOODS_2');
                                else
                                    countText += " " + BX.message('CT_BCS_FILTER_GOODS');
                                
                                obButton.find('[data-entity="count"]').removeClass('d-none').text(countText);
                                obButton.find('[data-entity="label"]').text(BX.message('CT_BCSF_SET_FILTER'));
                            }
                            else
                            {
                                obButton.find('[data-entity="count"]').addClass('d-none');
                                obButton.find('[data-entity="label"]').text(BX.message('CT_BCS_FILTER_NO_RESULT'));
                            }
                        }
                        
                        console.log(result);

                }

                if (!fromCache && this.cacheKey !== '')
                {
                        this.cache[this.cacheKey] = result;
                }
                this.cacheKey = '';
            }

        }
})(window);
<? $checkedItemExist = false; ?>
<div class="accordion-head filter-block inner-block" data-role="horizontal-filter-block">
    <a href="javascript:void(0);" class="accordeon-title filter-link">
        <span class="text font-large-button"><?
            $arCheckedBuf = array ();
            foreach ($arItem["VALUES"] as $val => $ar)
            {
                if ($ar["CHECKED"])
                {
                    $arCheckedBuf[] = $ar["VALUE"];
                    $checkedItemExist = true;
                }
            }
            if (!$checkedItemExist)
            {
                echo GetMessage("CT_BCSF_FILTER_ALL");
            }
            else
            {
                echo implode(", ", $arCheckedBuf);
            }
            ?></span>
        <i class="icon icon-arrow-light-down"></i>
    </a>
    <div class="accordion-body sub inner-body">
        <div class="scrollbar-inner">
            <div class="form-check">
                <div class="custom-checkbox ">
                        <input type="checkbox" value="" name="<?= $arCur["CONTROL_NAME_ALT"] ?>" id="<? echo "all_" . $arCur["CONTROL_ID"] . "_mobile" ?>" class="custom-control-input">
                        <label class="custom-control-label" for="<? echo "all_" . $arCur["CONTROL_ID"] . "_mobile" ?>">
                            <span><?=GetMessage("CT_BCSF_FILTER_ALL")?></span>
                        </label>
                    </div>
                
                <?
                foreach ($arItem["VALUES"] as $val => $ar)
                {
                    ?>
                    <div class="custom-checkbox ">
                        <input type="checkbox" value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> class="custom-control-input" id="<?= $ar["CONTROL_ID"] ?>">
                        <label class="custom-control-label" for="<?= $ar["CONTROL_ID"] ?>">
                            <span><?= $ar["VALUE"] ?></span>
                            <? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
                            <span class="count font-body-2"><? echo $ar["ELEMENT_COUNT"]; ?></span>
                            <? endif; ?>
                        </label>
                    </div>
                    <?
                }
                ?>
            </div>

        </div>
    </div>
</div>

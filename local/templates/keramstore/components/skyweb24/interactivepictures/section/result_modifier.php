<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\Json;
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;

if (empty($arResult['SETTINGS']) || !($settings = Json::decode($arResult['SETTINGS'])))
    return false;

if (!isset($settings['image']['url']) || empty($settings['image']['url']))
    return false;

$arResult['IMAGE'] = array (
    'WIDTH' => $settings['general']['naturalWidth'],
    'HEIGHT' => $settings['general']['naturalHeight'],
    'SRC' => $settings['image']['url']
);

if (isset($settings['spots']) && !empty($settings['spots']))
{
    foreach ($settings['spots'] as $spot)
    {
        if (!isset($spot['tooltip_content']['content_template_element_id']) || empty($spot['tooltip_content']['content_template_element_id']))
            continue;
        
        if ($obItem = CIBlockElement::GetByID($spot['tooltip_content']['content_template_element_id'])->GetNextElement())
        {
            $item = $obItem->GetFields();
            $item['PROPERTIES'] = $obItem->GetProperties();
        }

        if (!$item)
            continue;
        
        $fields = array (
            'COORDS' => array (
                'LEFT' => $spot['x'],
                'TOP' => $spot['y'],
            )
        );
        
        if (!empty($item['PREVIEW_PICTURE']) && ($preview = CFile::ResizeImageGet($item['PREVIEW_PICTURE'], array ('width' => 60, 'height' => 60), BX_RESIZE_IMAGE_PROPORTIONAL_ALT)))
        {
            $item['PREVIEW_PICTURE'] = array ('SRC' => $preview['src']);
        }
        
        $item['PRICE'] = CCatalogProduct::GetOptimalPrice($item['ID'], 1, $USER->GetUserGroupArray());
        
        if (!empty($item['IBLOCK_SECTION_ID']))
        {
            $item['SECTION'] = CIBlockSection::GetByID($item['IBLOCK_SECTION_ID'])->GetNext();
        }
        
        $arResult['ITEMS'][] = array_merge($item, $fields);
    }
}

/*echo "<pre>";
    print_r($settings);
    echo "</pre>";*/
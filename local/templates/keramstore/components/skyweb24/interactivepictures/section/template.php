<?
use Bitrix\Main\Web\Json;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Nextype\Alpha\Reviews;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (empty($arResult['IMAGE']))
    return;

$containerId = "interactive_image_" . randString();
?>

<div class="interactive-image-section<?=$arParams['VIEW_MODE'] == "HORIZONTAL" ? ' horizontal' : ''?>">
    <div class="interactive-image" data-orig-width="<?=$arResult['IMAGE']['WIDTH']?>" data-orig-height="<?=$arResult['IMAGE']['HEIGHT']?>" id="<?=$containerId?>" data-src="<?=$arResult['IMAGE']['SRC']?>">
        <?=Layout\Assets::showSpinner()?>

        <?
        if (!empty($arResult['ITEMS']))
        {
            foreach ($arResult['ITEMS'] as $item)
            {
                ?>
                <div class="fake-spot" style="left:<?=$item['COORDS']['LEFT']?>%;left:calc(<?=$item['COORDS']['LEFT']?>% - 35px);top:<?=$item['COORDS']['TOP']?>%;top:calc(<?=$item['COORDS']['TOP']?>% - 35px);"></div>
                <div class="spot" style="left:<?=$item['COORDS']['LEFT']?>%;left:calc(<?=$item['COORDS']['LEFT']?>% - 35px);top:<?=$item['COORDS']['TOP']?>%;top:calc(<?=$item['COORDS']['TOP']?>% - 35px);">
                    <div class="point"></div>

                    <div class="tooltip-product">
                        <div class="d-flex">
                            <?
                            if (!empty($item['PREVIEW_PICTURE']['SRC']))
                            {
                                ?>
                                <div class="picture">
                                    <a href="<?=$item['DETAIL_PAGE_URL']?>" class="img" style="background-image: url('<?=$item['PREVIEW_PICTURE']['SRC']?>')"></a>

                                    <?
                                    if (Options\Base::getInstance(SITE_ID)->getValue('catalogReviews'))
                                    {
                                        $reviewsSummary = Reviews\Base::getInstance()->getSummaryByProduct($item['ID']);

                                        if (isset($reviewsSummary['avg']) && $reviewsSummary['avg'] > 0)
                                        {
                                            ?><div class="rating"><i class="icon icon-star-12"></i> <?= number_format($reviewsSummary['avg'], 1, ",", " ")?></div><?
                                        }
                                    }
                                    ?>
                                </div>
                                <?
                            }
                            ?>
                            <div class="info">
                                <a href="<?=$item['DETAIL_PAGE_URL']?>" class="name"><?=$item['NAME']?></a>
                                <div class="price-container">
                                    
                                    <?
                                    if ($item['PRICE']['RESULT_PRICE']['DISCOUNT_PRICE'] > 0)
                                    {
                                        ?><div class="price"><?=CurrencyFormat($item['PRICE']['RESULT_PRICE']['DISCOUNT_PRICE'], $item['PRICE']['RESULT_PRICE']['CURRENCY'])?></div><?
                                    }

                                    if ($item['PRICE']['RESULT_PRICE']['DISCOUNT_PRICE'] > 0 && $item['PRICE']['RESULT_PRICE']['DISCOUNT_PRICE'] < $item['PRICE']['RESULT_PRICE']['BASE_PRICE'])
                                    {
                                        ?><div class="old-price"><?=CurrencyFormat($item['PRICE']['RESULT_PRICE']['BASE_PRICE'], $item['PRICE']['RESULT_PRICE']['CURRENCY'])?></div><?
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>

                        <?
                        if (!empty($item['SECTION']))
                        {
                            ?><a href="<?=$item['SECTION']['SECTION_PAGE_URL']?>" class="link"><?=GetMessage('CP_TPL_LINK_TO_SECTION')?><i class="icon icon-arrow-right-text-button-white"></i></a><?
                        }
                        ?>
                    </div>
                </div>

                <?
            }
        }
        ?>
    </div>
    
    <script>
        
    <? if ($arParams['VIEW_MODE'] == "VERTICAL"): ?>  
    var container = document.querySelector("[data-entity='<?=$arParams['ITEMS_CONTAINER_NAME']?>']");
    container.classList.add('float-grid');
    <? endif; ?>
        
    $(document).ready(function () {
        var callbackInitInteractiveImage = function () {

            $("#<?=$containerId?>").each(function () {
                var width = parseFloat($(this).data('orig-width')),
                    height = parseFloat($(this).data('orig-height')),
                    containerWidth = parseFloat($(this).width()),
                    containerHeight = parseInt((containerWidth / width) * height),
                    self = $(this);

                $(this).css('height', containerHeight);
                
                <? if ($arParams['VIEW_MODE'] == "HORIZONTAL"): ?>
                $(this).css('top', '-' + parseInt((containerHeight - parseFloat($(this).parent().height())) / 2) + 'px');
                <? endif; ?>

                $(this).Lazy({
                    effect: "fadeIn",
                    effectTime: 250,
                    threshold: 0,

                    afterLoad: function(element) {
                        element.find('.swiper-lazy-preloader').remove();
                        element.addClass('loaded');

                        self.find('.fake-spot').each(function () {
                            var spotOffset = $(this).position(),
                                spot = $(this).next();

                            if (!!spotOffset && !!spot)
                            {
                                var tooltip = spot.find('.tooltip-product'),
                                    tooltipWidth = parseFloat(tooltip.outerWidth(true)),
                                    tooltipHeight = parseFloat(tooltip.outerHeight(true));

                                // check left right position
                                if (spotOffset.left > tooltipWidth)
                                {
                                    tooltip.addClass('left');
                                }
                                else if (containerWidth - spotOffset.left > tooltipWidth)
                                {
                                    tooltip.addClass('right');
                                }

                                // check top position
                                if (spotOffset.top > tooltipHeight && containerHeight - spotOffset.top < tooltipHeight)
                                {
                                    tooltip.addClass('top');
                                }
                                else if (spotOffset.top < tooltipHeight && containerHeight - spotOffset.top > tooltipHeight)
                                {
                                    tooltip.addClass('bottom');
                                }
                            }
                        });
                    }
                });


            });
        }

        callbackInitInteractiveImage();
        $(window).resize(callbackInitInteractiveImage);
    });
    </script>
</div>


<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;
use Bitrix\Main\Application as BitrixApplication;

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

Application::getInstance()->epilog();
?>
<? if (!Layout\Base::getInstance()->isHideHeaderAndFooter()): ?>
<? if (Layout\Base::getInstance()->isHomepage()): ?>
    </div>
</div>
<? elseif (!Layout\Base::getInstance()->isCustomPage()): ?>
    <? if (Layout\Base::getInstance()->isShowRightMenu()): ?>
        </div>
        <div class="col-auto page-right-menu">
            <? Layout\Partial::getInstance()->render('right_menu.php', array (), false, false) ?>
        </div>
    </div>
    <? endif; ?>
        </div>
    </div>
</div>
<? endif; ?>

<? Layout\Ajax::getInstance(__DIR__)->renderLazyLoad('footer', array (), false, false) ?>

<? Layout\Ajax::getInstance(__DIR__)->render('mobile/sidebar_menu.php', array (), false, false) ?>

<?
if (Options\Base::getInstance(SITE_ID)->getValue('subscriptionCouponActive'))
{
    Layout\Ajax::getInstance()->render('footer/subscription_coupon.php', array (), false, false);
}
?>

<? endif; ?>

<? if (Layout\Base::getInstance()->isShowShortHeaderAndFooter())
{
    Layout\Partial::getInstance(__DIR__)->render('footer/short.php', array (), false, false);
}
?>

<?
if (Options\Base::getInstance(SITE_ID)->getValue('publicOptions'))
{
    if (strlen(BitrixApplication::getInstance()->getContext()->getRequest()->get('config_id')) > 0)
        Layout\Partial::getInstance()->render('ajax/public_options.php', array (), false, false);
    else
        Layout\Ajax::getInstance()->render('ajax/public_options.php', array (), false, false);
}
?>

<? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/regions_modal.php'); ?>

<script>
BX.ready(function(){
    new window.Alpha.Application({
        siteId: '<?= SITE_ID ?>',
        currentDir: '<?=BitrixApplication::getInstance()->getContext()->getRequest()->getRequestedPageDirectory()?>'
    });
});
</script>
<script>
    $( document ).ready(function() {
        var images = $("img");
        images.each(function () {
            var alt = $(this).attr("alt");
            if(typeof alt != "undefined"){
                if(alt.length > 0) {
                } else {
                    var title = $("h1").html();
                    console.log(title);
                    $(this).attr("alt", title);
                }
            } else {
                $(this).attr("alt", title);
            }
        });
    });
</script>
<?
Layout\Base::getInstance()->includeAnalytics();
?>
</body>
</html>

<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeTemplateLangFile(__FILE__);

$arTemplate = Array(
	"NAME"          => GetMessage("TEMPLATE_NAME"), 
	"DESCRIPTION"   => GetMessage("TEMPLATE_DESCRIPTION"),
        "EDITOR_STYLES" => array (
            '/bitrix/wizards/nextype/alpha/site/templates/alpha/vendor/bootstrap/bootstrap.grids.css',
            '/bitrix/wizards/nextype/alpha/site/templates/alpha/editor_styles.css'
        )
);

?>
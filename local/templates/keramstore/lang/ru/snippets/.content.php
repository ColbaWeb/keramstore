<?php

$MESS['SNIPPET_STRUCTURE_TWO_COLUMNS'] = "Структура: две колонки";
$MESS['SNIPPET_STRUCTURE_THREE_COLUMNS'] = "Структура: три колонки";
$MESS['SNIPPET_STRUCTURE_FOUR_COLUMNS'] = "Структура: четыре колонки";
$MESS['SNIPPET_STRUCTURE_FIVE_COLUMNS'] = "Структура: пять колонок";
$MESS['SNIPPET_NOTE'] = "Сноска";
$MESS['SNIPPET_IMAGES_TWO'] = "Два изображения";
$MESS['SNIPPET_INFORMATION'] = "Информационная вставка";
$MESS['SNIPPET_QUOTE'] = "Цитата";
$MESS['SNIPPET_LEADTEXT'] = "Лидирующий параграф";
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Options;

global $APPLICATION;

?>

<div class="header type2">
    <div class="top-header-wrapper">
        <div class="container">
            <div class="top-header">

                <div class="small-menu menu">
                    <? Layout\Ajax::getInstance()->render('header/menu.php') ?>
                </div>

                <div class="location">
                    
                    <? if (Regions\Base::getInstance()->isActive())
                        {
                            Layout\Ajax::getInstance()->render('header/regions.php');
                        }
                        else
                        {
                            ?>
                            <span class="font-body-small">
                            <?=Options\Base::getInstance()->getValue('address')?>
                            </span>
                            <?
                        }
                        ?>

                </div>

                <div class="phone">
                    
                    <? Layout\Partial::getInstance()->render('header/phone.php') ?>

                </div>

<!--                <div class="sign-in">-->
<!--                    -->
<!--                    --><?// Layout\Ajax::getInstance()->render('header/signin.php') ?>
<!---->
<!--                </div>-->

            </div>
        </div>
    </div>
    <div class="main-header-wrapper">
        <div class="container">
            <div class="main-header">
                <div class="menu-btn"> 
                    <a href="javascript:void(0)" data-main-menu-btn="" class="icon icon-menu"></a> 
                </div>

                <div class="logo"> 
                    
                    <? Layout\Partial::getInstance()->render('header/logo.php') ?>
                    
                    <div class="slogan font-body-small hidden-xl"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/header/slogan.php'); ?></div>
                </div>

                <div class="menu-button">
<!--                    <a href="javascript:void(0)" class="btn btn-primary">
                        <div class="hamburger hamburger-button">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                        <span><?/*=Loc::getMessage('NT_ALPHA_HEADER_CATALOG_MENU_TITLE')*/?></span>
                    </a>-->
                    <a href="/catalog" class="btn btn-primary">
                        <div class="hamburger hamburger-button">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                        <span><?=Loc::getMessage('NT_ALPHA_HEADER_CATALOG_MENU_TITLE')?></span>
                    </a>
                </div>
                
                <? Layout\Partial::getInstance()->render('header/search.php') ?>

                <div class="icons">
                    
                    <? Layout\Partial::getInstance()->render('header/search_icon.php') ?>
                    
                    <? Layout\Partial::getInstance()->render('header/wishlist.php') ?>
                        
                    <? Layout\Ajax::getInstance()->render('header/basket.php') ?>
                    
                </div>
            </div>
            <? $APPLICATION->IncludeComponent(
                "sfx:alphabet.list",
                ".default",
                Array(
                ),
                false
            );?>
            <div class="momilealf">
                <? $APPLICATION->IncludeComponent(
                    "sfx:alphabet.list",
                    ".default",
                    Array(
                    ),
                    false
                );?>
            </div>
            <div class="catalog menu">
                <? Layout\Ajax::getInstance()->render('header/menucat.php') ?>
            </div>
        </div>

    </div>
    
    <? Layout\Ajax::getInstance()->render('header/catalog_menu_2.php') ?>
    
</div>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
?>

<div class="short-header">
    <div class="container order-simple-container">
        <div class="d-flex justify-content-between align-items-center">
            <div class="logo"> 
                <? Layout\Partial::getInstance()->render('header/logo.php') ?>
            </div>
            <div class="phone">
                <? Layout\Partial::getInstance()->render('header/phone.php') ?>
            </div>
        </div>
    </div>
</div>
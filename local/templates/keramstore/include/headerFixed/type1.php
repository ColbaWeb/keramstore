<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

?>

<div class="header header-fixed open-header">

    <div class="main-header-wrapper">
        <div class="container">
            <div class="main-header">
                <div class="menu-btn"> 
                    <a href="javascript:void(0)" data-main-menu-btn="" class="icon icon-menu"></a> 
                </div>

                <div class="logo"> 
                    <? Layout\Partial::getInstance()->render('header/logo.php') ?>
                </div>

                <div class="menu-button">
                    <a href="/catalog" class="btn btn-primary">
                        <div class="hamburger hamburger-button">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                        <span><?=Loc::getMessage('NT_ALPHA_HEADER_CATALOG_MENU_TITLE')?></span>
                    </a>
                    <a href="<?= SITE_DIR ?>catalog/" class="btn btn-primary">
                        <span><?=Loc::getMessage('NT_ALPHA_HEADER_PRODUCTS_MENU_TITLE')?></span>
                    </a>
                </div>

                <div class="small-menu menu" id="fixed-small-menu">
                    
                    <? Layout\Ajax::getInstance()->render('headerFixed/menu.php') ?>
                    
                </div>
                <? Layout\Partial::getInstance()->render('header/phone_fix.php') ?>
                <div class="icons">
                    <a href="javascript:void(0)" class="search" data-entity="button-search-modal">
                        <i class="icon icon-search-2-line"></i>
                        <span><?=Loc::getMessage('NT_ALPHA_HEADER_SEARCH_MENU_TITLE')?></span>
                    </a>
                    
                    <? Layout\Partial::getInstance()->render('headerFixed/wishlist.php') ?>
                        
                    <? Layout\Ajax::getInstance()->render('headerFixed/basket.php') ?>
                    
                    
                </div>
            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {
            if ($('.header').hasClass('type3')) {
                $('.header').siblings('.header-fixed').find('.menu-button').addClass('type3');
            }
        });
    </script>
</div>

<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
?>

<div class="short-footer">
    <div class="container order-simple-container">
        <div class="d-flex justify-content-between align-items-center">
            <div class="logo-and-phone d-flex align-items-center">
                <div class="logo">
                    <? Layout\Partial::getInstance()->render('header/logo.php') ?>
                </div>
                <div class="phone">
                    <div class="slogan font-body-small hidden-xl"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/header/slogan.php'); ?></div>
                </div>
            </div>
            
            <? Layout\Partial::getInstance()->render('footer/social_links_black.php') ?>
            
        </div>
    </div>
</div>

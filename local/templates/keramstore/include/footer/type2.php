<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Options;

global $APPLICATION;

?>
<footer class="type-2 <?=Options\Base::getInstance()->getValue('footerBackgroundColor') == "white" ? "white" : "black"?>">
    <div class="container">
        <div class="footer">
            <div class="footer-row">
                <div class="logo-block">
                    <div class="logo">
                        <?
                        if (Options\Base::getInstance()->getValue('footerBackgroundColor') == "white")
                            Layout\Partial::getInstance()->render('footer/logo.php');
                        else
                            Layout\Partial::getInstance()->render('footer/logo_white.php');
                        ?>
                    </div>
                    <div class="subtitle"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/slogan.php'); ?></div>
                </div>
                
                <? Layout\Ajax::getInstance()->render('footer/menu.php') ?>
                
            </div>
            <div class="footer-row">
                <div class="subtitle"><? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/copyright.php'); ?></div>
                
                <? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/policy_link.php'); ?>
                
                <? Layout\Partial::getInstance()->render('footer/social_links.php') ?>

            </div>
        </div>
    </div>
</footer>
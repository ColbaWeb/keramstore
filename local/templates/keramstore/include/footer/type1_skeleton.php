<style>

    .skeleton-footer.type-1 {
        margin-top: 80px;
    }

    .skeleton-footer.type-1 .row{
        width: 100%;
    }
    .skeleton-footer.type-1 .container {
        display: flex;
    }
    .skeleton-footer.type-1 .skeleton-footer-logo {
        width: 140px;
        height: 48px;
        margin-bottom: 8px;
    }
    .skeleton-footer.type-1 .skeleton-footer-subtitle {
        margin-bottom: 24px;
        height: 0;
    }
    .skeleton-footer.type-1 .skeleton-footer-contact-item {
        height: 24px;
        width: 200px;
    }
    .skeleton-footer.type-1 .skeleton-footer-contact-item:not(:last-child) { 
        margin-bottom: 8px;
    }

    .skeleton-footer.type-1 .col-12 {
        padding: 28px 0;
    }
    .skeleton-footer.type-1 .col-12:first-child {
        padding-left: 15px;
    }
    .skeleton-footer.type-1 .col-12:last-child {
        padding-left: 30px;
        padding-right: 15px;
    }

    .skeleton-footer.type-1 .skeleton-footer-catalog-items {
        display: flex;
        flex-wrap: wrap;
        padding-bottom: 16px;
    }

    .skeleton-footer.type-1 .skeleton-footer-catalog-item {
        width: 120px;
        height: 24px;
        margin-bottom: 12px;
    }
    .skeleton-footer.type-1 .skeleton-footer-row {
        display: flex;
    }
    .skeleton-footer.type-1 .skeleton-footer-catalog-item:not(:last-child) {
        margin-right: 20px;
    }
    .skeleton-footer.type-1 .skeleton-footer-row:nth-child(2) {
        padding: 32px 0 24px;
    }
    .skeleton-footer.type-1 .skeleton-footer-items{
        display: flex;
        flex-wrap: wrap;
        padding-right: 40px;
    }
    .skeleton-footer.type-1 .skeleton-footer-item{
        height: 28px;
        width: 108px;
    }
    .skeleton-footer.type-1 .skeleton-footer-item:not(:last-child) {
        margin-right: 24px;
    }

    .skeleton-footer.type-1 .skeleton-footer-contact {
        margin-left: auto;
    }

    .skeleton-footer.type-1 .skeleton-contact-input-wrap {
        display: flex;
        margin-bottom: 12px;
    }

    .skeleton-footer.type-1 .skeleton-footer-input {
        border-radius: 6px 0 0 6px;
        background: #FCFCFC;
        width: 220px;
        height: 48px;
    }
    .skeleton-footer.type-1 .skeleton-footer-btn {
        border-radius: 0 6px 6px 0;
        width: 184px;
        height: 48px;
    }
    .skeleton-footer.type-1 .skeleton-subscribe-block-checkbox {
        width: 160px;
        height: 24px;
        margin-bottom: 2px;  
    }
    .skeleton-footer.type-1 .skeleton-subscribe-block-subtitle {
        height: 20px;
        margin-left: 15px;
        width: 100%;
    }

    .skeleton-footer.type-1 .skeleton-footer-row:last-child {
        padding-top: 24px;
    }

    .skeleton-footer.type-1 .skeleton-footer-socials {
        display: flex;
    }
    .skeleton-footer.type-1 .skeleton-footer-social {
        width: 24px;
        height: 24px;
        margin-right: 16px;
    }

    .skeleton-footer.type-1 .skeleton-footer-policy {
        margin-left: auto;
        height: 24px;
        width: 300px;
    }

    .skeleton-footer.type-1 .skeleton-subscribe-block {
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
    }

    .skeleton-bg-white {
        background: #FCFCFC;
    }

    @media (max-width: 1366px) {
        .skeleton-footer.type-1 .skeleton-footer-subtitle {
            margin-bottom: 26px;
        }
        .skeleton-footer.type-1 .col-12:first-child {
            padding: 28px 15px;
        }
        .skeleton-footer.type-1 .skeleton-footer-item {
            height: 24px;
        }
    }

    @media (max-width: 1024px) {
        .skeleton-footer.type-1 .col-12:first-child {
            flex: 1;
            max-width: 100%;
            padding: 0 0 22px;
        }
        
        .skeleton-footer.type-1 .col-12:last-child {
            flex: 1;
            max-width: 100%;
            padding: 0;
        }
        .skeleton-footer.type-1 {
            padding: 28px 0;
        }
        .skeleton-footer.type-1 .row {
            flex-direction: column;
            margin: 0;
        }
        .skeleton-footer.type-1 .skeleton-footer-catalog-items {
            display: none;
        }
        .skeleton-footer.type-1 .skeleton-contact-input-wrap {
            display: none;
        }
        .skeleton-footer.type-1 .skeleton-footer-row:nth-child(2) {
            flex-direction: column;
            padding: 0;
        }
        .skeleton-footer.type-1 .skeleton-footer-items {
            flex-direction: column;
            padding: 0;
            margin-bottom: 36px;
        }
        .skeleton-footer.type-1 .skeleton-footer-item:not(:last-child) {
            margin-bottom: 12px;
            margin-right: 0;
        }
        .skeleton-footer.type-1 .skeleton-footer-contact {
            margin: 0;
        }
        .skeleton-footer.type-1 .skeleton-subscribe-block-subtitle {
            width: 200px;
            margin: 0 0 12px;
        }
        .skeleton-footer.type-1 .skeleton-footer-row:last-child {
            flex-direction: column;
        }
        .skeleton-footer.type-1 .skeleton-footer-social {
            margin-bottom: 16px;
        }
        .skeleton-footer.type-1 .skeleton-footer-policy {
            margin: 0 auto;
        }
    }
    
</style>

<div class="skeleton-footer type-1 skeleton-bg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="skeleton-footer-logo skeleton-bg-white"></div>
                <div class="skeleton-footer-subtitle skeleton-bg-white"></div>
                <div class="skeleton-footer-adress skeleton-footer-contact-item skeleton-bg-white"></div>
                <div class="skeleton-footer-time skeleton-footer-contact-item skeleton-bg-white"></div>
                <div class="skeleton-footer-phone skeleton-footer-contact-item skeleton-bg-white"></div>
                <div class="skeleton-footer-email skeleton-footer-contact-item skeleton-bg-white"></div>
            </div>
            <div class="col-12 col-md-9">
                <div class="skeleton-footer-catalog-items">
                    <div class="skeleton-footer-catalog-item skeleton-bg-white"></div>
                    <div class="skeleton-footer-catalog-item skeleton-bg-white"></div>
                    <div class="skeleton-footer-catalog-item skeleton-bg-white"></div>
                    <div class="skeleton-footer-catalog-item skeleton-bg-white"></div>
                    <div class="skeleton-footer-catalog-item skeleton-bg-white"></div>
                    <div class="skeleton-footer-catalog-item skeleton-bg-white"></div>
                    <div class="skeleton-footer-catalog-item skeleton-bg-white"></div>
                </div>
                <div class="skeleton-footer-row">
                    <div class="skeleton-footer-items">
                        <div class="skeleton-footer-item skeleton-bg-white"></div>
                        <div class="skeleton-footer-item skeleton-bg-white"></div>
                        <div class="skeleton-footer-item skeleton-bg-white"></div>
                        <div class="skeleton-footer-item skeleton-bg-white"></div>
                        <div class="skeleton-footer-item skeleton-bg-white"></div>
                    </div>
                    <div class="skeleton-footer-contact">
                        <div class="skeleton-footer-contact-form">
                            <div class="skeleton-contact-input-wrap">
                                <div class="skeleton-footer-input"></div>
                                <div class="skeleton-footer-btn skeleton-bg-white"></div>
                            </div>
                            <div class="skeleton-subscribe-block">
                                <div class="skeleton-subscribe-block-checkbox skeleton-bg-white"></div>
                                <div class="skeleton-subscribe-block-subtitle skeleton-bg-white"></div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="skeleton-footer-row">
                    <div class="skeleton-footer-socials">
                        <div class="skeleton-footer-social skeleton-bg-white"></div>
                        <div class="skeleton-footer-social skeleton-bg-white"></div>
                        <div class="skeleton-footer-social skeleton-bg-white"></div>
                        <div class="skeleton-footer-social skeleton-bg-white"></div>
                        <div class="skeleton-footer-social skeleton-bg-white"></div>
                        <div class="skeleton-footer-social skeleton-bg-white"></div>
                        <div class="skeleton-footer-social skeleton-bg-white"></div>
                        <div class="skeleton-footer-social skeleton-bg-white"></div>
                    </div>
                    <div class="skeleton-footer-policy skeleton-bg-white"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Options;

global $APPLICATION;

?>
<script>
    $('footer').parent().css({'margin-top': 'auto'});
</script>
<footer class="<?=Options\Base::getInstance()->getValue('footerBackgroundColor') == "white" ? "white" : "black"?> type-1">
    <div class="container">
        <div class="footer row">
            <div class="col-12 col-md-3">
                <div class="logo">
                    <?
                    if (Options\Base::getInstance()->getValue('footerBackgroundColor') == "white")
                        Layout\Partial::getInstance()->render('footer/logo.php');
                    else
                        Layout\Partial::getInstance()->render('footer/logo_white.php');
                    ?>
                </div>
                <div class="subtitle">
                    <? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/slogan.php'); ?>
                </div>
                <div class="contacts">
                    <div class="adress">
                        <? if (Regions\Base::getInstance()->isActive())
                            echo Regions\Base::getInstance()->getCurrent()['PROPERTIES']['ADDRESS']['VALUE'];
                        else
                            echo Options\Base::getInstance()->getValue('address');
                        ?>
                    </div>
                    <div class="time">
                        <? if (Regions\Base::getInstance()->isActive())
                            echo Regions\Base::getInstance()->getCurrent()['PROPERTIES']['WORKTIME']['VALUE'];
                        else
                            echo Options\Base::getInstance()->getValue('worktime');
                        ?>
                    </div>
                    
                    <div class="phone">
                        <? Layout\Partial::getInstance()->render('footer/phone.php') ?>
                    </div>
                    <div class="email">
                        <? if (Regions\Base::getInstance()->isActive())
                            $email = Regions\Base::getInstance()->getCurrent()['PROPERTIES']['EMAIL']['VALUE'];
                        else
                            $email = Options\Base::getInstance()->getValue('email');
                        ?>
                        <a href="mailto:<?=$email?>"><?=$email?></a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9">
                
                <? Layout\Ajax::getInstance()->render('footer/catalog_menu.php') ?>
                
                
                <div class="menu-row">
                    
                    <? Layout\Ajax::getInstance()->render('footer/menu.php') ?>

<!--                    <div class="subscribe-block">-->
<!--                        -->
<!--                        --><?// Layout\Ajax::getInstance()->render('footer/subscribe.php') ?>
<!---->
<!--                    </div>-->
                </div>
                <div class="my-3 footer-text">
                    Представленная на сайте информация не является публичной офертой (ст. 436 ГК РФ). Стоимость и наличие товаров просьба уточнять в магазине.
                </div>
                <div class="footer-text">
                    Уведомляем Вас, что все персональные данные обрабатываются на сайте в целях его функционирования. Если вы не согласны, то можете прекратить работу с сайтом. В противном случае это будет являться согласием на обработку персональных данных.
                </div>
                <div class="bottom">
                    
                    <? Layout\Partial::getInstance()->render('footer/social_links.php') ?>
                    
                    <? $APPLICATION->IncludeFile(SITE_DIR . 'include/footer/policy_link.php'); ?>

                    
                </div>
            </div>
        </div>
    </div>
</footer>
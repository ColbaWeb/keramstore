<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Options;

global $APPLICATION;

?>
<div class="modal-container mobile mobile-sidebar-menu hidden">
    <div class="modal-content-container">
        <div class="menu-scrollbar-inner">
            <div class="content-wrapper">
                <div class="menu-catalog-modal">
                    <div class="menu-container">
                        <ul class="menu-list-1 list reset-ul-list">
                            <?

                            Layout\Partial::getInstance()->render('mobile/catalog_menu.php');

                            Layout\Partial::getInstance()->render('mobile/top_menu.php');?>
                            <li class="info-page">
                                <a href="/333/"><span>Бренды</span></a>
                            </li>
                            <?

                            if (Regions\Base::getInstance()->isActive())
                            {
                                Layout\Partial::getInstance()->render('mobile/regions.php');
                            }

                            Layout\Partial::getInstance()->render('mobile/phone.php');
                            ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="bottom-menu">
            
            <? Layout\Partial::getInstance()->render('mobile/bottom_menu.php'); ?>
            
        </div>
    </div>
    <div class="close-area"></div>
</div>

<a href="javascript:void(0);" class="menu-mobile-bottom-button"><i class="icon icon-menu"></i></a>

<script>
    $(document).ready(function () {
        if($('.menu-scrollbar-inner').length > 0) {
            let ps = new PerfectScrollbar('.menu-scrollbar-inner', {
                wheelSpeed: 1,
                wheelPropagation: false,
                maxScrollbarLength: 100,
                suppressScrollX: true,
            });

            $("body").on('click', ".menu-catalog-modal .menu-container .list li.has-sub > .has-sub", function (event) {
                event.preventDefault();

                $(".menu-container").addClass('open');
                var sub = $(this).parent().find('> .sub');
                sub.addClass('open');

                $(".menu-container .list").css('height', 'auto');
                $(".menu-container .list").css('height', sub.outerHeight());

                ps.update();

                return false;
            });

            $("body").on('click', ".menu-catalog-modal .menu-container .list .menu-back > a", function (event) {
                var sub = $(this).parent().parent().parent();
                if (sub.hasClass('open') && sub.hasClass('sub'))
                {
                    $(".menu-container .list").css('height', 'auto');
                    sub.removeClass('open');
                    $(".menu-container .list").css('height', sub.parent().parent().parent().outerHeight());

                    ps.update();
                }
            });
        }
    });
</script>
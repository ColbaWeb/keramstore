<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

Loc::loadMessages(__FILE__);

if (! \Bitrix\Main\Loader::includeModule('nextype.alpha') )
    die(Loc::getMessage('NOT_MODULE_INSTALLED'));

Application::getInstance(SITE_ID)->prolog();

Layout\Assets::getInstance()->addLess(array (
    SITE_TEMPLATE_PATH . '/less/elements.less',
    SITE_TEMPLATE_PATH . '/less/icons.less',
    SITE_TEMPLATE_PATH . '/less/template_styles.less',
    SITE_TEMPLATE_PATH . '/less/custom.less'
));

Layout\Assets::getInstance()->addCss(array (
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap.min.css',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap-select.min.css',
    SITE_TEMPLATE_PATH . '/vendor/perfect-scrollbar/perfect-scrollbar.css',
    SITE_TEMPLATE_PATH . '/vendor/swiper/swiper.min.css',
    SITE_TEMPLATE_PATH . '/vendor/lightbox/lightbox.min.css',
    SITE_TEMPLATE_PATH . '/custom.css',
    SITE_TEMPLATE_PATH . '/custom_media.css',
));

Layout\Assets::getInstance()->addJs(array (
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.min.js',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap.bundle.min.js',
    SITE_TEMPLATE_PATH . '/vendor/bootstrap/bootstrap-select.min.js',
    SITE_TEMPLATE_PATH . '/vendor/maskedinput/jquery.maskedinput.min.js',
    SITE_TEMPLATE_PATH . '/vendor/perfect-scrollbar/perfect-scrollbar.min.js',
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.inview.js',
    SITE_TEMPLATE_PATH . '/vendor/formstyler/jquery.formstyler.min.js',
    SITE_TEMPLATE_PATH . '/vendor/swiper/swiper.min.js',
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.lazy.min.js',
    SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.lazy.av.min.js',
    SITE_TEMPLATE_PATH . '/vendor/vue/vue.min.js',
    SITE_TEMPLATE_PATH . '/vendor/lightbox/lightbox.min.js',
    SITE_TEMPLATE_PATH . '/vendor/hover_touch/jquery.izoomify.js',
    
    SITE_TEMPLATE_PATH . '/js/application.js',
    SITE_TEMPLATE_PATH . '/js/layout.js',
    SITE_TEMPLATE_PATH . '/js/messages.js',
    SITE_TEMPLATE_PATH . '/js/basket.js',
    SITE_TEMPLATE_PATH . '/js/wishlist.js',
    SITE_TEMPLATE_PATH . '/js/menu.js',
    SITE_TEMPLATE_PATH . '/js/signform.js',
    SITE_TEMPLATE_PATH . '/js/popups.js',
    SITE_TEMPLATE_PATH . '/js/catalogitem.js',
    
    SITE_TEMPLATE_PATH . '/custom.js',
    SITE_TEMPLATE_PATH . '/custom_media.js',
));

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?= LANGUAGE_ID?>">
	<head>
		<? $APPLICATION->ShowMeta("viewport"); ?>
		<title><? $APPLICATION->ShowTitle(); ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="HandheldFriendly" content="true" />
		<meta name="format-detection" content="telephone=no">
        <meta property="og:site_name" content="<?=$_SERVER['HTTP_HOST'] ?>">
        <meta property="og:title" content="<?=$APPLICATION->ShowTitle() ?>">
        <meta property="og:url" content="<?=$_SERVER['HTTP_HOST'].$APPLICATION->GetCurPage() ?>">
        <meta name="og:type" content="website">
		<meta name="google-site-verification" content="SGed8q1hfbhkxRpQC8LsFlyFimVmH2RctvUMSVM3TUo" />

        <meta property="og:image" content="<?="https://".$_SERVER["HTTP_HOST"]."/upload/nextype.alpha/7d1/rxruzflpl7ggmdxr22h2ah384vbzvv1i.png"?>">
        <!--<meta property="og:image" content="<?php /*=$APPLICATION->IsHTTPS() ? "https://" : "http://".$_SERVER["HTTP_HOST"].CFile::GetPath(Nextype\Alpha\Options\Base::getInstance()->getValue('logo'))*/?>">-->
        <meta property="og:description" content="<?=$APPLICATION->ShowProperty('description') ?>">

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TSJ99JCVZZ"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TSJ99JCVZZ');
</script>

        <?  $APPLICATION->ShowHead(); ?>
		<?
            $APPLICATION->AddHeadString('<script>BX.message(' . CUtil::PhpToJSObject($MESS, false) . ')</script>', true);
            $APPLICATION->AddHeadString('<script>BX.message(' . CUtil::PhpToJSObject(array (
                "ADD_TO_BASKET_SUB_TITLE" => Loc::getMessage('NT_ALPHA_MESSAGE_ADD_TO_BASKET_SUB_TITLE'),
                "ADD_TO_WISHLIST_TITLE" => Loc::getMessage('NT_ALPHA_MESSAGE_ADD_TO_WISHLIST_TITLE'),
                "DELETE_FROM_WISHLIST_TITLE" => Loc::getMessage('NT_ALPHA_MESSAGE_DELETE_FROM_WISHLIST_TITLE'),
                "GO_TO_BASKET_TITLE" => Loc::getMessage('NT_ALPHA_MESSAGE_GO_TO_BASKET_TITLE'),
                "GO_TO_EMPTY_TITLE" => Loc::getMessage('NT_ALPHA_MESSAGE_GO_TO_EMPTY_TITLE'),
                "GO_TO_WISHLIST_TITLE" => Loc::getMessage('NT_ALPHA_MESSAGE_GO_TO_WISHLIST_TITLE'),
            ), false) . ')</script>', true);
                ?>
                
                    <style>
                        .modal-lazy-spinner{display: flex; width: 100%; min-height: 150px; justify-content: center; align-items: center;}
                        @keyframes placeHolderShimmer {
                            0% {
                                -webkit-transform: translateZ(0);
                                transform: translateZ(0);
                                background-position: -468px 0
                            }
                            to {
                                -webkit-transform: translateZ(0);
                                transform: translateZ(0);
                                background-position: 468px 0
                            }
                        }
                        
                        .skeleton-card {
                            transition: all .3s ease-in-out;
                            -webkit-backface-visibility: hidden;
                        }
                        
                        .skeleton-card.hidden {
                            transition: all .3s ease-in-out;
                            opacity: 0;
                            height: 0;
                            padding: 0
                        }
                        
                        .skeleton-bg {
                            will-change: transform;
                            /*animation: placeHolderShimmer 1s linear infinite forwards;*/
                            -webkit-backface-visibility: hidden;
                            background: #fff;
                            background: linear-gradient(90deg, #F5F5F5 8%, #fff 18%, #F5F5F5 33%);
                            background-size: 800px 104px;
                            height: 100%;
                            position: relative;
                            box-sizing: border-box;
                        }
                    </style>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();
                for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
                k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(47861291, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        </script>
        <!-- /Yandex.Metrika counter -->
        <?
        $APPLICATION->IncludeComponent(
	"coffeediz:schema.org.OrganizationAndPlace", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"TYPE" => "Organization",
		"NAME" => "Интернет магазин Keramstore",
		"POST_CODE" => "",
		"COUNTRY" => "RU",
		"REGION" => "Москва",
		"LOCALITY" => "Химки",
		"ADDRESS" => "Бутаково 4",
		"PHONE" => array(
			0 => "8 800 511-68-48",
			1 => "",
		),
		"FAX" => "",
		"SITE" => "www.keramstore.ru",
		"LOGO" => "https://www.keramstore.ru/upload/nextype.alpha/7d1/rxruzflpl7ggmdxr22h2ah384vbzvv1i.png",
		"DESCRIPTION" => "Предлагаем купить оригинальные коллекции европейских и российских производителей плитки и керамогранита, в которых собраны качественные, сертифицированные отделочные материалы, соответствующие актуальным тенденциям.",
		"PARAM_RATING_SHOW" => "N",
		"ITEMPROP" => "",
		"SHOW" => "Y",
		"TYPE_2" => "",
		"LOGO_NAME" => "Keramstore",
		"LOGO_CAPTION" => "Интернет-магазин Keramstore",
		"LOGO_DESCRIPTION" => "Предлагаем купить оригинальные коллекции европейских и российских производителей плитки и керамогранита, в которых собраны качественные, сертифицированные отделочные материалы, соответствующие актуальным тенденциям.",
		"LOGO_HEIGHT" => "62",
		"LOGO_WIDTH" => "285",
		"LOGO_TRUMBNAIL_CONTENTURL" => "",
		"EMAIL" => array(
		),
		"TAXID" => ""
	),
	false
);
        ?>
	</head>
	<body class="<? Layout\Base::getInstance()->showBodyClasses() ?>">
            <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
            
            <div class="blur-bar-container" id="system-messages"></div>
            
            <? if (!Layout\Base::getInstance()->isHideHeaderAndFooter()): ?>
            <? Layout\Ajax::getInstance()->render('search.php', array (), false, false) ?>
            
            <? Layout\Partial::getInstance(__DIR__)->render('headerFixed', array (), false, false) ?>

            <? Layout\Partial::getInstance(__DIR__)->render('header', array (), false, false) ?>
            
            <? if (Layout\Base::getInstance()->isHomepage()): ?>
            <div class="wrapper">
                    <div class="main-page">
            <? elseif (!Layout\Base::getInstance()->isCustomPage()): ?>
                <div class="wrapper">
                    <div class="inner-page">
                        <div class="header-title">
                            <div class="container">
                                <?
                                if (!Layout\Base::getInstance()->isHideBreadcrumbs())
                                    Layout\Partial::getInstance()->render('breadcrumbs.php', array (), false, false);
                                ?>
                                
                                <h1 <?=Layout\Base::getInstance()->isHideTitle() ? 'class="d-none"' : ''?> <?= ($APPLICATION->GetCurPage(false) === '/') ? 'class="d-none"' : ''?> id="pagetitle"><? $APPLICATION->ShowTitle(false) ?></h1>
                            </div>
                        </div>
                        <div class="container page-container">
                            <? if (Layout\Base::getInstance()->isShowRightMenu()): ?>
                            <div class="row">
                                <div class="col">
                            <? endif; ?>
            <? endif; ?>
            <? endif; ?>

            <? if (Layout\Base::getInstance()->isShowShortHeaderAndFooter())
            {
                Layout\Partial::getInstance(__DIR__)->render('header/short.php', array (), false, false);
            }
            ?>
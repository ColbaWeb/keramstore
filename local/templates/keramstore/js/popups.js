(function (window){
	'use strict';
        
    if (window.Alpha.Popups)
        return;

    
    window.Alpha.Popups = {
        
        htmlHeaderProlog: `<div class="modal fade" tabindex="-1" aria-hidden="true" data-entity="alpha-popup" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                            <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="icon icon-close-big"></i>
                            </a>
                        `,
        htmlHeaderEpilog: `<div class="scrollbar-inner" data-entity="content">`,
        htmlFooterProlog: `</div>`,
        htmlFooterEpilog: `</div>
                          </div>`,
        htmlTitleProlog: `<div class="modal-title font-h3">`,
        htmlTitleEpilog: `</div>`,


        pasteModal: function (title, content)
        {
            $('body > [data-entity="alpha-popup"]').remove();
            var html = $(this.htmlHeaderProlog + title + this.htmlHeaderEpilog + content + this.htmlFooterProlog + this.htmlFooterEpilog);
            if ($("body").append(html))
                return html;
            
            return false;
        },
        
        openAjaxContent: function (params)
        {
            if (typeof(params) != 'object' || params.url === undefined)
                return false;
                
            var object = this.pasteModal('', '<div class="modal-lazy-spinner"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>');
            if (typeof(object) == 'object')
            {
                object.modal('show');
                
                BX.ajax({
                    url: params.url,
                    method: 'GET',
                    dataType: 'html',
                    timeout: 30,
                    emulateOnload: true,
                    onsuccess: function (result) {
                        if (!result)
                            return;

                        var ob = BX.processHTML(result);

                        object.find('[data-entity="content"]').html(ob.HTML);
                        BX.ajax.processScripts(ob.SCRIPT, true);

                    }
                });
            }
        },
        
        open: function (title, content)
        {
            title = (title) || '';
            content = (content) || '';
            
            if (title != '' || content != '')
            {
                if (title != '')
                    title = this.htmlTitleProlog + title + this.htmlTitleEpilog;
                
                var object = this.pasteModal(title, content);
                
                if (typeof(object) == 'object')
                {
                    object.modal('show');
                    
                    return true;
                }
            }
        },
        
        closeAll: function ()
        {
            $('[data-entity="alpha-popup"]').modal('hide');
        }
        
    };
    
    
    
})(window);
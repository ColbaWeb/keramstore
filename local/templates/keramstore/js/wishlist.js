(function (window){
	'use strict';

    if (window.Alpha.Wishlist)
        return;
    
    window.Alpha.Wishlist = function (params)
    {
        this.params = {};
        this.timer = false;
        
        if (params.containerId !== undefined)
        {
            this.params = params;
            
            this.init();
        }
    };
    
    window.Alpha.Wishlist.prototype = {
        
        init: function () {
            var _ = this;
            
            /*BX.addCustomEvent(window, 'OnBasketChange', BX.delegate(function () {
                this.refreshList({});
            }, this));*/

            BX.addCustomEvent(window, 'OnWishlistChange', BX.delegate(function (event) {
                if (event === undefined || event.data === undefined || event.data.containerId === undefined || (typeof(event.data) == 'object' && !!event.data.containerId && event.data.containerId != this.params.containerId))
                    this.refreshList({});
            }, this));
        },
        
        refreshList: function (data)
        {
            
            var _ = this;
            data.sessid = BX.bitrix_sessid();
            data.siteId = this.params.siteId;
            data.ajaxMode = true;
            
            if (this.timer != undefined)
                clearTimeout(this.timer);
            
            this.timer = setTimeout(function () {

                BX.ajax({
                    url: _.params.ajaxPath,
                    method: 'POST',
                    dataType: 'html',
                    data: data,
                    timeout: 30,
                    emulateOnload: true,
                    onsuccess: function (result) {
                        if (!result)
                            return;

                        var ob = BX.processHTML(result);

                        BX(_.params.containerId).innerHTML = ob.HTML;
                        BX.ajax.processScripts(ob.SCRIPT, true);

                    }
                });
                
            }, 500);
        }
    }


})(window);

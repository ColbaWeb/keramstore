(function (window){
	'use strict';

    if (window.Alpha.Basket)
        return;
    
    window.Alpha.Basket = function (params)
    {
        this.params = {};
        this.timer = false;
        
        if (params.containerId !== undefined && params.params !== undefined)
        {
            this.params = params;
            
            this.init();
        }
    };
    
    window.Alpha.Basket.prototype = {
        
        init: function () {
            var _ = this;
            
            $("body").on('click', '#' + this.params.containerId + ' [data-entity="delete"]', function () {
                var itemId = parseInt($(this).closest('[data-item-id]').data('item-id'));
                if (itemId > 0)
                {
                    _.deleteItem(itemId);
                }
            });
            
            $("body").on('click', '#' + this.params.containerId + ' [data-entity="quantity-minus"], #' + this.params.containerId + ' [data-entity="quantity-plus"]', function () {
                var qtyInput = $(this).closest('[data-item-id]').find('[data-entity="quantity-input"]'),
                        itemRatio = parseFloat($(this).closest('[data-item-id]').data('item-qty-ratio'));

                if (qtyInput.length > 0)
                {
                    var qty = parseFloat(qtyInput.val());

                    if ($(this).data('entity') == "quantity-minus")
                    {
                        qty -= itemRatio;
                        if (qty <= 0)
                            qty = 0;
                    } else
                    {
                        qty += itemRatio;
                    }

                    qtyInput.val(qty).trigger('change');
                }
            });

            $("body").on('change', '#' + this.params.containerId + ' [data-entity="quantity-input"]', function () {

                var itemId = parseInt($(this).closest('[data-item-id]').data('item-id')),
                        itemRatio = parseFloat($(this).closest('[data-item-id]').data('item-qty-ratio')),
                        qtyValue = parseFloat($(this).val()),
                        changedValue = false;

                if (qtyValue == '')
                {
                    qtyValue = itemRatio;
                    changedValue = true;
                }

                if (qtyValue % itemRatio != 0)
                {
                    qtyValue = Math.ceil((itemRatio - (qtyValue % itemRatio) + qtyValue) * 100) / 100;
                    changedValue = true;
                }

                if (itemId > 0)
                {
                    _.updateItem(itemId, qtyValue);
                }

                if (changedValue)
                    $(this).val(qtyValue);
            });

            BX.addCustomEvent(window, 'OnBasketChange', BX.delegate(function (event) {
                if (event === undefined || event.data === undefined || event.data.containerId === undefined || (typeof(event.data) == 'object' && !!event.data.containerId && event.data.containerId != this.params.containerId))
                    this.refreshCart({});
                
            }, this));
            
        },
        
        updateItem: function (id, qty)
	{
            this.refreshCart ({action: 'update', itemId: id, quantity: qty});
	},

	deleteItem: function (id)
	{
            this.refreshCart ({action: 'delete', itemId: id});
	},
        
        refreshCart: function (data)
        {

            var _ = this;
            data.sessid = BX.bitrix_sessid();
            data.siteId = this.params.siteId;
            data.templateName = this.params.templateName;
            data.arParams = this.params.params;
                        
            if (this.timer != undefined)
                clearTimeout(this.timer);
            
            this.timer = setTimeout(function () {

                BX.ajax({
                    url: _.params.ajaxPath,
                    method: 'POST',
                    dataType: 'html',
                    data: data,
                    timeout: 30,
                    emulateOnload: true,
                    onsuccess: function (result) {
                        if (!result)
                            return;

                        var ob = BX.processHTML(result);

                        BX(_.params.containerId).innerHTML = ob.HTML;
                        BX.ajax.processScripts(ob.SCRIPT, true);
                        
                        $('#'+_.params.containerId+' .small-basket .items.has-scroll .scrollbar-inner').scroll(function(){
                            $(window).scroll();
                        });

                        if (!!data.action && (data.action == 'update' || data.action == 'delete'))
                        {
                            BX.onCustomEvent(window, 'OnBasketChange', {containerId: _.params.containerId});
                        }
                    }
                });
                
            }, 500);
        },
        
    };
    
})(window);

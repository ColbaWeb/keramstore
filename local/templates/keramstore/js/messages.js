(function (window){
	'use strict';

    if (window.Alpha.Messages)
        return;
    
    window.Alpha.Messages = function (params)
    {
        this.params = {};
        this.timer = false;
        this.containerId = "system-messages";
        this.message = false;
        this.timeToRemove = 3000;
        
        if (params !== undefined)
        {
            this.params = params;
            
            this.init();
        }
    };
    
    window.Alpha.Messages.prototype = {
        
        init: function () {
            var _ = this;
            
            if (!!this.params.type)
            {
                var obClose = BX.create('a', {
                    props: {className: 'close icon icon-close-big-white'},
                    events: {
                        click: BX.proxy(function (e) {
                            BX.PreventDefault(e);
                            
                            var parent = BX.findParent(BX.getEventTarget(e), {
                                className: 'blur-bar-wrap'
                            }, true);
                            
                            (new BX.easing({
                                duration : 300,
                                start:{opacity: 100},
                                finish:{opacity: 0},
                                step : function(state){
                                    parent.style.opacity = state.opacity/100;
                                },
                                complete: function()
                                {
                                    parent.remove();
                                }
                            })).animate();
                            
                            return false;
                        }, this)
                    }
                }),
                obLink = null, obLinkText = BX.message('GO_TO_EMPTY_TITLE'), obImage = null, obTitle = null, obMessage = null;

                if (!!this.params.title)
                {
                    obTitle = BX.create('div', {
                        props: {className: 'desc font-title'},
                        html: this.params.title
                    });
                }

                if (!!this.params.message)
                {
                    obMessage = BX.create('div', {
                        props: {className: 'name font-body-2'},
                        html: this.params.message
                    });
                }

                if (!!this.params.image && this.params.image.length > 0)
                {
                    obImage = BX.create('div', {
                        props: {
                            className: 'img',
                        },
                        dataset: {
                            src: this.params.image
                        }
                    });
                }

                if (!!this.params.url && this.params.url.length > 0)
                {
                    if (!!this.params.urlText && this.params.urlText.length > 0)
                        obLinkText = BX.message('GO_TO_' + this.params.urlText + '_TITLE');

                    obLink = BX.create('a', {
                        props: {className: 'btn btn-link font-large-button'},
                        attrs: {href: this.params.url},
                        children: [
                            BX.create('span', {
                                html: obLinkText
                            }),
                            BX.create('i', {
                                props: {className: 'icon icon-arrow-right-text-button-white'}
                            })
                        ]
                    });
                }

                this.pushMessage( BX.create('div', {
                    props: {className: 'blur-bar-wrap'},
                    children: [
                        obClose,
                        BX.create('div', {
                            props: {className: 'blur-bar'},
                            children: [
                                BX.create('div', {
                                    props: {className: 'info'},
                                    children: [
                                        obImage,
                                        BX.create('div', {
                                            className: 'text',
                                            children: [
                                                obTitle,
                                                obMessage
                                            ]
                                        })
                                    ]
                                }),
                                obLink
                            ]
                        })
                    ]
                }) );

            }
            
            
        },
                
        pushMessage: function (obMessage) {
            
            BX(this.containerId).appendChild(obMessage);
            
            $("#" + this.containerId).find(".img").Lazy({
                effect: "fadeIn",
                effectTime: 250,
                threshold: 0
            });
            
            setTimeout(function() {
                
                (new BX.easing({
                    duration : 300,
                    start:{opacity: 100},
                    finish:{opacity: 0},
                    step : function(state){
                        obMessage.style.opacity = state.opacity/100;
                    },
                    complete: function()
                    {
                        obMessage.remove();
                    }
                })).animate();
      
            }, this.timeToRemove);
            
        }
    };
    
})(window);

(function() {

    Alpha.Ajax = {
        
        layoutAjaxUrl: BX.message('SITE_DIR') + "include/ajax/layout.php",
        
        load: function (options)
        {
            options = options || {};
            
            if (options.containerId !== undefined && options.params !== undefined)
            {
                var url = this.layoutAjaxUrl + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : '');

                if (document.location.href.indexOf('test_skeleton=Y') == -1)
                {    
                    BX.ajax({
                            url: url,
                            method: 'POST',
                            timeout: 30,
                            data: {
                                params: options.params,
                                sessionId: BX.bitrix_sessid()
                            },
                            timeout: 30,
                            emulateOnload: true,
                            onsuccess: function (result) {
                                if (!result)
                                    return;

                                var ob = BX.processHTML(result);

                                BX(options.containerId).innerHTML = ob.HTML;
                                BX.ajax.processScripts(ob.SCRIPT, true);

                            }
                    });
                }
            }
            
        },
        
        lazyload: function (options) {
            options = options || {};
            
            if (options.containerId !== undefined && options.params !== undefined)
            {
                if (document.location.href.indexOf('test_skeleton=Y') == -1)
                {
                    $("#" + options.containerId).lazy({
                        ajaxlazy: BX.delegate(function (element) {
                            var url = this.layoutAjaxUrl + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : '');
                            if (document.location.href.indexOf('test_skeleton=Y') !== -1)
                                url = url + (url.indexOf('?') !== -1 ? '&test_skeleton=Y' : '?test_skeleton=Y');
                            
                            BX.ajax({
                                url: url,
                                method: 'POST',
                                timeout: 30,
                                data: {
                                    params: options.params,
                                    sessionId: BX.bitrix_sessid()
                                },
                                timeout: 30,
                                emulateOnload: true,
                                onsuccess: function (result) {
                                    if (!result)
                                        return;

                                    var ob = BX.processHTML(result);

                                    BX(options.containerId).innerHTML = ob.HTML;
                                    BX.ajax.processScripts(ob.SCRIPT, true);

                                    if (typeof(window.Alpha) == 'object' && typeof(window.Alpha.Application) == 'object')
                                    {
                                        window.Alpha.Application.refreshEvents();
                                    }
                                }
                            });
                        }, this)
                    });
                }
            }
        }
    };
})();
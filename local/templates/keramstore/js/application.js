(function (window){
	'use strict';
        
    if (window.Alpha)
        return;

    window.Alpha = {};
    
    window.Alpha.Application = function (params)
    {
        this.params = {};
        this.init();
        
    };
    
    window.Alpha.Application.prototype = {
        
        init: function () {
            
            BX.addCustomEvent('onAjaxSuccess', BX.delegate(this.refreshEvents, this)); 
            $(document).ajaxSuccess(BX.delegate(this.refreshEvents, this));
            
            this.initEvents();
            this.refreshEvents();
            this.lazyEvents();
        },
        
        initEvents: function () {
            
            var _ = this;

            let vh = window.innerHeight * 0.01;
            document.documentElement.style.setProperty('--vh', `${vh}px`);

            window.addEventListener('resize', () => {
                let vh = window.innerHeight * 0.01;
                document.documentElement.style.setProperty('--vh', `${vh}px`);
            });

            $(document).click(function(event) {
                if ($(event.target).closest("[data-dropdown-links-id]").length) return;
                if ($(event.target).closest('[data-role="sort-dropdown"]').length) return;
                if ($(event.target).closest(".search-header").length) return;
                if ($(event.target).closest(".search-fast-result").length) return;
                if ($(event.target).closest(".search-line").length) return;
                if ($(event.target).closest(".filter-container-horizontal").length) return;
                if ($(event.target).closest(".auth-popup").length) return;
                if ($(event.target).closest('[data-role="horizontal-filter-block"]').length) return;


                $("[data-dropdown-links-id], [data-dropdown-links], .search-header, [data-role='sort-dropdown'], .auth-popup, [data-role='horizontal-filter-block']").removeClass("open");
                $(".search-fast-result > .scrollbar-inner").empty();
                $("[data-dropdown-links]").parent().removeClass("open");
                $(".filter-container-horizontal, .filter-horizontal-btn").removeClass('active');

                event.stopPropagation();
            });

            $(function() {
                var $window = $(window),
                    $body = $("body"),
                    scrollDistance = 0;

                $body.on('modalOpen', function () {
                    scrollDistance = $window.scrollTop();
                    $body.css("top", scrollDistance * -1);
                });

                $body.on('modalClose', function () {
                    $body.css("top", "");
                    $window.scrollTop(scrollDistance);
                });
            });

            $(document).on('show.bs.modal', '.modal', function () {
                $('body').trigger('modalOpen');
            });
            $(document).on('hidden.bs.modal', '.modal', function () {
                $('body').trigger('modalClose');
            });

            $(document).on('scroll', function() {
                if ($(window).scrollTop() > ($('.top-header-wrapper').outerHeight() + $('.main-header-wrapper').outerHeight() + $('.bottom-header-wrapper').outerHeight() + 290)) {
                    $('.header.header-fixed.open-header').addClass('visible');
                }
                else {
                    $('.header.header-fixed.open-header').removeClass('visible');
                }
            });
            
            $(document).on('click', '.modal-container .close-area', function (event) {
                $(this).parent().addClass('hidden');
                $('body').removeClass('modal-opened').trigger('modalClose');
            });

            $(document).on('click', '.modal-container .modal-content-container .close', function (event) {
                $(this).parent().parent().addClass('hidden');
                $('body').removeClass('modal-opened').trigger('modalClose');
            });

            $(document).on('click', '.brands-search .custom-input .clear-btn', function(){
                $(this).siblings('input').val('').removeClass('filled');
            })

            $('body').on('input', '.city-choose-modal input', function () {
                if ($(this).val() !== "") {
                    $(this).parent().addClass('filled');
                } else {
                    $(this).parent().removeClass('filled');
                }
            })

            $('body').on('click', '.city-choose-modal .clear-btn', function () {
                $(this).siblings('input').val('');
                $(this).parent().removeClass('filled');
            })

            $(document).on('click', '.btn.btn-primary', function (event) {
                $(this).addClass('animating');
                //$('body').toggleClass('fixed');
                setTimeout(function(){
                    $('.btn.btn-primary').removeClass('animating');
                }, 280);
            });
            
            $(document).on('click', function (e) {
                if (!$(e.target).parents('.menu-container-wrapper.open').length && (!$(e.target).parents('.menu-button').length)) {
                    $('.menu-container-wrapper.open').removeClass('open');
                    $('.menu-button .btn').removeClass('opened');
                    $('body').removeClass('fixed');
                }
            })

            
            $(document).on('click', '.close-menu', function (event) {
                $(this).parents('.mobile-sidebar-menu').addClass('hidden');
                $('body').removeClass('modal-opened').trigger('modalClose');
            });
            
            $(document).on('click', '.menu-mobile-bottom-button', function (event) {
                $('.mobile-sidebar-menu').removeClass('hidden');
                $('body').trigger('modalOpen').addClass('modal-opened');
            });

            $(document).on('click', '.js-regions-modal', function (event) {
                $(this).parents('.mobile-sidebar-menu').addClass('hidden');
                $('body').removeClass('modal-opened').trigger('modalClose');
            });
            
            $(document).on('click', '.main-header .menu-btn', function (event) {
                $('.mobile-sidebar-menu').removeClass('hidden');
                $('body').trigger('modalOpen').addClass('modal-opened');
            });

            $(document).on('click', '.js-sort-mobile .sorting-list a', function (event) {
                $(".modal-container.mobile.js-sort-mobile").addClass('hidden');
                $('body').removeClass('modal-opened').trigger('modalClose');
            });

            $(document).on('click', '.page-container .modal-container.mobile .menu-catalog-modal .menu-container a', function (event) {
                $('body').removeClass('modal-opened').trigger('modalClose');
            });

            $("body").on('click', '[data-role="sort-dropdown"] [data-role="sort-dropdown-title"]', function () {
                $(this).parents('[data-role="sort-dropdown"]').toggleClass('open');
            });

            $("body").on('click', '[data-role="sort-dropdown"] [data-role="sort-dropdown-item"]', function (event) {
                if ($(this).data('value') !== undefined) {
                    $(this).parents('[data-role="sort-dropdown"]').removeClass('open').find('[data-role="sort-dropdown-title"] span').text($(this).data('value'));

                    $(this).closest('.link').siblings().find('[data-role="sort-dropdown-item"]').removeClass('active');


                    $(this).addClass('active');
                }
            });

            $("body").on('click', '.mobile-buttons .btn-show-catalog', function (event) {
                $('body').trigger('modalOpen').addClass('modal-opened');
                if($('.modal-container.js-catalog-mobile').hasClass('hidden')) {
                    $('.modal-container.js-catalog-mobile').removeClass('hidden');

                }

            });
            $("body").on('click', '.mobile-buttons .btn-show-sorting', function (event) {
                $('body').trigger('modalOpen').addClass('modal-opened');
                if($('.modal-container.js-sort-mobile').hasClass('hidden')) {
                    $('.modal-container.js-sort-mobile').removeClass('hidden');

                }

            });
            $("body").on('click', '.mobile-buttons .btn-show-filter', function (event) {
                $('body').trigger('modalOpen').addClass('modal-opened');
                if($('.modal-container.js-filter-mobile').hasClass('hidden')) {
                    $('.modal-container.js-filter-mobile').removeClass('hidden');

                }

            });
            $("body").on('click', '.horizontal-filter .btn-show-categories', function (event) {
                $('body').trigger('modalOpen').addClass('modal-opened');
                if($('.modal-container.js-catalog-categories').hasClass('hidden')) {
                    $('.modal-container.js-catalog-categories').removeClass('hidden');

                }

            });

            $("body").on('click', '.sidebar-filter .accordion-head > a', function () {
                $(this).parent().toggleClass('open');
            });
            $("body").on('click', '.filter-modal .accordion-head > a', function () {
                $(this).parent().toggleClass('open');
            });
            $("body").on('click', '.delivery-modal .accordion-head', function () {
                $(this).parent().toggleClass('open');
            });

            $("body").on('click', '.horizontal-filter .accordion-head > a, .pickup-points .accordion-head > a', function () {
                // $(this).parent().find(".accordion-body").toggleClass('active');
                $(this).parent().toggleClass('open');


                if($(this).parent().siblings().hasClass('open')) {
                    $(this).parent().siblings().removeClass('open');
                }

            });


            $("body").on('click', '.sidebar-menu li.has-sub > span .icon', function (event) {
                $(this).closest('.has-sub').toggleClass('open');
            });
            
            $('body').on('click', '[data-entity="button-search-modal"]', function (event) {
                event.preventDefault();
                $('.search-modal').addClass('opened');
                $('body').trigger('modalOpen').addClass('modal-opened').addClass('search-opened');
                return false;
            });

            $('body').on('click', function (event) {
                if ($('body').hasClass('search-opened') && !$(event.target).parents().hasClass('search') && !$(event.target).parents().hasClass('search-modal') && !$(event.target).hasClass('search-modal')) {
                    $('.search-modal').removeClass('opened');
                    $('body').removeClass('modal-opened').removeClass('search-opened').trigger('modalClose');
                }
            });

            this.initScrollbar();

            $(document).on('click', '.search-modal .clear', function() {
                $(this).siblings('input').val('').removeClass('filled');
                $('.search-modal').removeClass('opened');
                $('body').removeClass('modal-opened').removeClass('search-opened').trigger('modalClose');
            });


        },

        refreshEvents: function () {

            $('.custom-input input, .custom-input textarea').on('focusout', function () {
                if ($(this).val() != '') $(this).addClass('filled')
                else $(this).removeClass('filled');
            });

            $('.custom-input input, .custom-input textarea').each(function () {
                if ($(this).val() != '') $(this).addClass('filled')
                else $(this).removeClass('filled');
            });
            
            if (BX.message('PHONE_NUMBER_MASK') != "")
            {
                $('input[data-field-type="phone"]').mask(BX.message('PHONE_NUMBER_MASK'));
            }
            
            this.lazyEvents(true);
            this.initScrollbar();
        },

        initScrollbar: function() {
            $('.scrollbar-inner').each(function () {
                if (!$(this).hasClass('ps')) {
                    const ps = new PerfectScrollbar($(this)[0], {
                        wheelSpeed: 1,
                        maxScrollbarLength: 100,
                        suppressScrollX: true,
                    });
                    $(this).on('ps-scroll-y', function() {
                        ps.update();
                    });
                }
            });
        },

        lazyEvents: function ()
        {
           
            $('[data-role="lazyload-background"]').Lazy({
                effect: "fadeIn",
                effectTime: 250,
                threshold: 0,
                
                afterLoad: function(element) {
                    element.find('.swiper-lazy-preloader').remove();
                }
            });

            $('[data-role="lazyload-image"]').Lazy({
                effect: "fadeIn",
                effectTime: 250,
                threshold: 0,
                
                afterLoad: function(element) {
                    element.prev('.swiper-lazy-preloader').remove();
                }
            });

            $('[data-role="lazyload-video"]').Lazy({
                effect: "fadeIn",
                effectTime: 250,
                threshold: 0,

                afterLoad: function(element) {
                    element.prev('.swiper-lazy-preloader').remove();
                }
            });
        }
        
    };
    
    window.Alpha.LazyImage = function (params)
    {
        this.params = params;
        return this.init();
        
    };
    
    window.Alpha.LazyImage.prototype = {
        
        init: function () {

            if (this.params.type == "VIDEO")
            {
                return [
                    BX.create('div', {
                        props: {className: 'image-wrapper'},
                        children: [
                            BX.create('div', {
                                props: {className: 'img-lazy-spinner'},
                                html: '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>'
                            }),
                            BX.create('div', {
                                props: {className: 'video-wrapper js-video'},
                                attrs: {
                                    onclick: 'window.Alpha.VideoControl($(this));'
                                },
                                children: [
                                    BX.create('div', {
                                        props: {className: 'video-button'}
                                    }),
                                    BX.create('video', {
                                        dataset: {role: 'lazyload-video'},
                                        attrs: {
                                            preload: 'metadata',
                                            loop: 'loop'
                                        },
                                        children: [
                                            BX.create('data-src', {
                                                attrs: {src: this.params.src}
                                            })
                                        ]
                                    })
                                ]
                            })
                        ]
                    })
                ];
            }
            else
            {
                return [
                    BX.create('div', {
                        props: {className: 'image-wrapper'},
                        children: [
                            BX.create('div', {
                                props: {className: 'img-lazy-spinner'},
                                html: '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>'
                            }),
                            BX.create('img', {
                                dataset: {role: 'lazyload-image', src: this.params.src},
                                props: {className: !!this.params.className ? this.params.className : ''}
                            })
                        ]
                    })
                ];
            }

        }
        
    };

    window.Alpha.LazyBackground = function (params)
    {
        this.params = params;
        return this.init();
        
    };

    window.Alpha.LazyBackgroundThumb = function (params)
    {
        this.params = params;
        return this.init();

    };

    window.Alpha.VideoControl = function (videoContainer)
    {
        let playBtn = videoContainer.find('.video-button'),
            video = videoContainer.find('video').get(0);

        if (video.paused) {
            video.play();
            videoContainer.addClass('playing');
            playBtn.addClass('pause');
        } else {
            video.pause();
            videoContainer.removeClass('playing');
            playBtn.removeClass('pause');
        }
    }
    
    window.Alpha.LazyBackground.prototype = {
        
        init: function () {
            
            return [
                
                BX.create('div', {
                    props: {className: !!this.params.className ? this.params.className : ''},
                    dataset: {role: "lazyload-background", src: this.params.src},
                    html: '<div class="img-lazy-spinner"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
                })
                
            ];
            
        }
        
    };

    window.Alpha.LazyBackgroundThumb.prototype = {

        init: function () {
            if (this.params.type == "VIDEO")
            {
                return [
                    BX.create('div', {
                        props: {className: 'img-wrapper'},
                        children: [
                            BX.create('div', {
                                props: {className: 'video-thumb-icon'},
                            }),
                            BX.create('div', {
                                props: {className: (!!this.params.className ? this.params.className : '') + ' video-thumb'},
                                dataset: {role: "lazyload-background", src: this.params.src},
                                html: '<div class="img-lazy-spinner"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
                            })
                        ]
                    })
                ];
            }
            else
            {
                return [
                    BX.create('div', {
                        props: {className: 'img-wrapper'},
                        children: [
                            BX.create('div', {
                                props: {className: !!this.params.className ? this.params.className : ''},
                                dataset: {role: "lazyload-background", src: this.params.src},
                                html: '<div class="img-lazy-spinner"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
                            })
                        ]
                    })
                ];
            }

        }

    };

    window.Alpha.LazyForceLoad = function (params)
    {
        this.params = params;
        this.init();
        
    };
    
    window.Alpha.LazyForceLoad.prototype = {
        
        init: function () {
            
            if (typeof(this.params) == 'object' && !!this.params.parentNode && typeof(this.params.parentNode) == 'object')
            {
                $(this.params.parentNode).find('[data-role="lazyload-background"]').each(function () {
                    var instance = $(this).data("plugin_lazy");
                    if (typeof(instance) == 'object') {
                        instance.loadAll();
                    }
                });

            }
            
        }
        
    };
    
    
})(window);

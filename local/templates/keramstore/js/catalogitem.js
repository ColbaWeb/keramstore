(function (window) {
    'use strict';

    if (window.Alpha.CatalogItem)
        return;

    window.Alpha.CatalogItem = function (arParams)
    {
        this.productType = 0;
        this.showQuantity = true;
        this.showAbsent = true;
        this.secondPict = false;
        this.showOldPrice = false;
        this.showMaxQuantity = 'N';
        this.relativeQuantityFactor = 5;
        this.showPercent = false;
        this.showSkuProps = false;
        this.basketAction = 'ADD';
        this.showClosePopup = false;
        this.useCompare = false;
        this.showSubscription = false;
        this.visual = {
            ID: '',
            QUANTITY_ID: '',
            QUANTITY_UP_ID: '',
            QUANTITY_DOWN_ID: '',
            DISPLAY_PROP_DIV: '',
            BASKET_PROP_DIV: '',
            SUBSCRIBE_ID: '',
            PRICE_ID: '',
            PRICE_OLD_ID: '',
            DSC_PERC: '',
            SECOND_DSC_PERC: '',
            PICT_SLIDER: '',
            WISHLIST_ID: ''
        };
        this.product = {
            checkQuantity: false,
            maxQuantity: 0,
            stepQuantity: 1,
            isDblQuantity: false,
            canBuy: true,
            name: '',
            id: 0,
            addUrl: '',
            buyUrl: '',
            picture: ''
        };

        this.basketMode = '';
        this.basketData = {
            useProps: false,
            emptyProps: false,
            quantity: 'quantity',
            props: 'prop',
            basketUrl: '',
            sku_props: '',
            sku_props_var: 'basket_props',
            add_url: '',
            buy_url: '',
            wishlist_url: '',
            wishlist_page_url: BX.message('SITE_DIR') + 'wishlist/',
            add_wish_to_delayed: false
        };

        this.compareData = {
            compareUrl: '',
            compareDeleteUrl: '',
            comparePath: ''
        };

        this.touch = null;

        this.quantityDelay = null;
        this.quantityTimer = null;

        this.checkQuantity = false;
        this.maxQuantity = 0;
        this.minQuantity = 0;
        this.stepQuantity = 1;
        this.isDblQuantity = false;
        this.canBuy = true;
        this.precision = 6;
        this.precisionFactor = Math.pow(10, this.precision);
        this.bigData = false;
        this.fullDisplayMode = false;
        this.viewMode = '';
        this.isWishList = false;

        this.currentPriceMode = '';
        this.currentPrices = [];
        this.currentPriceSelected = 0;
        this.currentQuantityRanges = [];
        this.currentQuantityRangeSelected = 0;


        this.obProduct = null;
        this.blockNodes = {};
        this.obQuantity = null;
        this.obQuantityUp = null;
        this.obQuantityDown = null;
        this.obQuantityLimit = {};
        
        this.obBuyBtn = null;
        this.obBasketActions = null;
        this.obSubscribe = null;
        this.obWishlist = null;
        
        this.obCompare = null;
        this.obPrice = null;
        this.obDscPerc = null;
        this.obSecondDscPerc = null;
        this.obPopupWin = null;
        this.basketUrl = '';
        this.basketParams = {};
        this.isTouchDevice = BX.hasClass(document.documentElement, 'bx-touch');

        this.useEnhancedEcommerce = false;
        this.dataLayerName = 'dataLayer';

        this.errorCode = 0;

        if (typeof arParams === 'object')
        {
            if (arParams.PRODUCT_TYPE)
            {
                this.productType = parseInt(arParams.PRODUCT_TYPE, 10);
            }

            this.showQuantity = arParams.SHOW_QUANTITY;
            this.showAbsent = arParams.SHOW_ABSENT;
            this.secondPict = arParams.SECOND_PICT;
            this.showOldPrice = arParams.SHOW_OLD_PRICE;
            this.showMaxQuantity = arParams.SHOW_MAX_QUANTITY;
            this.relativeQuantityFactor = parseInt(arParams.RELATIVE_QUANTITY_FACTOR);
            this.showPercent = arParams.SHOW_DISCOUNT_PERCENT;
            this.showSkuProps = arParams.SHOW_SKU_PROPS;
            this.showSubscription = arParams.USE_SUBSCRIBE;

            if (arParams.ADD_TO_BASKET_ACTION)
            {
                this.basketAction = arParams.ADD_TO_BASKET_ACTION;
            }

            this.showClosePopup = arParams.SHOW_CLOSE_POPUP;
            this.useCompare = arParams.DISPLAY_COMPARE;
            this.fullDisplayMode = arParams.PRODUCT_DISPLAY_MODE === 'Y';
            this.bigData = arParams.BIG_DATA;
            this.viewMode = arParams.VIEW_MODE || '';

            if (arParams.IS_WISHLIST === "Y")
            {
                this.isWishList = true;
            }

            this.templateTheme = arParams.TEMPLATE_THEME || '';
            this.useEnhancedEcommerce = arParams.USE_ENHANCED_ECOMMERCE === 'Y';
            this.dataLayerName = arParams.DATA_LAYER_NAME;

            this.visual = arParams.VISUAL;

            switch (this.productType)
            {
                case 0: // no catalog
                case 1: // product
                case 2: // set
                case 3: // sku
                    if (arParams.PRODUCT && typeof arParams.PRODUCT === 'object')
                    {
                        this.currentPriceMode = arParams.PRODUCT.ITEM_PRICE_MODE;
                        this.currentPrices = arParams.PRODUCT.ITEM_PRICES;
                        this.currentPriceSelected = arParams.PRODUCT.ITEM_PRICE_SELECTED;
                        this.currentQuantityRanges = arParams.PRODUCT.ITEM_QUANTITY_RANGES;
                        this.currentQuantityRangeSelected = arParams.PRODUCT.ITEM_QUANTITY_RANGE_SELECTED;
                        this.product.picture = arParams.PRODUCT.PICTURE;

                        if (this.showQuantity)
                        {
                            this.product.checkQuantity = arParams.PRODUCT.CHECK_QUANTITY;
                            this.product.isDblQuantity = arParams.PRODUCT.QUANTITY_FLOAT;

                            if (this.product.checkQuantity)
                            {
                                this.product.maxQuantity = (this.product.isDblQuantity ? parseFloat(arParams.PRODUCT.MAX_QUANTITY) : parseInt(arParams.PRODUCT.MAX_QUANTITY, 10));
                            }

                            this.product.stepQuantity = (this.product.isDblQuantity ? parseFloat(arParams.PRODUCT.STEP_QUANTITY) : parseInt(arParams.PRODUCT.STEP_QUANTITY, 10));

                            this.checkQuantity = this.product.checkQuantity;
                            this.isDblQuantity = this.product.isDblQuantity;
                            this.stepQuantity = this.product.stepQuantity;
                            this.maxQuantity = this.product.maxQuantity;
                            this.minQuantity = this.currentPriceMode === 'Q'
                                    ? parseFloat(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY)
                                    : this.stepQuantity;

                            if (this.isDblQuantity)
                            {
                                this.stepQuantity = Math.round(this.stepQuantity * this.precisionFactor) / this.precisionFactor;
                            }
                        }

                        this.product.canBuy = arParams.PRODUCT.CAN_BUY;

                        if (arParams.PRODUCT.RCM_ID)
                        {
                            this.product.rcmId = arParams.PRODUCT.RCM_ID;
                        }

                        this.canBuy = this.product.canBuy;
                        this.product.name = arParams.PRODUCT.NAME;
                        this.product.id = arParams.PRODUCT.ID;
                        this.product.DETAIL_PAGE_URL = arParams.PRODUCT.DETAIL_PAGE_URL;

                        if (arParams.PRODUCT.ADD_URL)
                        {
                            this.product.addUrl = arParams.PRODUCT.ADD_URL;
                        }

                        if (arParams.PRODUCT.BUY_URL)
                        {
                            this.product.buyUrl = arParams.PRODUCT.BUY_URL;
                        }

                        if (arParams.BASKET && typeof arParams.BASKET === 'object')
                        {
                            this.basketData.useProps = arParams.BASKET.ADD_PROPS;
                            this.basketData.emptyProps = arParams.BASKET.EMPTY_PROPS;
                        }
                    } else
                    {
                        this.errorCode = -1;
                    }

                    break;
                
                default:
                    this.errorCode = -1;
            }
            
            
            if (arParams.BASKET && typeof arParams.BASKET === 'object')
            {
                if (arParams.BASKET.QUANTITY)
                {
                    this.basketData.quantity = arParams.BASKET.QUANTITY;
                }

                if (arParams.BASKET.PROPS)
                {
                    this.basketData.props = arParams.BASKET.PROPS;
                }

                if (arParams.BASKET.BASKET_URL)
                {
                    this.basketData.basketUrl = arParams.BASKET.BASKET_URL;
                }


                if (arParams.BASKET.ADD_URL_TEMPLATE)
                {
                    this.basketData.add_url = arParams.BASKET.ADD_URL_TEMPLATE;
                }

                if (arParams.BASKET.BUY_URL_TEMPLATE)
                {
                    this.basketData.buy_url = arParams.BASKET.BUY_URL_TEMPLATE;
                }
                
                if (arParams.BASKET.WISHLIST_URL_TEMPLATE)
                {
                    this.basketData.wishlist_url = arParams.BASKET.WISHLIST_URL_TEMPLATE;
                    if (arParams.BASKET.WISHLIST_PAGE_URL && arParams.BASKET.WISHLIST_PAGE_URL.length > 0)
                        this.basketData.wishlist_page_url = arParams.BASKET.WISHLIST_PAGE_URL;
                    if (arParams.BASKET.ADD_WISH_TO_DELAYED === "Y")
                        this.basketData.add_wish_to_delayed = true;
                }

                if (this.basketData.add_url === '' && this.basketData.buy_url === '')
                {
                    this.errorCode = -1024;
                }
            }
            
            
            

            if (this.useCompare)
            {
                if (arParams.COMPARE && typeof arParams.COMPARE === 'object')
                {
                    if (arParams.COMPARE.COMPARE_PATH)
                    {
                        this.compareData.comparePath = arParams.COMPARE.COMPARE_PATH;
                    }

                    if (arParams.COMPARE.COMPARE_URL_TEMPLATE)
                    {
                        this.compareData.compareUrl = arParams.COMPARE.COMPARE_URL_TEMPLATE;
                    } else
                    {
                        this.useCompare = false;
                    }

                    if (arParams.COMPARE.COMPARE_DELETE_URL_TEMPLATE)
                    {
                        this.compareData.compareDeleteUrl = arParams.COMPARE.COMPARE_DELETE_URL_TEMPLATE;
                    } else
                    {
                        this.useCompare = false;
                    }
                } else
                {
                    this.useCompare = false;
                }
            }

        }

        if (this.errorCode === 0)
        {
            BX.ready(BX.delegate(this.init, this));
        }
    };

    window.Alpha.CatalogItem.prototype = {

        init: function ()
        {
            var i = 0,
                    treeItems = null;

            this.obProduct = BX(this.visual.ID);
            if (!this.obProduct)
            {
                this.errorCode = -1;
            }
            
            this.obPrice = BX(this.visual.PRICE_ID);
            this.obPriceOld = BX(this.visual.PRICE_OLD_ID);
            this.obPriceTotal = BX(this.visual.PRICE_TOTAL_ID);
            if (!this.obPrice)
            {
                this.errorCode = -16;
            }


            if (this.showQuantity && this.visual.QUANTITY_ID && this.obProduct)
            {
                this.obQuantity = BX(this.visual.QUANTITY_ID);
                this.blockNodes.quantity = this.obProduct.querySelector('[data-entity="quantity-block"]');

                if (!this.isTouchDevice)
                {
                    BX.bind(this.obQuantity, 'focus', BX.proxy(this.onFocus, this));
                    BX.bind(this.obQuantity, 'blur', BX.proxy(this.onBlur, this));
                }

                if (this.visual.QUANTITY_UP_ID)
                {
                    this.obQuantityUp = BX(this.visual.QUANTITY_UP_ID);
                }

                if (this.visual.QUANTITY_DOWN_ID)
                {
                    this.obQuantityDown = BX(this.visual.QUANTITY_DOWN_ID);
                }
            }

            if (this.visual.QUANTITY_LIMIT && this.showMaxQuantity !== 'N')
            {
                this.obQuantityLimit.all = BX(this.visual.QUANTITY_LIMIT);
                if (this.obQuantityLimit.all)
                {
                    this.obQuantityLimit.value = this.obQuantityLimit.all.querySelector('[data-entity="quantity-limit-value"]');
                    if (!this.obQuantityLimit.value)
                    {
                        this.obQuantityLimit.all = null;
                    }
                }
            }


            this.obBasketActions = BX(this.visual.BASKET_ACTIONS_ID);
            if (this.obBasketActions)
            {
                if (this.visual.BUY_ID)
                {
                    this.obBuyBtn = BX(this.visual.BUY_ID);
                }
            }

            if (this.showSubscription)
            {
                this.obSubscribe = BX(this.visual.SUBSCRIBE_ID);
            }
            
            if (!!this.visual.PICT_SLIDER && BX(this.visual.PICT_SLIDER))
            {
                var obSlier = BX(this.visual.PICT_SLIDER);
                
                var productGalleryThumbs = new Swiper(obSlier.querySelector('.gallery-thumbs'), {
                    slidesPerView: 'auto',
                    freeMode: true,
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,

                });
                
                var productGalleryTop = new Swiper(obSlier.querySelector('.gallery-top'), {

                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true,
					},
					on: {
						slideChangeTransitionStart: function () {
							$(window).scroll();
						}
					},
                    
                    thumbs: {
                        swiper: productGalleryThumbs
                    }
                });

                $(obSlier).find('.swiper-container-thumbs').find('.swiper-slide').on('mouseover', function () {
                    productGalleryTop.slideTo($(this).index());
                });
            }

            if (!!this.visual.WISHLIST_ID && BX(this.visual.WISHLIST_ID))
            {
                this.obWishlist = BX(this.visual.WISHLIST_ID);
            }
            
            if (this.obWishlist)
            {
                BX.bind(this.obWishlist, 'click', BX.delegate(this.wishlistChange, this));
            }

            if (this.errorCode === 0)
            {
                
                if (this.bigData)
                {
                    var links = BX.findChildren(this.obProduct, {tag: 'a'}, true);
                    if (links)
                    {
                        for (i in links)
                        {
                            if (links.hasOwnProperty(i))
                            {
                                if (links[i].getAttribute('href') == this.product.DETAIL_PAGE_URL)
                                {
                                    BX.bind(links[i], 'click', BX.proxy(this.rememberProductRecommendation, this));
                                }
                            }
                        }
                    }
                }

                if (this.showQuantity)
                {
                    var startEventName = this.isTouchDevice ? 'touchstart' : 'mousedown';
                    var endEventName = this.isTouchDevice ? 'touchend' : 'mouseup';

                    if (this.obQuantityUp)
                    {
                        BX.bind(this.obQuantityUp, startEventName, BX.proxy(this.startQuantityInterval, this));
                        BX.bind(this.obQuantityUp, endEventName, BX.proxy(this.clearQuantityInterval, this));
                        BX.bind(this.obQuantityUp, 'mouseout', BX.proxy(this.clearQuantityInterval, this));
                        BX.bind(this.obQuantityUp, 'click', BX.delegate(this.quantityUp, this));
                    }

                    if (this.obQuantityDown)
                    {
                        BX.bind(this.obQuantityDown, startEventName, BX.proxy(this.startQuantityInterval, this));
                        BX.bind(this.obQuantityDown, endEventName, BX.proxy(this.clearQuantityInterval, this));
                        BX.bind(this.obQuantityDown, 'mouseout', BX.proxy(this.clearQuantityInterval, this));
                        BX.bind(this.obQuantityDown, 'click', BX.delegate(this.quantityDown, this));
                    }

                    if (this.obQuantity)
                    {
                        BX.bind(this.obQuantity, 'change', BX.delegate(this.quantityChange, this));
                    }
                }
                
                

                switch (this.productType)
                {
                    case 0: // no catalog
                    case 1: // product
                    case 2: // set
                        
                        this.checkQuantityControls();

                        break;
                    case 3: // sku
                        break;
                }

                if (this.obBuyBtn)
                {
                    if (this.basketAction === 'ADD')
                    {
                        BX.bind(this.obBuyBtn, 'click', BX.proxy(this.add2Basket, this));
                    } else
                    {
                        BX.bind(this.obBuyBtn, 'click', BX.proxy(this.buyBasket, this));
                    }
                }

                if (this.useCompare)
                {
                    this.obCompare = BX(this.visual.COMPARE_LINK_ID);
                    if (this.obCompare)
                    {
                        BX.bind(this.obCompare, 'click', BX.proxy(this.compare, this));
                    }

                    BX.addCustomEvent('onCatalogDeleteCompare', BX.proxy(this.checkDeletedCompare, this));
                }
            }
        },

        setAnalyticsDataLayer: function (action)
        {
            if (!this.useEnhancedEcommerce || !this.dataLayerName)
                return;

            var item = {},
                    info = {},
                    variants = [],
                    i, k, j, propId, skuId, propValues;

            item = {
                        'id': this.product.id,
                        'name': this.product.name,
                        'price': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].PRICE,
            };

            switch (action)
            {
                case 'addToCart':
                    info = {
                        'event': 'addToCart',
                        'ecommerce': {
                            'currencyCode': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].CURRENCY || '',
                            'add': {
                                'products': [{
                                        'name': item.name || '',
                                        'id': item.id || '',
                                        'price': item.price || 0,
                                        'brand': item.brand || '',
                                        'category': item.category || '',
                                        'variant': item.variant || '',
                                        'quantity': this.showQuantity && this.obQuantity ? this.obQuantity.value : 1
                                    }]
                            }
                        }
                    };
                    break;
            }

            window[this.dataLayerName] = window[this.dataLayerName] || [];
            window[this.dataLayerName].push(info);
        },

        getCookie: function (name)
        {
            var matches = document.cookie.match(new RegExp(
                    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                    ));

            return matches ? decodeURIComponent(matches[1]) : null;
        },

        rememberProductRecommendation: function ()
        {
            // save to RCM_PRODUCT_LOG
            var cookieName = BX.cookie_prefix + '_RCM_PRODUCT_LOG',
                    cookie = this.getCookie(cookieName),
                    itemFound = false;

            var cItems = [],
                    cItem;

            if (cookie)
            {
                cItems = cookie.split('.');
            }

            var i = cItems.length;

            while (i--)
            {
                cItem = cItems[i].split('-');

                if (cItem[0] == this.product.id)
                {
                    // it's already in recommendations, update the date
                    cItem = cItems[i].split('-');

                    // update rcmId and date
                    cItem[1] = this.product.rcmId;
                    cItem[2] = BX.current_server_time;

                    cItems[i] = cItem.join('-');
                    itemFound = true;
                } else
                {
                    if ((BX.current_server_time - cItem[2]) > 3600 * 24 * 30)
                    {
                        cItems.splice(i, 1);
                    }
                }
            }

            if (!itemFound)
            {
                // add recommendation
                cItems.push([this.product.id, this.product.rcmId, BX.current_server_time].join('-'));
            }

            // serialize
            var plNewCookie = cItems.join('.'),
                    cookieDate = new Date(new Date().getTime() + 1000 * 3600 * 24 * 365 * 10).toUTCString();

            document.cookie = cookieName + "=" + plNewCookie + "; path=/; expires=" + cookieDate + "; domain=" + BX.cookie_domain;
        },

        startQuantityInterval: function () // leave
        {
            var target = BX.proxy_context;
            var func = target.id === this.visual.QUANTITY_DOWN_ID
                    ? BX.proxy(this.quantityDown, this)
                    : BX.proxy(this.quantityUp, this);

            this.quantityDelay = setTimeout(
                    BX.delegate(function () {
                        this.quantityTimer = setInterval(func, 150);
                    }, this),
                    300
                    );
        },

        clearQuantityInterval: function () // leave
        {
            clearTimeout(this.quantityDelay);
            clearInterval(this.quantityTimer);
        },

        quantityUp: function () // leave
        {
            var curValue = 0,
                    boolSet = true;

            if (this.errorCode === 0 && this.showQuantity && this.canBuy)
            {
                curValue = (this.isDblQuantity ? parseFloat(this.obQuantity.value) : parseInt(this.obQuantity.value, 10));
                if (!isNaN(curValue))
                {
                    curValue += this.stepQuantity;
                    if (this.checkQuantity)
                    {
                        if (curValue > this.maxQuantity)
                        {
                            boolSet = false;
                        }
                    }

                    if (boolSet)
                    {
                        if (this.isDblQuantity)
                        {
                            curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
                        }

                        this.obQuantity.value = curValue;

                        this.setPrice();
                    }
                }
            }
        },

        quantityDown: function () // leave
        {
            var curValue = 0,
                    boolSet = true;

            if (this.errorCode === 0 && this.showQuantity && this.canBuy)
            {
                curValue = (this.isDblQuantity ? parseFloat(this.obQuantity.value) : parseInt(this.obQuantity.value, 10));
                if (!isNaN(curValue))
                {
                    curValue -= this.stepQuantity;

                    this.checkPriceRange(curValue);

                    if (curValue < this.minQuantity)
                    {
                        boolSet = false;
                    }

                    if (boolSet)
                    {
                        if (this.isDblQuantity)
                        {
                            curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
                        }

                        this.obQuantity.value = curValue;

                        this.setPrice();
                    }
                }
            }
        },

        quantityChange: function () // leave
        {
            var curValue = 0,
                    intCount;

            if (this.errorCode === 0 && this.showQuantity)
            {
                if (this.canBuy)
                {
                    curValue = this.isDblQuantity ? parseFloat(this.obQuantity.value) : Math.round(this.obQuantity.value);
                    if (!isNaN(curValue))
                    {
                        if (this.checkQuantity)
                        {
                            if (curValue > this.maxQuantity)
                            {
                                curValue = this.maxQuantity;
                            }
                        }

                        this.checkPriceRange(curValue);

                        intCount = Math.floor(
                                Math.round(curValue * this.precisionFactor / this.stepQuantity) / this.precisionFactor
                                ) || 1;
                        curValue = (intCount <= 1 ? this.stepQuantity : intCount * this.stepQuantity);
                        curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;

                        if (curValue < this.minQuantity)
                        {
                            curValue = this.minQuantity;
                        }

                        this.obQuantity.value = curValue;
                    } else
                    {
                        this.obQuantity.value = this.minQuantity;
                    }
                } else
                {
                    this.obQuantity.value = this.minQuantity;
                }

                this.setPrice();
            }
        },

        quantitySet: function (index)
        {
            var resetQuantity, strLimit;

            var newOffer = this.offers[index],
                    oldOffer = this.offers[this.offerNum];

            if (this.errorCode === 0)
            {
                this.canBuy = newOffer.CAN_BUY;

                this.currentPriceMode = newOffer.ITEM_PRICE_MODE;
                this.currentPrices = newOffer.ITEM_PRICES;
                this.currentPriceSelected = newOffer.ITEM_PRICE_SELECTED;
                this.currentQuantityRanges = newOffer.ITEM_QUANTITY_RANGES;
                this.currentQuantityRangeSelected = newOffer.ITEM_QUANTITY_RANGE_SELECTED;

                if (this.canBuy)
                {
                    if (this.blockNodes.quantity)
                    {
                        BX.style(this.blockNodes.quantity, 'display', '');
                    }

                    if (this.obBasketActions)
                    {
                        BX.style(this.obBasketActions, 'display', '');
                    }

                    if (this.obNotAvail)
                    {
                        BX.style(this.obNotAvail, 'display', 'none');
                    }

                    if (this.obSubscribe)
                    {
                        BX.style(this.obSubscribe, 'display', 'none');
                    }
                } else
                {
                    if (this.blockNodes.quantity)
                    {
                        BX.style(this.blockNodes.quantity, 'display', 'none');
                    }

                    if (this.obBasketActions)
                    {
                        BX.style(this.obBasketActions, 'display', 'none');
                    }

                    if (this.obNotAvail)
                    {
                        BX.style(this.obNotAvail, 'display', '');
                    }

                    if (this.obSubscribe)
                    {
                        if (newOffer.CATALOG_SUBSCRIBE === 'Y')
                        {
                            BX.style(this.obSubscribe, 'display', '');
                            this.obSubscribe.setAttribute('data-item', newOffer.ID);
                            BX(this.visual.SUBSCRIBE_ID + '_hidden').click();
                        } else
                        {
                            BX.style(this.obSubscribe, 'display', 'none');
                        }
                    }
                }

                this.isDblQuantity = newOffer.QUANTITY_FLOAT;
                this.checkQuantity = newOffer.CHECK_QUANTITY;

                if (this.isDblQuantity)
                {
                    this.stepQuantity = Math.round(parseFloat(newOffer.STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor;
                    this.maxQuantity = parseFloat(newOffer.MAX_QUANTITY);
                    this.minQuantity = this.currentPriceMode === 'Q' ? parseFloat(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
                } else
                {
                    this.stepQuantity = parseInt(newOffer.STEP_QUANTITY, 10);
                    this.maxQuantity = parseInt(newOffer.MAX_QUANTITY, 10);
                    this.minQuantity = this.currentPriceMode === 'Q' ? parseInt(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
                }

                if (this.showQuantity)
                {
                    var isDifferentMinQuantity = oldOffer.ITEM_PRICES.length
                            && oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED]
                            && oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED].MIN_QUANTITY != this.minQuantity;

                    if (this.isDblQuantity)
                    {
                        resetQuantity = Math.round(parseFloat(oldOffer.STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor !== this.stepQuantity
                                || isDifferentMinQuantity
                                || oldOffer.MEASURE !== newOffer.MEASURE
                                || (
                                        this.checkQuantity
                                        && parseFloat(oldOffer.MAX_QUANTITY) > this.maxQuantity
                                        && parseFloat(this.obQuantity.value) > this.maxQuantity
                                        );
                    } else
                    {
                        resetQuantity = parseInt(oldOffer.STEP_QUANTITY, 10) !== this.stepQuantity
                                || isDifferentMinQuantity
                                || oldOffer.MEASURE !== newOffer.MEASURE
                                || (
                                        this.checkQuantity
                                        && parseInt(oldOffer.MAX_QUANTITY, 10) > this.maxQuantity
                                        && parseInt(this.obQuantity.value, 10) > this.maxQuantity
                                        );
                    }

                    this.obQuantity.disabled = !this.canBuy;

                    if (resetQuantity)
                    {
                        this.obQuantity.value = this.minQuantity;
                    }

                    if (this.obMeasure)
                    {
                        if (newOffer.MEASURE)
                        {
                            BX.adjust(this.obMeasure, {html: newOffer.MEASURE});
                        } else
                        {
                            BX.adjust(this.obMeasure, {html: ''});
                        }
                    }
                }

                if (this.obQuantityLimit.all)
                {
                    if (!this.checkQuantity || this.maxQuantity == 0)
                    {
                        BX.adjust(this.obQuantityLimit.value, {html: ''});
                        BX.adjust(this.obQuantityLimit.all, {style: {display: 'none'}});
                    } else
                    {
                        if (this.showMaxQuantity === 'M')
                        {
                            strLimit = (this.maxQuantity / this.stepQuantity >= this.relativeQuantityFactor)
                                    ? BX.message('RELATIVE_QUANTITY_MANY')
                                    : BX.message('RELATIVE_QUANTITY_FEW');
                        } else
                        {
                            strLimit = this.maxQuantity;

                            if (newOffer.MEASURE)
                            {
                                strLimit += (' ' + newOffer.MEASURE);
                            }
                        }

                        BX.adjust(this.obQuantityLimit.value, {html: strLimit});
                        BX.adjust(this.obQuantityLimit.all, {style: {display: ''}});
                    }
                }
            }
        },

        
        checkTouch: function (event)
        {
            if (!event || !event.changedTouches)
                return false;

            return event.changedTouches[0].identifier === this.touch.identifier;
        },

        touchStartEvent: function (event)
        {
            if (event.touches.length != 1)
                return;

            this.touch = event.changedTouches[0];
        },

        touchEndEvent: function (event)
        {
            if (!this.checkTouch(event))
                return;

            var deltaX = this.touch.pageX - event.changedTouches[0].pageX,
                    deltaY = this.touch.pageY - event.changedTouches[0].pageY;

            if (Math.abs(deltaX) >= Math.abs(deltaY) + 10)
            {
                if (deltaX > 0)
                {
                    this.slideNext();
                }

                if (deltaX < 0)
                {
                    this.slidePrev();
                }
            }
        },

        checkPriceRange: function (quantity) // leave
        {
            if (typeof quantity === 'undefined' || this.currentPriceMode != 'Q')
                return;

            var range, found = false;

            for (var i in this.currentQuantityRanges)
            {
                if (this.currentQuantityRanges.hasOwnProperty(i))
                {
                    range = this.currentQuantityRanges[i];

                    if (
                            parseInt(quantity) >= parseInt(range.SORT_FROM)
                            && (
                                    range.SORT_TO == 'INF'
                                    || parseInt(quantity) <= parseInt(range.SORT_TO)
                                    )
                            )
                    {
                        found = true;
                        this.currentQuantityRangeSelected = range.HASH;
                        break;
                    }
                }
            }

            if (!found && (range = this.getMinPriceRange()))
            {
                this.currentQuantityRangeSelected = range.HASH;
            }

            for (var k in this.currentPrices)
            {
                if (this.currentPrices.hasOwnProperty(k))
                {
                    if (this.currentPrices[k].QUANTITY_HASH == this.currentQuantityRangeSelected)
                    {
                        this.currentPriceSelected = k;
                        break;
                    }
                }
            }
        },

        getMinPriceRange: function () // leave
        {
            var range;

            for (var i in this.currentQuantityRanges)
            {
                if (this.currentQuantityRanges.hasOwnProperty(i))
                {
                    if (
                            !range
                            || parseInt(this.currentQuantityRanges[i].SORT_FROM) < parseInt(range.SORT_FROM)
                            )
                    {
                        range = this.currentQuantityRanges[i];
                    }
                }
            }

            return range;
        },

        checkQuantityControls: function () // leave
        {
            if (!this.obQuantity)
                return;

            var reachedTopLimit = this.checkQuantity && parseFloat(this.obQuantity.value) + this.stepQuantity > this.maxQuantity,
                    reachedBottomLimit = parseFloat(this.obQuantity.value) - this.stepQuantity < this.minQuantity;

            if (reachedTopLimit)
            {
                BX.addClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled');
            } else if (BX.hasClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled'))
            {
                BX.removeClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled');
            }

            if (reachedBottomLimit)
            {
                BX.addClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled');
            } else if (BX.hasClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled'))
            {
                BX.removeClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled');
            }

            if (reachedTopLimit && reachedBottomLimit)
            {
                this.obQuantity.setAttribute('disabled', 'disabled');
            } else
            {
                this.obQuantity.removeAttribute('disabled');
            }
        },

        setPrice: function () // leave
        {
            var obData, price;

            if (this.obQuantity)
            {
                this.checkPriceRange(this.obQuantity.value);
            }

            this.checkQuantityControls();

            price = this.currentPrices[this.currentPriceSelected];

            if (this.obPrice)
            {
                if (price)
                {
                    BX.adjust(this.obPrice, {html: BX.Currency.currencyFormat(price.RATIO_PRICE, price.CURRENCY, true)});
                } else
                {
                    BX.adjust(this.obPrice, {html: ''});
                }

                if (this.showOldPrice && this.obPriceOld)
                {
                    if (price && price.RATIO_PRICE !== price.RATIO_BASE_PRICE)
                    {
                        BX.adjust(this.obPriceOld, {
                            style: {display: ''},
                            html: BX.Currency.currencyFormat(price.RATIO_BASE_PRICE, price.CURRENCY, true)
                        });
                    } else
                    {
                        BX.adjust(this.obPriceOld, {
                            style: {display: 'none'},
                            html: ''
                        });
                    }
                }

                if (this.obPriceTotal)
                {
                    if (price && this.obQuantity && this.obQuantity.value != this.stepQuantity)
                    {
                        BX.adjust(this.obPriceTotal, {
                            html: BX.message('PRICE_TOTAL_PREFIX') + ' <strong>'
                                    + BX.Currency.currencyFormat(price.PRICE * this.obQuantity.value, price.CURRENCY, true)
                                    + '</strong>',
                            style: {display: ''}
                        });
                    } else
                    {
                        BX.adjust(this.obPriceTotal, {
                            html: '',
                            style: {display: 'none'}
                        });
                    }
                }

                if (this.showPercent)
                {
                    if (price && parseInt(price.DISCOUNT) > 0)
                    {
                        obData = {style: {display: ''}, html: -price.PERCENT + '%'};
                    } else
                    {
                        obData = {style: {display: 'none'}, html: ''};
                    }

                    if (this.obDscPerc)
                    {
                        BX.adjust(this.obDscPerc, obData);
                    }

                    if (this.obSecondDscPerc)
                    {
                        BX.adjust(this.obSecondDscPerc, obData);
                    }
                }
            }
        },
        
        wishlistChange: function (event) {
            
            var target = BX.getEventTarget(event);
            
            BX.ajax({
                method: 'POST',
                dataType: 'json',
                url: this.basketData.wishlist_url,
                onsuccess: BX.proxy(function (response) {
                    
                    if (typeof(response) == 'object')
                    {
                        
                        if (response.result == 'ok' && response.state == "added")
                        {
                            if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                            {
                                new window.Alpha.Messages({
                                    type: 'default',
                                    title: BX.message('ADD_TO_WISHLIST_TITLE'),
                                    message: this.product.name,
                                    url: !this.isWishList ? this.basketData.wishlist_page_url : '',
                                    urlText: 'WISHLIST'
                                });
                            }

                            BX.addClass(this.obWishlist, 'active');
                            if (this.isWishList && this.obProduct)
                            {
                                BX.removeClass(this.obProduct, 'disabled');
                                let productLink = this.obProduct.querySelector(".product-card-link");
                                productLink && $(productLink).css('pointer-events', 'auto');
                            }
                        }
                        else if (response.result == 'ok' && response.state == "deleted")
                        {
                            if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                            {
                                new window.Alpha.Messages({
                                    type: 'default',
                                    title: BX.message('DELETE_FROM_WISHLIST_TITLE'),
                                    message: this.product.name,
                                });
                            }
                            
                            BX.removeClass(this.obWishlist, 'active');
                            if (this.isWishList && this.obProduct)
                            {
                                BX.addClass(this.obProduct, 'disabled');
                                let productLink = this.obProduct.querySelector(".product-card-link");
                                productLink && $(productLink).css('pointer-events', 'none');
                            }
                        }
                        
                        BX.onCustomEvent('OnWishlistChange');

                    }
                    else
                    {
                        console.log("error change wishlist: ", response);
                    }
                    
                    
                }, this)
            });
        },
        
        setWishlist: function (state) {
            
            state = (state) || '';
            
            if (state == "active")
            {
                BX.addClass(this.obWishlist, 'active');
            }
            else
            {
                BX.removeClass(this.obWishlist, 'active');
            }
            
        },
       
        compare: function (event)
        {
            var checkbox = this.obCompare.querySelector('[data-entity="compare-checkbox"]'),
                    target = BX.getEventTarget(event),
                    checked = true;

            if (checkbox)
            {
                checked = target === checkbox ? checkbox.checked : !checkbox.checked;
            }

            var url = checked ? this.compareData.compareUrl : this.compareData.compareDeleteUrl,
                    compareLink;

            if (url)
            {
                if (target !== checkbox)
                {
                    BX.PreventDefault(event);
                    this.setCompared(checked);
                }

                switch (this.productType)
                {
                    case 0: // no catalog
                    case 1: // product
                    case 2: // set
                        compareLink = url.replace('#ID#', this.product.id.toString());
                        break;
                    case 3: // sku
                        compareLink = url.replace('#ID#', this.offers[this.offerNum].ID);
                        break;
                }

                BX.ajax({
                    method: 'POST',
                    dataType: checked ? 'json' : 'html',
                    url: compareLink + (compareLink.indexOf('?') !== -1 ? '&' : '?') + 'ajax_action=Y',
                    onsuccess: checked
                            ? BX.proxy(this.compareResult, this)
                            : BX.proxy(this.compareDeleteResult, this)
                });
            }
        },

        compareResult: function (result)
        {

            if (!BX.type.isPlainObject(result))
                return;

            if (this.offers.length > 0)
            {
                this.offers[this.offerNum].COMPARED = result.STATUS === 'OK';
            }

            if (result.STATUS === 'OK')
            {
                BX.onCustomEvent('OnCompareChange');
                
                if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                {
                    new window.Alpha.Messages({
                        type: 'addToCompare',
                        title: BX.message('COMPARE_TITLE'),
                        message: BX.message('COMPARE_MESSAGE_OK')
                    });
                }

            }
            else
            {
                if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                {
                    new window.Alpha.Messages({
                        type: 'error',
                        title: BX.message('COMPARE_TITLE'),
                        message: (result.MESSAGE ? result.MESSAGE : BX.message('COMPARE_UNKNOWN_ERROR'))
                    });
                }

            }

        },

        compareDeleteResult: function ()
        {
            BX.onCustomEvent('OnCompareChange');

            if (this.offers && this.offers.length)
            {
                this.offers[this.offerNum].COMPARED = false;
            }
        },

        setCompared: function (state)
        {
            if (!this.obCompare)
                return;

            var checkbox = this.obCompare.querySelector('[data-entity="compare-checkbox"]');
            if (checkbox)
            {
                checkbox.checked = state;
            }
        },

        setCompareInfo: function (comparedIds)
        {
            if (!BX.type.isArray(comparedIds))
                return;

            for (var i in this.offers)
            {
                if (this.offers.hasOwnProperty(i))
                {
                    this.offers[i].COMPARED = BX.util.in_array(this.offers[i].ID, comparedIds);
                }
            }
        },

        compareRedirect: function ()
        {
            if (this.compareData.comparePath)
            {
                location.href = this.compareData.comparePath;
            } else
            {
                this.obPopupWin.close();
            }
        },

        checkDeletedCompare: function (id)
        {
            switch (this.productType)
            {
                case 0: // no catalog
                case 1: // product
                case 2: // set
                    if (this.product.id == id)
                    {
                        this.setCompared(false);
                    }

                    break;
                case 3: // sku
                    var i = this.offers.length;
                    while (i--)
                    {
                        if (this.offers[i].ID == id)
                        {
                            this.offers[i].COMPARED = false;

                            if (this.offerNum == i)
                            {
                                this.setCompared(false);
                            }

                            break;
                        }
                    }
            }
        },

        initBasketUrl: function ()
        {
            this.basketUrl = (this.basketMode === 'ADD' ? this.basketData.add_url : this.basketData.buy_url);
            switch (this.productType)
            {
                case 1: // product
                case 2: // set
                    this.basketUrl = this.basketUrl.replace('#ID#', this.product.id.toString());
                    break;
                case 3: // sku
                    this.basketUrl = this.basketUrl.replace('#ID#', this.offers[this.offerNum].ID);
                    break;
            }
            this.basketParams = {
                'ajax_basket': 'Y'
            };
            if (this.showQuantity)
            {
                this.basketParams[this.basketData.quantity] = this.obQuantity.value;
            }
            if (this.basketData.sku_props)
            {
                this.basketParams[this.basketData.sku_props_var] = this.basketData.sku_props;
            }
        },

        fillBasketProps: function ()
        {
            if (!this.visual.BASKET_PROP_DIV)
            {
                return;
            }
            var
                    i = 0,
                    propCollection = null,
                    foundValues = false,
                    obBasketProps = null;

            if (this.basketData.useProps && !this.basketData.emptyProps)
            {
                if (this.obPopupWin && this.obPopupWin.contentContainer)
                {
                    obBasketProps = this.obPopupWin.contentContainer;
                }
            } else
            {
                obBasketProps = BX(this.visual.BASKET_PROP_DIV);
            }
            if (obBasketProps)
            {
                propCollection = obBasketProps.getElementsByTagName('select');
                if (propCollection && propCollection.length)
                {
                    for (i = 0; i < propCollection.length; i++)
                    {
                        if (!propCollection[i].disabled)
                        {
                            switch (propCollection[i].type.toLowerCase())
                            {
                                case 'select-one':
                                    this.basketParams[propCollection[i].name] = propCollection[i].value;
                                    foundValues = true;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                propCollection = obBasketProps.getElementsByTagName('input');
                if (propCollection && propCollection.length)
                {
                    for (i = 0; i < propCollection.length; i++)
                    {
                        if (!propCollection[i].disabled)
                        {
                            switch (propCollection[i].type.toLowerCase())
                            {
                                case 'hidden':
                                    this.basketParams[propCollection[i].name] = propCollection[i].value;
                                    foundValues = true;
                                    break;
                                case 'radio':
                                    if (propCollection[i].checked)
                                    {
                                        this.basketParams[propCollection[i].name] = propCollection[i].value;
                                        foundValues = true;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            if (!foundValues)
            {
                this.basketParams[this.basketData.props] = [];
                this.basketParams[this.basketData.props][0] = 0;
            }
        },

        add2Basket: function ()
        {
            this.basketMode = 'ADD';
            this.basket();
        },

        buyBasket: function ()
        {
            this.basketMode = 'BUY';
            this.basket();
        },

        sendToBasket: function ()
        {
            if (!this.canBuy)
            {
                return;
            }

            // check recommendation
            if (this.product && this.product.id && this.bigData)
            {
                this.rememberProductRecommendation();
            }

            this.initBasketUrl();
            this.fillBasketProps();
            BX.ajax({
                method: 'POST',
                dataType: 'json',
                url: this.basketUrl,
                data: this.basketParams,
                onsuccess: BX.proxy(this.basketResult, this)
            });
        },

        basket: function ()
        {
            var contentBasketProps = '';
            if (!this.canBuy)
            {
                return;
            }
            switch (this.productType)
            {
                case 1: // product
                case 2: // set
                    if (this.basketData.useProps && !this.basketData.emptyProps)
                    {
                        this.initPopupWindow();
                        this.obPopupWin.setTitleBar(BX.message('TITLE_BASKET_PROPS'));
                        if (BX(this.visual.BASKET_PROP_DIV))
                        {
                            contentBasketProps = BX(this.visual.BASKET_PROP_DIV).innerHTML;
                        }
                        this.obPopupWin.setContent(contentBasketProps);
                        this.obPopupWin.setButtons([
                            new BasketButton({
                                text: BX.message('BTN_MESSAGE_SEND_PROPS'),
                                events: {
                                    click: BX.delegate(this.sendToBasket, this)
                                }
                            })
                        ]);
                        this.obPopupWin.show();
                    } else
                    {
                        this.sendToBasket();
                    }
                    break;
                case 3: // sku
                    this.sendToBasket();
                    break;
            }
        },

        basketResult: function (arResult)
        {
            var strContent = '',
                    strPict = '',
                    successful,
                    buttons = [];


            if (!BX.type.isPlainObject(arResult))
                return;

            successful = arResult.STATUS === 'OK';

            if (successful)
            {
                this.setAnalyticsDataLayer('addToCart');
            }

            if (successful && this.basketAction === 'BUY')
            {
                this.basketRedirect();
            } else
            {
                
                if (successful)
                {
                    BX.onCustomEvent('OnBasketChange');

                    if (BX.findParent(this.obProduct, {className: 'bx_sale_gift_main_products'}, 10))
                    {
                        BX.onCustomEvent('onAddToBasketMainProduct', [this]);
                    }

                    
                    if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                    {
                        new window.Alpha.Messages({
                            type: 'addToBasket',
                            title: BX.message('TITLE_SUCCESSFUL'),
                            message: this.product.name,
                            image: this.product.picture,
                            url: this.basketData.basketUrl,
                            urlText: 'BASKET'
                        });
                    }

                    
                } else
                {
                    if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                    {
                        new window.Alpha.Messages({
                            type: 'error',
                            title: BX.message('TITLE_ERROR'),
                            message: (arResult.MESSAGE ? arResult.MESSAGE : BX.message('BASKET_UNKNOWN_ERROR')),
                        });
                    }
                }
                
            }
        },

        basketRedirect: function ()
        {
            location.href = (this.basketData.basketUrl ? this.basketData.basketUrl : BX.message('BASKET_URL'));
        },

        initPopupWindow: function ()
        {
            if (this.obPopupWin)
                return;

            this.obPopupWin = BX.PopupWindowManager.create('CatalogSectionBasket_' + this.visual.ID, null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay: true,
                closeByEsc: true,
                titleBar: true,
                closeIcon: true,
                contentColor: 'white',
                className: this.templateTheme ? 'bx-' + this.templateTheme : ''
            });
        }

    };
    
    //********
    //** catalog.item list view mode
    //********
    
    if (window.Alpha.CatalogListItem)
		return;

	var BasketButton = function(params)
	{
		BasketButton.superclass.constructor.apply(this, arguments);
		this.buttonNode = BX.create('button', {
			props: {className: 'btn btn-primary btn-buy btn-sm', id: this.id},
			style: typeof params.style === 'object' ? params.style : {},
			text: params.text,
			events: this.contextEvents
		});

		if (BX.browser.IsIE())
		{
			this.buttonNode.setAttribute("hideFocus", "hidefocus");
		}
	};
	BX.extend(BasketButton, BX.PopupWindowButton);

	window.Alpha.CatalogListItem = function (arParams)
	{
		this.productType = 0;
		this.showQuantity = true;
		this.showAbsent = true;
		this.secondPict = false;
		this.showOldPrice = false;
		this.showMaxQuantity = 'N';
		this.relativeQuantityFactor = 5;
		this.showPercent = false;
		this.showSkuProps = false;
		this.basketAction = 'ADD';
		this.showClosePopup = false;
		this.useCompare = false;
		this.showSubscription = false;
		this.visual = {
			ID: '',
			PICT_ID: '',
			SECOND_PICT_ID: '',
			PICT_SLIDER_ID: '',
			QUANTITY_ID: '',
			QUANTITY_UP_ID: '',
			QUANTITY_DOWN_ID: '',
			PRICE_ID: '',
			PRICE_OLD_ID: '',
			DSC_PERC: '',
			SECOND_DSC_PERC: '',
			DISPLAY_PROP_DIV: '',
			BASKET_PROP_DIV: '',
			SUBSCRIBE_ID: '',
            ITEM_CODE_ID: '',
            WISHLIST_ID: ''
		};
		this.product = {
			checkQuantity: false,
			maxQuantity: 0,
			stepQuantity: 1,
			isDblQuantity: false,
			canBuy: true,
			name: '',
			pict: {},
			id: 0,
			addUrl: '',
			buyUrl: ''
		};

		this.basketMode = '';
		this.basketData = {
			useProps: false,
			emptyProps: false,
			quantity: 'quantity',
			props: 'prop',
			basketUrl: '',
			sku_props: '',
			sku_props_var: 'basket_props',
			add_url: '',
			buy_url: '',
            wishlist_url: '',
            wishlist_page_url: BX.message('SITE_DIR') + 'wishlist/',
            add_wish_to_delayed: false
		};

		this.compareData = {
			compareUrl: '',
			compareDeleteUrl: '',
			comparePath: ''
		};

		this.defaultPict = {
			pict: null,
			secondPict: null
		};

		this.defaultSliderOptions = {
			interval: 3000,
			wrap: true
		};
		this.slider = {
			options: {},
			items: [],
			active: null,
			sliding: null,
			paused: null,
			interval: null,
			progress: null
		};
		this.touch = null;

		this.quantityDelay = null;
		this.quantityTimer = null;

		this.checkQuantity = false;
		this.maxQuantity = 0;
		this.minQuantity = 0;
		this.stepQuantity = 1;
		this.isDblQuantity = false;
		this.canBuy = true;
		this.precision = 6;
		this.precisionFactor = Math.pow(10, this.precision);
		this.bigData = false;
		this.fullDisplayMode = false;
		this.viewMode = '';
		this.templateTheme = '';

		this.currentPriceMode = '';
		this.currentPrices = [];
		this.currentPriceSelected = 0;
		this.currentQuantityRanges = [];
		this.currentQuantityRangeSelected = 0;

		this.offers = [];
		this.offerNum = 0;
		this.treeProps = [];
		this.selectedValues = {};

		this.obProduct = null;
		this.blockNodes = {};
		this.obQuantity = null;
		this.obQuantityUp = null;
		this.obQuantityDown = null;
		this.obQuantityLimit = {};
		this.obPict = null;
		this.obSecondPict = null;
		this.obPictSlider = null;
		this.obPictSliderIndicator = null;
		this.obPrice = null;
		this.obTree = null;
		this.obBuyBtn = null;
		this.obBasketActions = null;
		this.obNotAvail = null;
		this.obSubscribe = null;
		this.obDscPerc = null;
		this.obSecondDscPerc = null;
		this.obSkuProps = null;
		this.obMeasure = null;
		this.obCompare = null;
                this.obWishlist = null;

		this.obPopupWin = null;
		this.basketUrl = '';
		this.basketParams = {};
		this.isTouchDevice = BX.hasClass(document.documentElement, 'bx-touch');
		this.hoverTimer = null;
		this.hoverStateChangeForbidden = false;
		this.mouseX = null;
		this.mouseY = null;

		this.useEnhancedEcommerce = false;
		this.dataLayerName = 'dataLayer';
		this.brandProperty = false;

		this.errorCode = 0;

		if (typeof arParams === 'object')
		{
			if (arParams.PRODUCT_TYPE)
			{
				this.productType = parseInt(arParams.PRODUCT_TYPE, 10);
			}

			this.showQuantity = arParams.SHOW_QUANTITY;
			this.showAbsent = arParams.SHOW_ABSENT;
			this.secondPict = arParams.SECOND_PICT;
			this.showOldPrice = arParams.SHOW_OLD_PRICE;
			this.showMaxQuantity = arParams.SHOW_MAX_QUANTITY;
			this.relativeQuantityFactor = parseInt(arParams.RELATIVE_QUANTITY_FACTOR);
			this.showPercent = arParams.SHOW_DISCOUNT_PERCENT;
			this.showSkuProps = arParams.SHOW_SKU_PROPS;
			this.showSubscription = arParams.USE_SUBSCRIBE;

			if (arParams.ADD_TO_BASKET_ACTION)
			{
				this.basketAction = arParams.ADD_TO_BASKET_ACTION;
			}

			this.showClosePopup = arParams.SHOW_CLOSE_POPUP;
			this.useCompare = arParams.DISPLAY_COMPARE;
			this.fullDisplayMode = arParams.PRODUCT_DISPLAY_MODE === 'Y';
			this.bigData = arParams.BIG_DATA;
			this.viewMode = arParams.VIEW_MODE || '';
			this.templateTheme = arParams.TEMPLATE_THEME || '';
			this.useEnhancedEcommerce = arParams.USE_ENHANCED_ECOMMERCE === 'Y';
			this.dataLayerName = arParams.DATA_LAYER_NAME;
			this.brandProperty = arParams.BRAND_PROPERTY;

			this.visual = arParams.VISUAL;

			switch (this.productType)
			{
				case 0: // no catalog
				case 1: // product
				case 2: // set
					if (arParams.PRODUCT && typeof arParams.PRODUCT === 'object')
					{
						this.currentPriceMode = arParams.PRODUCT.ITEM_PRICE_MODE;
						this.currentPrices = arParams.PRODUCT.ITEM_PRICES;
						this.currentPriceSelected = arParams.PRODUCT.ITEM_PRICE_SELECTED;
						this.currentQuantityRanges = arParams.PRODUCT.ITEM_QUANTITY_RANGES;
						this.currentQuantityRangeSelected = arParams.PRODUCT.ITEM_QUANTITY_RANGE_SELECTED;

						if (this.showQuantity)
						{
							this.product.checkQuantity = arParams.PRODUCT.CHECK_QUANTITY;
							this.product.isDblQuantity = arParams.PRODUCT.QUANTITY_FLOAT;

							if (this.product.checkQuantity)
							{
								this.product.maxQuantity = (this.product.isDblQuantity ? parseFloat(arParams.PRODUCT.MAX_QUANTITY) : parseInt(arParams.PRODUCT.MAX_QUANTITY, 10));
							}

							this.product.stepQuantity = (this.product.isDblQuantity ? parseFloat(arParams.PRODUCT.STEP_QUANTITY) : parseInt(arParams.PRODUCT.STEP_QUANTITY, 10));

							this.checkQuantity = this.product.checkQuantity;
							this.isDblQuantity = this.product.isDblQuantity;
							this.stepQuantity = this.product.stepQuantity;
							this.maxQuantity = this.product.maxQuantity;
							this.minQuantity = this.currentPriceMode === 'Q'
								? parseFloat(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY)
								: this.stepQuantity;

							if (this.isDblQuantity)
							{
								this.stepQuantity = Math.round(this.stepQuantity * this.precisionFactor) / this.precisionFactor;
							}
						}

						this.product.canBuy = arParams.PRODUCT.CAN_BUY;

						if (arParams.PRODUCT.MORE_PHOTO_COUNT)
						{
							this.product.morePhotoCount = arParams.PRODUCT.MORE_PHOTO_COUNT;
							this.product.morePhoto = arParams.PRODUCT.MORE_PHOTO;
						}

						if (arParams.PRODUCT.RCM_ID)
						{
							this.product.rcmId = arParams.PRODUCT.RCM_ID;
						}

						this.canBuy = this.product.canBuy;
						this.product.name = arParams.PRODUCT.NAME;
						this.product.pict = arParams.PRODUCT.PICT;
						this.product.id = arParams.PRODUCT.ID;
						this.product.DETAIL_PAGE_URL = arParams.PRODUCT.DETAIL_PAGE_URL;

						if (arParams.PRODUCT.ADD_URL)
						{
							this.product.addUrl = arParams.PRODUCT.ADD_URL;
						}

						if (arParams.PRODUCT.BUY_URL)
						{
							this.product.buyUrl = arParams.PRODUCT.BUY_URL;
						}
                                                
						if (arParams.BASKET && typeof arParams.BASKET === 'object')
						{
							this.basketData.useProps = arParams.BASKET.ADD_PROPS;
							this.basketData.emptyProps = arParams.BASKET.EMPTY_PROPS;
						}
					}
					else
					{
						this.errorCode = -1;
					}

					break;
				case 3: // sku
					if (arParams.PRODUCT && typeof arParams.PRODUCT === 'object')
					{
						this.product.name = arParams.PRODUCT.NAME;
						this.product.id = arParams.PRODUCT.ID;
						this.product.DETAIL_PAGE_URL = arParams.PRODUCT.DETAIL_PAGE_URL;
						this.product.morePhotoCount = arParams.PRODUCT.MORE_PHOTO_COUNT;
						this.product.morePhoto = arParams.PRODUCT.MORE_PHOTO;

						if (arParams.PRODUCT.RCM_ID)
						{
							this.product.rcmId = arParams.PRODUCT.RCM_ID;
						}
					}

					if (arParams.OFFERS && BX.type.isArray(arParams.OFFERS))
					{
						this.offers = arParams.OFFERS;
						this.offerNum = 0;

						if (arParams.OFFER_SELECTED)
						{
							this.offerNum = parseInt(arParams.OFFER_SELECTED, 10);
						}

						if (isNaN(this.offerNum))
						{
							this.offerNum = 0;
						}

						if (arParams.TREE_PROPS)
						{
							this.treeProps = arParams.TREE_PROPS;
						}

						if (arParams.DEFAULT_PICTURE)
						{
							this.defaultPict.pict = arParams.DEFAULT_PICTURE.PICTURE;
							this.defaultPict.secondPict = arParams.DEFAULT_PICTURE.PICTURE_SECOND;
						}
					}

					break;
				default:
					this.errorCode = -1;
			}
                        
                        
                        
			if (arParams.BASKET && typeof arParams.BASKET === 'object')
			{
				if (arParams.BASKET.QUANTITY)
				{
					this.basketData.quantity = arParams.BASKET.QUANTITY;
				}

				if (arParams.BASKET.PROPS)
				{
					this.basketData.props = arParams.BASKET.PROPS;
				}

				if (arParams.BASKET.BASKET_URL)
				{
					this.basketData.basketUrl = arParams.BASKET.BASKET_URL;
				}

				if (3 === this.productType)
				{
					if (arParams.BASKET.SKU_PROPS)
					{
						this.basketData.sku_props = arParams.BASKET.SKU_PROPS;
					}
				}

				if (arParams.BASKET.ADD_URL_TEMPLATE)
				{
					this.basketData.add_url = arParams.BASKET.ADD_URL_TEMPLATE;
				}

				if (arParams.BASKET.BUY_URL_TEMPLATE)
				{
					this.basketData.buy_url = arParams.BASKET.BUY_URL_TEMPLATE;
				}
                                
                if (arParams.BASKET.WISHLIST_URL_TEMPLATE)
                {
                    this.basketData.wishlist_url = arParams.BASKET.WISHLIST_URL_TEMPLATE;
                    if (arParams.BASKET.WISHLIST_PAGE_URL && arParams.BASKET.WISHLIST_PAGE_URL.length > 0)
                        this.basketData.wishlist_page_url = arParams.BASKET.WISHLIST_PAGE_URL;
                    if (arParams.BASKET.ADD_WISH_TO_DELAYED === "Y")
                        this.basketData.add_wish_to_delayed = true;
                }

				if (this.basketData.add_url === '' && this.basketData.buy_url === '')
				{
					this.errorCode = -1024;
				}
			}

			if (this.useCompare)
			{
				if (arParams.COMPARE && typeof arParams.COMPARE === 'object')
				{
					if (arParams.COMPARE.COMPARE_PATH)
					{
						this.compareData.comparePath = arParams.COMPARE.COMPARE_PATH;
					}

					if (arParams.COMPARE.COMPARE_URL_TEMPLATE)
					{
						this.compareData.compareUrl = arParams.COMPARE.COMPARE_URL_TEMPLATE;
					}
					else
					{
						this.useCompare = false;
					}

					if (arParams.COMPARE.COMPARE_DELETE_URL_TEMPLATE)
					{
						this.compareData.compareDeleteUrl = arParams.COMPARE.COMPARE_DELETE_URL_TEMPLATE;
					}
					else
					{
						this.useCompare = false;
					}
				}
				else
				{
					this.useCompare = false;
				}
			}
		}
                
                

		if (this.errorCode === 0)
		{
			BX.ready(BX.delegate(this.init,this));
		}
	};

	window.Alpha.CatalogListItem.prototype = {
		init: function()
		{
			var i = 0,
				treeItems = null;

			this.obProduct = BX(this.visual.ID);
			if (!this.obProduct)
			{
				this.errorCode = -1;
			}

			this.obPrice = BX(this.visual.PRICE_ID);
			this.obPriceOld = BX(this.visual.PRICE_OLD_ID);
			this.obPriceTotal = BX(this.visual.PRICE_TOTAL_ID);
			if (!this.obPrice)
			{
				this.errorCode = -16;
			}
                        
                        
                        
                        this.obPictSlider = BX(this.visual.PICT_SLIDER_ID);

			if (this.showQuantity && this.visual.QUANTITY_ID)
			{
				this.obQuantity = BX(this.visual.QUANTITY_ID);
				this.blockNodes.quantity = this.obProduct.querySelector('[data-entity="quantity-block"]');

				if (!this.isTouchDevice)
				{
					BX.bind(this.obQuantity, 'focus', BX.proxy(this.onFocus, this));
					BX.bind(this.obQuantity, 'blur', BX.proxy(this.onBlur, this));
				}

				if (this.visual.QUANTITY_UP_ID)
				{
					this.obQuantityUp = BX(this.visual.QUANTITY_UP_ID);
				}

				if (this.visual.QUANTITY_DOWN_ID)
				{
					this.obQuantityDown = BX(this.visual.QUANTITY_DOWN_ID);
				}
			}

			if (this.visual.QUANTITY_LIMIT && this.showMaxQuantity !== 'N')
			{
				this.obQuantityLimit.all = BX(this.visual.QUANTITY_LIMIT);
				if (this.obQuantityLimit.all)
				{
					this.obQuantityLimit.value = this.obQuantityLimit.all.querySelector('[data-entity="quantity-limit-value"]');
					if (!this.obQuantityLimit.value)
					{
						this.obQuantityLimit.all = null;
					}
				}
			}

			if (this.productType === 3 && this.fullDisplayMode)
			{
				if (this.visual.TREE_ID)
				{
					this.obTree = BX(this.visual.TREE_ID);
					if (!this.obTree)
					{
						this.errorCode = -256;
					}
				}

				if (this.visual.QUANTITY_MEASURE)
				{
					this.obMeasure = BX(this.visual.QUANTITY_MEASURE);
				}
			}

			this.obBasketActions = BX(this.visual.BASKET_ACTIONS_ID);
			if (this.obBasketActions)
			{
				if (this.visual.BUY_ID)
				{
					this.obBuyBtn = BX(this.visual.BUY_ID);
				}
			}

			this.obNotAvail = BX(this.visual.NOT_AVAILABLE_MESS);

			if (this.showSubscription)
			{
				this.obSubscribe = BX(this.visual.SUBSCRIBE_ID);
			}
                        
                        if (!!this.visual.WISHLIST_ID && BX(this.visual.WISHLIST_ID))
                        {
                            this.obWishlist = BX(this.visual.WISHLIST_ID);
                        }
                        
                        if (this.obWishlist)
                        {
                            BX.bind(this.obWishlist, 'click', BX.delegate(this.wishlistChange, this));
                        }

			if (this.showPercent)
			{
				if (this.visual.DSC_PERC)
				{
					this.obDscPerc = BX(this.visual.DSC_PERC);
				}
				if (this.secondPict && this.visual.SECOND_DSC_PERC)
				{
					this.obSecondDscPerc = BX(this.visual.SECOND_DSC_PERC);
				}
			}

			if (this.showSkuProps)
			{
				if (this.visual.DISPLAY_PROP_DIV)
				{
					this.obSkuProps = BX(this.visual.DISPLAY_PROP_DIV);
				}
			}
                        
			if (this.errorCode === 0)
			{
				

				if (this.bigData)
				{
					var links = BX.findChildren(this.obProduct, {tag:'a'}, true);
					if (links)
					{
						for (i in links)
						{
							if (links.hasOwnProperty(i))
							{
								if (links[i].getAttribute('href') == this.product.DETAIL_PAGE_URL)
								{
									BX.bind(links[i], 'click', BX.proxy(this.rememberProductRecommendation, this));
								}
							}
						}
					}
				}

				if (this.showQuantity)
				{
					var startEventName = this.isTouchDevice ? 'touchstart' : 'mousedown';
					var endEventName = this.isTouchDevice ? 'touchend' : 'mouseup';

					if (this.obQuantityUp)
					{
						BX.bind(this.obQuantityUp, startEventName, BX.proxy(this.startQuantityInterval, this));
						BX.bind(this.obQuantityUp, endEventName, BX.proxy(this.clearQuantityInterval, this));
						BX.bind(this.obQuantityUp, 'mouseout', BX.proxy(this.clearQuantityInterval, this));
						BX.bind(this.obQuantityUp, 'click', BX.delegate(this.quantityUp, this));
					}

					if (this.obQuantityDown)
					{
						BX.bind(this.obQuantityDown, startEventName, BX.proxy(this.startQuantityInterval, this));
						BX.bind(this.obQuantityDown, endEventName, BX.proxy(this.clearQuantityInterval, this));
						BX.bind(this.obQuantityDown, 'mouseout', BX.proxy(this.clearQuantityInterval, this));
						BX.bind(this.obQuantityDown, 'click', BX.delegate(this.quantityDown, this));
					}

					if (this.obQuantity)
					{
						BX.bind(this.obQuantity, 'change', BX.delegate(this.quantityChange, this));
					}
				}

				switch (this.productType)
				{
					case 0: // no catalog
					case 1: // product
					case 2: // set
						
                                                this.initializeSlider();
						this.checkQuantityControls();

						break;
					case 3: // sku
						if (this.offers.length > 0)
						{
							treeItems = BX.findChildren(this.obTree, {tagName: 'a'}, true);
                                                        
							if (treeItems && treeItems.length)
							{
								for (i = 0; i < treeItems.length; i++)
								{
									BX.bind(treeItems[i], 'click', BX.delegate(this.selectOfferProp, this));
								}
							}

							this.setCurrent();
						}
						else
						{
							this.initializeSlider();
						}

						break;
				}

				if (this.obBuyBtn)
				{
					if (this.basketAction === 'ADD')
					{
						BX.bind(this.obBuyBtn, 'click', BX.proxy(this.add2Basket, this));
					}
					else
					{
						BX.bind(this.obBuyBtn, 'click', BX.proxy(this.buyBasket, this));
					}
				}

				if (this.useCompare)
				{
					this.obCompare = BX(this.visual.COMPARE_LINK_ID);
					if (this.obCompare)
					{
						BX.bind(this.obCompare, 'click', BX.proxy(this.compare, this));
					}

					BX.addCustomEvent('onCatalogDeleteCompare', BX.proxy(this.checkDeletedCompare, this));
				}
			}
		},

		setAnalyticsDataLayer: function(action)
		{
			if (!this.useEnhancedEcommerce || !this.dataLayerName)
				return;

			var item = {},
				info = {},
				variants = [],
				i, k, j, propId, skuId, propValues;

			switch (this.productType)
			{
				case 0: //no catalog
				case 1: //product
				case 2: //set
					item = {
						'id': this.product.id,
						'name': this.product.name,
						'price': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].PRICE,
						'brand': BX.type.isArray(this.brandProperty) ? this.brandProperty.join('/') : this.brandProperty
					};
					break;
				case 3: //sku
					for (i in this.offers[this.offerNum].TREE)
					{
						if (this.offers[this.offerNum].TREE.hasOwnProperty(i))
						{
							propId = i.substring(5);
							skuId = this.offers[this.offerNum].TREE[i];

							for (k in this.treeProps)
							{
								if (this.treeProps.hasOwnProperty(k) && this.treeProps[k].ID == propId)
								{
									for (j in this.treeProps[k].VALUES)
									{
										propValues = this.treeProps[k].VALUES[j];
										if (propValues.ID == skuId)
										{
											variants.push(propValues.NAME);
											break;
										}
									}

								}
							}
						}
					}

					item = {
						'id': this.offers[this.offerNum].ID,
						'name': this.offers[this.offerNum].NAME,
						'price': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].PRICE,
						'brand': BX.type.isArray(this.brandProperty) ? this.brandProperty.join('/') : this.brandProperty,
						'variant': variants.join('/')
					};
					break;
			}

			switch (action)
			{
				case 'addToCart':
					info = {
						'event': 'addToCart',
						'ecommerce': {
							'currencyCode': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].CURRENCY || '',
							'add': {
								'products': [{
									'name': item.name || '',
									'id': item.id || '',
									'price': item.price || 0,
									'brand': item.brand || '',
									'category': item.category || '',
									'variant': item.variant || '',
									'quantity': this.showQuantity && this.obQuantity ? this.obQuantity.value : 1
								}]
							}
						}
					};
					break;
			}

			window[this.dataLayerName] = window[this.dataLayerName] || [];
			window[this.dataLayerName].push(info);
		},

		
		getCookie: function(name)
		{
			var matches = document.cookie.match(new RegExp(
				"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
			));

			return matches ? decodeURIComponent(matches[1]) : null;
		},

		rememberProductRecommendation: function()
		{
			// save to RCM_PRODUCT_LOG
			var cookieName = BX.cookie_prefix + '_RCM_PRODUCT_LOG',
				cookie = this.getCookie(cookieName),
				itemFound = false;

			var cItems = [],
				cItem;

			if (cookie)
			{
				cItems = cookie.split('.');
			}

			var i = cItems.length;

			while (i--)
			{
				cItem = cItems[i].split('-');

				if (cItem[0] == this.product.id)
				{
					// it's already in recommendations, update the date
					cItem = cItems[i].split('-');

					// update rcmId and date
					cItem[1] = this.product.rcmId;
					cItem[2] = BX.current_server_time;

					cItems[i] = cItem.join('-');
					itemFound = true;
				}
				else
				{
					if ((BX.current_server_time - cItem[2]) > 3600 * 24 * 30)
					{
						cItems.splice(i, 1);
					}
				}
			}

			if (!itemFound)
			{
				// add recommendation
				cItems.push([this.product.id, this.product.rcmId, BX.current_server_time].join('-'));
			}

			// serialize
			var plNewCookie = cItems.join('.'),
				cookieDate = new Date(new Date().getTime() + 1000 * 3600 * 24 * 365 * 10).toUTCString();

			document.cookie = cookieName + "=" + plNewCookie + "; path=/; expires=" + cookieDate + "; domain=" + BX.cookie_domain;
		},

		startQuantityInterval: function()
		{
			var target = BX.proxy_context;
			var func = target.id === this.visual.QUANTITY_DOWN_ID
				? BX.proxy(this.quantityDown, this)
				: BX.proxy(this.quantityUp, this);

			this.quantityDelay = setTimeout(
				BX.delegate(function() {
					this.quantityTimer = setInterval(func, 150);
				}, this),
				300
			);
		},

		clearQuantityInterval: function()
		{
			clearTimeout(this.quantityDelay);
			clearInterval(this.quantityTimer);
		},

		quantityUp: function()
		{
			var curValue = 0,
				boolSet = true;

			if (this.errorCode === 0 && this.showQuantity && this.canBuy)
			{
				curValue = (this.isDblQuantity ? parseFloat(this.obQuantity.value) : parseInt(this.obQuantity.value, 10));
				if (!isNaN(curValue))
				{
					curValue += this.stepQuantity;
					if (this.checkQuantity)
					{
						if (curValue > this.maxQuantity)
						{
							boolSet = false;
						}
					}

					if (boolSet)
					{
						if (this.isDblQuantity)
						{
							curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
						}

						this.obQuantity.value = curValue;

						this.setPrice();
					}
				}
			}
		},

		quantityDown: function()
		{
			var curValue = 0,
				boolSet = true;

			if (this.errorCode === 0 && this.showQuantity && this.canBuy)
			{
				curValue = (this.isDblQuantity ? parseFloat(this.obQuantity.value) : parseInt(this.obQuantity.value, 10));
				if (!isNaN(curValue))
				{
					curValue -= this.stepQuantity;

					this.checkPriceRange(curValue);

					if (curValue < this.minQuantity)
					{
						boolSet = false;
					}

					if (boolSet)
					{
						if (this.isDblQuantity)
						{
							curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
						}

						this.obQuantity.value = curValue;

						this.setPrice();
					}
				}
			}
		},

		quantityChange: function()
		{
			var curValue = 0,
				intCount;

			if (this.errorCode === 0 && this.showQuantity)
			{
				if (this.canBuy)
				{
					curValue = this.isDblQuantity ? parseFloat(this.obQuantity.value) : Math.round(this.obQuantity.value);
					if (!isNaN(curValue))
					{
						if (this.checkQuantity)
						{
							if (curValue > this.maxQuantity)
							{
								curValue = this.maxQuantity;
							}
						}

						this.checkPriceRange(curValue);

						intCount = Math.floor(
							Math.round(curValue * this.precisionFactor / this.stepQuantity) / this.precisionFactor
						) || 1;
						curValue = (intCount <= 1 ? this.stepQuantity : intCount * this.stepQuantity);
						curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;

						if (curValue < this.minQuantity)
						{
							curValue = this.minQuantity;
						}

						this.obQuantity.value = curValue;
					}
					else
					{
						this.obQuantity.value = this.minQuantity;
					}
				}
				else
				{
					this.obQuantity.value = this.minQuantity;
				}

				this.setPrice();
			}
		},

		quantitySet: function(index)
		{
			var resetQuantity, strLimit;
			
			var newOffer = this.offers[index],
				oldOffer = this.offers[this.offerNum];

			if (this.errorCode === 0)
			{
				this.canBuy = newOffer.CAN_BUY;

				this.currentPriceMode = newOffer.ITEM_PRICE_MODE;
				this.currentPrices = newOffer.ITEM_PRICES;
				this.currentPriceSelected = newOffer.ITEM_PRICE_SELECTED;
				this.currentQuantityRanges = newOffer.ITEM_QUANTITY_RANGES;
				this.currentQuantityRangeSelected = newOffer.ITEM_QUANTITY_RANGE_SELECTED;

				if (this.canBuy)
				{
					if (this.blockNodes.quantity)
					{
						BX.style(this.blockNodes.quantity, 'display', '');
					}

					if (this.obBasketActions)
					{
						BX.style(this.obBasketActions, 'display', '');
					}

					if (this.obNotAvail)
					{
						BX.style(this.obNotAvail, 'display', 'none');
					}

					if (this.obSubscribe)
					{
						BX.style(this.obSubscribe, 'display', 'none');
					}
				}
				else
				{
					if (this.blockNodes.quantity)
					{
						BX.style(this.blockNodes.quantity, 'display', 'none');
					}

					if (this.obBasketActions)
					{
						BX.style(this.obBasketActions, 'display', 'none');
					}

					if (this.obNotAvail)
					{
						BX.style(this.obNotAvail, 'display', '');
					}

					if (this.obSubscribe)
					{
						if (newOffer.CATALOG_SUBSCRIBE === 'Y')
						{
							BX.style(this.obSubscribe, 'display', '');
							this.obSubscribe.setAttribute('data-item', newOffer.ID);
							BX(this.visual.SUBSCRIBE_ID + '_hidden').click();
						}
						else
						{
							BX.style(this.obSubscribe, 'display', 'none');
						}
					}
				}

				this.isDblQuantity = newOffer.QUANTITY_FLOAT;
				this.checkQuantity = newOffer.CHECK_QUANTITY;

				if (this.isDblQuantity)
				{
					this.stepQuantity = Math.round(parseFloat(newOffer.STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor;
					this.maxQuantity = parseFloat(newOffer.MAX_QUANTITY);
					this.minQuantity = this.currentPriceMode === 'Q' ? parseFloat(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
				}
				else
				{
					this.stepQuantity = parseInt(newOffer.STEP_QUANTITY, 10);
					this.maxQuantity = parseInt(newOffer.MAX_QUANTITY, 10);
					this.minQuantity = this.currentPriceMode === 'Q' ? parseInt(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
				}

				if (this.showQuantity)
				{
					var isDifferentMinQuantity = oldOffer.ITEM_PRICES.length
						&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED]
						&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED].MIN_QUANTITY != this.minQuantity;

					if (this.isDblQuantity)
					{
						resetQuantity = Math.round(parseFloat(oldOffer.STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor !== this.stepQuantity
							|| isDifferentMinQuantity
							|| oldOffer.MEASURE !== newOffer.MEASURE
							|| (
								this.checkQuantity
								&& parseFloat(oldOffer.MAX_QUANTITY) > this.maxQuantity
								&& parseFloat(this.obQuantity.value) > this.maxQuantity
							);
					}
					else
					{
						resetQuantity = parseInt(oldOffer.STEP_QUANTITY, 10) !== this.stepQuantity
							|| isDifferentMinQuantity
							|| oldOffer.MEASURE !== newOffer.MEASURE
							|| (
								this.checkQuantity
								&& parseInt(oldOffer.MAX_QUANTITY, 10) > this.maxQuantity
								&& parseInt(this.obQuantity.value, 10) > this.maxQuantity
							);
					}

					this.obQuantity.disabled = !this.canBuy;

					if (resetQuantity)
					{
						this.obQuantity.value = this.minQuantity;
					}

					if (this.obMeasure)
					{
						if (newOffer.MEASURE)
						{
							BX.adjust(this.obMeasure, {html: newOffer.MEASURE});
						}
						else
						{
							BX.adjust(this.obMeasure, {html: ''});
						}
					}
				}

				if (this.obQuantityLimit.all)
				{
					if (!this.checkQuantity || this.maxQuantity == 0)
					{
						BX.adjust(this.obQuantityLimit.value, {html: ''});
						BX.adjust(this.obQuantityLimit.all, {style: {display: 'none'}});
					}
					else
					{
						if (this.showMaxQuantity === 'M')
						{
							strLimit = (this.maxQuantity / this.stepQuantity >= this.relativeQuantityFactor)
								? BX.message('RELATIVE_QUANTITY_MANY')
								: BX.message('RELATIVE_QUANTITY_FEW');
						}
						else
						{
							strLimit = this.maxQuantity;

							if (newOffer.MEASURE)
							{
								strLimit += (' ' + newOffer.MEASURE);
							}
						}

						BX.adjust(this.obQuantityLimit.value, {html: strLimit});
						BX.adjust(this.obQuantityLimit.all, {style: {display: ''}});
					}
				}
			}
		},

		initializeSlider: function()
		{
                    
                    if (!!this.obPictSlider)
                    {
                        var productGalleryThumbs = new Swiper(this.obPictSlider.querySelector('.gallery-thumbs'), {
                            slidesPerView: 'auto',
                            freeMode: true,
                            watchSlidesVisibility: true,
                            watchSlidesProgress: true,

                        });

                        var productGalleryTop = new Swiper(this.obPictSlider.querySelector('.gallery-top'), {

                            pagination: {
                                el: '.swiper-pagination',
                                clickable: true,
                            },

                            thumbs: {
                                swiper: productGalleryThumbs
                            }
                        });

                        $(this.obPictSlider).find('.swiper-container-thumbs').find('.swiper-slide').on('mouseover', function () {
                            productGalleryTop.slideTo($(this).index());
                        });
                    }
		},

		

		eq: function(obj, i)
		{
			var len = obj.length,
				j = +i + (i < 0 ? len : 0);

			return j >= 0 && j < len ? obj[j] : {};
		},

		selectOfferProp: function()
		{
			var i = 0,
				value = '',
				strTreeValue = '',
				arTreeItem = [],
				rowItems = null,
				target = BX.proxy_context;

			if (target && target.hasAttribute('data-treevalue'))
			{
				if (BX.hasClass(target, 'selected'))
					return;

				strTreeValue = target.getAttribute('data-treevalue');
				arTreeItem = strTreeValue.split('_');
				if (this.searchOfferPropIndex(arTreeItem[0], arTreeItem[1]))
				{
					rowItems = BX.findChildren(target.parentNode, {tagName: 'a'}, false);
					if (rowItems && 0 < rowItems.length)
					{
						for (i = 0; i < rowItems.length; i++)
						{
							value = rowItems[i].getAttribute('data-onevalue');
							if (value === arTreeItem[1])
							{
								BX.addClass(rowItems[i], 'selected');
							}
							else
							{
								BX.removeClass(rowItems[i], 'selected');
							}
						}
					}
				}
			}
		},

		searchOfferPropIndex: function(strPropID, strPropValue)
		{
			var strName = '',
				arShowValues = false,
				i, j,
				arCanBuyValues = [],
				allValues = [],
				index = -1,
				arFilter = {},
				tmpFilter = [];

			for (i = 0; i < this.treeProps.length; i++)
			{
				if (this.treeProps[i].ID === strPropID)
				{
					index = i;
					break;
				}
			}

			if (-1 < index)
			{
				for (i = 0; i < index; i++)
				{
					strName = 'PROP_'+this.treeProps[i].ID;
					arFilter[strName] = this.selectedValues[strName];
				}
				strName = 'PROP_'+this.treeProps[index].ID;
				arShowValues = this.getRowValues(arFilter, strName);
				if (!arShowValues)
				{
					return false;
				}
				if (!BX.util.in_array(strPropValue, arShowValues))
				{
					return false;
				}
				arFilter[strName] = strPropValue;
				for (i = index+1; i < this.treeProps.length; i++)
				{
					strName = 'PROP_'+this.treeProps[i].ID;
					arShowValues = this.getRowValues(arFilter, strName);
					if (!arShowValues)
					{
						return false;
					}
					allValues = [];
					if (this.showAbsent)
					{
						arCanBuyValues = [];
						tmpFilter = [];
						tmpFilter = BX.clone(arFilter, true);
						for (j = 0; j < arShowValues.length; j++)
						{
							tmpFilter[strName] = arShowValues[j];
							allValues[allValues.length] = arShowValues[j];
							if (this.getCanBuy(tmpFilter))
								arCanBuyValues[arCanBuyValues.length] = arShowValues[j];
						}
					}
					else
					{
						arCanBuyValues = arShowValues;
					}
					if (this.selectedValues[strName] && BX.util.in_array(this.selectedValues[strName], arCanBuyValues))
					{
						arFilter[strName] = this.selectedValues[strName];
					}
					else
					{
						if (this.showAbsent)
							arFilter[strName] = (arCanBuyValues.length > 0 ? arCanBuyValues[0] : allValues[0]);
						else
							arFilter[strName] = arCanBuyValues[0];
					}
					this.updateRow(i, arFilter[strName], arShowValues, arCanBuyValues);
				}
				this.selectedValues = arFilter;
				this.changeInfo();
			}
			return true;
		},

		updateRow: function(intNumber, activeID, showID, canBuyID)
		{
			var i = 0,
				value = '',
				isCurrent = false,
				rowItems = null;
                        
                        

			var lineContainer = this.obTree.querySelectorAll('[data-entity="sku-line-block"]'),
				listContainer;



			if (intNumber > -1 && intNumber < lineContainer.length)
			{
				rowItems = lineContainer[intNumber].querySelectorAll('a');
				//rowItems = BX.findChildren(lineContainer, {tagName: 'a'}, true);
                                 
				if (rowItems && 0 < rowItems.length)
				{
					for (i = 0; i < rowItems.length; i++)
					{
						value = rowItems[i].getAttribute('data-onevalue');
						isCurrent = value === activeID;
                                                
						if (isCurrent)
						{
							BX.addClass(rowItems[i], 'selected');
						}
						else
						{
							BX.removeClass(rowItems[i], 'selected');
						}

						if (BX.util.in_array(value, canBuyID))
						{
							BX.removeClass(rowItems[i], 'notallowed');
						}
						else
						{
							BX.addClass(rowItems[i], 'notallowed');
						}

						rowItems[i].style.display = BX.util.in_array(value, showID) ? '' : 'none';

						if (isCurrent)
						{
							lineContainer[intNumber].style.display = (value == 0 && canBuyID.length == 1) ? 'none' : '';
						}
					}
				}
			}
		},

		getRowValues: function(arFilter, index)
		{
			var i = 0,
				j,
				arValues = [],
				boolSearch = false,
				boolOneSearch = true;

			if (0 === arFilter.length)
			{
				for (i = 0; i < this.offers.length; i++)
				{
					if (!BX.util.in_array(this.offers[i].TREE[index], arValues))
					{
						arValues[arValues.length] = this.offers[i].TREE[index];
					}
				}
				boolSearch = true;
			}
			else
			{
				for (i = 0; i < this.offers.length; i++)
				{
					boolOneSearch = true;
					for (j in arFilter)
					{
						if (arFilter[j] !== this.offers[i].TREE[j])
						{
							boolOneSearch = false;
							break;
						}
					}
					if (boolOneSearch)
					{
						if (!BX.util.in_array(this.offers[i].TREE[index], arValues))
						{
							arValues[arValues.length] = this.offers[i].TREE[index];
						}
						boolSearch = true;
					}
				}
			}
			return (boolSearch ? arValues : false);
		},

		getCanBuy: function(arFilter)
		{
			var i, j,
				boolSearch = false,
				boolOneSearch = true;

			for (i = 0; i < this.offers.length; i++)
			{
				boolOneSearch = true;
				for (j in arFilter)
				{
					if (arFilter[j] !== this.offers[i].TREE[j])
					{
						boolOneSearch = false;
						break;
					}
				}
				if (boolOneSearch)
				{
					if (this.offers[i].CAN_BUY)
					{
						boolSearch = true;
						break;
					}
				}
			}

			return boolSearch;
		},

		setCurrent: function()
		{
			var i,
				j = 0,
				arCanBuyValues = [],
				strName = '',
				arShowValues = false,
				arFilter = {},
				tmpFilter = [],
				current = this.offers[this.offerNum].TREE;
                        
			for (i = 0; i < this.treeProps.length; i++)
			{
				strName = 'PROP_'+this.treeProps[i].ID;
				arShowValues = this.getRowValues(arFilter, strName);
                                
				if (!arShowValues)
				{
					break;
				}
				if (BX.util.in_array(current[strName], arShowValues))
				{
					arFilter[strName] = current[strName];
				}
				else
				{
					arFilter[strName] = arShowValues[0];
					this.offerNum = 0;
				}
				if (this.showAbsent)
				{
					arCanBuyValues = [];
					tmpFilter = [];
					tmpFilter = BX.clone(arFilter, true);
					for (j = 0; j < arShowValues.length; j++)
					{
						tmpFilter[strName] = arShowValues[j];
						if (this.getCanBuy(tmpFilter))
						{
							arCanBuyValues[arCanBuyValues.length] = arShowValues[j];
						}
					}
				}
				else
				{
					arCanBuyValues = arShowValues;
				}
                                
                                
				this.updateRow(i, arFilter[strName], arShowValues, arCanBuyValues);
			}
			this.selectedValues = arFilter;
			this.changeInfo();
		},

		changeInfo: function()
		{
			var i, j,
				index = -1,
				boolOneSearch = true,
				quantityChanged;

			for (i = 0; i < this.offers.length; i++)
			{
				boolOneSearch = true;
				for (j in this.selectedValues)
				{
					if (this.selectedValues[j] !== this.offers[i].TREE[j])
					{
						boolOneSearch = false;
						break;
					}
				}
				if (boolOneSearch)
				{
					index = i;
					break;
				}
			}
			if (index > -1)
			{
                                BX.cleanNode(this.obPictSlider);
                                
                                if (!!this.offers[index].PREVIEW_PICTURE && !!this.offers[index].PREVIEW_PICTURE.SRC)
                                {
                                    
                                    if (this.offers[index].MORE_PHOTO.length > 0)
                                    {
                                        var innerHtml = '<div class="swiper-container gallery-top"><div class="swiper-wrapper">', innerThumbs = '<div class="swiper-container gallery-thumbs"><div class="swiper-wrapper">';
                                        innerHtml += '<div class="swiper-slide"><img class="product-image" data-src="'+this.offers[index].PREVIEW_PICTURE.SRC+'"></div>';
                                        innerThumbs += '<div class="swiper-slide"></div>';
                                        
                                        for (i in this.offers[index].MORE_PHOTO)
					{
                                            if (this.offers[index].MORE_PHOTO.hasOwnProperty(i))
                                            {
                                                innerHtml += '<div class="swiper-slide"><img class="product-image" data-src="'+this.offers[index].MORE_PHOTO[i].SRC+'"></div>';
                                                innerThumbs += '<div class="swiper-slide"></div>';
                                            }
                                        }
                                        innerThumbs += '</div><div class="swiper-pagination"></div></div>';
                                        innerHtml += '</div><div class="swiper-pagination"></div></div>';
                                        
                                        BX.adjust(this.obPictSlider, {
                                            html: innerHtml + innerThumbs
                                        });
                                        
                                        this.initializeSlider();
                                    }
                                    else
                                    {
                                        
                                        BX.adjust(this.obPictSlider, {
                                            html: '<img class="product-image" data-src="'+this.offers[index].PREVIEW_PICTURE.SRC+'">'
                                        });
                                    }
                                    
                                }
                                else if (!!this.defaultPict && !!this.defaultPict.pict && !!this.defaultPict.pict.SRC)
                                {
                                    if (this.offers[index].MORE_PHOTO.length > 0)
                                    {
                                        var innerHtml = '<div class="swiper-container gallery-top"><div class="swiper-wrapper">', innerThumbs = '<div class="swiper-container gallery-thumbs"><div class="swiper-wrapper">';
                                        innerHtml += '<div class="swiper-slide"><img class="product-image" data-src="'+this.defaultPict.pict.SRC+'"></div>';
                                        innerThumbs += '<div class="swiper-slide"></div>';
                                        
                                        for (i in this.offers[index].MORE_PHOTO)
					{
                                            if (this.offers[index].MORE_PHOTO.hasOwnProperty(i))
                                            {
                                                innerHtml += '<div class="swiper-slide"><img class="product-image" data-src="'+this.offers[index].MORE_PHOTO[i].SRC+'"></div>';
                                                innerThumbs += '<div class="swiper-slide"></div>';
                                            }
                                        }
                                        innerThumbs += '</div><div class="swiper-pagination"></div></div>';
                                        innerHtml += '</div><div class="swiper-pagination"></div></div>';
                                        
                                        BX.adjust(this.obPictSlider, {
                                            html: innerHtml + innerThumbs
                                        });
                                        
                                        this.initializeSlider();
                                    }
                                    else
                                    {
                                        
                                        BX.adjust(this.obPictSlider, {
                                            html: '<img class="product-image" data-src="'+this.defaultPict.pict.SRC + '">'
                                        });
                                    }
                                }
                                
                                $(this.obPictSlider).find('.product-image').Lazy({
                                    effect: "fadeIn",
                                    effectTime: 250,
                                    threshold: 0
                                });
                                
                               
                                if (!!this.visual.ITEM_CODE_ID)
                                {
                                    var obItemCode = BX(this.visual.ITEM_CODE_ID),
                                        obItemCodeValue = obItemCode.querySelector('[data-entity="item-code-value"]');
                                    
                                    if (!!this.offers[index].ITEM_CODE && this.offers[index].ITEM_CODE.length > 0)
                                    {
                                        BX.adjust(obItemCodeValue, {
                                            html: this.offers[index].ITEM_CODE
                                        });
                                        
                                        BX.adjust(obItemCode, {
                                            style: {display: 'inline-flex'}
                                        });
                                    }
                                    else
                                    {
                                        BX.adjust(obItemCode, {
                                            style: {display: 'none'}
                                        });
                                    }
                                }
                                

				if (this.showSkuProps && this.obSkuProps)
				{
					if (this.offers[index].DISPLAY_PROPERTIES.length)
					{
						BX.adjust(this.obSkuProps, {style: {display: ''}, html: this.offers[index].DISPLAY_PROPERTIES});
					}
					else
					{
						BX.adjust(this.obSkuProps, {style: {display: 'none'}, html: ''});
					}
				}

				this.quantitySet(index);
				this.setPrice();
				this.setCompared(this.offers[index].COMPARED);

				this.offerNum = index;
			}
		},

		checkPriceRange: function(quantity)
		{
			if (typeof quantity === 'undefined'|| this.currentPriceMode != 'Q')
				return;

			var range, found = false;

			for (var i in this.currentQuantityRanges)
			{
				if (this.currentQuantityRanges.hasOwnProperty(i))
				{
					range = this.currentQuantityRanges[i];

					if (
						parseInt(quantity) >= parseInt(range.SORT_FROM)
						&& (
							range.SORT_TO == 'INF'
							|| parseInt(quantity) <= parseInt(range.SORT_TO)
						)
					)
					{
						found = true;
						this.currentQuantityRangeSelected = range.HASH;
						break;
					}
				}
			}

			if (!found && (range = this.getMinPriceRange()))
			{
				this.currentQuantityRangeSelected = range.HASH;
			}

			for (var k in this.currentPrices)
			{
				if (this.currentPrices.hasOwnProperty(k))
				{
					if (this.currentPrices[k].QUANTITY_HASH == this.currentQuantityRangeSelected)
					{
						this.currentPriceSelected = k;
						break;
					}
				}
			}
		},

		getMinPriceRange: function()
		{
			var range;

			for (var i in this.currentQuantityRanges)
			{
				if (this.currentQuantityRanges.hasOwnProperty(i))
				{
					if (
						!range
						|| parseInt(this.currentQuantityRanges[i].SORT_FROM) < parseInt(range.SORT_FROM)
					)
					{
						range = this.currentQuantityRanges[i];
					}
				}
			}

			return range;
		},

		checkQuantityControls: function()
		{
			if (!this.obQuantity)
				return;

			var reachedTopLimit = this.checkQuantity && parseFloat(this.obQuantity.value) + this.stepQuantity > this.maxQuantity,
				reachedBottomLimit = parseFloat(this.obQuantity.value) - this.stepQuantity < this.minQuantity;

			if (reachedTopLimit)
			{
				BX.addClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled');
			}
			else if (BX.hasClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled'))
			{
				BX.removeClass(this.obQuantityUp, 'product-item-amount-field-btn-disabled');
			}

			if (reachedBottomLimit)
			{
				BX.addClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled');
			}
			else if (BX.hasClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled'))
			{
				BX.removeClass(this.obQuantityDown, 'product-item-amount-field-btn-disabled');
			}

			if (reachedTopLimit && reachedBottomLimit)
			{
				this.obQuantity.setAttribute('disabled', 'disabled');
			}
			else
			{
				this.obQuantity.removeAttribute('disabled');
			}
		},

		setPrice: function()
		{
			var obData, price;

			if (this.obQuantity)
			{
				this.checkPriceRange(this.obQuantity.value);
			}

			this.checkQuantityControls();

			price = this.currentPrices[this.currentPriceSelected];
                        
			if (this.obPrice)
			{
				if (price)
				{
					BX.adjust(this.obPrice, {html: BX.Currency.currencyFormat(price.RATIO_PRICE, price.CURRENCY, true)});
				}
				else
				{
					BX.adjust(this.obPrice, {html: ''});
				}

				if (this.showOldPrice && this.obPriceOld)
				{
					if (price && price.RATIO_PRICE !== price.RATIO_BASE_PRICE)
					{
						BX.adjust(this.obPriceOld, {
							style: {display: ''},
							html: BX.Currency.currencyFormat(price.RATIO_BASE_PRICE, price.CURRENCY, true)
						});
					}
					else
					{
						BX.adjust(this.obPriceOld, {
							style: {display: 'none'},
							html: ''
						});
					}
				}

				if (this.obPriceTotal)
				{
					if (price && this.obQuantity && this.obQuantity.value != this.stepQuantity)
					{
						BX.adjust(this.obPriceTotal, {
							html: BX.message('PRICE_TOTAL_PREFIX') + ' <strong>'
							+ BX.Currency.currencyFormat(price.PRICE * this.obQuantity.value, price.CURRENCY, true)
							+ '</strong>',
							style: {display: ''}
						});
					}
					else
					{
						BX.adjust(this.obPriceTotal, {
							html: '',
							style: {display: 'none'}
						});
					}
				}

				if (this.showPercent)
				{
					if (price && parseInt(price.DISCOUNT) > 0)
					{
						obData = {style: {display: ''}, html: -price.PERCENT + '%'};
					}
					else
					{
						obData = {style: {display: 'none'}, html: ''};
					}

					if (this.obDscPerc)
					{
						BX.adjust(this.obDscPerc, obData);
					}

					if (this.obSecondDscPerc)
					{
						BX.adjust(this.obSecondDscPerc, obData);
					}
				}
			}
		},
                
        wishlistChange: function (event) {

            var target = BX.getEventTarget(event),
                changeUrl = this.basketData.wishlist_url;

            if (changeUrl == '' || changeUrl.indexOf('#ID#') == -1)
                return false;

            if (this.basketData.add_wish_to_delayed)
            {
                switch (this.productType)
                {
                    case 0: // no catalog
                    case 1: // product
                    case 2: // set
                        changeUrl = changeUrl.replace('#ID#', this.product.id.toString());
                        break;
                    case 3: // sku
                        changeUrl = changeUrl.replace('#ID#', this.offers[this.offerNum].ID);
                        break;
                }
            }
            else
                changeUrl = changeUrl.replace('#ID#', this.product.id.toString());

            BX.ajax({
                method: 'POST',
                dataType: 'json',
                url: changeUrl,
                onsuccess: BX.proxy(function (response) {

                    if (typeof(response) == 'object')
                    {

                        if (response.result == 'ok' && response.state == "added")
                        {
                            if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                            {
                                new window.Alpha.Messages({
                                    type: 'default',
                                    title: BX.message('ADD_TO_WISHLIST_TITLE'),
                                    message: this.product.name.toString(),
                                    url: !this.isWishList ? this.basketData.wishlist_page_url : '',
                                    urlText: 'WISHLIST'
                                });
                            }

                            BX.addClass(this.obWishlist, 'active');
                        }
                        else if (response.result == 'ok' && response.state == "deleted")
                        {
                            if (!!window.Alpha.Messages && typeof (window.Alpha.Messages) == 'function')
                            {
                                new window.Alpha.Messages({
                                    type: 'default',
                                    title: BX.message('DELETE_FROM_WISHLIST_TITLE'),
                                    message: this.product.name.toString()
                                });
                            }

                            BX.removeClass(this.obWishlist, 'active');
                        }

                        BX.onCustomEvent('OnWishlistChange');

                    }
                    else
                    {
                        console.log("error change wishlist: ", response);
                    }


                }, this)
            });
        },

        setWishlist: function (state) {

            state = (state) || '';

            if (state == "active")
            {
                BX.addClass(this.obWishlist, 'active');
            }
            else
            {
                BX.removeClass(this.obWishlist, 'active');
            }

        },

		compare: function(event)
		{
			var checkbox = this.obCompare.querySelector('[data-entity="compare-checkbox"]'),
				target = BX.getEventTarget(event),
				checked = true;

			if (checkbox)
			{
				checked = target === checkbox ? checkbox.checked : !checkbox.checked;
			}

			var url = checked ? this.compareData.compareUrl : this.compareData.compareDeleteUrl,
				compareLink;

			if (url)
			{
				if (target !== checkbox)
				{
					BX.PreventDefault(event);
					this.setCompared(checked);
				}

				switch (this.productType)
				{
					case 0: // no catalog
					case 1: // product
					case 2: // set
						compareLink = url.replace('#ID#', this.product.id.toString());
						break;
					case 3: // sku
						compareLink = url.replace('#ID#', this.offers[this.offerNum].ID);
						break;
				}

				BX.ajax({
					method: 'POST',
					dataType: checked ? 'json' : 'html',
					url: compareLink + (compareLink.indexOf('?') !== -1 ? '&' : '?') + 'ajax_action=Y',
					onsuccess: checked
						? BX.proxy(this.compareResult, this)
						: BX.proxy(this.compareDeleteResult, this)
				});
			}
		},

		compareResult: function(result)
		{
		
			if (!BX.type.isPlainObject(result))
				return;


			if (this.offers.length > 0)
			{
				this.offers[this.offerNum].COMPARED = result.STATUS === 'OK';
			}

			if (result.STATUS === 'OK')
			{
				BX.onCustomEvent('OnCompareChange');
                                
                                if (!!window.Alpha.Messages && typeof(window.Alpha.Messages) == 'function')
                                {
                                    new window.Alpha.Messages({
                                        type: 'addToCompare',
                                        title: BX.message('COMPARE_TITLE'),
                                        message: BX.message('COMPARE_MESSAGE_OK')
                                    });
                                }

			}
			else
			{
                            if (!!window.Alpha.Messages && typeof(window.Alpha.Messages) == 'function')
                                {
                                    new window.Alpha.Messages({
                                        type: 'error',
                                        title: BX.message('COMPARE_TITLE'),
                                        message: (result.MESSAGE ? result.MESSAGE : BX.message('COMPARE_UNKNOWN_ERROR'))
                                    });
                                }
                                
				
			}

		},

		compareDeleteResult: function()
		{
			BX.onCustomEvent('OnCompareChange');

			if (this.offers && this.offers.length)
			{
				this.offers[this.offerNum].COMPARED = false;
			}
		},

		setCompared: function(state)
		{
			if (!this.obCompare)
				return;

			var checkbox = this.obCompare.querySelector('[data-entity="compare-checkbox"]');
			if (checkbox)
			{
				checkbox.checked = state;
			}
		},

		setCompareInfo: function(comparedIds)
		{
			if (!BX.type.isArray(comparedIds))
				return;

			for (var i in this.offers)
			{
				if (this.offers.hasOwnProperty(i))
				{
					this.offers[i].COMPARED = BX.util.in_array(this.offers[i].ID, comparedIds);
				}
			}
		},

		compareRedirect: function()
		{
			if (this.compareData.comparePath)
			{
				location.href = this.compareData.comparePath;
			}
			else
			{
				this.obPopupWin.close();
			}
		},

		checkDeletedCompare: function(id)
		{
			switch (this.productType)
			{
				case 0: // no catalog
				case 1: // product
				case 2: // set
					if (this.product.id == id)
					{
						this.setCompared(false);
					}

					break;
				case 3: // sku
					var i = this.offers.length;
					while (i--)
					{
						if (this.offers[i].ID == id)
						{
							this.offers[i].COMPARED = false;

							if (this.offerNum == i)
							{
								this.setCompared(false);
							}

							break;
						}
					}
			}
		},

		initBasketUrl: function()
		{
			this.basketUrl = (this.basketMode === 'ADD' ? this.basketData.add_url : this.basketData.buy_url);
			switch (this.productType)
			{
				case 1: // product
				case 2: // set
					this.basketUrl = this.basketUrl.replace('#ID#', this.product.id.toString());
					break;
				case 3: // sku
					this.basketUrl = this.basketUrl.replace('#ID#', this.offers[this.offerNum].ID);
					break;
			}
			this.basketParams = {
				'ajax_basket': 'Y'
			};
			if (this.showQuantity)
			{
				this.basketParams[this.basketData.quantity] = this.obQuantity.value;
			}
			if (this.basketData.sku_props)
			{
				this.basketParams[this.basketData.sku_props_var] = this.basketData.sku_props;
			}
		},

		fillBasketProps: function()
		{
			if (!this.visual.BASKET_PROP_DIV)
			{
				return;
			}
			var
				i = 0,
				propCollection = null,
				foundValues = false,
				obBasketProps = null;

			if (this.basketData.useProps && !this.basketData.emptyProps)
			{
				if (this.obPopupWin && this.obPopupWin.contentContainer)
				{
					obBasketProps = this.obPopupWin.contentContainer;
				}
			}
			else
			{
				obBasketProps = BX(this.visual.BASKET_PROP_DIV);
			}
			if (obBasketProps)
			{
				propCollection = obBasketProps.getElementsByTagName('select');
				if (propCollection && propCollection.length)
				{
					for (i = 0; i < propCollection.length; i++)
					{
						if (!propCollection[i].disabled)
						{
							switch (propCollection[i].type.toLowerCase())
							{
								case 'select-one':
									this.basketParams[propCollection[i].name] = propCollection[i].value;
									foundValues = true;
									break;
								default:
									break;
							}
						}
					}
				}
				propCollection = obBasketProps.getElementsByTagName('input');
				if (propCollection && propCollection.length)
				{
					for (i = 0; i < propCollection.length; i++)
					{
						if (!propCollection[i].disabled)
						{
							switch (propCollection[i].type.toLowerCase())
							{
								case 'hidden':
									this.basketParams[propCollection[i].name] = propCollection[i].value;
									foundValues = true;
									break;
								case 'radio':
									if (propCollection[i].checked)
									{
										this.basketParams[propCollection[i].name] = propCollection[i].value;
										foundValues = true;
									}
									break;
								default:
									break;
							}
						}
					}
				}
			}
			if (!foundValues)
			{
				this.basketParams[this.basketData.props] = [];
				this.basketParams[this.basketData.props][0] = 0;
			}
		},

		add2Basket: function()
		{
			this.basketMode = 'ADD';
			this.basket();
		},

		buyBasket: function()
		{
			this.basketMode = 'BUY';
			this.basket();
		},

		sendToBasket: function()
		{
			if (!this.canBuy)
			{
				return;
			}

			// check recommendation
			if (this.product && this.product.id && this.bigData)
			{
				this.rememberProductRecommendation();
			}

			this.initBasketUrl();
			this.fillBasketProps();
			BX.ajax({
				method: 'POST',
				dataType: 'json',
				url: this.basketUrl,
				data: this.basketParams,
				onsuccess: BX.proxy(this.basketResult, this)
			});
		},

		basket: function()
		{
			var contentBasketProps = '';
			if (!this.canBuy)
			{
				return;
			}
			switch (this.productType)
			{
				case 1: // product
				case 2: // set
					if (this.basketData.useProps && !this.basketData.emptyProps)
					{
						this.initPopupWindow();
						this.obPopupWin.setTitleBar(BX.message('TITLE_BASKET_PROPS'));
						if (BX(this.visual.BASKET_PROP_DIV))
						{
							contentBasketProps = BX(this.visual.BASKET_PROP_DIV).innerHTML;
						}
						this.obPopupWin.setContent(contentBasketProps);
						this.obPopupWin.setButtons([
							new BasketButton({
								text: BX.message('BTN_MESSAGE_SEND_PROPS'),
								events: {
									click: BX.delegate(this.sendToBasket, this)
								}
							})
						]);
						this.obPopupWin.show();
					}
					else
					{
						this.sendToBasket();
					}
					break;
				case 3: // sku
					this.sendToBasket();
					break;
			}
		},

		basketResult: function(arResult)
		{
			var strContent = '',
				strPict = '',
				successful,
				buttons = [];

			
			if (!BX.type.isPlainObject(arResult))
				return;

			successful = arResult.STATUS === 'OK';

			if (successful)
			{
				this.setAnalyticsDataLayer('addToCart');
			}

			if (successful && this.basketAction === 'BUY')
			{
				this.basketRedirect();
			}
			else
			{
				
				if (successful)
				{
					BX.onCustomEvent('OnBasketChange');

					if  (BX.findParent(this.obProduct, {className: 'bx_sale_gift_main_products'}, 10))
					{
						BX.onCustomEvent('onAddToBasketMainProduct', [this]);
					}

					switch (this.productType)
					{
						case 1: // product
						case 2: // set
							strPict = this.product.pict.SRC;
							break;
						case 3: // sku
							strPict = (this.offers[this.offerNum].PREVIEW_PICTURE ?
									this.offers[this.offerNum].PREVIEW_PICTURE.SRC :
									this.defaultPict.pict.SRC
							);
							break;
					}
                                        
                    if (!!window.Alpha.Messages && typeof(window.Alpha.Messages) == 'function')
                    {
                        new window.Alpha.Messages({
                            type: 'addToBasket',
                            title: BX.message('TITLE_SUCCESSFUL'),
                            message: this.product.name,
                            image: strPict,
                            url: this.basketData.basketUrl,
                            urlText: 'BASKET'
                        });
                    }

                    }
                    else
                    {
                        if (!!window.Alpha.Messages && typeof(window.Alpha.Messages) == 'function')
                        {
                            new window.Alpha.Messages({
                                type: 'error',
                                title: BX.message('TITLE_ERROR'),
                                message: (arResult.MESSAGE ? arResult.MESSAGE : BX.message('BASKET_UNKNOWN_ERROR')),
                            });
                        }
				    }
				
			}
		},

		basketRedirect: function()
		{
			location.href = (this.basketData.basketUrl ? this.basketData.basketUrl : BX.message('BASKET_URL'));
		},

		initPopupWindow: function()
		{
			if (this.obPopupWin)
				return;

			this.obPopupWin = BX.PopupWindowManager.create('CatalogSectionBasket_' + this.visual.ID, null, {
				autoHide: true,
				offsetLeft: 0,
				offsetTop: 0,
				overlay : true,
				closeByEsc: true,
				titleBar: true,
				closeIcon: true,
				contentColor: 'white',
				className: this.templateTheme ? 'bx-' + this.templateTheme : ''
			});
		}
	};
        
        
        
        //*******************
        //**** catalog.product.subscribe
        //*******************
        
        if (!!window.Alpha.CatalogProductSubscribe)
	{
		return;
	}

	var subscribeButton = function(params)
	{
		subscribeButton.superclass.constructor.apply(this, arguments);
		this.nameNode = BX.create('span', {
			props : { id : this.id },
			style: typeof(params.style) === 'object' ? params.style : {},
			text: params.text
		});
		this.buttonNode = BX.create('span', {
			attrs: { className: params.className },
			style: { marginBottom: '0', borderBottom: '0 none transparent' },
			children: [this.nameNode],
			events : this.contextEvents
		});
		if (BX.browser.IsIE())
		{
			this.buttonNode.setAttribute("hideFocus", "hidefocus");
		}
	};
	BX.extend(subscribeButton, BX.PopupWindowButton);

	window.Alpha.CatalogProductSubscribe = function(params)
	{
		this.buttonId = params.buttonId;
		this.buttonClass = params.buttonClass;
		this.jsObject = params.jsObject;
		this.ajaxUrl = '/bitrix/components/bitrix/catalog.product.subscribe/ajax.php';
		this.alreadySubscribed = params.alreadySubscribed;
		this.listIdAlreadySubscribed = params.listIdAlreadySubscribed;
		this.urlListSubscriptions = params.urlListSubscriptions;
		this.listOldItemId = {};
		this.landingId = params.landingId;

		this.elemButtonSubscribe = null;
		this.elemPopupWin = null;
		this.defaultButtonClass = 'bx-catalog-subscribe-button';

		this._elemButtonSubscribeClickHandler = BX.delegate(this.subscribe, this);
		this._elemHiddenClickHandler = BX.delegate(this.checkSubscribe, this);

		BX.ready(BX.delegate(this.init,this));
	};

	window.Alpha.CatalogProductSubscribe.prototype.init = function()
	{
		if (!!this.buttonId)
		{
			this.elemButtonSubscribe = BX(this.buttonId);
			this.elemHiddenSubscribe = BX(this.buttonId+'_hidden');
		}

		if (!!this.elemButtonSubscribe)
		{
			BX.bind(this.elemButtonSubscribe, 'click', this._elemButtonSubscribeClickHandler);
		}

		if (!!this.elemHiddenSubscribe)
		{
			BX.bind(this.elemHiddenSubscribe, 'click', this._elemHiddenClickHandler);
		}

		this.setButton(this.alreadySubscribed);
		this.setIdAlreadySubscribed(this.listIdAlreadySubscribed);
	};

	window.Alpha.CatalogProductSubscribe.prototype.checkSubscribe = function()
	{
		if(!this.elemHiddenSubscribe || !this.elemButtonSubscribe) return;

		if(this.listOldItemId.hasOwnProperty(this.elemButtonSubscribe.dataset.item))
		{
			this.setButton(true);
		}
		else
		{
			BX.ajax({
				method: 'POST',
				dataType: 'json',
				url: this.ajaxUrl,
				data: {
					sessid: BX.bitrix_sessid(),
					checkSubscribe: 'Y',
					itemId: this.elemButtonSubscribe.dataset.item
				},
				onsuccess: BX.delegate(function (result) {
					if(result.subscribe)
					{
						this.setButton(true);
						this.listOldItemId[this.elemButtonSubscribe.dataset.item] = true;
					}
					else
					{
						this.setButton(false);
					}
				}, this)
			});
		}
	};

	window.Alpha.CatalogProductSubscribe.prototype.subscribe = function()
	{
		this.elemButtonSubscribe = BX.proxy_context;
		if(!this.elemButtonSubscribe) return false;

		BX.ajax({
			method: 'POST',
			dataType: 'json',
			url: this.ajaxUrl,
			data: {
				sessid: BX.bitrix_sessid(),
				subscribe: 'Y',
				itemId: this.elemButtonSubscribe.dataset.item,
				siteId: BX.message('SITE_ID'),
				landingId: this.landingId
			},
			onsuccess: BX.delegate(function (result) {
				if(result.success)
				{
					this.createSuccessPopup(result);
					this.setButton(true);
					this.listOldItemId[this.elemButtonSubscribe.dataset.item] = true;
				}
				else if(result.contactFormSubmit)
				{
                    this.initPopupWindow();
					this.elemPopupWin.setTitleBar(BX.message('CPST_SUBSCRIBE_POPUP_TITLE'));
					var form = this.createContentForPopup(result);
					this.elemPopupWin.setContent(form);
					this.elemPopupWin.setButtons([
						new subscribeButton({
							text: BX.message('CPST_SUBSCRIBE_BUTTON_NAME'),
							className : 'btn btn-primary',
							events: {
								click : BX.delegate(function() {
									if(!this.validateContactField(result.contactTypeData))
									{
										return false;
									}
									BX.ajax.submitAjax(form, {
										method : 'POST',
										url: this.ajaxUrl,
										processData : true,
										onsuccess: BX.delegate(function (resultForm) {
											resultForm = BX.parseJSON(resultForm, {});
											if(resultForm.success)
											{
												this.createSuccessPopup(resultForm);
												this.setButton(true);
												this.listOldItemId[this.elemButtonSubscribe.dataset.item] = true;
											}
											else if(resultForm.error)
											{
												if(resultForm.hasOwnProperty('setButton'))
												{
													this.listOldItemId[this.elemButtonSubscribe.dataset.item] = true;
													this.setButton(true);
												}
												var errorMessage = resultForm.message;
												if(resultForm.hasOwnProperty('typeName'))
												{
													errorMessage = resultForm.message.replace('USER_CONTACT',
														resultForm.typeName);
												}
												BX('bx-catalog-subscribe-form-notify').style.color = 'red';
												BX('bx-catalog-subscribe-form-notify').innerHTML = errorMessage;
											}
										}, this)
									});
								}, this)
							}
						}),
					]);
					this.elemPopupWin.show();
				}
				else if(result.error)
				{
					if(result.hasOwnProperty('setButton'))
					{
						this.listOldItemId[this.elemButtonSubscribe.dataset.item] = true;
						this.setButton(true);
					}
					this.showWindowWithAnswer({status: 'error', message: result.message});
				}
			}, this)
		});
	};

	window.Alpha.CatalogProductSubscribe.prototype.validateContactField = function(contactTypeData)
	{
		var inputFields = BX.findChildren(BX('bx-catalog-subscribe-form'),
			{'tag': 'input', 'attribute': {id: 'userContact'}}, true);
		if(!inputFields.length || typeof contactTypeData !== 'object')
		{
			BX('bx-catalog-subscribe-form-notify').style.color = 'red';
			BX('bx-catalog-subscribe-form-notify').innerHTML = BX.message('CPST_SUBSCRIBE_VALIDATE_UNKNOW_ERROR');
			return false;
		}

		var contactTypeId, contactValue, useContact, errors = [], useContactErrors = [];
		for(var k = 0; k < inputFields.length; k++)
		{
			contactTypeId = inputFields[k].getAttribute('data-id');
			contactValue = inputFields[k].value;
			useContact = BX('bx-contact-use-'+contactTypeId);
			if(useContact && useContact.value == 'N')
			{
				useContactErrors.push(true);
				continue;
			}
			if(!contactValue.length)
			{
				errors.push(BX.message('CPST_SUBSCRIBE_VALIDATE_ERROR_EMPTY_FIELD').replace(
					'#FIELD#', contactTypeData[contactTypeId].contactLable));
			}
		}

		if(inputFields.length == useContactErrors.length)
		{
			BX('bx-catalog-subscribe-form-notify').style.color = 'red';
			BX('bx-catalog-subscribe-form-notify').innerHTML = BX.message('CPST_SUBSCRIBE_VALIDATE_ERROR');
			return false;
		}

		if(errors.length)
		{
			BX('bx-catalog-subscribe-form-notify').style.color = 'red';
			for(var i = 0; i < errors.length; i++)
			{
				BX('bx-catalog-subscribe-form-notify').innerHTML = errors[i];
			}
			return false;
		}

		return true;
	};

	window.Alpha.CatalogProductSubscribe.prototype.reloadCaptcha = function()
	{
		BX.ajax.get(this.ajaxUrl+'?reloadCaptcha=Y', '', function(captchaCode) {
			BX('captcha_sid').value = captchaCode;
			BX('captcha_img').src = '/bitrix/tools/captcha.php?captcha_sid='+captchaCode+'';
		});
	};

	window.Alpha.CatalogProductSubscribe.prototype.createContentForPopup = function(responseData)
	{
		if(!responseData.hasOwnProperty('contactTypeData'))
		{
			return null;
		}

		var contactTypeData = responseData.contactTypeData, contactCount = Object.keys(contactTypeData).length,
			styleInputForm = '', manyContact = 'N', content = document.createDocumentFragment();

		if(contactCount > 1)
		{
			manyContact = 'Y';
			styleInputForm = 'display:none;';
			content.appendChild(BX.create('p', {
				text: BX.message('CPST_SUBSCRIBE_MANY_CONTACT_NOTIFY')
			}));
		}

		content.appendChild(BX.create('p', {
			props: {id: 'bx-catalog-subscribe-form-notify'}
		}));

		for(var k in contactTypeData)
		{
			if(contactCount > 1)
			{
				content.appendChild(BX.create('div', {
					props: {
						className: 'bx-catalog-subscribe-form-container'
					},
					children: [
						BX.create('div', {
							props: {
								className: 'checkbox'
							},
							children: [
								BX.create('lable', {
									props: {
										className: 'bx-filter-param-label'
									},
									attrs: {
										onclick: this.jsObject+'.selectContactType('+k+', event);'
									},
									children: [
										BX.create('input', {
											props: {
												type: 'hidden',
												id: 'bx-contact-use-'+k,
												name: 'contact['+k+'][use]',
												value: 'N'
											}
										}),
										BX.create('input', {
											props: {
												id: 'bx-contact-checkbox-'+k,
												type: 'checkbox'
											}
										}),
										BX.create('span', {
											props: {
												className: 'bx-filter-param-text'
											},
											text: contactTypeData[k].contactLable
										})
									]
								})
							]
						})
					]
				}));
			}
			content.appendChild(BX.create('div', {
				props: {
					id: 'bx-catalog-subscribe-form-container-'+k,
					className: 'bx-catalog-subscribe-form-container',
					style: styleInputForm
				},
				children: [
					BX.create('div', {
						props: {
							className: 'bx-catalog-subscribe-form-container-label'
						},
						text: BX.message('CPST_SUBSCRIBE_LABLE_CONTACT_INPUT').replace(
							'#CONTACT#', contactTypeData[k].contactLable)
					}),
					BX.create('div', {
						props: {
							className: 'bx-catalog-subscribe-form-container-input'
						},
						children: [
							BX.create('input', {
								props: {
									id: 'userContact',
									className: '',
									type: 'text',
									name: 'contact['+k+'][user]'
								},
								attrs: {'data-id': k}
							})
						]
					})
				]
			}));
		}
		if(responseData.hasOwnProperty('captchaCode'))
		{
			content.appendChild(BX.create('div', {
				props: {
					className: 'bx-catalog-subscribe-form-container'
				},
				children: [
					BX.create('span', {props: {className: 'bx-catalog-subscribe-form-star-required'}, text: '*'}),
					BX.message('CPST_ENTER_WORD_PICTURE'),
					BX.create('div', {
						props: {className: 'bx-captcha'},
						children: [
							BX.create('input', {
								props: {
									type: 'hidden',
									id: 'captcha_sid',
									name: 'captcha_sid',
									value: responseData.captchaCode
								}
							}),
							BX.create('img', {
								props: {
									id: 'captcha_img',
									src: '/bitrix/tools/captcha.php?captcha_sid='+responseData.captchaCode+''
								},
								attrs: {
									width: '180',
									height: '40',
									alt: 'captcha',
									onclick: this.jsObject+'.reloadCaptcha();'
								}
							})
						]
					}),
					BX.create('div', {
						props: {className: 'bx-catalog-subscribe-form-container-input'},
						children: [
							BX.create('input', {
								props: {
									id: 'captcha_word',
									className: '',
									type: 'text',
									name: 'captcha_word'
								},
								attrs: {maxlength: '50'}
							})
						]
					})
				]
			}));
		}
		var form = BX.create('form', {
			props: {
				id: 'bx-catalog-subscribe-form'
			},
			children: [
				BX.create('input', {
					props: {
						type: 'hidden',
						name: 'manyContact',
						value: manyContact
					}
				}),
				BX.create('input', {
					props: {
						type: 'hidden',
						name: 'sessid',
						value: BX.bitrix_sessid()
					}
				}),
				BX.create('input', {
					props: {
						type: 'hidden',
						name: 'itemId',
						value: this.elemButtonSubscribe.dataset.item
					}
				}),
				BX.create('input', {
					props: {
						type: 'hidden',
						name: 'landingId',
						value: this.landingId
					}
				}),
				BX.create('input', {
					props: {
						type: 'hidden',
						name: 'siteId',
						value: BX.message('SITE_ID')
					}
				}),
				BX.create('input', {
					props: {
						type: 'hidden',
						name: 'contactFormSubmit',
						value: 'Y'
					}
				})
			]
		});

		form.appendChild(content);

		return form;
	};

	window.Alpha.CatalogProductSubscribe.prototype.selectContactType = function(contactTypeId, event)
	{
		var contactInput = BX('bx-catalog-subscribe-form-container-'+contactTypeId), visibility = '',
			checkboxInput = BX('bx-contact-checkbox-'+contactTypeId);
		if(!contactInput)
		{
			return false;
		}

		if(checkboxInput != event.target)
		{
			if(checkboxInput.checked)
			{
				checkboxInput.checked = false;
			}
			else
			{
				checkboxInput.checked = true;
			}
		}

		if (contactInput.currentStyle)
		{
			visibility = contactInput.currentStyle.display;
		}
		else if (window.getComputedStyle)
		{
			var computedStyle = window.getComputedStyle(contactInput, null);
			visibility = computedStyle.getPropertyValue('display');
		}

		if(visibility === 'none')
		{
			BX('bx-contact-use-'+contactTypeId).value = 'Y';
			BX.style(contactInput, 'display', '');
		}
		else
		{
			BX('bx-contact-use-'+contactTypeId).value = 'N';
			BX.style(contactInput, 'display', 'none');
		}
	};

	window.Alpha.CatalogProductSubscribe.prototype.createSuccessPopup = function(result)
	{
		this.initPopupWindow();
		this.elemPopupWin.setTitleBar(BX.message('CPST_SUBSCRIBE_POPUP_TITLE'));
		var content = BX.create('div', {
			props:{
				className: 'bx-catalog-popup-content'
			},
			children: [
				BX.create('p', {
					props: {
						className: 'bx-catalog-popup-message'
					},
					text: result.message
				})
			]
		});
		this.elemPopupWin.setContent(content);
		this.elemPopupWin.setButtons([
			new subscribeButton({
				text : BX.message('CPST_SUBSCRIBE_BUTTON_CLOSE'),
				className : 'btn btn-primary',
				events : {
					click : BX.delegate(function() {
						this.elemPopupWin.destroy();
                        BX.removeClass(document.body, 'modal-open');
                        $('body').trigger('modalClose');
					}, this)
				}
			})
		]);
		this.elemPopupWin.show();
	};

	window.Alpha.CatalogProductSubscribe.prototype.initPopupWindow = function()
	{
		this.elemPopupWin = BX.PopupWindowManager.create('CatalogSubscribe_'+this.buttonId, null, {
			autoHide: true,
			offsetLeft: 0,
			offsetTop: 0,
			overlay : true,
			closeByEsc: true,
			titleBar: true,
			closeIcon: true,
			contentColor: 'white',
            events: {
                onPopupShow: function() {
                    $('body').trigger('modalOpen');
                    BX.addClass(document.body, 'modal-open');
                },
                onPopupClose: function() {
                    BX.removeClass(document.body, 'modal-open');
                    $('body').trigger('modalClose');
                }
            }
		});
	};

	window.Alpha.CatalogProductSubscribe.prototype.setButton = function(statusSubscription)
	{
		this.alreadySubscribed = Boolean(statusSubscription);
		if(this.alreadySubscribed)
		{
			this.elemButtonSubscribe.className = this.buttonClass + ' ' + this.defaultButtonClass + ' disabled';
			this.elemButtonSubscribe.innerHTML = '<span>' + BX.message('CPST_TITLE_ALREADY_SUBSCRIBED') + '</span>';
			BX.unbind(this.elemButtonSubscribe, 'click', this._elemButtonSubscribeClickHandler);
		}
		else
		{
			this.elemButtonSubscribe.className = this.buttonClass + ' ' + this.defaultButtonClass;
			this.elemButtonSubscribe.innerHTML = '<span>' + BX.message('CPST_SUBSCRIBE_BUTTON_NAME') + '</span>';
			BX.bind(this.elemButtonSubscribe, 'click', this._elemButtonSubscribeClickHandler);
		}
	};

	window.Alpha.CatalogProductSubscribe.prototype.setIdAlreadySubscribed = function(listIdAlreadySubscribed)
	{
		if (BX.type.isPlainObject(listIdAlreadySubscribed))
		{
			this.listOldItemId = listIdAlreadySubscribed;
		}
	};

	window.Alpha.CatalogProductSubscribe.prototype.showWindowWithAnswer = function(answer)
	{
		answer = answer || {};
		if (!answer.message) {
			if (answer.status == 'success') {
				answer.message = BX.message('CPST_STATUS_SUCCESS');
			} else {
				answer.message = BX.message('CPST_STATUS_ERROR');
			}
		}
		var messageBox = BX.create('div', {
			props: {
				className: 'bx-catalog-subscribe-alert'
			},
			children: [
				BX.create('span', {
					props: {
						className: 'bx-catalog-subscribe-aligner'
					}
				}),
				BX.create('span', {
					props: {
						className: 'bx-catalog-subscribe-alert-text'
					},
					text: answer.message
				}),
				BX.create('div', {
					props: {
						className: 'bx-catalog-subscribe-alert-footer'
					}
				})
			]
		});
		var currentPopup = BX.PopupWindowManager.getCurrentPopup();
		if(currentPopup) {
			currentPopup.destroy();
		}
		var idTimeout = setTimeout(function () {
			var w = BX.PopupWindowManager.getCurrentPopup();
			if (!w || w.uniquePopupId != 'bx-catalog-subscribe-status-action') {
				return;
			}
			w.close();
			w.destroy();
		}, 3500);
		var popupConfirm = BX.PopupWindowManager.create('bx-catalog-subscribe-status-action', null, {
			content: messageBox,
			onPopupClose: function () {
				this.destroy();
				clearTimeout(idTimeout);
			},
			autoHide: true,
			zIndex: 2000,
			className: 'bx-catalog-subscribe-alert-popup'
		});
		popupConfirm.show();
		BX('bx-catalog-subscribe-status-action').onmouseover = function (e) {
			clearTimeout(idTimeout);
		};
		BX('bx-catalog-subscribe-status-action').onmouseout = function (e) {
			idTimeout = setTimeout(function () {
				var w = BX.PopupWindowManager.getCurrentPopup();
				if (!w || w.uniquePopupId != 'bx-catalog-subscribe-status-action') {
					return;
				}
				w.close();
				w.destroy();
			}, 3500);
		};
	};

})(window);

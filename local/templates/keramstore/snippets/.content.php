<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

IncludeTemplateLangFile(__FILE__);

$SNIPPETS = Array(
    "structure_columns_two.snp" => array (
        "title" => GetMessage('SNIPPET_STRUCTURE_TWO_COLUMNS'),
        "description" => ""
    ),
    "structure_columns_three.snp" => array (
        "title" => GetMessage('SNIPPET_STRUCTURE_THREE_COLUMNS'),
        "description" => ""
    ),
    "structure_columns_four.snp" => array (
        "title" => GetMessage('SNIPPET_STRUCTURE_FOUR_COLUMNS'),
        "description" => ""
    ),
    "structure_columns_five.snp" => array (
        "title" => GetMessage('SNIPPET_STRUCTURE_FIVE_COLUMNS'),
        "description" => ""
    ),
    "note.snp" => array (
        "title" => GetMessage('SNIPPET_NOTE'),
        "description" => ""
    ),
    "images_two.snp" => array (
        "title" => GetMessage('SNIPPET_IMAGES_TWO'),
        "description" => ""
    ),
    "infobox.snp" => array (
        "title" => GetMessage('SNIPPET_INFORMATION'),
        "description" => ""
    ),
    "quote.snp" => array (
        "title" => GetMessage('SNIPPET_QUOTE'),
        "description" => ""
    ),
    "leadtext.snp" => array (
        "title" => GetMessage('SNIPPET_LEADTEXT'),
        "description" => ""
    ),
);

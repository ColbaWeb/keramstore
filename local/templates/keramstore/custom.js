/* custom JS file */

(function (window){
    'use strict';

    $( document ).ready(function(){
        $( ".alphabet_letter" ).hover(function(){
            let listFactory = $(this).children('.alphabet_letter__factory').eq(0);
            let offsetRight = $(window).width() - ($(this).offset().left + listFactory.outerWidth() + 20);
            if(offsetRight < 0){
                listFactory.css("left", offsetRight);
            }
        });

        $(".text_collection__btn").click(function(){
            $(".text_collection").toggleClass("open");
            if($(".text_collection").hasClass("open")){
                $(".text_collection__btn").text("Скрыть");
            }else{
                $(".text_collection__btn").text("Подробнее");
            }
        });

        var images = $("img");
        images.each(function () {
            var alt = $(this).attr("alt");
            if(typeof alt != "undefined"){
                if(alt.length > 0) {
                } else {
                    var title = $("h1").html();
                    console.log(title);
                    $(this).attr("alt", title);
                }
            } else {
                var title = $("h1").html();
                $(this).attr("alt", title);
            }
        });
    });

})(window);
<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/iblock/classes/general/comp_findtools.php");

class CIBlockFindToolsAlpha extends CIBlockFindTools
{
    public static function resolveComponentEngine(CComponentEngine $engine, $pageCandidates, &$arVariables)
    {
        /** @global CMain $APPLICATION */
        global $APPLICATION, $CACHE_MANAGER;
        static $aSearch = array("&lt;", "&gt;", "&quot;", "&#039;");
        static $aReplace = array("<", ">", "\"", "'");

        $component = $engine->getComponent();
        if ($component)
        {
            $iblock_id = intval($component->arParams["IBLOCK_ID"]);
            $strict_check = $component->arParams["DETAIL_STRICT_SECTION_CHECK"] === "Y";
        }
        else
        {
            $iblock_id = 0;
            $strict_check = false;
        }

        //To fix GetPagePath security hack for SMART_FILTER_PATH
        foreach ($pageCandidates as $pageID => $arVariablesTmp)
        {
            foreach ($arVariablesTmp as $variableName => $variableValue)
            {
                if ($variableName === "SMART_FILTER_PATH")
                    $pageCandidates[$pageID][$variableName] = str_replace($aSearch, $aReplace, $variableValue);
            }
        }

        $requestURL = $APPLICATION->GetCurPage(true);

        $cacheId = $requestURL.implode("|", array_keys($pageCandidates))."|".SITE_ID."|".$iblock_id.$engine->cacheSalt;
        $cache = new CPHPCache;
        if ($cache->StartDataCache(3600, $cacheId, "iblock_find"))
        {
            if (defined("BX_COMP_MANAGED_CACHE"))
            {
                $CACHE_MANAGER->StartTagCache("iblock_find");
                CIBlock::registerWithTagCache($iblock_id);
            }

            foreach ($pageCandidates as $pageID => $arVariablesTmp)
            {
                if (
                    $arVariablesTmp["SECTION_CODE_PATH"] != ""
                    && (isset($arVariablesTmp["ELEMENT_ID"]) || isset($arVariablesTmp["ELEMENT_CODE"]))
                )
                {
                    if (CIBlockFindTools::checkElement($iblock_id, $arVariablesTmp, $strict_check))
                    {
                        $arVariables = $arVariablesTmp;
                        if (defined("BX_COMP_MANAGED_CACHE"))
                            $CACHE_MANAGER->EndTagCache();
                        $cache->EndDataCache(array($pageID, $arVariablesTmp));
                        return $pageID;
                    }
                }
            }

            foreach ($pageCandidates as $pageID => $arVariablesTmp)
            {
                if (
                    $arVariablesTmp["SECTION_CODE_PATH"] != ""
                    && (!isset($arVariablesTmp["ELEMENT_ID"]) && !isset($arVariablesTmp["ELEMENT_CODE"]))
                )
                {
                    if (CIBlockFindTools::checkSection($iblock_id, $arVariablesTmp))
                    {
                        $arVariables = $arVariablesTmp;
                        if (defined("BX_COMP_MANAGED_CACHE"))
                            $CACHE_MANAGER->EndTagCache();
                        $cache->EndDataCache(array($pageID, $arVariablesTmp));
                        return $pageID;
                    }
                }
            }

            if (defined("BX_COMP_MANAGED_CACHE"))
                $CACHE_MANAGER->AbortTagCache();
            $cache->AbortDataCache();
        }
        else
        {
            $vars = $cache->GetVars();
            $pageID = $vars[0];
            $arVariables = $vars[1];
            return $pageID;
        }

        reset($pageCandidates);
        $pageID = key($pageCandidates);
        $arVariables = $pageCandidates[$pageID];

        return $pageID;
    }
}
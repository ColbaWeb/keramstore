<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?//var_dump($arResult['LETTERS']);
//die();?>


<div class="brands-list">
    <? foreach ($arResult['LETTERS'] as $letter => $items) {?>
        <div class="brand-section" data-entity="letter-section" data-value="<?=$letter?>">
            <div class="brand-title font-h2"><?=$letter?></div>
            <div class="brand-items-list row">
                <? foreach ($items['ITEMS'] as $collection) {?>
                    <div class="item-wrap col-4 col-md-3">
                        <a href="<?=$collection['DETAIL_PAGE_URL']?>" class="brand-item font-body-small">
                            <div data-role="lazyload-background" class="item-img" style="display: block; background-image: url(<?=$collection['IMG']?>); background-size: contain;"></div>
                            <div class="item-title"><?=$collection['NAME']?></div>
                        </a>
                        <img class="brand-item-country" src="<?=$arResult["COUNTRY"][$collection['COUNTRY']]["IMG"]?>" alt="<?=$arResult["COUNTRY"][$collection['COUNTRY']]["NAME"]?>">
                    </div>
                <?}?>
            </div>
        </div>
    <?}?>
</div>

<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hlbl = 7;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$rsData = $entity_data_class::getList(array(
    "select" => array("UF_NAME", "UF_FILE", "UF_XML_ID"),
    "order" => array("ID" => "ASC"),
    "filter" => array()
));

while($arData = $rsData->Fetch()){
    $arResult["COUNTRY"][$arData["UF_XML_ID"]]["NAME"] = $arData["UF_NAME"];
    $arResult["COUNTRY"][$arData["UF_XML_ID"]]["IMG"] = CFile::GetPath($arData["UF_FILE"]);
}

foreach ($arResult["ITEMS"] as $item){
    $i["NAME"] = $item["NAME"];
    $i["IMG"] = $item["DETAIL_PICTURE"]["SRC"];
    $i["DETAIL_PAGE_URL"] = $item["DETAIL_PAGE_URL"];
    $i["COUNTRY"] = $item["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"];

    $ch = (substr($item["NAME"], 0,1));

    if(ctype_alpha($ch)) {
        $arResult["LETTERS"][$ch]["ITEMS"][] = $i;
    }elseif (ctype_digit($ch)) {
        $arResult["LETTERS"]["1-9"]["ITEMS"][] = $i;
    }else{
        $arResult["LETTERS"]["А-Я"]["ITEMS"][] = $i;
    }
}


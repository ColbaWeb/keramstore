<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($this->StartResultCache())
    {
    $alphabet = range('A', 'Z');
    array_push($alphabet, "1-9", "А-Я");
    foreach ($alphabet as $l){
        $arResult["LETTERS"][$l]["NAME"] = $l;
    }

    $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(array("NAME"=>"ASC"), $arFilter, false, array(), $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $ch = (substr($arFields["NAME"], 0,1));

        if(ctype_alpha($ch)) {
            $arResult["LETTERS"][$ch]["ITEMS"][] = $arFields;
        }elseif (ctype_digit($ch)) {
            $arResult["LETTERS"]["1-9"]["ITEMS"][] = $arFields;
        }else{
            $arResult["LETTERS"]["А-Я"]["ITEMS"][] = $arFields;
        }
    }
    $this->IncludeComponentTemplate();
}
?>
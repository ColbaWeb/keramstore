<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="alphabet">
    <div class="alphabet_all__factory">
        <a href="/brands/">Бренды</a>
    </div>
    <? foreach ($arResult['LETTERS'] as $letter) {
        $disabled = empty($letter['ITEMS']);
        ?>
        <div class="alphabet_letter <?echo ($disabled ? 'disabled': '')?>">
            <span><?=$letter['NAME']?></span>
            <?if(!$disabled):?>
                <div class="alphabet_letter__factory"><ul>
                    <? foreach ($letter['ITEMS'] as $link) {?>
                        <li><a href="<?=$link['DETAIL_PAGE_URL']?>"><?=$link['NAME']?></a></li>
                    <?}?>
                </ul></div>
            <?endif;?>
        </div>
    <?}?>
</div>

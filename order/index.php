<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Оформление заказа");

if (\Nextype\Alpha\Options\Base::getInstance(SITE_ID)->getValue('simpleOrderActive'))
{
    $APPLICATION->IncludeFile(SITE_DIR . 'order/include/simple.php');
}
else
{
    $APPLICATION->IncludeFile(SITE_DIR . 'order/include/default.php');
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
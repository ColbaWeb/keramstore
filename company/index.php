<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Миссия компании заключается в том, чтобы предлагать самым взыскательным потребителям, дизайнерам и архитекторам богатый ассортимент керамических изделий для самых разных функциональных назначений, гарантируя при этом высокую эстетику и безупречное техническое качество.");
$APPLICATION->SetPageProperty("keywords", "keramstore, о компании keramstore, керамстор, купить плитку, керамическая плитка, заказать плитку, керамогранит, купить столешницу");
$APPLICATION->SetPageProperty("title", "О магазине | Керамическая плитка в Москве | KERAMSTORE");
$APPLICATION->SetTitle("О магазине");
?>
<div class="content-page">
    <div class="image">
        <? $APPLICATION->IncludeFile($APPLICATION->GetCurDir() . "company_image_inc.php") ?>
    </div>
    
    <? $APPLICATION->IncludeFile($APPLICATION->GetCurDir() . "company_text_inc.php") ?>
</div>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
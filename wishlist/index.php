<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "Избранные товары, доступные к покупке. Интернет магазин керамической плитки в Москве");
$APPLICATION->SetPageProperty("title", "Интернет-магазин керамической плитки в Москве | KERAMSTORE | Избранное");
$APPLICATION->SetTitle("Избранное");

if (\Nextype\Alpha\Options\Base::getInstance(SITE_ID)->getValue('wishListBasketEnabled'))
{
	$APPLICATION->IncludeFile(SITE_DIR . 'wishlist/include/basket.php');
}
else
{
    Nextype\Alpha\Tools\Wishlist::getList(false, true);
	$APPLICATION->IncludeFile(SITE_DIR . 'wishlist/include/default.php');
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php'); ?>
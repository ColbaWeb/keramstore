<?php
use \Bitrix\Main\Loader;
use Nextype\Alpha\Layout;

@define("ERROR_404", "Y");
@define("CUSTOM_PAGE", "Y");

if (in_array($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php', get_included_files()))
{
    $APPLICATION->RestartBuffer();
    $APPLICATION->SetTitle("Страница не найдена");
    $APPLICATION->SetDirProperty('bHideTitle', 'Y');
    
    require($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php');
}
else
{
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
}

CHTTP::SetStatus("404 Not Found");

$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->SetDirProperty('bHideTitle', 'Y');

if (Loader::includeModule('nextype.alpha'))
{

    Layout\Assets::getInstance(SITE_ID)->addLess(SITE_TEMPLATE_PATH . "/less/page404.less");
?>
    <div class="inner-page">
        <div class="page-404">
            <div class="container">
                <div class="img" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/404.gif');"></div>
                <div class="text-block">
                    <div class="d2">Страница не найдена</div>
                    <div class="text font-body">
                        К сожалению, такой страницы нет. Возможно, вы перешли по старой ссылке или ввели неправильный адрес. Не переживайте, вы тут ни при чем :)
                    </div>
                    <div class="buttons">
                        <a href="<?=SITE_DIR?>catalog/" class="btn btn-primary btn-large font-large-button">Перейти в каталог</a>
                        <a href="<?=SITE_DIR?>" class="link font-large-button">На главную <i class="icon icon-arrow-right-text-button-white"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?
}

if (in_array($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php', get_included_files()))
{
    require($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php');
}
else
{
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
}
?>
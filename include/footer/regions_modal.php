<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Options;

$geo = new Regions\Geo();
if (Options\Base::getInstance()->getValue('regionsSuggestPopupActive') && !$geo->isSetLocation())
{
    $geoRegion = $geo->getCurrentLocation();
    if (!empty($geoRegion))
    {
        ?>
        <div class="city-suggestion-container mobile">

            <div class="city-suggestion-popup">
                <div class="info">
                    <div class="title font-title"><?=Loc::getMessage('NT_ALPHA_HEADER_GEO_LOCATION_CITY_TITLE')?> <span><?=$geoRegion['NAME']?></span></div>

                    <? if (!empty($geoRegion['PROPERTIES']['REGION_NAME']['VALUE']))
                    {
                        ?><div class="subtitle font-body-2"><?=$geoRegion['PROPERTIES']['REGION_NAME']['VALUE']?></div><?
                    }
                    ?>
                </div>
                <div class="buttons">
                    <a href="#regions-modal" data-toggle="modal" class="btn change font-large-button"><?=Loc::getMessage('NT_ALPHA_HEADER_GEO_LOCATION_CHANGE_BUTTON')?></a>
                    <a href="<?=$geoRegion['LINK']?>" class="btn confirm btn-primary font-large-button"><?=Loc::getMessage('NT_ALPHA_HEADER_GEO_LOCATION_CONFIRM_BUTTON')?></a>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $(".city-suggestion-container .btn.change").on('click', function () {
                    $(".city-suggestion-container").removeClass('open');
                });
            });
        </script>
        <?
    }
}

$arRegionsList = Regions\Base::getInstance()->getList();
if (!empty($arRegionsList)) {
    ?>
    <div class="modal fade city-choose-modal" id="regions-modal" tabindex="-1" aria-labelledby="regions-modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon icon-close-big"></i>
                </a>
                <div class="scrollbar-inner">
                    <div class="modal-title font-h3"><?= Loc::getMessage('NT_ALPHA_HEADER_LOCATION_POPUP_TITLE') ?></div>
                    <div class="custom-input">
                        <input type="text" value="" id="region-search-popup-input">
                        <label for="region-search-popup-input"><?= Loc::getMessage('NT_ALPHA_HEADER_LOCATION_SEARCH_TITLE') ?></label>
                        <a href="javascript:void(0);" class="clear-btn"></a>
                    </div>

                    <ul class="recommended-cities-list font-body-2">
                        <?
                        foreach ($arRegionsList as $item) {
                            ?>
                            <li class="item">
                                <a <? if (!Options\Base::getInstance()->getValue('regionsOnSubdomains')): ?>rel="nofollow"<?
                                endif;
                                ?> href="<?= $item['LINK'] ?>"><?= $item['NAME'] ?></a>
                            </li>
                            <?
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?
}
?>
<script>
    $(document).ready(function(){
        $('.city-choose-modal .search-cities-list').css({'min-height': $('.city-choose-modal .recommended-cities-list').outerHeight() + 4});

        $("#region-search-popup-input").on('keyup', function () {
            if ($(this).val().length > 0)
            {
                var value = $(this).val().toLowerCase();
                $(".recommended-cities-list .item").addClass('d-none');

                $(".recommended-cities-list .item").each (function () {
                    var label = $(this).find('a').text().toLowerCase();
                    console.log(label, value, label.indexOf(value));
                    if (label.indexOf(value) != -1)
                    {
                        $(this).removeClass('d-none');
                    }
                });
            }
            else
            {
                $(".recommended-cities-list .item").removeClass('d-none');
            }
        });
    })
</script>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Tools;
?>

<div class="socials">
    <?
    foreach (Array ('vk', 'fb', 'Im', 'ok', 'yt', 'tw', 'zen', 'telegram', 'ws', 'tiktok') as $key)
    {
        if (strlen(Options\Base::getInstance()->getValue('socialLink' . ucfirst($key))) > 0)
        {
            ?><a href="<?=Options\Base::getInstance()->getValue('socialLink' . ucfirst($key))?>" target="_blank" rel="nofollow" class="item"><i class="icon icon-<?=$key?>-white"></i></a><?
        }
    }
    ?>
</div>
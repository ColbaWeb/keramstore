<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

global $APPLICATION;


$APPLICATION->IncludeComponent(
        "bitrix:menu", "catalog_footer", Array(
            "ROOT_MENU_TYPE" => "catalog",
            "MAX_LEVEL" => "1",
            "USE_EXT" => "Y",
            "ALLOW_MULTI_SELECT" => "N",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "36000000",
            "MENU_CACHE_USE_GROUPS" => "N",
            "MENU_CACHE_GET_VARS" => "",
            "PATH_TO_CATALOG" => SITE_DIR . "catalog/",
            "ITEMS_COUNT" => 6
        )
);
?>
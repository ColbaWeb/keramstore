<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;

global $APPLICATION;

if (CModule::IncludeModule('catalog') && CModule::IncludeModule('sale'))
{
    $APPLICATION->IncludeComponent(
            "nextype:alpha.subscribe.coupon", "main", Array(
                "CURRENCY_ID" => \CCurrency::GetBaseCurrency(),
                "COUPON_VALUE" => Options\Base::getInstance(SITE_ID)->getValue('subscriptionCouponValue'),
                "MIN_ORDER_PRICE" => Options\Base::getInstance(SITE_ID)->getValue('subscriptionCouponMinOrderPrice'),
            )
    );
}
?>
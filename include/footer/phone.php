<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Tools;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php");

$phones = Options\Base::getInstance()->getValue('phones');

if (Regions\Base::getInstance()->isActive() && Regions\Base::getInstance()->getCurrent())
{
    $phones = Regions\Base::getInstance()->getPhones();
}

?>

<? if (is_array($phones) && count($phones) > 1): ?>

    <div class="dropdown">
        <a href="tel:<?=Tools\Formatter::phoneToInt($phones[0]['phone'])?>" class="current" role="button" id="dropdownFooterPhones" aria-haspopup="true" aria-expanded="false">
            <span class="phone-link"><?=$phones[0]['phone']?></span>
            <i class="icon icon-arrow-light-down icon-arrow-light-down-white"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownFooterPhones">
            <div class="dropdown-menu-wrap">
                <? foreach ($phones as $item): ?>
                <a class="dropdown-item font-body-small" href="tel:<?=Tools\Formatter::phoneToInt($item['phone'])?>">
                    <span><?=$item['phone']?></span>
                    <span class="font-tiny-text"><?=$item['additional']?></span>
                </a>
                <? endforeach; ?>
                
                <? if (Options\Base::getInstance()->getValue('callbackForm')): ?>
                <a href="javascript:void(0)" onclick="window.Alpha.Popups.openAjaxContent({url: '<?=SITE_DIR?>include/ajax/form.php?id=callback'})" class="btn btn-primary"><?=Loc::getMessage('NT_ALPHA_HEADER_CALLBACK_BUTTON')?></a>
                <? endif; ?>

            </div>
        </div>
    </div>

<? elseif (is_array($phones) && count($phones) == 1): ?>
<a href="tel:<?=Tools\Formatter::phoneToInt($phones[0]['phone'])?>" role="button">
    <span><?=$phones[0]['phone']?></span>
</a>
<? endif; ?>

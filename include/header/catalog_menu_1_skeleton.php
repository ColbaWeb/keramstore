<style>
    .catalog-menu-skeleton {
        display: flex;
        margin-right: 32px;
    }
    .catalog-menu-skeleton-item {
        display: flex;
        padding: 11px 0;
    }
    .catalog-menu-skeleton-item:not(:last-child) {
        margin-right: 24px;
    }
    .catalog-menu-skeleton-item-icon {
        width: 24px;
        height: 24px;
        margin-left: 8px;
    }
    .catalog-menu-skeleton-item-text {
        width: 105px;
        height: 24px;
    }
</style>

<div class="catalog-menu-skeleton">
    <div class="catalog-menu-skeleton-item">
        <div class="catalog-menu-skeleton-item-text skeleton-bg"></div>
        <div class="catalog-menu-skeleton-item-icon skeleton-bg"></div>
    </div>
    <div class="catalog-menu-skeleton-item">
        <div class="catalog-menu-skeleton-item-text skeleton-bg"></div>
        <div class="catalog-menu-skeleton-item-icon skeleton-bg"></div>
    </div>
    <div class="catalog-menu-skeleton-item">
        <div class="catalog-menu-skeleton-item-text skeleton-bg"></div>
        <div class="catalog-menu-skeleton-item-icon skeleton-bg"></div>
    </div>
    <div class="catalog-menu-skeleton-item">
        <div class="catalog-menu-skeleton-item-text skeleton-bg"></div>
        <div class="catalog-menu-skeleton-item-icon skeleton-bg"></div>
    </div>
    <div class="catalog-menu-skeleton-item">
        <div class="catalog-menu-skeleton-item-text skeleton-bg"></div>
        <div class="catalog-menu-skeleton-item-icon skeleton-bg"></div>
    </div>
    <div class="catalog-menu-skeleton-item">
        <div class="catalog-menu-skeleton-item-text skeleton-bg"></div>
        <div class="catalog-menu-skeleton-item-icon skeleton-bg"></div>
    </div>
    <div class="catalog-menu-skeleton-item">
        <div class="catalog-menu-skeleton-item-text skeleton-bg"></div>
        <div class="catalog-menu-skeleton-item-icon skeleton-bg"></div>
    </div>
</div>
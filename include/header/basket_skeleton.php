<style>
    .basket-skeleton {
        display: flex;
        margin-left: 16px;
    }
    .basket-skeleton-icon {
        width: 24px;
        height: 24px;
        margin-right: 8px;
    }
    .basket-skeleton-total {
        width: 80px;
        height: 24px;
    }
    @media (max-width: 1024px) {
        .basket-skeleton-total {
            display: none;
        }
        .basket-skeleton-icon {
            margin: 0;
        }
        .basket-skeleton {
            margin: 0;
            padding: 10px;
        }
    }

</style>

<div class="basket-skeleton">
    <div class="basket-skeleton-icon skeleton-bg"></div>
    <div class="basket-skeleton-total skeleton-bg"></div>
</div>
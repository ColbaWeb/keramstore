<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;

if (!Options\Base::getInstance()->getValue('wishList'))
    return;

$listId = $cartId = "bx_wishlist".RandString();

$items = Tools\Wishlist::getList();

?>

<div class="favorites-container" id="<?=$listId?>">
    <a href="<?=SITE_DIR?>wishlist/" class="favorites<?=isset($items) && !empty($items) ? ' no-empty' : ''?>">
        <i class="icon icon-bx-heart"></i>
        <span><?=Loc::getMessage('NT_ALPHA_HEADER_WISHLIST_TITLE')?></span>
    </a>
</div>

<script>
        BX.ready(function(){

            new window.Alpha.Wishlist({
                siteId: '<?=SITE_ID?>',
                containerId: '<?=$listId?>',
                ajaxPath: '<?=SITE_DIR?>include/ajax/wishlist_layout.php'
            });

        });
    </script>
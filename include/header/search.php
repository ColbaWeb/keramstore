<?$APPLICATION->IncludeComponent(
	"bitrix:search.title",
	"inline",
	Array(
		"CATEGORY_0" => array("iblock_nt_alpha_catalog"),
		"CATEGORY_0_TITLE" => "",
		"CATEGORY_0_iblock_nt_alpha_catalog" => array("5"),
		"CHECK_DATES" => "N",
		"CONTAINER_ID" => "title-inline-search",
		"INPUT_ID" => "title-inline-search-input",
		"NUM_CATEGORIES" => "1",
		"ORDER" => "date",
		"PAGE" => SITE_DIR . "catalog/",
		"SHOW_INPUT" => "Y",
		"SHOW_OTHERS" => "N",
		"TOP_COUNT" => "5",
		"USE_LANGUAGE_GUESS" => "Y",
                "PRICE_CODE" => array (0 => "BASE")
	)
);?>
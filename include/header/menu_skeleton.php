<style>
    .top-menu-skeleton {
        display: flex;
        margin-left: 16px;
    }
    .top-menu-item {
        width: 80px;
        height: 20px;
    }
    .top-menu-item-wrap {
        padding: 8px 0;
    }
    .top-menu-item:not(:last-child) {
        margin-right: 16px;
    }
    .top-menu-item:last-child {
        margin-right: 4px;
    }
</style>

<div class="top-menu-skeleton">
    <div class="top-menu-item-wrap">
        <div class="top-menu-item skeleton-bg"></div>
    </div>
    <div class="top-menu-item-wrap">
        <div class="top-menu-item skeleton-bg"></div>
    </div>
    <div class="top-menu-item-wrap">
        <div class="top-menu-item skeleton-bg"></div>
    </div>
    <div class="top-menu-item-wrap">
        <div class="top-menu-item skeleton-bg"></div>
    </div>
    <div class="top-menu-item-wrap">
        <div class="top-menu-item skeleton-bg"></div>
    </div>
    <div class="top-menu-item-wrap">
        <div class="top-menu-item skeleton-bg"></div>
    </div>
</div>
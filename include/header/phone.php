<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Tools;

$phones = Options\Base::getInstance()->getValue('phones');

if (Regions\Base::getInstance()->isActive() && Regions\Base::getInstance()->getCurrent())
{
    $phones = Regions\Base::getInstance()->getPhones();
}

?>

<? if (is_array($phones) && count($phones) > 1): ?>
<div class="phone-block">
    <?foreach ($phones as $key => $item) {?>
        <a class="dropdown-item font-body-small" href="tel:<?=Tools\Formatter::phoneToInt($item['phone'])?>">
            <span><?=$item['phone']?></span>
            <?if($item['additional']):?>
                <span class="font-tiny-text"><?=$item['additional']?></span>
            <?endif;?>
        </a>
        <?if($item != end($phones)) {
            echo("|");
        }
    }?>
</div>
<? elseif (is_array($phones) && count($phones) == 1): ?>
<div class="dropdown">
    <a href="tel:<?=Tools\Formatter::phoneToInt($phones[0]['phone'])?>" class="current">
        <span class="font-body-small"><?=$phones[0]['phone']?></span>
    </a>
</div>
<? endif; ?>

<? if (Options\Base::getInstance()->getValue('callbackForm')): ?>
    <a href="javascript:void(0)" onclick="window.Alpha.Popups.openAjaxContent({url: '<?=SITE_DIR?>include/ajax/form.php?id=callback'})" class="btn btn-primary btn-callback"><?=Loc::getMessage('NT_ALPHA_HEADER_CALLBACK_BUTTON')?></a>
<? endif; ?>

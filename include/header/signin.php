<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Options;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php");

?>
<? if (!$USER->IsAuthorized()): ?>
<a href="<?=SITE_DIR?>auth/" data-toggle="modal" data-target="#authModal" class="sign-in-link font-body-small">
    <i class="icon icon-user-16"></i>
    <span><?=Loc::getMessage('NT_ALPHA_HEADER_SIGNIN_TITLE')?></span>
</a>

    <div class="modal fade" id="authModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon icon-close-big"></i>
                </a>
                <div class="scrollbar-inner">
                    <?
                    if (Options\Base::getInstance(SITE_ID)->getValue('smsAuthorizationActive'))
                    {
                        $APPLICATION->IncludeComponent("nextype:alpha.authorize", "popup", Array(
                            "RESEND_DELAY" => Options\Base::getInstance(SITE_ID)->getValue('smsAuthorizationResendTime'),
                            "PHONE_REGISTRATION" => "Y",
                            "EMAIL_REGISTRATION" => "N",
                            "EMAIL_REQUIRED" => "Y",
                                )
                        );
                    }
                    else
                    { ?>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login"
                               aria-selected="true"><?=Loc::getMessage('NT_ALPHA_HEADER_SIGNIN_FORM_TITLE')?></a>
                        </li>
                        
                        <? if (Option::get("main", "new_user_registration") == "Y"): ?>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="registration-tab" data-toggle="tab" href="#registration" role="tab" aria-controls="registration"
                               aria-selected="false"><?=Loc::getMessage('NT_ALPHA_HEADER_SIGNUP_FORM_TITLE')?></a>
                        </li>
                        <? endif; ?>
                        
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="login" role="tabpanel" aria-labelledby="login-tab">
                            
                            <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "popup", Array(
                                "REGISTER_URL" => SITE_DIR . "auth/",
                                "FORGOT_PASSWORD_URL" => "",
                                "PROFILE_URL" => SITE_DIR . "personal/",
                                "SHOW_ERRORS" => "Y",
                                "REDIRECT_URL" => $parentRequestUri
                                )
                           );?>
                            
                            
                            
                        </div>
                        
                        <? if (Option::get("main", "new_user_registration") == "Y"): ?>
                        
                        <div class="tab-pane" id="registration" role="tabpanel" aria-labelledby="registration-tab">
                            
                            <?$APPLICATION->IncludeComponent("nextype:alpha.system.auth.registration", "popup", Array(
                                "REDIRECT_URL" => $parentRequestUri
                            ));?> 
                            
                        </div>
                        <? endif; ?>
                    </div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
<? else: ?>
<div class="sign-in">
    <div class="dropdown">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#authModal" class="sign-in-link font-body-small">
            <i class="icon icon-user-16"></i>
            <span><?=Loc::getMessage('NT_ALPHA_HEADER_PERSONAL_TITLE')?></span>
            <i class="icon icon-arrow-light-down"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:menu", "personal_header", Array(
                    "ROOT_MENU_TYPE" => "personal",
                    "MAX_LEVEL" => "1",
                    "USE_EXT" => "Y",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => ""
                )
            );
            ?>
            
            
        </div>
    </div>
</div>
<? endif; ?>
                

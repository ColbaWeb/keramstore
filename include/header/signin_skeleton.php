<style>
    .signin-skeleton {
        display: flex;
        align-items: center;
        padding: 8px 0;
    }
    .signin-skeleton-icon-1 {
        width: 16px;
        height: 16px;
        margin-right: 4px;
    }
    .signin-skeleton-icon-2 {
        width: 8px;
        height: 16px;
        margin-left: 4px;
    }
    .signin-skeleton-total {
        width: 112px;
        height: 20px;
    }
</style>

<div class="signin-skeleton">
    <div class="signin-skeleton-icon-1 skeleton-bg"></div>
    <div class="signin-skeleton-total skeleton-bg"></div>
    <div class="signin-skeleton-icon-2 skeleton-bg"></div>
</div>
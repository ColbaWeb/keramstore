<style>
    .regions-skeleton {
        display: flex;
        align-items: center;
        padding: 8px 0;
    }
    .regions-skeleton-icon {
        width: 10px;
        height: 16px;
    }
    .regions-skeleton-text {
        width: 52px;
        height: 20px;
        margin: 0 4px;;
    }
</style>

<div class="regions-skeleton">
    <div class="regions-skeleton-icon skeleton-bg"></div>
    <div class="regions-skeleton-text skeleton-bg"></div>
</div>
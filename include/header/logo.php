<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;

$source = SITE_TEMPLATE_PATH . "/images/logo.svg";

if (strlen(Options\Base::getInstance()->getValue('logo')) > 0)
    $source = CFile::GetPath(Options\Base::getInstance()->getValue('logo'));

?>

<a href="<?=SITE_DIR?>">
    <?
    if (end(explode(".", $source)) == "svg")
    {
        print file_get_contents($_SERVER['DOCUMENT_ROOT'] . $source);
    }
    else
    {
        ?><img src="<?=$source?>" alt="<?=Application::getInstance()->getSite()['NAME']?>" title="<?=Application::getInstance()->getSite()['NAME']?>"> <?
    }
    ?>
</a>
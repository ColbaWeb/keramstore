<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Options;

Application::getInstance()->setRequestUri($parentRequestUri);
Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php");

?>

<?
$arRegionsList = Regions\Base::getInstance()->getList();

if (!empty($arRegionsList))
{
    if (Options\Base::getInstance()->getValue('regionsViewMode') == "list")
    {
        ?>
        <div class="dropdown">
            <a href="javascript:void(0)" class="current" role="button" id="dropdownRegionsMenuButton" aria-haspopup="true" aria-expanded="false">
                <i class="icon icon-bx-location"></i>
                <span class="font-body-small"><?=Regions\Base::getInstance()->getCurrent()['NAME']?></span>
                <i class="icon icon-arrow-light-down"></i>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownRegionsMenuButton">
                <div class="dropdown-menu-wrap">
                    <? foreach ($arRegionsList as $item): ?>
                    <a class="dropdown-item" <? if (!Options\Base::getInstance()->getValue('regionsOnSubdomains')): ?>rel="nofollow"<?endif;?> href="<?=$item['LINK']?>"><?=$item['NAME']?></a>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
        <?
    }
    else
    {
        ?>
        <a href="#regions-modal" class="current-region" data-toggle="modal">
                <i class="icon icon-bx-location"></i>
                <span class="font-body-small"><?=Regions\Base::getInstance()->getCurrent()['NAME']?></span>
        </a>
        <?
    }

    $geo = new Regions\Geo();
    if (Options\Base::getInstance()->getValue('regionsSuggestPopupActive') && !$geo->isSetLocation())
    {
        $geoRegion = $geo->getCurrentLocation();
        if (!empty($geoRegion))
        {
        ?>
        <div class="city-suggestion-container open">

            <div class="city-suggestion-popup">
                <div class="info">
                    <div class="title font-title"><?=Loc::getMessage('NT_ALPHA_HEADER_GEO_LOCATION_CITY_TITLE')?> <span><?=$geoRegion['NAME']?></span></div>

                    <? if (!empty($geoRegion['PROPERTIES']['REGION_NAME']['VALUE']))
                    {
                        ?><div class="subtitle font-body-2"><?=$geoRegion['PROPERTIES']['REGION_NAME']['VALUE']?></div><?
                    }
                    ?>
                </div>
                <div class="buttons">
                    <a href="#regions-modal" data-toggle="modal" class="btn change font-large-button"><?=Loc::getMessage('NT_ALPHA_HEADER_GEO_LOCATION_CHANGE_BUTTON')?></a>
                    <a href="<?=$geoRegion['LINK']?>" class="btn confirm btn-primary font-large-button"><?=Loc::getMessage('NT_ALPHA_HEADER_GEO_LOCATION_CONFIRM_BUTTON')?></a>
                </div>
            </div>
        </div>
        <script>
        $(document).ready(function () {
            $(".city-suggestion-container .btn.change").on('click', function () {
                $(".city-suggestion-container").removeClass('open');
            });
        });
        </script>
        <?
        }
    }
}
?>
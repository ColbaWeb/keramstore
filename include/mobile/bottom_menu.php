<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Tools;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php");

if (!$USER->IsAuthorized())
{
    ?><a href="<?=SITE_DIR?>auth/" class="item"><i class="icon icon-btn-profile"></i></a><?
}
else
{
    ?><a href="<?=SITE_DIR?>personal/" class="item"><i class="icon icon-btn-profile"></i></a><?
}
?>
    <a href="/catalog/search.php?q=" class="item" data-entity="button-search-modal"><i class="icon icon-btn-search"></i></a>
    <a href="javascript:void(0);" class="item close-menu"><i class="icon icon-close-big-white"></i></a>
<?
if (Options\Base::getInstance()->getValue('wishList'))
{
    ?><a href="<?=SITE_DIR?>wishlist/" class="item"><i class="icon icon-btn-favorites"></i></a><?
}
?>

<a href="<?=SITE_DIR . "basket/"?>" class="item"><i class="icon icon-btn-cart"></i></a>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Tools;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php");

$phones = Options\Base::getInstance()->getValue('phones');

if (Regions\Base::getInstance()->isActive() && Regions\Base::getInstance()->getCurrent())
{
    $phones = Regions\Base::getInstance()->getPhones();
}

?>

<? if (is_array($phones) && count($phones) > 1): ?>

    <li class="has-sub info-page">
        <a href="tel:<?=Tools\Formatter::phoneToInt($phones[0]['phone'])?>">
            <span><?=$phones[0]['phone']?></span>
        </a>
        <div class="sub">
            <ul class="menu-list-2 list reset-ul-list">
                <? foreach ($phones as $item): ?>
                <li>
                    <a href="tel:<?=Tools\Formatter::phoneToInt($item['phone'])?>"><span><?=$item['phone']?></span></a>
                </li>
                <? endforeach; ?>
                <li class="menu-back">
                    <a href="javascript:void(0)">
                        <i class="icon icon-arrow-light-right"></i>
                        <span><?=Loc::getMessage('NT_ALPHA_BACK_BUTTON')?></span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

<? elseif (is_array($phones) && count($phones) == 1): ?>
    <li class="info-page">
        <a href="tel:<?=Tools\Formatter::phoneToInt($phones[0]['phone'])?>">
            <span><?=$phones[0]['phone']?></span>
        </a>
    </li>
<? endif; ?>

<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;
use Nextype\Alpha\Regions;
use Nextype\Alpha\Options;

Application::getInstance()->setRequestUri($parentRequestUri);

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php");
?>

<? if (count(Regions\Base::getInstance()->getList()) > 0): ?>

    <li data-depth="2" class="has-sub info-page">
        <a href="#regions-modal" data-toggle="modal" class="js-regions-modal">
            <span> <i class="icon icon-bx-location"></i><?=Regions\Base::getInstance()->getCurrent()['NAME']?></span>
        </a>
        <div class="sub">
            <ul class="menu-list-2 list reset-ul-list">
                <? foreach (Regions\Base::getInstance()->getList() as $item): ?>
                <li data-depth="3">
                    <a <? if (!Options\Base::getInstance()->getValue('regionsOnSubdomains')): ?>rel="nofollow"<?endif;?> href="<?=$item['LINK']?>"><span><?=$item['NAME']?></span></a>
                </li>
                <? endforeach; ?>
                <li class="menu-back">
                    <a href="javascript:void(0)">
                        <i class="icon icon-arrow-light-right"></i>
                        <span><?=Loc::getMessage('NT_ALPHA_BACK_BUTTON')?></span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

<? endif; ?>
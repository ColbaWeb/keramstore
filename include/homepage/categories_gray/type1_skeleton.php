<style>
    .skeleton-categories-block.type-1 {
        padding: 36px 0;
    }

    .skeleton-categories-block.type-1 .skeleton-section-name{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin-bottom: 40px;
    }
    .skeleton-categories-block.type-1 .skeleton-section-name .skeleton-title{
        height: 44px;
        width: 200px;
        margin-bottom: 12px;
        background: #F5F5F5;
    }
    .skeleton-categories-block.type-1 .skeleton-section-name .skeleton-subtitle {
        height: 24px;
        width: 96px;
        background: #F5F5F5;
    }
    .skeleton-categories-block.type-1 .skeleton-card {
        padding-bottom: 100%;
        background: #F5F5F5;
        border-radius: 4px;
        overflow: hidden;
        position: relative;
    }
    .skeleton-categories-block.type-1 .skeleton-bg {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        padding: 16px;
        display: flex;
        flex-direction: column;
    }
    .skeleton-categories-block.type-1 .skeleton-card-image {
        height: 100%;
        background: #FCFCFC;
        margin-bottom: 16px;
    }
    .skeleton-categories-block.type-1 .skeleton-card-title {
        height: 40px;
        width: 100%;
        background: #FCFCFC;
    }

    @media (max-width: 1366px) {
        .skeleton-categories-block.type-1 .skeleton-section-name .skeleton-title{
            margin-bottom: 32px;
        }
    }

    @media (max-width: 767px) {
        .skeleton-categories-block.type-1 .skeleton-cards-list {
            margin: 0 -4px -8px;
        }
        .skeleton-categories-block.type-1 .skeleton-cards-list .col-6{
            padding: 0 4px;
            margin-bottom: 8px;
        }
    }

    @media (max-width: 575px) {
        .skeleton-categories-block.type-1 .skeleton-section-name{
            margin-bottom: 24px;  
            align-items: flex-start;  
        }
        .skeleton-categories-block.type-1 .skeleton-section-name .skeleton-title{
            margin-bottom: 8px;
        }
        .skeleton-categories-block.type-1 .skeleton-section-name .skeleton-title {
            height: 28px;
        }
        .skeleton-categories-block.type-1 .skeleton-section-name .skeleton-subtitle {
            height: 24px;
        }

        .skeleton-categories-block.type-1 {
            padding: 24px 0 0;
        }
        
    }
</style>

<div class="container">
    <div class="skeleton-categories-block type-1">
        <div class="skeleton-section-name">
            <div class="skeleton-title"></div>
            <div class="skeleton-subtitle"></div>
        </div>
        <div class="skeleton-cards-list row">
            <div class="col-12 col-md-6">
                <div class="skeleton-card">
                    <div class="skeleton-bg">
                        <div class="skeleton-card-image"></div>
                        <div class="skeleton-card-title"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="skeleton-card">
                    <div class="skeleton-bg">
                        <div class="skeleton-card-image"></div>
                        <div class="skeleton-card-title"></div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
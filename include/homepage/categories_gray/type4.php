<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"categories_gray_type4", 
	array(
		"SHOW_PARENT_NAME" => "Y",
		"IBLOCK_TYPE" => "nt_alpha_catalog",
		"IBLOCK_ID" => "5",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "1",
		"SECTION_FIELDS" => array(
			0 => "NAME",
			1 => "PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"COMPONENT_TEMPLATE" => "categories_gray_type4",
		"COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
		"FILTER_NAME" => "arrSectionsFilter",
		"CACHE_FILTER" => "Y",
		"SECTION_TITLE" => "Разделы каталога",
		"SECTION_LINK_TYPE" => "C",
		"SECTION_LINK_TITLE" => "Каталог",
		"SECTION_LINK_URL" => "/catalog/",
		"SHOW_SECTIONS_ID" => array(
		),
		"SHOW_SECTIONS_COUNT" => "5",
		"PRIORITY_SECTION_ID" => "2"
	),
	false
);?>
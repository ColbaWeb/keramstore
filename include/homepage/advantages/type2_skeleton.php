<style>
    .skeleton-advantages.type-2 {
        display: flex;
        padding: 36px 0;
    }


    .skeleton-advantages.type-2 .skeleton-card:not(:last-child) {
        margin-right: 24px;
    }
    .skeleton-advantages.type-2 .skeleton-card {
        flex-basis: calc(25% - 18px);
        min-width: 232px;
        background: #F5F5F5;
        border-radius: 4px;
        
    }
    .skeleton-advantages.type-2 .skeleton-bg {
        padding: 8px;
        display: flex;
    }
    .skeleton-advantages.type-2 .skeleton-text-block {
        width: 100%;
    }
    .skeleton-advantages.type-2 .skeleton-img {
        width: 48px;
        height: 48px;
        margin-right: 12px;
        background: #FCFCFC;
        flex-shrink: 0;
    }
    .skeleton-advantages.type-2 .skeleton-title {
        background: #FCFCFC;
        margin-bottom: 8px;
        width: 100%;
        height: 26px;
    }
    .skeleton-advantages.type-2 .skeleton-text {
        background: #FCFCFC;
        height: 48px;
        width: 100%;
    }

    @media (max-width: 1366px) {
        .skeleton-advantages.type-2 {
            padding: 28px 0;
        }
    }

    @media (max-width: 1024px) {
        .skeleton-advantages.type-2 {
            overflow-x: scroll;
        }
    }

    @media (max-width: 575px) {
        .skeleton-advantages.type-2 {
            padding: 24px 0;
        }
    }
</style>

<div class="container">
    <div class="skeleton-advantages type-2">
        <div class="skeleton-card">
            <div class="skeleton-bg">
                <div class="skeleton-img"></div>
                <div class="skeleton-text-block">
                    <div class="skeleton-title"></div>
                    <div class="skeleton-text"></div>
                </div>
            </div>
        </div>
        <div class="skeleton-card">
            <div class="skeleton-bg">
                <div class="skeleton-img"></div>
                <div class="skeleton-text-block">
                    <div class="skeleton-title"></div>
                    <div class="skeleton-text"></div>
                </div>
            </div>
        </div>
        <div class="skeleton-card">
            <div class="skeleton-bg">
                <div class="skeleton-img"></div>
                <div class="skeleton-text-block">
                    <div class="skeleton-title"></div>
                    <div class="skeleton-text"></div>
                </div>
            </div>
        </div>
        <div class="skeleton-card">
            <div class="skeleton-bg">
                <div class="skeleton-img"></div>
                <div class="skeleton-text-block">
                    <div class="skeleton-title"></div>
                    <div class="skeleton-text"></div>
                </div>
            </div>
        </div>

    </div>
</div>
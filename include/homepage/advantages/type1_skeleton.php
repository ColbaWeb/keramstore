<style>
    .skeleton-advantages.type-1 {
        display: flex;
        padding: 36px 0;
    }
    .skeleton-advantages.type-1 .skeleton-card:not(:last-child) {
        margin-right: 24px;
    }
    .skeleton-advantages.type-1 .skeleton-card {
        flex-basis: calc(25% - 18px);
        min-width: 232px;
        border-radius: 4px;
        overflow: hidden;
    }
    .skeleton-advantages.type-1 .skeleton-img {
        width: 48px;
        height: 48px;
        margin-bottom: 12px;
    }
    .skeleton-advantages.type-1 .skeleton-title {
        margin-bottom: 8px;
        width: 100%;
        height: 26px;
    }
    .skeleton-advantages.type-1 .skeleton-text {
        height: 48px;
        width: 100%;
    }

    @media (max-width: 1366px) {
        .skeleton-advantages.type-1 {
            padding: 28px 0;
        }
    }

    @media (max-width: 1024px) {
        .skeleton-advantages.type-1 {
            overflow-x: scroll;
        }
    }

    @media (max-width: 575px) {
        .skeleton-advantages.type-1 {
            padding: 24px 0;
        }
    }
</style>
<div class="container">
    <div class="skeleton-advantages type-1">
        <div class="skeleton-card">
            <div class="skeleton-img skeleton-bg"></div>
            <div class="skeleton-title skeleton-bg"></div>
            <div class="skeleton-text skeleton-bg"></div>
        </div>
        <div class="skeleton-card">
            <div class="skeleton-img skeleton-bg"></div>
            <div class="skeleton-title skeleton-bg"></div>
            <div class="skeleton-text skeleton-bg"></div>
        </div>
        <div class="skeleton-card">
            <div class="skeleton-img skeleton-bg"></div>
            <div class="skeleton-title skeleton-bg"></div>
            <div class="skeleton-text skeleton-bg"></div>
        </div>
        <div class="skeleton-card">
            <div class="skeleton-img skeleton-bg"></div>
            <div class="skeleton-title skeleton-bg"></div>
            <div class="skeleton-text skeleton-bg"></div>
        </div>

    </div>
</div>
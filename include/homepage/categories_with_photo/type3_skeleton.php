<style>
    .skeleton-categories-with-photo.type-3 {
        padding: 36px 0;
    }
    .skeleton-categories-with-photo.type-3 .skeleton-section-name{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin-bottom: 12px;
    }
    .skeleton-categories-with-photo.type-3 .skeleton-section-name .skeleton-title{
        height: 44px;
        width: 200px;
        margin-bottom: 12px;
        background: #F5F5F5;
    }
    .skeleton-categories-with-photo.type-3 .skeleton-section-name .skeleton-subtitle {
        height: 24px;
        width: 96px;
        background: #F5F5F5;
    }
    .skeleton-categories-with-photo.type-3 .skeleton-card {
        padding-bottom: 100%;
        background: #F5F5F5;
        border-radius: 4px;
        overflow: hidden;
        position: relative;
    }
    .skeleton-categories-with-photo.type-3 .skeleton-bg {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        padding: 16px;
        display: flex;
        flex-direction: column;
    }
    .skeleton-categories-with-photo.type-3 .skeleton-card-image {
        height: 100%;
        background: #FCFCFC;
        margin-bottom: 16px;
    }
    .skeleton-categories-with-photo.type-3 .skeleton-card-title {
        height: 40px;
        width: 100%;
        background: #FCFCFC;
    }

    @media (max-width: 1366px) {
        .skeleton-categories-with-photo.type-3 .skeleton-section-name .skeleton-title{
            margin-bottom: 32px;
        }
    }

    @media (max-width: 992px) {
        .skeleton-categories-with-photo.type-3 .row {
            flex-wrap: nowrap;
            overflow: scroll;
            margin: 0 -4px;
        }
        .skeleton-categories-with-photo.type-3 .col-12 {
            width: 232px !important;
            height: 232px;
            max-width: none;
            flex: none;
            padding: 0 4px;
        }
    }
    @media (max-width: 575px) {
        .skeleton-categories-with-photo.type-3 .skeleton-section-name{
            margin-bottom: 24px;  
            align-items: flex-start;  
        }
        .skeleton-categories-with-photo.type-3 .skeleton-section-name .skeleton-title{
            margin-bottom: 8px;
        }
        .skeleton-categories-with-photo.type-3 .skeleton-section-name .skeleton-title {
            height: 28px;
        }
        .skeleton-categories-with-photo.type-3 .skeleton-section-name .skeleton-subtitle {
            height: 24px;
        }
        .skeleton-categories-with-photo.type-3 {
            padding: 24px 0;
        }
        
    }
</style>

<div class="container">
    <div class="skeleton-categories-with-photo type-3">
        <div class="skeleton-section-name">
            <div class="skeleton-title"></div>
            <div class="skeleton-subtitle"></div>
        </div>
        <div class="skeleton-cards-list row">
            <div class="col-12 col-md-3">
                <div class="skeleton-card">
                    <div class="skeleton-bg">
                        <div class="skeleton-card-image"></div>
                        <div class="skeleton-card-title"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="skeleton-card">
                    <div class="skeleton-bg">
                        <div class="skeleton-card-image"></div>
                        <div class="skeleton-card-title"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="skeleton-card">
                    <div class="skeleton-bg">
                        <div class="skeleton-card-image"></div>
                        <div class="skeleton-card-title"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="skeleton-card">
                    <div class="skeleton-bg">
                        <div class="skeleton-card-image"></div>
                        <div class="skeleton-card-title"></div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
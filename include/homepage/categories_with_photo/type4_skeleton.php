<style>
    .skeleton-categories-with-photo.type-4 {
        padding: 36px 0;
    }
    .skeleton-categories-with-photo.type-4 .skeleton-section-name{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin-bottom: 40px;
    }
    .skeleton-categories-with-photo.type-4 .skeleton-section-name .skeleton-title{
        height: 44px;
        width: 200px;
        margin-bottom: 12px;
        background: #F5F5F5;
    }
    .skeleton-categories-with-photo.type-4 .skeleton-section-name .skeleton-subtitle {
        height: 24px;
        width: 96px;
        background: #F5F5F5;
    }
    
    .skeleton-categories-with-photo.type-4 .skeleton-card {
        padding-bottom: 100%;
        background: #F5F5F5;
        border-radius: 4px;
        overflow: hidden;
        position: relative;
    }
    .skeleton-categories-with-photo.type-4 .skeleton-bg {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        padding: 16px;
        display: flex;
        flex-direction: column;
    }
    .skeleton-categories-with-photo.type-4 .skeleton-card-image {
        height: 100%;
        background: #FCFCFC;
        margin-bottom: 16px;
    }
    .skeleton-categories-with-photo.type-4 .skeleton-card-title {
        height: 40px;
        width: 100%;
        background: #FCFCFC;
    }
    .skeleton-categories-with-photo.type-4 .row > .col-12:last-child .col-6:nth-child(1), .skeleton-categories-with-photo.type-4 .row > .col-12:last-child .col-6:nth-child(2) {
        margin-bottom: 24px;
    }

    @media (max-width: 767px) {
        .skeleton-categories-with-photo.type-4 .row > .col-12:first-child {
            margin-bottom: 8px;
        }
        .skeleton-categories-with-photo.type-4 .row > .col-12:last-child .col-6:nth-child(1), .row > .col-12:last-child .col-6:nth-child(2) {
            margin-bottom: 8px;
        }
        .skeleton-categories-with-photo.type-4 .row > .col-12:last-child > .row {
            margin: 0 -4px;
        }
        .skeleton-categories-with-photo.type-4 .col-6 {
            padding: 0 4px;
        }
    }

    @media (max-width: 575px) {
        .skeleton-categories-with-photo.type-4 .skeleton-section-name{
            margin-bottom: 24px;  
            align-items: flex-start;  
        }
        .skeleton-categories-with-photo.type-4 .skeleton-section-name .skeleton-title {
            height: 28px;
        }
        .skeleton-categories-with-photo.type-4 .skeleton-section-name .skeleton-subtitle {
            height: 24px;
        }
        .skeleton-categories-with-photo.type-4 {
            padding: 24px 0;
        }

    }
</style>

<div class="container">
    <div class="skeleton-categories-with-photo type-4">
        <div class="skeleton-section-name">
            <div class="skeleton-title"></div>
            <div class="skeleton-subtitle"></div>
        </div>
        <div class="skeleton-cards-list row">
            <div class="col-12 col-md-6">
                <div class="skeleton-card big">
                    <div class="skeleton-bg">
                        <div class="skeleton-card-image"></div>
                        <div class="skeleton-card-title"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-6">
                        <div class="skeleton-card">
                            <div class="skeleton-bg">
                                <div class="skeleton-card-image"></div>
                                <div class="skeleton-card-title"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="skeleton-card">
                            <div class="skeleton-bg">
                                <div class="skeleton-card-image"></div>
                                <div class="skeleton-card-title"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="skeleton-card">
                            <div class="skeleton-bg">
                                <div class="skeleton-card-image"></div>
                                <div class="skeleton-card-title"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="skeleton-card">
                            <div class="skeleton-bg">
                                <div class="skeleton-card-image"></div>
                                <div class="skeleton-card-title"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
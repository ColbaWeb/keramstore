<style>

    .skeleton-product-day {
        padding: 36px 0;
    }

    .skeleton-product-day .skeleton-product-day-image {
        padding-bottom: 100%;
    }
    .skeleton-product-day .skeleton-subtitle {
        height: 26px;
        width: 100px;
    }
    .skeleton-product-day .skeleton-title {
        height: 100px;
        margin-bottom: 24px;
        width: 100%;
        margin-top: auto;
    }
    .skeleton-product-day .skeleton-text {
        height: 84px;
        width: 100%;
        margin-bottom: 32px;
    }
    .skeleton-product-day .skeleton-price {
        width: 300px;
        height: 40px;
        margin-bottom: 4px;
    }
    .skeleton-product-day .col-6:first-child .col-wrap{
        display: flex;
        flex-direction: column;
        height: 100%;
        padding: 31px 32px 31px 31px;
    }
    .skeleton-product-day .skeleton-one-click {
        margin-bottom: 16px;
        height: 24px;
        width: 150px;
    }
    .skeleton-product-day .skeleton-buttons {
        display: flex;
        margin-bottom: auto;
    }
    .skeleton-product-day .skeleton-buttons .skeleton-counter{
        width: 128px;
        height: 60px;
        margin-right: 8px;
    }
    .skeleton-product-day .skeleton-buttons .skeleton-buy {
        width: 160px;
        height: 60px;
        margin-right: 8px;
    }
    .skeleton-product-day .skeleton-buttons .skeleton-favorites {
        width: 60px;
        height: 60px;
    }
    .skeleton-product-day .skeleton-category {
        margin-top: auto;
        height: 26px;
        width: 150px;
    }
    @media (max-width: 1366px) {
        .skeleton-product-day .col-6:first-child .col-wrap {
            padding: 23px 24px 23px 23px;
        }
    }
    @media (max-width: 1024px) {
        .skeleton-product-day .col-6 {
            width: 100%;
            max-width: none;
        }
        .skeleton-product-day .col-6:last-child {
            order: -1;
        }
        .skeleton-product-day .row {
            flex-direction: column;
        }
    }
    @media (max-width: 575px) {
        .skeleton-product-day .col-6:first-child .col-wrap {
            padding: 16px 12px 24px 12px;
        }
        .skeleton-product-day .skeleton-buttons {
            flex-wrap: wrap;
        }
        .skeleton-product-day .skeleton-buttons .skeleton-counter{ 
            width: 100%;
            margin: 0 0 8px;
        }
    }
</style>
<div class="container">
    <div class="skeleton-product-day">
        <div class="row">
           <div class="col-6">
                <div class="col-wrap">
                   <div class="skeleton-title skeleton-bg"></div>
                   <div class="skeleton-text skeleton-bg"></div>
                   <div class="skeleton-price skeleton-bg"></div>
                   <div class="skeleton-one-click skeleton-bg"></div>
                   <div class="skeleton-buttons">
                       <div class="skeleton-counter skeleton-bg"></div>
                       <div class="skeleton-buy skeleton-bg"></div>
                       <div class="skeleton-favorites skeleton-bg"></div>
                   </div>
                   <!-- <div class="skeleton-category skeleton-bg"></div> -->
                </div>
           </div>
           <div class="col-6">
                <div class="skeleton-product-day-image skeleton-bg"></div>
           </div>
        </div>
    </div>
</div>
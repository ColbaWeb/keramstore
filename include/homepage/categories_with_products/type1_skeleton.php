<style>

    .skeleton-categories-with-goods {
        padding: 36px 0;
    }

    .skeleton-categories-with-goods .skeleton-card-image {
        height: 100%;
        padding-bottom: 100%;
        margin-bottom: 16px;
    }
    .skeleton-categories-with-goods .skeleton-card-price {
        height: 24px;
        width: 50%;
        margin-bottom: 4px;
    }
    .skeleton-categories-with-goods .skeleton-card-title {
        height: 52px;
        width: 100%;
    }
    .skeleton-categories-with-goods .skeleton-categories-with-goods-content {
        display: flex;
    }
    .skeleton-categories-with-goods .skeleton-category-card-wrap {
        flex: 1;
        width: auto;
        min-width: 27.46%;
        max-width: 27.46%;
        margin-right: 24px;
    }
    .skeleton-categories-with-goods .skeleton-cards-list {
        flex: 1;
    }
    .skeleton-categories-with-goods .skeleton-mobile-category-container {
        display: none;
        margin-bottom: 24px;
    }

    .skeleton-categories-with-goods .skeleton-mobile-category-img {
        width: 52px;
        height: 52px;
        border-radius: 4px;
        margin-right: 8px;
    }
    .skeleton-categories-with-goods .skeleton-mobile-category-text-block {
        width: 200px;
    }
    .skeleton-categories-with-goods .skeleton-mobile-category-title {
        height: 22px;
        width: 80%;
        margin-bottom: 8px;
    }
    .skeleton-categories-with-goods .skeleton-mobile-category-text {
        height: 24px;
        width: 60%;
    }

    @media (max-width: 1366px) {
        .skeleton-categories-with-goods .skeleton-section-name{
            margin-bottom: 32px;  
            align-items: flex-start;  
        }
        .skeleton-categories-with-goods .skeleton-card-title {
            height: 48px;
        }
        .skeleton-categories-with-goods {
            padding: 28px 0;
        }
    }

    @media (max-width: 1024px) {
        .skeleton-categories-with-goods .skeleton-cards-list {
            margin: 0 -4px;
            overflow: scroll;
            flex-wrap: nowrap;
        }
        .skeleton-categories-with-goods .skeleton-cards-list .col-2{
            padding: 0 4px;
            width: 232px !important;
            min-width: 232px;
        }
        .skeleton-categories-with-goods .skeleton-mobile-category-container {
            display: flex;
        }
        .skeleton-categories-with-goods .skeleton-category-card-wrap {
            display: none;
        }
        .skeleton-categories-with-goods .skeleton-cards-list {
            max-width: none;
            width: 100%;
            flex: 1;
            padding: 0 4px;
        }
        .skeleton-categories-with-goods .skeleton-card-wrap {
            width: 232px;
            min-width: 232px;
            max-width: none;
            height: 324px;
            flex-shrink: 0;
            flex: 0;
        }
    }

    @media (max-width: 575px) {
        .skeleton-categories-with-goods .skeleton-card-price {
            height: 44px;
        }
        .skeleton-categories-with-goods {
            padding: 24px 0;
        }
    }
</style>

<div class="container">
    <div class="skeleton-categories-with-goods">
        <div class="skeleton-mobile-category-container">
            <div class="skeleton-mobile-category-img skeleton-bg"></div>
            <div class="skeleton-mobile-category-text-block">
                <div class="skeleton-mobile-category-title skeleton-bg"></div>
                <div class="skeleton-mobile-category-text skeleton-bg"></div>
            </div>
        </div>
        <div class="skeleton-categories-with-goods-content">
            <div class="skeleton-category-card-wrap">
                <div class="skeleton-category-card skeleton-bg"></div>
            </div>
            <div class="skeleton-cards-list row">
                <div class="col skeleton-card-wrap">
                    <div class="skeleton-card">
                        <div class="skeleton-card-image skeleton-bg"></div>
                        <div class="skeleton-card-text-block">
                            <div class="skeleton-card-price skeleton-bg"></div>
                            <div class="skeleton-card-title skeleton-bg"></div>
                        </div>
                    </div>
                </div>
                <div class="col skeleton-card-wrap">
                    <div class="skeleton-card">
                        <div class="skeleton-card-image skeleton-bg"></div>
                        <div class="skeleton-card-text-block">
                            <div class="skeleton-card-price skeleton-bg"></div>
                            <div class="skeleton-card-title skeleton-bg"></div>
                        </div>
                    </div>
                </div>
                <div class="col skeleton-card-wrap">
                    <div class="skeleton-card">
                        <div class="skeleton-card-image skeleton-bg"></div>
                        <div class="skeleton-card-text-block">
                            <div class="skeleton-card-price skeleton-bg"></div>
                            <div class="skeleton-card-title skeleton-bg"></div>
                        </div>
                    </div>
                </div>
                <div class="col skeleton-card-wrap">
                    <div class="skeleton-card">
                        <div class="skeleton-card-image skeleton-bg"></div>
                        <div class="skeleton-card-text-block">
                            <div class="skeleton-card-price skeleton-bg"></div>
                            <div class="skeleton-card-title skeleton-bg"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
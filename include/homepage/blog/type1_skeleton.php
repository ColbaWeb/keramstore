<style>
    .skeleton-blog {
        padding: 36px 0;
    }
    .skeleton-blog .skeleton-section-name{
        height: 44px;
        width: 200px;
        margin-bottom: 12px;
    }
    .skeleton-blog .col-6:first-child {
        display: flex;
        flex-direction: column;
        justify-content: center;
    }
    .skeleton-blog .skeleton-section-subtitle {
        height: 24px;
        width: 150px;
        margin-bottom: 40px;
    }
    .skeleton-blog .skeleton-blog-link-title {
        height: 26px;
        width: 100%;
        margin-bottom: 4px;
    }
    .skeleton-blog .skeleton-blog-link-date {
        height: 20px;
        width: 150px;
    }
    .skeleton-blog .skeleton-blog-link:not(:last-child) {
        margin-bottom: 24px;
    }
    .skeleton-blog .skeleton-image {
        padding-bottom: 75%;
    }

    .skeleton-blog-mobile {
        display: none;
    }

    .skeleton-blog-mobile .row {
        margin: 0 -4px;
        flex-wrap: nowrap;
        overflow: scroll;
    }
    .skeleton-blog-mobile .col {
        padding: 0 4px;
    }

    .skeleton-blog-mobile .col {
        max-width: 292px;
        min-width: 292px;
    }
    .skeleton-mobile-card-image {
        padding-bottom: 75%;
        margin-bottom: 6px;
    }

    .skeleton-mobile-card-title {
        height: 72px;
        width: 100%;
        margin-bottom: 4px;
    }
    
    .skeleton-mobile-card-date {
        height: 20px;
        width: 100px;
    }

    .skeleton-blog-mobile-title-block {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 32px;
    }

    .skeleton-blog-mobile-title {
        height: 40px;
        width: 150px;
    }
    .skeleton-blog-mobile-link {
        height: 24px;
        width: 150px;
    }

    @media (max-width: 1366px) {
        .skeleton-blog .skeleton-section-name{
            height: 40px;
        }
        .skeleton-blog .skeleton-blog-link-title {
            height: 24px;
        }
        .skeleton-blog .skeleton-blog-link:not(:last-child) {
            margin-bottom: 16px;
        }
        .skeleton-blog .skeleton-section-subtitle {
            margin-bottom: 32px;
        }
    }

    @media (max-width: 1024px) {
        .skeleton-blog {
            display: none;
        }
        .skeleton-blog-mobile {
            display: block;
        }
    }

    @media (max-width: 575px) {
        .skeleton-blog-mobile-title-block {
            margin-bottom: 24px;
        }
        .skeleton-blog-mobile-title {
            height: 28px;
        }
        .skeleton-mobile-card-title {
            height: 36px;
        }
    }
</style>

<div class="container">
    <div class="skeleton-blog">
        <div class="row">
            <div class="col-6">
                <div class="skeleton-section-name skeleton-bg"></div>
                <div class="skeleton-section-subtitle skeleton-bg"></div>
                <div class="skeleton-blog-links">
                    <div class="skeleton-blog-link">
                        <div class="skeleton-blog-link-title skeleton-bg"></div>
                        <div class="skeleton-blog-link-date skeleton-bg"></div>
                    </div>
                    <div class="skeleton-blog-link">
                        <div class="skeleton-blog-link-title skeleton-bg"></div>
                        <div class="skeleton-blog-link-date skeleton-bg"></div>
                    </div>
                    <div class="skeleton-blog-link">
                        <div class="skeleton-blog-link-title skeleton-bg"></div>
                        <div class="skeleton-blog-link-date skeleton-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="skeleton-image skeleton-bg"></div>
            </div>
        </div>
    </div>
    <div class="skeleton-blog-mobile">
        <div class="skeleton-blog-mobile-title-block">
            <div class="skeleton-blog-mobile-title skeleton-bg"></div>
            <div class="skeleton-blog-mobile-link skeleton-bg"></div>
        </div>
        <div class="row">
            <div class="col">
                <div class="skeleton-mobile-card">
                    <div class="skeleton-mobile-card-image skeleton-bg"></div>
                    <div class="skeleton-mobile-card-text-block">
                        <div class="skeleton-mobile-card-title skeleton-bg"></div>
                        <div class="skeleton-mobile-card-date skeleton-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="skeleton-mobile-card">
                    <div class="skeleton-mobile-card-image skeleton-bg"></div>
                    <div class="skeleton-mobile-card-text-block">
                        <div class="skeleton-mobile-card-title skeleton-bg"></div>
                        <div class="skeleton-mobile-card-date skeleton-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="skeleton-mobile-card">
                    <div class="skeleton-mobile-card-image skeleton-bg"></div>
                    <div class="skeleton-mobile-card-text-block">
                        <div class="skeleton-mobile-card-title skeleton-bg"></div>
                        <div class="skeleton-mobile-card-date skeleton-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="skeleton-mobile-card">
                    <div class="skeleton-mobile-card-image skeleton-bg"></div>
                    <div class="skeleton-mobile-card-text-block">
                        <div class="skeleton-mobile-card-title skeleton-bg"></div>
                        <div class="skeleton-mobile-card-date skeleton-bg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
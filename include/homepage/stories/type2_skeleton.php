<style>

    .skeleton-stories.type-2 {
        display: flex;
        align-items: center;
        padding: 36px 12px;
    }
    .skeleton-stories.type-2 .skeleton-stories-title {
        height: 44px;
        width: 220px;
        margin-bottom: 12px;
    }
    .skeleton-stories.type-2 .skeleton-stories-text {
        height: 24px;
        width: 200px;
    }
    .skeleton-stories.type-2 .skeleton-stories-text-container {
        min-width: 224px;
        margin-right: 24px;
    }
    .skeleton-stories.type-2 .skeleton-stories-list {
        display: flex;
        max-width: calc(100% - 224px);
        overflow: hidden;
    }
    .skeleton-stories.type-2 .skeleton-stories-item-wrap {
        width: 166px;
        flex-shrink: 0;
        margin-right: 24px;
    }
    .skeleton-stories.type-2 .skeleton-stories-item {
        height: 166px;
        border-radius: 100%;
    }

    @media (max-width: 1600px) {
        .skeleton-stories.type-2 .skeleton-stories-item-wrap {
            width: 141px;
        }
        .skeleton-stories.type-2 .skeleton-stories-item {
            height: 141px;
        }
        .skeleton-stories.type-2 {
            padding: 28px 12px;
        }
    }

    @media (max-width: 1366px) {
        .skeleton-stories.type-2 .skeleton-stories-title {
            height: 40px;
        }
        .skeleton-stories.type-2 .skeleton-stories-text {
            height: 24px;
        }
    }
    @media (max-width: 991px) {
        .skeleton-stories.type-2 {
            flex-direction: column;
            align-items: flex-start;
            padding: 24px 0;
        }
        .skeleton-stories.type-2 .skeleton-stories-text-container {
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            min-width: auto;
            margin: 0 0 24px;
        }
        .skeleton-stories.type-2 .skeleton-stories-title {
            margin: 0;
        }
        .skeleton-stories.type-2 .skeleton-stories-list {
            max-width: none;
            width: 100%;
        }
    }
    @media (max-width: 575px) {
        .skeleton-stories.type-2 .skeleton-stories-title {
            height: 28px;
            width: 100px;
        }
        .skeleton-stories.type-2 .skeleton-stories-text {
            width: 120px;
        }
        .skeleton-stories.type-2 .skeleton-stories-item-wrap {
            margin-right: 8px;
        }
        .skeleton-stories.type-2 {
            padding: 24px 0;
        }
    }
</style> 

<div class="container">
    <div class="skeleton-stories type-2">
        <div class="skeleton-stories-text-container">
            <div class="skeleton-stories-title skeleton-bg"></div>
            <div class="skeleton-stories-text skeleton-bg"></div>
        </div>
        <div class="skeleton-stories-list">
            <div class="skeleton-stories-item-wrap"><div class="skeleton-stories-item skeleton-bg"></div></div>
            <div class="skeleton-stories-item-wrap"><div class="skeleton-stories-item skeleton-bg"></div></div>
            <div class="skeleton-stories-item-wrap"><div class="skeleton-stories-item skeleton-bg"></div></div>
            <div class="skeleton-stories-item-wrap"><div class="skeleton-stories-item skeleton-bg"></div></div>
            <div class="skeleton-stories-item-wrap"><div class="skeleton-stories-item skeleton-bg"></div></div>
            <div class="skeleton-stories-item-wrap"><div class="skeleton-stories-item skeleton-bg"></div></div>
        </div>

    </div>
</div>
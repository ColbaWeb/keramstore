<style>
    .skeleton-map {
        height: 700px;
        width: 100%;
    }
    @media (max-width: 1366px) {
        .skeleton-map {
            height: 600px;
        }
    }
    @media (max-width: 575px) {
        .skeleton-map {
            height: 80vh;
        }
    }
</style>
<div class="container">
    <div class="skeleton-map skeleton-bg">
    </div>
</div>
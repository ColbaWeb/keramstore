<?

global $arrSectionsFilterNew;
$arrSectionsFilterNew = array("=UF_NEW" => 1);

$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "homepage_collections",
    array(
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "nt_alpha_catalog",
        "IBLOCK_ID" => "5",
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "3",
        "SECTION_FIELDS" => array(
            0 => "NAME",
            1 => "DESCRIPTION",
            2 => "PICTURE",
            3 => "DETAIL_PICTURE",
            4 => "IBLOCK_SECTION_ID",
            5 => "",
        ),
        "SECTION_USER_FIELDS" => array(
            0 => "UF_NEW",
            1 => "UF_HIT",
            2 => "UF_SALE",
            3 => "UF_IN_SALON",
            4 => "UF_SURFACE",
            5 => "UF_SIZES",
            6 => "UF_MIN_PRICE",
            7 => "UF_BRANDS",
            8 => "UF_SIZES_LINE",
            9 => "UF_SURFACE_LINE",
            10 => "",
        ),
        "ADD_SECTIONS_CHAIN" => "N",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "N",
        "COMPONENT_TEMPLATE" => "homepage_collections",
        "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
        "FILTER_NAME" => "arrSectionsFilterNew",
        "CACHE_FILTER" => "Y",
        "SECTION_TITLE" => "Новинки",
        "SECTION_LINK_TYPE" => "C",
        "SECTION_LINK_TITLE" => "В каталог",
        "SECTION_LINK_URL" => "/catalog/",
        "SHOW_SECTIONS_ID" => array(
        ),
        "SHOW_SECTIONS_COUNT" => "20",
        "TYPE_HOME" => "NEW",
        "DETAIL_LINK" => "/novinki",
        "CUSTOM_SECTION_SORT" => array(
            "DATE_UPDATE" => "DESC",
        ),
        "ADDITIONAL_COUNT_ELEMENTS_FILTER" => "additionalCountFilter",
        "HIDE_SECTIONS_WITH_ZERO_COUNT_ELEMENTS" => "N"
    ),
    false
);?>
<style>
    .skeleton-tabs-with-goods {
        padding: 36px 0;
    }
    .skeleton-tabs-with-goods .skeleton-section-name{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin-bottom: 40px;
    }
    .skeleton-tabs-with-goods .skeleton-section-name .skeleton-title{
        height: 44px;
        width: 200px;
        margin-bottom: 12px;
        background: #F5F5F5;
    }
    .skeleton-tabs-with-goods .skeleton-section-name .skeleton-subtitle {
        height: 24px;
        width: 96px;
        background: #F5F5F5;
    }

    .skeleton-tabs-with-goods .skeleton-section-name .skeleton-tabs {
        height: 48px;
        width: 100%;
        max-width: 550px;
        background: #F5F5F5;
    }

    .skeleton-tabs-with-goods .skeleton-card.big {
        height: calc(100% - 85px);
    }
    
    .skeleton-tabs-with-goods .skeleton-card-image {
        height: 100%;
        padding-bottom: 100%;
        margin-bottom: 16px;
    }
    .skeleton-tabs-with-goods .skeleton-card-price {
        height: 24px;
        width: 50%;
        margin-bottom: 4px;
    }
    .skeleton-tabs-with-goods .skeleton-card-title {
        height: 40px;
        width: 100%;
    }

    .skeleton-tabs-with-goods .skeleton-cards-list > .col-12:last-child .col-6:nth-child(1), .skeleton-tabs-with-goods .skeleton-cards-list > .col-12:last-child .col-6:nth-child(2) {
        margin-bottom: 24px;
    }

    @media (max-width: 1366px) {
         .skeleton-tabs-with-goods {
            padding: 28px 0;
        }
    }

    @media (max-width: 767px) {
        .skeleton-tabs-with-goods .skeleton-cards-list > .col-12:first-child {
            margin-bottom: 24px;
        }
        .skeleton-tabs-with-goods .skeleton-cards-list > .col-12:last-child .col-6:nth-child(1), .skeleton-cards-list > .col-12:last-child .col-6:nth-child(2) {
            margin-bottom: 8px;
        }
        .skeleton-tabs-with-goods .skeleton-cards-list > .col-12:last-child > .row {
            margin: 0 -4px;
        }
        .skeleton-tabs-with-goods .skeleton-cards-list .col-6 {
            padding: 0 4px;
        }
    }

    @media (max-width: 575px) {
        .skeleton-tabs-with-goods .skeleton-section-name{
            margin-bottom: 24px;  
            align-items: flex-start;  
        }
        .skeleton-tabs-with-goods .skeleton-section-name .skeleton-title {
            height: 28px;
        }
        .skeleton-tabs-with-goods .skeleton-section-name .skeleton-subtitle {
            height: 24px;
        }
         .skeleton-tabs-with-goods {
            padding: 24px 0;
        }

    }
</style>

<div class="container">
    <div class="skeleton-tabs-with-goods">
        <div class="skeleton-section-name">
            <div class="skeleton-title"></div>
            <div class="skeleton-tabs"></div>
        </div>
        <div class="skeleton-cards-list row">
            <div class="col-12 col-md-6">
                <div class="skeleton-card big">
                    <div class="skeleton-card-image skeleton-bg"></div>
                    <div class="skeleton-card-text-block">
                        <div class="skeleton-card-price skeleton-bg"></div>
                        <div class="skeleton-card-title skeleton-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row">
                    
                    <div class="col-6">
                        <div class="skeleton-card">
                            <div class="skeleton-card-image skeleton-bg"></div>
                            <div class="skeleton-card-text-block">
                                <div class="skeleton-card-price skeleton-bg"></div>
                                <div class="skeleton-card-title skeleton-bg"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="skeleton-card">
                            <div class="skeleton-card-image skeleton-bg"></div>
                            <div class="skeleton-card-text-block">
                                <div class="skeleton-card-price skeleton-bg"></div>
                                <div class="skeleton-card-title skeleton-bg"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="skeleton-card">
                            <div class="skeleton-card-image skeleton-bg"></div>
                            <div class="skeleton-card-text-block">
                                <div class="skeleton-card-price skeleton-bg"></div>
                                <div class="skeleton-card-title skeleton-bg"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="skeleton-card">
                            <div class="skeleton-card-image skeleton-bg"></div>
                            <div class="skeleton-card-text-block">
                                <div class="skeleton-card-price skeleton-bg"></div>
                                <div class="skeleton-card-title skeleton-bg"></div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
        

    </div>
</div>
<?
$APPLICATION->IncludeComponent(
	"nextype:alpha.company.short", 
	"main", 
	array(
		"SECTION_TITLE" => "Коротко о нас",
		"SECTION_LINK_TITLE" => "Подробнее о компании",
		"SECTION_LINK_URL" => "/company/",
		"DISPLAY_SOCIAL_LINKS" => "Y",
		"DESCRIPTION_TITLE" => "",
		"DESCRIPTION_TEXT" => "",
		"DESCRIPTION_LINK_TITLE" => "",
		"DESCRIPTION_LINK_URL" => "/catalog/",
		"HL_ADVANTAGES_ID" => "2",
		"HL_ADVANTAGES_DISPLAY_ID" => array(
			0 => "1",
			1 => "2",
			2 => "3",
			3 => "4",
		),
		"CONTACTS_TYPE" => "S",
		"COMPONENT_TEMPLATE" => "main",
		"SECTION_IMAGE" => ""
	),
	false
);
?>
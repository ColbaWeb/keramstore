<style>
    .skeleton-company .skeleton-section-name{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin-bottom: 40px;
    }
    .skeleton-company .skeleton-section-name .skeleton-title{
        height: 44px;
        width: 200px;
        margin-bottom: 12px;
        background: #F5F5F5;
    }
    .skeleton-company .skeleton-section-name .skeleton-subtitle {
        height: 24px;
        width: 96px;
        background: #F5F5F5;
    }

    .skeleton-company .skeleton-company-title {
        height: 32px;
        width: 70%;
        margin-bottom: 24px;
    }

    .skeleton-company .skeleton-company-text {
        height: 188px;
        width: 100%;
        margin-bottom: 23px;
    }

    .skeleton-company .skeleton-company-link {
        height: 26px;
        width: 140px;
        margin-bottom: 32px;
    }

    .skeleton-company .skeleton-company-advantages {
        display: flex;
        flex-wrap: wrap;
        margin: 0 -12px;
    }

    .skeleton-company .skeleton-company-advantage{
        flex: 0 0 50%;
        max-width: 50%;
        margin-bottom: 24px;
        padding: 0 12px;
        display: flex;
    }

    .skeleton-company .skeleton-company-advantage .skeleton-advantage-img {
        width: 48px;
        height: 48px;
        margin-right: 12px;
    }

    .skeleton-company .skeleton-company-advantage .skeleton-text-block-title {
        height: 26px;
        margin-bottom: 8px;
        width: 100%;
    }

    .skeleton-company .skeleton-company-advantage .skeleton-text-block-text {
        height: 48px;
        width: 100%;
    }

    .skeleton-company .skeleton-company-image {
        width: 100%;
        height: 405px;
        position: relative;
    }

    .skeleton-company .skeleton-advantage-text-block {
        flex: 1;
    }

    .skeleton-company .skeleton-company-contact-info {
        padding: 16px 15px 15px 15px;
    }

    .skeleton-company .skeleton-company-contact-info > div {
        height: 24px;
        width: 70%;
    }

    .skeleton-company .skeleton-company-contact-info > div:not(:last-child)  {
        margin-bottom: 8px;
    }

    .skeleton-company .skeleton-company-image-socials {
        position: absolute;
        display: block;
        background: #FCFCFC;
        height: 48px;
        width: 80%;
        left: 16px;
        bottom: 16px;
    }

    @media (max-width: 1366px) {
        .skeleton-company .skeleton-section-name .skeleton-title{
            margin-bottom: 32px;
        }
        .skeleton-company .skeleton-company-title {
            height: 28px;
            margin-bottom: 20px;
        }
        .skeleton-company .skeleton-company-text {
            height: 184px;
            margin-bottom: 19px;
        }
        .skeleton-company .skeleton-company-link {
            margin-bottom: 24px;
        }
        .skeleton-company .skeleton-company-advantage {
            margin-bottom: 16px;
        }
        .skeleton-company .skeleton-company-advantage .skeleton-text-block-title {
            height: 24px;
        }
        .skeleton-company .skeleton-company-advantage .skeleton-text-block-text {
            height: 40px;
        }
        .skeleton-company .skeleton-company-image {
            height: 343px;
        }
        .skeleton-company .skeleton-company-contact-info {
            padding: 16px 11px 15px 11px;
        }
        .skeleton-company .skeleton-company-contact-info > div {
            height: 20px;
            width: 80%;
        }
        .skeleton-company .skeleton-company-image-socials  {
            left: 12px;
            bottom: 12px;
        }
    }

    @media (max-width: 1024px) {
        .skeleton-company .skeleton-company-advantages {
            margin: 0;
            padding: 0;
            flex-direction: column;
        }
        .skeleton-company .skeleton-company-advantage {
            padding: 0;
            width: 100%;
            flex: 1;
            max-width: none;
        }
    }

    @media (max-width: 768px) {
        .skeleton-company .col-6 {
            flex: 0 0 100%;
            max-width: 100%;
        }
    }

    @media (max-width: 575px) {
        .skeleton-company .skeleton-section-name{
            margin-bottom: 24px;  
            align-items: flex-start;  
        }
        .skeleton-company .skeleton-section-name .skeleton-title {
            margin-bottom: 8px;
            height: 28px;
        }
        .skeleton-company .skeleton-company-image {
            height: 212px;
        }
        .skeleton-company .skeleton-company-title {
            height: 24px;
            margin-bottom: 16px;
        }
        .skeleton-company .skeleton-company-text {
            height: 180px;
            margin-bottom: 14px;
        }
        .skeleton-company .skeleton-company-link {
            height: 33px;
        }
        .skeleton-company .skeleton-company-advantage .skeleton-advantage-img {
            margin-right: 8px;
        }
        .skeleton-company .skeleton-company-advantage .skeleton-text-block-title {
            height: 18px;
            margin-bottom: 4px;
        }
        .skeleton-company .skeleton-company-advantage .skeleton-text-block-text {
            height: 16px;
        }
        .skeleton-company .skeleton-company-contact-info > div {
            height: 16px;
            width: 60%;
        }
        .skeleton-company .col-6:last-child {
            padding: 0;
        }
        .skeleton-company .skeleton-company-image-socials  {
            left: 8px;
            bottom: 8px;
        }
    }
</style>

<div class="container">
    <div class="skeleton-company">
       <div class="skeleton-section-name">
            <div class="skeleton-title"></div>
            <div class="skeleton-subtitle"></div>
        </div>
        <div class="skeleton-company-content">
            <div class="row">
                <div class="col-6">
                    <div class="skeleton-company-title skeleton-bg"></div>
                    <div class="skeleton-company-text skeleton-bg"></div>
                    <div class="skeleton-company-link skeleton-bg"></div>
                    <div class="skeleton-company-advantages">
                        <div class="skeleton-company-advantage">
                            <div class="skeleton-advantage-img skeleton-bg"></div>
                            <div class="skeleton-advantage-text-block">
                                <div class="skeleton-text-block-title skeleton-bg"></div>
                                <div class="skeleton-text-block-text skeleton-bg"></div>
                            </div>
                        </div>
                        <div class="skeleton-company-advantage">
                            <div class="skeleton-advantage-img skeleton-bg"></div>
                            <div class="skeleton-advantage-text-block">
                                <div class="skeleton-text-block-title skeleton-bg"></div>
                                <div class="skeleton-text-block-text skeleton-bg"></div>
                            </div>
                        </div>
                        <div class="skeleton-company-advantage">
                            <div class="skeleton-advantage-img skeleton-bg"></div>
                            <div class="skeleton-advantage-text-block">
                                <div class="skeleton-text-block-title skeleton-bg"></div>
                                <div class="skeleton-text-block-text skeleton-bg"></div>
                            </div>
                        </div>
                        <div class="skeleton-company-advantage">
                            <div class="skeleton-advantage-img skeleton-bg"></div>
                            <div class="skeleton-advantage-text-block">
                                <div class="skeleton-text-block-title skeleton-bg"></div>
                                <div class="skeleton-text-block-text skeleton-bg"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="skeleton-company-image skeleton-bg">
                        <div class="skeleton-company-image-socials"></div>
                    </div>
                    <div class="skeleton-company-contact-info">
                        <div class="skeleton-company-adress skeleton-bg"></div>
                        <div class="skeleton-company-time skeleton-bg"></div>
                        <div class="skeleton-company-phone skeleton-bg"></div>
                        <div class="skeleton-company-email skeleton-bg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
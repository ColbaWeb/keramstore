<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="//yastatic.net/share2/share.js" charset="utf-8"></script>

<div class="social-wrap">
    <a href="javascript:void(0);" class="social" id="share-social-btn">
        <i class=" icon icon-share-2"></i>
    </a>
    <script>
        $(document).ready(function(){
            $('#share-social-btn').on('click', function(){
                $(this).parent().toggleClass('opened');
            });
        })
    </script>
    <div class="yashare-auto-init ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,viber,whatsapp,skype,telegram"></div>
</div>
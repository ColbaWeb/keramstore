<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Nextype\Alpha\Application;
use Nextype\Alpha\Layout;

if (\Bitrix\Main\Loader::includeModule('nextype.alpha'))
{
    try
    {
        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

        if ($request->isPost() && strlen($request->getPost('params')) > 0)
        {
            $arParams = unserialize(base64_decode($request->getPost('params')));
            if (is_array($arParams) && $arParams['file'])
            {
                Layout\Assets::getInstance()->addLess(array (
                    SITE_TEMPLATE_PATH . '/less/elements.less',
                    SITE_TEMPLATE_PATH . '/less/icons.less',
                    SITE_TEMPLATE_PATH . '/less/item-detail.less',
                ));
                
                Layout\Partial::getInstance($arParams['currentDir'])->render($arParams['file'], $arParams['data']);
                
                Layout\Assets::getInstance()->compileLess();
                Layout\Assets::getInstance()->pasteInlineCss();
                Layout\Assets::getInstance()->pasteInlineJs();
            }
            
        }
    }
    catch (Exception $e)
    {
        echo $e->getMessage();
    }
    
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
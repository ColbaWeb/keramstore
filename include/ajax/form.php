<?
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;
use Bitrix\Main\Loader;
use Bitrix\Main\Application as BitrixApplication;

define('DISABLED_REGIONS_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!Loader::includeModule("nextype.alpha"))
{
    Tools\Messages::error('The nextype.alpha module could not be connected');
    return;
}

$request = BitrixApplication::getInstance()->getContext()->getRequest();

if (strlen($request->get('id')) == 0)
{
    Tools\Messages::error('Form code not set');
    return;
}

$formId = Tools\Forms::getFormId($request->get('id'));
if (empty($formId))
{
    Tools\Messages::error('Form not found');
    return;
}

?>
<?$APPLICATION->IncludeComponent(
	"bitrix:form",
	"popup",
	Array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AUTOCOMPLETE_FIELD_NAME" => "",
		"AUTOCOMPLETE_FIELD_VALUE" => "",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "N",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"NOT_SHOW_FILTER" => "",
		"NOT_SHOW_TABLE" => "",
		"RESULT_ID" => $_REQUEST["RESULT_ID"],
		"SEF_MODE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_STATUS" => "N",
		"SHOW_VIEW_PAGE" => "N",
		"START_PAGE" => "new",
		"SUCCESS_URL" => "",
		"USER_CONSENT" => "Y",
		"USER_CONSENT_ID" => "1",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => Array("action"=>"action"),
		"WEB_FORM_ID" => $formId
	)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>
<?
use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;
use Bitrix\Main\Loader;
use Bitrix\Main\Application as BitrixApplication;

define('DISABLED_REGIONS_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!Loader::includeModule("nextype.alpha"))
{
    Tools\Messages::error('The nextype.alpha module could not be connected');
    return;
}

$request = BitrixApplication::getInstance()->getContext()->getRequest();

$APPLICATION->IncludeComponent("nextype:alpha.buy1click", "main", Array (
    "PRODUCT_ID" => intval($request->get('id')),
    "QUANTITY" => intval($request->get('qty')) > 1 ? intval($request->get('qty')) : 1
) );


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Nextype\Alpha\Application;
use Nextype\Alpha\Tools;
use Bitrix\Main\Web\Json;

if (\Bitrix\Main\Loader::includeModule('nextype.alpha'))
{
    try
    {
        $result = false;
        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        
        if (strlen($request->get('id')) > 0)
        {
            
            $productId = $request->get('id');
            $list = Tools\Wishlist::getList(true);
            
            if (isset($list[$productId]))
            {
                if (Tools\Wishlist::deleteItem($productId))
                {
                    $result = array (
                        'result' => 'ok',
                        'state' => 'deleted'
                    );
                }
            }
            else
            {
                if (Tools\Wishlist::addItem($productId))
                {
                    $result = array (
                        'result' => 'ok',
                        'state' => 'added'
                    );
                }
            }
            
        }
        
        if ($result)
        {
            print Json::encode($result);
        }
        else
        {
            print Json::encode(array (
                'result' => 'error',
                'message' => ''
            ));
        }

    }
    catch (Exception $e)
    {
        print Json::encode(array (
            'result' => 'error',
            'message' => $e->getMessage()
        ));
    }
    
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
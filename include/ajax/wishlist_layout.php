<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


use Bitrix\Main\Localization\Loc;
use Nextype\Alpha\Application;
use Nextype\Alpha\Options;
use Nextype\Alpha\Tools;

if (\Bitrix\Main\Loader::includeModule('nextype.alpha'))
{

    Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/header.php");
    
    if (!Options\Base::getInstance()->getValue('wishList'))
        return;

    $listId = $cartId = "bx_wishlist".RandString();

    $items = Tools\Wishlist::getList();

?>

<a href="<?= SITE_DIR ?>wishlist/" class="favorites<?= isset($items) && !empty($items) ? ' no-empty' : '' ?>">
    <i class="icon icon-bx-heart"></i>
    <span><?= Loc::getMessage('NT_ALPHA_HEADER_WISHLIST_TITLE') ?></span>
</a>

<?
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");

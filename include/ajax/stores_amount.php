<?
use Bitrix\Main\Application as BitrixApplication;
use Bitrix\Main\Text\Encoding;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$request = BitrixApplication::getInstance()->getContext()->getRequest();
$params = $request->get('params');

if (isset($_REQUEST['charset']) && strtolower($_REQUEST['charset']) != "utf-8")
{
    $params["MAIN_TITLE"] = Encoding::convertEncoding($params["MAIN_TITLE"], 'UTF-8', $_REQUEST['charset']);
}

if (is_array($params))
{
	$APPLICATION->IncludeComponent("bitrix:catalog.store.amount", "main", $params);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>
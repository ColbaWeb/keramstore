<?php
define('DISABLED_REGIONS_EVENT', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Nextype\Alpha\Application;
use Nextype\Alpha\Regions;

if (\Bitrix\Main\Loader::includeModule('nextype.alpha'))
{
    try
    {

        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        $response = \Bitrix\Main\Context::getCurrent()->getResponse();
        
        if (strlen($request->get('id')) > 0)
        {
            Regions\Base::getInstance()->setRegion($request->get('id'));
        }
        
        if (strlen($request->get('r')) > 0)
        {
            $response->addHeader('Location', $request->get('r'));
            $response->flush();
        }
    }
    catch (Exception $e)
    {
        echo $e->getMessage();
    }
    
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
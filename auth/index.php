<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
?>
<script>
	<?if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0 && preg_match('#^/\w#', $_REQUEST["backurl"])):?>
	document.location.href = "<?=CUtil::JSEscape($_REQUEST["backurl"])?>";
	<?endif?>
</script>


<div class="alert alert-info">
    <b class="alert-heading">Поздравляем!</b>
    <p>Вы зарегистрированы и успешно авторизовались.</p>
    
    <a href="<?=SITE_DIR?>" class="btn btn-primary">Вернуться на главную страницу</a>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>